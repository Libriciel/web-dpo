# Docker

## Avertissements

### Fichiers envoyés dans les conteneurs docker

L'utilisation avec docker en mode développement implique, pour éviter de polluer l'espace de travail sur le poste du
développeur avec les répertoires `app/tmp`, `vendors` ou `node_modules`, de lister les fichiers à envoyer dans le
conteneur docker dans les fichiers `docker-compose.yml` et `docker/app/Dockerfile`.

Cela ne pose pas de problème pour les fichiers se trouvant dans `app`, mais peut nécessiter des ajustements si des fichiers
sont ajoutés dans d'autres répertoires ou à la racine.

### Patches SQL

Par ailleurs, pour pouvoir passer les fichiers SQL au lancement du docker, il faut penser, si on renomme ou ajoute des
fichiers SQL, à modifier la fonction `init_data_db` du fichier `docker/app/usr/bin/docker-entrypoint.sh`.

### Fichiers importants pour docker

| Fichier / dossier                                                 | Git     | Commentaire                                                                                                         |
| ---                                                               | ---     | ---                                                                                                                 |
| `docker/app/Dockerfile`                                           | Nouveau  | `Dockerfile` du conteneur applicatif                                                                                                                   |
| `docker/app/etc/apache2/sites-available/webdpo.conf`              | Nouveau  | Configuration apache du conteneur applicatif                                                                                                                   |
| `docker/app/etc/cron.d/web-dpo`                                   | Nouveau  | Tâche planifiée du conteneur applicatif                                                                          |
| `docker/app/usr/bin/composer-install.php`                         | Nouveau  | Script facilitant l'installation de `composer` sur le conteneur applicatif                                                                                                                  |
| `docker/app/usr/bin/docker-entrypoint.sh`                         | Nouveau  | Script exécuté au lancement du conteneur applicatif, passe les patches SQL, corrige les sources de PHPUnit, création de liens symboliques, modification des permissions et lancement des services (cron, apache)                                  |
| `docker/app/var/www/html/web-dpo/app/Config/database.php`         | Nouveau  | Configuration de la connexion à la base de données propre au conteneur applicatif                                                                                                                   |
| `docker/app/var/www/html/web-dpo/app/Config/email.php`            | Nouveau  | Configuration du serveur de mail propre au conteneur applicatif                                                                                                                  |
| `docker/postgres/docker-entrypoint-initdb.d/000-init.sql`         | Nouveau  | Création de l'utilisateur et de la base de données `webdpo` sur le conteneur PostgreSQL                                                       |
| `.env`                                                            | Nouveau  | Variables d'environnement utilisées pour les conteneurs docker                                                                                         |
| `cake_utils.sh`                                                   | Nouveau  | Lancement de commandes (pre_commit, lint, tests, ...) dans le conteneur applicatif |
| `docker-compose.sh`                                               | Nouveau  | Commandes permettant de nettoyer ou de lancer les conteneurs docker nécessaires à web-DPO (voir ci-dessous)                                                                                        |
| `docker-compose.yml`                                              | Nouveau  | `docker-compose` des services de `web-DPO`                                                                                       |
| `README-docker.md`                                                | Nouveau  | Ce fichier d'information                                                                                                                   |

## Commandes

### Lancement de web-DPO

```bash
./docker-compose.sh web-dpo
```

### Nettoyage des images docker etc...

Lorsqu'on change de page ou que l'on a fait des modifications importantes, pour supprimer les images docker etc et
repartir sur une installation propre.

```bash
./docker-compose.sh clear
```

### Exécution côté conteneur applicatif

```bash
docker-compose exec web-dpo-app /bin/bash
```

#### Console de vérification de l'application

```bash
sudo -u www-data vendors/bin/cake checks -app app
```

#### Lancement de l'intégration continue manuellement

```bash
./cake_utils.sh pre_commit
```

## Conteneurs Docker

| Application     | Container             | URL                    | Identifiant            |
| ---             | ---                   | ---                    | ---                    |
| __Cloudooo__    | `web-dpo-cloudooo`    |                        |                        |
| __Golem__       | `web-dpo-golem`       |                        |                        |
| __Mailcatcher__ | `web-dpo-mailcatcher` | http://localhost:2081/ |                        |
| __PostgreSQL__  | `web-dpo-db`          |                        | `webdpo` / `webdpo`    |
| __web-DPO__     | `web-dpo-app`         | http://localhost:2080/ | `superadmin` / `admin` |

Voir le fichier `docker-compose.yml`
