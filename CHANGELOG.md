# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.
Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

[2.1.3] - 02-02-2022
=====

### Ajouts
- [#672](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/672)
  Mise en place de la pagination sur les différentes pages permettant de visualiser les différents états des traitements.

### Evolutions
- [#672](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/672)
  Mise en place d'un appel AJAX lors de la visualisation de l'historique d'un traitement

### Corrections
- [#636](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/636)
  Pb lors de la MAJ entre les traitements et les services lors de la migration de la base de données.

- [#634](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/634)
  Réinitialisation des valeurs des champs définissant si une AIPD est obligatoire lorsqu'ils sont cachés

- Correction de l'utilisation des filtres sur les référentiels lors de la visualisation de tous les traitements de l'entité

- [#673](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/673)
  Conservation des filtres lors du changement de page.

- [#641](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/641)
  Correction de la création d'une norme

[2.1.2] - 23-11-2021
=====

### Ajouts

### Evolutions

### Corrections
- [#629](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/629)
  Faute d'orthographe dans l'ongle AIPD.

- [#628](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/628)
  Gestion des modèles de formulaire.

- [#627](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/627)
  Gestion des droits sur l'action d'activation ou désactivation d'un formulaire.

- [#626](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/626)
  Suppression de l'unicité des noms des fichiers pour les modèles en base de données.

- [#625](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/625)
  De l'utilisation des filtres dans les formulaires 


[2.1.1] - 09-11-2021
=====

### Ajouts
- [#623](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/623)
  Création d'un référentiel avec fichier annexe.
  Mise à disposition du référentiel existante.
  Modification d'un référentiel.
  Abrogation d'un référentiel.
  Association d'un référentiel sur un traitement.
  Filtre via un référentiel dans le registre.
  Ajout des champs normes et référentiel dans les formulaires.

### Evolutions
- [#611](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/611)
  Lors de l'installation de l'application, pouvoir définir lors de l'exécution du SHELL le SALT, le cipherSeed et le mot de passe de l'utilisateur "superadmin".

- Abrogation normes (RU-005, RU-063, AU-004, AU-006, AU-010, AU-028, AU-034, AU-035, AU-046, AU-047, AU-048, AU-049, AU-050, NS-009, NS-016, NS-020, NS-042, NS-046, NS-048, NS-049, NS-050, NS-051, NS-052, NS-054, NS-057)

### Corrections
- [#612](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/612)
  Correction de l'application des conditions sur les différents champs crée dans le formulaire lors de la rédaction d'un traitement

- [#613](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/613)
  Correction de l'affichage de la modal permettant de répondre à un commentaire.

- [#622](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/622)
  Correction de la modification du mot de passe de l'utilisateur.

- Correction de l'affichage de la norme sélectionné sur un traitement après abrogation de celle-ci.

[2.1.0] - 18-06-2021
=====

### Ajouts
- [#453](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/453)
  Mise en place d'un paramètre pour savoir si l'application est en mode SAAS pour adapter l'hébergeur dans le menu "Politique de confidentialité" (RGPD)
  
- [#454](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/454)
  Lors de la rédaction d'un traitement, il est possible à tout moment de générer le projet de traitement. 
- [#454](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/454)
  Mise en place d'une variable sur l'état du traitement afin de pouvoir faire une condition sur le filigrane dans le modèle.
- Ajout d'un droit permettant la génération d'un traitement en cours de rédaction
  
- Ajout des filtres sur les formulaires
  
- [#522](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/522)
  Ajout d'un droit permettant l'initialisation d'un traitement par un autre profil que le DPO.
  
- [#477](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/477)
  Sauvegarde automatique d'un formulaire toute les 10 secondes
  
- [#508](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/508)
  Archivage d'un formulaire
  
- [#522](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/522)
  Ajout d'un droit permettant l'initialisation d'un traitement
  
- [#541](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/541)
  Partage d'un traitement avec les utilisateurs du même service
  
- [#481](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/481)
  Association en masse d'un ou plusieurs services à un ou plusieurs utilisateurs.
- [#481](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/481)
  Ajout de l'import d'une liste de service via un fichier CSV
- [#481](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/481)
  Filtrer les services par nom
- [#481](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/481)
  Dissocier un service de tous les utilisateurs
  
- [#528](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/528)
  Masquage des informations du rédacteur dans un traitement via un paramétre dans l'entité
  
- [#489](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/489)
  Mise en place du fil d'ariane (breadcrumb)
  
- [#534](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/534)
  Ajout aux menus d'administration d'une entité la gestion des articles de la FAQ en tant que superadmin.
  
- [#480](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/480)
  Ajout d'une option au registre pour la génération de l'extrait de registre au format HTML
  
- [#510](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/510)
  Ajout de la possibilité de faire des conditions sur les champs concernant le transfert hors UE, les données sensibles,
la réalisation de l'AIPD et le dépôt de l'AIPD en annexe lors de la création d'un formulaire

- [#473](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/473)
  Duplication d'un traitement, depuis le menu "Toutes les déclaration de l'entité
  
- Ajout d'un droit permettant la duplication d'un traitement

- [#478](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/478)
  Enregistrement d'un traitement en mode "brouillon"

### Evolutions
- [#322](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/322)
  Modification des mails envoyés en notification.
  
- [#302](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/302)
  Le numéro d'enregistrement défini manuellement lors de l'insertion au registre, ne peut plus être identique au préfixe du registre.
  
- [#447](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/447)
  Lors de la déclaration d'un sous-traitant seul la "Raison sociale" est obligatoire, pour pouvoir intégrer les sous-traitants non Français.
- [#447](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/447)
  Lors de la déclaration d'un co-responsable seul la "Raison sociale" est obligatoire, pour pouvoir intégrer les co-responsables non Français.
  
- [#459](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/459)
  Mise à jour de TinyMCE en version 5.5.1

- [#457](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/457)
  Mise en place du moteur TinyMCE pour le formatage des champs "Champ d'information" et "Label" sur le formulaire.  
  
- [#449](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/449)
  Mise en place du moteur TinyMCE pour le formatage de la réponse lors d'une demande de consultation.

- [#474](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/474)
  Gestion des formulaires pour toutes les entitées
  
- [#542](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/542)
  Gestion des modèlès d'extrait de registre pour les entitées

- [#543](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/543)
  Gestion des modèles de formulaire pour les entitées

- [#516](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/516)
  Gestion des modèlès présentation
  
- [#501](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/501)
  Migration de boostrap en version 4.5

- [#500](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/500)
  Migration Font awesome 5

- [#519](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/519)
  Modification du texte de la page de politique de confidentialité (RGPD), prise en compte du mode SAAS et installation par le client
  
- Lors de l'initialisation d'un traitement, le rédacteur n'est plus afficher.
  
- [#524](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/524)
  Modification de l'e-mail pour le DPO dans l'entité. Il ne correspond plus à l'e-mail de l'utilisateur DPO
  
- Modification de la variable "dpo_email" par "dpo_emaildpo"
- Modification de la variable "valeur_rt_organisation_email_dpo" par "valeur_rt_organisation_emaildpo"
  
- [#492](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/492)
  Modififcation de la police d'écriture (Ubuntu)
  
- Amélioration de la navigation en tant que superadmin
  
- [#484](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/484)
  Modification de l'affichage des messages

- [#458](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/458)
  Mise en place des filtres pour rechercher un traitement dans l'entité

- [#547](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/547)
  Mise en place d'un paramètre pour l'identification d'un utilisateur en mode sensible à la case ou non

### Corrections
- [#302](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/302)
  Système de génération du numéro d'enregistrement du traitement au registre.
  
- [#459](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/459)
  Traduction du moteur (TinyMCE) de mise en forme du texte lors de l'ajout ou modification d'un article.
  
- Correction de l'espacement entre le champ de sélection et les boutons dans la modal lors de l'envoi en consultation.
  
- [#533](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/533)
  Classement par ordre alphabétique des organisations
  
- [#463](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/463)
  Amélioration (lenteur) lors de la sélection d'une organisation en mode multi-collectivité après la connexion lorsqu'il y a beaucoup de structure

- [#530](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/530)
  Modification menu superadmin

- [#529](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/529)
  Correction des conditions sur les champs radio pour le masquage du champ
  
- [#514](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/514)
  Factorisation de la sauvegarder des fichiers sur le disque

- [#461](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/461)
  Afficher dans la synthèse d'un traitement le transfert hors UE, co-responsable, sous-traitance, realisation AIPD, obligation AIPD, depot AIPD, RT externe, partage au service

- [#460](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/460)
  Masquage de l'onglet "Informations complémentaires", lorsqu'il n'y a aucune information

- Mise à jour de sécurité. (salt + chiffrement)

### Suppressions
- [#515](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/515)
  Suppression du paramétrage concernant l'utilisation du modèle de présentation
  
- [#470](https://gitlab.libriciel.fr/libriciel/pole-citoyens/web-DPO/web-DPO/-/issues/470)
  Suppression du mode CAS dans le connecteur LDAP

[2.0.3] - 18-03-2021
=====

### Ajouts

### Evolutions

### Corrections
- Correction de la génération documentaire, concernant les variables supplémentaires ajoutés dans le formulaire pour la 
co-responsabilité et la sous-traitance.
- Correction de l'affichage de la modal de notification des traitements lors de la connexion d'un utilisateur.
- Correction : date sortie version patch 2.0.2

[2.0.2] - 03-12-2020
=====

### Ajouts

### Evolutions

### Corrections
- Correction de la mise en place des conditions sur les champs radio, lors de la déclaration d'un traitement


[2.0.1] - 21-10-2020
=====

### Ajouts

### Evolutions

### Corrections
- Correction des droits obligatoire pour le profil "DPO" (patch sql)
- Correction sur l'erreur lors du changement d'entité via la pop-up


[2.0.0] - 18-09-2020
=====

## Ajouts
- Mise en place d'une FAQ.
- Mise en place d'un typage des annexes.
- Mise en place de l'initialisation d'un traitement par le DPO
- Mise en place de la visualisation de tous les traitements au sein de sa collectivité.
- Mise en place des traitements dit de sous-traitance (responsable de traitement externe)
- Mise en place d'un registre de sous-traitance

### sur les droits :
- Ajout d'un droit en rapport avec la création d'un article dans la FAQ.
- Ajout d'un droit en rapport avec la modification d'un article dans la FAQ.
- Ajout d'un droit en rapport avec la suppression d'un article dans la FAQ.
- Ajout d'un droit en rapport avec la visualisation d'un article dans la FAQ.
- Ajout d'un droit en rapport avec la gestion du typage des annexes.
- Ajout d'un droit en rapport avec la consultation de tous les traitements au sein de l'entité.
- Ajout d'un droit en rapport avec la création d'un co-responsable lors de la déclaration d'un traitement.
- Ajout d'un droit en rapport avec la création d'un sous-traitant lors de la déclaration d'un traitement.

### sur les formulaires :
- Mise en place de condition sur les différents champs du formulaire. Condition appliquée lors de la déclaration d'un traitement.
- Mise en place des champs pour la sous-finalité. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place des champs concernant la base légale. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place des champs concernant la prise de décision automatisée. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place des champs concernant le transfert des données hors UE. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place des champs concernant l'utilisation des données sensibles. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place de l'acceptation de différent format de fichier en annexe d'un traitement. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place des questions définissant si une AIPD est obligatoire ou pas. Avec paramétrage sur l'utilisation des champs dans un traitement.
- Mise en place de valeur par défaut lors de la création d'un traitement. Ces valeurs par défaut sont ajoutées lors de la création d'un traitement.
- Ajout de la possibilité de rendre obligatoire un champ case à coché dans le formulaire.

### sur les traitements :
- Ajout d'un champ sur la réalisation de l'AIPD
- Ajout d'un champ sur le dépôt de l'AIPD
- Déclaration de traitement en tant que sous-traitant pour le compte d'un responsable de traitement. Avec notion de sous-traitance ultérieure.

### sur les modèles :
- Ajout des variables concernant les informations complémentaires.
- Ajout des variables concernant le responsable de traitement.
- Ajout des variables concernant l'AIPD.
- Ajout des variables concernant les co-responsables.
- Ajout des variables concernant les sous-traitants.
- Ajout des variables concernant les annexes.

### sur les co-responsables :
- Ajout des informations relatives au DPO.

### sur les sous-traitants :
- Ajout des informations relatives au DPO.

### sur les normes :
- Ajout des normes "Actes réglementaires uniques" (RU-060, RU-063, RU-065, RU-066)
- Ajout des normes "Normes simplifiées" (NS-060)
- Ajout des normes "Référentiel santé" (RS)
- Ajout des normes "Méthodologies de référence" (MR)

### sur le registre :
- Ajout de nouveau filtre


## Evolutions
- Affichage de l'onglet "Information de l'entité" lors de la visualisation d'un traitement au registre
- PostgreSQL 10.9
- PHP 7.4
- Ubuntu 20.04 LTS | Centos8

### sur les formulaires :
- Plus de distinction entre un formulaire sans sous-traitance et avec sous-traitance.

### sur les modèles :
- Modification de certaines variables.

### sur les co-responsables :
- Il n'y a plus de duplication des informations du co-responsable lors de la déclaration d'un traitement. Si le co-responsable est modifié les informations seront reportées automatiquement sur le traitement.
- Certains champs ne sont plus obligatoires.

### sur les sous-traitants :
- Il n'y a plus de duplication des informations du sous-traitant lors de la déclaration d'un traitement. Si le sous-traitant est modifié les informations seront reportées automatiquement sur le traitement.
- Certains champs ne sont plus obligatoires.

### sur un traitement :
- Évolution du libellé du champ "Finalité"
- Évolution du libellé du champ "Transfert hors UE"
- Évolution du libellé du champ "Données sensibles"


## Corrections
- Abroger norme "Actes réglementaires uniques" (RU-059)
- Générations documentaires
- Génération au format CSV


### Suppressions
- Suppression de l'onglet "Information de l'entité" lors de la création, modification et visualisation d'un traitement
- Suppression du verrouillage des traitements au registre.


[1.1.0] - 03-12-2019
=====

### Ajouts
- Mise en place de la civilité du responsable de l'entité.
- Mise en place de la politique de confidentialité de l'application.
- Définition de l'entité responsable de la plate forme web-dpo afin d'afficher les informations de celle-ci dans la page de politique de confidentialité.
- Ajout d'un nouveau type de champ dans le formulaire (multi-select)
- Mise en place des masques sur les champs téléphone, fax et siret d'un sous-traitant
- Ajout de la page de configuration de la page de connexion.
- Ajout variable de la date de création et de la date de dernière modification d'un traitement, pour la génération.
- Ajout de la pagination sur la visualisation de toutes les entités en tant que superadmin
- Ajout du filtre sur l'entité sur la visualisation de toutes les entités en tant que superadmin
- Ajout de nouveaux droits
- Ajout d'une nouvelle page d'accueil pour les utilisateurs qui n'ont pas d'action sur les traitements.
- Ajout d'un modèle de "Presentation", afin de permettre la génération de registre avec une page de sommaire.

### Evolutions
- L'utilisateur ne peut plus changer son identifiant.
- Un utilisateur provenant d'un LDAP ne peut pas changer son mot de passe.
- Séparation du changement de mot de passe de la page de "Préférences".
- Modification de l'emplacement du dossier cible pour l'enregistrement des documents déposé dans l'application afin de 
faciliter les backups.
- Lors du login, si l'utilisateur a été créé à la main sur l'application, on n'interroge pas les LDAPS potentiellement
connecté.
- Si DPO non défini interdire la création d'un traitement
- Formatage des nom de variable lors de la création ou modification d'un formulaire. (suppression accents, espace, caratère spéciaux)
- Lors de la la modification d'un formulaire, il n'est plus possible de modifier le nom de variable d'un champ déjà existant.
- Nouvelle page de connexion LS
- Lors de la création d'une entité la tache automatique sur la "conversion des annexes" est automatiquement configuré et activé.
- Trie du tableau de modèle de formulaire par nom

### Corrections
- Génération du registre au format .CSV
- Correction sur l'affichage du rédacteur lorsqu'un utilisateur non rédacteur consulte le traitement.
- Correction de la modification des valeurs dans un menu déroulant lors de la modification d'un formulaire.
- Correction de l'affectation d'un sous-traitant à une entité
- Correction de l'affectation d'un co-responsable à une entité
- Correction du fichier webdpo.inc.default sur l'url de Gedooo
- Affichage du formulaire
- Affichage du traitement
- Lors de la déclaration d'un traitement les services sont ceux de l'utilisateur connecté dans l'entité actuelle.
- Bug lors de la définition du DPO. Mise en place d'un profil DPO, ou on peut ne modifier que certains droits
- Le bouton "entrée" n'a plus d'action dans les formulaires
- Correction de l'enregistrement d'une norme lorsque le fichier de la norme n'est pas au format PDF.
- Restriction des actions en fonction des droits utilisateur
- Correction de l'insertion d'un traitement au registre. 
- Suppression des espaces au début et à la fin des options lors de la création des champs dans le formulaire
- Lors de la modification de l'entité (information sur l'entité et / ou le DPO) les informations de l'entité dans les traitements sont modifier. Sauf les traitements au registre

### Suppressions
- Suppression de l'utilisation du système de verrouillage d'un traitement au registre.
- Suppression de l'affichage des variables du co-responsable dans les variables pour l'extrait de registre.
- Suppresion de la visualisation d'un profil.

[1.0.1] - 13-12-2018
=====

### Ajouts
- Mise en place d'une page de vérification de l'installation de l'application en tant que superadmin.
- Mise en place d'une tâche automatique pour la conversion des annexes PDF pour permettre la génération.

### Evolutions
- Modification de la création d'un formulaire pour qu'il ressemble le plus possible à la déclaration d'un traitement.
- Possibilité de définir le préfix d'enregistrement du numéro du traitement au registre
- Lors du changement de collectivité par un utilisateur qui a plus de 5 collectivités, une modal ("pop-up") a été mise 
en place pour faciliter l'affichage et le changement.

### Corrections
- Duplication d'un formulaire dans une autre entité.
- Filtre sur le type de formulaire utilisé au registre.
- Génération du registre avec les annexes.
- Modification du nom des varibles dans l'aide pour crée les modèles.

[1.0.0] - 03-07-2018
=====

### Ajouts
- Fork du projet web-CIL
- Mise en place de la notion de sous-traitance
- Mise en place de la notion de co-responsabilité
- Export du registre en csv
- Choix de notification par email en fonction de chaque utilisateur.
- Ajout connecteur LDAP + tâche automatique pour la récupération des informations LDAP

### Evolutions
- Gestion des entitées en tant que super-administrateur 

### Corrections
- Remplacement de la notion de CIL par DPO
- Affichage de la force du mot de passe attendu dans l'entité à la création d'un utilisateur 
