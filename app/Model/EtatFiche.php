<?php

/**
 * EtatFiche
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class EtatFiche extends AppModel {

    public $name = 'EtatFiche';

    const ENCOURS_REDACTION = 1; // En cours de rédaction
    const ENCOURS_VALIDATION = 2; // En cours de validation
    const VALIDER = 3; // Validée
    const REFUSER = 4; // Refusée
    const VALIDER_DPO = 5; // Validée par le DPO
    const DEMANDE_AVIS = 6; // Demande d avis
    const ARCHIVER = 7; // Archivée
    const REPLACER_REDACTION = 8; // Replacer en rédaction
    const MODIFICATION_TRAITEMENT_REGISTRE = 9; // Modification du traitement inséré au registre
    const REPONSE_AVIS = 10; // Réponse à la demande d avis
    const INITIALISATION_TRAITEMENT = 11; // Initialisation du traitement par le DPO
    const TRAITEMENT_INITIALISE_RECU = 12; // Traitement initialisé reçu par le rédacteur
    const TRAITEMENT_INITIALISE_REDIGER = 13; // Traitement initialisé rédiger
    const TRAITEMENT_SEND_USER_SERVICE_DECLARANT = 14; // Traitement initialisé rédiger

    const LISTE_ETAT_TRAITEMENT_REGISTRE = [
        EtatFiche::VALIDER_DPO,
        EtatFiche::ARCHIVER,
        EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE
    ];

    /**
     * belongsTo associations
     *
     * @var array
     * 
     * @access public
     * @created 13/04/2015
     * @version V1.0.0
     */
    public $belongsTo = [
        'Etat' => [
            'className' => 'Etat',
            'foreignKey' => 'etat_id',
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'PreviousUser' => [
            'className' => 'User',
            'foreignKey' => 'previous_user_id'
        ]
    ];

    /**
     * hasOne associations
     *
     * @var array
     * 
     * @access public
     * @created 13/04/2015
     * @version V1.0.0
     */
    public $hasMany = [
        'Commentaire' => [
            'className' => 'Commentaire',
            'foreignKey' => 'etat_fiches_id',
            'dependent' => true
        ]
    ];
    
    /**
     *
     * @var type
     * 
     * @access public
     * @created 11/01/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public $hasOne = [
        'Modification' => [
            'className' => 'Modification',
            'foreignKey' => 'etat_fiches_id',
            'dependent' => true
        ]
    ];

}
