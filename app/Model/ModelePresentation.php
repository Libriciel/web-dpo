<?php

/**
 * ModelePresentation
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'modelePresentation');

class ModelePresentation extends AppModel {

    public $name = 'modelePresentation';

    /**
     * belongsTo associations
     * 
     * @var array
     * 
     * @access public
     * @created 18/06/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisations_id'
        ]
    ];

    public function saveFileModelePresentation($file, $organisation_id)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            ['application/vnd.oasis.opendocument.text'],
            '.odt',
            CHEMIN_MODELES_PRESENTATIONS
        );

        if ($infoFile['success'] == true) {
            $this->deleteAll([
                'organisations_id' => $organisation_id
            ]);

            $this->create([
                'fichier' => $infoFile['nameFile'],
                'organisations_id' => $organisation_id,
                'name_modele' => $infoFile['name']
            ]);
            $success = $this->save(null, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('default','default.flasherrorEnregistrementErreur'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }

}
