<?php

/**
 * ConnecteurLdap
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class ConnecteurLdap extends AppModel {

    public $name = 'ConnecteurLdap';
    
    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 26/03/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];

    /**
     * @param array $file
     * @param int $connecteur_id
     * @return array
     * @throws Exception
     *
     * @access public
     *
     * @created 25/11/2020
     * @version V2.1.0
     */
    public function saveFileCertificat($file, $connecteur_id)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            [
                'application/x-x509-ca-cert',
                'application/pkix-cert',
                'text/plain'
            ],
            '.crt',
            CHEMIN_CERTIFICATS
        );

        if ($infoFile['success'] == true) {
            $this->id = $connecteur_id;
            $record = [
                'certificat_url' => $infoFile['nameFile'],
                'certificat_name' => $infoFile['name']
            ];
            $success = $this->save($record, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('connecteur_ldap','connecteur_ldap.flasherrorErreurEnregistrementConnecteur'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }

}
