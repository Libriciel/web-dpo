<?php

/**
 * Article
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Article extends AppModel {

    public $name = 'Article';
    
    public $validationDomain = 'validation';

    /**
     * Règles de validation supplémentaires.
     *
     * @var array
     */
    public $validate = [
        'name' => [
            'isUniqueMultiple' => [
                'rule' => 'isUnique',
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ]
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 11/04/2018
     * @version v1.0.0
     */
    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'articles_organisations',
            'foreignKey' => 'article_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'ArticleOrganisation'
        ]
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 18/07/2019
     * @version V1.0.2
     */
    public $belongsTo = [
        'Createdbyorganisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'createdbyorganisation_id'
        ]
    ];
}
