<?php

/**
 * Service
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Service extends AppModel implements LinkedOrganisationInterface{

    public $name = 'Service';

    public $validationDomain = 'validation';
    
    public $displayField = 'libelle';

    /**
     * Règles de validation supplémentaires.
     *
     * @var array
     */
    public $validate = [
        'libelle' => [
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['libelle', 'organisation_id']],
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ],
        'organisation_id' => [
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['libelle', 'organisation_id']],
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ]
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 18/06/2015
     * @modified 04/11/2016
     * @version V0.9.0
     */
    public $hasMany = [
        'OrganisationUser' => [
            'className' => 'OrganisationUser',
            'foreignKey' => 'service_id',
            'dependent' => false
        ],
        'Formulaire' => [
            'className' => 'Formulaire',
            'foreignKey' => 'service_id',
            'dependent' => false
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'service_id',
            'dependent' => false
        ],
        'OrganisationUserService' => [
            'className' => 'OrganisationUserService',
            'foreignKey' => 'service_id',
            'dependent' => false
        ]
    ];

    /**
     * Retourne l'id de l'organisation liée au service (dans un array).
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ["{$this->alias}.{$this->primaryKey}" => $id],
        ];
        return (array)Hash::get($this->find('first', $query), "{$this->alias}.organisation_id");
    }

    /**
     * Retourne un champ virtuel contenant le nombre d'utilisateurs liés au service.
     *
     * @param string $roleIdField | 'Service.id' --> Champ représentant le Service.id
     * @param string $fieldName | 'users_count' --> Nom du champ virtuel
     * @return string
     */
    public function vfLinkedUsersCount($primaryKeyField = 'Service.id', $fieldName = 'users_count') {
        $query = [
            'alias' => 'organisation_user_services',
            'fields' => ['COUNT(organisation_users.user_id)'],
            'joins' => [
                words_replace(
                    $this->OrganisationUserService->join('OrganisationUser', ['type' => 'INNER']),
                    ['OrganisationUser' => 'organisation_users', 'OrganisationUserService' => 'organisation_user_services']
                )
            ],
            'conditions' => [
                "organisation_user_services.service_id = {$primaryKeyField}"
            ],
            'contain' => false
        ];
        $sql = $this->OrganisationUserService->sql($query);

        return "( {$sql} ) AS \"{$this->alias}__{$fieldName}\"";
    }

    public function vfLinkedFichesCount($primaryKeyField = 'Service.id', $fieldName = 'fiches_count') {
        $query = [
            'alias' => 'fiche',
            'fields' => ['COUNT(fiche.service_id)'],
            'conditions' => [
                "fiche.service_id = {$primaryKeyField}"
            ],
            'contain' => false
        ];
        $sql = $this->Fiche->sql($query);

        return "( {$sql} ) AS \"{$this->alias}__{$fieldName}\"";
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        $query = [
            'alias' => 'organisation_user_services',
            'fields' => ['organisation_user_services.service_id'],
            'joins' => [
                words_replace(
                    $this->OrganisationUserService->join('OrganisationUser', ['type' => 'INNER']),
                    ['OrganisationUser' => 'organisation_users', 'OrganisationUserService' => 'organisation_user_services']
                )
            ],
            'conditions' => [
                "organisation_users.organisation_id" => $organisation_id
            ],
            'contain' => false
        ];
        $sql = $this->OrganisationUserService->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
