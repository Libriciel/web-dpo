<?php

/**
 * Soustraitance
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Soustraitance extends AppModel {

    public $name = 'Soustraitance';
    
    public $validationDomain = 'validation';
    
    public $actsAs = [
        'Database.DatabaseFormattable' => [
            'Database.DatabaseDefaultFormatter' => [
                'formatTrim' => [
                    'NOT' => ['binary']
                ],
                'formatNull' => true,
                'formatNumeric' => [
                    'float',
                    'integer'
                ],
                'formatSuffix'  => '/_id$/',
                'formatStripNotAlnum' => '/^(telephone|fax|siret)$/'
            ]
        ],
    ];
    
    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public $validate = [
        'fiche_id' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'soustraitant_id' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'raisonsociale' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'siret' => [
            'luhn' => [
                'rule' => [
                    'luhn',
                    true
                ],
                'message' => 'validation.numeroSIRETNonValide'
            ],
            [
                'rule' => 'numeric', 
                'message' => 'validation.numeroSIRETUniquementComposeChiffre'
            ],
        ],
        'ape' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 18/06/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ],
        'Soustraitant' => [
            'className' => 'Soustraitant',
            'foreignKey' => 'soustraitant_id'
        ],
    ];
}
