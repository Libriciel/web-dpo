<?php

/**
 * OrganisationUser
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class OrganisationUser extends AppModel {

    public $name = 'OrganisationUser';
    
    public $validationDomain = 'validation';

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 29/04/2015
     * @version V0.9.0
     */
    public $validate = [
        'organisation_id' => [
            [
                'rule' => 'notBlank',
                'message' => 'validation.selectionnerOrganisation'
            ]
        ]
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 13/04/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 13/04/2015
     * @version V0.9.0
     */
    public $hasMany = [
        'Droit' => [
            'className' => 'Droit',
            'foreignKey' => 'organisation_user_id',
            'dependent' => true
        ],
        'OrganisationUserRole' => [
            'className' => 'OrganisationUserRole',
            'foreignKey' => 'organisation_user_id',
            'dependent' => true
        ],
        'OrganisationUserService' => [
            'className' => 'OrganisationUserService',
            'foreignKey' => 'organisation_user_id'
        ]
    ];

    /**
     * hasOne associations
     *
     * @var array
     *
     * @access public
     * @created 26/06/2015
     * @version V0.9.0
     */
    public $hasOne = [
        'OrganisationUserService' => [
            'className' => 'OrganisationUserService',
            'foreignKey' => 'organisation_user_id',
            'dependent' => true
        ]
    ];

}
