<?php

/**
 * Organisation
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Organisation extends AppModel {

    public $name = 'Organisation';
    
    public $validationDomain = 'validation';

    public $displayField = 'raisonsociale';

    public $actsAs = [
        'Database.DatabaseFormattable' => [
            'Database.DatabaseDefaultFormatter' => [
                'formatTrim' => ['NOT' => ['binary']],
                'formatNull' => true,
                'formatNumeric' => ['float', 'integer'],
                'formatSuffix'  => '/_id$/',
                'formatStripNotAlnum' => '/^(telephone|siret|fax|telephoneresponsable)$/'
            ]
        ],
        'LettercaseFormattable' => [
            'upper_noaccents' => ['nomresponsable'],
            'title' => ['prenomresponsable']
        ]
    ];

    /**
     * Champs virtuels du modèle
     *
     * @var array
     */
    public $virtualFields = [
        'responsable_nom_complet' => '( COALESCE( "Organisation"."civiliteresponsable", \'\' ) || \' \' || COALESCE( "Organisation"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Organisation"."nomresponsable", \'\' ) )',
        'responsable_nom_complet_court' => '( COALESCE( "Organisation"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Organisation"."nomresponsable", \'\' ) )'
    ];

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     */
    public $validate = [
        'logo_file' => [
            'upload' => [
                'rule' => [
                    'extension',
                        [
                        'jpg',
                        'jpeg',
                        'png',
                        'gif',
                        ''
                    ]
                ],
                'allowEmpty' => true,
                'message' => 'validation.envoyerFichiersRestreintImage'
            ]
        ],
        'model_file' => [
            'upload' => [
                'rule' => [
                    'extension',
                        [
                        'odt',
                        ''
                    ]
                ],
                'allowEmpty' => true,
                'message' => 'validation.envoyerFichiersRestreintODT'
            ]
        ],
        'siret' => [
            'luhn' => [
                'rule' => [
                    'luhn',
                    true
                ],
                'message' => 'validation.numeroSIRETNonValide'
            ],
            [
                'rule' => 'numeric', 
                'message' => 'validation.numeroSIRETUniquementComposeChiffre'
            ],
        ],
        'email' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
        'emailresponsable' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
        'numerodpo' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'allowEmpty' => false,
                'on' => 'update'
            ]
        ],
        'dpo' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'allowEmpty' => false,
                'on' => 'update'
            ]
        ],
        'emaildpo' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'allowEmpty' => false,
                'on' => 'update'
            ],
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
    ];

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     */
    public $hasAndBelongsToMany = [
        'User' => [
            'className' => 'User',
            'joinTable' => 'organisations_users',
            'foreignKey' => 'organisation_id',
            'associationForeignKey' => 'user_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'OrganisationUser'
        ],
        'Soustraitant' => [
            'className' => 'Soustraitant',
            'joinTable' => 'soustraitants_organisations',
            'foreignKey' => 'organisation_id',
            'associationForeignKey' => 'soustraitant_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'SoustraitantOrganisation'
        ],
        'Responsable' => [
            'className' => 'Responsable',
            'joinTable' => 'responsables_organisations',
            'foreignKey' => 'organisation_id',
            'associationForeignKey' => 'responsable_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'ResponsableOrganisation'
        ],
        'Article' => [
            'className' => 'Article',
            'joinTable' => 'articles_organisations',
            'foreignKey' => 'organisation_id',
            'associationForeignKey' => 'article_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'ArticleOrganisation'
        ],
        'Typage' => [
            'className' => 'Typage',
            'joinTable' => 'typages_organisations',
            'foreignKey' => 'organisation_id',
            'associationForeignKey' => 'typage_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'TypageOrganisation'
        ],
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     *
     * @edit 18/07/2019
     * @version V1.0.2
     */
    public $hasMany = [
        'Createdarticle' => [
            'className' => 'Article',
            'foreignKey' => 'createdbyorganisation_id',
            'dependent' => true
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
        'Role' => [
            'className' => 'Role',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
        'Service' => [
            'className' => 'Service',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
        'Cron' => [
            'className' => 'Cron',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
        'FormulaireOrganisation' => [
            'className' => 'FormulaireOrganisation',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
        'FormulaireModeleOrganisation' => [
            'className' => 'FormulaireModeleOrganisation',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
    ];

    public $hasOne = [
        'ConnecteurLdap' => [
            'className' => 'ConnecteurLdap',
            'foreignKey' => 'organisation_id'
        ],
        'Authentification' => [
            'className' => 'Authentification',
            'foreignKey' => 'organisation_id'
        ],
        'ModeleExtraitRegistreOrganisation' => [
            'className' => 'ModeleExtraitRegistreOrganisation',
            'foreignKey' => 'organisation_id',
            'dependent' => true
        ],
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Dpo' => [
            'className' => 'User',
            'foreignKey' => 'dpo'
        ]
    ];

    /**
     * @param type|true $cascade
     *
     * @access public
     * @created 17/06/2015
     * @version V0.9.0
     */
    public function beforeDelete($cascade = true) {
        $oldextension = $this->field('logo');
        $oldfile = IMAGES . 'logos' . DS . $this->id . '.' . $oldextension;
        if (file_exists($oldfile)) {
            unlink($oldfile);
        }
    }

    /**
     * @param array $data
     * @return bool|mixed
     * @throws Exception
     *
     * @access public
     *
     * @created 17/06/2015
     * @version V0.9.0
     *
     * @modified 07/04/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function saveAddEditForm($data) {
        $success = true;

        if (isset($data[$this->alias]['logo_file']['tmp_name'])) {
            $logo = $data[$this->alias]['logo_file'];
        }
        unset($data[$this->alias]['logo_file']);

        // En cas de modification, on récupère les valeurs qui ne sont pas renvoyées
        $id = Hash::get($data, "{$this->alias}.{$this->primaryKey}");
        if (empty($id) === false) {
            $record = $this->find(
                'first',
                [
                    'fields' => array_diff(array_keys($this->schema()), [
                        'id',
                        'numerodpo',
                        'dpo',
                        'emaildpo',
                        'created',
                        'modified'
                    ]),
                    'conditions' => [
                        'id' => $id
                    ],
                    'contain' => false
                ]
            );
            $data[$this->alias] = array_merge($record[$this->alias], $data[$this->alias]);
        }
        
        $this->create($data);
        $success = false !== $this->save(null, ['atomic' => false]);

        if ($success === true) {
            if (isset($logo['tmp_name']) && !empty($logo['tmp_name'])) {
                $success = $this->saveFileLogo($logo);
            }
        }

        return $success;
    }

    /**
     * Sauvegarde du logo sur le disque
     *
     * @param array $logo
     * @return bool|mixed
     * @throws Exception
     *
     * @access public
     *
     * @created 17/06/2015
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function saveFileLogo($logo)
    {
        $mime = mime_content_type($logo['tmp_name']);
        $accepted = Configure::read('logoAcceptedTypes');

        if (in_array($mime, $accepted) === true) {
            // On verifie si le dossier file existe. Si c'est pas le cas on le cree
            $success = create_arborescence_files();

            if ($success === true) {
                $path = CHEMIN_LOGOS . $this->id . DS;
                if (file_exists($path) === false) {
                    $success = mkdir($path, 0777, true) && $success;
                }

                if (!empty($logo['tmp_name']) &&
                    file_exists($logo['tmp_name']) === true
                ) {
                    $uuid = time();
                    $minPathFile = $this->id . DS . $uuid;
                    $pathFile = $path . $uuid;
                    $oldfile = $this->field('logo');

                    $success = $success && move_uploaded_file($logo['tmp_name'], $pathFile);
                    if ($success === true) {
                        $record = [
                            'logo' => $minPathFile
                        ];
                        $success = false !== $this->save($record, ['atomic' => false]);

                        if ($success === true && file_exists(CHEMIN_LOGOS.$oldfile)) {
                            unlink(CHEMIN_LOGOS.$oldfile);
                        }
                    }
                }
            }
        } else {
            $success = false;
        }

        return $success;
    }

}
