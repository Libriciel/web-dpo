<?php

/**
 * Notification
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Notification extends AppModel {

    public $name = 'Notification';

    const DEMANDE_AVIS = 1; // Demande d'avis
    const VALIDATION_DEMANDEE = 2; // Validation demandée
    const TRAITEMENT_VALIDE = 3; // Traitement validé
    const TRAITEMENT_REFUSE = 4; // Traitement refusé
    const COMMENTAIRE_AJOUTE = 5; // Commentaire ajouté sur le traitement
    const TRAITEMENT_INITIALISE = 6; // Traitement initialisé par le DPO


    /**
     * belongsTo associations
     *
     * @var array
     * 
     * @access public
     * @created 13/04/2015
     * @version V1.0.0
     */
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ]
    ];

}
