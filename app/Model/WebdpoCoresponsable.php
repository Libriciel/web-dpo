<?php

/**
 * WebdpoCoresponsable
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class WebdpoCoresponsable extends AppModel
{

    public $name = 'WebdpoCoresponsable';

    public $useTable = false;

    protected $_schema = [];

    public function coresponsableSave ($fiche_id, $dataCoresponsable, $useCoresponsable)
    {
        $success = true;
        $Coresponsable = ClassRegistry::init('Coresponsable');

        if (empty($dataCoresponsable['currentCoresponsable'])
            && empty($dataCoresponsable['coresponsables'])
            && $useCoresponsable == true
        ) {
            $Coresponsable->invalidate('coresponsables', 'Champ obligatoire');
            return false;
        }

        if (empty($fiche_id)) {
            return false;
        }

        if ($useCoresponsable == false) {
            $success = $Coresponsable->deleteAll([
                    'Coresponsable.fiche_id' => $fiche_id
                ]) && $success;

            return true;
        }

        if (isset($dataCoresponsable['currentCoresponsable']) && !empty($dataCoresponsable['currentCoresponsable']) && !empty($dataCoresponsable['coresponsables'])) {
            foreach ($dataCoresponsable['currentCoresponsable'] as $currentCoresponsable) {
                if (($key = array_search($currentCoresponsable['responsable_id'], $dataCoresponsable['coresponsables'])) !== false) {
                    unset($dataCoresponsable['coresponsables'][$key]);
                }
            }

            $noDeleteCoresponsable = Hash::extract($dataCoresponsable['currentCoresponsable'], '{n}.coresponsable_id');
            $success = $Coresponsable->deleteAll([
                    'Coresponsable.id !=' => $noDeleteCoresponsable,
                    'Coresponsable.fiche_id' => $fiche_id
                ]) && $success;
        } else {
            $success = $Coresponsable->deleteAll([
                    'Coresponsable.fiche_id' => $fiche_id
                ]) && $success;
        }

        if ($success === false) {
            return $success;
        }

        if (!empty($dataCoresponsable['coresponsables'])) {
            $Responsable = ClassRegistry::init('Responsable');

            foreach ($dataCoresponsable['coresponsables'] as $coresponsable_id) {
                $responsable = $Responsable->find('first', [
                    'conditions' => [
                        'id' => $coresponsable_id
                    ]
                ]);

                $Coresponsable->create([
                    'fiche_id' => $fiche_id,
                    'responsable_id' => $responsable['Responsable']['id'],
                ]);
                $success = false !== $Coresponsable->save(null, ['atomic' => false]) && $success;
            }
        }

        return $success;
    }

    public function cleanAndCheckDataCoresponsable($data, $champs)
    {
        if ($data['Fiche']['coresponsable'] == false) {
            if (isset($data['WebdpoFiche']['coresponsabilitefields'])) {
                unset($data['WebdpoFiche']['coresponsabilitefields']);
            }

            if (isset($data['Coresponsable'])) {
                unset($data['Coresponsable']);
            }

            if (isset($data[$this->alias])) {
                unset($data[$this->alias]);
            }

            return $data;
        }

        $arrayCoresponsableFields = [];
        foreach ($champs as $champ) {
            if ($champ['Champ']['champ_coresponsable'] === true && $champ['Champ']['champ_soustraitant'] === false) {
                $arrayCoresponsableFields[] = $champ;
            }
        }

        // On vérifie que les champs suplémentaite du formualaire ne soient pas obligatoire
        if (isset($data[$this->alias]) && !empty($data[$this->alias])) {
            $success = true;

            foreach ($data[$this->alias] as $responsable_id => $coresponsabilitefield) {
                foreach ($arrayCoresponsableFields as $arrayCoresponsableField) {
                    $details = json_decode($arrayCoresponsableField['Champ']['details'], true);
                    if ($details['obligatoire'] === true) {
                        if (in_array($arrayCoresponsableField['Champ']['type'], ['checkboxes', 'multi-select'])) {
                            if (empty($coresponsabilitefield[$details['name']])) {
                                $this->validate[$details['name']]['multiple'] = ['rule' => ['multiple', ['min' => 1]]];
                                $success = false;
                            }
                        } else {
                            if (empty($coresponsabilitefield[$details['name']])) {
                                $this->validate[$details['name']]['notBlank'] = ['rule' => ['notBlank']];
                                $success = false;
                            }
                        }

                        $this->saveAll($data[$this->alias], ['atomic' => false, 'validate' => 'only']);
                    }
                }
            }

            if ($success === false) {
                return $data;
            }

            $data['WebdpoFiche']['coresponsabilitefields'] = json_encode($data[$this->alias], true);
        }

        return $data;
    }
}