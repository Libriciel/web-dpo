<?php

/**
 * ModeleExtraitRegistre
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class ModeleExtraitRegistre extends AppModel {

    public $name = 'ModeleExtraitRegistre';

    public $hasMany = [
        'ModeleExtraitRegistreOrganisation' => [
            'className' => 'ModeleExtraitRegistreOrganisation',
            'foreignKey' => 'modele_extrait_registre_id',
            'dependent' => true
        ]
    ];

    /**
     * @param $file
     * @return array
     * @throws Exception
     *
     * @access public
     *
     * @created 25/11/2020
     * @version V2.1.0
     */
    public function saveFileModeleExtraitRegistre($file)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            ['application/vnd.oasis.opendocument.text'],
            '.odt',
            CHEMIN_MODELES_EXTRAIT
        );

        if ($infoFile['success'] == true) {
            $this->create([
                'fichier' => $infoFile['nameFile'],
                'name_modele' => $infoFile['name']
            ]);
            $success = $this->save(null, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('default','default.flasherrorModeleExtraitExiste'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }

    public function getModeleExtraitRegistreConditionOrganisation($organisation_id)
    {
        $query = [
            'alias' => 'modele_extrait_registres_organisations',
            'fields' => [
                'modele_extrait_registres_organisations.modele_extrait_registre_id'
            ],
            'conditions' => [
                'modele_extrait_registres_organisations.organisation_id' => $organisation_id,
            ]
        ];

        $sql = $this->ModeleExtraitRegistreOrganisation->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
