<?php

/**
 * FormulaireModeleOrganisation
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.1.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class FormulaireModeleOrganisation extends AppModel {

    public $name = 'FormulaireModeleOrganisation';
    
    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public $belongsTo = [
        'Formulaire' => [
            'className' => 'Formulaire',
            'foreignKey' => 'formulaire_id'
        ],
        'Modele' => [
            'className' => 'Modele',
            'foreignKey' => 'modele_id'
        ],
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];
    
}
