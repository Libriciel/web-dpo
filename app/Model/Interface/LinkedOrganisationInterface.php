<?php

/**
 * LinkedOrganisationInterface
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model.Interface
 */

interface LinkedOrganisationInterface
{
    /**
     * Retourne les id des entités liées à l'enregistrement.
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id);

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id);
}
