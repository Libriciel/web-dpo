<?php

/**
 * FieldAsFilterOptionBehavior
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model.Behavior
 */

App::uses('LinkedOrganisationInterface', 'Model/Interface');

class FieldAsFilterOptionBehavior extends ModelBehavior
{
    /**
     * @param Model $model
     * @param array $config
     */
    public function setup( Model $model, $config = [] )
    {
        if (is_a($model, 'LinkedOrganisationInterface') === false) {
            $message = 'La classe %s doit implémenter LinkedOrganisationInterface pour pouvoir être utilisée';
            throw new RuntimeException(sprintf($message, get_class($model)));
        }
    }

    /**
     * Retourne une liste (en clé et en valeur) uniques et triées pour un champ particulier.
     *
     * @param string $fieldName
     * @param int $organisation_id
     * @return array
     */
    public function getStringOptionList(Model $model, $fieldName, $organisation_id) {
        $query = [
            'fields' => [
                "{$model->alias}.{$fieldName}",
                "{$model->alias}.{$fieldName}",
            ],
            'conditions' => [],
            'group' => [
                "{$model->alias}.{$fieldName}",
            ],
            'order' => ["{$model->alias}.{$fieldName} ASC"]
        ];

        if ($organisation_id !== null) {
            $query['conditions'][] = $model->getConditionOrganisation($organisation_id);
        }

        return $model->find('list', $query);
    }
}

