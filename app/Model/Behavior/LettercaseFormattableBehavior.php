<?php

/**
 * LettercaseFormattableBehavior
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model.Behavior
 */

class LettercaseFormattableBehavior extends ModelBehavior
{
    public $settings = [];

    /**
     * @param Model $model
     * @param array $config
     *
     * @author Christian BUFFIN <chrisitian.buffin@libriciel.coop>
     * @edit 14/03/2019
     * @version V1.0.2
     */
    public function setup( Model $model, $config = [] )
    {
        $defaults = [
            'lower' => [],
            'lower_noaccents' => [],
            'title' => [],
            'title_noaccents' => [],
            'upper' => [],
            'upper_noaccents' => []
        ];
        $this->settings[$model->alias] = $config + $defaults;
    }

    /**
     * Remplace les lettres contenant des signes diacritiques (accents, ...) par des
     * lettres sans signes et les ligatures et remplace les ligatures (ex. œ) par les
     * caractères la composant dans une chaine de caractères multibyte.
     *
     * @see {@link https://gist.github.com/devlucas/5703396}
     * @see {@link http://www.web1.pro/unicode.htm}
     *
     * @param string $str
     * @return string
     *
     * @author Christian BUFFIN <chrisitian.buffin@libriciel.coop>
     * @edit 14/03/2019
     * @version V1.0.2
     */
    protected static function _noAccents($str)
    {
        $str = htmlentities($str, ENT_COMPAT, 'UTF-8');
        $ligs = array(
            'from' => array('&aelig;', '&AElig;', '&oelig;', '&OElig;', '&szlig;'),
            'to' => array('ae', 'AE', 'oe', 'OE', 's')
        );
        $str = str_replace($ligs['from'], $ligs['to'], $str);
        $symbols = 'uml|acute|grave|circ|tilde|cedil|ring|th|slash|horn';
        $str = mb_ereg_replace('&([a-zA-Z])('.$symbols.');', '\1', $str);
        return html_entity_decode($str, ENT_COMPAT, 'UTF-8');
    }

    /**
     * @param $valeur
     * @return string
     */
    public static function formatageVariable($valeur)
    {
        // valeur sans accents
        $valeur = static::_noAccents($valeur);

        // valeur en minuscule
        $valeur = mb_convert_case($valeur, MB_CASE_LOWER);

        // supprime espace en début et fin de la chaine
        $valeur = trim($valeur);

        // remplace les espaces par _
        $valeur = str_replace(' ', '_', $valeur);

        // remplace les caractères spéciaux
        $valeur = preg_replace('#[^a-zA-Z 0-9_-]#', '', $valeur);

        return ($valeur);
    }

    /**
     * @param Model $model
     * @param array $options
     * @return mixed
     *
     * @author Christian BUFFIN <chrisitian.buffin@libriciel.coop>
     * @edit 14/03/2019
     * @version V1.0.2
     */
    public function beforeValidate(Model $model, $options = [])
    {
        $result = parent::beforeValidate($model, $options);

        foreach ($this->settings[$model->alias] as $convert => $fields) {
            $fields = (array)$fields;
            foreach ($fields as $fieldName) {
                $value = Hash::get($model->data, "{$model->alias}.{$fieldName}");
                if (empty($value) === false) {
                    $caseMode = null;

                    switch ($convert) {
                        // Minuscule
                        case 'lower':
                        case 'lower_noaccents':
                            $caseMode = MB_CASE_LOWER;
                            break;
                        // Première lettre en majuscule
                        case 'title':
                        case 'title_noaccents':
                            $caseMode = MB_CASE_TITLE;
                            break;
                        // Majuscule
                        case 'upper':
                        case 'upper_noaccents':
                            $caseMode = MB_CASE_UPPER;
                            break;
                    }

                    if ($caseMode !== null) {
                        if (strpos($convert, '_noaccents') !== false) {
                            $value = static::_noAccents($value);
                        }
                        $model->data[$model->alias][$fieldName] = mb_convert_case($value, $caseMode);
                    }
                }
            }
        }

        return $result;
    }
}
