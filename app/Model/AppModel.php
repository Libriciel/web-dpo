<?php

/**
 * AppModel
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('Model', 'Model');
App::uses('FrValidation', 'Validation');
App::uses('Validation', 'Utility');

class AppModel extends Model
{

    public $recursive = -1;

    /**
     * Behaviors utilisés par le modèle.
     *
     * ATTENTION: les behaviors utilisant le datasource PostgresPostgres sont
     * chargés dans le constructeur pour éviter des erreurs avec le datasource
     * LDAP.
     *
     * @var array
     */
    public $actsAs = [];

    /**
     * Cache "live", notamment utilisé par la méthode enums.
     *
     * @var array
     */
    protected $_appModelCache = [];

    /**
     *
     * @param bool|int|string|array $id Set this ID for this model on startup,
     * can also be an array of options, see above.
     * @param string $table Name of database table to use.
     * @param string $ds DataSource connection name.
     */
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        if (get_class($this->getDataSource()) === 'PostgresPostgres') {
            $behaviors = [
                'Containable',
                'Database.DatabaseTable',
                'Database.DatabaseFormattable',
                'Database.DatabaseAutovalidate',
                'Postgres.PostgresAutovalidate',
                'Postgres.PostgresExtraValidationRules'
            ];
            foreach (Hash::normalize($behaviors) as $behavior => $config) {
                $this->Behaviors->load($behavior, $config);
            }
        }
    }

    /**
     * Retourne la liste des options venant des champs possédant la règle de
     * validation inList.
     *
     * @return array
     */
    public function enums()
    {
        $cacheKey = $this->useDbConfig . '_' . __CLASS__ . '_enums_' . $this->alias;

        // Dans le cache "live" ?
        if (false === isset($this->_appModelCache[$cacheKey])) {
            $this->_appModelCache[$cacheKey] = Cache::read($cacheKey);

            // Dans le cache CakePHP ?
            if (false === $this->_appModelCache[$cacheKey]) {
                $this->_appModelCache[$cacheKey] = [];

                $domain = Inflector::underscore($this->alias);

                // D'autres champs avec la règle inList ?
                foreach ($this->validate as $field => $validate) {
                    foreach ($validate as $ruleName => $rule) {
                        if (($ruleName === 'inList') && !isset($this->_appModelCache[$cacheKey][$this->alias][$field])) {
                            $fieldNameUpper = strtoupper($field);

                            $tmp = $rule['rule'][1];
                            $list = array();

                            foreach ($tmp as $value) {
                                $list[$value] = __d($domain, "ENUM::{$fieldNameUpper}::{$value}");
                            }

                            $this->_appModelCache[$cacheKey][$this->alias][$field] = $list;
                        }
                    }
                }

                Cache::write($cacheKey, $this->_appModelCache[$cacheKey]);
            }
        }

        return (array)$this->_appModelCache[$cacheKey];
    }

    /**
     * Vérifie qu'un enregistrement soit bien unique avec les valeurs de plusieurs
     * colonnes (comme lorsdqu'une table possède un INDEX UNIQUE sur plusieurs
     * colonnes).
     *
     * @param array $data Les données du champ envoyées à la validation
     * @param array $fieldNames La liste des champs à contrôler
     * @return type
     * @deprecated
     *
     */
    public function isUniqueMultiple(array $data, array $fieldNames)
    {
        $query = [
            'fields' => ["{$this->alias}.{$this->primaryKey}"],
            'recursive' => -1,
            'conditions' => []
        ];

        foreach ($fieldNames as $fieldName) {
            $fieldName = "{$this->alias}.{$fieldName}";
            $query['conditions'][$fieldName] = Hash::get($this->data, $fieldName);
        }

        $primaryKey = Hash::get($this->data, "{$this->alias}.{$this->primaryKey}");
        $primaryKey = true === empty($primaryKey) ? $this->{$this->primaryKey} : $primaryKey;

        if (false === empty($primaryKey)) {
            $query['conditions']['NOT'] = ["{$this->alias}.{$this->primaryKey}" => $primaryKey];
        }

        return [] === $this->find('first', $query);
    }

    /**
     * "Surcharge" de la méthode Model::updateAll afin de retourner "true"
     * lorsqu'il n'existe pas d'enregistrement répondant aux conditions.
     *
     * @param array $fields
     * @param mixed $conditions
     * @return boolean
     */
    public function updateAllIfFound($fields, $conditions = true)
    {
        return (
                [] === $this->find('first', compact('conditions'))
                || parent::updateAll($fields, $conditions)
            ) !== false;
    }

    /**
     * Règle de validation équivalent à un index unique sur plusieurs colonnes.
     *
     * @param array $check
     * @param array $fields
     * @return boolean
     * @deprecated voir la méthode areUnique
     *
     * public $validate = array(
     * 'name' => array(
     *              array(
     *                      'rule' => array( 'checkUnique', array( 'name', 'modeletypecourrierpcg66_id' ) ),
     *                      'message' => 'Cet intitulé de pièce est déjà utilisé avec ce modèle de courrier.'
     *              )
     *      ),
     *      'modeletypecourrierpcg66_id' => array(
     *              array(
     *                      'rule' => array( 'checkUnique', array( 'name', 'modeletypecourrierpcg66_id' ) ),
     *                      'message' => 'Ce modèle de courrier est déjà utilisé avec cet intitulé de pièce.'
     *              ),
     *      )
     * );
     *
     */
    public function checkUnique($check, $fields)
    {
        if (!is_array($check) || empty($fields)) {
            return false;
        }

        $fields = (array)$fields;
        $available = array_keys($this->data[$this->alias]);

        // A°) On n'a pas tous les champs dans this->data
        if (array_intersect($fields, $available) !== $fields) {
            // TODO -> throw_error ou réfléchir ?
            return false;
        }

        // B°) Tous les champs sont dans this->data
        $querydata = ['conditions' => [], 'recursive' => -1, 'contain' => false];
        foreach ($fields as $field) {
            $querydata['conditions']["{$this->alias}.{$field}"] = $this->data[$this->alias][$field];
        }

        // 1°) Pas l'id -> SELECT COUNT(*) FROM table WHERE name = XXX and modeletypecourrierpcg66_id = XXXX == 0
        // 2°) On a l'id
        if (isset($this->data[$this->alias][$this->primaryKey]) && !empty($this->data[$this->alias][$this->primaryKey])) {
            // SELECT COUNT(*) FROM table WHERE name = XXX and modeletypecourrierpcg66_id = XXXX AND id <> XXXX == 0
            $querydata['conditions']["{$this->alias}.{$this->primaryKey} <>"] = $this->data[$this->alias][$this->primaryKey];
        }

        $found = $this->find('first', $querydata);
        return empty($found);
    }

    /**
     * Exemple: 'champ' => notEmptyIf( $check, 'reference', true, array( 'P' ) )
     *
     * @param array $checks
     * @param string $reference
     * @param boolean $condition
     * @param array $values
     * @return boolean
     */
    public function checkNotBlankIf($checks, $reference, $condition, $values)
    {
        if (!(is_array($checks) && !empty($reference))) {
            return false;
        }

        $success = true;
        $referenceValue = Hash::get($this->data, "{$this->alias}.{$reference}");

        if (!empty($checks)) {
            foreach ($checks as $value) {
                if (in_array($referenceValue, $values, true) === $condition) {
                    $success = Validation::notBlank($value) && $success;
                }
            }
        }

        return $success;
    }

    public function checkMultipleIf($checks, $reference, $condition, $values, $options = [], $caseInsensitive = false)
    {
        if (!(is_array($checks) && !empty($reference))) {
            return false;
        }

        $success = true;
        $referenceValue = Hash::get($this->data, "{$this->alias}.{$reference}");

        if (!empty($checks)) {
            foreach ($checks as $value) {
                if (in_array($referenceValue, $values, true) === $condition) {
                    $success = Validation::multiple($value, $options, $caseInsensitive) && $success;
                }
            }
        }

        return $success;
    }

    /**
     * Exemple: 'champ' => notEmptyIf( $check, 'reference', true, array( 'P' ) )
     *
     * @param array $checks
     * @param string $reference
     * @param boolean $condition
     * @param array $values
     * @return boolean
     */
    public function checkBlankIf($checks, $reference, $condition, $values)
    {
        if (!(is_array($checks) && !empty($reference))) {
            return false;
        }

        $success = true;
        $referenceValue = Hash::get($this->data, "{$this->alias}.{$reference}");

        if (!empty($checks)) {
            foreach ($checks as $value) {
                if (in_array($referenceValue, $values, true) === $condition) {
                    $success = Validation::blank($value) && $success;
                }
            }
        }

        return $success;
    }

    /**
     * Règle de validation équivalent à un index unique sur plusieurs colonnes.
     *
     * @fixme: il faudrait remplacer les méthodes `checkUnique` et `isUniqueMultiple`
     *
     * @param array $check
     * @param array $fields
     * @return boolean
     */
    public function areUnique($check, $fields)
    {
        if (!is_array($check) || empty($fields)) {
            return false;
        }

        $data = $this->data;

        //@todo rendre la vérif du numero generique en rapport avec le @fixme
        if (isset($data['Fiche']['numero']) === false) {
            return true;
        }

        $fields = (array)$fields;
        $available = array_keys($data[$this->alias]);

        $primaryKey = null;
        if (!empty($this->{$this->primaryKey})) {
            $primaryKey = $this->{$this->primaryKey};
        } elseif (isset($data[$this->alias][$this->primaryKey]) && !empty($data[$this->alias][$this->primaryKey])) {
            $primaryKey = $data[$this->alias][$this->primaryKey];
        }

        // S'il manque des champs pour un enregistrement existant, on va les récupérer
        if (!empty($primaryKey) && array_intersect($fields, $available) !== $fields) {
            $query = [
                'fields' => array_diff($fields, $available),
                'contain' => false,
                'conditions' => [
                    "{$this->alias}.{$this->primaryKey}" => $primaryKey
                ]
            ];
            $record = $this->find('first', $query);
            $data[$this->alias] = array_merge($record[$this->alias], $data[$this->alias]);
            $data[$this->alias][$this->primaryKey] = $primaryKey;
        }

        $querydata = array('conditions' => array(), 'recursive' => -1, 'contain' => false);
        foreach ($fields as $field) {
            $querydata['conditions']["{$this->alias}.{$field}"] = $data[$this->alias][$field];
        }

        // 1°) Pas l'id -> SELECT COUNT(*) FROM table WHERE name = XXX and modeletypecourrierpcg66_id = XXXX == 0
        // 2°) On a l'id
        if (isset($data[$this->alias][$this->primaryKey]) && !empty($data[$this->alias][$this->primaryKey])) {
            // SELECT COUNT(*) FROM table WHERE name = XXX and modeletypecourrierpcg66_id = XXXX AND id <> XXXX == 0
            $querydata['conditions']["{$this->alias}.{$this->primaryKey} <>"] = $data[$this->alias][$this->primaryKey];
        }

        $found = $this->find('first', $querydata);
        return empty($found);
    }

    /**
     * Retourne la sous-requête d'un des champs virtuels se trouvant dans
     * $this->virtualFields.
     *
     * @param string $field
     * @param string $alias
     * @return string
     * @throws RuntimeException
     */
    public function getVirtualFieldSql($field, $alias = true)
    {
        $virtualField = words_replace(Hash::get($this->virtualFields, $field), [$this->name => $this->alias]);

        if (empty($virtualField)) {
            $message = "Virtual field \"{$field}\" does not exist in model \"{$this->alias}\".";
            throw new RuntimeException($message, 500);
        }

        $sq = "( $virtualField )";
        if ($alias) {
            $sq = "{$sq} AS \"{$this->alias}__{$field}\"";
        }

        return $sq;
    }

    /**
     * @param $file
     * @param $typeFile
     * @param $extension
     * @param $pathSave
     * @return array
     */
    public function saveFileOnDisk($file, $typeFile, $extension, $pathSave)
    {
        $infoFile = [
            'success' => false,
            'nameFile' => null,
            'name' => null,
            'message' => null,
            'statutMessage' => null
        ];

        if (isset($file) &&
            !empty($file) &&
            $file['error'] == 0
        ) {
            $mime = mime_content_type($file['tmp_name']);

            if (in_array($mime, $typeFile) === true) {
                if (!empty($file['name'])) {
                    // On verifie si le dossier file existe. Si c'est pas le cas on le cree
                    $success = create_arborescence_files();

                    if ($success === true) {
                        if (!empty($file['tmp_name']) &&
                            file_exists($file['tmp_name']) === true
                        ) {
                            $nameFile = time() . $extension;

                            $success = move_uploaded_file($file['tmp_name'], $pathSave . $nameFile);

                            if ($success == true) {
                                $infoFile = [
                                    'success' => $success,
                                    'nameFile' => $nameFile,
                                    'name' => $file['name'],
                                ];
                            }
                        } else {
                            $infoFile['message'] = __d('default', 'default.flashRecuperationFichierImpossible');
                            $infoFile['statutMessage'] = 'flasherror';
                        }
                    } else {
                        $infoFile['message'] = __d('default', 'default.flashCreationArborescenceFiles');
                        $infoFile['statutMessage'] = 'flasherror';
                    }
                } else {
                    $infoFile['message'] = __d('default', 'default.flashNomFichierVide');
                    $infoFile['statutMessage'] = 'flasherror';
                }
            } else {
                $infoFile['message'] = __d('default', 'default.flashExtensionNonValide');
                $infoFile['statutMessage'] = 'flasherror';
            }
        } else {
            $infoFile['message'] = __d('default', 'default.flashAucunFichier');
            $infoFile['statutMessage'] = 'flashwarning';
        }

        return ($infoFile);
    }

}
