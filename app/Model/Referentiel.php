<?php

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Referentiel extends AppModel
{
    public $name = 'Referentiel';

    public $validationDomain = 'validation';

    public $validate = [
        'libelle' => [
            'isUniqueMultiple' => [
                'rule' => 'isUnique',
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ]
    ];

    public $hasMany = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ]
    ];

    /**
     * Enregistrement du fichier sur le disque et en BDD
     *
     * @param array $file
     * @param int $referentiel_id
     * @return array
     * @throws Exception
     *
     * @access public
     *
     * @created 21/10/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.1
     */
    public function saveFileReferentiel($file, $referentiel_id)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            ['application/pdf'],
            '.pdf',
            CHEMIN_NEW_REFERENTIELS
        );

        if ($infoFile['success'] == true) {
            $this->id = $referentiel_id;
            $record = [
                'fichier' => $infoFile['nameFile'],
                'name_fichier' => $infoFile['name']
            ];
            $success = $this->save($record, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('referentiel','referentiel.flasherrorErreurEnregistrementNorme'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }
}
