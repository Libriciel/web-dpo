<?php

/**
 * Norme
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Norme extends AppModel {

    public $name = 'Norme';
    
    public $validate = [
        'norme' => [
            'checkUnique' => [
                'rule' => ['checkUnique', ['norme', 'numero']]
            ]
        ],
        'numero' => [
            'inclusiveRange' => [
                'rule' => ['inclusiveRange', 0, 999]
            ],
            'checkUnique' => [
                'rule' => ['checkUnique', ['norme', 'numero']]
            ]
        ]
    ];
    
    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 07/12/2017
     * @version V1.0.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];
    
    public $hasMany = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ]
    ];
    
    public function beforeValidate($options = [])
    {
        $result = parent::beforeValidate($options);

        if(true === $result && true === isset($this->data[$this->alias]['numero'])) {
            $numero = Hash::get($this->data, "{$this->alias}.numero");
            $this->data[$this->alias]['numero'] = sprintf('%03d', ltrim($numero, 0));
        }

        return $result;
    }

    /**
     * Enregistrement du fichier sur le disque et en BDD
     *
     * @param array $file
     * @param int $norme_id
     * @return array
     * @throws Exception
     *
     * @access public
     *
     * @created 07/04/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function saveFileNorme($file, $norme_id)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            ['application/pdf'],
            '.pdf',
            CHEMIN_NEW_NORMES
        );

        if ($infoFile['success'] == true) {
            $this->id = $norme_id;
            $record = [
                'fichier' => $infoFile['nameFile'],
                'name_fichier' => $infoFile['name']
            ];
            $success = $this->save($record, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('norme','norme.flasherrorErreurEnregistrementNorme'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }

}
