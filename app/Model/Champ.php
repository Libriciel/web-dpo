<?php

/**
 * Champ
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Champ extends AppModel {

    public $name = 'Champ';

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 27/03/2020
     * @version V2.0.0
     */
    public $validate = [
        'formulaire_id' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'type' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'ligne' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'colonne' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'details' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ]
    ];

    /**
     * hasOne associations
     *
     * @var array
     * 
     * @author Théo GUILLON <theo.guillon@adullact-projet.coop>
     * @access public
     * @created 19/04/2018
     * @version V1.0.0
     */
    public $hasOne = [
        'Formulaire' => [
            'className' => 'Formulaire',
            'foreignKey' => 'id'
        ]
    ];
    
}
