<?php

/**
 * ListeDroit
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class ListeDroit extends AppModel {
    public $name = 'ListeDroit';

    public $displayField = 'libelle';

    const REDIGER_TRAITEMENT = 1; // Rédiger une fiche
    const VALIDER_TRAITEMENT = 2; // Valider une fiche
    const VISER_TRAITEMENT = 3; // Viser une fiche
    const CONSULTER_REGISTRE = 4; // Consulter le registre
    const INSERER_TRAITEMENT_REGISTRE = 5; // Insérer une fiche dans le registre
    const MODIFIER_TRAITEMENT_REGISTRE = 6; // modifier une fiche du registre
    const TELECHARGER_TRAITEMENT_REGISTRE = 7; // Télécharger une fiche du registre
    const CREER_UTILISATEUR = 8; // Créer un utilisateur
    const MODIFIER_UTILISATEUR = 9; // Modifier un utilisateur
    const SUPPRIMER_UTILISATEUR = 10; // Supprimer un utilisateur
    const CREER_ORGANISATION = 11; // Créer une organisation
    const MODIFIER_ORGANISATION = 12; // Modifier une organisation
    const CREER_PROFIL = 13; // Créer un profil
    const MODIFIER_PROFIL = 14; // Modifier un profil
    const SUPPRIMER_PROFIL = 15; // Supprimer un profil
    const CREER_SERVICE = 16; // Créer un service
    const MODIFIER_SERVICE = 17; // Modifier un service
    const SUPPRIMER_SERVICE  = 18; // Supprimer un service
    const CREER_NORME = 19; // Créer une norme
    const VISUALISER_NORME = 20; // Créer une norme
    const MODIFIER_NORME = 21; // Modifier une norme
    const ABROGER_NORME = 22; // Abroger une norme
    const GESTION_SOUSTRAITANT = 23; // Gestion des soustraitants
    const GESTION_CORESPONSABLE = 24; // Gestion des co-responsables
    const GESTION_MAINTENANCE = 25; // Gestion des connecteurs
    const GESTION_MODELE = 26; // Gestion des modèles
    const GESTION_FORMULAIRE = 27; // Gestion des formulaires
    const CREER_ARTICLE_FAQ = 28; // Créer un article dans la FAQ
    const MODIFIER_ARTICLE_FAQ = 29; // Modifier un article dans la FAQ
    const CONSULTER_ARTICLE_FAQ = 30; // Consulter un article dans la FAQ
    const SUPPRIMER_ARTICLE_FAQ = 31; // Supprimer un article dans la FAQ
    const GESTION_TYPAGE = 32; //Gestion des types d'annexes
    const CONSULTER_ALL_TRAITEMENT = 33; // Consultation de l'ensemble des traitements en cour dans l'entité
    const GESTION_CORESPONSABLE_TRAITEMENT = 34; // Gestion des co-responsables lors de la déclaration d'un traitement
    const GESTION_SOUSTRAITANT_TRAITEMENT = 35; // Gestion des sous-traitant lors de la déclaration d'un traitement
    const DUPLIQUER_TRAITEMENT = 36; // Dupliquer un traitement
    const INITIALISATION_TRAITEMENT = 37; // Initialisation d'un traitement
    const GENERER_TRAITEMENT_EN_COURS = 38; // Génération d'un traitement en cours de rédaction (hors registre)
    const MODIFIER_TRAITEMENT_CONSULTATION = 39; // Modifier un traitement lors de la reception en demande d'avis
    const CREER_REFERENTIEL = 40; // Créer un référentiel
    const VISUALISER_REFERENTIEL = 41; // Visualiser un référentiel
    const MODIFIER_REFERENTIEL = 42; // Modifier un référentiel
    const ABROGER_REFERENTIEL = 43; // Abroger un référentiel

    const LISTE_DROITS_ADMINISTRATEUR = [
        ListeDroit::REDIGER_TRAITEMENT,
        ListeDroit::VALIDER_TRAITEMENT,
        ListeDroit::VISER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE,
        ListeDroit::CREER_UTILISATEUR,
        ListeDroit::MODIFIER_UTILISATEUR,
        ListeDroit::SUPPRIMER_UTILISATEUR,
        ListeDroit::MODIFIER_ORGANISATION,
        ListeDroit::CREER_PROFIL,
        ListeDroit::MODIFIER_PROFIL,
        ListeDroit::SUPPRIMER_PROFIL,
        ListeDroit::CREER_SERVICE,
        ListeDroit::MODIFIER_SERVICE,
        ListeDroit::SUPPRIMER_SERVICE,
        ListeDroit::CREER_NORME,
        ListeDroit::MODIFIER_NORME,
        ListeDroit::ABROGER_NORME,
        ListeDroit::GESTION_SOUSTRAITANT,
        ListeDroit::GESTION_CORESPONSABLE,
        ListeDroit::GESTION_MAINTENANCE,
        ListeDroit::GESTION_MODELE,
        ListeDroit::GESTION_FORMULAIRE,
        ListeDroit::CREER_ARTICLE_FAQ,
        ListeDroit::MODIFIER_ARTICLE_FAQ,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::SUPPRIMER_ARTICLE_FAQ,
        ListeDroit::GESTION_TYPAGE,
        ListeDroit::CONSULTER_ALL_TRAITEMENT,
        ListeDroit::GESTION_CORESPONSABLE_TRAITEMENT,
        ListeDroit::GESTION_SOUSTRAITANT_TRAITEMENT,
        ListeDroit::DUPLIQUER_TRAITEMENT,
        ListeDroit::GENERER_TRAITEMENT_EN_COURS,
        ListeDroit::CREER_REFERENTIEL,
        ListeDroit::VISUALISER_REFERENTIEL,
        ListeDroit::MODIFIER_REFERENTIEL,
        ListeDroit::ABROGER_REFERENTIEL,
    ];

    const LISTE_DROITS_REDACTEUR = [
        ListeDroit::REDIGER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE,
        ListeDroit::VISUALISER_NORME,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::VISUALISER_REFERENTIEL
    ];

    const LISTE_DROITS_VALIDEUR = [
        ListeDroit::VALIDER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE,
        ListeDroit::VISUALISER_NORME,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::VISUALISER_REFERENTIEL
    ];

    const LISTE_DROITS_CONSULTANT = [
        ListeDroit::VISER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::VISUALISER_NORME,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::MODIFIER_TRAITEMENT_CONSULTATION,
        ListeDroit::VISUALISER_REFERENTIEL
    ];

    const LISTE_DROITS_DPO = [
        ListeDroit::REDIGER_TRAITEMENT,
        ListeDroit::VALIDER_TRAITEMENT,
        ListeDroit::VISER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::INSERER_TRAITEMENT_REGISTRE,
        ListeDroit::MODIFIER_TRAITEMENT_REGISTRE,
        ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE,
        ListeDroit::CREER_UTILISATEUR,
        ListeDroit::MODIFIER_UTILISATEUR,
        ListeDroit::SUPPRIMER_UTILISATEUR,
        ListeDroit::MODIFIER_ORGANISATION,
        ListeDroit::CREER_PROFIL,
        ListeDroit::MODIFIER_PROFIL,
        ListeDroit::SUPPRIMER_PROFIL,
        ListeDroit::CREER_SERVICE,
        ListeDroit::MODIFIER_SERVICE,
        ListeDroit::SUPPRIMER_SERVICE,
        ListeDroit::CREER_NORME,
        ListeDroit::MODIFIER_NORME,
        ListeDroit::ABROGER_NORME,
        ListeDroit::GESTION_SOUSTRAITANT,
        ListeDroit::GESTION_CORESPONSABLE,
        ListeDroit::GESTION_MAINTENANCE,
        ListeDroit::GESTION_MODELE,
        ListeDroit::GESTION_FORMULAIRE,
        ListeDroit::CREER_ARTICLE_FAQ,
        ListeDroit::MODIFIER_ARTICLE_FAQ,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::SUPPRIMER_ARTICLE_FAQ,
        ListeDroit::GESTION_TYPAGE,
        ListeDroit::CONSULTER_ALL_TRAITEMENT,
        ListeDroit::GESTION_CORESPONSABLE_TRAITEMENT,
        ListeDroit::GESTION_SOUSTRAITANT_TRAITEMENT,
        ListeDroit::DUPLIQUER_TRAITEMENT,
        ListeDroit::INITIALISATION_TRAITEMENT,
        ListeDroit::GENERER_TRAITEMENT_EN_COURS,
        ListeDroit::MODIFIER_TRAITEMENT_CONSULTATION,
        ListeDroit::CREER_REFERENTIEL,
        ListeDroit::VISUALISER_REFERENTIEL,
        ListeDroit::MODIFIER_REFERENTIEL,
        ListeDroit::ABROGER_REFERENTIEL,
    ];

    const LISTE_DROITS_DPO_MINIMUM = [
        ListeDroit::REDIGER_TRAITEMENT,
        ListeDroit::VALIDER_TRAITEMENT,
        ListeDroit::VISER_TRAITEMENT,
        ListeDroit::CONSULTER_REGISTRE,
        ListeDroit::INSERER_TRAITEMENT_REGISTRE,
        ListeDroit::MODIFIER_TRAITEMENT_REGISTRE,
        ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE,
        ListeDroit::CREER_NORME,
        ListeDroit::MODIFIER_NORME,
        ListeDroit::ABROGER_NORME,
        ListeDroit::GESTION_MODELE,
        ListeDroit::GESTION_FORMULAIRE,
        ListeDroit::CREER_ARTICLE_FAQ,
        ListeDroit::MODIFIER_ARTICLE_FAQ,
        ListeDroit::CONSULTER_ARTICLE_FAQ,
        ListeDroit::SUPPRIMER_ARTICLE_FAQ,
        ListeDroit::GESTION_TYPAGE,
        ListeDroit::CONSULTER_ALL_TRAITEMENT,
        ListeDroit::GESTION_CORESPONSABLE_TRAITEMENT,
        ListeDroit::GESTION_SOUSTRAITANT_TRAITEMENT,
        ListeDroit::DUPLIQUER_TRAITEMENT,
        ListeDroit::INITIALISATION_TRAITEMENT,
        ListeDroit::GENERER_TRAITEMENT_EN_COURS,
        ListeDroit::MODIFIER_TRAITEMENT_CONSULTATION,
        ListeDroit::CREER_REFERENTIEL,
        ListeDroit::VISUALISER_REFERENTIEL,
        ListeDroit::MODIFIER_REFERENTIEL,
        ListeDroit::ABROGER_REFERENTIEL,
    ];

    /**
     * hasMany associations
     *
     * @var array
     * 
     * @access public
     * @created 13/04/2015
     * @version V0.9.0
     */
    public $hasMany = [
        'Droit' => [
            'className' => 'Droit',
            'foreignKey' => 'liste_droit_id',
            'dependent' => true
        ]
    ];

    /**
     * $hasAndBelongsToMany associations
     *
     * @var array
     * 
     * @access public
     * @created 13/04/2015
     * @version V0.9.0
     */
    public $hasAndBelongsToMany = [
        'Role' => [
            'className' => 'Role',
            'joinTable' => 'role_droits',
            'foreignKey' => 'liste_droit_id',
            'associationForeignKey' => 'role_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'RoleDroit'
        ]
    ];
}
