<?php

/**
 * Responsable
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Responsable extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Responsable';
    
    public $validationDomain = 'validation';
    
    public $actsAs = [
        'Database.DatabaseFormattable' => [
            'Database.DatabaseDefaultFormatter' => [
                'formatTrim' => [
                    'NOT' => ['binary']
                ],
                'formatNull' => true,
                'formatNumeric' => [
                    'float',
                    'integer'
                ],
                'formatSuffix'  => '/_id$/',
                'formatStripNotAlnum' => '/^(telephoneresponsable|telephonestructure|faxstructure|siretstructure|telephonefixe_dpo|telephoneportable_dpo)$/'
            ]
        ],
        'FieldAsFilterOption',
        'LettercaseFormattable' => [
            'upper_noaccents' => ['nomresponsable', 'nom_dpo'],
            'title' => ['prenomresponsable', 'prenom_dpo']
        ]
    ];

    /**
     * Champs virtuels du modèle
     *
     * @var array
     */
    public $virtualFields = [
        'nom_complet' => '( COALESCE( "Responsable"."civiliteresponsable", \'\' ) || \' \' || COALESCE( "Responsable"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Responsable"."nomresponsable", \'\' ) )',
        'nom_complet_court' => '( COALESCE( "Responsable"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Responsable"."nomresponsable", \'\' ))',
        'nom_complet_dpo' => '( COALESCE( "Responsable"."civility_dpo", \'\' ) || \' \' || COALESCE( "Responsable"."prenom_dpo", \'\' ) || \' \' || COALESCE( "Responsable"."nom_dpo", \'\' ) )',
        'nom_complet_court_dpo' => '( COALESCE( "Responsable"."prenom_dpo", \'\' ) || \' \' || COALESCE( "Responsable"."nom_dpo", \'\' ))'
    ];

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public $validate = [
        'siretstructure' => [
            'luhn' => [
                'rule' => [
                    'luhn',
                    true
                ],
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETNonValide'
            ],
            [
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETUniquementComposeChiffre'
            ],
        ],
        'apestructure' => [
            'minLength' => [
                'rule' => ['minLength', 5],
                'allowEmpty' => true
            ],
            'maxLength' => [
                'rule' => ['maxLength', 5],
                'allowEmpty' => true
            ],
        ],
        'emailstructure' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ],
        'emailresponsable' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ],
        'email_dpo' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ]
    ];
    
    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 11/04/2018
     * @version v1.0.0
     */
    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'responsables_organisations',
            'foreignKey' => 'responsable_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'ResponsableOrganisation'
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'joinTable' => 'coresponsables',
            'foreignKey' => 'responsable_id',
            'associationForeignKey' => 'fiche_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'Coresponsable'
        ]
    ];

    /**
     * Retourne les id des organisations liées au responsable.
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ['responsable_id' => $id],
        ];
        return Hash::extract(
            $this->ResponsableOrganisation->find('all', $query),
            '{n}.ResponsableOrganisation.organisation_id'
        );
    }

    /**
     * Retourne une condition permettant de limiter aux sous-traitants liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        $query = [
            'alias' => 'responsables_organisations',
            'fields' => [
                'responsables_organisations.responsable_id'
            ],
            'conditions' => [
                'responsables_organisations.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->ResponsableOrganisation->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
