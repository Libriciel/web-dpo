<?php

/**
 * Modele
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Modele extends AppModel {

    public $name = 'Modele';

    public $belongsTo = [
        'Formulaire' => [
            'className' => 'Formulaire',
            'foreignKey' => 'formulaire_id'
        ]
    ];

    public $hasMany = [
        'FormulaireModeleOrganisation' => [
            'className' => 'FormulaireModeleOrganisation',
            'foreignKey' => 'modele_id',
            'dependent' => true
        ],
    ];

    public $validate = [
        'name_modele' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'fichier' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
    ];

    public function saveFileModele($file, $formulaire_id)
    {
        $infoFile = $this->saveFileOnDisk(
            $file,
            ['application/vnd.oasis.opendocument.text'],
            '.odt',
            CHEMIN_MODELES
        );

        if ($infoFile['success'] == true) {
            $this->create([
                'formulaire_id' => $formulaire_id,
                'fichier' => $infoFile['nameFile'],
                'name_modele' => $infoFile['name']
            ]);

            $success = $this->save(null, ['atomic' => false]);

            if ($success == false) {
                $infoFile = [
                    'success' => false,
                    'nameFile' => null,
                    'name' => null,
                    'message' => __d('default','default.flasherrorModeleExtraitExiste'),
                    'statutMessage' => 'flasherror'
                ];
            }
        }

        return $infoFile;
    }

}
