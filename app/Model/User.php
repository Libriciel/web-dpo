<?php

/**
 * User
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;
use Libriciel\Utility\Password\PasswordGeneratorAnssi;

class User extends AppModel implements LinkedOrganisationInterface {

    public $name = 'User';

    public $validationDomain = 'validation';

    public $actsAs = [
        'Database.DatabaseFormattable' => [
            'Database.DatabaseDefaultFormatter' => [
                'formatTrim' => [
                    'NOT' => ['binary']
                ],
                'formatNull' => true,
                'formatNumeric' => [
                    'float',
                    'integer'
                ],
                'formatSuffix'  => '/_id$/',
                'formatStripNotAlnum' => '/^(telephonefixe|telephoneportable)$/'
            ]
        ],
        'LettercaseFormattable' => [
            'upper_noaccents' => ['nom'],
            'title' => ['prenom']
        ]
    ];

    /**
     * Champs virtuels du modèle
     *
     * @var array
     */
    public $virtualFields = [
        'is_dpo' => '( EXISTS( SELECT * FROM "organisations" WHERE "organisations"."dpo" = "User"."id" ) )',
        'nom_complet' => '( COALESCE( "User"."civilite", \'\' ) || \' \' || COALESCE( "User"."prenom", \'\' ) || \' \' || COALESCE( "User"."nom", \'\' ) )',
        'nom_complet_court' => '( COALESCE( "User"."prenom", \'\' ) || \' \' || COALESCE( "User"."nom", \'\' ) )'
    ];

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 24/10/2015
     * @version V0.9.0
     */
    public $validate = [
        'password' => [
            'checkPasswordStrength' => [
                'allowEmpty' => true,
                'required' => false,
                'rule' => ['checkPasswordStrength'],
                'message' => false
            ]
        ],
        'passwd' => [
            [
                'rule' => 'notBlank',
                'message' => 'validation.confirmerPassword'
            ],
            [
                'rule' => [
                    'comparePassword',
                    'password'
                ],
                'message' => 'validation.lesMotsPasseNonIdentiques'
            ]
        ],
        'new_password' => [
            [
                'rule' => 'notBlank',
                'message' => 'validation.confirmerPassword'
            ],
            'checkPasswordStrength' => [
                'allowEmpty' => true,
                'required' => false,
                'rule' => ['checkPasswordStrength'],
                'message' => false
            ]
        ],
        'new_passwd' => [
            [
                'rule' => 'notBlank',
                'message' => 'validation.confirmerPassword'
            ],
            [
                'rule' => [
                    'comparePassword',
                    'new_password'
                ],
                'message' => 'validation.lesMotsPasseNonIdentiques'
            ]
        ],
        'old_password' => [
            [
                'rule' => 'notBlank',
                'message' => 'validation.confirmerPassword'
            ]
        ],
        'nom' => [
            [
                'rule' => ['custom', REGEXP_ALPHA_FR],
                'message' => 'validation.seulementLettresAcceptees'
            ]
        ],
        'prenom' => [
            [
                'rule' => ['custom', REGEXP_ALPHA_FR],
                'message' => 'validation.seulementLettresAcceptees'
            ]
        ],
        'email' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide'
            ]
        ]
    ];

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     *
     * @access public
     * @created 18/12/2015
     * @version V0.9.0
     */
    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'organisations_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'OrganisationUser'
        ]
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 26/03/2015
     * @version V0.9.0
     */
    public $hasMany = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'user_id',
            'dependent' => true,
        ],
        'EtatFiche' => [
            'className' => 'EtatFiche',
            'foreignKey' => 'user_id',
            'dependent' => true,
        ],
        'Previous' => [
            'className' => 'EtatFiche',
            'foreignKey' => 'previous_user_id'
        ],
        'Commentaire' => [
            'className' => 'Commentaire',
            'foreignKey' => 'user_id'
        ],
        'Destin' => [
            'className' => 'Commentaire',
            'foreignKey' => 'destinataire_id'
        ],
        'Dpo' => [
            'className' => 'Organisation',
            'foreignKey' => 'dpo'
        ],
        'Notification' => [
            'className' => 'Notification',
            'foreignKey' => 'user_id'
        ],
        'Role' => [
            'className' => 'Role',
            'foreignKey' => 'id'
        ],
        'OrganisationUserRole' => [
            'className' => 'OrganisationUserRole',
            'foreignKey' => 'id'
        ],
        'RoleDroit' => [
            'className' => 'RoleDroit',
            'foreignKey' => 'id'
        ],
        'Droit' => [
            'className' => 'Droit',
            'foreignKey' => 'id'
        ],
        'Organisationdpo' => [
            'className' => 'Organisation',
            'foreignKey' => 'dpo'
        ],
        'Jeton' => [
            'className' => 'Jeton',
            'foreignKey' => 'user_id'
        ]
    ];

    /**
     * hasOne associations
     *
     * @var array
     *
     * @access public
     * @created 26/06/2015
     * @version V0.9.0
     */
    public $hasOne = [
        'Admin' => [
            'className' => 'Admin',
            'foreignKey' => 'user_id',
            'dependent' => true
        ]
    ];

    /**
     * @param type $options
     * @return boolean
     *
     * @access public
     * @created 29/04/2015
     * @version V0.9.0
     */
    public function beforeSave($options = []) {
        if (false === isset($this->data[$this->alias]['password']) && true === isset($this->data[$this->alias]['new_password'])) {
            $this->data[$this->alias]['password'] = $this->data[$this->alias]['new_password'];
        }

        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);

            $password = $this->data[$this->alias]['password'];
            $this->data[$this->alias]['password'] = $passwordHasher->hash($password);
            $this->data[$this->alias]['password_force'] = PasswordStrengthMeterAnssi::strength($password);
        }
        return true;
    }

    /**
     * @param type $field
     * @param type|null $compareField
     * @return boolean
     *
     * @access public
     * @created 29/04/2015
     * @version V0.9.0
     */
    function comparePassword($field = [], $compareField = null) {
        foreach ($field as $key => $value) {
            $v1 = $value;
            $v2 = $this->data[$this->name][$compareField];

            if ($v1 != $v2) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    /**
     * Retourne la force de mot de passe minimale pour un utilisateur donné en
     * fonction de ce qui est configuré pour ses entités.
     *
     * Si on ne trouve pas d'entité (ex. superadmin au début), on retourne la
     * valeur maximale.
     *
     * @param int $id L'id de l'utilisateur
     * @return int
     */
    public function getMinUserPasswordStrength($id) {
        $query = [
            'fields' => ['MAX(Organisation.force) AS "Organisation__force"'],
            'joins' => [
                $this->join('OrganisationUser', ['type' => 'INNER']),
                $this->OrganisationUser->join('Organisation', ['type' => 'INNER'])
            ],
            'conditions' => [
                'User.id' => $id
            ]
        ];
        $result = (array)$this->find('first', $query);
        $force = Hash::get($result, 'Organisation.force');

        return null === $force ? 5 : $force;
    }

    /**
     * Retourne la force de mot de passe minimale pour un utilisateur donné en
     * fonction de ce qui est configuré pour ses entités.
     *
     * Si on ne trouve pas d'entité (ex. superadmin au début), on retourne la
     * valeur maximale.
     *
     * @param int $id L'id de l'utilisateur
     * @return int
     */
    public function getMaxForceOrganisationStrength(array $id = null) {
        $force = null;

        if ($id != null) {
            $result = $this->Organisation->find('first',[
                'conditions' => [
                    'id' => $id
                ],
                'fields' => [
                    'MAX(Organisation.force) AS "Organisation__force"'
                ],
            ]);
            $force = Hash::get($result, 'Organisation.force');
        }

        return null === $force ? 5 : $force;
    }

    // @fixme: supprimer les autres règles de validation sur 5 caractères
    public function checkPasswordStrength(array $check) {
        $organisations_ids = Hash::get($this->data, 'User.organisation_id');

        if (is_numeric($this->id) && true === empty($organisations_ids)){
            $force = $this->getMinUserPasswordStrength($this->id);
        } else {
            $organisations = Hash::get($this->data, 'User.organisation_id');
            if (empty($organisations)){
                $organisations = null;
            }

            $force = $this->getMaxForceOrganisationStrength($organisations);
        }

        $success = true;
        foreach($check as $field => $value) {
            if (PasswordStrengthMeterAnssi::strength($value) < $force) {
                $success = false;
                $message = 'Vous devez renseigner un mot de passe de force %d au minimum';
                $this->invalidate($field, sprintf($message, $force));
            }
        }
        return $success;
    }

    /**
     * Suppression des erreurs de validation non traduites (Validate::checkPasswordStrength)
     * pour les champs password et new_password lorsqu'il existe au moins un autre
     * message d'erreur.
     */
    public function afterValidate() {
        foreach($this->validationErrors as $field => $errors) {
            if(true === in_array($field, ['password', 'new_password'])) {
                foreach($errors as $idx => $error) {
                    if('Validate::checkPasswordStrength' === $error && $idx > 0) {
                        unset($this->validationErrors[$field][$idx]);
                    }
                }
            }
        }
        parent::afterValidate();
    }

    public function saveLdapManagerLdap($data) {
        $this->Role->recursive = -1;

        if (!$this->Role->exists($data['User']['role_id']) ||
            !isset($data['User']['username']) ||
            !isset($data['User']['prenom']) ||
            !isset($data['User']['nom']) ||
            !isset($data['User']['email'])
        ) {
            return false;
        }

        if (!isset($data['User']['ldap'])) {
            $data['User']['ldap'] = true;
        }

        if (!isset($data['User']['civilite'])) {
            $data['User']['civilite'] = 'M.';
        }

        if (!isset($data['User']['password'])) {
            $data['User']['password'] = PasswordGeneratorAnssi::generate();
        }

        $role_organisationId = $this->Role->find('first', [
            'conditions' => [
                'id' => $data['User']['role_id']
            ],
            'fields' => [
                'organisation_id'
            ]
        ]);

        $userLdap = parent::save($data, [
            'validate' => true,
            'fieldList' => [
                'ldap',
                'username',
                'nom',
                'prenom',
                'email',
                'civilite',
                'password'
            ]
        ]);

        if (is_array($userLdap)) {
            $this->OrganisationUser->create([
                'OrganisationUser' => [
                    'user_id' => $userLdap['User']['id'],
                    'organisation_id' => $role_organisationId['Role']['organisation_id'],
                ]
            ]);
            $this->OrganisationUser->save(null, ['atomic' => false]);

            $this->OrganisationUserRole->create([
                'OrganisationUserRole' => [
                    'organisation_user_id' => $this->OrganisationUser->id,
                    'role_id' => $userLdap['User']['role_id'],
                ]
            ]);
            $this->OrganisationUserRole->save(null, ['atomic' => false]);

            // Droits
            $query = [
                'conditions' => [
                    'RoleDroit.role_id' => $data['User']['role_id']
                ],
                'fields' => [
                    'liste_droit_id'
                ]
            ];
            $droits = $this->RoleDroit->find('all', $query);

            foreach($droits as $droit) {
                $this->Droit->create([
                    'organisation_user_id' => $this->OrganisationUser->id,
                    'liste_droit_id' => $droit['RoleDroit']['liste_droit_id']
                ]);
                $this->Droit->save(null, ['atomic' => false]);
            }

            return $userLdap;
        } else {
            return false;
        }
    }

    /**
     * Retourne les id des organisations liées à l'utilisateur.
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ['user_id' => $id],
        ];
        return Hash::extract(
            $this->OrganisationUser->find('all', $query),
            '{n}.OrganisationUser.organisation_id'
        );
    }

    public function vfNomComplet($alias = null, $fieldName = 'nom_complet') {
        $alias = $alias === null ? $this->alias : $alias;
        $sql = str_replace("\"{$this->name}\"", "\"{$alias}\"", $this->virtualFields['nom_complet']);
        return "{$sql} AS \"{$alias}__{$fieldName}\"";
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        $query = [
            'alias' => 'organisations_users',
            'fields' => [
                'organisations_users.user_id'
            ],
            'conditions' => [
                'organisations_users.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->OrganisationUser->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
