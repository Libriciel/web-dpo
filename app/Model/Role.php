<?php

/**
 * Role
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Role extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Role';
    
    public $validationDomain = 'validation';

    public $displayField = 'libelle';

    /**
     * Tri par défaut.
     *
     * @var array
     */
    public $order = ['Role.libelle ASC'];

    /**
     * Règles de validation supplémentaires.
     *
     * @var array
     */
    public $validate = [
        'libelle' => [
            'notBlank' => [
                'rule' => ['notBlank'],
                'message' => 'validation.nomProfilRequis'
            ],
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['libelle', 'organisation_id']],
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ],
        'organisation_id' => [
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['libelle', 'organisation_id']],
                'message' => 'validation.valeurDejaUtilisee'
            ]
        ]
    ];

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     *
     * @access public
     * @created 26/03/2015
     * @version V0.9.0
     */
    public $hasAndBelongsToMany = [
        'ListeDroit' => [
            'className' => 'ListeDroit',
            'joinTable' => 'role_droits',
            'foreignKey' => 'role_id',
            'associationForeignKey' => 'liste_droit_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'RoleDroit'
        ]
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 26/03/2015
     * @version V0.9.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 06/05/2015
     * @version V0.9.0
     */
    public $hasMany = [
        'OrganisationUserRole' => [
            'className' => 'OrganisationUserRole',
            'foreignKey' => 'role_id',
            'dependent' => true
        ]
    ];

    /**
     * Retourne un champ virtuel contenant le nombre d'utilisateurs liés au profil.
     *
     * @param string $primaryKeyField | 'Role.id' --> Champ représentant le Role.id
     * @param string $fieldName | 'users_count' --> Nom du champ virtuel
     * @return string
     */
    public function vfLinkedUsersCount($primaryKeyField = 'Role.id', $fieldName = 'users_count') {
        $subQuery = [
            'alias' => 'organisation_user_roles',
            'fields' => ['COUNT(organisation_user_roles.id)'],
            'joins' => [
                words_replace(
                    $this->OrganisationUserRole->join('OrganisationUser', ['type' => 'INNER']),
                    ['OrganisationUser' => 'organisation_users', 'OrganisationUserRole' => 'organisation_user_roles']
                )
            ],
            'conditions' => [
                "organisation_user_roles.role_id = {$primaryKeyField}"
            ],
            'contain' => false
        ];
        $sql = $this->OrganisationUserRole->sql($subQuery);
        return "( {$sql} ) AS \"{$this->alias}__{$fieldName}\"";
    }

    /**
     * Retourne l'id de l'organisation liée au profil (dans un array).
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ["{$this->alias}.{$this->primaryKey}" => $id],
        ];
        return (array)Hash::get($this->find('first', $query), "{$this->alias}.organisation_id");
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        return $this->getDataSource()->conditions(
            ["{$this->alias}.organisation_id" => $organisation_id],
            true,
            false,
            $this
        );
    }
}
