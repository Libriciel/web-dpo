<?php

/**
 * Soustraitant
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Soustraitant extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Soustraitant';
    
    public $validationDomain = 'validation';

    public $actsAs = [
        'Database.DatabaseFormattable' => [
            'Database.DatabaseDefaultFormatter' => [
                'formatTrim' => [
                    'NOT' => ['binary']
                ],
                'formatNull' => true,
                'formatNumeric' => [
                    'float',
                    'integer'
                ],
                'formatSuffix'  => '/_id$/',
                'formatStripNotAlnum' => '/^(telephoneresponsable|telephonestructure|faxstructure|siretstructure|telephonefixe_dpo|telephoneportable_dpo)$/'
            ]
        ],
        'FieldAsFilterOption',
        'LettercaseFormattable' => [
            'upper_noaccents' => ['nomresponsable', 'nom_dpo'],
            'title' => ['prenomresponsable', 'prenom_dpo']
        ]
    ];

    /**
     * Champs virtuels du modèle
     *
     * @var array
     */
    public $virtualFields = [
        'nom_complet' => '( COALESCE( "Soustraitant"."civiliteresponsable", \'\' ) || \' \' || COALESCE( "Soustraitant"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Soustraitant"."nomresponsable", \'\' ) )',
        'nom_complet_court' => '( COALESCE( "Soustraitant"."prenomresponsable", \'\' ) || \' \' || COALESCE( "Soustraitant"."nomresponsable", \'\' ))',
        'nom_complet_dpo' => '( COALESCE( "Soustraitant"."civility_dpo", \'\' ) || \' \' || COALESCE( "Soustraitant"."prenom_dpo", \'\' ) || \' \' || COALESCE( "Soustraitant"."nom_dpo", \'\' ) )',
        'nom_complet_court_dpo' => '( COALESCE( "Soustraitant"."prenom_dpo", \'\' ) || \' \' || COALESCE( "Soustraitant"."nom_dpo", \'\' ))'
    ];
    
    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public $validate = [
        'siretstructure' => [
            'luhn' => [
                'rule' => [
                    'luhn',
                    true
                ],
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETNonValide'
            ],
            [
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETUniquementComposeChiffre'
            ],
        ],
        'apestructure' => [
            'minLength' => [
                'rule' => ['minLength', 5],
                'allowEmpty' => true
            ],
            'maxLength' => [
                'rule' => ['maxLength', 5],
                'allowEmpty' => true
            ],
        ],
        'emailstructure' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ],
        'emailresponsable' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ],
        'email_dpo' => [
            [
                'rule' => ['custom', REGEXP_EMAIL_FR],
                'message' => 'validation.adresseMailNonValide',
                'allowEmpty' => true
            ]
        ]
    ];
    
    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 11/04/2018
     * @version v1.0.0
     */
    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'soustraitants_organisations',
            'foreignKey' => 'soustraitant_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'SoustraitantOrganisation'
        ],
        'Fiche' => [
            'className' => 'Fiche',
            'joinTable' => 'soustraitances',
            'foreignKey' => 'soustraitant_id',
            'associationForeignKey' => 'fiche_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'Soustraitance'
        ]
    ];

    /**
     * Retourne les id des entités liées au sous-traitant.
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ['soustraitant_id' => $id],
        ];
        return Hash::extract(
            $this->SoustraitantOrganisation->find('all', $query),
            '{n}.SoustraitantOrganisation.organisation_id'
        );
    }

    /**
     * Retourne une condition permettant de limiter aux sous-traitants liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        $query = [
            'alias' => 'soustraitants_organisations',
            'fields' => [
                'soustraitants_organisations.soustraitant_id'
            ],
            'conditions' => [
                'soustraitants_organisations.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->SoustraitantOrganisation->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
