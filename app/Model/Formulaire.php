<?php

/**
 * Formulaire
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Formulaire extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Formulaire';

    public $displayField = 'libelle';

    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 24/02/2020
     * @version V2.0.0
     */
    public $validate = [
        'libelle' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usesousfinalite' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usebaselegale' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usedecisionautomatisee' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usetransferthorsue' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usedonneessensible' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'useallextensionfiles' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
        'usepia' => [
            'notBlank' => [
                'on' => 'update',
                'rule' => ['notBlank']
            ]
        ],
    ];

    /**
     * hasOne associations
     *
     * @var array
     * 
     * @access public
     * @created 18/06/2015
     * @version V0.9.0
     */
    public $hasOne = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'form_id'
        ]
    ];
    
    /**
     * belongsTo associations
     *
     * @var array
     * 
     * @author Théo GUILLON <theo.guillon@adullact-projet.coop>
     * @access public
     * @created 04/11/2016
     * @version V1.0.0
     */
    public $belongsTo = [
        'Service' => [
            'className' => 'Service',
            'foreignKey' => 'service_id'
        ]
    ];
    
    public $hasMany = [
        'Champ' => [
            'className' => 'Champ',
            'foreignKey' => 'formulaire_id'
        ],
        'FormulaireOrganisation' => [
            'className' => 'FormulaireOrganisation',
            'foreignKey' => 'formulaire_id',
            'dependent' => true
        ],
        'FormulaireModeleOrganisation' => [
            'className' => 'FormulaireModeleOrganisation',
            'foreignKey' => 'formulaire_id',
            'dependent' => true
        ],
        'Modele' => [
            'className' => 'Modele',
            'foreignKey' => 'formulaire_id',
            'dependent' => true
        ],

    ];

    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'formulaires_organisations',
            'foreignKey' => 'formulaire_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'FormulaireOrganisation'
        ]
    ];

    /**
     * Retourne un champ virtuel contenant le nombre de fiches liées au formulaire.
     *
     * @param string $primaryKeyField | 'Formulaire.id' --> Champ représentant le Formulaire.id
     * @param string $fieldName | 'fiches_count' --> Nom du champ virtuel
     * @return string
     */
    public function vfLinkedFichesCount($primaryKeyField = 'Formulaire.id', $fieldName = 'fiches_count') {
        $subQuery = [
            'alias' => 'fiches',
            'fields' => ['COUNT(fiches.id)'],
            'conditions' => [
                "fiches.form_id = {$primaryKeyField}"
            ],
            'contain' => false
        ];
        $sql = $this->Fiche->sql($subQuery);
        return "( {$sql} ) AS \"{$this->alias}__{$fieldName}\"";
    }

    /**
     * Retourne l'id de l'organisation liée au formulaire (dans un array).
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'contain' => [
                'FormulaireOrganisation' => [
                    'Organisation' => [
                        'id',
                        'raisonsociale',
                        'order' => ['raisonsociale']
                    ]
                ],
            ],
            'conditions' => ["{$this->alias}.{$this->primaryKey}" => $id],
        ];

        return (array)Hash::extract($this->find('first', $query), "FormulaireOrganisation.{n}.Organisation.id");
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        return $this->getDataSource()->conditions(
            ["{$this->alias}.organisation_id" => $organisation_id],
            true,
            false,
            $this
        );
    }

    /**
     * Retourne une condition permettant de limiter aux formulaires à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getFormulaireConditionOrganisation($organisation_id , $active = null)
    {
        $query = [
            'alias' => 'formulaires_organisations',
            'fields' => [
                'formulaires_organisations.formulaire_id'
            ],
            'conditions' => [
                'formulaires_organisations.organisation_id' => $organisation_id,
            ]
        ];

        if ($active === true) {
            $query['conditions']['formulaires_organisations.active'] = true;
        }

        $sql = $this->FormulaireOrganisation->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
