<?php

/**
 * Fichierarticle
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('Folder', 'Utility');

class Fichierarticle extends AppModel {

    public $name = 'Fichierarticle';
    
    public $validationDomain = 'validation';

    /**
     * Règles de validation supplémentaires.
     *
     * @var array
     */
    public $validate = [
        'nom' => [
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['nom', 'article_id']],
                'message' => 'validation.valeurNomFichierDejaUtilisee'
            ]
        ]
    ];

    public $hasOne = 'Fichier';

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public $belongsTo = [
        'Article' => [
            'className' => 'Article',
            'foreignKey' => 'article_id'
        ]
    ];

    /**
     * 
     * @param int $idArticle
     * @param uuid $uuidDossier
     * @param int $idUser
     * @param boolean $transaction La méthode doit-elle gérer elle-même une transaction (par défaut: false) ?
     * @return boolean
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 07/08/2020
     * @version V2.0.0
     */
    public function transfereSave($idArticle, $uuidDossier, $idUser, $transaction = false)
    {
        $success = true;

        if (!empty($uuidDossier)) {
            $dir = CHEMIN_PIECE_JOINT_ARTICLE_TMP . $idUser . DS . $uuidDossier;
            $files = $this->Fichier->scan_dir($dir);

            if (isset($files) && !empty($files)) {
                if ($transaction == true) {
                    $this->begin();
                }

                // On verifie si le dossier file existe. Si c'est pas le cas on le cree
                $success = $success && create_arborescence_files();

                $accepted = Configure::read('allFileAnnexeAcceptedTypes');

                foreach ($files as $key => $file) {
                    $extension = pathinfo($file, PATHINFO_EXTENSION);
                    $pathFileTMP = $dir . DS . $file;

                    $mime = mime_content_type($pathFileTMP);

                    if (in_array($mime, $accepted) === true) {
                        $url = time();
                        $newNameFile = $url . $key . '.' . $extension;
                        $newPathFile = CHEMIN_PIECE_JOINT_ARTICLE . $newNameFile;

                        $success = $success && rename($pathFileTMP, $newPathFile);

                        if ($success) {
                            $this->create([
                                'nom' => $file,
                                'url' => $newNameFile,
                                'article_id' => $idArticle
                            ]);
                            $success = $success && $this->save(null, ['atomic' => false]);
                        }
                    }
                }

                if ($success == true) {
                    if ($transaction == true) {
                        $this->commit();
                    }

                    $folder = new Folder($dir);
                    $folder->delete();
                } else {
                    if ($transaction == true) {
                        $this->rollback();
                    }
                }
            }
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * @param int $id
     * @param boolean $transaction La méthode doit-elle gérer elle-même une
     *  transaction (par défaut: false) ?
     * @return boolean
     *
     * @access public
     * @created 26/06/2015
     * @version V1.0.0
     */
    public function deleteFichier($id, $transaction = false) {
        $success = true;
        if($transaction == true) {
            $this->begin();
        }

        $fichier = $this->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);

        $success = $success && unlink(PIECE_JOINT_ARTICLE . $fichier['Fichier']['url']);
        $success = $success && $this->delete($id);

        if ($success) {
            if($transaction == true) {
                $this->commit();
            }
            return true;
        } else {
            if($transaction == true) {
                $this->rollback();
            }
            return false;
        }
    }

}
