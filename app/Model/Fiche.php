<?php

/**
 * Fiche
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('Conversion', 'Utility');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

require_once APP . 'Vendor' . DS . 'phpgedooo_client' . DS . 'src' . DS . 'GDO_PartType.php';
require_once APP . 'Vendor' . DS . 'phpgedooo_client' . DS . 'src' . DS . 'GDO_IterationType.php';
require_once APP . 'Vendor' . DS . 'phpgedooo_client' . DS . 'src' . DS . 'GDO_FieldType.php';
require_once APP . 'Vendor' . DS . 'phpgedooo_client' . DS . 'src' . DS . 'GDO_ContentType.php';
require_once APP . 'Vendor' . DS . 'phpgedooo_client' . DS . 'src' . DS . 'GDO_FusionType.php';

class Fiche extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Fiche';

    const LISTE_BASE_LEGALE = [
        "La personne concernée a consenti au traitement de ses données à caractère personnel pour une ou plusieurs finalités spécifiques." => "La personne concernée a consenti au traitement de ses données à caractère personnel pour une ou plusieurs finalités spécifiques.",
        "Le traitement est nécessaire à l'exécution d'un contrat auquel la personne concernée est partie ou à l'exécution de mesures précontractuelles prises à la demande de celle-ci." => "Le traitement est nécessaire à l'exécution d'un contrat auquel la personne concernée est partie ou à l'exécution de mesures précontractuelles prises à la demande de celle-ci.",
        "Le traitement est nécessaire au respect d'une obligation légale à laquelle le responsable du traitement est soumis." => "Le traitement est nécessaire au respect d'une obligation légale à laquelle le responsable du traitement est soumis.",
        "Le traitement est nécessaire à la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne physique." => "Le traitement est nécessaire à la sauvegarde des intérêts vitaux de la personne concernée ou d'une autre personne physique.",
        "Le traitement est nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement." => "Le traitement est nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique dont est investi le responsable du traitement.",
        "Le traitement est nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers, à moins que ne prévalent les intérêts ou les libertés et droits fondamentaux de la personne concernée qui exigent une protection des données à caractère personnel, notamment lorsque la personne concernée est un enfant." => "Le traitement est nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers, à moins que ne prévalent les intérêts ou les libertés et droits fondamentaux de la personne concernée qui exigent une protection des données à caractère personnel, notamment lorsque la personne concernée est un enfant. \n \n(Non applicable au traitement effectué par les autorités publiques dans l'exécution de leurs missions.)"
    ];

    const LISTE_PAYS_HORS_UE = [
        "AFGHANISTAN" => "AFGHANISTAN",
        "AFRIQUE DU SUD" => "AFRIQUE DU SUD",
        "ALBANIE" => "ALBANIE",
        "ALGERIE" => "ALGERIE",
        "ANDORRA" => "ANDORRA",
        "ANGOLA" => "ANGOLA",
        "ANGUILLA" => "ANGUILLA",
        "ANTARCTIQUE" => "ANTARCTIQUE",
        "ANTIGUA ET BARBUDA" => "ANTIGUA ET BARBUDA",
        "ANTILLES NERLANDAISES" => "ANTILLES NERLANDAISES",
        "ARABIE SAOUDITE" => "ARABIE SAOUDITE",
        "ARGENTINE" => "ARGENTINE",
        "ARMENIE" => "ARMENIE",
        "ARUBA" => "ARUBA",
        "AUSTRALIE" => "AUSTRALIE",
        "AZERBAIDJAN" => "AZERBAIDJAN",
        "BAHAMAS" => "BAHAMAS",
        "BAHREIN" => "BAHREIN",
        "BANGLADESH" => "BANGLADESH",
        "BARBADE" => "BARBADE",
        "BIÉLORUSSIE" => "BIÉLORUSSIE",
        "BELIZE" => "BELIZE",
        "BENIN" => "BENIN",
        "BERMUDES" => "BERMUDES",
        "BHOUTAN" => "BHOUTAN",
        "BOLIVIE" => "BOLIVIE",
        "BOSNIE ET HERZEGOVINE" => "BOSNIE ET HERZEGOVINE",
        "BOTSWANA" => "BOTSWANA",
        "BOUVET, ILE" => "BOUVET, ILE",
        "BRESIL" => "BRESIL",
        "BRUNEI DARUSSALAM" => "BRUNEI DARUSSALAM",
        "BURKINA FASO" => "BURKINA FASO",
        "BURUNDI" => "BURUNDI",
        "CAMBODGE" => "CAMBODGE",
        "CAMEROUN" => "CAMEROUN",
        "CANADA" => "CANADA",
        "CAP VERT" => "CAP VERT",
        "CAYMAN, ILES" => "CAYMAN, ILES",
        "CENTRAFRICAINE, REPUBLIQUE" => "CENTRAFRICAINE, REPUBLIQUE",
        "CEUTA" => "CEUTA",
        "CHILI" => "CHILI",
        "CHINE" => "CHINE",
        "CHRISTMAS, ILE" => "CHRISTMAS, ILE",
        "COCOS, ILES (KEELING)" => "COCOS, ILES (KEELING)",
        "COLOMBIE" => "COLOMBIE",
        "COMORES" => "COMORES",
        "CONGO" => "CONGO",
        "CONGO, REPUBLIQUE DEMOCRATIQUE DU" => "CONGO, REPUBLIQUE DEMOCRATIQUE DU",
        "COOK, ILES" => "COOK, ILES",
        "COREE DU NORD" => "COREE DU NORD",
        "COREE DU SUD" => "COREE DU SUD",
        "COSTA RICA" => "COSTA RICA",
        "COTE D'IVOIRE" => "COTE D'IVOIRE",
        "CUBA" => "CUBA",
        "DJIBOUTI" => "DJIBOUTI",
        "DOMINICAINE, REPUBLIQUE" => "DOMINICAINE, REPUBLIQUE",
        "DOMINIQUE" => "DOMINIQUE",
        "EGYPTE" => "EGYPTE",
        "EMIRATS ARABES UNIS" => "EMIRATS ARABES UNIS",
        "EQUATEUR" => "EQUATEUR",
        "ERYTHREE" => "ERYTHREE",
        "ETATS-UNIS" => "ETATS-UNIS",
        "ETATS-UNIS, ILES MINEURS ELOIGNEES DES" => "ETATS-UNIS, ILES MINEURS ELOIGNEES DES",
        "ETHIOPIE" => "ETHIOPIE",
        "FEROE ILES" => "FEROE ILES",
        "FIDJI" => "FIDJI",
        "GABON" => "GABON",
        "GAMBIE" => "GAMBIE",
        "GEORGIE" => "GEORGIE",
        "GEORGIE DU SUD ET ILES SANDWICH DU SUD" => "GEORGIE DU SUD ET ILES SANDWICH DU SUD",
        "GHANA" => "GHANA",
        "GIBRALTAR" => "GIBRALTAR",
        "GRENADE" => "GRENADE",
        "GROENLAND" => "GROENLAND",
        "GUAM" => "GUAM",
        "GUATEMALA" => "GUATEMALA",
        "GUINEE" => "GUINEE",
        "GUINEE EQUATORIALE" => "GUINEE EQUATORIALE",
        "GUINEE-BISSAU" => "GUINEE-BISSAU",
        "GUYANA" => "GUYANA",
        "HAITI" => "HAITI",
        "HEARD, ILE ET MC DONALD, ILES" => "HEARD, ILE ET MC DONALD, ILES",
        "HONDURAS" => "HONDURAS",
        "HONG KONG" => "HONG KONG",
        "INDE" => "INDE",
        "INDONESIE" => "INDONESIE",
        "IRAK" => "IRAK",
        "IRAN" => "IRAN",
        "ISLANDE" => "ISLANDE",
        "ISRAEL" => "ISRAEL",
        "JAMAIQUE" => "JAMAIQUE",
        "JAPON" => "JAPON",
        "JORDANIE" => "JORDANIE",
        "KAZAKHSTAN" => "KAZAKHSTAN",
        "KENYA" => "KENYA",
        "KIRGHIZSTAN" => "KIRGHIZSTAN",
        "KIRIBATI" => "KIRIBATI",
        "KOSOVO" => "KOSOVO",
        "KOWEIT" => "KOWEIT",
        "LAOS" => "LAOS",
        "LESOTHO" => "LESOTHO",
        "LIBAN" => "LIBAN",
        "LIBERIA" => "LIBERIA",
        "LIBYE" => "LIBYE",
        "LIECHTENSTEIN" => "LIECHTENSTEIN",
        "MACAO" => "MACAO",
        "REPUBLIQUE DE MACEDOINE DU NORD" => "REPUBLIQUE DE MACEDOINE DU NORD",
        "MADAGASCAR" => "MADAGASCAR",
        "MALAISIE" => "MALAISIE",
        "MALAWI" => "MALAWI",
        "MALDIVES" => "MALDIVES",
        "MALI" => "MALI",
        "MALOUINES , ILES" => "MALOUINES , ILES",
        "MARIANNE DU NORD, ILES" => "MARIANNE DU NORD, ILES",
        "MAROC" => "MAROC",
        "MARSHALL, ILES" => "MARSHALL, ILES",
        "MAURICE" => "MAURICE",
        "MAURITANIE" => "MAURITANIE",
        "MELILLA" => "MELILLA",
        "MEXIQUE" => "MEXIQUE",
        "MICRONESIE, ETATS FEDERES DE" => "MICRONESIE, ETATS FEDERES DE",
        "MOLDAVIE, REPUBLIQUE DE" => "MOLDAVIE, REPUBLIQUE DE",
        "MONGOLIE" => "MONGOLIE",
        "MONTENEGRO" => "MONTENEGRO",
        "MONTSERRAT" => "MONTSERRAT",
        "MOZAMBIQUE" => "MOZAMBIQUE",
        "MYANMAR" => "MYANMAR",
        "NAMIBIE" => "NAMIBIE",
        "NAURU" => "NAURU",
        "NEPAL" => "NEPAL",
        "NICARAGUA" => "NICARAGUA",
        "NIGER" => "NIGER",
        "NIGERIA" => "NIGERIA",
        "NIUE" => "NIUE",
        "NORFOLK, ILES" => "NORFOLK, ILES",
        "NORVEGE" => "NORVEGE",
        "NOUVELLE- CALEDONIE" => "NOUVELLE- CALEDONIE",
        "NOUVELLE- ZELANDE" => "NOUVELLE- ZELANDE",
        "OCEAN INDIEN, TERRITOIRE BRITANNIQUE" => "OCEAN INDIEN, TERRITOIRE BRITANNIQUE",
        "OMAN" => "OMAN",
        "OUGANDA" => "OUGANDA",
        "OUZBEKISTAN" => "OUZBEKISTAN",
        "PAKISTAN" => "PAKISTAN",
        "PALAOS" => "PALAOS",
        "PALESTINE" => "PALESTINE",
        "PANAMA" => "PANAMA",
        "PAPOUASIE NOUVELLE-GUINEE" => "PAPOUASIE NOUVELLE-GUINEE",
        "PARAGUAY" => "PARAGUAY",
        "PEROU" => "PEROU",
        "PHILIPPINES" => "PHILIPPINES",
        "PITCAIRN, ILES" => "PITCAIRN, ILES",
        "POLYNESIE FRANCAISE" => "POLYNESIE FRANCAISE",
        "QATAR" => "QATAR",
        "RUSSIE, FEDERATION" => "RUSSIE, FEDERATION",
        "RWANDA" => "RWANDA",
        "SAINT BARTHELEMY" => "SAINT BARTHELEMY",
        "SAINT- CHRISTOPHE-ET-NEVIS" => "SAINT- CHRISTOPHE-ET-NEVIS",
        "SAINTE-LUCIE" => "SAINTE-LUCIE",
        "SAINT-HELENE (Y COMPRIS ILES ASCENSION ET TRISTAN DE CUNHA)" => "SAINT-HELENE (Y COMPRIS ILES ASCENSION ET TRISTAN DE CUNHA)",
        "SAINT-MARIN" => "SAINT-MARIN",
        "SAINT-PIERRE ET MIQUELON" => "SAINT-PIERRE ET MIQUELON",
        "SAINT-VINCENT ET LES GRENADINES" => "SAINT-VINCENT ET LES GRENADINES",
        "SALOMON, ILES" => "SALOMON, ILES",
        "SALVADOR" => "SALVADOR",
        "SAMOA" => "SAMOA",
        "SAMOA AMERICAINE" => "SAMOA AMERICAINE",
        "SAO TOME ET PRINCIPE" => "SAO TOME ET PRINCIPE",
        "SENEGAL" => "SENEGAL",
        "SERBIE" => "SERBIE",
        "SEYCHELLES" => "SEYCHELLES",
        "SIERRA LEONE" => "SIERRA LEONE",
        "SINGAPOUR" => "SINGAPOUR",
        "SOMALIE" => "SOMALIE",
        "SOUDAN" => "SOUDAN",
        "SRI LANKA" => "SRI LANKA",
        "SUD SOUDAN" => "SUD SOUDAN",
        "SUISSE" => "SUISSE",
        "SURINAME" => "SURINAME",
        "SWAZILAND" => "SWAZILAND",
        "SYRIE" => "SYRIE",
        "TADJIKISTAN" => "TADJIKISTAN",
        "TAIWAN" => "TAIWAN",
        "TANZANIE" => "TANZANIE",
        "TCHAD" => "TCHAD",
        "TERRES AUSTRALES ET ANTARCTIQUES" => "TERRES AUSTRALES ET ANTARCTIQUES",
        "THAILANDE" => "THAILANDE",
        "TIMOR ORIENTAL" => "TIMOR ORIENTAL",
        "TOGO" => "TOGO",
        "TOKELAU" => "TOKELAU",
        "TONGA" => "TONGA",
        "TRINIDAD ET TOBAGO" => "TRINIDAD ET TOBAGO",
        "TUNISIE" => "TUNISIE",
        "TURKMENISTAN" => "TURKMENISTAN",
        "TURKS ET CAICOS, ILES" => "TURKS ET CAICOS, ILES",
        "TURQUIE" => "TURQUIE",
        "TUVALU" => "TUVALU",
        "UKRAINE" => "UKRAINE",
        "URUGUAY" => "URUGUAY",
        "VANUATU" => "VANUATU",
        "VATICAN, ETAT DE LA CITE DU (SAINT-SIEGE)" => "VATICAN, ETAT DE LA CITE DU (SAINT-SIEGE)",
        "VENEZUELA" => "VENEZUELA",
        "VIERGES, ILES (BRITANNIQUES)" => "VIERGES, ILES (BRITANNIQUES)",
        "VIERGES, ILES (ETATS-UNIS" => "VIERGES, ILES (ETATS-UNIS",
        "VIET NAM" => "VIET NAM",
        "WALLIS ET FUTUNA, ILES" => "WALLIS ET FUTUNA, ILES",
        "YEMEN" => "YEMEN",
        "ZAMBIE" => "ZAMBIE",
        "ZIMBABWE" => "ZIMBABWE"
    ];

    const TYPE_GARANTIE_HORS_UE = [
        "Clauses contractuelles types (CCT)" => "Clauses contractuelles types (CCT)",
        "Règles d'entreprise contraignantes (BCR)" => "Règles d'entreprise contraignantes (BCR)",
        "Pays adéquat" => "Pays adéquat",
        "Privacy shield" => "Privacy shield",
        "Code de conduite" => "Code de conduite",
        "Certification" => "Certification",
        "Dérogations (art 49)" => "Dérogations (art 49)"
    ];

    const LISTE_DONNEES_SENSIBLES = [
        "Données révélant l'origine raciale ou ethnique" => "Données révélant l'origine raciale ou ethnique",
        "Données révélant les opinions politiques" => "Données révélant les opinions politiques",
        "Données révélant les convictions religieuses ou philosophiques " => "Données révélant les convictions religieuses ou philosophiques ",
        "Données révélant l'appartenance syndicale" => "Données révélant l'appartenance syndicale",
        "Données génétiques" => "Données génétiques",
        "Données biométriques aux fins d'identifier une personne physique de manière unique" => "Données biométriques aux fins d'identifier une personne physique de manière unique",
        "Données concernant la santé" => "Données concernant la santé",
        "Données concernant la vie sexuelle ou l'orientation sexuelle" => "Données concernant la vie sexuelle ou l'orientation sexuelle",
        "Données relatives à des condamnations pénales ou infractions" => "Données relatives à des condamnations pénales ou infractions"
    ];

    const LISTE_CRITERES = [
        "Évaluation/scoring" => "Évaluation/scoring (y compris le profilage)",
        "Décision automatique avec effet légal ou similaire" => "Décision automatique avec effet légal ou similaire",
        "Surveillance systématique" => "Surveillance systématique",
        "Données sensibles ou hautement personnellles" => "Données sensibles ou hautement personnelles (santé, géolocalisation, etc.)",
        "Collecte à large échelle" => "Collecte à large échelle",
        "Croisement de données" => "Croisement de données",
        "Personnes vulnérables" => "Personnes vulnérables (patients, personnes âgées, enfants, etc.)",
        "Usage innovant" => "Usage innovant (utilisation d’une nouvelle technologie)",
        "Exclusion du bénéfice d’un droit/contrat" => "Exclusion du bénéfice d’un droit/contrat"
    ];

    const LISTING_FIELDS_TRAITEMENT_NO_REQUIRED_PIA = [
        'ressources_humaines',
        'relation_fournisseurs',
        'gestion_electoral',
        'comites_entreprise',
        'association',
        'sante_prise_patient',
        'avocats',
        'greffiers',
        'notaires',
        'collectivites_affaires_scolaires',
        'controles_acces',
        'ethylotests'
    ];

    const LISTING_FIELDS_TRAITEMENT_REQUIRED_PIA = [
        'sante_medicosociaux',
        'donnees_genetiques',
        'profils_personnes_gestion_rh',
        'surveiller_constante_employes',
        'gestion_alertes_sociale_sanitaire',
        'gestion_alertes_professionnelle',
        'donnees_sante_registre',
        'profilage_rupture_contrat',
        'mutualises_manquements_rupture_contrat',
        'profilage_donnees_externes',
        'biometriques',
        'gestion_logements_sociaux',
        'accompagnement_social',
        'localisation_large_echelle'
    ];

    public $validate = [
        'organisation_id' => [
            'areUnique' => [
                'on' => 'update',
                'rule' => [
                    'areUnique', ['organisation_id', 'numero'], false
                ],
                'message' => '... est déjà utilisée.'
            ]
        ],
        'numero' => [
            'areUnique' => [
                'on' => 'update',
                'rule' => [
                    'areUnique', ['organisation_id', 'numero'], false
                ],
                'message' => '... est déjà utilisée.'
            ]
        ],
        'transfert_hors_ue' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'donnees_sensibles' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
//        'obligation_pia' => [
//            'checkNotBlankIf' => [
//                'rule' => ['checkNotBlankIf', 'usepia', true, [true]]
//            ]
//        ],
        'realisation_pia' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'formulaireOLD', true, [false]]
            ],
        ],
        'depot_pia' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'realisation_pia', true, ['1']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'realisation_pia', true, ['0']]
            ]
        ],
        'coresponsable' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'soustraitance' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
    ];

    /**
     * hasOne associations
     *
     * @var array
     *
     * @access public
     * @created 04/01/2016
     * @version V1.0.0
     */
    public $hasOne = [
        'ExtraitRegistre' => [
            'className' => 'ExtraitRegistre',
            'foreignKey' => 'fiche_id'
        ]
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Norme' => [
            'className' => 'Norme',
            'foreignKey' => 'norme_id',
            'dependent' => true
        ],
        'Formulaire' => [
            'className' => 'Formulaire',
            'foreignKey' => 'form_id'
        ],
        'Champ' => [
            'className' => 'Champ',
            'foreignKey' => 'formulaire_id'
        ],
        'Service' => [
            'className' => 'Service',
            'foreignKey' => 'service_id'
        ],
        'Referentiel' => [
            'className' => 'Referentiel',
            'foreignKey' => 'referentiel_id',
            'dependent' => true
        ]
    ];

    public $hasAndBelongsToMany = [
        'Responsable' => [
            'className' => 'Responsable',
            'joinTable' => 'coresponsables',
            'foreignKey' => 'fiche_id',
            'associationForeignKey' => 'responsable_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'Coresponsable'
        ],
        'Soustraitant' => [
            'className' => 'Soustraitant',
            'joinTable' => 'soustraitances',
            'foreignKey' => 'fiche_id',
            'associationForeignKey' => 'soustraitant_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'Soustraitance'
        ],
    ];

    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public $hasMany = [
        'EtatFiche' => [
            'className' => 'EtatFiche',
            'foreignKey' => 'fiche_id',
            'dependent' => true,
        ],
        'Notification' => [
            'className' => 'Notification',
            'foreignKey' => 'fiche_id',
            'dependent' => true
        ],
        'Historique' => [
            'className' => 'Historique',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
        'Valeur' => [
            'className' => 'Valeur',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
        'TraitementRegistre' => [
            'className' => 'TraitementRegistre',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
        'ExtraitRegistre' => [
            'className' => 'ExtraitRegistre',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
        'Fichier' => [
            'className' => 'Fichier',
            'foreignKey' => 'fiche_id',
            'dependent' => true
        ],
        'Coresponsable' => [
            'className' => 'Coresponsable',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
        'Soustraitance' => [
            'className' => 'Soustraitance',
            'foreignKey' => 'fiche_id',
            'dependant' => true
        ],
    ];

    /**
     * @param int|null $idUser
     * @param type|null $fiche
     * @return boolean
     *
     * @access public
     * @created 09/04/2015
     * @version V1.0.0
     */
    public function isOwner($idUser = null, $fiche = null) {
        if ($idUser == $fiche['Fiche']['user_id']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param type $string
     * @return type
     *
     * @access public
     * @created 26/06/2015
     * @version V1.0.0
     */
    public function isJson($string) {
        if (is_string($string) && is_object(json_decode($string)) || is_array(json_decode($string)) ) {
            json_decode($string);
            return (json_last_error() == JSON_ERROR_NONE);
        }

       return false;
    }

    /**
     * 
     * @param json $tabId
     * @param string $file
     * @param string $cheminFile
     * @param int $idOrganisation
     * @param bool $historique
     * @return type
     */
    public function preparationGeneration(
        $tabId,
        $file,
        $cheminFile,
        $idOrganisation,
        $historique = false,
        $outputFormat = 'pdf',
        $sendInfoEntiteToModeles = false,
        $titleExtrait = false
    ){
        $donnees = [];

        if ($sendInfoEntiteToModeles === true) {
            // On récupère et met en forme les informations de l'organisation
            $donnees = $this->_preparationGenerationOrganisation($idOrganisation, $donnees);
        }

        // On récupère et met en forme les numéro d'enregistrement du traitement
        $donnees = $this->_preparationGenerationNumeroEnregistrement($tabId, $donnees);

        /* On met en forme les valeurs du traitement (informations de
         * l'organisation + champ propre au formulaire + informations du DPO +
         * information sur le déclarant au moment de la création du traitement
         */
        foreach ((array)json_decode($tabId, true) as $key => $id) {
            $donnees['traitement'][$key] = $this->_preparationGenerationValeurTraitement($id, $donnees['traitement'][$key], $historique);
        }
        
        if ($historique == true) {
            $id = json_decode($tabId);

            $donnees = $this->_preparationGenerationHistorique($id, $donnees);
        }

        // On récupère l'état du traitement pour la mise en place du filigrane dans le modèle
        $donnees = $this->_preparationEtatTraitement($id, $donnees);

//        debug(array_keys(Hash::flatten($donnees)));
//        var_dump($donnees);
//        die;
        
        // On effectue la génération
        $pdf = $this->genereFusion($file, $cheminFile, $donnees, $outputFormat);
        return $pdf;
    }

    public function preparationGenerationRegistre(
        $urlTraitementsODT,
        $modeleFile,
        $cheminModele,
        $idOrganisation,
        $titleExtrait = false
    ) {
        $donnees = [];

        // On récupère et met en forme les informations de l'organisation
        $donnees = $this->_preparationGenerationOrganisation($idOrganisation, $donnees);

        if ($titleExtrait === true) {
            $valueTitre = TITRE_EXTRAIT_REGISTRE;
            $valueTitreComplet = TITRE_COMPLET_EXTRAIT_REGISTRE;
        } else {
            $valueTitre = TITRE_REGISTRE;
            $valueTitreComplet = TITRE_COMPLET_REGISTRE;
        }

        $donnees['titre'] = [
            'value' => $valueTitre,
            'type' => 'text'
        ];

        $donnees['titre_complet'] = [
            'value' => $valueTitreComplet,
            'type' => 'text'
        ];

        if (!empty($urlTraitementsODT)) {
            foreach ($urlTraitementsODT as $key =>  $filename) {
                if (file_exists($filename)) {
                    $mime = mime_content_type($filename);

                    switch ($mime) {
                        case 'application/vnd.oasis.opendocument.text':
                            $content = file_get_contents($filename);
                            break;
                        default:
                            break;
//                            $msgstr = 'Type MIME %s non pris en charge pour le fichier %s';
//                            throw new RuntimeException(sprintf($msgstr, $mime, $filename));
                    }

                    $donnees['corpsTraitements'][$key]['traitement_genere'] = [
                        'value' => $content,
                        'type' => 'file'
                    ];
                }
            }
        }

//        var_dump($donnees);
//        die;

        // On effectue la génération
        $pdf = $this->genereFusion($modeleFile, $cheminModele, $donnees);
        return $pdf;
    }

    /**
     * Récupération les inforations à l'instant T de l'organisation puis
     * mise en forme des informations pour l'envoyer à la génération
     *
     * @param int $idOrganisation -> id de l'organisation en cours (en session)
     * @param array() $donnees -> tableau des valeurs déjà en forme pour la génération
     * @return array()
     *
     * @access private
     * @created 04/04/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _preparationGenerationOrganisation($idOrganisation, $donnees) {
        // On récupère en BDD les informations à l'instant T de l'organisation en cours
        $organisation = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $idOrganisation
            ],
            'fields' => [
                'raisonsociale',
                'telephone',
                'fax',
                'adresse',
                'email',
                'sigle',
                'siret',
                'ape',
                'civiliteresponsable',
                'nomresponsable',
                'prenomresponsable',
                'emailresponsable',
                'telephoneresponsable',
                'fonctionresponsable',
                'dpo',
                'numerodpo',
                'emaildpo',
                'responsable_nom_complet',
                'responsable_nom_complet_court'
            ]
        ]);

        // On récupère en BDD les informations sur l'utilisateur qui est DPO
        $dpo = $this->User->find('first', [
            'conditions' => [
                'id' => $organisation['Organisation']['dpo']
            ],
            'fields' => [
                'civilite',
                'nom',
                'prenom',
                'telephonefixe',
                'telephoneportable',
                'nom_complet',
                'nom_complet_court'
            ]
        ]);
        
        if (isset($organisation['Organisation']['dpo']) === true) {
            unset($organisation['Organisation']['dpo']);
            $dpo['User']['numerodpo'] = $organisation['Organisation']['numerodpo'];
            $dpo['User']['emaildpo'] = $organisation['Organisation']['emaildpo'];
            unset($organisation['Organisation']['numerodpo']);
            unset($organisation['Organisation']['emaildpo']);
        }

        $donnees = [];
        foreach ($organisation['Organisation'] as $key => $valueOrganisation) {
            $donnees['organisation_'.$key] = [
                'value' => $valueOrganisation,
                'type' => 'text'
            ];
        }
        
        foreach ($dpo['User'] as $key => $valueUserDPO) {
            $donnees['dpo_'.$key] = [
                'value' => $valueUserDPO,
                'type' => 'text'
            ];
        }

        return ($donnees);
    }

    /**
     * Récupération du numéro d'enregistrement du traitement au registre et
     * mise en forme des valeurs pour l'envoyer à la génération
     *
     * @param json $tabId -> id des traitements à générer
     * @param array() $donnees -> tableau des valeurs déjà en forme pour la génération
     * @return array()
     *
     * @access private
     * @created 04/04/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _preparationGenerationNumeroEnregistrement($tabId, $donnees) {
        /* On récupére les valeurs du/des traitement(s) + numéro
         * d'enregistrement du traitement
         */
        $arrayId = (array)json_decode($tabId);
        $numeroEnregistrements = [];
        foreach ($arrayId as $key => $id) {
            $numeroEnregistrements[$key] = $this->find('first', [
                'conditions' => [
                    'id' => $id
                ],
                'fields' => [
                    'numero'
                ]
            ]);
        }

        // On met en forme le numéro d'enregistrement du traitement au registre
        foreach ($numeroEnregistrements as $key => $numeroEnregistrement) {
            $donnees['traitement'][$key]['valeur_numeroenregistrement'] = [
                'value' => $numeroEnregistrement['Fiche']['numero'],
                'type' => 'text'
            ];
        }

        return ($donnees);
    }

    /**
     * On récupére les valeurs du/des traitement(s) + numéro
     * d'enregistrement du traitement
     *
     * @param json $tabId -> id des traitements à générer
     * @param array() $donnees -> tableau des valeurs déjà en forme pour la génération
     * @return array()
     *
     * @access private
     * @created 04/04/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _preparationGenerationValeurTraitement($fiche_id, $donnees, $historique)
    {
        $conditions = [
            'fiche_id' => $fiche_id,
            'champ_name !=' => [
                'coresponsabilitefields',
                'soustraitancefields'
            ]
        ];

        if ($historique == false) {
            $conditions['champ_name'] = [
                'outilnom',
                'finaliteprincipale',
                'declarantpersonnenom',
                'declarantpersonneemail',
                'declarantpersonneportable',
                'declarantpersonnefix',
            ];
        }

        $data = $this->Valeur->find('all', [
            'conditions' => $conditions,
            'fields' => [
                'valeur',
                'champ_name'
            ]
        ]);

        $data = $this->_addInfoTraitement($fiche_id, $data, $historique);

        /**
         * On recupere les champs 'deroulant', 'checkboxes', 'radios' qui
         * sont dans le formulaire associer a la fiche
         */
        $idForm = $this->find('first', [
            'conditions' => [
                'id' => $fiche_id
            ]
        ]);

        $typeChamps = ['deroulant', 'checkboxes', 'multi-select'];
        $champs = ClassRegistry::init('Champ')->find('all', [
            'conditions' => [
                'formulaire_id' => $idForm['Fiche']['form_id'],
                'type' => $typeChamps,
            ],
        ]);

        /**
         * On decode les infos du champ details pour ensuite faire
         * un tableau avec le name du champs et les valeurs
         */
        $choixChampMultiple = [];
        $checkBoxField = [];
        foreach ($champs as $value) {
            $options = json_decode($value['Champ']['details'], true);

            if (in_array($value['Champ']['type'], ['checkboxes', 'multi-select'])) {
                $checkBoxField[$options['name']] = $options['options'];
            } else {
                $choixChampMultiple[$options['name']] = $options['options'];
            }
        }

        $champsInformationComplementaire = [
            'sousFinalite',
            'baselegale',
            'criteres',
        ];

        $sections = [
            'horsue',
            'donneessensibles',
            'coresponsables',
            'soustraitances',
        ];

        /**
         * On vérifie que le tableau qu'on a créé juste au dessus existe.
         * Si il exite on on prend la valeur de l'id choisit dans le tableau,
         * sinon on prend directement la valeur enregistré dans la table Valeur.
         */
        foreach ($data as $key => $value) {
            if (!empty($choixChampMultiple[$value['Valeur']['champ_name']])) {
                $donnees['valeur_' . $value['Valeur']['champ_name']] = [
                    'value' => $choixChampMultiple[$value['Valeur']['champ_name']][intval($value['Valeur']['valeur'])],
                    'type' => 'text'
                ];
            } elseif (!empty($checkBoxField[$value['Valeur']['champ_name']])) {
                $choixCheckbox = json_decode($value["Valeur"]["valeur"]);

                foreach ($choixCheckbox as $key => $choix) {
                    $donnees[$value['Valeur']['champ_name']][$key]['valeur_' . $value['Valeur']['champ_name']] = [
                        'value' => $checkBoxField[$value['Valeur']['champ_name']][$choix],
                        'type' => 'text'
                    ];
                }
            } elseif (in_array($value['Valeur']['champ_name'], $champsInformationComplementaire)) {
                foreach (json_decode($value['Valeur']['valeur']) as $key => $val) {
                    $donnees[$value['Valeur']['champ_name']][$key]['valeur_' . $value['Valeur']['champ_name']] = [
                        'value' => $val,
                        'type' => 'text'
                    ];
                }
            } elseif (in_array($value['Valeur']['champ_name'], $sections)) {

                $jsons = json_decode($value['Valeur']['valeur'], true);

                if ($value['Valeur']['champ_name'] === 'coresponsables') {
                    $prefix = 'coresponsable_';
                } elseif ($value['Valeur']['champ_name'] === 'soustraitances') {
                    $prefix = 'soustraitance_';
                } else {
                    $prefix = 'valeur_';
                }

                $key = 0;
                foreach ($jsons as $json) {
                    foreach ($json as $variable => $val) {
                        // @fixme : soustraitant_ = soustraitance_ : variable sup formulaire
                        $nameVariable = $prefix . $variable;
                        if (strpos($variable, $prefix) === 0 ||
                            strpos($variable, 'soustraitant_') === 0
                        ) {
                            $nameVariable = $variable;
                        }

                        $donnees[$value['Valeur']['champ_name']][$key][$nameVariable] = [
                            'value' => $val,
                            'type' => 'text'
                        ];
                    }

                    $key++;
                }
            } else {
                $donnees['valeur_' . $value['Valeur']['champ_name']] = [
                    'value' => $value['Valeur']['valeur'],
                    'type' => 'text'
                ];
            }
        }
//        unset($donnees['valeur_fichiers']);
        
        if ($historique == true) {
            $donnees = $this->_preparationAnnexe($fiche_id, $donnees);
        }

        return ($donnees);
    }

    private function _addInfoTraitement($fiche_id, $data, $historique)
    {
        $traitement = $this->find('first', [
            'conditions' => [
                'id' => $fiche_id
            ],
            'fields' => [
                'norme_id',
                'referentiel_id',
                'coresponsable',
                'created',
                'modified',
                'soustraitance',
                'obligation_pia',
                'realisation_pia',
                'depot_pia',
                'transfert_hors_ue',
                'donnees_sensibles',
                'service_id'
            ]
        ]);

        foreach ($traitement as $fiche) {
            foreach ($fiche as $key => $value) {
                switch ($key) {
                    case 'norme_id':
                        $data = $this->_addNorme($data, $value);
                        break;

                    case 'referentiel_id':
                        $data = $this->_addReferentiel($data, $value);
                        break;

                    case 'coresponsable':
                        if ($value === true) {
                            $data = $this->_addValueData($data, 'Oui', $key);

                            if ($historique === true) {
                                $data = $this->_addCoresponsable($data, $fiche_id);
                            }
                        } else {
                            $data = $this->_addValueData($data, 'Non', $key);
                        }
                        break;

                    case 'soustraitance':
                        if ($value === true) {
                            $data = $this->_addValueData($data, 'Oui', $key);
                            if ($historique === true) {
                                $data = $this->_addSoustraitance($data, $fiche_id);
                            }
                        } else {
                            $data = $this->_addValueData($data, 'Non', $key);
                        }
                        break;

                    case 'obligation_pia':
                    case 'realisation_pia':
                    case 'depot_pia':
                    case 'transfert_hors_ue':
                    case 'donnees_sensibles':
                        if (!is_null($value)) {
                            if ($value === true) {
                                $data = $this->_addValueData($data, 'Oui', $key);
                            } else {
                                $data = $this->_addValueData($data, 'Non', $key);
                            }
                        } else {
                            $data = $this->_addValueData($data, '', $key);
                        }
                        break;

                    case 'service_id':
                        $service = $this->Service->find('first', [
                            'conditions' => [
                                'id' => $value
                            ],
                            'fields' => [
                                'libelle'
                            ]
                        ]);

                        $valueService = "Aucun service";
                        if (!empty($service)) {
                            $valueService = $service['Service']['libelle'];
                        }

                        $data = $this->_addValueData($data, $valueService, 'declarantservice');
                        break;

                    case 'created':
                    case 'modified':
                        $data = $this->_addValueData($data, $value, 'fiche'.$key);
                        break;

                    default:
                        break;
                }
            }
        }

        return $data;
    }

    private function _addNorme($data, $norme_id)
    {
        $valueNorme = '';
        $valueNormeLibelle = '';
        $valueNormeDescription = '';

        if (!empty($norme_id)) {
            $norme = $this->Norme->find('first', [
                'conditions' => [
                    'id' => $norme_id
                ],
                'fields' => [
                    'norme',
                    'numero',
                    'libelle',
                    'description'
                ]
            ]);

            if (!empty($norme)) {
                $valueNorme = $norme['Norme']['norme'] . "-" . $norme['Norme']['numero'];
                $valueNormeLibelle = $norme['Norme']['libelle'];
                $valueNormeDescription = preg_replace('@<[^>]*?>.*?>@si', "\n\r", $norme['Norme']['description']);
            }
        }

        $data = $this->_addValueData($data, $valueNorme,'norme');
        $data = $this->_addValueData($data, $valueNormeLibelle, 'normelibelle');
        $data = $this->_addValueData($data, $valueNormeDescription,'normedescription');

        return $data;
    }

    private function _addReferentiel($data, $referentiel_id)
    {
        $valueReferentiel = '';
        $valueReferentielDescription = '';

        if (!empty($referentiel_id)) {
            $referentiel = $this->Referentiel->find('first', [
                'conditions' => [
                    'id' => $referentiel_id
                ],
                'fields' => [
                    'name',
                    'description'
                ]
            ]);

            if (!empty($referentiel)) {
                $valueReferentiel = $referentiel['Referentiel']['name'];
                $valueReferentielDescription = nl2br($referentiel['Referentiel']['description']);
                $valueReferentielDescription = html_entity_decode(strip_tags(str_replace("<br />", "\n", $valueReferentielDescription)));
            }
        }

        $data = $this->_addValueData($data, $valueReferentiel,'referentiel');
        $data = $this->_addValueData($data, $valueReferentielDescription,'referentieldescription');

        return $data;
    }

    private function _addCoresponsable($data, $fiche_id)
    {
        $schemaResponsableFields =  array_filter(array_keys($this->Responsable->schema()), function($name) {
            return in_array($name, ['createdbyorganisation', 'created', 'modified']) === false && preg_match('/_id$/', $name) !== 1;
        });

        $condition = [];
        $query = [
            'conditions' => $condition,
            'order' => [
                'Responsable.raisonsocialestructure ASC'
            ],
            'fields' => $schemaResponsableFields
        ];

        $subQuery = [
            'alias' => 'coresponsables',
            'fields' => [
                'coresponsables.responsable_id'
            ],
            'conditions' => [
                'coresponsables.fiche_id' => $fiche_id
            ]
        ];
        $sql = $this->Coresponsable->sql($subQuery);

        $query['conditions'][] = "Responsable.id IN ( {$sql} )";

        $coresponsables = $this->Responsable->find('all', $query);

        $coresponsabilitefields = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'champ_name ' => 'coresponsabilitefields'
            ],
            'fields' => [
                'valeur'
            ]
        ]);

        if (!empty($coresponsabilitefields)) {
            $coresponsabilitefields = json_decode(Hash::get($coresponsabilitefields, 'Valeur.valeur'), true);
            foreach ($coresponsables as $coresponsable) {
                $responsable_id = $coresponsable['Responsable']['id'];
                unset($coresponsable['Responsable']['id']);

                if (isset($coresponsabilitefields[$responsable_id])) {
                    $merge[] = array_merge(
                        $coresponsable['Responsable'],
                        $coresponsabilitefields[$responsable_id]
                    );
                }
            }
        } else {
            $merge = Hash::extract($coresponsables, '{n}.Responsable');
        }

        $data = $this->_addValueData($data, json_encode($merge, true), 'coresponsables');

        return ($data);
    }

    private function _addSoustraitance($data, $fiche_id)
    {
        $schemaSoustraitantFields =  array_filter(array_keys($this->Soustraitant->schema()), function($name) {
            return in_array($name, ['createdbyorganisation', 'created', 'modified']) === false && preg_match('/_id$/', $name) !== 1;
        });

        $condition = [];
        $query = [
            'conditions' => $condition,
            'order' => [
                'Soustraitant.raisonsocialestructure ASC'
            ],
            'fields' => $schemaSoustraitantFields
        ];

        $subQuery = [
            'alias' => 'soustraitances',
            'fields' => [
                'soustraitances.soustraitant_id'
            ],
            'conditions' => [
                'soustraitances.fiche_id' => $fiche_id
            ]
        ];
        $sql = $this->Soustraitance->sql($subQuery);

        $query['conditions'][] = "Soustraitant.id IN ( {$sql} )";

        $soustraitances = $this->Soustraitant->find('all', $query);

        $soustraitancefields = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'champ_name ' => 'soustraitancefields'
            ],
            'fields' => [
                'valeur'
            ]
        ]);
        $soustraitancefields = json_decode(Hash::get($soustraitancefields, 'Valeur.valeur'), true);

        if (!empty($soustraitancefields)) {
            foreach ($soustraitances as $key => $soustraitance) {
                $soustraitant_id = $soustraitance['Soustraitant']['id'];
                unset($soustraitance['Soustraitant']['id']);

                if (isset($soustraitancefields[$soustraitant_id])) {
                    $merge[] = array_merge(
                        $soustraitance['Soustraitant'],
                        $soustraitancefields[$soustraitant_id]
                    );
                }
            }
        } else {
            $merge = Hash::extract($soustraitances, '{n}.Soustraitant');
        }

        $data = $this->_addValueData($data, json_encode($merge, true), 'soustraitances');

        return ($data);
    }

    private function _addValueData ($data, $valeur, $champ_name)
    {
        $data[] = [
            'Valeur' => [
                'valeur' => $valeur,
                'champ_name' => $champ_name
            ]
        ];

        return $data;
    }

    private function _preparationAnnexe($id, $donnees) {
        // On chercher si le traitement comporte des annexe(s)
        $fileAnnexes = $this->Fichier->find('all', [
            'contain' => [
                'Typage'
            ],
            'conditions' => [
                'Fichier.fiche_id' => $id
            ],
            'fields' => [
                'Fichier.nom',
                'Fichier.url',
                'Fichier.created',
                'Typage.libelle'
            ]
        ]);

        if (!empty($fileAnnexes)) {
            foreach ($fileAnnexes as $key => $fileAnnexe) {
                $filename = CHEMIN_PIECE_JOINT . $fileAnnexe['Fichier']['url'];
                
                if (file_exists($filename)) {
                    $mime = mime_content_type($filename);

                    switch($mime) {
                        case 'application/pdf':
                            $target = CHEMIN_PIECE_JOINT_CONVERSION . preg_replace('/\.pdf$/i', '.odt', $fileAnnexe['Fichier']['url']);
                            
                            if (file_exists($target)) {
                                $content = file_get_contents($target);
                            } else {
                                $msgstr = 'Le fichier convertie n\'est pas présent sur le disque : %s';
                                throw new RuntimeException(sprintf($msgstr, $target));
                            }
                            break;
                        case 'application/vnd.oasis.opendocument.text':
                            $content = file_get_contents($filename);
                            break;
                        default:
                            break;
//                            $msgstr = 'Type MIME %s non pris en charge pour le fichier %s';
//                            throw new RuntimeException(sprintf($msgstr, $mime, $filename));
                    }

                    if (!empty($content)) {
                        $donnees['fichiers'][$key]['valeur_annexe'] = [
                            'value' => $content,
                            'type' => 'file'
                        ];
                    }

                    $donnees['info_fichiers'][$key]['valeur_nom_fichier'] = [
                        'value' => $fileAnnexe['Fichier']['nom'],
                        'type' => 'text'
                    ];

                    $donnees['info_fichiers'][$key]['valeur_date_depot'] = [
                        'value' => $fileAnnexe['Fichier']['created'],
                        'type' => 'text'
                    ];

                    $donnees['info_fichiers'][$key]['valeur_type_fichier'] = [
                        'value' => $fileAnnexe['Typage']['libelle'],
                        'type' => 'text'
                    ];

                    $donnees['info_fichiers'][$key]['valeur_taille_fichier'] = [
                        'value' => (string)round((filesize($filename) / 1024), 2) . ' kB',
                        'type' => 'text'
                    ];

                } else {
                    debug("PAS TROUVER" . CHEMIN_PIECE_JOINT . $fileAnnexe['Fichier']['url']);
                }
            }
        }

        return ($donnees);
    }

    /**
     *
     * @param type $id
     * @param array $donnees
     * @return array
     *
     * @access private
     * @created 04/04/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _preparationGenerationHistorique($id, $donnees) {
        $historiques = $this->Historique->find('all', [
            'conditions' => [
                'fiche_id' => $id
            ],
            'fields' => [
                'content',
                'created'
            ]
        ]);

        foreach ($historiques as $historique) {
            $donnees['historiques'][] = [
                'created' => [
                    'value' => $historique['Historique']['created'],
                    'type' => 'date'
                ],
                'content' => [
                    'value' => $historique['Historique']['content'],
                    'type' => 'text'
                ]
            ];
        }

        return ($donnees);
    }

    /**
     * @param int $fiche_id
     * @param array $donnees
     * @return array
     *
     * @access private
     *
     * @created 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _preparationEtatTraitement ($fiche_id, $donnees)
    {
        $etatFiche = $this->EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'actif' => true
            ],
            'fields' => [
                'id',
                'etat_id'
            ]
        ]);

        if (in_array($etatFiche['EtatFiche']['etat_id'], EtatFiche::LISTE_ETAT_TRAITEMENT_REGISTRE)) {
            $donnees['etat_traitement_alias'] = [
                'value' => 'REGISTRE',
                'type' => 'text'
            ];
        } else {
            $donnees['etat_traitement_alias'] = [
                'value' => 'REDACTION',
                'type' => 'text'
            ];
        }

        return ($donnees);
    }

    /**
     * Génération PDF à la volée
     *
     * @param type $file
     * @param type $cheminFile
     * @param type $donnees
     * @return type
     *
     * @access public
     * @created 04/01/2016
     * @version V1.0.0
     */
    private function genereFusion($file, $cheminFile, $donnees, $outputFormat = 'pdf') {
        App::uses('FusionConvBuilder', 'FusionConv.Utility');
        App::uses('FusionConvDebugger', 'FusionConv.Utility');
        App::uses('FusionConvConverterCloudooo', 'FusionConv.Utility/Converter');

//        var_dump($donnees);
//        die;

        $MainPart = new phpgedooo_client\GDO_PartType();

        $data = [];
        $types = [];
        $correspondances = [];
        
        $this->_format($MainPart, $donnees, $data, $types, $correspondances);

        $Document = FusionConvBuilder::main($MainPart, $data, $types, $correspondances);
//echo '<pre>';
//print_r(FusionConvDebugger::allPathsToCsv($MainPart, true));die();
//echo '</pre>';
        $sMimeType = 'application/vnd.oasis.opendocument.text';
        
        $Template = new phpgedooo_client\GDO_ContentType("", 'model.odt', "application/vnd.oasis.opendocument.text", "binary", file_get_contents($cheminFile . $file));
        $Fusion = new phpgedooo_client\GDO_FusionType($Template, $sMimeType, $Document);

        $Fusion->process();
        if ($outputFormat == 'pdf') {
            $pdf = FusionConvConverterCloudooo::convert($Fusion->getContent()->binary);
        } else {
            $pdf = FusionConvConverterCloudooo::convert($Fusion->getContent()->binary, 'odt', 'odt' );
        }

        return $pdf;
    }

    /**
     *
     * @param type $MainPart
     * @param type $aData
     * @param type $data
     * @param type $types
     * @param type $correspondances
     */
    private function _format(&$MainPart, &$aData, &$data, &$types, &$correspondances) {
        if (!empty($aData)) {
            foreach ($aData as $key => $value) {
                if (is_array($value) && !empty($value[0])) {
                    $this->_formatIteration($MainPart, $key, $value);
                } elseif (isset($value['value']) && isset($value['type'])) {
                    $data[$key] = $value['value'];
                    $types[$key] = $value['type'];
                    $correspondances[$key] = $key;
                }
            }
        }
    }

    /**
     *
     * @param type $MainPart
     * @param type $iteration
     * @param type $aData
     */
    private function _formatIteration(&$MainPart, $iteration, &$aData) {
        foreach ($aData as $key => $value) {
            $dataIteration = [];
            
            foreach ($value as $keyIteration => $valueIteration) {
                if (isset($valueIteration[0])) {
                    $this->_formatIterationChild($dataIteration, $keyIteration, $valueIteration, $typesIteration, $correspondancesIteration);
                } elseif (isset($valueIteration['type'])) {
                    $dataIteration[$keyIteration] = $valueIteration['value'];
                    $typesIteration[$iteration . '.' . $keyIteration] = $valueIteration['type'];
                    $correspondancesIteration[$iteration . '.' . $keyIteration] = $keyIteration;
                }
            }
            $aDataIteration[$iteration][] = $dataIteration;
        }

        FusionConvBuilder::iteration($MainPart, $iteration, $aDataIteration, $typesIteration, $correspondancesIteration);
    }

    /**
     *
     * @param type $dataIteration
     * @param type $iteration
     * @param type $aData
     * @param type $typesIteration
     * @param type $correspondancesIteration
     */
    private function _formatIterationChild(&$dataIteration, $iteration, $aData, &$typesIteration, &$correspondancesIteration) {
        foreach ($aData as $key => $value) {
            $dataIterationchild = [];
            
            foreach ($value as $keyIteration => $valueIteration) {
                if (isset($valueIteration[0])) {
                    $this->_formatIterationChild($dataIterationchild, $keyIteration, $valueIteration, $typesIteration, $correspondancesIteration);
                } elseif (isset($valueIteration['type'])) {
                    $dataIterationchild[$keyIteration] = $valueIteration['value'];
                    $typesIteration[$iteration . '.' . $keyIteration] = $valueIteration['type'];
                    $correspondancesIteration[$iteration . '.' . $keyIteration] = $keyIteration;
                }
            }
            $dataIteration[$iteration][] = $dataIterationchild;
        }
    }

    /**
     * Retourne l'id de l'organisation liée à la fiche (dans un array).
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ["{$this->alias}.{$this->primaryKey}" => $id],
        ];
        return (array)Hash::get($this->find('first', $query), "{$this->alias}.organisation_id");
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        return $this->getDataSource()->conditions(
            ["{$this->alias}.organisation_id" => $organisation_id],
            true,
            false,
            $this
        );
    }

    // @todo: à placer dans AppModel
    public function beforeValidate($options = array())
    {
        $return = parent::beforeValidate($options);

        foreach ($this->schema() as $fieldName => $params) {
            if ($params['type'] === 'boolean' && isset($this->data[$this->alias][$fieldName]) === true) {
                if (is_bool($this->data[$this->alias][$fieldName]) === true) {
                    $this->data[$this->alias][$fieldName] = (int)$this->data[$this->alias][$fieldName];
                }
            }
        }

        return $return;
    }

    public function generationExtraitRegistreHtml($organisation_id, $tabFichesIds)
    {
        $donnees = [];

        // On récupère et met en forme les informations de l'organisation
        $donnees = $this->_preparationGenerationOrganisation($organisation_id, $donnees);

        // On récupère et met en forme les numéro d'enregistrement du traitement
        $donnees = $this->_preparationGenerationNumeroEnregistrement($tabFichesIds, $donnees);

        /* On met en forme les valeurs du traitement (informations de
         * l'organisation + champ propre au formulaire + informations du DPO +
         * information sur le déclarant au moment de la création du traitement
         */
        foreach ((array)json_decode($tabFichesIds, true) as $key => $id) {
            $donnees['traitement'][$key] = $this->_preparationGenerationValeurTraitement($id, $donnees['traitement'][$key], false);
        }

        return $donnees;
    }
}
