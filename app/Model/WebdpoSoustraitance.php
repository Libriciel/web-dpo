<?php

/**
 * WebdpoSoustraitance
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class WebdpoSoustraitance extends AppModel
{

    public $name = 'WebdpoSoustraitance';

    public $useTable = false;

    protected $_schema = [];

    /**
     * Enregistrement en bdd du ou des soutritants selectionner
     *
     * @param $fiche_id
     * @param $dataSoustraitance
     * @param $useSoustraitance
     * @return bool
     * @throws Exception
     */
    public function soustraitanceSave ($fiche_id, $dataSoustraitance, $useSoustraitance)
    {
        $success = true;
        $Soustraitance = ClassRegistry::init('Soustraitance');

        if (empty($dataSoustraitance['currentSoustraitance'])
            && empty($dataSoustraitance['soustraitances'])
            && $useSoustraitance == true
        ) {
            $Soustraitance->invalidate('soustraitances', 'Champ obligatoire');
            return false;
        }

        if (empty($fiche_id)) {
            return false;
        }

        if ($useSoustraitance == false) {
            $success = $Soustraitance->deleteAll([
                    'Soustraitance.fiche_id' => $fiche_id
                ]) && $success;

            return true;
        }

        if (isset($dataSoustraitance['currentSoustraitance']) && !empty($dataSoustraitance['currentSoustraitance']) && !empty($dataSoustraitance['soustraitances'])) {
            foreach ($dataSoustraitance['currentSoustraitance'] as $currentSoustraitance) {
                if (($key = array_search($currentSoustraitance['soustraitant_id'], $dataSoustraitance['soustraitances'])) !== false) {
                    unset($dataSoustraitance['soustraitances'][$key]);
                }
            }

            $noDeleteSoustraitance = Hash::extract($dataSoustraitance['currentSoustraitance'], '{n}.soustraitance_id');
            $success = $Soustraitance->deleteAll([
                    'Soustraitance.id !=' => $noDeleteSoustraitance,
                    'Soustraitance.fiche_id' => $fiche_id
                ]) && $success;
        } else {
            $success = $Soustraitance->deleteAll([
                    'Soustraitance.fiche_id' => $fiche_id
                ]) && $success;
        }

        if ($success === false) {
            return $success;
        }

        if (!empty($dataSoustraitance['soustraitances'])) {
            $Soustraitant = ClassRegistry::init('Soustraitant');

            foreach ($dataSoustraitance['soustraitances'] as $soustraitant_id) {
                $soustraitant = $Soustraitant->find('first', [
                    'conditions' => [
                        'id' => $soustraitant_id
                    ]
                ]);

                $Soustraitance->create([
                    'fiche_id' => $fiche_id,
                    'soustraitant_id' => $soustraitant['Soustraitant']['id'],
                ]);
                $success = false !== $Soustraitance->save(null, ['atomic' => false]) && $success;
            }
        }

        return $success;
    }

    public function cleanAndCheckDataSoustraitance($data, $champs)
    {
        if ($data['Fiche']['soustraitance'] == false) {
            if (isset($data['WebdpoFiche']['soustraitancefields'])) {
                unset($data['WebdpoFiche']['soustraitancefields']);
            }

            if (isset($data['Soustraitance'])) {
                unset($data['Soustraitance']);
            }

            if (isset($data[$this->alias])) {
                unset($data[$this->alias]);
            }

            return $data;
        }

        $arraySoustraitanceFields = [];
        foreach ($champs as $champ) {
            if ($champ['Champ']['champ_soustraitant'] === true && $champ['Champ']['champ_coresponsable'] === false) {
                $arraySoustraitanceFields[] = $champ;
            }
        }

        // On vérifie que les champs suplémentaite du formualaire ne soient pas obligatoire
        if (isset($data[$this->alias]) && !empty($data[$this->alias])) {
            $success = true;

            foreach ($data[$this->alias] as $soustraitant_id => $soustraitancefield) {
                foreach ($arraySoustraitanceFields as $arraySoustraitanceField) {
                    $details = json_decode($arraySoustraitanceField['Champ']['details'], true);
                    if ($details['obligatoire'] === true) {
                        if (in_array($arraySoustraitanceField['Champ']['type'], ['checkboxes', 'multi-select'])) {
                            if (empty($soustraitancefield[$details['name']])) {
                                $this->validate[$details['name']]['multiple'] = ['rule' => ['multiple', ['min' => 1]]];
                                $success = false;
                            }
                        } else {
                            if (empty($soustraitancefield[$details['name']])) {
                                $this->validate[$details['name']]['notBlank'] = ['rule' => ['notBlank']];
                                $success = false;
                            }
                        }

                        $this->saveAll($data[$this->alias], ['atomic' => false, 'validate' => 'only']);
                    }
                }
            }

            if ($success === false) {
                return $data;
            }

            $data['WebdpoFiche']['soustraitancefields'] = json_encode($data[$this->alias], true);
        }

        return $data;
    }
}