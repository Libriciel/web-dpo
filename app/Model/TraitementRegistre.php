<?php

/**
 * TraitementRegistre
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class TraitementRegistre extends AppModel {

    public $name = 'TraitementRegistre';

    /**
     * belongsTo associations
     * 
     * @var array
     * 
     * @access public
     * @created 04/01/2016
     * @version V0.9.0
     */
    public $belongsTo = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ]
    ];

}
