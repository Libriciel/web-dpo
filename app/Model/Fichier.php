<?php

/**
 * Fichier
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('Folder', 'Utility');

class Fichier extends AppModel {

    public $name = 'Fichier';
    
    public $validationDomain = 'validation';

    /**
     * Règles de validation supplémentaires.
     *
     * @var array
     */
    public $validate = [
        'nom' => [
            'isUniqueMultiple' => [
                'rule' => ['isUniqueMultiple', ['nom', 'fiche_id']],
                'message' => 'validation.valeurNomFichierDejaUtilisee'
            ]
        ],
    ];

    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public $belongsTo = [
        'Fiche' => [
            'className' => 'Fiche',
            'foreignKey' => 'fiche_id'
        ],
        'Typage' => [
            'className' => 'Typage',
            'foreignKey' => 'typage_id'
        ]
    ];
    
    /**
     * @param type $data
     * @param int|null $id
     * @param boolean $transaction La méthode doit-elle gérer elle-même une
     *  transaction (par défaut: true) ?
     * @return boolean
     * 
     * @deprecated since ???
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function saveFichier($data, $id = null, $transaction = true) {
        if (isset($data['Fiche']['fichiers']) && !empty($data['Fiche']['fichiers'])) {
            foreach ($data['Fiche']['fichiers'] as $key => $file) {
                if (!empty($file['name'])) {
                    $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

                    if ($extension == 'odt' || $extension == 'pdf') {
                        $success = true;

                        if (!empty($file['name'])) {
                            if($transaction == true) {
                                $this->begin();
                            }

                            // On verifie si le dossier file existe. Si c'est pas le cas on le cree
                            create_arborescence_files();

                            $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                            $name = $file['name'];
                            if (!empty($file['tmp_name'])) {
                                $url = time();
                                $success = $success && move_uploaded_file($file['tmp_name'], CHEMIN_PIECE_JOINT . $url . $key . '.' . $extension);
                                if ($success) {
                                    $this->create([
                                        'nom' => $name,
                                        'url' => $url . $key . '.' . $extension,
                                        'fiche_id' => $id
                                    ]);
                                    $success = $success && $this->save(null, ['atomic' => false]);
                                }
                            } else {
                                $success = false;
                            }
                        }
                    } else {
                        $success = false;
                    }

                    if ($success) {
                        if($transaction == true) {
                            $this->commit();
                        }
                    } else {
                        if($transaction == true) {
                            $this->rollback();
                        }
                        return false;
                    }
                } else {
                    return true;
                }
            }
        }
        return true;
    }

    /**
     *
     * @param int $idFiche
     * @param boolean $useAllExtensionFiles
     * @param string $uuidDossier
     * @param int $idUser
     * @param array $typages
     * @param boolean $transaction La méthode doit-elle gérer elle-même une
     *  transaction (par défaut: false) ?
     * @return boolean
     */
    public function transfereSave($idFiche, $useAllExtensionFiles, $uuidDossier, $idUser, $typages = [], $transaction = false)
    {
        $success = true;

        if (!empty($uuidDossier)) {
            $dir = CHEMIN_PIECE_JOINT_TMP . $idUser . DS . $uuidDossier;
            $files = $this->scan_dir($dir);

            $this->set('files', $files);

            if (isset($files) && !empty($files)) {
                $success = true;

                if ($transaction == true) {
                    $this->begin();
                }

                // On verifie si le dossier file existe. Si c'est pas le cas on le cree
                $success = $success && create_arborescence_files();

                if ($useAllExtensionFiles == true) {
                    $accepted = Configure::read('allFileAnnexeAcceptedTypes');
                } else {
                    $accepted = ['application/pdf', 'application/vnd.oasis.opendocument.text'];
                }

                foreach ($files as $key => $file) {
                    $extension = pathinfo($file, PATHINFO_EXTENSION);
                    $pathFileTMP = $dir . DS . $file;

                    $mime = mime_content_type($pathFileTMP);

                    if (in_array($mime, $accepted) === true) {
                        $url = time();
                        $newNameFile = $url . $key . '.' . $extension;
                        $newPathFile = CHEMIN_PIECE_JOINT . $newNameFile;

                        $success = $success && rename($pathFileTMP, $newPathFile);

                        $typeFile = null;
                        if (!empty($typages)) {
                            if (array_key_exists('typage_tmp_'.$key, $typages) === true) {
                                $typeFile = $typages['typage_tmp_'.$key];
                            }
                        }

                        if ($success) {
                            $this->create([
                                'nom' => $file,
                                'url' => $newNameFile,
                                'fiche_id' => $idFiche,
                                'typage_id' => $typeFile,
                            ]);
                            $success = $success && $this->save(null, ['atomic' => false]);
                        }
                    }
                }

                if ($success == true) {
                    if ($transaction == true) {
                        $this->commit();
                    }

                    $folder = new Folder($dir);
                    $folder->delete();
                } else {
                    if ($transaction == true) {
                        $this->rollback();
                    }
                }
            }
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * @param int $id
     * @param boolean $transaction La méthode doit-elle gérer elle-même une
     *  transaction (par défaut: false) ?
     * @return boolean
     *
     * @access public
     * @created 26/06/2015
     * @version V1.0.0
     */
    public function deleteFichier($id, $transaction = false) {
        $success = true;

        if ($transaction == true) {
            $this->begin();
        }

        $fichier = $this->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);

        $success = $success && unlink(CHEMIN_PIECE_JOINT . $fichier['Fichier']['url']);
        $success = $success && $this->delete($id);

        if ($success) {
            if ($transaction == true) {
                $this->commit();
            }
            return true;
        } else {
            if ($transaction == true) {
                $this->rollback();
            }
            return false;
        }
    }

    public function scan_dir($dir) {
        $ignored = ['.', '..', '.svn', '.htaccess'];

        $files = [];
        foreach (scandir($dir) as $file) {
            if (in_array($file, $ignored)) continue;
            $files[$file] = filemtime($dir . '/' . $file);
        }

//        ksort($files);
        asort($files);
        $files = array_keys($files);

        return ($files) ? $files : [];
    }

    /**
     * @param $data
     * @param $fiche_id
     * @return bool
     * @throws Exception
     *
     * @access public
     *
     * @created 09/02/2021
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function copyFicheAnnexeFiles($data, $fiche_id)
    {
        $success = true;

        foreach ($data as $key => $file) {
            $oldFile = CHEMIN_PIECE_JOINT . $file['url'];

            $newNameFile = time() . $key . '.' . pathinfo($oldFile, PATHINFO_EXTENSION);
            $newPathFile = CHEMIN_PIECE_JOINT . $newNameFile;

            $success = $success && false !== copy($oldFile, $newPathFile);

            if ($success === true) {
                $this->create([
                    'nom' => $file['nom'],
                    'url' => $newNameFile,
                    'fiche_id' => $fiche_id,
                    'typage_id' => $file['typage_id'],
                ]);
                $success = $success && $this->save(null, ['atomic' => false]);
            }
        }

        return $success;
    }

}
