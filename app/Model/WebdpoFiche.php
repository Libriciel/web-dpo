<?php

/**
 * WebdpoFiche
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('DatabaseDefaultFormatter', 'Database.Utility');

class WebdpoFiche extends AppModel {

    public $name = 'WebdpoFiche';

//    public $validationDomain = 'validation';

    public $useTable = false;

    protected $_schema = [];

    public $validate = [
        // ------------------RT EXTERNE------------------------------------------
        'rt_externe_telephone' => [
            'maxLength' => [
                'rule' => ['maxLength', 15],
                'allowEmpty' => true
            ],
            'phone' => [
                'rule' => ['phone', null, 'fr'],
                'allowEmpty' => true
            ]
        ],
        'rt_externe_fax' => [
            'maxLength' => [
                'rule' => ['maxLength', 15],
                'allowEmpty' => true
            ],
            'phone' => [
                'rule' => ['phone', null, 'fr'],
                'allowEmpty' => true
            ]
        ],
        'rt_externe_email' => [
            [
                'rule' => ['email'],
                'allowEmpty' => true,
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
        'rt_externe_siret' => [
            'luhn' => [
                'rule' => [
                    'luhn',
                    true
                ],
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETNonValide'
            ],
            [
                'rule' => 'numeric',
                'allowEmpty' => true,
                'message' => 'validation.numeroSIRETUniquementComposeChiffre'
            ],
        ],
        'rt_externe_ape' => [
            'minLength' => [
                'rule' => ['minLength', 5],
                'allowEmpty' => true
            ],
            'maxLength' => [
                'rule' => ['maxLength', 5],
                'allowEmpty' => true
            ],
        ],
        'rt_externe_emailresponsable' => [
            'email' => [
                'rule' => ['email'],
                'allowEmpty' => true,
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
        'rt_externe_telephoneresponsable' => [
            'maxLength' => [
                'rule' => ['maxLength', 15],
                'allowEmpty' => true
            ],
            'phone' => [
                'rule' => ['phone', null, 'fr'],
                'allowEmpty' => true
            ]
        ],
        'rt_externe_email_dpo' => [
            [
                'rule' => ['email'],
                'allowEmpty' => true,
                'message' => 'validation.adresseMailNonValide'
            ]
        ],
        'rt_externe_telephonefixe_dpo' => [
            'maxLength' => [
                'rule' => ['maxLength', 15],
                'allowEmpty' => true
            ],
            'phone' => [
                'rule' => ['phone', null, 'fr'],
                'allowEmpty' => true
            ]
        ],
        'rt_externe_telephoneportable_dpo' => [
            'maxLength' => [
                'rule' => ['maxLength', 15],
                'allowEmpty' => true
            ],
            'phone' => [
                'rule' => ['phone', null, 'fr'],
                'allowEmpty' => true
            ]
        ],
        // ---------------------------------------------------------------------
        // ------------------REDACTEUR------------------------------------------
        'declarantpersonnenom' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'declarantpersonneemail' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
//        'declarantservice' => [
//            'notBlank' => [
//                'rule' => ['notBlank']
//            ]
//        ],
        // ---------------------------------------------------------------------
        // ------------------INFORMATION TRAITEMENT-----------------------------
        'outilnom' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'finaliteprincipale' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        // ---------------------------------------------------------------------
        // ------------------INFORMATION TRAITEMENT-----------------------------
        'decisionAutomatisee' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
        'descriptionDecisionAutomatisee' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'decisionAutomatisee', true, ['Oui']]
            ]
        ],
        // ---------------------------------------------------------------------
        // ------------------------------- AIPD --------------------------------
        // @info: usepia est un champ virtuel nécessaire venant du modèle Formulaire
        'ressources_humaines' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'usepia', true, [true]]
            ]
        ],
        'relation_fournisseurs' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'ressources_humaines', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'ressources_humaines', true, ['Oui']]
            ]
        ],
        'gestion_electoral' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'relation_fournisseurs', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'relation_fournisseurs', true, ['Oui']]
            ]
        ],
        'comites_entreprise' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'gestion_electoral', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'gestion_electoral', true, ['Oui']]
            ]
        ],
        'association' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'comites_entreprise', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'comites_entreprise', true, ['Oui']]
            ]
        ],
        'sante_prise_patient' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'association', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'association', true, ['Oui']]
            ]
        ],
        'avocats' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'sante_prise_patient', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'sante_prise_patient', true, ['Oui']]
            ]
        ],
        'greffiers' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'avocats', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'avocats', true, ['Oui']]
            ]
        ],
        'notaires' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'greffiers', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'greffiers', true, ['Oui']]
            ]
        ],
        'collectivites_affaires_scolaires' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'notaires', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'notaires', true, ['Oui']]
            ]
        ],
        'controles_acces' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'collectivites_affaires_scolaires', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'collectivites_affaires_scolaires', true, ['Oui']]
            ]
        ],
        'ethylotests' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'controles_acces', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'controles_acces', true, ['Oui']]
            ]
        ],
        'sante_medicosociaux' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'ethylotests', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'ethylotests', true, ['Oui']]
            ]
        ],
        'donnees_genetiques' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'sante_medicosociaux', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'sante_medicosociaux', true, ['Oui']]
            ]
        ],
        'profils_personnes_gestion_rh' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'donnees_genetiques', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'donnees_genetiques', true, ['Oui']]
            ]
        ],
        'surveiller_constante_employes' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'profils_personnes_gestion_rh', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'profils_personnes_gestion_rh', true, ['Oui']]
            ]
        ],
        'gestion_alertes_sociale_sanitaire' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'surveiller_constante_employes', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'surveiller_constante_employes', true, ['Oui']]
            ]
        ],
        'gestion_alertes_professionnelle' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'gestion_alertes_sociale_sanitaire', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'gestion_alertes_sociale_sanitaire', true, ['Oui']]
            ]
        ],
        'donnees_sante_registre' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'gestion_alertes_professionnelle', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'gestion_alertes_professionnelle', true, ['Oui']]
            ]
        ],
        'profilage_rupture_contrat' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'donnees_sante_registre', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'donnees_sante_registre', true, ['Oui']]
            ]
        ],
        'mutualises_manquements_rupture_contrat' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'profilage_rupture_contrat', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'profilage_rupture_contrat', true, ['Oui']]
            ]
        ],
        'profilage_donnees_externes' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'mutualises_manquements_rupture_contrat', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'mutualises_manquements_rupture_contrat', true, ['Oui']]
            ]
        ],
        'biometriques' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'profilage_donnees_externes', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'profilage_donnees_externes', true, ['Oui']]
            ]
        ],
        'gestion_logements_sociaux' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'biometriques', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'biometriques', true, ['Oui']]
            ]
        ],
        'accompagnement_social' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'gestion_logements_sociaux', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'gestion_logements_sociaux', true, ['Oui']]
            ]
        ],
        'localisation_large_echelle' => [
            'checkNotBlankIf' => [
                'rule' => ['checkNotBlankIf', 'accompagnement_social', true, ['Non']]
            ],
            'checkBlankIf' => [
                'rule' => ['checkBlankIf', 'accompagnement_social', true, ['Oui']]
            ]
        ],
        // ---------------------------------------------------------------------
        'traitement_considere_risque' => [
            'checkBlankIfCountOptionsChecked' => [
                'rule' => ['checkBlankIfCountOptionsChecked', 'criteres', 0]
            ],
            'checkNotBlankIfCountOptionsChecked' => [
                'rule' => ['checkNotBlankIfCountOptionsChecked', 'criteres', 1]
            ],
            'checkBlankIfCountOptionsChecked' => [
                'rule' => ['checkBlankIfCountOptionsChecked', 'criteres', 2]
            ]
        ],
        // ---------------------------------------------------------------------
    ];

    public function saveWithVirtualFields(
        $data,
        $champs,
        $formulaireOptions,
        $formulaireOLD,
        $actionAdd,
        $initialisationMode = false,
        $initialisationRecu = false
    ) {
        $success = true;
        $Fiche = ClassRegistry::init('Fiche');

        unset($data['Fiche']['soustraitant']);

        // champ virtuel pour la régle de validation
        $data['Fiche']['usepia'] = $formulaireOptions['Formulaire']['usepia'];
        $data['Fiche']['formulaireOLD'] = $formulaireOLD;

        if ($initialisationMode === true) {
            unset($this->validate['ressources_humaines']['checkNotBlankIf']);

            $data['Fiche']['valide'] = false;
        }

        // Sauvegarde BDD Fiches
        $Fiche->create($data['Fiche']);
        $success = false !== $Fiche->save(null, ['atomic' => false]) && $success;

        // Check Transfere hors UE
        $data = $this->cleanAndCheckDataHorsUe($data, $initialisationMode);

        // Check données sensibles
        $data = $this->cleanAndCheckDataDonneesSensibles($data, $initialisationMode);

        // RT
        $data = $this->cleanAndCheckDataRT($data, $initialisationMode);

        // Co-responsable
        $WebdpoCoresponsable = ClassRegistry::init('WebdpoCoresponsable');
        $success = false !== $WebdpoCoresponsable->coresponsableSave(
            $Fiche->id,
            $data['Coresponsable'],
            $data['Fiche']['coresponsable']
        ) && $success;
        $data = $WebdpoCoresponsable->cleanAndCheckDataCoresponsable($data, $champs);

        // Soustraitance
        $WebdpoSoustraitance = ClassRegistry::init('WebdpoSoustraitance');
        $success = false !== $WebdpoSoustraitance->soustraitanceSave(
            $Fiche->id,
            $data['Soustraitance'],
            $data['Fiche']['soustraitance']
        ) && $success;
        $data = $WebdpoSoustraitance->cleanAndCheckDataSoustraitance($data, $champs);

        $newDataChamps = $this->checkDataConditons($data, $champs);
        $data = Hash::extract($newDataChamps, 'data');
        $champs = Hash::extract($newDataChamps, 'champs');

        foreach ($champs as $champ) {
            $details = json_decode(Hash::get($champ, 'Champ.details'));
            if ($details->obligatoire == true) {
                $exit = false;

                if ($champ['Champ']['champ_coresponsable'] === true && $data['Fiche']['coresponsable'] == false ||
                    $champ['Champ']['champ_soustraitant'] === true && $data['Fiche']['soustraitance'] == false
                ) {
                    $exit = true;
                    unset($data['Valeur'][$details->name]);
                }

                if ($exit === false && $initialisationMode === false) {
                    if (isset($this->validate[$details->name]) == false) {
                        $this->validate[$details->name] = [];
                    }

                    if (in_array(Hash::get($champ, 'Champ.type'), ['checkboxes', 'multi-select'])) {
                        $this->validate[$details->name]['multiple'] = ['rule' => ['multiple', ['min' => 1]]];
                    } else {
                        $this->validate[$details->name]['notBlank'] = ['rule' => ['notBlank']];
                    }
                }
            }
        }

        $prefixRtSt = 'rt_';
        if ($data['Fiche']['rt_externe'] === true) {
            $prefixRtSt = 'st_';
        }

        $fieldsToKeep = [
            $prefixRtSt.'organisation_raisonsociale',
            $prefixRtSt.'organisation_telephone',
            $prefixRtSt.'organisation_fax',
            $prefixRtSt.'organisation_adresse',
            $prefixRtSt.'organisation_email',
            $prefixRtSt.'organisation_sigle',
            $prefixRtSt.'organisation_siret',
            $prefixRtSt.'organisation_ape',
            $prefixRtSt.'organisation_civiliteresponsable',
            $prefixRtSt.'organisation_prenomresponsable',
            $prefixRtSt.'organisation_nomresponsable',
            $prefixRtSt.'organisation_fonctionresponsable',
            $prefixRtSt.'organisation_emailresponsable',
            $prefixRtSt.'organisation_telephoneresponsable',
            $prefixRtSt.'organisation_nom_complet_dpo',
            $prefixRtSt.'organisation_numerodpo',
            $prefixRtSt.'organisation_telephonefixe_dpo',
            $prefixRtSt.'organisation_emaildpo',
            $prefixRtSt.'organisation_telephoneportable_dpo'
        ];

        if ($actionAdd === false) {
            $fieldsToKeep = array_merge ($fieldsToKeep, [
                'declarantpersonnenom',
                'declarantpersonneportable',
                'declarantpersonneemail',
                'declarantpersonnefix'
            ]);

//            if ($initialisationRecu === false) {
//                $fieldsToKeep = array_merge ($fieldsToKeep, [
//                    'declarantservice',
//                ]);
//            }
        }

        $data = $this->cleanupData($data, $champs, $formulaireOLD, $fieldsToKeep);
        $data[$this->alias]['usepia'] = $formulaireOptions['Formulaire']['usepia'];

        $this->data[$this->alias] = $data[$this->alias];

        $dynamicValidationErrors = [];
        $originalData = $this->data;
        $originalValidationErrors = $this->validationErrors;
        foreach (['horsue', 'donneessensibles'] as $fieldIndex) {
            if (isset($originalData[$this->alias][$fieldIndex]) === true && empty($originalData[$this->alias][$fieldIndex]) === false) {
                foreach ($originalData[$this->alias][$fieldIndex] as $key => $organisme) {
                    $dynamicData = [
                        $this->alias => $organisme +
                            [
//                                'coresponsable' => $originalData[$this->alias]['coresponsable']
                                'coresponsable' => $data['Fiche']['coresponsable']
                            ]
                    ];

                    // Ajout de la valeur des champs pour les règles de validation dynamiques checkNotBlankIf
                    foreach (array_keys($organisme) as $tmp) {
                        $validate = (array)Hash::get($this->validate, $tmp);
                        foreach ($validate as $validationRule) {
                            if (isset($validationRule['rule'][0]) === true && $validationRule['rule'][0] === 'checkNotBlankIf') {
                                $dynamicData[$this->alias][$validationRule['rule'][1]] = $originalData[$this->alias][$validationRule['rule'][1]];
                            }
                        }
                    }

                    $this->create($dynamicData);
                    if ($this->validates() !== true) {
                        foreach ($this->validationErrors as $idx => $errors) {
                            $dynamicValidationErrors[$fieldIndex][$key][$idx] = $errors;
                        }
                    }
                }
            }
        }
        $this->data = $originalData;
        $this->validationErrors = $originalValidationErrors;

        $virtualFieldsValidates = [
            'rt_externe_telephone',
            'rt_externe_fax',
            'rt_externe_siret',
            'rt_externe_telephoneresponsable',
            'rt_externe_telephonefixe_dpo',
            'rt_externe_telephoneportable_dpo'
        ];
        foreach ($virtualFieldsValidates as $fieldname) {
            if (isset($this->data[$this->alias][$fieldname]) === true) {
                $this->data[$this->alias][$fieldname] = DatabaseDefaultFormatter::formatStripNotAlnum($this->data[$this->alias][$fieldname]);
            }
        }

        $this->create([$this->alias => $this->data[$this->alias]]);
        $success = $this->validates() && $success && empty($dynamicValidationErrors) === true;

        $validationErrors = $Fiche->validationErrors + $this->validationErrors;
        $this->validationErrors = Hash::merge($this->validationErrors, $dynamicValidationErrors);

        if ($actionAdd === false) {
            $idsToDelete = array_keys($Fiche->Valeur->find('list', [
                'conditions' => [
                    'champ_name !=' => $fieldsToKeep,
                    'fiche_id' => $Fiche->id
                ],
                'contain' => false
            ]));

            if (empty($idsToDelete) == false) {
                $success = $Fiche->Valeur->deleteAll([
                        'Valeur.id' => $idsToDelete
                ]) && $success;
            }
        }

        unset($this->data[$this->alias]['usepia']);

        foreach($this->data[$this->alias] as $fieldName => $value) {
            if ($fieldName != 'formulaire_id' && ($value != '' || in_array($fieldName, $this->validate))) {
                if (is_array($value)) {
                    $value = json_encode($value);
                }

                $Fiche->Valeur->create([
                    'champ_name' => $fieldName,
                    'fiche_id' => $Fiche->id,
                    'valeur' => $value
                ]);

                $success = false !== $Fiche->Valeur->save(null, ['atomic' => false]) && $success;
                $Fiche->Valeur->validationErrors = [];
            }
        }

        return $success;
    }

    protected function cleanupData(array $data, array $formulaireChamps, bool $formulaireOLD, array $fieldsToDelete)
    {
        // @TODO faire cela au moment de la génération
        // @TODO passer le champ en column dans fiches
        // Add value textuel en fonction
        if (isset($data[$this->alias]['decisionAutomatisee']) === false) {
            $data[$this->alias]['decisionAutomatisee'] = 'Non';
        }

        // Suppression de la valeur de décrivant la prise de décision automatique si cela n'a pas été indiquer à Oui
        if ($data[$this->alias]['decisionAutomatisee'] === 'Non') {
            unset($data[$this->alias]['descriptionDecisionAutomatisee']);
        }

        // Suppression des sous-finalité vide avant enregistrement en BDD
        if (isset($data[$this->alias]['sousFinalite']) === true) {
            foreach ($data[$this->alias]['sousFinalite'] as $key => $sousFinalite) {
                if (empty($sousFinalite)) {
                    unset($data[$this->alias]['sousFinalite'][$key]);
                }
            }
        }

        foreach ($fieldsToDelete as $field) {
            if (isset($data[$this->alias][$field]) == true) {
                unset($data[$this->alias][$field]);
            }
        }

        return ($data);
    }

    public function checkNotBlankIfCountOptionsChecked($checks, $reference, $nbOptionsChecked)
    {
        if (!(is_array($checks) && !empty($reference))) {
            return false;
        }

        $success = true;
        $referenceValue = Hash::get($this->data, "{$this->alias}.{$reference}");

        if (!empty($checks)) {
            if (empty($referenceValue)) {
                $countOptionsChecked = 0;
            } else {
                $countOptionsChecked = count($referenceValue);
            }

            if ($countOptionsChecked == $nbOptionsChecked) {
                foreach ($checks as $value) {
                    $success = Validation::notBlank($value) && $success;
                }
            }
        }

        return $success;
    }

    public function checkBlankIfCountOptionsChecked($checks, $reference, $nbOptionsChecked)
    {
        if (!(is_array($checks) && !empty($reference))) {
            return false;
        }

        $success = true;
        $referenceValue = Hash::get($this->data, "{$this->alias}.{$reference}");

        if (!empty($checks)) {
            if (empty($referenceValue)) {
                $countOptionsChecked = 0;
            } else {
                $countOptionsChecked = count($referenceValue);
            }

            if ($countOptionsChecked >= $nbOptionsChecked) {
                foreach ($checks as $value) {
                    $success = Validation::blank($value) && $success;
                }
            }
        }

        return $success;
    }

    /**
     * On vérifie que les champs dans "data" soit en adéquation avec les conditions créées sur les champs
     *
     * @access protected
     *
     * @param $data
     * @param $champs
     * @return array
     *
     * @created 15/04/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function checkDataConditons($data, $champs)
    {
        foreach ($champs as $key => $champ) {
            $options = json_decode($champ['Champ']['details'], true);

            if (isset($options['conditions']) && !empty($options['conditions'])) {
                $fieldConditions = json_decode($options['conditions'], true);

                foreach ($fieldConditions as $fieldCondition) {
                    $data_merge = array_merge($data[$this->alias], $data['Fiche']);

                    if (isset($fieldCondition['ifTheField']) &&
                        isset($data_merge[$fieldCondition['ifTheField']])
                    ) {
                        $valueCondition = '';

                        if (is_array($data_merge[$fieldCondition['ifTheField']])) {
                            // champ checkbox, multi-select
                            $goodCondition = false;

                            foreach ($data_merge[$fieldCondition['ifTheField']] as $valueSelected) {
                                if (in_array($fieldCondition['ifTheField'].$valueSelected, $fieldCondition['hasValue'])) {
                                    $goodCondition = true;
                                }
                            }

                            if ($goodCondition === true) {
                                $valueCondition = $fieldCondition['mustBe'];
                            } else {
                                $valueCondition = $fieldCondition['ifNot'];
                            }

                        } else {
                            // input, textearea, radio, deroulant
                            if ($data_merge[$fieldCondition['ifTheField']] == $fieldCondition['hasValue']) {
                                $valueCondition = $fieldCondition['mustBe'];
                            } else {
                                $valueCondition = $fieldCondition['ifNot'];
                            }
                        }

                        if ($valueCondition == 'hidden' && isset($data[$this->alias][$fieldCondition['thenTheField']])) {
                            // On supprimer le champ qui doit être cacher par la condition
                            unset($data[$this->alias][$fieldCondition['thenTheField']]);

                            if ($options['obligatoire'] == true) {
                                $options['obligatoire'] = false;
                                $champs[$key]['Champ']['details'] = json_encode($options);
                            }
                        }
                    } else {
                        // On supprimer le champ qui doit être cacher par la condition
                        unset($data[$this->alias][$fieldCondition['thenTheField']]);

                        if ($options['obligatoire'] == true) {
                            $options['obligatoire'] = false;
                            $champs[$key]['Champ']['details'] = json_encode($options);
                        }
                    }
                }
            }
        }

        $newDataChamps = [
            'data' => $data,
            'champs' => $champs
        ];

        return ($newDataChamps);
    }

    protected function cleanAndCheckDataRT($data, $initialisationMode)
    {
        $valuesRequiredRT = [
            'rt_externe_raisonsociale',
            'rt_externe_adresse',
            'rt_externe_siret',
            'rt_externe_ape',
            'rt_externe_civiliteresponsable',
            'rt_externe_prenomresponsable',
            'rt_externe_nomresponsable',
            'rt_externe_fonctionresponsable',
            'rt_externe_emailresponsable'
        ];

        $valueRT = [
            'rt_externe_telephone',
            'rt_externe_fax',
            'rt_externe_email',
            'rt_externe_telephoneresponsable',
            'rt_externe_civility_dpo',
            'rt_externe_prenom_dpo',
            'rt_externe_nom_dpo',
            'rt_externe_numerocnil_dpo',
            'rt_externe_email_dpo',
            'rt_externe_telephonefixe_dpo',
            'rt_externe_telephoneportable_dpo',
        ];

        if ($data['Fiche']['rt_externe'] === true && $initialisationMode === false) {
            foreach ($valuesRequiredRT as $valueRequiredRT) {
                //if (!isset($data['WebdpoFiche'][$valueRequiredRT]) || empty($data['WebdpoFiche'][$valueRequiredRT])) {
                    $this->validate[$valueRequiredRT] = ['notBlank' => ['rule' => ['notBlank']]] + (isset($this->validate[$valueRequiredRT]) ? $this->validate[$valueRequiredRT] : []);
                //}
            }
        } else {
            $valueRT = array_merge($valueRT, $valuesRequiredRT);

            foreach ($valueRT as $value) {
                if (array_key_exists($value, $data['WebdpoFiche']) === true && $initialisationMode === false) {
                    unset($data['WebdpoFiche'][$value]);
                }
            }
        }

        return ($data);
    }

    protected function cleanAndCheckDataHorsUe($data, $initialisationMode)
    {
        // Suppression des valeurs associées aux transfère hors UE si cela n'a pas été indiquer dans les infos générale
        if ($data['Fiche']['transfert_hors_ue'] == false) {
            unset($data[$this->alias]['horsue']);

            return $data;
        }

        if ($initialisationMode === false) {
            foreach (['organismeDestinataireHorsUe', 'paysDestinataireHorsUe', 'typeGarantieHorsUe'] as $fieldName) {
                $this->validate[$fieldName]['notBlank'] = ['rule' => ['notBlank']];
            }
        }

        return $data;
    }

    protected function cleanAndCheckDataDonneesSensibles($data, $initialisationMode)
    {
        // Suppression des valeurs associées aux données sensible si cela n'a pas été indiquer dans les infos générale
        if ($data['Fiche']['donnees_sensibles'] == false) {
            unset($data[$this->alias]['donneessensibles']);

            return $data;
        }

        if ($initialisationMode === false) {
            foreach (['typeDonneeSensible', 'descriptionDonneeSensible', 'dureeConservationDonneeSensible'] as $fieldName) {
                $this->validate[$fieldName]['notBlank'] = ['rule' => ['notBlank']];
            }
        }

        return $data;
    }

    public function beforeValidate($options = array()) {
//        $this->data = $this->cleanupData($this->data);
        return parent::beforeValidate($options);
    }
}
