<?php

/**
 * Authentification
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');

class Authentification extends AppModel {

    public $name = 'Authentification';
    
    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 21/03/2018
     * @version V1.0.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];
    
    /**
     * @deprecated v2.1.0 07/04/2021
     * pb avec la fonction "CAS". Suppression de l'option de connexion "CAS" le temps de passer en v3
     *
     * @param type $data
     * @param int|null $id
     * @return boolean
     * 
     * @access public
     *
     * @created 21/03/2018
     * @version V1.0.0
     */
//    public function saveFile($data, $id) {
//        if (isset($data) && !empty($data)) {
//            $file = $data;
//            $success = true;
//
//            $extension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
//
//            if ($extension == 'odt') {
//                if (!empty($file['name'])) {
//                    // On verifie si le dossier file existe. Si c'est pas le cas on le cree
//                    create_arborescence_files();
//
//                    if (!empty($file['tmp_name'])) {
//                        $url = time();
//                        $nameFileServer = $url . '.' . $extension;
//
//                        $success = $success && move_uploaded_file($file['tmp_name'], CHEMIN_CONNECTEURS . $nameFileServer);
//
//                        if ($success) {
//                            $success = $this->updateAll(
//                                ['Authentification.name_fichier' => "'$nameFileServer'"],
//                                ['Authentification.id' => $id]
//                            );
//                        }
//                    } else {
//                        $success = false;
//                    }
//                } else {
//                    $success = false;
//                }
//
//                return $success ? (true) : (false);
//            } else {
//                return (2);
//            }
//        }
//
//        return (3);
//    }
    
}
