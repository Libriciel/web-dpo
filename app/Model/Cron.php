<?php

/**
 * Crons
 *
 * Tâches planifiées
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('AppTools', 'Lib');
App::uses('CakeTime', 'Utility');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Cron extends AppModel implements LinkedOrganisationInterface {
    
    public $name = 'Cron';
    
    // status d'exécution
    const EXECUTION_STATUS_SUCCES = 'SUCCES';
    const EXECUTION_STATUS_WARNING = 'WARNING';
    const EXECUTION_STATUS_FAILED = 'FAILED';
    const EXECUTION_STATUS_LOCKED = 'LOCKED';
    
    // status sous forme de chaines de caractères a insérer dans le rapport d'exécution des procédures appelées par les crons
    const MESSAGE_FIN_EXEC_SUCCES = 'TRAITEMENT_TERMINE_OK';
    const MESSAGE_FIN_EXEC_WARNING = 'TRAITEMENT_TERMINE_ALERTE';
    const MESSAGE_FIN_EXEC_ERROR = 'TRAITEMENT_TERMINE_ERREUR';
    
    /**
     * belongsTo associations
     *
     * @var array
     *
     * @access public
     * @created 21/03/2018
     * @version V1.0.0
     */
    public $belongsTo = [
        'Organisation' => [
            'className' => 'Organisation',
            'foreignKey' => 'organisation_id'
        ]
    ];
    
//    public function beforeSave($options = []) {
//        $tableau_execution_duration = [
//            'day' => $this->data[$this->alias]['jours_execution_duration'],
//            'hour' => $this->data[$this->alias]['heures_execution_duration'],
//            'minute' => $this->data[$this->alias]['minutes_execution_duration'],
//        ];
//        
//        $execution_duration = AppTools::arrayToDuration($tableau_execution_duration);
//        
//        if (!empty($execution_duration)){
//            $this->data[$this->alias]['execution_duration'] = $execution_duration;
//            
//            return true;
//        } else {
//            return false;
//        }
//    }
    
    /**
     * Retourne la prochaine date d'exécution pour le cron en fonction de l'id
     *
     * @param integer|array $id --> identifiant ou tableau de données de l'enregistrement
     * @return string --> date-heure de la prochaine exécution sous forme 'Y-m-d H:i:s'
     * 
     * @version v1.0.0
     * @access public
     */
    public function calcNextExecutionTime($id) {
        // initialisations
        $now = date('Y-m-d H:i:s');
        $nowTimestamp = CakeTime::fromString($now);
        
        // lecture en base ou initialisation de l'archive
        if (is_array($id)) {
            $data = $id;
        } else {
            $data = $this->find('first', [
                'conditions' => [
                    'id' => $id
                ],
                'recursive' => -1
            ]);
        }
        
        if (empty($data['Cron']['next_execution_time'])) {
            $nextExecuteTime = $now;
        } else {
            $nextExecuteTime = $data['Cron']['next_execution_time'];
        }
        
        // Si la date d'exection est supérieur alors on n'éxécute la valeur de la tache programmé
        if ($nowTimestamp <= CakeTime::fromString($nextExecuteTime)) {
            return $nextExecuteTime;
        }

        // Récupération de l'échéance prochaine de l'exécution
        $nextDateDuration = AppTools::addSubDurationToDate($nextExecuteTime, $data['Cron']['execution_duration'], 'Y-m-d H:i:s');
        $nextExecuteTimestamp = CakeTime::fromString($nextDateDuration);
         
        //Si l'intervale n'est pas suppérieur à maitenant alors la taches à une date d'éxécution passé.
        if ($nowTimestamp > $nextExecuteTimestamp) {
            $dateNext = new DateTime($nextExecuteTime);
            $dateNow = new DateTime($now);
            $nowDateString = $dateNow->format('Y-m-d') . ' ' . $dateNext->format('H:i:s');
            $nextDateDuration = AppTools::addSubDurationToDate($nowDateString, $data['Cron']['execution_duration'], 'Y-m-d H:i:s');
            $nextExecuteTimestamp = CakeTime::fromString($nextDateDuration);
        }
        
        // Si l'échéance est supérieur à maintenant alors l'échéance prochaine devient la prochaine exécution
        if ($nowTimestamp < $nextExecuteTimestamp) {
            $nextExecuteTime = $nextDateDuration;
        }
        
        return $nextExecuteTime;
    }

    /**
     * Retourne l'id de l'organisation liée à la tâche planifiée (dans un array).
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ["{$this->alias}.{$this->primaryKey}" => $id],
        ];
        return (array)Hash::get($this->find('first', $query), "{$this->alias}.organisation_id");
    }

    /**
     * Retourne une condition permettant de limiter aux enregistrements liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        return $this->getDataSource()->conditions(
            ["{$this->alias}.organisation_id" => $organisation_id],
            true,
            false,
            $this
        );
    }
}
