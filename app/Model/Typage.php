<?php

/**
 * Typage
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Model
 */

App::uses('AppModel', 'Model');
App::uses('LinkedOrganisationInterface', 'Model/Interface');

class Typage extends AppModel implements LinkedOrganisationInterface {

    public $name = 'Typage';
    
    public $validationDomain = 'validation';
    
    /**
     * validate associations
     *
     * @var array
     *
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public $validate = [
        'libelle' => [
            'notBlank' => [
                'rule' => ['notBlank']
            ]
        ],
    ];
    
    /**
     * hasMany associations
     *
     * @var array
     *
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public $hasAndBelongsToMany = [
        'Organisation' => [
            'className' => 'Organisation',
            'joinTable' => 'typages_organisations',
            'foreignKey' => 'typage_id',
            'associationForeignKey' => 'organisation_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'with' => 'TypageOrganisation'
        ]
    ];

    /**
     * Retourne un champ virtuel contenant le nombre d'entités liées au @fixme.
     *
     * @param string $primaryKeyField | 'Typage.id' --> Champ représentant le Typage.id
     * @param string $fieldName | 'organisations_count' --> Nom du champ virtuel
     * @return string
     */
    public function vfLinkedOrganisationsCount($primaryKeyField = 'Typage.id', $fieldName = 'organisations_count') {
        $subQuery = [
            'alias' => 'typages_organisations',
            'fields' => ['COUNT(DISTINCT(typages_organisations.organisation_id))'],
            'conditions' => [
                "typages_organisations.typage_id = {$primaryKeyField}"
            ],
            'contain' => false
        ];
        $sql = $this->TypageOrganisation->sql($subQuery);
        return "( {$sql} ) AS \"{$this->alias}__{$fieldName}\"";
    }

    /**
     * Retourne les id des entités liées au type d'annexe.
     *
     * @param int $id
     * @return array
     */
    public function getLinkedOrganisationsIds($id) {
        $query = [
            'fields' => ['organisation_id'],
            'conditions' => ['typage_id' => $id],
        ];
        return Hash::extract(
            $this->TypageOrganisation->find('all', $query),
            '{n}.TypageOrganisation.organisation_id'
        );
    }

    /**
     * Retourne une condition permettant de limiter aux types d'annexes liés à une ou plusieurs entités.
     *
     * @param array|int $organisation_id
     * @return string
     */
    public function getConditionOrganisation($organisation_id) {
        $query = [
            'alias' => 'typages_organisations',
            'fields' => [
                'typages_organisations.typage_id'
            ],
            'conditions' => [
                'typages_organisations.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->TypageOrganisation->sql($query);
        return "{$this->alias}.{$this->primaryKey} IN ( {$sql} )";
    }
}
