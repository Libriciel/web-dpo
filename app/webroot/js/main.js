let makeUsersDeroulant = function() {
    try {
        let selectorClass = ".chosen-select, .usersDeroulant";

        $(selectorClass).chosen({
            no_results_text: "Aucun résultat trouvé pour",
            allow_single_deselect: true,
            width: "100%",
            search_contains: true
        });

        $(selectorClass).on("change", function (event) {
            try {
                $(event.target).trigger("chosen:updated");
            } catch(e) {
                console.error(e);
            }
        });
    } catch(exception) {
        console.log(exception);
    }
};

let correctRowspanHover = function() {
    let rowsWithRowspan = $('.table-striped tr').filter(function() {
        let tr = $(this);
        return $(tr).find('td, th').filter(function() {
            let cell = $(this);
            return $(cell).attr('rowspan') > 1;
        }).length > 0;
    });
    $(rowsWithRowspan).each(function(idx, tr) {
        let nexts = [],
            current = tr,
            currentClass = '';

        if ($(tr).hasClass('even')) {
            currentClass = 'even';
        } else if ($(tr).hasClass('odd')) {
            currentClass = 'odd';
        }

        if (currentClass !== '') {
            while(($(current).next().hasClass('even') && currentClass === 'even') || ($(current).next().hasClass('odd') && currentClass === 'odd')) {
                nexts.push($(current).next());
                current = $(current).next();
            }

            $(tr).hover(
                // Handler in
                function() {
                    $(nexts).each(function() { $(this).addClass('hover'); });
                },
                // Handler out
                function() {
                    $(nexts).each(function() { $(this).removeClass('hover'); });
                }
            );
            $(nexts).each(function(idx, next) {
                $(next).hover(
                    // Handler in
                    function() {
                        $(tr).addClass('hover');
                        $(nexts).each(function() { $(this).addClass('hover'); });
                    },
                    // Handler out
                    function() {
                        $(tr).removeClass('hover');
                        $(nexts).each(function() { $(this).removeClass('hover'); });
                    }
                );
            });
        }
    });
}

$(document).ready(function () {
    $('.multi-select2').select2({
        language: "fr",
        placeholder: 'Sélectionnez une ou plusieurs options',
        allowClear: true,
        width: '100%'
    });

    $('.reponseStartTinyMCE').tinymce({
        script_url : '/js/node_modules/tinymce/tinymce.min.js',
        language_url : '/js/node_modules/tinymce-i18n/langs/fr_FR.js',
        language : 'fr_FR',
        mode : 'textareas',
        editor_selector: 'reponseStartTinyMCE',
        height : 150,
        menubar : false,
        statusbar : true,
        plugins: [
            'lists',
            'link',
            'autolink'
        ],
        toolbar: 'fontsizeselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link unlink',
    });

	try {
		$('#modalNotif').modal();
	} catch(exception) {
		console.log(exception);
	}

    makeUsersDeroulant();

	try {
		$('.my-tooltip').tooltip({
			delay: {
			    'show': 800,
                'hide': 0
            },
			container: 'body'
		});
	} catch(exception) {
		console.log(exception);
	}

    $('[id^="collapse"]').on('shown.bs.collapse', function () {
        let coucou = $(this).attr('id');
        $('html, body').animate({
            scrollTop: $('#' + coucou).offset().top - 100
        }, 'fast');
        return false;
    });

    $('.btn-del-file').click(function () {
        let value = $(this).attr('data');
        $('.tr-file-' + value).css('color', '#d3d3d3');
        $('.boutonsFile' + value).hide();
        $('.boutonsFileHide' + value).show();
        let newElement = jQuery('<input type="hidden" class="hiddenFile' + value + '" name="delfiles[]" value="' + value + '"/>');
        $('.hiddenfields').append(newElement);
    });

    $('.btn-cancel-file').click(function () {
        let value = $(this).attr('data');
        $('.tr-file-' + value).css('color', '#000000');
        $('.boutonsFileHide' + value).hide();
        $('.boutonsFile' + value).show();
        $('.hiddenFile' + value).remove();

    });

    $('.btn-edit-registre').click(function () {
        let id = $(this).attr('data');
        $('#idEditRegistre').val(id);
    });

    $('.btn-insert-registre').click(function () {
        let id = $(this).attr('data-id');
        $('#idFiche').val(id);
        
        let normeId = $(this).attr('data-norme-id');
        $('#norme').val(normeId).trigger('chosen:updated');

        if ($(this).attr('data-type') === 'false'){
            $('#typedeclaration').parent().parent().show();
        } else{
            $('#typedeclaration').parent().parent().hide();
        }

    });

    $('.btn_envoyerValideur').click(function () {
        let id = $(this).attr('data-id');
        $('#ficheNumVal').val(id);

        let fiche = $(this).attr('data-fiche');
        $('#etatFicheVal').val(fiche);
    });

    $('.btn_envoyerConsultation').click(function () {
        let id = $(this).attr('data-id');
        $('#ficheNumCons').val(id);

        let fiche = $(this).attr('data-fiche');
        $('#etatFicheCons').val(fiche);
    });

    $('.btn_ReorienterTraitement').click(function () {
        let id = $(this).attr('data-id');
        $('#ficheNumReo').val(id);

        let fiche = $(this).attr('data-fiche');
        $('#etatFicheReo').val(fiche);
    });

    $('.btn-upload-modele').click(function () {
        let id = $(this).attr('data');
        $('#idUploadModele').val(id);
    });

    $('.btn_DuplicationTraitementOrganisations').click(function () {
        let id = $(this).attr('data-id');
        $('#fiche_id').val(id);
    });

    // Défauts pour le plugin jQuery filestyle
    $.fn.filestyle.defaults['text'] = 'Parcourir';
    $.fn.filestyle.defaults['htmlIcon'] = '<i class="far fa-folder-open fa-lg"></i>';
    $.fn.filestyle.defaults['btnClass'] = 'btn btn-outline-primary';

    correctRowspanHover();
});

function openTarget(idFicheNotification) {
    let scrollToTarget = function (idFicheNotification) {
        $('html, body').animate({scrollTop: $('#btn_ajax_historique' + idFicheNotification).offset().top - 90}, 1000);
        if (idFicheNotification !== 0) {
            $('#btn_ajax_historique' + idFicheNotification).click();
        }
    };
    
    if (idFicheNotification) {
        scrollToTarget(idFicheNotification);
    }
}

function verificationExtension() {
    $('#fileAnnexe').change(function () {
        let annexes = this.files;

        for (let i = 0; i< annexes.length; i++ ) {
            let tmpName = annexes[i]['name'];

            if (tmpName !== '') {
                //suppression du fakepath ajouter par webkit
                tmpName = tmpName.replace("C:\\fakepath\\", "");

                //verification de l'extension odt OU pdf
                let ctrlName = tmpName.split('.');

                if (ctrlName[ctrlName.length - 1] !== 'odt' && ctrlName[ctrlName.length - 1] !== 'pdf') {
                    $(this).val('');
                    $('#errorExtentionAnnexe').modal('show');
                }
            }
        }
    });
}

let WebcilForm = {
	chosenFields: function(form) {
		let selects;
		if (true === $.isFunction( $.fn.chosen )) {
			try {
				selects = $(form).find('select').chosen();
			} catch(exception) {
				selects = [];
			}
		} else {
			selects = [];
		}

		return $(selects);
	},
	reset: function(event) {
		let form = $(event.target).closest('form'),
			typesWithValue = ['color',
                'date',
                'datetime',
                'email',
                'month',
                'number',
                'password',
                'range',
                'research',
                'search',
                'tel',
                'text',
                'url',
                'week'
            ];
			typesWithChecked = ['checkbox', 'radio'];
			fieldsWithValue = $(form).find(
			    'input[type="'+typesWithValue.join('"], input[type="')+'"]'+', select, textarea'
            );
			fieldsWithChecked = $(form).find(
			    'input[type="'+typesWithChecked.join('"], input[type="')+'"]'
            );

		$(fieldsWithValue).val('');
		$(fieldsWithValue).trigger('change');

		$(fieldsWithChecked).prop('checked', false);
		$(fieldsWithValue).trigger('click');

		// On indique au plugin chosen qu'il doit mettre à jour les champs
		WebcilForm.chosenFields(form).trigger('chosen:updated');

		event.preventDefault();
		event.stopPropagation();
	},
	init: function(form) {
		$(form).find('.search-reset').bind('click', WebcilForm.reset);

		WebcilForm.chosenFields(form).each(function(idx, select){
			let chosen = $('#'+$(select).attr('id')+'_chosen'),
				span = $(chosen).find('a.chosen-single.chosen-default span');

			$(select).on('change', function(event, params) {
				if('undefined' === typeof params) {
					$(span).addClass('text-muted');
				} else {
					$(span).removeClass('text-muted');
				}
			});

			$(span).addClass('text-muted');
		});
	}
};

$(function(){
	$('form.search-form').each(function(idx, form){
	    WebcilForm.init(form);
	});
});

/*
La touche "Entree" du clavier n'a plus d'action
sauf sur les grand champs texte pour aller à la ligne
*/
$(document).on('keydown', ':input:not(textarea)', function(event) {
    if (event.key == 'Enter') {
        event.preventDefault();
    }
});
// document.addEventListener("keydown", function(event) {
//     if (event.keyCode == 13) {
//         event.preventDefault();
//         return false;
//     }
// }, true);
