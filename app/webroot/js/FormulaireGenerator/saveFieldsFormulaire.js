function getVirtualFields(showErrors = true)
{
    let success = true,
        dynamicFields = [],
        errorTabId = null,
        liTabActive = $('#addAndEditForm li a.active').attr("href"),
        divTabActive = $('#addAndEditForm div.active').attr('id');

    $('#addAndEditForm input[type="hidden"][name^="data[Formulaire][form-container-"]').remove();

    $('.ui-droppable').each(function(idxDroppable, droppable) {
        if (success === false) {
            return;
        }

        let droppableId = $(droppable).attr('id'),
            idTab = '#'+$(droppable).parent().attr('id');

        errorTabId = idTab;

        $(idTab).addClass('active');

        $(droppable).find('.draggable').each(function (idxDraggable, draggable) {
            let objFieldDetails = {},
                objFieldValues = {},
                fieldLine = null,
                fieldColumn = null,
                fieldType = null,
                fieldName = null,
                options = [];

            fieldLine = Math.round($(draggable).position().top / 45 + 1);

            // @TODO
            if ($(this).position().left < 10) {
                fieldColumn = 1;
            } else {
                fieldColumn = 2;
            }

            let alertFormField = function(message) {
                if (showErrors === true) {
                    alert(message);
                    event.preventDefault();
                }
            };

            if ($(draggable).hasClass('small-text')) {
                let fieldDraggable = $(this).find('input'),
                    fieldInputName = $(fieldDraggable).attr('name');

                if ($.trim(fieldInputName).length === 0) {
                    alertFormField('Le nom de variable du petit champ texte est incorrect');
                    return false;
                } else {
                    fieldType = 'input';

                    objFieldDetails['name'] = fieldInputName;
                    objFieldDetails['placeholder'] = $(fieldDraggable).attr('placeholder');
                    objFieldDetails['label'] = $(this).find('.labeler').html();
                    objFieldDetails['default'] = $(fieldDraggable).val();
                }

            } else if ($(draggable).hasClass('long-text')) {
                let fieldTextareaDraggable = $(this).find('textarea'),
                    fieldTextareaName = $(fieldTextareaDraggable).attr('name');

                if ($.trim(fieldTextareaName).length === 0) {
                    alertFormField('Le nom de variable du grand champ texte est incorrect');
                    return false;
                } else {
                    fieldType = 'textarea';

                    objFieldDetails['name'] = fieldTextareaName;
                    objFieldDetails['placeholder'] = $(fieldTextareaDraggable).attr('placeholder');
                    objFieldDetails['label'] = $(this).find('.labeler').html();
                    objFieldDetails['default'] = $(fieldTextareaDraggable).val();
                }

            } else if ($(draggable).hasClass('date')) {
                let fieldDateDraggable = $(this).find('input'),
                    fieldDateName = $(fieldDateDraggable).attr('name');

                if ($.trim(fieldDateName).length === 0) {
                    alertFormField('Le nom de variable du champ date est incorrect');
                    return false;
                } else {
                    fieldType = 'date';

                    objFieldDetails['name'] = fieldDateName;
                    objFieldDetails['placeholder'] = $(fieldDateDraggable).attr('placeholder');
                    objFieldDetails['label'] = $(this).find('.labeler').html();
                    objFieldDetails['default'] = $(fieldDateDraggable).val();
                }

            } else if ($(draggable).hasClass('title')) {
                fieldType = 'title';
                objFieldDetails['content'] = $(this).find('h1').html();

            } else if ($(draggable).hasClass('texte')) {
                fieldType = 'texte';
                objFieldDetails['content'] = $(this).find('h5').html();

            } else if ($(draggable).hasClass('help')) {
                fieldType = 'help';
                objFieldDetails['content'] = $(this).find('.messager').html();

            } else if ($(draggable).hasClass('checkboxes')) {
                let fieldCheckboxeDraggable = $(this).find('input'),
                    fieldCheckboxeName = $(fieldCheckboxeDraggable).attr('name');

                if ($.trim(fieldCheckboxeName).length === 0) {
                    alertFormField('Le nom de variable des cases à cocher est incorrect');
                    return false;
                } else {
                    fieldType = 'checkboxes';

                    objFieldDetails['name'] = fieldCheckboxeName;
                    objFieldDetails['label'] = $(this).find('.labeler').html();

                    options = [];
                    $(this).find('input[type="checkbox"]').each(function () {
                        options.push($(this).next('label').text());
                    });
                    objFieldDetails['options'] = options;

                    let valuesCheckboxesDefault = [];
                    $('input[name="' + fieldCheckboxeName + '"]:checked').each(function () {
                        // valuesCheckboxesDefault.push($(this).value);
                        valuesCheckboxesDefault.push($(this).attr('value'));
                    });
                    objFieldDetails['default'] = valuesCheckboxesDefault;
                }

            } else if ($(draggable).hasClass('radios')) {
                let fieldRadioDraggable = $(this).find('input'),
                    fieldRadioName = $(fieldRadioDraggable).attr('name');

                if ($.trim(fieldRadioName).length === 0) {
                    alertFormField('Le nom de variable du choix unique est incorrect');
                    return false;
                } else {
                    fieldType = 'radios';

                    objFieldDetails['name'] = fieldRadioName;
                    objFieldDetails['label'] = $(this).find('.labeler').html();
                    objFieldDetails['default'] = $(this).find('input[name="' + fieldRadioName + '"]:checked').next('label').text();

                    options = [];
                    $(this).find('input[type="radio"]').each(function () {
                        options.push($(this).next('label').text());
                    });
                    objFieldDetails['options'] = options;
                }

            } else if ($(draggable).hasClass('deroulant')) {
                let fieldDeroulantDraggable = $(this).find('option'),
                    fieldDeroulantName = $(fieldDeroulantDraggable).attr('name');

                if ($.trim(fieldDeroulantName).length === 0) {
                    alertFormField('Le nom de variable du menu déroulant est incorrect');
                    return false;
                } else {
                    fieldType = 'deroulant';

                    objFieldDetails['name'] = fieldDeroulantName;
                    objFieldDetails['label'] = $(this).find('.labeler').html();

                    options = [];
                    $(this).find('option').each(function () {
                        if ($(this).text()) {
                            options.push($(this).text());
                        }
                    });
                    objFieldDetails['options'] = options;

                    let valuesDeroulantDefault = '';
                    $('option[name="' + fieldDeroulantName + '"]:checked').each(function () {
                        if ($(this).value !== '') {
                            valuesDeroulantDefault = this.value;
                        }
                    });
                    objFieldDetails['default'] = valuesDeroulantDefault;
                }

            } else if ($(draggable).hasClass('multi-select')) {
                let fieldMultiDraggable = $(this).find('option'),
                    fieldMultiSelectName = $(fieldMultiDraggable).attr('name');

                if ($.trim(fieldMultiSelectName).length === 0) {
                    alertFormField('Le nom de variable du menu multi-select est incorrect');
                    return false;
                } else {
                    fieldType = 'multi-select';

                    objFieldDetails['name'] = fieldMultiSelectName;
                    objFieldDetails['label'] = $(this).find('.labeler').text();

                    options = [];
                    $(this).find('option').each(function () {
                        if ($(this).text()) {
                            options.push($(this).text());
                        }
                    });
                    objFieldDetails['options'] = options;

                    let valuesMultiSelectDefault = [];
                    $('option[name="' + fieldMultiSelectName + '"]:checked').each(function () {
                        valuesMultiSelectDefault.push(this.index);
                    });
                    objFieldDetails['default'] = valuesMultiSelectDefault;
                }
            }

            if ($(draggable).attr('data-virtual-required') === 'fieldRequired') {
                objFieldDetails['obligatoire'] = true;
            } else {
                objFieldDetails['obligatoire'] = false;
            }

            let dataVirtualConditions = $(draggable).attr('data-virtual-conditions');
            if (dataVirtualConditions) {
                objFieldDetails['conditions'] = dataVirtualConditions
            } else {
                objFieldDetails['conditions'] = ''
            }

            objFieldValues = {
                'type' : fieldType,
                'ligne' : fieldLine,
                'colonne' : fieldColumn,
                'details' : JSON.stringify(objFieldDetails)
            };

            $.each(objFieldValues, function(fieldName, value) {
                let hidden = $('<input type="hidden" name="data[Formulaire]['+droppableId+']['+idxDraggable+'][Champ]['+fieldName+']" value="" />');
                hidden.attr('value', value);
                dynamicFields.push(hidden);
            });
        });
    });

    $('.active').removeClass('active');

    if (success === true) {
        $('#'+divTabActive).addClass('active');
        $('a[href="'+liTabActive+'"]').addClass('active');

        return dynamicFields;
    } else {
        $(errorTabId).addClass('active');
        $('a[href="'+errorTabId+'"]').addClass('active');

        // event.preventDefault();
    }
}

function addVirtualFieldsForm(dynamicFields)
{
    $.each(dynamicFields, function(idx, hidden) {
        $('form').append(hidden);
    });

    return true;
}

$(document).ready(function () {

    $('#container').on('click', ':submit', function() {
        if ($(this).val() === 'Cancel') {
            return ;
        }

        let dynamicFields = getVirtualFields();

        addVirtualFieldsForm(dynamicFields);
    });

});
