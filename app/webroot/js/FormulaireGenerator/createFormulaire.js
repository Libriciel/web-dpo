let createForm = function(typeCreateForm) {
    let idContainer = '#form-container-'+typeCreateForm,
        fieldOptions = '#field-options-'+typeCreateForm,
        idBtnApplicable = 'applicable-'+typeCreateForm,
        idBtnCloser = 'closer-'+typeCreateForm,
        idBtnGroup = 'btn-group-'+typeCreateForm,
        idBtnInput = 'btn-input-'+typeCreateForm,
        fixmeOffset = 45,
        incrementationId = 0;

    // -------------------------------------------------

    jQuery.fn.extend({
        snapToGrid: function() {
            return $(this).each(function() {
                let size = Math.round($(this).closest('.form-container').width() / 2);
                $(this).draggable({
                    containment: 'parent',
                    grid: [size, fixmeOffset],
                    snapTolerance: fixmeOffset
                });
            });
        }
    });

    $(window).resize(function() {
        $(idContainer)
            .find('.draggable')
            .snapToGrid();

        resizeFormContainer();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let target = $(e.target).attr("href");
        $(target)
            .find('.draggable')
            .snapToGrid();

        resizeFormContainer();

        // @info: correction largeur intitulé "placeholder" select2
        $(target + ' .select2-search__field').each(function() {
            let width = $(this).closest('.select2-selection__rendered').width();
            $(this).width(width);
        });
    });

    refresh();

    $(idContainer).resizable({
        handles: 's'
    });

    let repositionFieldOnGrid = function(field) {
        let snapTolerance = $(field).draggable('option', 'snapTolerance'),
            top = (Math.round($(field).position().top) - (Math.round($(field).position().top % snapTolerance))),
            left = (Math.round($(field).position().left + snapTolerance) <= Math.round($(idContainer).width() / 2)) ? 0 : (Math.round(($(idContainer).width() / 2) + 1));

        field.css({ 'left': left + 'px', 'top': top + 'px'});
    };

    $(idContainer).droppable({
        'drop': function (event, ui) {
            repositionFieldOnGrid($(ui.draggable));
        }
    });

    let resizeFormContainer = function () {
        // On cherche l'élément le plus bas du tableau
        let last = null,
            lastPosition = 0,
            // lastHeight = 500,
            lastHeight = parseInt($('.form-container').css('min-height')),
            currentPosition = null,
            currentHeight = null,
            children = $(idContainer).children(),
            newHeight = null;

        $(children).each(function () {
            currentPosition = $(this).offset().top;
            currentHeight = $(this).height();

            if ((currentPosition + currentHeight) >= (lastPosition + lastHeight)) {
                last = $(this);
                lastPosition = currentPosition;
                lastHeight = currentHeight;
            }
        });

        newHeight = (lastPosition + lastHeight - $(idContainer).offset().top) + fixmeOffset;
        newHeight = Math.round(newHeight);

        $(idContainer).css('height', newHeight + 'px');
    };

    resizeFormContainer();

    //--------------------------------------------------------------------------
    let draggableVerticalOffset = function () {
        let last = null,
            lastPosition = 0,
            lastHeight = 0,
            currentPosition,
            currentHeight,
            children = $(idContainer).children('.draggable'),
            top = 0;

        $(children).each(function () {
            currentPosition = Math.round($(this).offset().top);
            currentHeight = Math.round($(this).height());

            if ((currentPosition + currentHeight) >= (lastPosition + lastHeight)) {
                last = $(this);
                lastPosition = currentPosition;
                lastHeight = currentHeight;
            }
        });

        if ((lastPosition + lastHeight) > 0) {
            top = (lastPosition + lastHeight - $(idContainer).offset().top) + fixmeOffset;
        }

        return top;
    };
    //--------------------------------------------------------------------------

    /**
     * Création des éléments du formulaire lors du clic sur un bouton.
     * Pour rajouter des boutons il faut les rajouter sur l'élément HTML
     * et rajouter un case dans le switch
     */
    $('.'+idBtnInput).click(function () {
        let id = $(this).attr('id');

        $(idContainer).find('.ui-selected').removeClass('ui-selected');

        let top = draggableVerticalOffset();
        let newElement = null;

        switch (id) {
            case 'btn-small-text-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 small-text ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Petit champ texte' +
                                '</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-md-12">' +
                            '<input type="text" class="form-control" placeholder="Aide à la saisie"/>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-long-text-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 long-text ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Grand champ texte' +
                                '</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-md-12">' +
                            '<textarea name="" placeholder="Aide à la saisie" class="form-control" cols="30" rows="6"></textarea>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-date-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 date ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Champ date' +
                                '</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-md-12">' +
                            '<input class="form-control" placeholder="jj/mm/aaaa" id="datetimepicker' + incrementationId + '"/>' +
                        '</div>' +
                    '</div>'
                );
                incrementationId++;
                break;

            case 'btn-checkbox-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 checkboxes ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="input select">' +
                            '<label class="col-md-12">' +
                                '<span class="labeler">' +
                                    'Cases à cocher' +
                                '</span>' +
                            '</label>' +
                            '<div class="col-md-12 contentCheckbox">' +
                                '<div class="checkbox">'+
                                'Aucune option sélectionnée'+
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-radio-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 radios ui-selected" data-virtual-required="fieldRequired" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Choix unique' +
                                '</span>' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-md-12 contentRadio">' +
                            'Aucune option sélectionnée' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-deroulant-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 deroulant ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Menu déroulant' +
                                '</span>' +
                            '</label>' +
                            '<select class="transformSelect form-control contentDeroulant" data-placeholder=" ">' +
                                '<option value="">Aucune option sélectionnée</option>' +
                            '</select>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-multi-select-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable col-md-6 form-group multi-select ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12">' +
                            '<label>' +
                                '<span class="labeler">' +
                                    'Menu multi-select' +
                                '</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="col-md-12">' +
                            '<input type="hidden" name="multi-select" value="" id="multi-select_"/>' +
                            '<select name="multi-select" class="form-control multiSelect contentMultiSelect" multiple="multiple">' +
                                '<option>Aucune option sélectionnée</option>' +
                            '</select>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-title-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 title ui-selected text-center" style="left : 0px; top : ' + top + 'px;">' +
                        '<h1>' +
                            'Titre de catégorie' +
                        '</h1>' +
                    '</div>'
                );
                break;

            case 'btn-help-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 help ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<div class="col-md-12 alert alert-info">' +
                            '<div class="col-md-12 text-center">' +
                                '<i class="fa fa-info-circle fa-2x"></i>' +
                            '</div>' +
                            '<div class="col-md-12 messager">' +
                                'Champ d\'information' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
                break;

            case 'btn-texte-'+typeCreateForm:
                newElement = jQuery(
                    '<div class="draggable form-group col-md-6 texte ui-selected" style="left : 0px; top : ' + top + 'px;">' +
                        '<h5>' +
                            'Votre texte' +
                        '</h5>' +
                    '</div>'
                );
                break;

            default:
                break;
        }

        $(idContainer).append(newElement);
        hideDetails();
        displayDetails($(idContainer).find('.ui-selected'));
        refresh();
        resizeFormContainer();

        repositionFieldOnGrid(newElement);
        $(newElement).snapToGrid();

        for (let i = 0; i < incrementationId; i++) {
            $('#datetimepicker' + i).datetimepicker({
                viewMode: 'year',
                startView: 'decade',
                format: 'dd/mm/yyyy',
                minView: 2,
                language: 'fr',
                autoclose: true
            });
        }
    });

    /**
     * Permet de raffraichir les fonctions onClick lors du rajout d'un champ.
     * En son absence, aucun onclick ne sera ajouté
     *
     * @returns {undefined}
     */
    function refresh() {
        let size = null;
        // let position = null;
        let draggableClass = $(idContainer).find('.draggable');

        draggableClass.unbind('click');
        $(idContainer).unbind('click');

        size = Math.round($(idContainer).width() / 2);

        draggableClass.draggable({
            containment: 'parent',
            opacity: 0.70,
            grid: [size, fixmeOffset],
            snapTolerance: fixmeOffset
        });

        draggableClass.click(function () {
            if ($(this).hasClass('ui-selected')) {
                $(this).removeClass('ui-selected');
                hideDetails();
            } else {
                $(idContainer).find('.ui-selected').removeClass('ui-selected');
                hideDetails();
                $(this).addClass('ui-selected');
                displayDetails($(this));
            }
        });

        // $('.draggable').on('dragstop', function (event, ui) {
        //     position = $(this).position();
        //
        //     if (position.left < size / 2) {
        //         $(this).css('left', 0);
        //     } else {
        //         $(this).css('left', size + 1);
        //     }
        //
        //     $(this).css('top', Math.ceil(position.top));
        // });
    }

    /**
     * Options du champ
     *
     * Affiche le formulaire avec les détails à remplir
     * sur chaque champ ajouté lors de la séléction
     *
     * Définition du nom de variable, nom du champ, aide à la saisie,
     * options du champ, champ obligatoire
     *
     * @param {type} object
     * @returns {undefined}
     */
    function displayDetails(object)
    {
        let champId = null,
            options = null,
            check = null,
            readonly = '',
            findType = '',
            displayConditions = '',
            conditions = '';

        let btnGroupsTrashApplicable = '<div class="row">' +
            '<div class="col-md-12 text-center">' +
                '<div class="buttons">' +
                    '<div class="'+idBtnGroup+' text-center">' +
                        '<button type="button" class="btn btn-outline-danger btn-sm" id="'+idBtnCloser+'">' +
                            '<i class="fa fa-trash"></i>' +
                        '</button>' +
                        '<button type="button" class="btn btn-outline-success btn-sm" id="'+idBtnApplicable+'">' +
                            '<i class="fa fa-check"></i> Appliquer' +
                        '</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

        if ($(object).is('.small-text, .long-text, .date, .checkboxes, .radios, .deroulant, .multi-select')) {
            // champ obligatoire
            let fieldRequiredChecked = '';

            if ($(object).attr('data-virtual-required') === 'fieldRequired') {
                fieldRequiredChecked = 'checked="true"';
            }

            check = '<div class="form-check">'
                +'<input class="form-check-input obligForm " type="checkbox" value=""  '+fieldRequiredChecked+' id="checkboxFieldRequired">'
                +'<label class="form-check-label" for="checkboxFieldRequired">'
                    +'Champ obligatoire'
                +'</label>'
            +'</div>';

            // Ecriture des conditions dans les options du champs
            conditions = $(object).attr('data-virtual-conditions');
            if (conditions) {
                displayConditions = writeConditionsInOption(conditions);
            }
        }

        if ($(object).is('.small-text, .long-text, .date')) {
            let attrNameNomDeVariable = null,
                classNameNomDeVariableRequired = '',
                attrPlaceholderAideSaisie = null,
                forLabelNomChamp = null,
                forPlaceholderAideSaisie = null,
                placeholderAideSaisieReadonly = '';

            if ($(object).is('.small-text, .date')) {
                findType = 'input';

                if ($(object).hasClass('small-text')) {
                    champId = 'name-small-text-'+typeCreateForm;
                    forLabelNomChamp = 'label-small-text-'+typeCreateForm;
                    forPlaceholderAideSaisie = 'placeholder-small-text-'+typeCreateForm;

                } else if ($(object).hasClass('date')) {
                    champId = 'name-date-'+typeCreateForm;
                    forLabelNomChamp = 'label-date-'+typeCreateForm;
                    forPlaceholderAideSaisie = 'placeholder-date-'+typeCreateForm;
                    placeholderAideSaisieReadonly = "readonly='readonly'";
                }
            }

            if ($(object).hasClass('long-text')) {
                findType = 'textarea';

                champId = 'name-long-text-'+typeCreateForm;
                forLabelNomChamp = 'label-long-text-'+typeCreateForm;
                forPlaceholderAideSaisie = 'placeholder-long-text-'+typeCreateForm;
            }

            attrPlaceholderAideSaisie = $(idContainer).find('.ui-selected').find(findType).attr('placeholder');
            attrNameNomDeVariable = $(idContainer).find('.ui-selected').find(findType).attr('name');
            if (!attrNameNomDeVariable) {
                attrNameNomDeVariable = '';
            } else {
                classNameNomDeVariableRequired = "readonly='readonly'";
            }

            if ($(idContainer).find('.ui-selected').find(findType).attr('class') === 'form-control champNomVariableReadonly') {
                readonly = "readonly='readonly'";
            }

            options = jQuery('' +
                '<div class="row">' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="'+champId+'">Nom de variable' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                            '<input type="text" class="form-control nameForm"'+readonly+' id="'+champId+'" placeholder="Nom UNIQUE" value="'+attrNameNomDeVariable+'" '+classNameNomDeVariableRequired+'>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                            '<div class="form-group">' +
                            '<label for="'+forLabelNomChamp+'">' +
                                'Nom du champ' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                            '<input type="text" class="form-control labelForm" id="'+forLabelNomChamp+'" placeholder="Label du champ" value="'+$(idContainer).find('.ui-selected').find('.labeler').html()+'">' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="'+forPlaceholderAideSaisie+'">' +
                                'Aide à la saisie' +
                            '</label>' +
                            '<input type="text" class="form-control placeholderForm"'+placeholderAideSaisieReadonly+' id="'+forPlaceholderAideSaisie+'" value="'+attrPlaceholderAideSaisie+'">' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                        check +
                    '</div>' +
                '</div>' +
                displayConditions +
                btnGroupsTrashApplicable
            );
        }

        if ($(object).is('.checkboxes, .radios, .deroulant, .multi-select')) {
            let list = '',
                classNameNomDeVariableRequired = '',
                nom = '',
                forLabelNomChamp = null,
                classListeValeur = '',
                nameNomDeVariable = '',
                forLabelNomDuChamp = '',
                idListeDesValeurs = '';

            if ($(object).is('.checkboxes, .radios')) {
                findType = 'input';

                if ($(object).hasClass('checkboxes')) {
                    champId = 'name-checkboxes-'+typeCreateForm;

                    nameNomDeVariable = 'checkboxes';
                    forLabelNomDuChamp = 'label-checkbox';

                    idListeDesValeurs = 'option-checkbox';
                    classListeValeur = 'checkboxForm';

                    $(idContainer).find('.ui-selected').find(findType).each(function () {
                        if (list === '') {
                            list = list + $(this).next('label').text();
                        } else {
                            list = list + '\n' + $(this).next('label').text();
                        }
                    });

                } else if ($(object).hasClass('radios')) {
                    champId = 'name-radios-'+typeCreateForm;

                    nameNomDeVariable = 'radios';
                    forLabelNomDuChamp = 'label-radios';

                    idListeDesValeurs = 'option-radios';
                    classListeValeur = 'radioForm';

                    check = '<div class="checkbox">' +
                        '<label>' +
                            '<input type="checkbox" id="checkboxFieldRequired" class="obligForm" checked="true" readonly="readonly" disabled="disabled">' +
                            'Champ obligatoire' +
                        '</label>' +
                    '</div>';

                    $(idContainer).find('.ui-selected').find(findType).each(function () {
                        if (list === '') {
                            list = list + $(this).val();
                        } else {
                            list = list + '\n' + $(this).val();
                        }
                    });
                }
            }

            if ($(object).is('.deroulant, .multi-select')) {
                findType = 'option';

                if ($(object).hasClass('deroulant')) {
                    champId = 'name-deroulant';

                    nameNomDeVariable = 'deroulant';
                    forLabelNomDuChamp = 'label-deroulant';

                    idListeDesValeurs = 'option-deroulant';
                    classListeValeur = 'deroulantForm';

                } else if ($(object).hasClass('multi-select')) {
                    champId = 'name-multi-select-'+typeCreateForm;

                    nameNomDeVariable = 'multi-select';
                    forLabelNomDuChamp = 'label-multi-select';

                    idListeDesValeurs = 'option-multi-select';
                    classListeValeur = 'multiSelectForm';
                }

                $(idContainer).find('.ui-selected').find(findType).each(function () {
                    if (list === '') {
                        list = list + $(this).text();
                    } else {
                        list = list + '\n' + $(this).text();
                    }
                });
            }

            if (!$(idContainer).find('.ui-selected').find(findType).attr('name')) {
                nom = '';
            } else {
                nom = $(idContainer).find('.ui-selected').find(findType).attr('name').replace('[]', '');
                classNameNomDeVariableRequired = "readonly='readonly'";
            }

            if ($(idContainer).find('.ui-selected').find(findType).attr('class') === "champNomVariableReadonly") {
                readonly = "readonly='readonly'";
            }

            options = jQuery('' +
                '<div class="row">' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for='+champId+'>Nom de variable' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                            '<input type="text" class="form-control nameForm"'+readonly+' name="'+nameNomDeVariable+'" id="'+champId+'" placeholder="Nom UNIQUE" value="'+nom+'" '+classNameNomDeVariableRequired+'>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="'+forLabelNomDuChamp+'">' +
                                'Nom du champ' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                            '<input type="text" class="form-control labelForm" id="'+forLabelNomDuChamp+'" placeholder="Label du champ" value="' + $(idContainer).find('.ui-selected').find('.labeler').html() + '">' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="'+idListeDesValeurs+'">' +
                                'Liste des options (une option par ligne)' +
                                '<span class="obligatoire"> *</span>' +
                            '</label>' +
                            '<textarea class="form-control '+classListeValeur+'" id="'+idListeDesValeurs+'">' + list + '</textarea>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                        check +
                    '</div>' +
                '</div>' +
                displayConditions +
                btnGroupsTrashApplicable
            );
        }

        if ($(object).hasClass('title')) {
            let labelForContenu = 'content-title';
            let classContenu = 'titleForm';
            let valueContenu = $(idContainer).find('.ui-selected').find('h1').html();

            champId = null;

            options = jQuery('' +
                '<div class="col-md-12">' +
                    '<div class="form-group">' +
                        '<label for="'+labelForContenu+'">' +
                            'Contenu' +
                            '<span class="obligatoire"> *</span>' +
                        '</label>' +
                        '<input type="text" class="form-control '+classContenu+'" name="'+labelForContenu+'" id="'+labelForContenu+'" value="'+valueContenu+'">' +
                    '</div>' +
                '</div>' +
                btnGroupsTrashApplicable
            );
        }

        if (object.is('.texte, .help')) {
            let classContenu = '',
                valueContenu = null,
                labelForContenu = '';

            champId = null;

            if ($(object).hasClass('texte')) {
                labelForContenu = 'content-texte';
                classContenu = 'texteForm';
                valueContenu = $(idContainer).find('.ui-selected').find('h5').html();

            }  else if ($(object).hasClass('help')) {
                labelForContenu = 'content-help';
                classContenu = 'helpForm';
                valueContenu = $(idContainer).find('.ui-selected').find('.messager').html();
            }

            options = jQuery('' +
                '<div class="col-md-12">' +
                    '<div class="form-group">' +
                        '<label for="'+labelForContenu+'">' +
                            'Contenu' +
                            '<span class="obligatoire"> *</span>' +
                        '</label>' +
                        '<textarea class="form-control startTinyMCE '+classContenu+'" name="'+labelForContenu+'">' + valueContenu + '</textarea>' +
                    '</div>' +
                '</div>' +
                btnGroupsTrashApplicable
            );
        }

        /*Afficher les options en fontion du champ en question*/
        $(fieldOptions).append(options);

        if (champId !== null) {
            $('#' + champId).keyup(function () {
                let value = $(this).val().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').replace(/[^a-zA-Z 0-9_]+/g, '');
                $(this).val(value);
            });
        }

        $('.multiSelect').select2({
            language: "fr",
            placeholder: 'Sélectionnez une ou plusieurs options',
            width: '100%',
            allowClear: true
        });

        $('.startTinyMCE').tinymce({
            script_url : '/js/node_modules/tinymce/tinymce.min.js',
            language_url : '/js/node_modules/tinymce-i18n/langs/fr_FR.js',
            language : 'fr_FR',
            mode : 'textareas',
            editor_selector: 'startTinyMCE',
            height : 150,
            menubar : false,
            statusbar : false,
            plugins: [
                'lists',
                'link',
                'autolink'
            ],
            toolbar: 'fontsizeselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link unlink',
        });

        // On applique les modifications du champ en question au clic sur le boutton "Appliquer"
        $('#'+idBtnApplicable).click(function () {
            let fieldId = $(idContainer).find('.ui-selected').find('[id]').attr('id');
            let conditionsTextuelle = '';

            $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
                let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
                if (conditionsJson) {
                    conditions = JSON.parse(conditionsJson);
                    if (typeof conditions == 'object') {
                        $.each(conditions, function (uuid, condition) {
                            if (fieldId === condition['ifTheField']) {
                                conditionsTextuelle = conditionsTextuelle + writeConditionsInOption(conditionsJson, false);
                            }
                        });
                    }
                }
            });

            if (conditionsTextuelle !== '') {
                $('#allConditionsDelete').html(conditionsTextuelle);

                $('#modalWarningDeleteConditions').modal('show');

                // Si on click sur "Supprimer"
                $('#deleteCondition').click(function () {
                    // Suppression des conditions en référence avec le champ qui est modifier
                    // On supprimer les conditions sur les autres champs si le champ en question fait partie de la condition
                    $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
                        let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
                        if (conditionsJson) {
                            conditions = JSON.parse(conditionsJson);
                            if (typeof conditions == 'object') {
                                $.each(conditions, function (uuid, condition) {
                                    if (fieldId === condition['ifTheField']) {
                                        delete conditions[uuid];
                                    }
                                });

                                $(dataVitualContions).attr('data-virtual-conditions', '');
                                $(dataVitualContions).attr('data-virtual-conditions', JSON.stringify(conditions));
                            }
                        }
                    });

                    applyOptionsFieldOnField();

                    $('#modalWarningDeleteConditions').modal('hide');
                });

                // Si on click sur "Annuler"
                $('#modalWarningDeleteConditions').on('hidden.bs.modal', function () {
                    $('#modalWarningDeleteConditions').modal('hide');

                    $(idContainer).find('.ui-selected').removeClass('ui-selected');
                    hideDetails();
                });

            } else {
                applyOptionsFieldOnField();
            }
        });

        // On supprime le champ en question au clic sur le bouton "Poubelle"
        $('#'+idBtnCloser).click(function () {
            let fieldRemoveId = $(idContainer).find('.ui-selected').find('[id]').attr('id');

            // On supprimer les conditions sur les autres champs si le champ en question fait partie de la condition
            $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
                let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
                if (conditionsJson) {
                    conditions = JSON.parse(conditionsJson);
                    if (typeof conditions == 'object') {
                        $.each(conditions, function (uuid, condition) {
                            $.each(condition, function (key, value) {
                                if (jQuery.inArray(key, ['ifTheField', 'thenTheField']) !== -1) {
                                    if (fieldRemoveId === value) {
                                        delete conditions[uuid];
                                    }
                                }
                            });
                        });

                        $(dataVitualContions).attr('data-virtual-conditions', '');
                        $(dataVitualContions).attr('data-virtual-conditions', JSON.stringify(conditions));
                    }
                }
            });

            $(idContainer).find('.ui-selected').remove();
            hideDetails();
        });

        // On supprime la condition crée sur le champ
        $('.removeCondition').click(function () {
            let condition_id = $(this).attr('id');

            let conditions = JSON.parse($(idContainer).find('.ui-selected').attr('data-virtual-conditions'));
            if (conditions) {
                $.each(conditions, function (key) {
                    if (condition_id === key) {
                        delete conditions[key];
                        return false;
                    }
                });
            }

            $(idContainer).find('.ui-selected').attr('data-virtual-conditions', JSON.stringify(conditions));
            $('#'+condition_id).parent().remove();
        });
    }

    /**
     * Cache les détails du champ lors de sa déselection
     *
     * @returns {undefined}
     */
    function hideDetails()
    {
        $(fieldOptions+">div").remove();
    }

    /**
     * Vérifier si le nom de variable donner n'hésite pas et si c'est pas vide
     *
     * @created 27/03/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function checkNewNameField(nameField)
    {
        let success = true;

        // On vérifie que le "name" du champ n'est pas vide
        if (!nameField) {
            alert("Le nom de la variable est vide !");
            return false;
        }

        // On vérifie que le "name" du champ ne soit pas égale à "undefined"
        if (nameField === 'undefined') {
            alert("Le nom de la variable est ne peut pas être 'undefined' !");
            return false;
        }

        // On vérifie que le "name" du champ n'a pas de caractère spéciaux, espace, tiré
        let transformNameField = nameField.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').replace(/[^a-zA-Z 0-9_]+/g, '');
        if (nameField !== transformNameField) {
            alert("Le nom de la variable ne respecte pas les règles de nommage. Il est interdit d'utiliser des majuscules, des caractères spéciaux, des espaces, des tirets");
            return false;
        }

        // On vérifie dans le "container" qu'il n'héxiste pas un autre champ avec le même "name"
        let candidates = $(idContainer+' *[name=' + nameField + ']');
        let number = $(candidates).length;

        $(candidates).each(function (idx, candidate) {
            if ($(candidate).closest('.ui-selected').length > 0) {
                number--;
            }
        });

        if (number > 0) {
            alert('Un champ possède déjà ce nom de variable !');
            return false;
        }

        if ($('#'+nameField).filter(function (idx, elemt) { return $(this).closest('.ui-selected').length === 0; }).length !== 0) {
            alert('Un champ possède déjà ce nom de variable !');
            return false;
        }

        return success;
    }

    /**
     * Vérifier si le nom du champ donner n'est pas vide
     *
     * @created 27/03/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function checkNewLabelField(labelField)
    {
        let success = true;

        if (!labelField) {
            alert("Le nom du champ ne peut pas être vide !");
            return false;
        }

        return success;
    }

    /**
     * Vérifier si les option du champ ne sont pas vide
     *
     * @created 27/03/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function checkNewOptionsField(optionsField)
    {
        let success = true;

        $.each(optionsField, function (key, option) {
            if (!option) {
                alert("Il n'est pas permis d'avoir une option vide !");
                success = false;
                return success;
            }
        });

        return success;
    }

    /**
     * Vérifier si le contenu du champ donner n'est pas vide
     *
     * @created 27/03/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function checkNewContenuField(contenuField)
    {
        let success = true;

        if (!contenuField) {
            alert("Le contenu du champ ne peut pas être vide !");
            return false;
        }

        return success;
    }

    /**
     * Attribut au champ crée si il est obligatoire ou non
     *
     * @created 27/03/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function giveToFieldIfRequired(required)
    {
        $(idContainer).find('.ui-selected').find('div').find('label').find('span.obligatoire').remove();
        $(idContainer).find('.ui-selected').attr('data-virtual-required', 'fieldNotRequired');

        if (required === true) {
            $(idContainer).find('.ui-selected').find('div').find('label').find('span.labeler').after('<span class="obligatoire"> *</span>');
            $(idContainer).find('.ui-selected').attr('data-virtual-required', 'fieldRequired');
        }
    }

    /**
     * Ecrit textuellement les différentes conditions du champs pour les afficher
     *
     * @created 01/04/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function writeConditionsInOption(conditions, noTrash = true) {
        let writeConditions = '',
            success = true;

        $.each(JSON.parse(conditions), function (key, value) {
            let field = $('#'+value['ifTheField']),
                ifTheField = $(field).labels().text(),
                hasValue = $(field).find('option[value="'+value['hasValue']+'"]').text(),
                thenTheField = $('label[for="'+value['thenTheField']+'"]').text(),
                mustBe = '',
                ifNot = '',
                textHasValue = '';

            if (!ifTheField) {
                alert("Une condition est en erreur !");
                success = false;
                return success;
            }

            if (!hasValue) {
                if (Array.isArray(value['hasValue']) === true ||
                    jQuery.isPlainObject(value['hasValue']) === true
                ) {
                    let arrayHasValue = value.hasValue

                    $.each(arrayHasValue, function (k, v) {
                        if ($(field).is("select")) {
                            hasValue = $(field).find('option[value="' + v + '"]').text();
                        } else {
                            hasValue = $('input[id="' + v + '"]').next('label').text();
                        }

                        textHasValue = textHasValue + '<strong>' + ' "' + $.trim(hasValue) + '", ' + '</strong>';
                    });
                } else {
                    // Radios
                    hasValue = $('input[type="radio"][name="' + value['ifTheField'] + '"][value="' + value['hasValue'] + '"]').val();
                    textHasValue = '<strong>' + ' "' + hasValue + '" ' + '</strong>';
                }
            } else {
                textHasValue = '<strong>' + ' "' + hasValue + '" ' + '</strong>';
            }

            if (value['mustBe'] === 'shown') {
                mustBe = 'affiché';
            } else {
                mustBe = 'caché';
            }

            if (value['ifNot'] === 'shown') {
                ifNot = 'affiché';
            } else {
                ifNot = 'caché';
            }

            let btnTrash = '';
            if (noTrash === true) {
                btnTrash =  '<a id="'+key+'" href="#" class="removeCondition">' +
                    '<span class="fa fa-trash fa-lg fa-danger"></span>' +
                '</a>';
            }

            writeConditions = writeConditions + '<p><li>' +
                'Si le champ' +
                    '<strong>'+
                        ' "'+ifTheField+'" '+
                    '</strong>' +
                'a comme valeur'+
                    textHasValue +
                'alors le champ' +
                    '<strong>'+
                        ' "'+thenTheField+'" '+
                    '</strong>' +
                'sera' +
                    '<strong>'+
                        ' "'+mustBe+'" '+
                    '</strong>' +
                'sinon il sera' +
                    '<strong>'+
                        ' "'+ifNot+'" '+
                    '</strong>' +
                btnTrash +
            '</li></p>';
        });

        if (success === true) {
            writeConditions = '<div class="col-md-12">' +
                writeConditions +
                '</div>';
        }

        return writeConditions;
    }

    /**
     * On vérifie et on applique les "Options du champ" sur le champ
     *
     * @created 02/04/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    function applyOptionsFieldOnField()
    {
        let success = true,
            newNameField = null,
            newLabelField = null,
            newPlaceholderField = null,
            newOptionsField = null,
            newContenuField = null,
            newRequiredField = false,
            newRepeteField = false;

        $('#'+idBtnApplicable).closest(fieldOptions).find('input, textarea').each(function () {
            // On récupère le "Nom de variable" et on le vérifie
            if ($(this).hasClass('nameForm')) {
                newNameField = $(this).val();
                if (newNameField.startsWith(typeCreateForm+'_') === false && typeCreateForm !== 'formulaire') {
                    newNameField = typeCreateForm + '_' + newNameField;
                    $(this).val(newNameField);
                }

                success = checkNewNameField(newNameField);
            }

            // On récupère le "Nom du champ" et on le vérifie
            if ($(this).hasClass('labelForm')) {
                newLabelField = $(this).val();
                success = checkNewLabelField(newLabelField);
            }

            // On récupère l' "Aide à la saisie"
            if ($(this).hasClass('placeholderForm')) {
                newPlaceholderField = $(this).val();
            }

            // On récupère la "Liste des options" et on la vérifie
            if ($(this).is('.checkboxForm, .radioForm, .deroulantForm, .multiSelectForm')) {
                newOptionsField = $(this).val().split('\n');
                success = checkNewOptionsField(newOptionsField);
            }

            // On récupère le "Contenu" et on le vérifie
            if ($(this).is('.titleForm, .helpForm, .texteForm')) {
                newContenuField = $(this).val();
                success = checkNewContenuField(newContenuField);
            }

            // On récupère le "Champ obligatoire"
            if ($(this).hasClass('obligForm')) {
                if ($(this).prop('checked')) {
                    newRequiredField = true;
                }
            }

            // si erreur on sort de la boucle (de .each)
            if (success === false) {
                return success;
            }
        });

        // On arrête le traitement
        if (success === false) {
            return success;
        }

        $(idContainer).find('.ui-selected').each(function () {
            let objet = '';

            // Pour tous les champs ont leur attribut le label et si le champ est obligatoire ou non
            if ($(this).is('.small-text, .long-text, .date, .checkboxes, .radios, .deroulant, .multi-select')) {
                // give to field the "label"
                $(idContainer).find('.ui-selected').find('div').find('label').find('span.labeler').text(newLabelField);

                // add attribut "for" to "label"
                $(idContainer).find('.ui-selected').find('label').attr('for', newNameField);

                // give to field if required
                giveToFieldIfRequired(newRequiredField);
            }

            // Attribution les valeurs aux champs "Petit champ texte" et "Champ date"
            if ($(this).is('.small-text, .date')) {
                // add attribute "id"
                $(idContainer).find('.ui-selected').find('input').attr('id', newNameField);

                // give to field the "name"
                $(idContainer).find('.ui-selected').find('input').attr('name', newNameField);

                // give to field the "placeholder"
                $(idContainer).find('.ui-selected').find('input').attr('placeholder', newPlaceholderField);
            }

            // Attribution les valeurs au champ "Grand champ texte"
            if ($(this).hasClass('long-text')) {
                // add attribute "id"
                $(idContainer).find('.ui-selected').find('textarea').attr('id', newNameField);

                // give to field the "name"
                $(idContainer).find('.ui-selected').find('textarea').attr('name', newNameField);

                // give to field the "placeholder"
                $(idContainer).find('.ui-selected').find('textarea').attr('placeholder', newPlaceholderField);
            }

            // Attribution les valeurs au champ "Cases à cocher"
            if ($(this).hasClass('checkboxes')) {
                objet = objet + '<input id="'+newNameField+'" type="hidden" name="'+newNameField+'" value="">'
                $.each(newOptionsField, function (index, value) {
                    objet = objet + '<div class="form-check">' +
                        '<input class="form-check-input" type="checkbox" name="'+newNameField+'" value="" id="'+newNameField+index+'">' +
                        '<label class="form-check-label" for="'+newNameField+index+'">' +
                            value +
                        '</label>' +
                    '</div>';
                });

                // give to field the options
                $(idContainer).find('.ui-selected').find('.contentCheckbox').html(jQuery(objet));
            }

            // Attribution les valeurs au champ "Choix unique"
            if ($(this).hasClass('radios')) {
                objet = objet + '<input id="'+newNameField+'" type="hidden" name="'+newNameField+'" value="">'
                $.each(newOptionsField, function (index, value) {
                    objet = objet +
                        '<div class="form-check radio">' +
                            '<input class="form-check-input" type="radio" name="'+newNameField+'" id="'+newNameField+index+'" value="'+value+'">' +
                            '<label class="form-check-label" for="'+newNameField+index+'">'+
                                value +
                            '</label>'+
                        '</div>'
                    ;
                });

                // give to field the options
                $(idContainer).find('.ui-selected').find('.contentRadio').html(jQuery(objet));
            }

            // Attribution les valeurs au champ "Menu déroulant"
            if ($(this).hasClass('deroulant')) {
                objet = objet + '<option name="'+newNameField+'" value=""></option>';

                $.each(newOptionsField, function (index, value) {
                    objet = objet + '<option id="'+newNameField+index+'" type="deroulant" name="'+newNameField+'" value="'+index+'">' +
                        value +
                    '</option>';
                });

                // add attribute "id"
                $(idContainer).find('.ui-selected').find('select').attr('id', newNameField);

                // give to field the options
                $(idContainer).find('.ui-selected').find('.contentDeroulant').html(jQuery(objet));
            }

            // Attribution les valeurs au champ "Menu multi-sélect"
            if ($(this).hasClass('multi-select')) {
                $.each(newOptionsField, function (index, value) {
                    objet = objet + '<option id="'+newNameField+index+'" name="'+newNameField+'" value="'+index+'">' +
                        value +
                    '</option>';
                });

                // add attribute "id"
                $(idContainer).find('.ui-selected').find('input[type="hidden"]').attr('id', newNameField+'_');
                $(idContainer).find('.ui-selected').find('input[type="hidden"]').attr('name', newNameField);
                $(idContainer).find('.ui-selected').find('select').attr('name', newNameField);
                $(idContainer).find('.ui-selected').find('select').attr('id', newNameField);

                // give to field the options
                $(idContainer).find('.ui-selected').find('.contentMultiSelect').html(jQuery(objet));
            }

            // Attribution les valeurs au champ "Titre de catégorie"
            if ($(this).hasClass('title')) {
                $(idContainer).find('.ui-selected').find('h1').html(newContenuField);
            }

            // Attribution les valeurs au champ "Champ d'information"
            if ($(this).hasClass('help')) {
                $(idContainer).find('.ui-selected').find('.messager').html(newContenuField);
            }

            // Attribution les valeurs au champ "Label"
            if ($(this).hasClass('texte')) {
                let breakTag = (newContenuField || typeof newContenuField === 'undefined') ? '<br ' + '/>' : '<br>'
                let text = (newContenuField + '').replace(/(\r\n|\n\r|\r|\n)/g, breakTag + '$1');
                $(idContainer).find('.ui-selected').find('h5').html(text);
            }
        });

        // $(idContainer).find('.ui-selected').attr('style', function (i, style) {
        //     return style.replace(/height[^;]+;?/g, '');
        // });

        // $(idContainer).find('.ui-selected').attr('style', function (i, style) {
        //     return style.replace(/width[^;]+;?/g, '');
        // });

        // On rend non modifiable le nom de variable
        $('#'+idBtnApplicable).closest(fieldOptions).find('input.nameForm').attr('readonly', true);
    }

};
