$(document).ready(function () {

    $('.boutonDelete').popover({
            delay: {show: 500, hide: 100},
            animation: true,
            content: 'Supprimer',
            placement: 'top',
            trigger: 'hover'
        }
    );
    $('.boutonEdit').popover({
            delay: {show: 500, hide: 100},
            animation: true,
            content: 'Modifier',
            placement: 'top',
            trigger: 'hover'
        }
    );

    $('.multiDeroulant').chosen({
        no_results_text: 'Aucun résultat trouvé pour',
        width: '100%'
    });

    $('#deroulant').change(function () {
        $('.droitsVille').css('display', 'none');
        $('#deroulant option:selected').each(function () {
            var clickedOptionValue = $(this).attr('value');
            $('#droitsVille' + clickedOptionValue).css('display', '');
            $('#droitsVille' + clickedOptionValue).css('visibility', 'visible');
        });
    }).trigger('change');


    $('.btnDroitsParticuliers').click(function () {
        var valeur = $(this).attr('value');
        if ($('#droitsParticuliers' + valeur).is(':visible')) {
            $('#droitsParticuliers' + valeur).hide();
        }
        else {
            $('#droitsParticuliers' + valeur).show();
        }
    });

    $("[class*='deroulantRoles']").change(function () {
        var idOrga = $(this).attr('id'); //id de l'organisation

        $('.deroulantRoles' + idOrga + ' option:selected').each(function () {
            var idRole = $(this).attr('value'); // id du rôle
            var data = null;

            for (var key in data = eval('tableau_js' + idRole)) {
                $('.checkDroits' + idOrga + data[key]).prop('checked', true);
            }
        });
        $('.deroulantRoles' + idOrga + ' option:not(:selected)').each(function () {
            var idRole = $(this).attr('value'); // id du rôle
            var data = null;

            for (var key in data = eval('tableau_js' + idRole)) {
                $('.checkDroits' + idOrga + data[key]).prop('checked', false);
            }
        });
    }).trigger('change');

    // Mask champs
    $('#telephonefixe').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });
    $('#telephoneportable').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });

    $('#nom').keyup(function() {
        var value = $(this).val().toUpperCase();
        $(this).val(value);
    });

    String.prototype.ucFirst = function(){
        return this.substr(0,1).toUpperCase()+this.substr(1);
    };

    $('#prenom').keyup(function() {
        var value = $(this).val().toLowerCase().ucFirst();

        value = value.toLowerCase().replace(/(^|\s|\-)([a-zéèêë])/g,function(u,v,w){
            return v+w.toUpperCase();
        });

        $(this).val(value);
    });
});
