$(document).ready(function () {
    // $('.error').addClass('has-error');

    // Mask champs
    $('#telephone').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });
    $('#fax').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });
    $('#siret').mask('000 000 000 00000', {
        placeholder: '___ ___ ___ _____'
    });
    $('#ape').mask("AAAAA", {
        placeholder: "_____"
    });
    $('#telephoneresponsable').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });

    $('#nomresponsable').keyup(function() {
        let value = $(this).val().toUpperCase();
        $(this).val(value);
    });

    String.prototype.ucFirst = function(){
        return this.substr(0,1).toUpperCase()+this.substr(1);
    };

    $('#prenomresponsable').keyup(function() {
        let value = $(this).val().toLowerCase().ucFirst();

        value = value.toLowerCase().replace(/(^|\s|\-)([a-zéèêë])/g,function(u,v,w) {
            return v+w.toUpperCase();
        });

        $(this).val(value);
    });
});
