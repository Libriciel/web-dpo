$(document).ready(function () {

    $('#nom').keyup(function() {
        var value = $(this).val().toUpperCase();
        $(this).val(value);
    });

    String.prototype.ucFirst = function(){
        return this.substr(0,1).toUpperCase()+this.substr(1);
    };

    $('#prenom').keyup(function() {
        var value = $(this).val().toLowerCase().ucFirst();

        value = value.toLowerCase().replace(/(^|\s|\-)([a-zéèêë])/g,function(u,v,w){
            return v+w.toUpperCase();
        });

        $(this).val(value);
    });

});