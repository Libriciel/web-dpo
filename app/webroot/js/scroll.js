$(document).ready(function () {

    function scrolled() {
        let currentScroll = document.body.scrollTop || document.documentElement.scrollTop,
            forScroll = $('.tab-pane.active #virtualFieldsOptions.forScroll'),
            forScrollWidth = $(forScroll).width(),
            navbarHeight = $('nav.navbar-ls').height() + $('nav.second-nav').height();

        if (forScroll.length) {
            $(forScroll).removeClass('fixedScroll');
            $(forScroll).css('width', '');

            if (currentScroll >= ($(forScroll).offset().top - navbarHeight)) {
                $(forScroll).addClass('fixedScroll');
                $(forScroll).width(forScrollWidth);
            }
        }
    }

    addEventListener('scroll', scrolled, false);

    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e){
    //     let forScroll = $('.forScroll');
    //
    //     if (forScroll.length) {
    //         $(forScroll).removeClass('fixedScroll');
    //         $(forScroll).css('width', '');
    //     }
    // });
    
});
