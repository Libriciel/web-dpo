/**
 * Created by aurelien on 11/06/14.
 */
$(document).ready(function(){
    // fade out good flash messages after 5 seconds
    $('.toast').animate({opacity: 1.0}, 5000).fadeOut();

    $('.toast').toast({
        'autohide': false
    });

    // $('.toast').toast('show');
});
