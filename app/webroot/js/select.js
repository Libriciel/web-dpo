$(document).ready(function () {
	try {
		let selectorClass = ".chosen-select, .usersDeroulant";

		$(selectorClass).chosen({
			no_results_text: "Aucun résultat trouvé pour",
			allow_single_deselect: true,
			width: '100%',
			search_contains: true
		});

		$(selectorClass).on("change", function (event) {
			try {
				$(event.target).trigger("chosen:updated");
			} catch(e) {
				console.error(e);
			}
		});
	} catch(exception) {
		console.log(exception);
	}

});
