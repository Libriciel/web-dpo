// -----------------------------------------------------------------------------
let addSousFinalite = function(idField) {

    //on add input button click
    $('#AddMoreFileBox').click(function (e) {
        //add input box
        $('#InputsWrapper').append('<div class="form-group">'
            +'<label for="sousfinalite" class="control-label"> '
                + 'Sous finalité ' + idField
            +'</label>'
            +'<input name="data[WebdpoFiche][sousFinalite][]" id="sousFinalite_'+ idField +'" class="form-control" type="text">'
            +'<a href="#" class="removeclass btn btn-outline-danger borderless">'
                +'<i class="fa fa-trash fa-lg"></i>'
            +'</a>'
        +'</div>');

        idField++;
    });

    //user click on remove text
    $('body').on('click', '.removeclass', function(e) {
        let idInputDelete = $(this).prevAll("input[type=text]").attr('id');
        let intIdInputDelete = parseInt(idInputDelete.substr(idInputDelete.indexOf("_") + 1));
        let i = intIdInputDelete + 1;

        $(this).parent('div').remove(); //remove text box

        while (i <= idField) {
            let newValue = (i - 1);

            $('#sousFinalite_'+i).siblings().first().text('Sous finalité '+ newValue);
            $('#sousFinalite_'+i).attr('id', 'sousFinalite_'+newValue);

            i++;
        }

        idField--;
    });
};
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
let addDecisionAutomatisee = function() {
    $('#decisionAutomatisee').change(function () {
        let select = $(this).val();

        displayDescriptionDecisionAutomatisee(select);
    });
};

function displayDescriptionDecisionAutomatisee(val){
    if (val == 'Oui') {
        $('#descriptionDecisionAutomatisee').parent().parent().show();
    } else {
        $('#descriptionDecisionAutomatisee').parent().parent().hide();
        // $('#descriptionDecisionAutomatisee').val('');
    }
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
let addHorsUE = function(idHorsUE) {
    $('#transfert_hors_ue').change(function () {
        let select = $(this).val();

        displayInfoSupHorsUE(select);
    });

    //on add input button click
    $('#AddOrganismeHorsUEFileBox').click(function (e) {
        createInputsInfoSupHorsUe(idHorsUE);

        idHorsUE++;

        makeUsersDeroulant();
    });

    //user click on remove text
    $('body').on('click', '.removeHorsUE', function(e) {
        let idInputDeleteHorsUE = $(this).parent('div').parent('div').attr('id');
        let intIdInputDeleteHorsUE = parseInt(idInputDeleteHorsUE.substr(idInputDeleteHorsUE.indexOf("_") + 1));
        let i = intIdInputDeleteHorsUE + 1;

        // On supprime de groupe de champ
        let groupeHorsUe = $('#HorsUe_'+intIdInputDeleteHorsUE);
        $(groupeHorsUe).prev('hr').remove();
        $(groupeHorsUe).remove();

        while (i < idHorsUE) {
            let newValue = (i - 1);

            let destinataireHorsUe = $('#FicheHorsue'+i+'OrganismeDestinataireHorsUe').val();
            let typeGarantieHorsUe = $('#FicheHorsue'+i+'TypeGarantieHorsUe').val();
            let paysDestinataireHorsUe = $('#FicheHorsue'+i+'PaysDestinataireHorsUe').val();

            let horsUe = $('#HorsUe_'+i);

            $(horsUe).prev('hr').remove();
            $(horsUe).remove();

            createInputsInfoSupHorsUe(newValue);

            $('#FicheHorsue'+newValue+'OrganismeDestinataireHorsUe').val(destinataireHorsUe);
            $('#FicheHorsue'+newValue+'TypeGarantieHorsUe').val(typeGarantieHorsUe);
            $('#FicheHorsue'+newValue+'PaysDestinataireHorsUe').val(paysDestinataireHorsUe);

            makeUsersDeroulant();

            i++;
        }

        idHorsUE--;
    });
};

function displayInfoSupHorsUE(val){
    if (val == true) {
        $('#infoSupHorsUE').show();
    } else {
        $('#infoSupHorsUE').hide();
    }
}
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
let addDonneeSensible = function(idDonneesSensibles) {
    // $('#donneesSensible').change(function () {
    $('#donnees_sensibles').change(function () {
        let select = $(this).val();

        displayInfoSupDonneesSensibles(select);
    });

    // on add input button click
    $('#AddDonneesSensibles').click(function (e) {
        createInputsInfoSupDonneeSensible(idDonneesSensibles);

        idDonneesSensibles++;

        makeUsersDeroulant();
    });

    //user click on remove text
    $('body').on('click', '.removeDonneeSensible', function (e) {
        let idInputDeleteDonneeSensible = $(this).parent('div').parent('div').attr('id');
        let intIdInputDeleteDonneeSensible = parseInt(idInputDeleteDonneeSensible.substr(idInputDeleteDonneeSensible.indexOf("_") + 1));
        let i = intIdInputDeleteDonneeSensible + 1;

        // On supprime de groupe de champ
        let groupeDonneeSensible = $('#DonneeSensible_' + intIdInputDeleteDonneeSensible);
        $(groupeDonneeSensible).prev('hr').remove();
        $(groupeDonneeSensible).remove();

        while (i < idDonneesSensibles) {
            let newValue = (i - 1);

            let typeDonneeSensible = $('#FicheDonneessensibles' + i + 'TypeDonneeSensible').val();
            let dureeConservationDonneeSensible = $('#FicheDonneessensibles' + i + 'DescriptionDonneeSensible').val();
            let descriptionDonneeSensible = $('#FicheDonneessensibles' + i + 'DureeConservationDonneeSensible').val();

            let donneeSensible = $('#DonneeSensible_' + i);
            $(donneeSensible).prev('hr').remove();
            $(donneeSensible).remove();

            createInputsInfoSupDonneeSensible(newValue);

            $('#FicheDonneessensibles' + newValue + 'TypeDonneeSensible').val(typeDonneeSensible);
            $('#FicheDonneessensibles' + newValue + 'DescriptionDonneeSensible').val(dureeConservationDonneeSensible);
            $('#FicheDonneessensibles' + newValue + 'DureeConservationDonneeSensible').val(descriptionDonneeSensible);

            makeUsersDeroulant();

            i++;
        }

        idDonneesSensibles--;
    });
};

function displayInfoSupDonneesSensibles(val){
    if (val == true) {
        $('#infoSupDonneesSensibles').show();
    } else {
        $('#infoSupDonneesSensibles').hide();
    }
}
// -----------------------------------------------------------------------------
