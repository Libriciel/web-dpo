$(document).ready(function () {

    displayAllInputsPia();

    let ressourcesHumaines = $('#ressources_humaines');
    displayFieldPiaListNotRequired($(ressourcesHumaines).attr('id'), $(ressourcesHumaines).val(), false);
    $(ressourcesHumaines).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let relationFournisseurs = $('#relation_fournisseurs');
    displayFieldPiaListNotRequired($(relationFournisseurs).attr('id'), $(relationFournisseurs).val(), false);
    $(relationFournisseurs).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let gestionElectoral = $('#gestion_electoral');
    displayFieldPiaListNotRequired($(gestionElectoral).attr('id'), $(gestionElectoral).val(), false);
    $(gestionElectoral).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let comitesEntreprise = $('#comites_entreprise');
    displayFieldPiaListNotRequired($(comitesEntreprise).attr('id'), $(comitesEntreprise).val(), false);
    $(comitesEntreprise).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let association = $('#association');
    displayFieldPiaListNotRequired($(association).attr('id'), $(association).val(), false);
    $(association).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let santePrisePatient = $('#sante_prise_patient');
    displayFieldPiaListNotRequired($(santePrisePatient).attr('id'), $(santePrisePatient).val(), false);
    $(santePrisePatient).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let avocats = $('#avocats');
    displayFieldPiaListNotRequired($(avocats).attr('id'), $(avocats).val(), false);
    $(avocats).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let greffiers = $('#greffiers');
    displayFieldPiaListNotRequired($(greffiers).attr('id'), $(greffiers).val(), false);
    $(greffiers).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let notaires = $('#notaires');
    displayFieldPiaListNotRequired($(notaires).attr('id'), $(notaires).val(), false);
    $(notaires).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let collectivitesAffairesScolaires = $('#collectivites_affaires_scolaires');
    displayFieldPiaListNotRequired($(collectivitesAffairesScolaires).attr('id'), $(collectivitesAffairesScolaires).val(), false);
    $(collectivitesAffairesScolaires).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let controlesAcces = $('#controles_acces');
    displayFieldPiaListNotRequired($(controlesAcces).attr('id'), $(controlesAcces).val(), false);
    $(controlesAcces).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let ethylotests = $('#ethylotests');
    displayFieldPiaListNotRequired($(ethylotests).attr('id'), $(ethylotests).val(), false);
    $(ethylotests).change(function () {
        displayFieldPiaListNotRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    //

    let santeMedicosociaux = $('#sante_medicosociaux');
    displayFieldPiaListRequired($(santeMedicosociaux).attr('id'), $(santeMedicosociaux).val(), false);
    $(santeMedicosociaux).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let donneesGenetiques = $('#donnees_genetiques');
    displayFieldPiaListRequired($(donneesGenetiques).attr('id'), $(donneesGenetiques).val(), false);
    $(donneesGenetiques).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let profilsPersonnesGestionRh = $('#profils_personnes_gestion_rh');
    displayFieldPiaListRequired($(profilsPersonnesGestionRh).attr('id'), $(profilsPersonnesGestionRh).val(), false);
    $(profilsPersonnesGestionRh).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let surveillerConstanteEmployes = $('#surveiller_constante_employes');
    displayFieldPiaListRequired($(surveillerConstanteEmployes).attr('id'), $(surveillerConstanteEmployes).val(), false);
    $(surveillerConstanteEmployes).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let gestionAlertesSocialeSanitaire = $('#gestion_alertes_sociale_sanitaire');
    displayFieldPiaListRequired($(gestionAlertesSocialeSanitaire).attr('id'), $(gestionAlertesSocialeSanitaire).val(), false);
    $(gestionAlertesSocialeSanitaire).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let gestionAlertesProfessionnelle = $('#gestion_alertes_professionnelle');
    displayFieldPiaListRequired($(gestionAlertesProfessionnelle).attr('id'), $(gestionAlertesProfessionnelle).val(), false);
    $(gestionAlertesProfessionnelle).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let donneesSanteRegistre = $('#donnees_sante_registre');
    displayFieldPiaListRequired($(donneesSanteRegistre).attr('id'), $(donneesSanteRegistre).val(), false);
    $(donneesSanteRegistre).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let profilageRuptureContrat = $('#profilage_rupture_contrat');
    displayFieldPiaListRequired($(profilageRuptureContrat).attr('id'), $(profilageRuptureContrat).val(), false);
    $(profilageRuptureContrat).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let mutualisesManquementsRuptureContrat = $('#mutualises_manquements_rupture_contrat');
    displayFieldPiaListRequired($(mutualisesManquementsRuptureContrat).attr('id'), $(mutualisesManquementsRuptureContrat).val(), false);
    $(mutualisesManquementsRuptureContrat).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let profilageDonneesExternes = $('#profilage_donnees_externes');
    displayFieldPiaListRequired($(profilageDonneesExternes).attr('id'), $(profilageDonneesExternes).val(), false);
    $(profilageDonneesExternes).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let biometriques = $('#biometriques');
    displayFieldPiaListRequired($(biometriques).attr('id'), $(biometriques).val(), false);
    $(biometriques).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let gestionLogementsSociaux = $('#gestion_logements_sociaux');
    displayFieldPiaListRequired($(gestionLogementsSociaux).attr('id'), $(gestionLogementsSociaux).val(), false);
    $(gestionLogementsSociaux).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let accompagnementSocial = $('#accompagnement_social');
    displayFieldPiaListRequired($(accompagnementSocial).attr('id'), $(accompagnementSocial).val(), false);
    $(accompagnementSocial).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        scrollEnd();
    });

    let localisationLargeEchelle = $('#localisation_large_echelle');
    displayFieldPiaListRequired($(localisationLargeEchelle).attr('id'), $(localisationLargeEchelle).val(), false);
    $(localisationLargeEchelle).change(function () {
        displayFieldPiaListRequired($(this).attr('id'), $(this).val());
        displayFieldRisque(false);
        scrollEnd();
    });

    //

    displayFieldRisque(false);
    $('#info_pia input[type=checkbox]').change(function () {
        displayFieldRisque();
        scrollEnd();
    });

    let traitementConsidereRisque = $('#traitement_considere_risque');
    displayFieldTraitementConsidereRisque($(traitementConsidereRisque).val(), false)
    $(traitementConsidereRisque).change(function () {
        displayFieldTraitementConsidereRisque($(this).val());
        scrollEnd();
    });
});

function displayFieldTraitementConsidereRisque(val, showAlert = true)
{
    if (val == 'Oui') {
        if (showAlert === true) {
            alert('La réalisation d\'une analyse d\'impact (AIPD) est OBLIGATOIRE');
        }
        $('#obligation_pia').val('1');
    }

    if (val == 'Non') {
        if (showAlert === true) {
            alert('La réalisation d\'une analyse d\'impact (AIPD) n\'est pas obligatoire');
        }
        $('#obligation_pia').val('0');
    }
}

function displayFieldRisque(showFirst = true)
{
    let nbChecked = $("#info_pia :checkbox:checked").length;

    if (nbChecked >= 2) {
        if (showFirst === true && nbChecked === 2) {
            alert('La réalisation d\'une analyse d\'impact (AIPD) est OBLIGATOIRE');
        }
        $('#obligation_pia').val('1');

        $('#traitement_considere_risque').parent().hide();
        $('#traitement_considere_risque').val('');
    } else if (nbChecked == 1) {
        $('#obligation_pia').val('');
        $('#traitement_considere_risque').parent().show();
    } else if (nbChecked == 0) {
        $('#traitement_considere_risque').parent().hide();
        $('#traitement_considere_risque').val('');

        if (showFirst === true) {
            $('#obligation_pia').val('0');
        }
    }
}

function displayAllInputsPia() {
    $('#info_pia .displayInput').parent().hide();
    $('#liste_obligatoire').hide();
    $('#liste_criteres').hide();
}

function displayFieldPiaListNotRequired(id, val, showAlert = true) {
    if (val == 'Non') {
        $('#'+id).parent().next('div').removeAttr('style');

        if (!$('#'+id).parent().next('div').length) {
            showListeObligatoire();
        }

        $('#obligation_pia').val('');

    } else {
        if (val == 'Oui') {
            if (showAlert === true) {
                alert('La réalisation d\'une analyse d\'impact (AIPD) n\'est pas obligatoire');
            }
            $('#obligation_pia').val('0');
        }

        // On cache et on reset les champs pour lesquelles une AIPD n'est pas obligatoire
        $('#'+id).parent().nextAll('div').hide();
        $('#'+id).parent().nextAll('div').find('select').prop('selectedIndex',0);

        // On cache et on reset les champs pour lesquelles une AIPD est obligatoire
        $('#liste_obligatoire').hide();
        $('#liste_obligatoire').find('div.form-group').hide();
        $('.fieldRequiredPia').val('');

        // On cache et on reset les champs concernant les critères
        $('#liste_criteres').hide();
        $('#info_pia input[type=checkbox]').prop('checked',false);
        $('#traitement_considere_risque').val('');
    }
}

function showListeObligatoire()
{
    $('#liste_obligatoire').show();
    $('#sante_medicosociaux').parent().show();
}

function displayFieldPiaListRequired(id, val, showAlert = true)
{
    if (val == 'Non') {
        $('#' + id).parent().next('div').removeAttr('style');

        if (!$('#' + id).parent().next('div').length) {
            showCriteres();
        }

        $('#obligation_pia').val('');

    } else {
        if (val == 'Oui') {
            if (showAlert === true) {
                alert('La réalisation d\'une analyse d\'impact (AIPD) est OBLIGATOIRE');
            }
            $('#obligation_pia').val('1');
        }

        // On cache et on reset les champs pour lesquelles une AIPD est obligatoire
        $('#'+id).parent().nextAll('div').hide();
        $('#'+id).parent().nextAll('div').find('select').prop('selectedIndex',0);

        // On cache et on reset les champs concernant les critères
        $('#liste_criteres').hide();
        $('#info_pia input[type=checkbox]').prop('checked',false);
        $('#traitement_considere_risque').val('');
    }
}

function showCriteres()
{
    $('#liste_criteres').show();
    $('#criteres').parent().show();
    $('#traitement_considere_risque').parent().hide();
}

function scrollEnd()
{
    $(document).scrollTop($(document).height());
}
