$(document).ready(function () {
    $('.error').addClass('has-error');

    // Mask champs
    $('.maskTelephone').mask('00 00 00 00 00', {
        placeholder: '__ __ __ __ __'
    });

    $('.maskSiret').mask('000 000 000 00000', {
        placeholder: '___ ___ ___ _____'
    });

    $('.maskApe').mask("AAAAA", {
        placeholder: "_____"
    });

    $('#nomresponsable, #nom_dpo').keyup(function() {
        var value = $(this).val().toUpperCase();
        $(this).val(value);
    });

    String.prototype.ucFirst = function(){
        return this.substr(0,1).toUpperCase()+this.substr(1);
    };

    $('#prenomresponsable, #prenom_dpo').keyup(function() {
        var value = $(this).val().toLowerCase().ucFirst();

        value = value.toLowerCase().replace(/(^|\s|\-)([a-zéèêë])/g,function(u,v,w){
            return v+w.toUpperCase();
        });

        $(this).val(value);
    });
});