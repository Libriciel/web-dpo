msgid ""
msgstr ""
"Project-Id-Version: web-DPO 1.2\n"
"PO-Revision-Date: 2020-04-03 12:00+0100\n"
"Last-Translator: Théo GUILLON <theo.guillon@libriciel.coop>\n"
"Language-Team: Théo GUILLON <theo.guillon@libriciel.coop>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>1);\n"

# ======================================================================================================================

##################### Controller/CircuitsController.php #######################

###################### Session->setFlash ######################

msgid "circuit.flashsuccessLeCircuit"
msgstr "Le circuit"

msgid "circuit.flashsuccessAjouter"
msgstr "a été ajouté"

msgid "circuit.flashsuccessModifier"
msgstr "a été modifié"

msgid "circuit.flasherrorErreurEnregistrementCircuit"
msgstr "Une erreur s'est produite lors de l'enregistrement du circuit"

msgid "circuit.flasherrorErreurFormulaireCircuit"
msgstr "Veuillez corriger les erreurs du formulaire"

msgid "circuit.flasherrorInvalideID"
msgstr "Invalide id pour le "

msgid "circuit.flasherrorCircuitTraitement"
msgstr "circuit de traitement"

msgid "circuit.flasherrorAffichageVueImpossible"
msgstr "affichage de la vue impossible."

msgid "circuit.flasherrorSupprimerImpossible"
msgstr "suppression impossible."

msgid "circuit.flasherrorLeCircuitTraitement"
msgstr "Le circuit de traitement"

msgid "circuit.flasherrorNePeutPasSupprimer"
msgstr "ne peut pas être supprimé."

msgid "circuit.flasherrorAEteSupprimer"
msgstr "a été supprimé."

msgid "circuit.flasherrorErreurPendantSuppression"
msgstr "Une erreur est survenue pendant la suppression"

###############################################################

###############################################################################
