<?php
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');

function tempdir()
{
    $tempfile=tempnam(sys_get_temp_dir(),'');
    // you might want to reconsider this line when using this snippet.
    // it "could" clash with an existing directory and this line will
    // try to delete the existing one. Handle with caution.
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}

abstract class Conversion
{
    public static function convert($data, $dataExtention, $dataSortieExtention)
    {
        require_once 'XML/RPC2/Client.php';

        $options = [
            'uglyStructHack' => true
        ];

        $url = 'http://' . Configure::read('FusionConv.cloudooo_host') . ':' . Configure::read('FusionConv.cloudooo_port');
        $client = XML_RPC2_Client::create($url, $options);
        
        try {
            $result = $client->convertFile(base64_encode($data), $dataExtention, $dataSortieExtention, false, true);
            return base64_decode($result, true);
        } catch (XML_RPC2_FaultException $e) {
            CakeLog::error('Exception #' . $e->getFaultCode() . ' : ' . $e->getFaultString());
            return false;
        }
    }

    public static function pdf2odt($path)
    {
        $folderPath = tempdir();
        $pdf = new File($path);
        $folder = new Folder($folderPath, true, 0755);

        //Preparation de la commande ghostscript
        $PDFTK_EXEC = Configure::read('PDFTK_EXEC');
        $PDFINFO_EXEC = Configure::read('PDFINFO_EXEC');
        $GS_RESOLUTION = Configure::read('GS_RESOLUTION');

        $command = $PDFINFO_EXEC . ' ' . $pdf->pwd() . ' | grep -a Pages: | sed -e "s/ *Pages: *//g"';
        $NbrPage = trim(shell_exec($command));
        if ($NbrPage > 0) {
            for ($i = 1; $i <= $NbrPage; $i++) {
                $pageName = 'page_' . sprintf('%04d', $i);
                shell_exec($PDFTK_EXEC . ' ' . $pdf->pwd() . ' cat ' . $i . ' output ' . $folder->pwd() . DS . $pageName . '.pdf-orign 2>&1');
                shell_exec($PDFTK_EXEC . ' ' . $folder->pwd() . DS . $pageName . '.pdf-orign dump_data output ' . $folder->pwd() . DS . $pageName . '.txt 2>&1');
                shell_exec($PDFTK_EXEC . ' ' . $folder->pwd() . DS . $pageName . '.pdf-orign update_info ' . $folder->pwd() . DS . $pageName . '.txt output ' . $folder->pwd() . DS . $pageName . '.pdf 2>&1');
            }
        } else {
            $msgstr = 'Erreur lors du lancement de la commande: %s';
            throw new RuntimeException(sprintf($msgstr, $command));
        }

        $files = $folder->find('.*\.pdf', true);

        $i = 1;
        foreach ($files as $file) {
            $file = new File($folder->pwd() . DS . $file);

            $imagick = new Imagick();
            $imagick->setResolution($GS_RESOLUTION, $GS_RESOLUTION);
            $imagick->readImage($file->pwd() . '[0]');
            $imagick->setImageFormat('png');
            $imagick->writeImage($folder->pwd() . DS . $i . '.png');

            if ($imagick->getImageHeight() > $imagick->getImageWidth()) {
                $orientaion = 'portrait';
            } else {
                $orientaion = 'landscape';
            }

            $pageParam[$i] = ['path' => $folder->pwd() . DS . $i . '.png',
                'name' => $i . '.png',
                'orientation' => $orientaion];

            CakeLog::debug('page ' . $i . '| orientation=' . $orientaion);
            $imagick->clear();
            $file->close();

            $i++;
        }

        //génération du fichier ODT
        if (empty($pageParam)) {
            throw new InternalErrorException('Impossible de convertir le fichier : paramètres manquants');
        }

        static::generateOdtFileWithImages($folder, $pageParam);

        $file = new File($folder->pwd() . DS . 'result.odt');
        $content = $file->read();

        $file->close();
        $folder->delete();
        return $content;
    }

    /**
     * @param $folder
     * @param $aPagePng
     */
    public static function generateOdtFileWithImages(&$folder, $aPagePng)
    {
        App::import('Vendor', 'phpodt/phpodt');
        $odt = ODT::getInstance(true, $folder->pwd() . DS . 'result.odt');
        $pageStyleP = new PageStyle('myPageStylePortrait', 'Standard');
        $pageStyleP->setOrientation(StyleConstants::PORTRAIT);
        $pageStyleP->setHorizontalMargin('0cm', '0cm');
        $pageStyleP->setVerticalMargin('0cm', '0cm');
        $pStyleP = new ParagraphStyle('myPStyleP', 'Standard');
        $pStyleP->setBreakAfter(StyleConstants::PAGE);
        $pageStyleL = new PageStyle('myPageStyleLandscape', 'Landscape');
        $pageStyleL->setOrientation(StyleConstants::LANDSCAPE);
        $pageStyleL->setHorizontalMargin('0cm', '0cm');
        $pageStyleL->setVerticalMargin('0cm', '0cm');
        $pStyleL = new ParagraphStyle('myPStyleL', 'Landscape');

        $pStyleL->setBreakBefore(StyleConstants::PAGE);


        foreach ($aPagePng as $keyPage => $page) {
            if ($page['orientation'] == 'landscape') {
                $p = new Paragraph($pStyleL);
                $p->addImage($page['path'], '29.7cm', '21cm', true, $page['name'], 'paragraph');
            } else {
                $p = new Paragraph($pStyleP);
                $p->addImage($page['path'], '21cm', '29.7cm', true, $page['name'], 'paragraph');
            }
        }
        $odt->output();
    }
}
