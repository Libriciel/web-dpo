<?php
App::uses('LibricielChecksCake', 'LibricielChecks.Utility');
App::uses('LibricielChecksFilesystem', 'LibricielChecks.Utility');
App::uses('LibricielChecksPhp', 'LibricielChecks.Utility');
App::uses('LibricielChecksServer', 'LibricielChecks.Utility');
App::uses('LibricielChecksValidator', 'LibricielChecks.Utility');

abstract class WebdpoChecks
{

    public static function results()
    {
        return array_merge(
            static::environment(),
            static::php(),
            static::app(),
            static::services()
        );
    }
    
    public static function environment()
    {
        return [
            'Environment' => [
                'binaries' => LibricielChecksFilesystem::checkBinaries(
                    [
                        Configure::read('PDFTK_EXEC'),
                        Configure::read('PDFINFO_EXEC')
                    ]
                ),
                'directories' => LibricielChecksFilesystem::checkPermissions(
                    [
                        TMP => 'rwx'
                    ]
                ),
                'files' => LibricielChecksFilesystem::checkPermissions(
                    [
                        CONFIG.'database.php' => 'r',
                        CONFIG.'email.php' => 'r',
                        CONFIG.'webdpo.inc' => 'r'
                    ]
                ),
                'cache' => LibricielChecksCake::checkCacheUsage(),
                'cache_check' => LibricielChecksCake::checkCachePermissions(),
                'freespace' => LibricielChecksFilesystem::checkFreeSpace(
                    [
                        // 1. Répertoire temporaire de CakePHP
                        TMP,
                        // 2. Répertoire temporaire pour les conversions.
                        sys_get_temp_dir()
                        // 3. Répertoire temporaire pour les PDF.
//                        Configure::read( 'Cohorte.dossierTmpPdfs' ),
                        // 4. Répertoire de cache des wsdl
//                        ini_get( 'soap.wsdl_cache_dir' ),                        
                    ]
                )
            ]
        ];
    }
    
    /**
     * Vérifications concernant PHP:
     *	- la version utilisée
     *	- les extensions nécessaires
     *	- les variables du php.ini nécessaires
     *
     * @return array
     * @access protected
     */
    public static function php()
    {
        $result = [
            'Php' => [
                'informations' => [
                    'Version' => LibricielChecksValidator::version(
                        'PHP',
                        PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION . '.' . PHP_RELEASE_VERSION,
                        '7.2.0',
                        '7.5.0'
                    )
                ],
                'extensions' => LibricielChecksPhp::checkExtensions(
                    [
                        'curl',
                        'dom',
                        'gd',
                        'imagick',
                        'ldap',
                        'mbstring',
                        'mcrypt',
                        'pgsql',
                        'soap',
                        'xml',
                        'xsl',
                        'zip'
                    ]
                ),
                'pear_extensions' => LibricielChecksPhp::checkPear(
                    [
                        'xml_rpc2'
                    ]
                )
            ]
        ];

        // En ligne de commande, les configurations ini sont potentiellement différentes, donc on n'en vérifie qu'une
        if (strpos(PHP_SAPI, 'cli') !== false) {
                $result['Php']['inis'] = LibricielChecksPhp::checkInis(
                    [
                        'date.timezone' => [
                            ['rule' => ['notBlank']]
                        ]
                    ]
                );
            } else  {
                $result['Php']['inis'] = LibricielChecksPhp::checkInis(
                    [
                        'date.timezone' => [
                            ['rule' => ['notBlank']]
                        ],
                        'expose_php' => [
                            ['rule' => ['blank']]
                        ],
                        'file_uploads' => [
                            [
                                'rule' => ['comparison', '==', true],
                                'allowEmpty' => false
                            ]
                        ],
                        'max_input_vars' => [
                            [
                                'rule' => ['comparison', '>=', 1000],
                                'allowEmpty' => false
                            ]
                        ],
                        'max_execution_time' => [
                            [
                                'rule' => ['comparison', '>=', 3600],
                                'allowEmpty' => false
                            ]
                        ],
                        'post_max_size' => [
                            [
                                'rule' => ['bytesizeComparison', '>=', '50M'],
                                'message' => 'Veuillez entrer une valeur %s %s',
                                'allowEmpty' => false
                            ]
                        ],
                        'session.gc_maxlifetime' => [
                            [
                                'rule' => ['comparison', '>=', 14400],
                                'allowEmpty' => false
                            ]
                        ],
                        'upload_max_filesize' => [
                            [
                                'rule' => ['bytesizeComparison', '>=', '50M'],
                                'message' => 'Veuillez entrer une valeur %s %s',
                                'allowEmpty' => false
                            ]
                        ],
                    ]
                );
            }

        return $result;
    }

    public static function app()
    {
        return [
            'WebDPO' => [
                'configure' => LibricielChecksCake::checkConfigured(static::configureKeys()),
                'define' => LibricielChecksPhp::checkDefined(static::defineKeys())
            ]
        ];
    }

    public static function configureKeys()
    {
        return [
            'FusionConv.Gedooo.wsdl' => [
                ['rule' => 'string', 'allowEmpty' => false],
                ['rule' => 'url', 'allowEmpty' => false]
            ],
            'FusionConv.method' => [
				['rule' => 'string', 'allowEmpty' => false],
                ['rule' => 'inList', ['GedoooCloudooo', 'GedoooUnoconv']]
            ],
            'FusionConv.cloudooo_host' => [
                ['rule' => 'string', 'allowEmpty' => false],
                ['rule' => 'url', 'allowEmpty' => false]
            ],
            'FusionConv.cloudooo_port' => [
                ['rule' => 'integer', 'allowEmpty' => false]
            ],
            'FusionConv.FusionConvConverterCloudooo.xml_rpc_class' => [
                ['rule' => 'integer', 'allowEmpty' => false],
                ['rule' => 'inList', [1, 2], 'allowEmpty' => false]
            ],
            'FusionConv.FusionConvConverterCloudooo.server' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'FusionConv.FusionConvConverterCloudooo.port' => [
                ['rule' => 'integer', 'allowEmpty' => false]
            ],
            'GS_RESOLUTION' => [
                ['rule' => 'integer', 'allowEmpty' => false],
                ['rule' => 'range', 50, 500, 'allowEmpty' => false],
            ],
            'PDFTK_EXEC' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'PDFINFO_EXEC' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
        ];
    }

    public static function defineKeys()
    {
        return [
            'FICHIER' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'PIECE_JOINT' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_PIECE_JOINT' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'PIECE_JOINT_TMP' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_PIECE_JOINT_TMP' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'MODELES' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_MODELES' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'EXTRAIT_REGISTRE' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_MODELES_EXTRAIT' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'REGISTRE' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_REGISTRE' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'NORMES' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_NORMES' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CONNECTEURS' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_CONNECTEURS' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'AUTHENTIFICATION' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_CONNECTEURS_AUTHENTIFICATION' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'EXPORTS' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'CHEMIN_EXPORTS' => [
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'FORMAT_DATE' => [//@todo
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'FORMAT_DATE_HEURE' => [//@todo
                ['rule' => 'string', 'allowEmpty' => false]
            ],
            'REGEXP_ALPHA_FR' => [//@todo: traduction de Validate::preg_pattern
                ['rule' => 'string', 'allowEmpty' => false],
                ['rule' => 'preg_pattern', 'allowEmpty' => false]
            ],
            'REGEXP_EMAIL_FR' => [
                ['rule' => 'string', 'allowEmpty' => false],
                ['rule' => 'preg_pattern', 'allowEmpty' => false]
            ]
        ];
    }

    protected static function _fusion($path)
    {
        App::uses('FusionConvBuilder', 'FusionConv.Utility');
        $MainPart = new phpgedooo_client\GDO_PartType();

        $Fusion = new phpgedooo_client\GDO_FusionType(
            new phpgedooo_client\GDO_ContentType(
                "",
                basename($path),
                "application/vnd.oasis.opendocument.text",
                "binary",
                file_get_contents($path)
            ),
            'application/vnd.oasis.opendocument.text',
            FusionConvBuilder::main($MainPart, [], [], [])
        );

        $reporting = error_reporting();
        error_reporting(0);
        @$Fusion->process();
        error_reporting($reporting);
        $content = $Fusion->getContent();

        if ((isset($content->binary) && preg_match('/^PK/m', $content->binary) === 1) === false) {
            $msgstr = 'Erreur lors de la fusion: '.isset($content->Message) ? $content->Message : 'erreur inconnue';
            throw new RuntimeException($msgstr);
        }
        
        return $content->binary;
    }

    protected static function _testFusion($path)
    {
        $return = [
            'success' => false,
            'value' => $path,
            'message' => null
        ];

        try {
            $content = static::_fusion($path);
            $odt = strpos($content, "PK") === 0
                && strpos($content, 'mimetypeapplication/vnd.oasis.opendocument.text') !== false;
            if ($odt === true) {
                $return['success'] = true;
            } else {
                $return['success'] = false;
                $return['message'] = 'Le document généré ne ressemble pas à un fichier ODT';
            }
        } catch(Exception $e) {
            $return['success'] = false;
            $return['message'] = $e->getMessage();
        }

        return $return;
    }
    protected static function _testConversion($path)
    {
        $return = [
            'success' => false,
            'value' => $path,
            'message' => null
        ];

        try {
            $content = static::_fusion($path);
            $pdf = '';

            switch(Configure::read('FusionConv.method')) {
                case 'GedoooCloudooo':
                    App::uses('FusionConvConverterCloudooo', 'FusionConv.Utility/Converter');
                    $pdf = FusionConvConverterCloudooo::convert($content);
                    break;
                case 'GedoooUnoconv':
                    App::uses('FusionConvConverterUnoconv', 'FusionConv.Utility/Converter');
                    $pdf = FusionConvConverterUnoconv::convert($content);
                    break;
                default:
                    $msgstr = 'La méthode de conversion %s n\'est pas prise en charge.';
                    throw new RuntimeException(sprintf($msgstr, 'FusionConv.method'));
            }
            
            if (preg_match('/^%PDF\-[0-9]/m', $pdf) === 1) {
                $return['success'] = true;
            } else {
                $return['success'] = false;
                $return['message'] = 'Erreur inconnue lors de la conversion';
            }
        } catch(Exception $e) {
            $return['success'] = false;
            $return['message'] = $e->getMessage();
        }

        return $return;
    }

    public static function services()
    {
        $gedoooTestFile = CakePlugin::path('FusionConv').'Config'.DS.'OdtVide.odt';

        return [
            'Services' => [
                'Gedooo' => [
                    'tests' => [
                        'Accès au WebService' => LibricielChecksServer::webservice(Configure::read('FusionConv.Gedooo.wsdl')),
                        'Présence du modèle de test' => LibricielChecksFilesystem::checkPermissions($gedoooTestFile, 'r'),
                        'Test de fusion' => static::_testFusion($gedoooTestFile)
                    ]
                ],
                'Cloudooo' => [
                    'tests' => [
                        'Accès au serveur cloudooo de FusionConv' => LibricielChecksServer::socket(
                            Configure::read('FusionConv.cloudooo_host'),
                            Configure::read('FusionConv.cloudooo_port')
                        ),
                        'Accès au serveur de FusionConv.FusionConvConverterCloudooo' => LibricielChecksServer::socket(
                            Configure::read('FusionConv.FusionConvConverterCloudooo.server'),
                            Configure::read('FusionConv.FusionConvConverterCloudooo.port')
                        ),
                        'Test de conversion' => static::_testConversion($gedoooTestFile)
                    ]
                ]
            ]
        ];
    }
}
