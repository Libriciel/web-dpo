<?php

/**
 * FormulairesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class FormulairesController extends AppController
{
    public $uses = [
        'Champ',
        'Formulaire',
        'FormulaireOrganisation',
        'Norme',
        'Referentiel',
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        if ($this->Droits->isSu() === true || true !== $this->Droits->authorized([ListeDroit::GESTION_FORMULAIRE])) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     *
     * @created 18/06/2015
     * @version V1.0.0
     *
     * @modified 26/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @modified 10/11/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function index()
    {
        $this->set('title', __d('formulaire', 'formulaire.titreListeFormulaire'));

        $conditions = [
            'Formulaire.archive' => 'false'
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        if ($this->request->is('post') || false === empty($search)) {
            $data = $this->request->data;

            // Applications des filtres
            // Filtrer par entité associée
            if (!empty($data['Filtre']['organisation'])) {
                $conditions[] = $this->Formulaire->getFormulaireConditionOrganisation(
                    $data['Filtre']['organisation']
                );
            }

            // Filtrer par entité créatrice
            $createdbyorganisation = (string)Hash::get($data, 'Filtre.createdbyorganisation');
            if ($createdbyorganisation !== '') {
                $conditions['Formulaire.createdbyorganisation'] = $createdbyorganisation;
            }

            // Filtrer par formulaire actif
            if (!empty($data['Filtre']['actif'])) {
                $conditions['Formulaire.active'] = $data['Filtre']['actif'];
            }

            // Filtrer par formulaire RT externe
            if (!empty($data['Filtre']['rt_externe'])) {
                $conditions['Formulaire.rt_externe'] = $data['Filtre']['rt_externe'];
            }

            // Filtrer par formulaire archive
            if (!empty($data['Filtre']['archive'])) {
                $conditions['Formulaire.archive'] = $data['Filtre']['archive'];
            }

            if (isset($data['FormulaireOrganisation']) &&
                empty($data['FormulaireOrganisation']['organisation_id']) === false &&
                empty($data['FormulaireOrganisation']['formulaire_id']) === false
            ) {
                $success = true;
                $this->Formulaire->FormulaireOrganisation->begin();
                $mesOrganisations_ids = array_keys($this->WebcilUsers->mesOrganisations('list'));

                // On vérifie que l'utilisateur a bien les droits sur les organsations sélectionner
                if (!empty(array_intersect($data['FormulaireOrganisation']['organisation_id'], $mesOrganisations_ids)) === true ) {
                    $organisations_ids = Hash::extract($this->request->data, 'FormulaireOrganisation.organisation_id');
                    $formulairesSelected_ids = Hash::extract($this->request->data, 'FormulaireOrganisation.formulaire_id');

                    foreach ($organisations_ids as $organisation_id) {
                        if ($success == false) {
                            break;
                        }

                        $organisationAssocieFormulaires = $this->Formulaire->FormulaireOrganisation->find('list', [
                            'conditions' => [
                                'organisation_id' => $organisation_id
                            ],
                            'fields' => [
                                'FormulaireOrganisation.id',
                                'FormulaireOrganisation.formulaire_id'
                            ]
                        ]);

                        // On récupère uniquement les formulaires qui ne sont pas encore associés à l'organisation
                        $result = array_intersect($formulairesSelected_ids, $organisationAssocieFormulaires);
                        $dataFormulaires_ids = array_diff($formulairesSelected_ids, $result);

                        if (!empty($dataFormulaires_ids)) {
                            $etatFormulaires = $this->Formulaire->find('list', [
                                'conditions' => [
                                    'Formulaire.id' => $dataFormulaires_ids
                                ],
                                'fields' => [
                                    'Formulaire.id',
                                    'Formulaire.active'
                                ]
                            ]);

                            foreach ($dataFormulaires_ids as $dataFormulaire_id) {
                                if ($success == false) {
                                    break;
                                }

                                $data = [
                                    'FormulaireOrganisation' => [
                                        'formulaire_id' => $dataFormulaire_id,
                                        'organisation_id' => $organisation_id,
                                        'active' => (int)$etatFormulaires[$dataFormulaire_id]
                                    ]
                                ];

                                $this->Formulaire->FormulaireOrganisation->create($data);
                                $success = $success && false !== $this->Formulaire->FormulaireOrganisation->save(null, ['atomic' => false]);
                            }
                        }
                    }

                    if ($success == true) {
                        $this->Formulaire->FormulaireOrganisation->commit();
                        $this->Session->setFlash(
                            __d('formulaire', 'formulaire.flashsuccessFormulaireAffecterEnregistrer'),
                            'flashsuccess'
                        );
                    } else {
                        $this->Formulaire->FormulaireOrganisation->rollback();
                        $this->Session->setFlash(
                            __d('formulaire', 'formulaire.flasherrorErreurEnregistrementFormulaireAffecter'),
                            'flasherror'
                        );
                    }
                }
            }

            unset($this->request->data['FormulaireOrganisation']);
        }

        if (empty($this->request->data['Filtre']['archive'])) {
            $this->request->data['Filtre']['archive'] = 'false';
        }

        $this->set([
            'mesOrganisations' => $this->WebcilUsers->mesOrganisations('list'),
            'formulaires' => $this->_getSearchResults($conditions),
        ]);
    }

    public function _getSearchResults($conditions)
    {
        $paginate = [
            'contain' => [
                'FormulaireOrganisation' => [
                    'Organisation' => [
                        'id',
                        'raisonsociale',
                        'order' => ['raisonsociale']
                    ]
                ],
            ],
            'conditions' => $conditions,
            'fields' => [
                'Formulaire.id',
                'Formulaire.libelle',
                'Formulaire.description',
                'Formulaire.rt_externe',
                'Formulaire.createdbyorganisation',
                'Formulaire.created',
                'Formulaire.archive'
            ],
            'order' => [
                'Formulaire.libelle ASC'
            ]
        ];

        // Ajout de conditions suivant l'utilisateur connecté et l'action
        if ($this->request->params['action'] === 'index') {
            array_push($paginate['fields'],
                'Formulaire.active',
                'Formulaire.oldformulaire',
                $this->Formulaire->vfLinkedFichesCount()
            );
        }

        if ($this->request->params['action'] === 'entite') {
            $paginate['conditions'][] = $this->Formulaire->getFormulaireConditionOrganisation($this->Session->read('Organisation.id'));
        }

        $this->paginate = $paginate;
        return $this->paginate($this->Formulaire);
    }

    /**
     * @access public
     *
     * @created 10/11/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function entite()
    {
        $this->set('title', __d('formulaire', 'formulaire.titreMesFormulairesOrganisation'));

        $conditions[] = $this->Formulaire->getFormulaireConditionOrganisation(
            $this->Session->read('Organisation.id')
        );

        $this->set([
            'formulaires' => $this->_getSearchResults($conditions),
        ]);
    }

    /**
     * Permet de dupliquer un formulaire qui est vérouiller
     *
     * @access public
     * @created 26/04/2016
     * @version V1.0.0
     *
     * @modified 18/06/2020
     */
    public function dupliquer()
    {
        if ($this->request->is('post') === false && $this->request->is('put') === false) {
            throw new MethodNotAllowedException();
        }

        $findFormulaireToDuplicate = $this->Formulaire->find('count', [
            'conditions' => [
                'id' => $this->request->data['Formulaire']['id']
            ]
        ]);

        if ($findFormulaireToDuplicate === 0) {
            throw new NotFoundException();
        }

        $this->Formulaire->begin();

        $success = $this->copyFormulaire($this->request->data);

        if ($success == true) {
            $this->Formulaire->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessFormulaireDupliquer'),
                'flashsuccess'
            );
        } else {
            $this->Formulaire->rollback();
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Dupliquer un formulaire d'un organisation à une autre en tant que DPO
     * dans une collectivité ou on a les droits.
     *
     * @access public
     * @created 18/05/2017
     * @version v1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @modified 18/06/2020
     *
     * @deprecated v2.1.0
     */
    public function dupliquerOrganisation()
    {
        if ($this->request->is('post') === false && $this->request->is('put') === false) {
            throw new MethodNotAllowedException();
        }

        $this->Formulaire->begin();

        $success = $this->copyFormulaire($this->request->data);

        if ($success == true) {
            $this->Formulaire->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessFormulaireDupliquerOtherOrganisation'),
                'flashsuccess'
            );
        } else {
            $this->Formulaire->rollback();
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @param $data
     * @return bool
     *
     * @access protected
     * @version v2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @created 18/06/2020
     */
    protected function copyFormulaire($data)
    {
        $success = true;

        $id = $data['Formulaire']['id'];

        $formulaireToDuplicate = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $id,
            ],
            'fields' => [
                'soustraitant',
                'usesousfinalite',
                'usebaselegale',
                'usedecisionautomatisee',
                'usetransferthorsue',
                'usedonneessensible',
                'useallextensionfiles',
                'usepia'
            ]
        ]);

        if (!empty($formulaireToDuplicate)) {
            // C'est un nouveau formulaire en renseignant les infos
            $this->Formulaire->create([
                'libelle' => $data['Formulaire']['libelle'],
                'active' => false,
                'description' => $data['Formulaire']['description'],
                'soustraitant' => $formulaireToDuplicate['Formulaire']['soustraitant'],
                'usesousfinalite' => $formulaireToDuplicate['Formulaire']['usesousfinalite'],
                'usebaselegale' => $formulaireToDuplicate['Formulaire']['usebaselegale'],
                'usedecisionautomatisee' => $formulaireToDuplicate['Formulaire']['usedecisionautomatisee'],
                'usetransferthorsue' => $formulaireToDuplicate['Formulaire']['usetransferthorsue'],
                'usedonneessensible' => $formulaireToDuplicate['Formulaire']['usedonneessensible'],
                'useallextensionfiles' => $formulaireToDuplicate['Formulaire']['useallextensionfiles'],
                'usepia' => $formulaireToDuplicate['Formulaire']['usepia'],
                'rt_externe' => $data['Formulaire']['rt_externe'],
                'createdbyorganisation' => $this->Session->read('Organisation.id')
            ]);

            // On enregistre le formualire
            $success = $success && false !== $this->Formulaire->save(null, ['atomic' => false]);
        } else {
            $success = false;
        }

        if ($success == true) {
            // On recupere l'id du formulaire qu'on vien d'enregistré
            $idForm = $this->Formulaire->getLastInsertId();

            // On recupere en BDD tout les champs qui corresponde a $id
            $champs = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $id
                ]
            ]);

            foreach ($champs as $champ) {
                // On cree un nouveau champs avec l'id du nouveau formulaire qu'on a cree et les info qu'on a décodé
                $this->Champ->create([
                    'formulaire_id' => $idForm,
                    'type' => $champ['Champ']['type'],
                    'ligne' => $champ['Champ']['ligne'],
                    'colonne' => $champ['Champ']['colonne'],
                    'details' => $champ['Champ']['details'],
                    'champ_coresponsable' => $champ['Champ']['champ_coresponsable'],
                    'champ_soustraitant' => $champ['Champ']['champ_soustraitant'],
                ]);

                // On enregistre le champ
                $success = $success && false !== $this->Champ->save(null, ['atomic' => false]);
            }
        }

        return $success;
    }

    /**
     * Supprime un formulaire
     *
     * @param int $formulaire_id
     *
     * @access public
     *
     * @created 18/06/2015
     * @version V1.0.0
     *
     * @modified 26/11/2020
     * @version V2.1.0
     */
    public function delete($formulaire_id)
    {
        if ($this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $formulaire = $this->Formulaire->find('first', [
            'contain' => [
                'FormulaireOrganisation'
            ],
            'conditions' => [
                'Formulaire.id' => $formulaire_id,
                'Formulaire.active' => false,
            ],
            'fields' => [
                'Formulaire.id',
                'Formulaire.libelle',
                'Formulaire.description',
                'Formulaire.rt_externe',
                'Formulaire.createdbyorganisation',
                'Formulaire.created',
                'Formulaire.active',
                'Formulaire.oldformulaire',
                $this->Formulaire->vfLinkedFichesCount()
            ],
        ]);

        if (empty($formulaire)) {
            throw new NotFoundException();
        }

        if (empty($formulaire['FormulaireOrganisation']) &&
            $formulaire['Formulaire']['fiches_count'] === 0 &&
            $formulaire['Formulaire']['createdbyorganisation'] === $this->Session->read('Organisation.id')
        ){

            $success = true;
            $this->Formulaire->begin();

            $success = $success && false !== $this->Formulaire->delete($formulaire_id, ['atomic' => false]);


            if ($success == true) {
                $this->Formulaire->commit();
                $this->Session->setFlash(
                    __d('formulaire', 'formulaire.flashsuccessFormulaireSupprimer'),
                    'flashsuccess'
                );
            } else {
                $this->Formulaire->rollback();
                $this->Session->setFlash(
                    __d('default', 'default.flasherrorEnregistrementErreur'),
                    'flasherror'
                );
            }
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->redirect($this->referer());
    }

    /**
     * @access public
     * @created 18/06/2015
     * @version V1.0.0
     */
    public function addFirst()
    {
        if ($this->request->is('post') === false &&
            empty($this->request->data)
        ) {
            throw new MethodNotAllowedException();
        }

        $success = true;
        $this->Formulaire->begin();

        $data = $this->request->data;
        $data['Formulaire']['createdbyorganisation'] = $this->Session->read('Organisation.id');

        $this->Formulaire->create($data);
        $success = $success && false !== $this->Formulaire->save(null, ['atomic' => true]);

        if ($success == true) {
            $this->redirect([
                'controller' => 'formulaires',
                'action' => 'add',
                $this->Formulaire->getInsertId()
            ]);
        } else {
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }

        $this->redirect([
            'controller' => 'formulaires',
            'action' => 'index'
        ]);
    }

    /**
     * Ajout des champs et options d'un formulaire
     *
     * @param int $id ID du formulaire ajouter dans la fonction addFirst()
     *
     * @access public
     *
     * @created 18/06/2015
     * @edit 24/12/2015
     * @version V1.0.0
     *
     * @modified 26/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function add($id)
    {
        return $this->edit($id);
    }

    /**
     * @param int $id
     * @param bool $state
     *
     * @access public
     *
     * @created 18/06/2015
     * @edit 24/12/2015
     * @version V1.0.0
     */
    public function toggle($id, $state = null)
    {
        $success = true;
        $this->Formulaire->begin();

        $this->Formulaire->id = $id;

        $success = $this->Formulaire->updateAll(
            [
                'active' => (int) !$state
            ],
            [
                'id' => $id
            ]
        ) !== false;

        if ($success == true) {
            $success = $success && $this->_toggleFormlaire($id, $state);
        }

        if ($success == true) {
            $this->Formulaire->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessFormulaireEnregistrer'),
                'flashsuccess'
            );

            $this->redirect($this->Referers->get());
        } else {
            $this->Formulaire->rollback();
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }
    }

    public function archive($id, $state = null)
    {
        $query = [
            'contain' => [
                'FormulaireOrganisation' => [
                    'Organisation' => [
                        'id',
                        'raisonsociale',
                        'order' => ['raisonsociale']
                    ]
                ],
            ],
            'conditions' => [
                'Formulaire.id' => $id
            ],
            'fields' => [
                'Formulaire.id',
                'Formulaire.libelle',
                'Formulaire.description',
                'Formulaire.rt_externe',
                'Formulaire.createdbyorganisation',
                'Formulaire.created',
                'Formulaire.archive'
            ],
        ];
        $formulaire = $this->Formulaire->find('first', $query);

        if (!empty($formulaire) &&
            empty($formulaire['FormulaireOrganisation'])
        ) {
            $success = true;
            $this->Formulaire->begin();

            $this->Formulaire->id = $id;

            if ($formulaire['Formulaire']['archive'] === false) {
                $success = $success && $this->Formulaire->updateAll([
                        'archive' => true,
                        'active' => false
                    ], [
                        'id' => $id
                ]) !== false;
            } else {
                $success = $success && $this->Formulaire->updateAll([
                        'archive' => false,
                    ], [
                        'id' => $id
                ]) !== false;
            }
        } else {
            $success = false;
        }

        if ($success == true) {
            $this->Formulaire->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessFormulaireEnregistrer'),
                'flashsuccess'
            );

            $this->redirect($this->Referers->get());
        } else {
            $this->Formulaire->rollback();
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }
    }

    public function toggleFormulaire($formulaire_id, $state = null)
    {
        $success = true;
        $this->FormulaireOrganisation->begin();

        $success = $success && $this->_toggleFormlaire($formulaire_id, $state);

        if ($success == true) {
            $this->FormulaireOrganisation->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessFormulaireEnregistrer'),
                'flashsuccess'
            );

            $this->redirect($this->Referers->get());
        } else {
            $this->FormulaireOrganisation->rollback();
            $this->Session->setFlash(
                __d('default', 'default.flasherrorEnregistrementErreur'),
                'flasherror'
            );
        }
    }

    private function _toggleFormlaire($formulaire_id, $state)
    {
        return $this->FormulaireOrganisation->updateAll(
            [
                'active' => (int) !$state
            ],
            [
                'formulaire_id' => $formulaire_id
            ]
        ) !== false;
    }

    public function dissocier($formulaire_id)
    {
        $this->FormulaireOrganisation->begin();

        $success = false !== $this->FormulaireOrganisation->deleteAll([
                'FormulaireOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
                'FormulaireOrganisation.formulaire_id' => $formulaire_id
            ]);

        if ($success == true) {
            $this->FormulaireOrganisation->commit();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flashsuccessDissocier'),
                'flashsuccess'
            );
        } else {
            $this->TypageOrganisation->rollback();
            $this->Session->setFlash(
                __d('formulaire', 'formulaire.flasherrorErreurDissocier'),
                'flasherror'
            );
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Edition des champs et options d'un formulaire
     *
     * @param int $id ID du formulaire ajouter dans la fonction addFirst()
     *
     * @access public
     *
     * @version V1.0.0
     *
     * @modified 26/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function edit($id)
    {
        $query = [
            'fields' => [
                'Formulaire.libelle',
                'Formulaire.active',
                'Formulaire.oldformulaire',
                'Formulaire.rt_externe',
                'Formulaire.createdbyorganisation',
                'Formulaire.archive',
                $this->Formulaire->vfLinkedFichesCount()
            ],
            'conditions' => ['Formulaire.id' => $id]
        ];
        $formulaire = $this->Formulaire->find('first', $query);

        if (empty($formulaire)) {
            throw new NotFoundException();
        }

        if ($formulaire['Formulaire']['active'] === true ||
            $formulaire['Formulaire']['oldformulaire'] === true ||
            $formulaire['Formulaire']['createdbyorganisation'] !== $this->Session->read('Organisation.id') ||
            $formulaire['Formulaire']['archive'] === true ||
            $formulaire['Formulaire']['fiches_count'] > 0
        ) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        if ($this->request->params['action'] === 'add') {
            $title = __d('formulaire', 'formulaire.titreCreerFormulaire');
        } else {
            $title = __d('formulaire', 'formulaire.titreEditerFormulaire');
        }
        $this->set('title', $title . $formulaire['Formulaire']['libelle']);
        $this->set('rt_externe', $formulaire['Formulaire']['rt_externe']);

        if ($this->request->is('POST') || $this->request->is('PUT')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->Formulaire->begin();

            $data = $this->request->data;
            unset($data['Fiche'], $data['Trash']);

            $formulaireEdit = $this->Formulaire->find('first', [
                'conditions' => [
                    'Formulaire.id' => $id
                ],
                'fields' => [
                    'Formulaire.rt_externe'
                ]
            ]);

            $this->Formulaire->create([
                'id' => $id,
                'usesousfinalite' => $data['Formulaire']['usesousfinalite'],
                'usebaselegale' => $data['Formulaire']['usebaselegale'],
                'usedecisionautomatisee' => $data['Formulaire']['usedecisionautomatisee'],
                'usetransferthorsue' => $data['Formulaire']['usetransferthorsue'],
                'usedonneessensible' => $data['Formulaire']['usedonneessensible'],
                'useallextensionfiles' => $data['Formulaire']['useallextensionfiles'],
                'usepia' => $data['Formulaire']['usepia'],
                'rt_externe' => $formulaireEdit['Formulaire']['rt_externe']
            ]);
            $success = false !== $this->Formulaire->save(null, ['atomic' => false]) && $success;

            if ($success === true) {
                $success = false !== $this->Champ->deleteAll(['formulaire_id' => $id], ['atomic' => false]);
            }

            if ($success == true) {
                if (isset($data['Formulaire']['form-container-formulaire'])) {
                    foreach ($data['Formulaire']['form-container-formulaire'] as $fields) {
                        $success = $this->saveFields($id, $fields);
                    }
                }

                if (isset($data['Formulaire']['form-container-coresponsable']) && $success == true) {
                    foreach ($data['Formulaire']['form-container-coresponsable'] as $fields) {
                        $success = $this->saveFields($id, $fields, true);
                    }
                }

                if (isset($data['Formulaire']['form-container-soustraitant']) && $success == true) {
                    foreach ($data['Formulaire']['form-container-soustraitant'] as $fields) {
                        $success = $this->saveFields($id, $fields, false, true);
                    }
                }
            }

            if ($success == true) {
                $this->Formulaire->commit();
                $this->Session->setFlash(
                    __d('formulaire', 'formulaire.flashsuccessFormulaireEnregistrer'),
                    'flashsuccess'
                );

                $this->redirect([
                    'controller' => 'formulaires',
                    'action' => 'index'
                ]);
            } else {
                $this->Formulaire->rollback();
                $this->Session->setFlash(
                    __d('default', 'default.flasherrorEnregistrementErreur'),
                    'flasherror'
                );
            }
        }

        $this->getFields($id);

        if ($formulaire['Formulaire']['rt_externe'] === true) {
            $this->getOrganisationForSt();
        }

        $usefieldsredacteur = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usefieldsredacteur'
            ]
        ]);
        $usefieldsredacteur = $usefieldsredacteur['Organisation']['usefieldsredacteur'];
        $this->set(compact('usefieldsredacteur'));

        $this->_getReferentiel();
        $this->_getNormes();

        $this->view = 'edit';
    }

    private function _getReferentiel()
    {
        $options_referentiels = $this->Referentiel->find('list', [
            'conditions' => [
                'abroger' => false
            ],
            'fields' => [
                'id',
                'name'
            ],
            'order' => [
                'name ASC'
            ]
        ]);
        $this->set(compact('options_referentiels'));
    }

    private function _getNormes()
    {
        $query = [
            'conditions' => ['abroger' => false],
            'fields' => ['id', 'norme', 'numero', 'libelle'],
            'order' => ['norme', 'numero']
        ];
        $normes = $this->Norme->find('all', $query);

        $options_normes = Hash::combine(
            $normes,
            '{n}.Norme.id',
            [
                '%s-%s : %s',
                '{n}.Norme.norme',
                '{n}.Norme.numero',
                '{n}.Norme.libelle'
            ]
        );
        $this->set(compact('options_normes'));
    }

    private function begnWith($str, $begnString)
    {
        $len = strlen($begnString);
        return (substr($str, 0, $len) === $begnString);
    }

    private function saveFields($formulaire_id, $fields, $ongletChampCoresponsable = false, $ongletChampSoustraitant = false)
    {
        $success = true;

        $typeFields = [
            'input',
            'textarea',
            'date',
            'checkboxes',
            'radios',
            'deroulant',
            'multi-select',
            'title',
            'help',
            'texte',
        ];

        $valueConditionResult = [
            'shown',
            'hidden',
        ];

        foreach ($fields as $field) {
            $array = (array)json_decode($field['details'], true);
            $valeur = array_merge($field, $array);
            unset($valeur['details']);

            foreach ($valeur as $clef => $val) {
                if ($success == false) {
                    break;
                }

                switch ($clef) {
                    case 'type':
                        if (in_array($val, $typeFields)) {
                            $type = $val;
                        } else {
                            $success = false;
                        }
                        break;

                    case 'ligne':
                        $ligne = (int)round($val, 0);
                        break;

                    case 'colonne':
                        $colonne = (int)round($val, 0);
                        break;

                    case 'name':
                        $formatName = LettercaseFormattableBehavior::formatageVariable($val);

                        if ($ongletChampCoresponsable === true && $ongletChampSoustraitant === false) {
                            if ($this->begnWith($formatName, 'coresponsable_') === false) {
                                $formatName = 'coresponsable_'.$formatName;
                            }
                        }

                        if ($ongletChampSoustraitant === true && $ongletChampCoresponsable === false) {
                            if ($this->begnWith($formatName, 'soustraitant_') === false) {
                                $formatName = 'soustraitant_'.$formatName;
                            }
                        }

                        $sortie[$clef] = $formatName;
                        break;

                    case 'options':
                        $options = [];
                        foreach ($val as $key => $option) {
                            $options[$key] = trim($option);
                        }

                        $sortie[$clef] = $options;
                        break;

                    case 'default':
                        if (!is_array($val)) {
                            $sortie[$clef] = trim($val);
                        } else {
                            $options = [];
                            foreach ($val as $key => $option) {
                                $options[$key] = trim($option);
                            }

                            $sortie[$clef] = $options;
                        }
                        break;

                    case 'conditions':
                        if (empty($val)) {
                            break;
                        }

                        $conditionsChecked = [];
                        $conditions = json_decode($val, true);

                        if (empty($conditions)){
                            break;
                        }

                        foreach ($conditions as  $uuid => $condition) {
                            $condition = (array)$condition;

                            if (!isset($condition['ifTheField']) || $condition['ifTheField'] == '') {
                                $success = false;
                            }

                            if (!isset($condition['hasValue']) || $condition['hasValue'] == '') {
                                $success = false;
                            }

                            if (!isset($condition['thenTheField']) || $condition['thenTheField'] == '') {
                                $success = false;
                            }

                            if (!isset($condition['mustBe']) || in_array($condition['mustBe'], $valueConditionResult)  == false) {
                                $success = false;
                            }

                            if (!isset($condition['ifNot']) || in_array($condition['ifNot'], $valueConditionResult) == false) {
                                $success = false;
                            }

                            if ($success == true) {
                                $save = true;

                                // On recupère uniquement les champs qui nous interesse
                                $tmpCondition = [
                                    'ifTheField' => $condition['ifTheField'],
                                    'hasValue' => $condition['hasValue'],
                                    'thenTheField' => $condition['thenTheField'],
                                    'mustBe' => $condition['mustBe'],
                                    'ifNot' => $condition['ifNot'],
                                ];

                                if (!empty($conditionsChecked)) {
                                    // On vérifie dans les conditions près à être sauvegarder qu'il n'y a pas de doublon
                                    foreach ($conditionsChecked as $tmp) {
                                        if ($save == false) {
                                            break;
                                        }

                                        $diff_result = array_diff($tmpCondition, $tmp);
                                        if (empty($diff_result)) {
                                            $save = false;
                                        }
                                    }
                                }

                                if ($save == true) {
                                    $conditionsChecked[$uuid] = $tmpCondition;
                                }
                            }
                        }

                        $sortie[$clef] = json_encode($conditionsChecked, JSON_FORCE_OBJECT);
                        break;

                    default:
                        $sortie[$clef] = $val;
                        break;
                }
            }

            if ($success == false) {
                break;
            }
            $data = [
                'formulaire_id' => $formulaire_id,
                'type' => $type,
                'ligne' => $ligne,
                'colonne' => $colonne,
                'details' => json_encode($sortie),
                'champ_coresponsable' => $ongletChampCoresponsable,
                'champ_soustraitant' => $ongletChampSoustraitant,
            ];
            $this->Champ->create($data);
            $success = false !== $this->Champ->save(null, ['atomic' => false]) && $success;
        }

        return $success;
    }

    /**
     * Récupére en BDD les champs défini dans le formulaire en fonction des différents onglet
     * Ajoute les valeurs par défault défini dans le formulaire
     *
     * @param int $formulaire_id ID du formulaire utilisé
     * @param bool $formulaireOLD TRUE si le formulaire vien d'une version antérieur à la V2.0.0
     *
     * @access private
     *
     * @created 26/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getFields($formulaire_id, $formulaireOLD = false)
    {
        $formulaireFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => false,
                'champ_soustraitant' => false,
            ],
            'fields' => [
                'type',
                'ligne',
                'colonne',
                'details'
            ],
            'order' => [
                'colonne ASC',
                'ligne ASC'
            ]
        ]);

        if ($formulaireOLD === false) {
            $coresponsableFields = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $formulaire_id,
                    'champ_coresponsable' => true,
                    'champ_soustraitant' => false,
                ],
                'fields' => [
                    'type',
                    'ligne',
                    'colonne',
                    'details'
                ],
                'order' => [
                    'colonne ASC',
                    'ligne ASC'
                ]
            ]);

            $soustraitantFields = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $formulaire_id,
                    'champ_coresponsable' => false,
                    'champ_soustraitant' => true,
                ],
                'fields' => [
                    'type',
                    'ligne',
                    'colonne',
                    'details'
                ],
                'order' => [
                    'colonne ASC',
                    'ligne ASC'
                ]
            ]);
        } else {
            $coresponsableFields = [];
            $soustraitantFields = [];
        }

        $fields =  [
            'formulaire' =>  $formulaireFields,
            'coresponsable' =>  $coresponsableFields,
            'soustraitant' =>  $soustraitantFields,
        ];


        if (empty($this->request->data) || $this->request->params['action'] === 'show') {
            if ($formulaireOLD === false && $this->request->params['action'] === 'edit' || $this->request->params['action'] === 'show') {
                $this->request->data = $this->Formulaire->find('first', [
                    'conditions' => [
                        'id' => $formulaire_id
                    ],
                    'fields' => [
                        'usesousfinalite',
                        'usebaselegale',
                        'usedecisionautomatisee',
                        'usetransferthorsue',
                        'usedonneessensible',
                        'useallextensionfiles',
                        'usepia'
                    ]
                ]);
            }

            foreach ($fields as $field) {
                $details = Hash::extract($field, '{n}.Champ.details');
                foreach ($details as $detail) {
                    $detail = (array)json_decode($detail);

                    if (isset($detail['default']) === true) {
                        $this->request->data['Formulaire'][$detail['name']] = $detail['default'];
                    } else {
                        $this->request->data['Formulaire']['default'] = null;
                    }
                }
            }

            $this->request->data['Formulaire']['form-container-formulaire'] = $fields['formulaire'];
            $this->request->data['Formulaire']['form-container-coresponsable'] = $fields['coresponsable'];
            $this->request->data['Formulaire']['form-container-soustraitant'] = $fields['soustraitant'];
        }
    }


    /**
     * Permet de visualiser un formulaire
     *
     * @param int $id ID du formulaire
     *
     * @access public
     *
     * @created 03/11/2016
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function show($id)
    {
        $formulaire = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $id
            ],
            'fields' => [
                'libelle',
                'oldformulaire',
                'soustraitant',
                'rt_externe'
            ]
        ]);
        if (empty($formulaire)) {
            throw new NotFoundException();
        }

        $this->set('title', __d('formulaire', 'formulaire.titreShowFormulaire') . $formulaire['Formulaire']['libelle']);

        if ($formulaire['Formulaire']['rt_externe'] === true) {
            $this->getOrganisationForSt();
        }

        $formulaireOLD = $formulaire['Formulaire']['oldformulaire'];
        $soustraitantOLD = $formulaire['Formulaire']['soustraitant'];

        $this->getFields($id, $formulaireOLD);

        $usefieldsredacteur = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usefieldsredacteur'
            ]
        ]);
        $usefieldsredacteur = $usefieldsredacteur['Organisation']['usefieldsredacteur'];

        $this->set('rt_externe', $formulaire['Formulaire']['rt_externe']);
        $this->set(compact('formulaireOLD', 'soustraitantOLD', 'usefieldsredacteur'));

        $this->_getReferentiel();
        $this->_getNormes();
    }

    /**
     *
     *
     * @access public
     *
     * @created 01/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function getOrganisationForSt()
    {
        $organisation = $this->Organisation->find('first', [
            'joins' => [
                $this->Organisation->join('Dpo', ['type' => 'INNER']),
            ],
            'conditions' => [
                'Organisation.id' => $this->Session->read('Organisation.id'),
                'Dpo.id = Organisation.dpo'
            ],
            'fields' => [
                'Organisation.raisonsociale',
                'Organisation.telephone',
                'Organisation.fax',
                'Organisation.adresse',
                'Organisation.email',
                'Organisation.sigle',
                'Organisation.siret',
                'Organisation.ape',
                'Organisation.civiliteresponsable',
                'Organisation.prenomresponsable',
                'Organisation.nomresponsable',
                'Organisation.emailresponsable',
                'Organisation.telephoneresponsable',
                'Organisation.fonctionresponsable',
                'Organisation.numerodpo',
                $this->Organisation->Dpo->vfNomComplet(),
                'Dpo.email',
                'Dpo.telephonefixe',
                'Dpo.telephoneportable',
            ]
        ]);

        foreach ($organisation['Organisation'] as $key => $value) {
            $this->request->data['Trash']['st_organisation_'.$key] = $value;
        }

        foreach ($organisation['Dpo'] as $key => $value) {
            $this->request->data['Trash']['st_organisation_'.$key.'_dpo'] = $value;
        }
    }

    /**
     * Fonction ajax permettant la sauvegarde automatique d'un formulaire
     *
     * @param int $form_id
     * @return bool|null
     *
     * @access public
     *
     * @created  23/02/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function ajax_save_auto_formulaire($form_id)
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;

            if (isset($data)) {
                $success = true;
                $this->Formulaire->begin();
                unset($data['Fiche'], $data['Trash']);

                $emptyArrayFormualaire = array_search("", $data['Formulaire']) !== false;

                if ($emptyArrayFormualaire === false) {

                    $formulaireEdit = $this->Formulaire->find('first', [
                        'conditions' => [
                            'Formulaire.id' => $form_id
                        ],
                        'fields' => [
                            'Formulaire.rt_externe'
                        ]
                    ]);

                    $this->Formulaire->create([
                        'id' => $form_id,
                        'usesousfinalite' => $data['Formulaire']['usesousfinalite'],
                        'usebaselegale' => $data['Formulaire']['usebaselegale'],
                        'usedecisionautomatisee' => $data['Formulaire']['usedecisionautomatisee'],
                        'usetransferthorsue' => $data['Formulaire']['usetransferthorsue'],
                        'usedonneessensible' => $data['Formulaire']['usedonneessensible'],
                        'useallextensionfiles' => $data['Formulaire']['useallextensionfiles'],
                        'usepia' => $data['Formulaire']['usepia'],
                        'rt_externe' => $formulaireEdit['Formulaire']['rt_externe']
                    ]);
                    $success = false !== $this->Formulaire->save(null, ['atomic' => false]);
                }

                if ($success === true) {
                    $success = false !== $this->Champ->deleteAll(['formulaire_id' => $form_id], ['atomic' => false]);
                }

                if (isset($data['Formulaire']['form-container-formulaire']) && $success === true) {
                    foreach ($data['Formulaire']['form-container-formulaire'] as $fields) {
                        $success = $this->saveFields($form_id, $fields);
                    }
                }

                if (isset($data['Formulaire']['form-container-coresponsable']) && $success == true) {
                    foreach ($data['Formulaire']['form-container-coresponsable'] as $fields) {
                        $success = $this->saveFields($form_id, $fields, true);
                    }
                }

                if (isset($data['Formulaire']['form-container-soustraitant']) && $success == true) {
                    foreach ($data['Formulaire']['form-container-soustraitant'] as $fields) {
                        $success = $this->saveFields($form_id, $fields, false, true);
                    }
                }

                if ($success == true) {
                    $this->Formulaire->commit();
                } else {
                    $this->Formulaire->rollback();
                }

                return $success;
            }
        }

        return null;
    }

}
