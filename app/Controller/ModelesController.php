<?php

/**
 * ModelesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ListeDroit', 'Model');

class ModelesController extends AppController {

    public $uses = [
        'Modele',
        'Formulaire',
        'Champ',
        'Fiche',
        'Valeur',
        'Organisation',
        'User',
        'Responsable',
        'Coresponsable',
        'Soustraitant',
        'Soustraitance'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

//        $action = Inflector::underscore($this->request->params['action']);

        if ($this->Droits->isSu() === true || true !== $this->Droits->authorized([ListeDroit::GESTION_MODELE])) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     * @created 18/06/2015
     * @version V1.0.0
     */
    public function index()
    {
        $this->set('title', __d('modele', 'modele.titreListeModele'));

        if ($this->request->is('post')) {
            if (isset($this->request->data['FormulaireModeleOrganisation']) &&
                empty($this->request->data['FormulaireModeleOrganisation']['organisation_id']) === false &&
                empty($this->request->data['FormulaireModeleOrganisation']['modele_id']) === false
            ) {
                $data = $this->request->data;
                $success = true;
                $this->Modele->FormulaireModeleOrganisation->begin();

                $organisation_ids = Hash::extract($data, 'FormulaireModeleOrganisation.organisation_id');

                $modele = $this->Modele->find('first', [
                    'fields' => [
                        'Modele.formulaire_id'
                    ],
                    'conditions' => [
                        'Modele.id' => $data['FormulaireModeleOrganisation']['modele_id']
                    ]
                ]);

                foreach ($organisation_ids as $organisation_id) {
                    $data = [
                        'FormulaireModeleOrganisation' => [
                            'formulaire_id' => (int)$modele['Modele']['formulaire_id'],
                            'modele_id' => (int)$data['FormulaireModeleOrganisation']['modele_id'],
                            'organisation_id' => (int)$organisation_id
                        ]
                    ];

                    $this->Modele->FormulaireModeleOrganisation->create($data);
                    $success = $success && false !== $this->Modele->FormulaireModeleOrganisation->save(null, ['atomic' => false]);

                    if ($success === false) {
                        break;
                    }
                }

                if ($success === true) {
                    $this->Modele->FormulaireModeleOrganisation->commit();
                    $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleAffecterEnregistrer'), 'flashsuccess');
                } else {
                    $this->Modele->FormulaireModeleOrganisation->rollback();
                    $this->Session->setFlash(__d('modele', 'modele.flasherrorErreurEnregistrementModeleAffecter'), 'flasherror');
                }
            }

            unset($this->request->data['FormulaireModele']);
        }

        $this->set([
            'mesFormulaires' => $this->_getFormulairesMyOrganisations($this->WebcilUsers->mesOrganisations('list')),
            'modeles' => $this->_getSearchResults()
        ]);
    }

    /**
     * @access public
     *
     * @created  20/11/2020
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function entite()
    {
        $this->set('title', __d('modele', 'modele.titreMesModelesPourFormulaires'));

        $this->set([
            'modeles' => $this->_getSearchResults()
        ]);
    }

    /**
     * @access protected
     *
     * @created  20/11/2020
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    protected function _getFormulairesMyOrganisations($mesOrganisations)
    {
        $sqlALeDroitUtiliseOrganisation = $this->Formulaire->FormulaireOrganisation->sql([
            'alias' => 'formulaires_organisations',
            'fields' => [
                'formulaires_organisations.formulaire_id'
            ],
            'conditions' => [
                'formulaires_organisations.formulaire_id = Formulaire.id',
                'formulaires_organisations.organisation_id' => array_keys($mesOrganisations)
            ]
        ]);

        $sqlOrganisationAlreadyUsedFormulaire = $this->Fiche->sql([
            'alias' => 'fiches',
            'fields' => [
                'fiches.form_id'
            ],
            'conditions' => [
                'fiches.form_id = Formulaire.id',
                'fiches.organisation_id' => array_keys($mesOrganisations)
            ]
        ]);

        $sqlFormulaireLieOrganisation = $this->Formulaire->FormulaireModeleOrganisation->sql([
            'alias' => 'formulaires_modeles_organisations',
            'fields' => [
                'formulaires_modeles_organisations.formulaire_id'
            ],
            'conditions' => [
                'formulaires_modeles_organisations.formulaire_id = Formulaire.id',
                'formulaires_modeles_organisations.organisation_id' => array_keys($mesOrganisations)
            ]
        ]);

        $query = [
            'fields' => [
                'Formulaire.id',
                'Formulaire.libelle'
            ],
            'conditions' => [
                'OR' => [
                    // Le formulaire a le droit d'être utilisé dans mon organisation
                    "Formulaire.id IN ( {$sqlALeDroitUtiliseOrganisation} )",
                    // le formulaire a déjà été utilisé dans une fiche de mon organisation
                    "Formulaire.id IN ( {$sqlOrganisationAlreadyUsedFormulaire} )"
                ],
                // le formulaire n'est pas encore utilisé dans mon organisation
                "Formulaire.id NOT IN ( {$sqlFormulaireLieOrganisation} )"
            ],
            'order' => [
                'Formulaire.libelle ASC'
            ]
        ];

        return $this->Formulaire->find('list', $query);
    }

    /**
     * @access protected
     *
     * @created  20/11/2020
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    protected function _getSearchResults()
    {
        $query = [
            'contain' => [
                'Formulaire',
                'FormulaireModeleOrganisation' => [
                    'Organisation.id',
                    'Organisation.raisonsociale'
                ],
            ],
            'joins' => [
                $this->Modele->join('FormulaireModeleOrganisation'),
                $this->Modele->FormulaireModeleOrganisation->join('Organisation')
            ],
            'fields' => [
                'Modele.id',
                'Modele.name_modele',
                'Modele.fichier',
                'Formulaire.id',
                'Formulaire.libelle',
            ],
            'order' => [
                'Formulaire.libelle ASC',
            ],
            'group' => [
                'Modele.id',
                'Modele.name_modele',
                'Modele.fichier',
                'Formulaire.id',
                'Formulaire.libelle',
            ],
        ];

        if ($this->request->params['action'] === 'entite') {
            $query['conditions'] = [
                'organisation_id' => $this->Session->read('Organisation.id')
            ];
        }

        $this->paginate = $query;
        return $this->paginate($this->Modele);
    }

    /**
     * @return false|string|null
     *
     * @access public
     *
     * @created  23/11/2020
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function ajax_update_listing_organisation()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            if (empty($this->request->data['formulaire_id'])) {
                if (empty($this->request->data['modele_id'])) {
                    return null;
                } else {
                    $modele_id = $this->request->data['modele_id'];
                }

                $modele = $this->Modele->find('first', [
                    'fields' => [
                        'Modele.formulaire_id'
                    ],
                    'conditions' => [
                        'Modele.id' => $modele_id
                    ]
                ]);
                $formulaire_id = $modele['Modele']['formulaire_id'];
            } else {
                $formulaire_id = $this->request->data['formulaire_id'];
            }

            $sqlOrganisationIsAssociatedToFormulaire = $this->Formulaire->FormulaireOrganisation->sql([
                'alias' => 'formulaires_organisations',
                'fields' => [
                    'formulaires_organisations.organisation_id'
                ],
                'conditions' => [
                    'formulaires_organisations.organisation_id = Organisation.id',
                    'formulaires_organisations.formulaire_id' => $formulaire_id
                ]
            ]);

            $sqlOrganisationAlreadyUsedFormulaire = $this->Fiche->sql([
                'alias' => 'fiches',
                'fields' => [
                    'fiches.organisation_id'
                ],
                'conditions' => [
                    'fiches.organisation_id = Organisation.id',
                    'fiches.form_id' => $formulaire_id
                ]
            ]);

            $sqlOrganisationHasModelForFormulaire = $this->Formulaire->FormulaireModeleOrganisation->sql([
                'alias' => 'formulaires_modeles_organisations',
                'fields' => [
                    'formulaires_modeles_organisations.organisation_id'
                ],
                'conditions' => [
                    'formulaires_modeles_organisations.organisation_id = Organisation.id',
                    'formulaires_modeles_organisations.formulaire_id' => $formulaire_id
                ]
            ]);

            $query = [
                'fields' => [
                    'Organisation.id',
                    'Organisation.raisonsociale'
                ],
                'conditions' => [
                    'OR' => [
                        // Mon entité est associée au formulaire
                        "Organisation.id IN ( {$sqlOrganisationIsAssociatedToFormulaire} )",
                        [
                            // Mon entité n'est pas associée au formulaire
                            "Organisation.id NOT IN ( {$sqlOrganisationIsAssociatedToFormulaire} )",
                            // Mais le formulaire a déjà été utilisé dans mon entité
                            "Organisation.id IN ( {$sqlOrganisationAlreadyUsedFormulaire} )",
                        ]
                    ],
                    // Il n'existe pas encore de modèle pour ce formulaire dans mon entité
                    "Organisation.id NOT IN ( {$sqlOrganisationHasModelForFormulaire} )",
                    // 'est une organisation que je peux administrer
                    "Organisation.id" => array_keys($this->WebcilUsers->mesOrganisations('list'))
                ],
                'order' => [
                    'Organisation.raisonsociale'
                ]
            ];
            $organisations_id = $this->Formulaire->Organisation->find('list', $query);

            return json_encode($organisations_id);
        }

        return null;
    }

    public function dissocier($modele_id)
    {
        $this->Modele->FormulaireModeleOrganisation->begin();

        $success = false !== $this->Modele->FormulaireModeleOrganisation->deleteAll([
            'FormulaireModeleOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
            'FormulaireModeleOrganisation.modele_id' => $modele_id
        ]);

        if ($success == true) {
            $this->Modele->FormulaireModeleOrganisation->commit();
            $this->Session->setFlash(__d('modele', 'modele.flashsuccessDissocier'), 'flashsuccess');
        } else {
            $this->Modele->FormulaireModeleOrganisation->rollback();
            $this->Session->setFlash(__d('modele', 'modele.flasherrorErreurDissocier'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @access public
     *
     * @created 18/06/2015
     * @version V1.0.0
     *
     * @modified 20/11/2020
     * @version V2.1.0
     */
    public function add()
    {
        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $data = $this->request->data;

            $mesOrganisations_id = array_keys($this->WebcilUsers->mesOrganisations('list'));

            // On vérifie que l'utilisateur à bien les droits sur les organisations cible
            foreach ($data['Modele']['addOrganisation_id'] as $organisationCible) {
                if (in_array($organisationCible, $mesOrganisations_id) == false) {
                    throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
                }
            }

            $this->Modele->begin();

            $info = $this->Modele->saveFileModele(
                $data['Modele']['modele'],
                $data['Modele']['addFormulaire_id']
            );
            $success = $success && false !== $info['success'];

            if ($success == true) {
                $modele_id = $this->Modele->getLastInsertID();
                foreach ($data['Modele']['addOrganisation_id'] as $organisation_id) {
                    $this->Modele->FormulaireModeleOrganisation->create([
                        'FormulaireModeleOrganisation' => [
                            'formulaire_id' => $data['Modele']['addFormulaire_id'],
                            'modele_id' => $modele_id,
                            'organisation_id' => $organisation_id
                        ]
                    ]);

                    $success = $success && false !== $this->Modele->FormulaireModeleOrganisation->save(null, ['atomic' => false]);

                    if ($success == false) {
                        $info['message'] = __d('default', 'default.flasherrorEnregistrementErreur');
                        $info['statutMessage'] = 'flasherror';
                        break;
                    }
                }
            }

            if ($success == true) {
                $this->Modele->commit();
                $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleEnregistrer'), 'flashsuccess');
            } else {
                $this->Modele->rollback();
                $this->Session->setFlash($info['message'], $info['statutMessage']);
            }
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @param type $file
     * @return type
     * 
     * @access public
     * @created 18/06/2015
     * @version V1.0.0
     */
    public function download($file, $nameFile)
    {
        if (true !== $this->Droits->authorized([ListeDroit::GESTION_MODELE])) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->response->file(CHEMIN_MODELES . $file, [
            'download' => true,
            'name' => $nameFile
        ]);
        
        return $this->response;
    }

    /**
     * Permet de supprimer en bdd de le model associer par qu'ontre on ne 
     * supprime dans aucun cas le fichier enregistré
     *
     * @access public
     *
     * @param $modele_id
     *
     * @modified 23/11/2020
     * @modified 27/05/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function delete($modele_id)
    {
        $modeles = $this->Modele->find('all', [
            'conditions' => [
                'Modele.id' => $modele_id
            ]
        ]);

        if (!empty($modeles)) {
            $success = true;
            $this->Modele->begin();

            $success = $success && false !== $this->Modele->delete($modele_id);

            if ($success === true) {
                $this->Modele->commit();
                $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleSupprimer'), 'flashsuccess');
            } else {
                $this->Modele->rollback();
                $this->Session->setFlash(__d('modele', 'modele.flasherrorErreurSupprimerModele'), 'flasherror');
            }
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * 
     * @access public
     * @created 26/04/2016
     * @version V1.0.2
     */
    public function infoVariable($idFormulaire = null)
    {
        $this->set('title', __d('modele', 'modele.titreInfoVariableModele'));

        // L'entité utilise un modèle de présentation
        $useModelePresentation = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usemodelepresentation'
            ]
        ]);
        $usemodelepresentation = Hash::get($useModelePresentation, 'Organisation.usemodelepresentation');

        if ($usemodelepresentation === false) {
            // Information sur l'organisation
            $organisation = $this->Organisation->find('first', [
                'conditions' => [
                    'id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'raisonsociale',
                    'telephone',
                    'fax',
                    'adresse',
                    'email',
                    'sigle',
                    'siret',
                    'ape',
                    'dpo',
                    'numerodpo',
                    'emaildpo'
                ]
            ]);

            // Responsable de l'entité
            $responsableEntite = $this->Organisation->find('first', [
                'conditions' => [
                    'id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'civiliteresponsable',
                    'nomresponsable',
                    'prenomresponsable',
                    'emailresponsable',
                    'telephoneresponsable',
                    'fonctionresponsable',
                    'responsable_nom_complet',
                    'responsable_nom_complet_court'
                ]
            ]);

            // Information sur le DPO de l'organisation
            $userDPO = $this->User->find('first', [
                'conditions' => [
                    'id' => $organisation['Organisation']['dpo']
                ],
                'fields' => [
                    'civilite',
                    'nom',
                    'prenom',
                    'telephonefixe',
                    'telephoneportable',
                    'nom_complet',
                    'nom_complet_court'
                ]
            ]);

            if (isset($organisation['Organisation']['dpo']) === true) {
                unset($organisation['Organisation']['dpo']);
            }

            if (isset($organisation['Organisation']['numerodpo']) === true) {
                $userDPO['User'] += [
                    'numerodpo' => $organisation['Organisation']['numerodpo'],
                    'emaildpo' => $organisation['Organisation']['emaildpo']
                ];

                unset($organisation['Organisation']['numerodpo']);
                unset($organisation['Organisation']['emaildpo']);
            }

            $variablesOrganisation = [];
            foreach ($organisation['Organisation'] as $key => $valueOrganisation) {
                $variablesOrganisation['Entite'][$key] = [
                    'organisation_'.$key => $valueOrganisation
                ];
            }

            foreach ($responsableEntite['Organisation'] as $key => $valueResponsable) {
                $variablesOrganisation['Responsable'][$key] = [
                    'organisation_'.$key => $valueResponsable
                ];
            }

            foreach ($userDPO['User'] as $key => $valueUserDPO) {
                $variablesOrganisation['Dpo'][$key] = [
                    'dpo_'.$key => $valueUserDPO
                ];
            }
            $this->set(compact('variablesOrganisation'));
        }

        $valeurPropreTraitement = [
            'declarant' => [
                'declarantpersonnenom',
                'declarantservice',
                'declarantpersonneemail'
            ],
            'traitement' => [
                'outilnom',
                'finaliteprincipale',
                'transfert_hors_ue',
                'donnees_sensibles',
                'obligation_pia',
                'realisation_pia',
                'depot_pia',
                'coresponsable',
                'soustraitance',
                'numeroenregistrement',
                'norme',
                'normelibelle',
                'normedescription',
                'referentiel',
                'referentieldescription',
                'fichecreated',
                'fichemodified',
            ]
        ];

        $rt_externe = false;
        $viewAnnexe = false;
        $viewHistorique = false;
        if ($idFormulaire != null) {
            $viewAnnexe = true;
            $viewHistorique = true;

            //information sur les champs du formulaire
            $variables = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $idFormulaire,
                    'champ_coresponsable' => false,
                    'champ_soustraitant' => false,
                ],
                'fields' => [
                    'type',
                    'details'
                ]
            ]);
            $variables = Hash::extract($variables, '{n}.Champ');

            $this->_getCoresponsables($idFormulaire);
            $this->_getSoustraitances($idFormulaire);

            $optionsUseFormulaire = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $idFormulaire
                ],
                'fields' => [
                    'usesousfinalite',
                    'usebaselegale',
                    'usedecisionautomatisee',
                    'usetransferthorsue',
                    'usedonneessensible',
                    'usepia'
                ]
            ]);

            $formulaireRT = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $idFormulaire
                ],
                'fields' => [
                    'rt_externe'
                ]
            ]);
            $rt_externe = $formulaireRT['Formulaire']['rt_externe'];

            $prefixChampName = 'rt_organisation_';
            $rtVariables = [];
            if ($formulaireRT['Formulaire']['rt_externe'] === true) {
                $rtVariables = [
                    'rt_externe_raisonsociale',
                    'rt_externe_telephone',
                    'rt_externe_fax',
                    'rt_externe_adresse',
                    'rt_externe_email',
                    'rt_externe_siret',
                    'rt_externe_ape',
                    'rt_externe_civiliteresponsable',
                    'rt_externe_prenomresponsable',
                    'rt_externe_nomresponsable',
                    'rt_externe_fonctionresponsable',
                    'rt_externe_emailresponsable',
                    'rt_externe_telephoneresponsable',
                    'rt_externe_civility_dpo',
                    'rt_externe_prenom_dpo',
                    'rt_externe_nom_dpo',
                    'rt_externe_numerocnil_dpo',
                    'rt_externe_email_dpo',
                    'rt_externe_telephonefixe_dpo',
                    'rt_externe_telephoneportable_dpo',
                ];

                $prefixChampName = 'st_organisation_';
            }

            $rtOrstValues = $this->Organisation->find('first', [
                'joins' => [
                    $this->Organisation->join('Dpo', ['type' => 'INNER']),
                ],
                'conditions' => [
                    'Organisation.id' => $this->Session->read('Organisation.id'),
                    'Dpo.id = Organisation.dpo'
                ],
                'fields' => [
                    'Organisation.raisonsociale',
                    'Organisation.telephone',
                    'Organisation.fax',
                    'Organisation.adresse',
                    'Organisation.email',
                    'Organisation.sigle',
                    'Organisation.siret',
                    'Organisation.ape',
                    'Organisation.civiliteresponsable',
                    'Organisation.prenomresponsable',
                    'Organisation.nomresponsable',
                    'Organisation.emailresponsable',
                    'Organisation.telephoneresponsable',
                    'Organisation.fonctionresponsable',
                    'Organisation.numerodpo',
                    'Organisation.emaildpo',
                    $this->Organisation->Dpo->vfNomComplet(),
                    'Dpo.telephonefixe',
                    'Dpo.telephoneportable',
                ]
            ]);

            $arrayRtOrSt = [];
            foreach ($rtOrstValues['Organisation'] as $key => $value) {
                $arrayRtOrSt[] = $prefixChampName . $key;
            }

            foreach ($rtOrstValues['Dpo'] as $key => $value) {
                $arrayRtOrSt[] = $prefixChampName . $key . '_dpo';
            }

            $this->set(compact('variables', 'optionsUseFormulaire', 'rtVariables', 'arrayRtOrSt'));
        }

        $this->set(compact('valeurPropreTraitement', 'rt_externe', 'viewAnnexe', 'viewHistorique', 'usemodelepresentation'));
    }

    private function _getCoresponsables($formulaire_id)
    {
        $coresponsables =  array_filter(array_keys($this->Responsable->schema()), function($name) {
            return in_array($name, ['id', 'createdbyorganisation', 'created', 'modified']) === false && preg_match('/_id$/', $name) !== 1;
        });

        foreach ($coresponsables as $key => $coresponsable) {
            $translation = str_replace('_', '', ucwords($coresponsable, "_"));
            $coresponsables[__d('responsable_soustraitant', 'responsable_soustraitant.champ' . $translation)] = 'coresponsable_'.$coresponsable;
            unset($coresponsables[$key]);
        }

        $coresponsableFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => true,
                'champ_soustraitant' => false,
            ],
            'fields' => [
                'type',
                'details'
            ]
        ]);

        foreach ($coresponsableFields as $coresponsableField) {
            $details = json_decode($coresponsableField['Champ']['details'], true);
            $coresponsables[$details['label']] = $details['name'];
        }

        $this->set(compact('coresponsables'));
    }

    private function _getSoustraitances($formulaire_id)
    {
        $soustraitances =  array_filter(array_keys($this->Soustraitant->schema()), function($name) {
            return in_array($name, ['id', 'createdbyorganisation', 'created', 'modified']) === false && preg_match('/_id$/', $name) !== 1;
        });

        foreach ($soustraitances as $key => $soustraitance) {
            $translation = str_replace('_', '', ucwords($soustraitance, "_"));
            $soustraitances[__d('responsable_soustraitant', 'responsable_soustraitant.champ' . $translation)] = 'soustraitance_'.$soustraitance;
            unset($soustraitances[$key]);
        }

        $soustraitanceFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => false,
                'champ_soustraitant' => true,
            ],
            'fields' => [
                'type',
                'details'
            ]
        ]);

        foreach ($soustraitanceFields as $soustraitanceField) {
            $details = json_decode($soustraitanceField['Champ']['details'], true);
            $soustraitances[$details['label']] = $details['name'];
        }

        $this->set(compact('soustraitances'));
    }

}
