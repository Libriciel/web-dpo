<?php

/**
 * RegistresController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('EtatFiche', 'Model');
App::uses('ListeDroit', 'Model');

class RegistresController extends AppController {

    public $uses = [
        'EtatFiche',
        'Fiche',
        'Fichier',
        'Formulaire',
        'Modification',
        'Norme',
        'Organisation',
        'OrganisationUser',
        'TraitementRegistre',
        'Valeur',
        'Soustraitant',
        'Soustraitance',
        'Responsable',
        'Referentiel',
        'Coresponsable',
        'Typage'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);

        $this->Droits->assertNotSu();

        if ($action === 'add') {
            $this->Droits->assertDpo();
        } elseif ($action === 'edit') {
            $this->Droits->assertDpo();
        } elseif ($action === 'imprimer') {
            $this->Droits->assertAuthorized([ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE]);
        } elseif ($action === 'index') {
            $this->Droits->assertAuthorized([ListeDroit::CONSULTER_REGISTRE]);
        } elseif ($action === 'registre_activite') {
            $this->Droits->assertAuthorized([ListeDroit::CONSULTER_REGISTRE]);
        } elseif ($action === 'registre_soustraitance') {
            $this->Droits->assertAuthorized([ListeDroit::CONSULTER_REGISTRE]);
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     * @created 21/09/2015
     * @version V1.0.0
     */
    public function index() {
        $this->Session->write('nameController', "registres");
        $this->Session->write('nameView', "index");

        $this->set('title', __d('registre', 'registre.titreRegistre') . $this->Session->read('Organisation.raisonsociale'));

        $organisationActuelle = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'dpo'
            ]
        ]);
        $this->set(compact('organisationActuelle'));
        
        $condition = [
            'EtatFiche.etat_id' => [
                EtatFiche::VALIDER_DPO,
                EtatFiche::ARCHIVER,
                EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE
            ],
            'EtatFiche.actif' => true,
            'Fiche.organisation_id' => $this->Session->read('Organisation.id')
        ];

        $conditionValeur = [];
        $order = 'Fiche.created DESC';
        $limit = 20;
        $maxLimit = 100;
        
        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $named = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if(false === empty($named)) {
            $this->request->data = $named;
        }
        
        // Conditions venant implicitement de l'action sur les filtres
        if ($this->request->is('post') || false === empty($named)) {
            // Filtre sur l'utilisateur à l'origine du traitement
            if (!empty($this->request->data['Registre']['user'])) {
                $condition['Fiche.user_id'] = $this->request->data['Registre']['user'];
            }

            // Filtre sur le nom du traitement
            if (!empty($this->request->data['Registre']['outil'])) {
                $conditionValeur[] = [
                    'Valeur.champ_name' => 'outilnom',
                    'NOACCENTS_UPPER( Valeur.valeur ) LIKE' => '%'.str_replace(
                        '*',
                        '%',
                        trim(noaccents_upper($this->request->data['Registre']['outil']))
                    ).'%'
                ];
            }

            // Filtre sur le service à l'origine du traitement
            if (!empty($this->request->data['Registre']['service'])) {
                $condition['Fiche.service_id'] = $this->request->data['Registre']['service'];
//                $conditionValeur[] = [
//                    'Valeur.champ_name' => 'declarantservice',
//                    'Valeur.valeur' => $this->request->data['Registre']['service']
//                ];
            }
            
            // Filtre sur le nombre de traitement à l'affichage
            if (!empty($this->request->data['Registre']['nbAffichage'])) {
                $limit = $this->request->data['Registre']['nbAffichage'];
            } else {
                $limit = 20;
            }
            
            if ( $limit > $maxLimit ) {
                $maxLimit = $limit;
            }

            // Filtre sur l'ordre d'affichage
            if (!empty($this->request->data['Registre']['order'])) {
                if ($this->request->data['Registre']['order'] === 'Fiche.numero ASC') {
//                    $order = 'regexp_replace("Fiche"."numero", \'^[^0-9]+\', \'\')::INT ASC';
//                    $order = 'regexp_replace(numero, \'^.*\-\', \'\')::INT ASC';
                    $order = 'regexp_replace(numero, \'^.*[^0-9]([0-9]+)$\', \'\1\', \'g\')::INT ASC';
                }
                elseif ($this->request->data['Registre']['order'] === 'Fiche.numero DESC') {
//                    $order = 'regexp_replace("Fiche"."numero", \'^[^0-9]+\', \'\')::INT DESC';
//                    $order = 'regexp_replace(numero, \'^.*\-\', \'\')::INT DESC';
                    $order = 'regexp_replace(numero, \'^.*[^0-9]([0-9]+)$\', \'\1\', \'g\')::INT DESC';
                }
                else {
                    $order = $this->request->data['Registre']['order'];
                }
            } else {
                $order = 'Fiche.created DESC';
            }

            // Filtre sur une norme
            if (!empty($this->request->data['Registre']['norme'])) {
                $condition['Fiche.norme_id'] = $this->request->data['Registre']['norme'];
            }

            // Filtre sur un référentiel
            if (!empty($this->request->data['Registre']['referentiel'])) {
                $condition['Fiche.referentiel_id'] = $this->request->data['Registre']['referentiel'];
            }

            // Filtre sur les formulaires
            if (!empty($this->request->data['Registre']['formulaire'])) {
                $condition['Fiche.form_id'] = $this->request->data['Registre']['formulaire'];
            }

            // Filtre sur l'obligation d'un AIPD
            if (!empty($this->request->data['Registre']['obligation_pia'])) {
                $condition['Fiche.obligation_pia'] = $this->request->data['Registre']['obligation_pia'];
            }

            // Filtre sur la réalisation d'un AIPD
            if (!empty($this->request->data['Registre']['realisation_pia'])) {
                $condition['Fiche.realisation_pia'] = $this->request->data['Registre']['realisation_pia'];
            }

            // Filtre sur le dépot d'un AIPD
            if (!empty($this->request->data['Registre']['depot_pia'])) {
                $condition['Fiche.depot_pia'] = $this->request->data['Registre']['depot_pia'];
            }

            // Filtre sur le transfert hors ue
            if (!empty($this->request->data['Registre']['transfert_hors_ue'])) {
                $condition['Fiche.transfert_hors_ue'] = $this->request->data['Registre']['transfert_hors_ue'];
            }

            // Filtre sur les données sensibles
            if (!empty($this->request->data['Registre']['donnees_sensibles'])) {
                $condition['Fiche.donnees_sensibles'] = $this->request->data['Registre']['donnees_sensibles'];
            }

            // Filtre sur la soustraitance
            if (!empty($this->request->data['Registre']['soustraitance'])) {
                $condition['Fiche.soustraitance'] = $this->request->data['Registre']['soustraitance'];
            }

            // Filtre par sous-traitant
            if (!empty($this->request->data['Registre']['soustraitant'])) {
                $subQuery = [
                    'alias' => 'soustraitances',
                    'fields' => ['soustraitances.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'soustraitances.soustraitant_id' => $this->request->data['Registre']['soustraitant']
                    ]
                ];
                $sql = $this->Soustraitance->sql($subQuery);
                $condition[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur la co-responsabilité
            if (!empty($this->request->data['Registre']['coresponsable'])) {
                $condition['Fiche.coresponsable'] = $this->request->data['Registre']['coresponsable'];
            }

            // Filtre par responsable
            if (!empty($this->request->data['Registre']['responsable'])) {
                $subQuery = [
                    'alias' => 'coresponsables',
                    'fields' => ['coresponsables.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'coresponsables.responsable_id' => $this->request->data['Registre']['responsable']
                    ]
                ];
                $sql = $this->Coresponsable->sql($subQuery);
                $condition[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur la decision automatisee
            if (!empty($this->request->data['Registre']['decisionAutomatisee'])) {
                $subQuery = [
                    'alias' => 'valeurs',
                    'fields' => ['valeurs.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'valeurs.champ_name' => 'decisionAutomatisee',
                        'valeurs.valeur' => $this->request->data['Registre']['decisionAutomatisee']
                    ]
                ];
                $sql = $this->Valeur->sql($subQuery);
                $condition[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur le type d'annexe
            if (!empty($this->request->data['Registre']['type_annexe'])) {
                $subQuery = [
                    'alias' => 'fichiers',
                    'fields' => ['fichiers.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'fichiers.typage_id' => $this->request->data['Registre']['type_annexe']
                    ]
                ];
                $sql = $this->Fichier->sql($subQuery);
                $condition[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur le type de registre
            if (!empty($this->request->data['Registre']['typeRegistre']) && $this->request->data['Registre']['typeRegistre'] !== 'null') {
                $condition['Fiche.rt_externe'] = $this->request->data['Registre']['typeRegistre'];

                if ($this->request->data['Registre']['typeRegistre'] === 'true') {
                    $this->set('title', __d('registre', 'registre.titreRegistreSoustraitance') . $this->Session->read('Organisation.raisonsociale'));
                } else {
                    $this->set('title', __d('registre', 'registre.titreRegistreActivite') . $this->Session->read('Organisation.raisonsociale'));
                }
            }
        }
        
        if (false === empty($conditionValeur)) {
            $subQuery = [
                'alias' => 'valeurs',
                'fields' => ['valeurs.fiche_id'],
                'conditions' => $conditionValeur,
                'contain' => false
            ];

            $sql = words_replace($this->Fiche->Valeur->sql($subQuery), [
                'Valeur' => 'valeurs'
            ]);
            $condition[] = "Fiche.id IN ( {$sql} )";
        }

        $query = [
            'conditions' => $condition,
            'contain' => [
                'Fiche' => [
                    'id',
                    'created',
                    'modified',
                    'numero',
                    'coresponsable',
                    'soustraitance',
                    'obligation_pia',
                    'realisation_pia',
                    'depot_pia',
                    'rt_externe',
                    'transfert_hors_ue',
                    'donnees_sensibles',
                    'User' => [
                        'nom',
                        'prenom'
                    ],
                    'Valeur' => [
                        'fields' => [
                            'champ_name',
                            'valeur'
                        ],
                        'conditions' => [
                            'champ_name' => [
                                'outilnom',
                                'finaliteprincipale',
//                                'declarantservice'
                            ]
                        ]
                    ],
                    'Fichier' => [],
                    'Norme' => [
                        'fields' => [
                            'id',
                            'norme',
                            'numero',
                            'libelle',
                            'description',
                        ],
                    ],
                    'Referentiel' => [
                        'fields' => [
                            'id',
                            'name'
                        ],
                    ],
                    'Formulaire' => [
                        'fields' => [
                            'soustraitant'
                        ]
                    ],
                    'Service' => [
                        'fields' => [
                            'libelle'
                        ]
                    ]
                ]
            ],
            'order' => $order,
            'limit' => $limit,
            'maxLimit' => $maxLimit
        ];

        $this->paginate = $query;
        $fichesValid = $this->paginate('EtatFiche');

        foreach ($fichesValid as $idx => $fiche) {
            $list = Hash::combine($fiche, 'Fiche.Valeur.{n}.champ_name', 'Fiche.Valeur.{n}.valeur');
            $fichesValid[$idx]['Fiche']['Valeur'] = $list;
        }

        $enumsNormes = $this->Norme->enums();
        $this->set(compact('enumsNormes'));
        
        $query = [
            'fields' => ['id', 'norme', 'numero', 'libelle', 'description'],
            'order' => ['norme', 'numero']
        ];
        $normes = $this->Norme->find('all', $query);
        
        $options_normes = Hash::combine(
            $normes,
            '{n}.Norme.id',
            [
                '%s-%s : %s',
                '{n}.Norme.norme',
                '{n}.Norme.numero',
                '{n}.Norme.libelle'
            ]
        );
        $this->set(compact('options_normes'));

        $options_referentiels = $this->Referentiel->find('list', [
            'fields' => [
                'id',
                'name'
            ],
            'order' => [
                'name ASC'
            ]
        ]);
        $this->set(compact('options_referentiels'));
        
        foreach ($fichesValid as $key => $value) {
            if ($value['EtatFiche']['etat_id'] == EtatFiche::ARCHIVER) {
                $fichesValid[$key]['Readable'] = true;
            } else {
                $fichesValid[$key]['Readable'] = false;
            }
        }

        $this->set('fichesValid', $fichesValid);
        
        if (empty($this->request->data['Registre']['nbAffichage'])) {
            $this->request->data['Registre']['nbAffichage'] = 20;
        }

        $options = [
            'users' => $this->WebcilUsers->users('list', ['restrict' => true]),
            'services' => $this->WebcilUsers->services('list', [
                'restrict' => true,
                'fields' => [
                    'Service.id',
                    'Service.libelle'
                ]
            ]),
            'formulaires' => $this->Formulaire->find('list', [
                'Formulaire.id' => 'Formulaire.libelle',
                'contain' => [
                    'FormulaireOrganisation' => [
                        'Organisation' => [
                            'id',
                            'raisonsociale',
                            'order' => ['raisonsociale']
                        ]
                    ],
                ],
                'conditions' => [
                    $this->Formulaire->getFormulaireConditionOrganisation($this->Session->read('Organisation.id'))
                ],
            ]),
            'all_soustraitants' => $this->Soustraitant->find('list', [
                'restrict' => true,
                'fields' => [
                    'Soustraitant.id',
                    'Soustraitant.raisonsocialestructure',
                ]
            ]),
            'all_responsables' => $this->Responsable->find('list', [
                'restrict' => true,
                'fields' => [
                    'Responsable.id',
                    'Responsable.raisonsocialestructure',
                ]
            ]),
            'types_annexe' => $this->Typage->find('list', [
                'restrict' => true,
                'fields' => [
                    'Typage.id',
                    'Typage.libelle',
                ]
            ]),
        ];
        $this->set(compact('options'));
        
        $optionsAction = [];
        if ($this->Droits->authorized(ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE) == true) {
            $optionsAction[0] = __d('registre', 'registre.btnImprimerExtraitRegistrePDF');
        }
        
        $formulaires = [];
        if ($this->Droits->authorized($this->Droits->isDpo()) == true) {
            $optionsAction[2] = __d('registre', 'registre.btnGenererTraitementRegistrePDF');
            $optionsAction[3] = __d('registre', 'registre.btnExportCsv');
            $optionsAction[4] = __d('registre', 'registre.btnGenererExtraitTraitementsHtml');
        }
        $this->set(compact('optionsAction'));

        if ($this->Droits->authorized([ListeDroit::DUPLIQUER_TRAITEMENT]) === true) {
            $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
            $this->set(compact('mesOrganisations'));
        }
    }

    /**
     * Permet la modification d'un traitement inséré dans le registre
     *
     * @param type $idFiche
     *
     * @access public
     * @created 21/09/2015
     * @version V1.0.0
     */
    public function edit() {
        $this->Droits->assertRecordAuthorized('Fiche', $this->request->data['Registre']['idEditRegistre']);

        $success = true;
        $this->Modification->begin();

        $success = $success && $this->EtatFiche->updateAll([
            'actif' => false
                ], [
            'fiche_id' => $this->request->data['Registre']['idEditRegistre'],
            'etat_id' => [EtatFiche::VALIDER_DPO, EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE],
            'actif' => true
        ]) !== false;

        if ($success == true) {
            $this->EtatFiche->create([
                'EtatFiche' => [
                    'fiche_id' => $this->request->data['Registre']['idEditRegistre'],
                    'etat_id' => EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE,
                    'previous_user_id' => $this->Auth->user('id'),
                    'user_id' => $this->Auth->user('id')
                ]
            ]);

            $success = false !== $this->EtatFiche->save(null, ['atomic' => false]) && $success;

            if ($success == true) {
                $idEtatFiche = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'fiche_id' => $this->request->data['Registre']['idEditRegistre'],
                        'actif' => true
                    ]
                ]);

                $this->Modification->create([
                    'etat_fiches_id' => $idEtatFiche['EtatFiche']['id'],
                    'modif' => $this->request->data['Registre']['motif']
                ]);

                $success = false !== $this->Modification->save(null, ['atomic' => false]) && $success;
            }
        }

        if ($success == true) {
            $this->Modification->commit();

            $this->redirect([
                'controller' => 'fiches',
                'action' => 'edit',
                $this->request->data['Registre']['idEditRegistre']
            ]);
        } else {
            $this->Modification->rollback();
            $this->Session->setFlash(__d('fiche', 'flasherrorErreurContacterAdministrateur'), 'flasherror');

            $this->redirect([
                'controller' => 'registres',
                'action' => 'index',
                $this->request->data['Registre']['idEditRegistre']
            ]);
        }
    }

    /**
     * @access public
     * @created 21/09/2015
     * @version V1.0.0
     */
    public function add()
    {
        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->data;

            $this->Droits->assertRecordAuthorized('Fiche', $data['Registre']['idfiche']);

            $this->redirect([
                'controller' => 'etat_fiches',
                'action' => 'insertRegistre',
                $data['Registre']['idfiche'],
                $data['Registre']['numero'] ?: 'null'
            ]);
        }
    }

    /**
     * Permet d'imprimer tout les traitements selectionné et vérouillé au
     * registrer en mes mettent l'un a la suite
     *
     * @param array $tabId
     *
     * @access public
     * @created 13/05/2016
     * @version V1.0.0
     */
    public function imprimer($tabId)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty(json_decode($tabId))) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $tabId = json_decode($tabId);

        // On verifie si le dossier file existe. Si c'est pas le cas on le cree
        create_arborescence_files();

        $folder = TMP . "imprimerRegistre";
        $date = date('d-m-Y_H-i');

        // On verifie si le dossier existe. Si c'est pas le cas on le cree
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $files_concat = '';
        foreach ($tabId as $ficheID) {
            /**
             * On recupere en BDD le flux de donnee qui a ete enregistre
             * au verrouillage du traitement en fonction de ID
            */
            $pdf = $this->TraitementRegistre->find('first', [
                'conditions' => ['fiche_id' => $ficheID],
                'fields' => ['data']
            ]);

            /**
             * On cree un fichier .pdf avec le flux de donnee de la BDD
             * qu'on enregistre dans /var/www/webdpo/app/tmp/imprimerRegistre
             */
            $monPDF = fopen($folder . DS . $ficheID . '.pdf', 'a');
            fputs($monPDF, $pdf['TraitementRegistre']['data']);
            fclose($monPDF);

            // On concatene le chemin du fichier .pdf
            $files_concat .= $folder . DS . $ficheID . '.pdf ';
        }

        /**
         * On concatene tout les PDFs qu'on a cree et on enregistre
         * la concatenation dans /var/www/webdpo/app/files/registre
         */
        shell_exec('pdftk' . ' ' . $files_concat . 'cat output ' . CHEMIN_REGISTRE . 'Registre_' . $date . '.pdf');

        //On supprime de dossier imprimerRegistre dans /tmp
        shell_exec('rm -rf ' . realpath($folder));

        //On propose le telechargement
        $this->response->file(CHEMIN_REGISTRE . 'Registre_' . $date . '.pdf', array(
            'download' => true,
            'name' => 'Registre_' . $date . '.pdf'
        ));

        return $this->response;
    }

    public function registreActivite()
    {
        $this->redirect(['controller' => 'registres', 'action' => 'index', 'Registre.typeRegistre' => 'false']);
    }

    public function registreSoustraitance()
    {
        $this->redirect(['controller' => 'registres', 'action' => 'index', 'Registre.typeRegistre' => 'true']);
    }

}
