<?php

/**
 * FichesController : Controller des fiches
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');
App::uses('File', 'Utility');

/**
 * @property mixed ResponsableOrganisation
 */
class FichesController extends AppController
{
    public $helpers = [
        'Html',
        'Form',
        'Session'
    ];

    public $components = [
        'Jetons'
    ];

    public $uses = [
        'Champ',
        'Coresponsable',
        'Responsable',
        'ResponsableOrganisation',
        'EtatFiche',
        'Extrait',
        'ExtraitRegistre',
        'Fiche',
        'Fichier',
        'Formulaire',
        'FormulaireOrganisation',
        'FormulaireModeleOrganisation',
        'Historique',
        'Modele',
        'ModeleExtraitRegistre',
        'ModeleExtraitRegistreOrganisation',
        'ModelePresentation',
        'Norme',
        'Organisation',
        'Referentiel',
        'Service',
        'Soustraitant',
        'Soustraitance',
        'SoustraitantOrganisation',
        'Typage',
        'TypageOrganisation',
        'TraitementRegistre',
        'User',
        'Valeur',
        'WebdpoFiche',
        'WebdpoCoresponsable',
        'WebdpoSoustraitance'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        // Pour les autres que index et les download, aucune idée sur la façon de restreindre les droits
        //$anyone = ['index', 'delete_file', 'delete_recording_file', 'download', 'download_file_extrait', 'download_file_traitement'];

        if (in_array($action, ['add', 'archive']) === true) {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'delete') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'dupliquerTraitement') {
            $this->Droits->assertAuthorized([ListeDroit::DUPLIQUER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'edit') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'ajax_update_listing_responsable') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'ajax_update_listing_soustraitant') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'show') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'genereTraitementEnCours') {
            $this->Droits->assertAuthorized([ListeDroit::GENERER_TRAITEMENT_EN_COURS]);
            $this->Droits->assertNotSu();
        } elseif (in_array($action, [
                'genereTraitement',
                'genererDeclarations',
                'genereTraitementNonVerrouiller',
                'genereExtraitRegistre',
                'genereExtraitRegistreHtml'
            ]) === true) {
            $this->Droits->assertAuthorized([ListeDroit::TELECHARGER_TRAITEMENT_REGISTRE]);
            $this->Droits->assertNotSu();
        }
        /*elseif (in_array($action, $anyone) === false) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }*/
    }

    /**
     * La page d'accueil des fiches est celle du pannel général
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function index()
    {
        $this->redirect([
            'controller' => 'pannel',
            'action' => 'index'
        ]);
    }

    public function saveFileTmp($formulaire_id)
    {
        $this->autoRender = false;

        if (!empty($this->request->params['form']['fichiers']) && !empty($this->request->params['pass'])) {
            if (empty($this->Session->read('Auth.User.uuid'))) {
                $this->Session->write('Auth.User.uuid', CakeText::uuid());
            }

            // On verifie si le dossier file existe. Si c'est pas le cas on le cree
            create_arborescence_files($this->Session->read('Auth.User.id'));

            $allextentionFile = $this->Formulaire->find('first', [
               'conditions' => [
                   'id' => $formulaire_id
               ],
               'fields' => [
                   'useallextensionfiles'
               ]
            ]);

            if ($allextentionFile['Formulaire']['useallextensionfiles'] == true) {
                $accepted = Configure::read('allFileAnnexeAcceptedTypes');
            } else {
                $accepted = ['application/pdf', 'application/vnd.oasis.opendocument.text'];
            }

            $typages = $this->_typages();

            $json = [];
            foreach ($this->request->params['form']['fichiers']['tmp_name'] as $key => $tmpFile) {
                $dir = CHEMIN_PIECE_JOINT_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid');

                if (!is_dir($dir)) {
                    mkdir($dir);
                }

                $filename = $this->request->params['form']['fichiers']['name'][$key];
                $path = $dir . DS . $filename;

                $mime = mime_content_type($tmpFile);

                if (in_array($mime, $accepted) === true) {
                    move_uploaded_file($tmpFile, $path);

                    $allTmpFiles = $files = $this->Fichier->scan_dir($dir);
                    $last_key_file = key(array_slice( $allTmpFiles, -1, 1, true ));

                    $selectType = '';
                    if (!empty($typages)) {
                        $selectType = '<div class="form-group">'
                            . '<select name="data[Fichier_tmp][typage_tmp_'.$last_key_file.']" id="typage_tmp_'.$last_key_file.'" class="form-control typeAnnexeSelected">'
                            . '<option value="">Séléctionnez un type pour le fichier</option>';
                        foreach ($typages as $val => $typage) {
                            $selectType = $selectType . '<option value="' . $val . '">' . $typage . '</option>';
                        }
                        $selectType = $selectType . '</select></div>';
                    }

                    $json[] = [
                        'status' => 'success',
                        'filename' => $filename,
                        'path' => $path,
                        'optionType' => $selectType,
                    ];
                } else {
                    $json[] = [
                        'status' => 'error',
                    ];
                }
            }

            header('Content-type: "application/json"');
            echo json_encode($json);
        }
    }

    public function deleteFile()
    {
        $this->autoRender = false;

        $file = new File(CHEMIN_PIECE_JOINT_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid') . DS . $this->request->data('filename'));
        $file->delete();

    }

    /**
     * Suppression de l'enregistrement de la présente d'un fichier dans la table
     * "fichiers"
     *
     * @access public
     * @created 19/10/2018
     * @version V1.0.1²
     */
    public function deleteRecordingFile()
    {
        $this->autoRender = false;

        $success = true;
        $this->Fichier->begin();

        $success = $success && false !== $this->Fichier->delete($this->request->data('idFile'));

        if ($success == true) {
            $this->Fiche->commit();

            $this->_deleteFileDisk($this->request->data('urlFile'));
            $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementSupprimer'), 'flashsuccess');
        } else {
            $this->Fiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorPasDroitPage'), 'flasherror');
        }
    }

    /**
     * Supprimer physiquement le fichier sur le disque
     *
     * @param char $urlFile
     *
     * @access protected
     * @created 19/10/2018
     * @version V1.0.1
     */
    protected function _deleteFileDisk($urlFile)
    {
        $targetFileDelete = CHEMIN_PIECE_JOINT . $urlFile;

        if (file_exists($targetFileDelete) === true) {
            $mimeFile = mime_content_type($targetFileDelete);

            if ($mimeFile === 'application/pdf') {
                $filename = CHEMIN_PIECE_JOINT_CONVERSION . preg_replace('/\.pdf$/i', '.odt', $urlFile);

                if (file_exists($filename) === true) {
                    $filenameConversion = new File($filename);
                    $filenameConversion->delete();
                }
            }

            $file = new File($targetFileDelete);
            $file->delete();
        }
    }

    /**
     * Gère l'ajout d'un fiche
     *
     * @param int $formulaire_id : ID du formulaire utilisé
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @modified 23/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function add($formulaire_id)
    {
        if ($this->Droits->existDPO() === false) {
            throw new ForbiddenException("Aucun DPO n'a été défini pour votre entité. Veuillez contacter votre administrateur.");
        }

        if (true !== $this->Droits->authorized(ListeDroit::REDIGER_TRAITEMENT)) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->set('title', __d('fiche', 'fiche.titreCrationFiche'));

        return $this->_createFiche($formulaire_id, false);
    }

    /**
     * Gère l'initialisation d'une fiche
     *
     * @param int $formulaire_id : ID du formulaire utilisé
     *
     * @access public
     *
     * @created 23/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function initialisation($formulaire_id)
    {
        if (true !== $this->Droits->authorized(ListeDroit::INITIALISATION_TRAITEMENT)) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->set('title', __d('fiche', 'fiche.titreInitialisationFiche'));

        return $this->_createFiche($formulaire_id, true);
    }

    /**
     * Gère la création d'une fiche
     *
     * @param int $formulaire_id : ID du formulaire utilisé
     * @param bool $initialisationMode : Connaitre le mode de création de la fiche (rédaction = false, initialisation = true)
     *
     * @created 23/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _createFiche($formulaire_id, $initialisationMode)
    {
        if (!empty($this->Session->read('Auth.User.uuid'))) {
            $dir = CHEMIN_PIECE_JOINT_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid');
            $files = $this->Fichier->scan_dir($dir);

            $this->set('files', $files);
        }

//        $this->Droits->assertRecordAuthorized('Formulaire', $formulaire_id);

        if ($this->request->is('POST')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                if (!empty($this->Session->read('Auth.User.uuid'))) {
                    $this->Session->delete('Auth.User.uuid');
                }

                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->Fiche->begin();

            $data = $this->request->data;
            $data['Fiche']['user_id'] = $this->Auth->user('id');
            $data['Fiche']['form_id'] = $formulaire_id;
            $data['Fiche']['organisation_id'] = $this->Session->read('Organisation.id');

            $formulaireFields = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $formulaire_id
                ],
                'fields' => [
                    'type',
                    'details',
                    'champ_coresponsable',
                    'champ_soustraitant'
                ]
            ]);

            $formulaireOptions = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $formulaire_id
                ],
                'fields' => [
                    'usesousfinalite',
                    'usebaselegale',
                    'usedecisionautomatisee',
                    'usetransferthorsue',
                    'usedonneessensible',
                    'usepia'
                ]
            ]);

            if ($formulaireOptions['Formulaire']['usepia'] == true) {
                $data['Fiche']['obligation_pia'] = $this->deductValueFieldObligationPia($data);
            }

            $formulaireRT = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $formulaire_id
                ],
                'fields' => [
                    'rt_externe'
                ]
            ]);

            if ($formulaireRT['Formulaire']['rt_externe'] === true) {
                $data['Fiche']['rt_externe'] = true;
            } else {
                $data['Fiche']['rt_externe'] = 0;
            }

            $success = $this->WebdpoFiche->saveWithVirtualFields(
                $data,
                $formulaireFields,
                $formulaireOptions,
                false,
                true,
                $initialisationMode
            );

            if ($success == false &&
                empty($this->Fiche->validationErrors) &&
                $initialisationMode === false
            ) {
                $success = true;
            }

            if ($success == true) {
                $last = $this->Fiche->getLastInsertID();

                if ($initialisationMode == true) {
                    $commentaireHistorique = __d('historique', 'historique.initialisationTraitement');
                    $etat_id_traitement = EtatFiche::INITIALISATION_TRAITEMENT;
                } else {
                    $commentaireHistorique = __d('historique', 'historique.creationTraitement');
                    $etat_id_traitement = EtatFiche::ENCOURS_REDACTION;
                }

                $contentHistorique = $commentaireHistorique . ' ' . $this->Auth->user('nom_complet');
                $success = $success && false !== $this->_saveHistorique($contentHistorique, $last);

                if ($success == true) {
                    $success = $success && false !== $this->_saveEtatFiche(
                        $last,
                        $etat_id_traitement,
                        $this->Auth->user('id'),
                        $this->Auth->user('id')
                    );

                    if (!empty($this->Session->read('Auth.User.uuid'))) {
                        if ($success == true) {
                            $useAllExtensionFiles = $this->Formulaire->find('first', [
                                'conditions' => [
                                    'id' => $formulaire_id
                                ],
                                'fields' => [
                                    'useallextensionfiles'
                                ]
                            ]);

                            $success = $success && false !== $this->Fichier->transfereSave(
                                $last,
                                $useAllExtensionFiles['Formulaire']['useallextensionfiles'],
                                $this->Session->read('Auth.User.uuid'),
                                $this->Session->read('Auth.User.id'),
                                isset($data['Fichier_tmp']) ? $data['Fichier_tmp'] : []
                            );
                        }
                    }
                }
            }

            if ($initialisationMode === true) {
                if ($success == true &&
                    empty($this->Fiche->validationErrors) &&
                    empty($this->Fichier->validationErrors) &&
                    empty($this->WebdpoFiche->validationErrors) &&
                    empty($this->WebdpoCoresponsable->validationErrors) &&
                    empty($this->WebdpoSoustraitance->validationErrors)
                ) {
                    $this->Fiche->commit();
                    $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementEnregistrer'), 'flashsuccess');

                    $this->Session->delete('Auth.User.uuid');

                    $this->redirect($this->Referers->get());
                } else {
                    $this->Fiche->rollback();
                    $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
                }
            } else {
                if (empty($this->Fiche->validationErrors)) {
                    if ($success == true &&
                        empty($this->Fiche->validationErrors) &&
                        empty($this->Fichier->validationErrors) &&
                        empty($this->WebdpoFiche->validationErrors) &&
                        empty($this->WebdpoCoresponsable->validationErrors) &&
                        empty($this->WebdpoSoustraitance->validationErrors)
                    ) {
                        $this->Fiche->commit();
                        $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementEnregistrer'), 'flashsuccess');

                        $this->Session->delete('Auth.User.uuid');
                        $this->Session->delete('Fiche');

                        $this->redirect($this->Referers->get());
                    } else {
                        $this->Fiche->updateAll([
                            'valide' => false
                        ], [
                            'id' => $last
                        ]);

                        $this->Fiche->commit();

                        $this->Session->setFlash(__d('fiche', 'fiche.flashwarningTraitementEnregistrerIncomplete'), 'flashwarning');

                        $this->Session->delete('Fiche');
                        $session = [
                            'Fiche' => [
                                'id' => $last,
                                'error' => [
                                    'Fichier' => $this->Fiche->validationErrors,
                                    'WebdpoFiche' => $this->WebdpoFiche->validationErrors,
                                    'WebdpoCoresponsable' => $this->WebdpoCoresponsable->validationErrors,
                                    'WebdpoSoustraitance' => $this->WebdpoSoustraitance->validationErrors,
                                ],
                                'referers' => $this->Referers->get()
                            ]
                        ];
                        $this->Session->write($session);

                        $this->redirect([
                            'action' => 'edit',
                            $last
                        ]);
                    }
                } else {
                    $this->Fiche->rollback();
                    $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
                }
            }
        }

        $useValueDefault = false;
        if (empty($this->request->data)) {
            $useValueDefault = true;

            // User
            $userLog = $this->User->find('first', [
                'conditions' => [
                    'User.id' => $this->Session->read("Auth.User.id")
                ],
                'fields' => [
                    'id',
                    'nom_complet_court',
                    'email',
                    'telephonefixe',
                    'telephoneportable'
                ]
            ]);
            $this->request->data['WebdpoFiche']['declarantpersonnenom'] = $userLog['User']['nom_complet_court'];
            $this->request->data['WebdpoFiche']['declarantpersonneemail'] = $userLog['User']['email'];
            $this->request->data['WebdpoFiche']['declarantpersonnefix'] = $userLog['User']['telephonefixe'];
            $this->request->data['WebdpoFiche']['declarantpersonneportable'] = $userLog['User']['telephoneportable'];
            $this->request->data['Fiche']['user_id'] = $userLog['User']['id'];
        } else {
            if (empty($this->request->data['Fiche']['user_id'])) {
                $this->request->data['Fiche']['user_id'] = $this->Session->read('Auth.User.id');
            }
        }

        $findFormulaire = $this->Formulaire->find('count', [
            'conditions' => [
                'id' => $formulaire_id
            ]
        ]);
        if (empty($findFormulaire)) {
            throw new NotFoundException();
        }

        $formulaireOLD = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $formulaire_id
            ],
            'fields' => [
                'oldformulaire',
                'soustraitant'
            ]
        ]);

        $rt_externe = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $formulaire_id
            ],
            'fields' => [
                'rt_externe',
            ]
        ]);
        $this->set('rt_externe', $rt_externe['Formulaire']['rt_externe']);

        if ($rt_externe['Formulaire']['rt_externe'] === true) {
            $this->getOrganisationForSt();
        }

        $this->getFormulaireFields($formulaire_id, $useValueDefault);

        // Récupére en BDD les options des champs défini lors de la création du formulaire
        $this->getOptionsFields($formulaire_id);

        // Récupère en BDD les normes. Renvoie les normes et les descriptions
        $this->getNormes();

        $this->getReferentiel();

        // On récupère les types d'annexes
        $typages = $this->_typages();

        // On récupère les responsables
        $responsables = $this->_responsables();

        // On récupère les soustraitants
        $soustraitants = $this->_soustraitants();

        $servicesUser = $this->WebcilUsers->services('list', [
            'restrict' => true,
            'user' => true,
            'fields' => [
                'Service.id',
                'Service.libelle'
            ]
        ]);

        $usefieldsredacteur = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usefieldsredacteur'
            ]
        ]);
        $usefieldsredacteur = $usefieldsredacteur['Organisation']['usefieldsredacteur'];

        $this->updateBreadcrumbs($this->Referers->get());

        $this->set(compact(
            'formulaire_id',
            'formulaireOLD',
            'typages',
            'responsables',
            'soustraitants',
            'initialisationMode',
            'servicesUser',
            'usefieldsredacteur'
        ));

        $this->view = 'add';
    }

    /**
     * @return mixed
     */
    protected function _responsables()
    {
        $condition = [];
        $query = [
            'conditions' => $condition,
            'order' => [
                'Responsable.raisonsocialestructure ASC'
            ]
        ];

        $subQuery = [
            'alias' => 'responsables_organisations',
            'fields' => [
                'responsables_organisations.responsable_id'
            ],
            'conditions' => [
                'responsables_organisations.responsable_id = Responsable.id',
                'responsables_organisations.organisation_id' => $this->Session->read('Organisation.id')
            ]
        ];
        $sql = $this->ResponsableOrganisation->sql($subQuery);

        $query['conditions'][] = "Responsable.id IN ( {$sql} )";

        $responsables = $this->Responsable->find('all', $query);

        return $responsables;
    }

    protected function _coresponsables($fiche_id)
    {
        $coresponsables = null;

        if (empty($this->request->data['Coresponsable'])) {
            $coresponsables = $this->Coresponsable->find('all', [
                'conditions' => [
                    'fiche_id' => $fiche_id
                ]
            ]);

            $coresponsableSelected = Hash::extract($coresponsables, '{n}.Coresponsable.responsable_id');

            $this->request->data['Coresponsable']['coresponsables'] = $coresponsableSelected;
        }

        $this->set(compact('coresponsables'));
    }

    /**
     * @return array
     */
    protected function _soustraitants()
    {
        $soustraitants = [];
        $condition = [];
        $query = [
            'conditions' => $condition,
            'order' => [
                'Soustraitant.raisonsocialestructure ASC'
            ]
        ];

        $subQuery = [
            'alias' => 'soustraitants_organisations',
            'fields' => [
                'soustraitants_organisations.soustraitant_id'
            ],
            'conditions' => [
                'soustraitants_organisations.organisation_id' => $this->Session->read('Organisation.id')
            ]
        ];
        $sql = $this->SoustraitantOrganisation->sql($subQuery);
        $query['conditions'][] = "Soustraitant.id IN ( {$sql} )";

        $soustraitants = $this->Soustraitant->find('all', $query);

        return $soustraitants;
    }

    protected function _soustraitances($fiche_id)
    {
        $soustraitances = null;

        if (empty($this->request->data['Soustraitance'])) {
            $soustraitances = $this->Soustraitance->find('all', [
                'conditions' => [
                    'fiche_id' => $fiche_id
                ]
            ]);

            $soustraitanceSelected = Hash::extract($soustraitances, '{n}.Soustraitance.soustraitant_id');

            $this->request->data['Soustraitance']['soustraitances'] = $soustraitanceSelected;
        }

        $this->set(compact('soustraitances'));
    }

    /**
     * Gère la suppression de traitement
     *
     * @param int $id : id de la fiche
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function delete($id)
    {
        $this->Droits->assertRecordAuthorized('Fiche', $id);

        if ($this->Droits->isOwner($id) === false || $this->Droits->isDeletable($id) === false) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $takenJeton = $this->Jetons->takenJeton($id);
        if ($takenJeton['success'] === false) {
            $this->Session->setFlash($takenJeton['message'], 'flashwarning');
            return $this->redirect($this->Referers->get());
        }

        $success = true;
        $this->Fiche->begin();

        $etatFiche = $this->EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'actif' => true
            ],
            'fields' => [
                'etat_id'
            ]
        ]);

        if ($etatFiche['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
            $success = false;
        }

        if ($success == true) {
            $fichiers = $this->Fichier->find('all', [
                'conditions' => [
                    'fiche_id' => $id
                ],
                'fields' => [
                    'url'
                ]
            ]);

            $success = $success && false !== $this->Fiche->delete($id);
        }

        if ($success == true) {
            $this->Fiche->commit();

            if (!empty($fichiers)) {
                foreach ($fichiers as $fichier) {
                    $this->_deleteFileDisk($fichier['Fichier']['url']);
                }
            }

            $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementSupprimer'), 'flashsuccess');
        } else {
            $this->Fiche->rollback();
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorSupprimerTraitementImpossible'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Modification d'un traitement
     *
     * @param int $id : id de la fiche
     *
     * @access public
     *
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @modified 27/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function edit($id)
    {
        $takenJeton = $this->Jetons->takenJeton($id);
        if ($takenJeton['success'] === false) {
            $this->Session->setFlash($takenJeton['message'], 'flashwarning');
            return $this->redirect($this->Referers->get());
        }

        $this->Jetons->get($id);

        if (!$id) {
            $id = $this->request->data['Fiche']['id'];
        }

        $sessionFiche = $this->Session->read('Fiche');
        $referers = $this->Referers->get();
        if (!empty($sessionFiche)) {
            if ($sessionFiche['id'] === $id) {
                $this->Fichier->validationErrors = $sessionFiche['error']['Fichier'];
                $this->WebdpoFiche->validationErrors = $sessionFiche['error']['WebdpoFiche'];
                $this->WebdpoCoresponsable->validationErrors = $sessionFiche['error']['WebdpoCoresponsable'];
                $this->WebdpoSoustraitance->validationErrors = $sessionFiche['error']['WebdpoSoustraitance'];
                $referers = $sessionFiche['referers'];
            }
        }

        $this->updateBreadcrumbs($referers);

        $this->Droits->assertRecordAuthorized('Fiche', $id);

        if (!$this->Droits->isEditable($id)) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        if (!empty($this->Session->read('Auth.User.uuid'))) {
            $dir = CHEMIN_PIECE_JOINT_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid');
            $files = $this->Fichier->scan_dir($dir);

            $this->set(compact('files'));
        }

        if ($this->Droits->existDPO() === false) {
            throw new ForbiddenException("Aucun DPO n'a été défini pour votre entité. Veuillez contacter votre administrateur.");
        }

        if (!$id && !$this->request->data['Fiche']['id']) {
            $this->Session->setFlash(__d('default', 'default.flasherrorTraitementInexistant'), 'flasherror');
            $this->redirect($referers);
        }

        $title = __d('fiche', 'fiche.titreEditionFiche');
        $nameTraiment = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'champ_name' => 'outilnom'
            ]
        ]);
        if (!empty($nameTraiment)) {
            $title .= $nameTraiment['Valeur']['valeur'];
        }
        $this->set('title', $title);

        if ($this->request->is(['POST', 'PUT'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                if (!empty($this->Session->read('Auth.User.uuid'))) {
                    $this->Session->delete('Auth.User.uuid');
                }
                $this->Jetons->release($id);
                $this->redirect($referers);
            }

            $success = true;
            $this->Fiche->begin();

            $data = $this->request->data;

//            unset($data['Fiche']['service_id']);
            unset($data['Trash']);

            $data['Fiche']['id'] = $id;

            $fiche = $this->Fiche->find('first', [
                'conditions' => [
                    'id' => $id
                ],
                'fields' => [
                    'user_id',
                    'form_id'
                ]
            ]);
            $form_id = $fiche['Fiche']['form_id'];

            if ($fiche['Fiche']['user_id'] !== $this->Session->read('Auth.User.id')) {
                unset($data['Fiche']['partage']);
            }

            $formulaireFields = $this->Champ->find('all', [
                'conditions' => [
                    'formulaire_id' => $form_id
                ],
                'fields' => [
                    'type',
                    'details',
                    'champ_coresponsable',
                    'champ_soustraitant'
                ]
            ]);

            $formulaireOptions = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $form_id
                ],
                'fields' => [
                    'usesousfinalite',
                    'usebaselegale',
                    'usedecisionautomatisee',
                    'usetransferthorsue',
                    'usedonneessensible',
                    'usepia'
                ]
            ]);

            $formulaireOLD = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $form_id
                ],
                'fields' => [
                    'oldformulaire'
                ]
            ]);

            if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
                if ($formulaireOptions['Formulaire']['usepia'] == true) {
                    $data['Fiche']['obligation_pia'] = $this->deductValueFieldObligationPia($data);
                }
            }

            $formulaireRT = $this->Formulaire->find('first', [
                'conditions' => [
                    'id' => $form_id
                ],
                'fields' => [
                    'rt_externe'
                ]
            ]);

            if ($formulaireRT['Formulaire']['rt_externe'] === true) {
                $data['Fiche']['rt_externe'] = true;
            } else {
                $data['Fiche']['rt_externe'] = 0;
            }

            $etatFiche = $this->EtatFiche->find('first', [
                'conditions' => [
                    'fiche_id' => $id,
                    'actif' => true
                ],
                'fields' => [
                    'etat_id'
                ]
            ]);

            $initialisationMode = false;
            if ($etatFiche['EtatFiche']['etat_id'] === EtatFiche::INITIALISATION_TRAITEMENT) {
                $initialisationMode = true;
            }

            $initialisationRecu = false;
            if ($etatFiche['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
                $initialisationRecu = true;
            }

            $success = $this->WebdpoFiche->saveWithVirtualFields(
                $data,
                $formulaireFields,
                $formulaireOptions,
                $formulaireOLD['Formulaire']['oldformulaire'],
                false,
                $initialisationMode,
                $initialisationRecu
            );

            if ($success == false &&
                empty($this->Fiche->validationErrors) &&
                $initialisationMode === false
            ) {
                $success = true;
            }

            if ($success == true) {
                $etatTraitement = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'fiche_id' => $id,
                        'actif' => true
                    ],
                    'fields' => [
                        'etat_id'
                    ]
                ]);

                if ($etatTraitement['EtatFiche']['etat_id'] === EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE) {
                    $texteHistorique = __d('historique', 'historique.modifierTraitementRegistre');
                } elseif ($etatTraitement['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
                    $texteHistorique = __d('historique', 'historique.redactionTraitementInitialiser');
                }else {
                    $texteHistorique = __d('historique', 'historique.modifierTraitement');
                }

                $contentHistorique = $this->Auth->user('nom_complet') . ' ' . $texteHistorique;
                $success = $success && false !== $this->_saveHistorique($contentHistorique, $id);

                if (isset($this->request->data['delfiles']) && !empty($this->request->data['delfiles'])) {
                    debug("lalal");die;
                    foreach (array_unique($this->request->data['delfiles']) as $val) {
                        $success = $success && $this->Fichier->deleteFichier($val);
                    }
                }

                if (!empty($this->Session->read('Auth.User.uuid'))) {
                    if ($success == true) {
                        $useAllExtensionFiles = $this->Formulaire->find('first', [
                            'conditions' => [
                                'id' => $form_id
                            ],
                            'fields' => [
                                'useallextensionfiles'
                            ]
                        ]);

                        $success = $success && false !== $this->Fichier->transfereSave(
                            $id,
                            $useAllExtensionFiles['Formulaire']['useallextensionfiles'],
                            $this->Session->read('Auth.User.uuid'),
                            $this->Session->read('Auth.User.id'),
                            isset($data['Fichier_tmp']) ? $data['Fichier_tmp'] : []
                        );
                    }
                }

                $filesBDD = $this->Fichier->find('all', [
                    'conditions' => [
                        'fiche_id' => $id
                    ]
                ]);

                if (!empty($filesBDD)) {
                    foreach ($filesBDD as $file) {
                        if (isset($data['Fichier']['typage_'.$file['Fichier']['id']]) && $success === true) {
                            if ($file['Fichier']['typage_id'] !== $data['Fichier']['typage_'.$file['Fichier']['id']]) {
                                $file['Fichier']['typage_id'] = $data['Fichier']['typage_'.$file['Fichier']['id']];
                                $success = $success && $this->Fichier->save($file, ['atomic' => false]);
                            }
                        }
                    }
                }

                if ($success == true && $etatFiche['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
                    $success = $success && $this->EtatFiche->updateAll([
                        'actif' => false
                    ], [
                        'fiche_id' => $id
                    ]) !== false;

                    if ($success == true) {
                        $success = $success && false !== $this->_saveEtatFiche(
                            $id,
                            EtatFiche::TRAITEMENT_INITIALISE_REDIGER,
                            $this->Auth->user('id'),
                            $this->Auth->user('id')
                        );
                    }
                }
            }

            if ($initialisationMode === true) {
                if ($success == true &&
                    empty($this->Fiche->validationErrors) &&
                    empty($this->Fichier->validationErrors) &&
                    empty($this->WebdpoFiche->validationErrors) &&
                    empty($this->WebdpoCoresponsable->validationErrors) &&
                    empty($this->WebdpoSoustraitance->validationErrors)
                ) {
                    $this->Fiche->commit();
                    $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementModifier'), 'flashsuccess');

                    $this->Jetons->release($id);

                    $this->Session->delete('Auth.User.uuid');

                    $this->redirect($this->Referers->get());
                } else {
                    $this->Fiche->rollback();
                    $this->Session->setFlash(__d('fiche', 'fiche.flasherrorTraitementModifier'), 'flasherror');
                }
            } else {
                if (empty($this->Fiche->validationErrors)) {
                    if ($success == true &&
                        empty($this->Fiche->validationErrors) &&
                        empty($this->Fichier->validationErrors) &&
                        empty($this->WebdpoFiche->validationErrors) &&
                        empty($this->WebdpoCoresponsable->validationErrors) &&
                        empty($this->WebdpoSoustraitance->validationErrors)
                    ) {
                        $this->Fiche->updateAll([
                            'valide' => true
                        ], [
                            'id' => $id
                        ]);

                        $this->Fiche->commit();
                        $this->Session->setFlash(__d('fiche', 'fiche.flashsuccessTraitementModifier'), 'flashsuccess');

                        $this->Jetons->release($id);

                        $this->Session->delete('Auth.User.uuid');
                        $this->Session->delete('Fiche');

                        return $this->redirect($referers);
                    } else {
                        $this->Fiche->updateAll([
                            'valide' => false
                        ], [
                            'id' => $id
                        ]);

                        $this->Fiche->commit();
                        $this->Session->setFlash(__d('fiche', 'fiche.flashwarningTraitementEnregistrerIncomplete'), 'flashwarning');
                    }
                } else {
                    $this->Fiche->rollback();
                    $this->Session->setFlash(__d('default', 'default.flasherrorTraitementModifier'), 'flasherror');
                }
            }
        }

        $fiche = $this->Fiche->find('first', [
            'conditions' => [
                'id' => $id
            ],
            'fields' => [
                'user_id',
                'form_id',
                'norme_id',
                'referentiel_id',
                'coresponsable',
                'soustraitance',
                'obligation_pia',
                'realisation_pia',
                'depot_pia',
                'rt_externe',
                'transfert_hors_ue',
                'donnees_sensibles',
                'service_id',
                'partage',
            ]
        ]);

        // On récupère en BDD les champs tous les champs du formulaire
        $this->getFormulaireFields($fiche['Fiche']['form_id'], false);

        // Récupère en BDD les normes. Renvoie les normes et les descriptions
        $this->getNormes($fiche['Fiche']['norme_id']);

        // Récupère en BDD le référentiel.
        $this->getReferentiel($fiche['Fiche']['referentiel_id']);

        if (empty($this->request->data)) {
            $this->request->data['Fiche']['user_id'] = $fiche['Fiche']['user_id'];
            $this->request->data['Fiche']['partage'] = $fiche['Fiche']['partage'];

            $this->request->data['Fiche']['norme_id'] = $fiche['Fiche']['norme_id'];
            $this->request->data['Fiche']['referentiel_id'] = $fiche['Fiche']['referentiel_id'];
            $this->request->data['Fiche']['coresponsable'] = $fiche['Fiche']['coresponsable'];
            $this->request->data['Fiche']['soustraitance'] = $fiche['Fiche']['soustraitance'];

            $this->request->data['Fiche']['transfert_hors_ue'] = $fiche['Fiche']['transfert_hors_ue'];
            $this->request->data['Fiche']['donnees_sensibles'] = $fiche['Fiche']['donnees_sensibles'];

            $this->request->data['Fiche']['obligation_pia'] = $fiche['Fiche']['obligation_pia'];
            $this->request->data['Fiche']['realisation_pia'] = $fiche['Fiche']['realisation_pia'];
            $this->request->data['Fiche']['depot_pia'] = $fiche['Fiche']['depot_pia'];

            // Récupère les valeurs des champs du traitement
            $this->getValueFields($id, $fiche['Fiche']['rt_externe']);
        } else {
            if (empty($this->request->data['Fiche']['user_id'])) {
                $this->request->data['Fiche']['user_id'] = $fiche['Fiche']['user_id'];
            }
        }

        $this->getFilesSave($id);

        $formulaireOLD = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $fiche['Fiche']['form_id']
            ],
            'fields' => [
                'oldformulaire',
                'soustraitant'
            ]
        ]);

        $this->set('rt_externe', $fiche['Fiche']['rt_externe']);

        $etatTraitement = $this->EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'actif' => true
            ],
            'fields' => [
                'etat_id'
            ]
        ]);

        $showRegistre = false;
        if (in_array($etatTraitement['EtatFiche']['etat_id'], EtatFiche::LISTE_ETAT_TRAITEMENT_REGISTRE)) {
            $showRegistre = true;
        }

        if ($showRegistre === false && $fiche['Fiche']['rt_externe'] === true) {
            $this->getOrganisationForSt();
        }

        // On récupère les responsables associés à l'entité
        $responsables = $this->_responsables();
        $this->set(compact('responsables'));

        // On récupère les coresponsables associés au traitement
        $this->_coresponsables($id);

        // On récupère les soustraitant associés à l'entité
        $soustraitants = $this->_soustraitants();
        $this->set(compact('soustraitants'));

        // On récupère la soustraitance associés au traitement
        $this->_soustraitances($id);

        // Récupére en BDD les options des champs défini lors de la création du formulaire
        $this->getOptionsFields($fiche['Fiche']['form_id']);

        $typages = $this->_typages();

        $initialisationMode = false;
        if ($etatTraitement['EtatFiche']['etat_id'] === EtatFiche::INITIALISATION_TRAITEMENT) {
            $initialisationMode = true;
        }

        $initialisationRecu = false;
        if ($etatTraitement['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
            $initialisationRecu = true;
        }

        $servicesUser = $this->WebcilUsers->services('list', [
            'restrict' => true,
            'user' => true,
            'fields' => [
                'Service.id',
                'Service.libelle'
            ]
        ]);

        if ($fiche['Fiche']['user_id'] === $this->Auth->user('id')) {
            if (!empty($servicesUser) && !empty($fiche['Fiche']['service_id'])) {
                if (array_key_exists($fiche['Fiche']['service_id'], $servicesUser)) {
                    $this->set(compact('servicesUser'));
                    $this->request->data['Fiche']['service_id'] = $fiche['Fiche']['service_id'];
                } else {
                    $serviceFiche = $this->Service->find('first', [
                        'conditions' => [
                            'id' => $fiche['Fiche']['service_id']
                        ],
                        'fields' => [
                            'libelle'
                        ]
                    ]);

                    $this->request->data['Trash']['service_libelle'] = $serviceFiche['Service']['libelle'];
                }
            } else {
                $this->set(compact('servicesUser'));
                $this->request->data['Fiche']['service_id'] = null;
            }
        } else {
            if (!empty($fiche['Fiche']['service_id'])) {
                $serviceFiche = $this->Service->find('first', [
                    'conditions' => [
                        'id' => $fiche['Fiche']['service_id']
                    ],
                    'fields' => [
                        'libelle'
                    ]
                ]);

                $serviceLibelle = $serviceFiche['Service']['libelle'];
            } else {
                $serviceLibelle = 'Aucun service';
            }

            $this->request->data['Trash']['service_libelle'] = $serviceLibelle;
        }

        $showPartage = false;
        if ($fiche['Fiche']['user_id'] === $this->Auth->user('id') &&
            in_array($etatTraitement['EtatFiche']['etat_id'], [
                EtatFiche::ENCOURS_REDACTION,
                EtatFiche::REPONSE_AVIS,
                EtatFiche::REPLACER_REDACTION,
                EtatFiche::TRAITEMENT_INITIALISE_RECU,
                EtatFiche::TRAITEMENT_INITIALISE_REDIGER
            ]
        )) {
            $showPartage = true;
        }

        $usefieldsredacteur = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usefieldsredacteur'
            ]
        ]);
        $usefieldsredacteur = $usefieldsredacteur['Organisation']['usefieldsredacteur'];

        $this->set(compact(
            'formulaireOLD',
            'typages',
            'initialisationMode',
            'initialisationRecu',
            'showPartage',
            'usefieldsredacteur'
        ));
        $this->set('formulaire_id', $fiche['Fiche']['form_id']);

        $this->view = 'add';
    }

    /**
     * Gère l'affichage d'une traitement
     *
     * @param int $id
     *
     * @access public
     *
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @edit 26/02/2019
     * @version v1.0.2
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @edit 26/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function show($id)
    {
        $this->Droits->assertRecordAuthorized('Fiche', $id);

        $this->requestAction([
            'controller' => 'pannel',
            'action' => 'supprimerLaNotif',
            $id
        ]);

        $nameTraitement = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'champ_name' => 'outilnom'
            ],
            'fields' => [
                'valeur'
            ]
        ]);
        $this->set('title', __d('fiche', 'fiche.titreApercuFiche') . Hash::get($nameTraitement, 'Valeur.valeur'));

        $fiche = $this->Fiche->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);

        if (isset($fiche['Fiche']['referentiel_id'])) {
            $referentielTraitement = $this->Referentiel->find('first', [
                'conditions' => [
                    'id' => $fiche['Fiche']['referentiel_id']
                ],
                'fields' => [
                    'id',
                    'name',
                    'name_fichier',
                    'fichier'
                ]
            ]);
            $this->set(compact('referentielTraitement'));
        }

        if (isset($fiche['Fiche']['norme_id'])) {
            $normeTraitement = $this->Norme->find('first', [
                'conditions' => [
                    'id' => $fiche['Fiche']['norme_id']
                ]
            ]);
            $this->set(compact('normeTraitement'));
        }

        // Récupère les valeurs des champs du traitement
        $this->getValueFields($id, $fiche['Fiche']['rt_externe']);

        $this->getFormulaireFields($fiche['Fiche']['form_id']);

        // Récupère en BDD les normes. Renvoie les normes et les descriptions
        $this->getNormes();

        // On récupère les fichiers en annexe
        $this->getFilesSave($id);
        $typages = $this->_typages();

        $formulaireOLD = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $fiche['Fiche']['form_id']
            ],
            'fields' => [
                'oldformulaire'
            ]
        ]);

        $this->set('rt_externe', $fiche['Fiche']['rt_externe']);

        // Récupére en BDD les options des champs défini lors de la création du formulaire
        $this->getOptionsFields($fiche['Fiche']['form_id']);

        $etatTraitement = $this->EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'actif' => true
            ],
            'fields' => [
                'etat_id'
            ]
        ]);

        $showRegistre = false;
        if (in_array($etatTraitement['EtatFiche']['etat_id'], EtatFiche::LISTE_ETAT_TRAITEMENT_REGISTRE)) {
            $showRegistre = true;
        }

        if ($showRegistre === false && $fiche['Fiche']['rt_externe'] === true) {
            $this->getOrganisationForSt();
        }

        $this->request->data['Fiche']['user_id'] = $fiche['Fiche']['user_id'];
        $this->request->data['Fiche']['transfert_hors_ue'] = $fiche['Fiche']['transfert_hors_ue'];
        $this->request->data['Fiche']['donnees_sensibles'] = $fiche['Fiche']['donnees_sensibles'];
        $this->request->data['Fiche']['obligation_pia'] = $fiche['Fiche']['obligation_pia'];
        $this->request->data['Fiche']['realisation_pia'] = $fiche['Fiche']['realisation_pia'];
        $this->request->data['Fiche']['depot_pia'] = $fiche['Fiche']['depot_pia'];
        $this->request->data['Fiche']['coresponsable'] = $fiche['Fiche']['coresponsable'];
        $this->request->data['Fiche']['soustraitance'] = $fiche['Fiche']['soustraitance'];

        // On récupère les responsable
        $responsables = $this->_responsables();
        $this->set(compact('responsables'));

        // On récupère les soustraitant
        $soustraitants = $this->_soustraitants();
        $this->set(compact('soustraitants'));

        // On récupère les coresponsables associés au traitement
        $this->_coresponsables($id);

        // On récupère la soustraitance associés au traitement
        $this->_soustraitances($id);

        if (!empty($fiche['Fiche']['service_id'])) {
            $serviceFiche = $this->Service->find('first', [
                'conditions' => [
                    'id' => $fiche['Fiche']['service_id']
                ],
                'fields' => [
                    'libelle'
                ]
            ]);

            $this->request->data['Trash']['service_libelle'] = $serviceFiche['Service']['libelle'];
        } else {
            $this->request->data['Trash']['service_libelle'] = 'Aucun service';
        }

        $usefieldsredacteur = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'usefieldsredacteur'
            ]
        ]);
        $usefieldsredacteur = $usefieldsredacteur['Organisation']['usefieldsredacteur'];

        $this->updateBreadcrumbs($this->Referers->get());

        $this->set('formulaire_id', $fiche['Fiche']['form_id']); //@TODO
        $this->set(compact('showRegistre', 'formulaireOLD', 'typages', 'usefieldsredacteur'));
    }

    /**
     * Gère le téléchargement des pieces jointes d'une fiche
     *
     * @param int|null $url
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function download($url = null, $nomFile = 'file.odt')
    {
        $this->response->file(CHEMIN_PIECE_JOINT . $url, [
            'download' => true,
            'name' => $nomFile
        ]);

        return $this->response;
    }

    /**
     * Téléchargement du traitement verrouiller
     *
     * @param int $fiche_id
     * @param type $numeroRegistre
     *
     * @access public
     * @created 04/01/2016
     * @version V1.0.0
     *
     * @modified 08/01/2020
     * @version V2.0.0
     * @deprecated
     */
    public function downloadFileTraitement($fiche_id)
    {
        $this->Droits->assertRecordAuthorized('Fiche', $fiche_id);

        $fiche = $this->Fiche->find('first', [
            'conditions' => ['id' => $fiche_id]
        ]);

        $nameTraiment = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'champ_name' => 'outilnom'
            ]
        ]);

        $pdf = $this->TraitementRegistre->find('first', [
            'conditions' => ['fiche_id' => $fiche_id],
            'fields' => ['data']
        ]);

        if (empty($pdf)) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorErreurPDF'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        header("content-type: application/pdf");
        header('Content-Disposition: attachment; filename="Traitement_' . $nameTraiment['Valeur']['valeur'] . '_' . $fiche['Fiche']['numero'] . '.pdf"');

        echo($pdf['TraitementRegistre']['data']);
    }

    /**
     * Téléchargement de l'extrait de registre verrouiller
     *
     * Si aucun fiche_id n'est passé en paramètre, un message d'erreur est
     * stocké en session et on est redirigé vers l'écran de visualisation
     * des registres.
     *
     * @param type $fiche_id
     *
     * @access public
     * @created 09/01/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function downloadFileExtrait($fiche_id)
    {
        // On vérifie que $fiche_id n'est pas vide
        if (empty($fiche_id)) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $this->Droits->assertRecordAuthorized('Fiche', $fiche_id);

        $fiche = $this->Fiche->find('first', [
            'conditions' => ['id' => $fiche_id]
        ]);

        $nameTraiment = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'champ_name' => 'outilnom'
            ]
        ]);

        $pdf = $this->ExtraitRegistre->find('first', [
            'conditions' => ['fiche_id' => $fiche_id],
            'fields' => ['data']
        ]);

        if (empty($pdf)) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorErreurPDF'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        header("content-type: application/pdf");
        header('Content-Disposition: attachment; filename="Extrait_' . $nameTraiment['Valeur']['valeur'] . '_' . $fiche['Fiche']['numero'] . '.pdf"');
        echo($pdf['ExtraitRegistre']['data']);
    }

    /**
     * Retourne le PDF du traitement dont l'id est passé en paramètres.
     *
     * Si aucun id n'est passé en paramètre, un message d'erreur est stocké en
     * session et on est redirigé vers l'écran de visualisation des registres.
     *
     * On supprime également la notification concernant le traitement.
     *
     * @param int $fiche_id
     * @param bool $historique
     * @param string $outputFormat
     * @return mixed
     *
     * @access protected
     *
     * @created 07/04/2017
     * @version V1.0.0
     * @author Christian BUFFIN <christian.buffin@libriciel.coop>
     *
     * @created 18/11/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _genereTraitement(
        $fiche_id,
        $historique = true,
        $outputFormat = 'pdf',
        $pathModele = null,
        $fileModele = null,
        $sendInfoEntiteToModeles = false)
    {
        $fiche = $this->Fiche->find('first', [
            'conditions' => [
                'id' => $fiche_id
            ]
        ]);

        if ($historique === true) {
            $annexefiles = $this->Fichier->find('all', [
                'conditions' => [
                    'fiche_id' => $fiche['Fiche']['id']
                ],
                'fields' => [
                    'url'
                ]
            ]);

            foreach ($annexefiles as $annexefile) {
                if (mime_content_type(CHEMIN_PIECE_JOINT . $annexefile['Fichier']['url']) === 'application/pdf') {
                    if (file_exists(CHEMIN_PIECE_JOINT_CONVERSION . preg_replace('/\.pdf$/i', '.odt', $annexefile['Fichier']['url'])) === false) {
                        $this->Session->setFlash(__d('fiche', 'fiche.flashwarningAnnexeNonConvertie'), 'flashwarning');
                        $this->redirect($this->Referers->get());
                    }
                }
            }
        }

        // On récupére le modèle de formulaire pour l'entité
        $query = [
            'fields' => [
                'Modele.id',
                'Modele.name_modele',
                'Modele.fichier',
            ],
            'joins' => [
                $this->Modele->join('FormulaireModeleOrganisation'),
            ],
            'conditions' => [
                'FormulaireModeleOrganisation.formulaire_id' => $fiche['Fiche']['form_id'],
                'FormulaireModeleOrganisation.organisation_id' => $this->Session->read('Organisation.id')
            ],
        ];
        $modeleFormulaire = $this->Modele->find('first', $query);

        // On vérifie que l'organisation à bien un modèle
        if (empty($modeleFormulaire)) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorRecuperationModele'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        // On vérifie que ce modèle est présent sur le disque
        $pathModeleFormulaire = CHEMIN_MODELES;
        $fileModeleFormulaire = $modeleFormulaire['Modele']['fichier'];
        if (file_exists($pathModeleFormulaire . $fileModeleFormulaire) == false) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorRecuperationModele'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        $pdf = $this->Fiche->preparationGeneration(
            $fiche_id,
            $fileModeleFormulaire,
            $pathModeleFormulaire,
            $this->Session->read('Organisation.id'),
            $historique,
            $outputFormat,
            $sendInfoEntiteToModeles
        );

        $this->requestAction(array(
            'controller' => 'pannel',
            'action' => 'supprimerLaNotif',
            (int) $fiche_id
        ));

        return $pdf;
    }

    /**
     * Rajout du modele de présentation à la génération documentaire (extrait de registre ou génération du registre)
     *
     * @param array $urlTraitementODT
     * @return mixed
     */
    protected function _genereRegistre($urlTraitementODT, $extraitRegistre = false)
    {
        $organisation_id = $this->Session->read('Organisation.id');

        // On récupére le modèle de présentation
        $modelePresentation = ClassRegistry::init('ModelePresentation')->find('first', [
            'conditions' => [
                'organisations_id' => $organisation_id
            ]
        ]);

        // On vérifie que les infos du modèle existe bien
        if (!empty($modelePresentation) &&
            file_exists(CHEMIN_MODELES_PRESENTATIONS.$modelePresentation['ModelePresentation']['fichier'])
        ) {
            $pdf = $this->Fiche->preparationGenerationRegistre(
                $urlTraitementODT,
                $modelePresentation['ModelePresentation']['fichier'],
                CHEMIN_MODELES_PRESENTATIONS,
                $organisation_id,
                $extraitRegistre
            );

            return $pdf;
        } else {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorRecuperationModelePresentation'), 'flasherror');
            $this->redirect($this->Referers->get());
        }
    }

    /**
     * Genere le traitement de registre
     *
     * @param int $fiche_id
     * @return data
     *
     * @access public
     *
     * @created 09/01/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @modified 07/12/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function genereTraitement($fiche_id)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty($fiche_id)) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $useModelPresentation = $this->useModelePresentation($this->Session->read('Organisation.id'));

        if ($useModelPresentation === true) {
            $sendInfoEntiteToModeles = false;
        } else {
            $sendInfoEntiteToModeles = true;
        }

        $pdf = $this->_genereTraitement(
            $fiche_id,
            true,
            'pdf',
            null,
            null,
            $sendInfoEntiteToModeles
        );

        $this->response->disableCache();
        $this->response->body($pdf);
        $this->response->type('application/pdf');
        $this->response->download('Traitement.pdf');

        return $this->response;
    }

    /**
     * Genere le traitement en cours de rédaction
     *
     * @param $fiche_id
     *
     * @access public
     *
     * @created 11/03/2021
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function genereTraitementEnCours($fiche_id)
    {
        return $this->genereTraitement($fiche_id);
    }

    /**
     * Genere les traitements de registre
     *
     * @param json $tabId
     * @return data
     *
     * @access public
     * @created 09/01/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function genereTraitementNonVerrouiller($tabId)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty(json_decode($tabId))) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $useModelPresentation = $this->useModelePresentation($this->Session->read('Organisation.id'));

        // On verifie si le dossier file existe. Si c'est pas le cas on le cree
        create_arborescence_files();

        $folder = TMP . "imprimerRegistreNonVerrouiller";
        $date = date('d-m-Y_H-i');

        //on verifie si le dossier existe. Si c'est pas le cas on le cree
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $files_concat = '';
        foreach (json_decode($tabId) as $ficheID) {
            if ($useModelPresentation === false) {
                // Sans modele de présentation
                $pdf = $this->_genereTraitement($ficheID, true, 'pdf');

                $monPDF = fopen($folder . DS . $ficheID . '.pdf', 'a');
                fputs($monPDF, $pdf);
                fclose($monPDF);

                //On concatene le chemin du fichier .pdf
                $files_concat .= $folder . DS . $ficheID . '.pdf ';
            } else {
                // Avec modele de présentation
                $odt = $this->_genereTraitement($ficheID, true,'odt');

                $monODT = fopen($folder . DS . $ficheID . '.odt', 'a');
                fputs($monODT, $odt);
                fclose($monODT);

                //On concatene le chemin du fichier .pdf
                $chemintraitementGenereODT[] = $folder . DS . $ficheID . '.odt';
            }
        }

        if ($useModelPresentation === true) {
            $pdf = $this->_genereRegistre($chemintraitementGenereODT);

            $this->response->disableCache();
            $this->response->body($pdf);
            $this->response->type('application/pdf');
            $this->response->download('Registre_' . $date . '.pdf');

            $monRegistrePDF = fopen(CHEMIN_REGISTRE . 'Registre_' . $date . '.pdf', 'a');
            fputs($monRegistrePDF, $this->response);
            fclose($monRegistrePDF);
        } else {
            /**
             * On concatene tout les PDFs qu'on a cree et on enregistre
             * la concatenation dans /var/www/webdpo/app/files/registre
             */
            shell_exec('pdftk' . ' ' . $files_concat . 'cat output ' . CHEMIN_REGISTRE . 'Registre_' . $date . '.pdf');

            //On propose le telechargement
            $this->response->file(CHEMIN_REGISTRE . 'Registre_' . $date . '.pdf', [
                'download' => true,
                'name' => 'Registre_' . $date . '.pdf'
            ]);
        }

        // On supprime de dossier imprimerRegistreNonVerrouiller dans /tmp
        shell_exec('rm -rf ' . realpath($folder));

        return $this->response;
    }

    /**
     * On récupére en BDD si l'entité utilise un modele de presentation
     *
     * @param int $organisation_id
     * @return bool
     */
    private function useModelePresentation($organisation_id)
    {
        $useModelePresentationOrganisation = $this->Organisation->find('first', [
            'conditions' => [
                'Organisation.id' => $organisation_id
            ],
            'fields' => [
                'Organisation.usemodelepresentation'
            ]
        ]);

        return $useModelePresentationOrganisation['Organisation']['usemodelepresentation'];
    }

    /**
     * Retourne le PDF des extraits de registre dont les id sont passés en
     * paramètres.
     *
     * Si aucun id n'est passé en paramètre, un message d'erreur est stocké en
     * session et on est redirigé vers l'écran de visualisation des registres.
     *
     * @param json $tabId
     * @return string
     *
     * @access public
     *
     * @created 07/04/2017
     * @version V1.0.0
     * @author Christian BUFFIN <christian.buffin@libriciel.coop>
     *
     * @created 07/10/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _genereExtraitRegistre($tabId)
    {
        $organisation_id = $this->Session->read('Organisation.id');

        // On récupére le modèle de l'extrait de registre
        $modeleExtrait = $this->ModeleExtraitRegistre->find('first', [
            'conditions' => [
                $this->ModeleExtraitRegistre->getModeleExtraitRegistreConditionOrganisation($organisation_id)
            ]
        ]);

        // On vérifie que l'organisation à bien un modèle d'extrait de registre
        if (empty($modeleExtrait)) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorRecuperationModeleExtraitRegistre'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        // On vérifie que ce modèle de l'extrait de registre est présent sur le disque
        $pathModeleExtrait = CHEMIN_MODELES_EXTRAIT;
        $fileModeleExtrait = $modeleExtrait['ModeleExtraitRegistre']['fichier'];

        if (file_exists($pathModeleExtrait . $fileModeleExtrait) == false) {
            $this->Session->setFlash(__d('fiche', 'fiche.flasherrorPresenceModeleServeur'), 'flasherror');
            $this->redirect($this->Referers->get());
        }

        // On recupere en BDD si l'entité utilise un modele de présentation
        $useModelPresentation = $this->useModelePresentation($organisation_id);

        if ($useModelPresentation === true) {
            $folderExtraitRegistreTMP = TMP . "imprimerExtraitRegistre";
            $date = date('d-m-Y_H-i');

            // Avec modele de présentation
            $odt = $this->Fiche->preparationGeneration(
                $tabId,
                $fileModeleExtrait,
                $pathModeleExtrait,
                $organisation_id,
                false,
                'odt',
                false
            );

            // On verifie si le dossier existe. Si c'est le cas on supprime tous les fichiers, sinon on le cree
            if (file_exists($folderExtraitRegistreTMP) === true) {
                $files = glob($folderExtraitRegistreTMP.'/*');
                foreach($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
            } else {
                mkdir($folderExtraitRegistreTMP, 0777, true);
            }

            $pathFileTMP = $folderExtraitRegistreTMP . DS . 'Extrait_Registre.odt';
            $monODT = fopen($pathFileTMP, 'a');
            fputs($monODT, $odt);
            fclose($monODT);

            $chemintraitementGenereODT[] = $pathFileTMP;

            return $this->_genereRegistre($chemintraitementGenereODT, true);

        } else {
            return $this->Fiche->preparationGeneration(
                $tabId,
                $fileModeleExtrait,
                $pathModeleExtrait,
                $organisation_id,
                false,
                'pdf',
                true
            );
        }
    }

    /**
     * Genere l'extrait de registre
     *
     * @param json $tabFichesIds
     * @return data
     *
     * @access public
     * @created 09/01/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function genereExtraitRegistre($tabFichesIds)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty(json_decode($tabFichesIds))) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $pdf = $this->_genereExtraitRegistre($tabFichesIds);

        $this->response->disableCache();
        $this->response->body($pdf);
        $this->response->type('application/pdf');
        $this->response->download('ExtraitRegistre.pdf');

        return $this->response;
    }

    public function genereExtraitRegistreHtml($tabFichesIds)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty(json_decode($tabFichesIds))) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $this->set('title', 'Extrait de registre de ' . $this->Session->read('Organisation.raisonsociale'));

        $organisation_id = $this->Session->read('Organisation.id');

        $data = $this->Fiche->generationExtraitRegistreHtml($organisation_id, $tabFichesIds);
        $this->set(compact('data'));

        ob_start();
        $view = new View($this, false);
        $payment_form = $view->render('/Registres/extrait_html', 'extrait');
        ob_end_clean();

        // On verifie si le dossier file existe. Si c'est pas le cas on le cree
        $success = create_arborescence_files();
        $date = date('Y-m-d_H-i-s');
        $nameFileHtml = "extrait_registre_{$date}.html";
        if ($success === true) {
            $htmlExtraitRegistre = fopen(CHEMIN_EXTRAIT_REGISTRE_HTML . $nameFileHtml, 'w+');
            fputs($htmlExtraitRegistre, $payment_form);
            fclose($htmlExtraitRegistre);
        }

        $this->response->disableCache();
        $this->response->body($payment_form);
        $this->response->type('text/html');
        $this->response->download($nameFileHtml);

        return $this->response;
    }

    /**
     * Gère l'archivage des fiches
     *
     * @param int $id
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @deprecated V2.1.0 18/11/2020
     */
    public function archive($id)
    {
        debug('archive -> FichesController');
        return 403;

        if (empty($id)) {
            $this->Session->setFlash(__d('default', 'default.flasherrorTraitementInexistant'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $this->Droits->assertRecordAuthorized('Fiche', $id);

        $success = true;
        $this->Fiche->begin();

        $pdfTraitement = $this->_genereTraitement(json_encode($id));

        // Si la génération n'est pas vide on enregistre les data du Traitement en base de données
        if (!empty($pdfTraitement)) {
            $this->TraitementRegistre->create([
                'fiche_id' => $id,
                'data' => $pdfTraitement
            ]);
            $success = $success && false !== $this->TraitementRegistre->save(null, ['atomic' => false]);
        } else {
            $success = false;
        }

        if ($success == true) {
            $pdfExtrait = $this->_genereExtraitRegistre(json_encode($id));

            // Si la génération n'est pas vide on enregistre les data de l'Extrait de registre en base de données
            if (!empty($pdfExtrait)) {
                $this->ExtraitRegistre->create([
                    'fiche_id' => $id,
                    'data' => $pdfExtrait
                ]);
                $success = $success && false !== $this->ExtraitRegistre->save(null, ['atomic' => false]);
            } else {
                $success = false;
            }

            if ($success == true) {
                $success = $success && $this->Fiche->EtatFiche->updateAll([
                        'actif' => false
                    ], [
                            'fiche_id' => $id,
                            'etat_id' => [EtatFiche::VALIDER_DPO, EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE],
                            'actif' => true
                        ]
                    ) !== false;

                if ($success == true) {
                    $this->Fiche->EtatFiche->create([
                        'EtatFiche' => [
                            'fiche_id' => $id,
                            'etat_id' => EtatFiche::ARCHIVER,
                            'previous_user_id' => $this->Auth->user('id'),
                            'user_id' => $this->Auth->user('id')
                        ]
                    ]);
                    $success = $success && false !== $this->Fiche->EtatFiche->save(null, ['atomic' => false]);

                    if ($success == true) {
                        $contentHistorique = $this->Auth->user('nom_complet') . ' ' . __d ('historique', 'historique.verrouillerTraitement');
                        $success = $success && false !== $this->_saveHistorique($contentHistorique, $id);
                    }
                }
            }
        }

        if ($success == true) {
            $this->Fiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementArchiver'), 'flashsuccess');
        } else {
            $this->Fiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @param $tabId
     */
    public function export($tabId)
    {
        // On vérifie que $tadId n'est pas vide
        if (empty(json_decode($tabId))) {
            $this->Session->setFlash(__d('registre', 'registre.flasherrorAucunTraitementSelectionner'), 'flasherror');

            $this->redirect($this->Referers->get());
        }

        $ids = json_decode($tabId);

        // Récupération des colonnes
        // @todo: vérifier qu'il suffit d'ordeonner par ligne, colonne
        // @fixme: vérifier qu'il s'agit du même formulaire
        $headers = [];
        $headersTemplate = [
            'generale' => [
                'declarantpersonnenom' => __d('fiche', 'fiche.champDeclarantpersonnenom'),
                'declarantservice' => __d('fiche', 'fiche.champDeclarantservice'),
                'declarantpersonneemail' => __d('fiche', 'fiche.champDeclarantpersonneemail'),
                'norme' => "Norme",
                'numero_enregistrement' => "Numero d'enregistrement au registre",
                'outilnom' => __d('fiche', 'fiche.champOutilnom'),
                'finaliteprincipale' => __d('fiche', 'fiche.champFinaliteprincipale'),
                'sousFinalite' => 'Sous finalité',
                'transfert_hors_ue' => __d('fiche', 'fiche.champTransfertHorsUe'),
                'horsue' => __d('fiche', 'fiche.textInfoTransfereHorsUEComplementaire'),
                'donnees_sensibles' => __d('fiche', 'fiche.champDonneesSensibles'),
                'donneessensibles' => __d('fiche', 'fiche.textInfoDonneesSensiblesComplementaire'),
                'obligation_pia' => __d('fiche', 'fiche.champObligationPia'),
                'realisation_pia' => __d('fiche', 'fiche.champRealisationPia'),
                'depot_pia' => __d('fiche', 'fiche.champDepotPia'),
                'coresponsable' => __d('fiche', 'fiche.champCoresponsable'),
                'soustraitance' => __d('fiche', 'fiche.champSoustraitance'),
                'baselegale' => __d('fiche', 'fiche.champBaselegale'),
                'decisionAutomatisee' => __d('fiche', 'fiche.champDecisionAutomatisee'),
                'descriptionDecisionAutomatisee' => __d('fiche', 'fiche.champDescriptionDecisionAutomatisee'),
            ],
            'formulaire' => [],
            'entite' => [
                'rt_organisation_raisonsociale' => __d('fiche', 'fiche.champRaisonsociale'),
                'rt_organisation_siret' => __d('fiche', 'fiche.champSiret'),
                'rt_organisation_ape' => __d('fiche', 'fiche.champApe'),
                'rt_organisation_telephone' => __d('fiche', 'fiche.champTelephone'),
                'rt_organisation_adresse' => __d('fiche', 'fiche.champAdresse'),
                'rt_organisation_email' => __d('fiche', 'fiche.champEmail'),
                'rt_organisation_sigle' => __d('fiche', 'fiche.champSigle'),
                'rt_organisation_civiliteresponsable' => __d('fiche', 'fiche.champCiviliteresponsable'),
                'rt_organisation_prenomresponsable' => __d('fiche', 'fiche.champPrenomresponsable'),
                'rt_organisation_nomresponsable' => __d('fiche', 'fiche.champNomresponsable'),
                'rt_organisation_fonctionresponsable' => __d('fiche', 'fiche.champFonctionresponsable'),
                'rt_organisation_emailresponsable' => __d('fiche', 'fiche.champEmailresponsable'),
                'rt_organisation_telephoneresponsable' => __d('fiche', 'fiche.champTelephoneresponsable'),
                'rt_organisation_nom_complet_dpo' => __d('fiche', 'fiche.champDpo'),
                'rt_organisation_numerodpo' => __d('fiche', 'fiche.champNumerodpo'),
                'rt_organisation_emaildpo' => __d('fiche', 'fiche.champEmailDpo'),
            ]
        ];

        $fields = $this->Fiche->find('all', [
            'fields' => [
                'Fiche.form_id',
                'Champ.details'
            ],
            'joins' => [
                $this->Fiche->join('Formulaire', ['type' => 'INNER']),
                $this->Fiche->Formulaire->join('Champ'),
            ],
            'conditions' => [
                'Fiche.id' => $ids
            ],
            'group' => [
                'Fiche.form_id',
                'Champ.ligne',
                'Champ.colonne',
                'Champ.details'
            ],
            'order' => [
                'Champ.ligne ASC',
                'Champ.colonne ASC',
                'Champ.details ASC'
            ]
        ]);
        $formIds = array_unique(Hash::extract($fields, '{n}.Fiche.form_id'));

        foreach($formIds as $formId) {
            $headers[$formId] = $headersTemplate['generale'];
        }

        $optionsFormulaire = [];
        foreach ($fields as $field) {
            $details = json_decode($field['Champ']['details'], true);

            if (!empty($details)) {
                if (isset($details['options'])) {
                    $optionsFormulaire[$field['Fiche']['form_id']][$details['name']] = $details['options'];
                }

                if (isset($details['name'])) {
                    $headers[$field['Fiche']['form_id']][$details['name']] = $details['label'];
                } else {
                    $headers[$field['Fiche']['form_id']][$details['content']] = $details['content'];
                }
            }
        }

        // Pour la possition des traitements à l'exportation
        $case = [];
        foreach ($ids as $key => $value) {
            $case[] = sprintf('WHEN "Fiche"."id" = %d THEN %d', $value, $key);
        }
        $case = sprintf('(CASE %s END) ASC', implode(' ', $case));

        // Récupération des données
        $traitements = $this->Fiche->find('all', [
            'fields' => [
                'Fiche.id',
                'Fiche.form_id',
                'Fiche.norme_id',
                'Fiche.numero',
                'Fiche.coresponsable',
                'Fiche.soustraitance',
                'Fiche.obligation_pia',
                'Fiche.realisation_pia',
                'Fiche.depot_pia',
                'Fiche.rt_externe',
                'Fiche.transfert_hors_ue',
                'Fiche.donnees_sensibles',
                'Fiche.service_id'
            ],
            'conditions' => [
                'Fiche.id' => $ids
            ],
            'contain' => [
                'Formulaire' => [
                    'fields' => [
                        'Formulaire.libelle',
                        'Formulaire.usesousfinalite',
                        'Formulaire.usebaselegale',
                        'Formulaire.usedecisionautomatisee',
                        'Formulaire.usetransferthorsue',
                        'Formulaire.usedonneessensible',
                        'Formulaire.usepia',
                        'Formulaire.oldformulaire',
                        'Formulaire.rt_externe'
                    ]
                ],
                'Valeur' => [
                    'fields' => [
                        'Valeur.valeur',
                        'Valeur.champ_name'
                    ]
                ],
                'Norme' => [
                    'fields' => [
                        'Norme.norme',
                        'Norme.numero'
                    ]
                ],
                'Service' => [
                    'fields' => [
                        'Service.id',
                        'Service.libelle'
                    ]
                ]
            ],
            'order' => [
                $case
            ]
        ]);

        $data = [];
        foreach ($traitements as $traitement) {
            $row = [];
            $traitementFicheId = $traitement['Fiche']['id'];
            $traitementFormId = $traitement['Fiche']['form_id'];

            if ($traitement['Formulaire']['oldformulaire'] == true) {
                unset($headers[$traitementFormId]['sousFinalite']);
                unset($headers[$traitementFormId]['obligation_pia']);
                unset($headers[$traitementFormId]['realisation_pia']);
                unset($headers[$traitementFormId]['depot_pia']);
                unset($headers[$traitementFormId]['baselegale']);
            } else {
                if ($traitement['Formulaire']['usesousfinalite'] === false) {
                    unset($headers[$traitementFormId]['sousFinalite']);
                }

                if ($traitement['Formulaire']['usebaselegale'] === false) {
                    unset($headers[$traitementFormId]['baselegale']);
                }

                if ($traitement['Formulaire']['usedecisionautomatisee'] === false) {
                    unset($headers[$traitementFormId]['decisionAutomatisee']);
                    unset($headers[$traitementFormId]['descriptionDecisionAutomatisee']);
                }

                if ($traitement['Formulaire']['usetransferthorsue'] === false) {
                    unset($headers[$traitementFormId]['horsue']);
                }

                if ($traitement['Formulaire']['usedonneessensible'] === false) {
                    unset($headers[$traitementFormId]['donneessensibles']);
                }

                if ($traitement['Formulaire']['usepia'] === false) {
                    unset($headers[$traitementFormId]['obligation_pia']);
                }
            }

            if ($traitement['Fiche']['rt_externe'] === true) {
                $headersTemplate['generale']['soustraitance'] = __d('fiche', 'fiche.champSoustraitanceUlterieur');

                unset($headersTemplate['entite']);

                $headersTemplate['entite'] = [
                    'rt_externe_raisonsociale' => __d('rt_externe', 'rt_externe.champRtExterneRaisonsociale'),
                    'rt_externe_siret' => __d('rt_externe', 'rt_externe.champRtExterneSiret'),
                    'rt_externe_ape' => __d('rt_externe', 'rt_externe.champRtExterneApe'),
                    'rt_externe_telephone' => __d('rt_externe', 'rt_externe.champRtExterneTelephone'),
                    'rt_externe_fax' => __d('rt_externe', 'rt_externe.champRtExterneFax'),
                    'rt_externe_adresse' => __d('rt_externe', 'rt_externe.champRtExterneAdresse'),
                    'rt_externe_email' => __d('rt_externe', 'rt_externe.champRtExterneEmail'),
                    'rt_externe_civiliteresponsable' => __d('rt_externe', 'rt_externe.champRtExterneCiviliteresponsable'),
                    'rt_externe_prenomresponsable' => __d('rt_externe', 'rt_externe.champRtExternePrenomresponsable'),
                    'rt_externe_nomresponsable' => __d('rt_externe', 'rt_externe.champRtExterneNomresponsable'),
                    'rt_externe_fonctionresponsable' => __d('rt_externe', 'rt_externe.champRtExterneFonctionresponsable'),
                    'rt_externe_emailresponsable' => __d('rt_externe', 'rt_externe.champRtExterneEmailresponsable'),
                    'rt_externe_telephoneresponsable' => __d('rt_externe', 'rt_externe.champRtExterneTelephoneresponsable'),
                    'rt_externe_civility_dpo' => __d('rt_externe', 'rt_externe.champRtExterneCivilityDpo'),
                    'rt_externe_prenom_dpo' => __d('rt_externe', 'rt_externe.champRtExternePrenomDpo'),
                    'rt_externe_nom_dpo' => __d('rt_externe', 'rt_externe.champRtExterneNomDpo'),
                    'rt_externe_numerocnil_dpo' => __d('rt_externe', 'rt_externe.champRtExterneNumerocnilDpo'),
                    'rt_externe_email_dpo' => __d('rt_externe', 'rt_externe.champRtExterneEmailDpo'),
                    'rt_externe_telephonefixe_dpo' => __d('rt_externe', 'rt_externe.champRtExterneTelephonefixeDpo'),
                    'rt_externe_telephoneportable_dpo' => __d('rt_externe', 'rt_externe.champRtExterneTelephoneportableDpo'),
                    'st_organisation_raisonsociale' => __d('fiche', 'fiche.champRaisonsocialeStructureSoustraitant'),
                    'st_organisation_siret' => __d('fiche', 'fiche.champSiretStructureSoustraitant'),
                    'st_organisation_ape' => __d('fiche', 'fiche.champApeStructureSoustraitant'),
                    'st_organisation_telephone' => __d('fiche', 'fiche.champTelephoneStructureSoustraitant'),
                    'st_organisation_fax' => __d('fiche', 'fiche.champFaxStructureSoustraitant'),
                    'st_organisation_adresse' => __d('fiche', 'fiche.champAdresseStructureSoustraitant'),
                    'st_organisation_email' => __d('fiche', 'fiche.champEmailStructureSoustraitant'),
                    'st_organisation_sigle' => __d('fiche', 'fiche.champSigleStructureSoustraitant'),
                    'st_organisation_civiliteresponsable' => __d('fiche', 'fiche.champCiviliteresponsableStructureSoustraitant'),
                    'st_organisation_prenomresponsable' => __d('fiche', 'fiche.champPrenomresponsableStructureSoustraitant'),
                    'st_organisation_nomresponsable' => __d('fiche', 'fiche.champNomresponsableStructureSoustraitant'),
                    'st_organisation_fonctionresponsable' => __d('fiche', 'fiche.champFonctionresponsableStructureSoustraitant'),
                    'st_organisation_emailresponsable' => __d('fiche', 'fiche.champEmailresponsableStructureSoustraitant'),
                    'st_organisation_telephoneresponsable' => __d('fiche', 'fiche.champTelephoneresponsableStructureSoustraitant'),
                    'st_organisation_nom_complet_dpo' => __d('fiche', 'fiche.champDpoStructureSoustraitant'),
                    'st_organisation_numerodpo' => __d('fiche', 'fiche.champNumerodpoStructureSoustraitant'),
                    'st_organisation_emaildpo' => __d('fiche', 'fiche.champEmailDpoStructureSoustraitant'),
                    'st_organisation_telephonefixe_dpo' => __d('fiche', 'fiche.champFixDpoStructureSoustraitant'),
                    'st_organisation_telephoneportable_dpo' => __d('fiche', 'fiche.champPortableDpoStructureSoustraitant'),
                ];
            }

            foreach($formIds as $formId) {
                $headers[$formId] = array_merge($headers[$formId], $headersTemplate['entite']);
            }

            $valeurs = Hash::combine($traitement, 'Valeur.{n}.champ_name', 'Valeur.{n}.valeur');

            foreach ($valeurs as $key => $valeur) {
                if (!empty($optionsFormulaire[$traitementFormId]) &&
                    array_key_exists($key, $optionsFormulaire[$traitementFormId])
                ){
                    if ($this->Fiche->isJson($valeur) === true) {
                        $valeur = json_decode($valeur);
                    }

                    if (is_array($valeur)) {
                        $tmp = "";
                        foreach ($valeur as $val) {
                            if (empty($tmp)) {
                                $tmp = $optionsFormulaire[$traitementFormId][$key][$val];
                            } else {
                                $tmp = $tmp . "\n" .$optionsFormulaire[$traitementFormId][$key][$val];
                            }
                        }
                    } else {
                        if (isset($optionsFormulaire[$traitementFormId][$key][$valeur])) {
                            $tmp = $optionsFormulaire[$traitementFormId][$key][$valeur];
                        } else {
                            $newKey = array_search($valeur, $optionsFormulaire[$traitementFormId][$key]);
                            $tmp = $optionsFormulaire[$traitementFormId][$key][$newKey];
                        }
                    }
                    $valeurs[$key] = $tmp;
                } else {
                    if ($this->Fiche->isJson($valeurs[$key]) === true) {
                        $valeur = json_decode($valeur);

                        if (is_array($valeur)) {
                            $tmp = "";
                            foreach ($valeur as $val) {
                                $tmp = $tmp . "\n" . $val;
                            }
                            $valeurs[$key] = $tmp;
                        }

                        if (is_object($valeur)) {
                            $tmp = "";
                            foreach ($valeur as $k => $val) {
                                foreach ($val as $v) {
                                    $tmp = $tmp . "\n" . $v;
                                }

                                $tmp = $tmp . "\n";
                            }

                            $valeurs[$key] = $tmp;
                        }

                    }
                }
            }

            foreach (array_keys($headers[$traitementFormId]) as $fieldname) {
                $row[$fieldname] = Hash::get($valeurs, $fieldname);
            }

            $data[$traitementFormId][$traitementFicheId] = $row;
            $data[$traitementFormId][$traitementFicheId]['numero_enregistrement'] = $traitement['Fiche']['numero'];
            if (isset($traitement['Norme']['id'])) {
                $data[$traitementFormId][$traitementFicheId]['norme'] = $traitement['Norme']['norme'] . '-' . $traitement['Norme']['numero'];
            }

            $data[$traitementFormId][$traitementFicheId]['transfert_hors_ue'] = $this->convertBoolToString($traitement['Fiche']['transfert_hors_ue']);
            $data[$traitementFormId][$traitementFicheId]['donnees_sensibles'] = $this->convertBoolToString($traitement['Fiche']['donnees_sensibles']);
            $data[$traitementFormId][$traitementFicheId]['obligation_pia'] = $this->convertBoolToString($traitement['Fiche']['obligation_pia']);
            $data[$traitementFormId][$traitementFicheId]['realisation_pia'] = $this->convertBoolToString($traitement['Fiche']['realisation_pia']);
            $data[$traitementFormId][$traitementFicheId]['depot_pia'] = $this->convertBoolToString($traitement['Fiche']['depot_pia']);
            $data[$traitementFormId][$traitementFicheId]['coresponsable'] = $this->convertBoolToString($traitement['Fiche']['coresponsable']);
            $data[$traitementFormId][$traitementFicheId]['soustraitance'] = $this->convertBoolToString($traitement['Fiche']['soustraitance']);

            if (isset($traitement['Service']['libelle'])) {
                $data[$traitementFormId][$traitementFicheId]['declarantservice'] = $traitement['Service']['libelle'];
            }

            $nameFileCsv[$traitementFormId] = str_replace(' ', '_', $traitement['Formulaire']['libelle']);
        }

        foreach ($headers as $key => $header) {
            $csv[$key] = array_merge(
                [$header],
                Hash::extract($data[$key], '{n}')
            );
        }
        
        // On verifie si le dossier file existe. Si c'est pas le cas on le cree
        create_arborescence_files();

        $date = date("Y-m-d_H-i-s");
        if (!file_exists(CHEMIN_EXPORTS . $date)) {
            mkdir(CHEMIN_EXPORTS . $date, 0777, true);
        }

        $csvFiles = [];
        foreach ($csv as $key => $valueCsv) {
            //@fixme générer des noms de fichiers ou un dossier temporaire
            $chemin = CHEMIN_EXPORTS . $date . DS . $nameFileCsv[$key].'_'.$date.'.csv';
            $fp = fopen($chemin, 'w');

            foreach ($valueCsv as $fields) {
                fputcsv($fp, $fields);
            }

            fclose($fp);
            $csvFiles[] = $chemin;
        }

        $path = tempnam(TMP, $this->Session->id().'_');
        $zip = new ZipArchive(); 

        if($zip->open($path, ZipArchive::CREATE) === true) {
            foreach ($csvFiles as $csvFile) {
                $zip->addFile($csvFile, basename($csvFile));
            }
            $zip->close();

        } else {
            $this->Session->setFlash(__d('fiche', 'flasherrorErreurCreationZip'), 'flasherror');
        }
        
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename=Extrait-CSV-'.$date.'.zip');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        unlink($path);
        
        exit();
    }

    private function convertBoolToString($val)
    {
        if (is_null($val) === true) {
            return '';
        }

        if ($val === true) {
            return 'Oui';
        } elseif ($val === false) {
            return 'Non';
        }
    }

    /**
     * Récupére en BDD les normes et les descriptions
     *
     * @access private
     *
     * @created 25/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getNormes($norme_id = null)
    {
        $query = [
            'conditions' => [
                'abroger' => false
            ],
            'fields' => [
                'id',
                'norme',
                'numero',
                'libelle',
                'description'
            ],
            'order' => [
                'norme',
                'numero'
            ]
        ];
        $normes = $this->Norme->find('all', $query);

        $options_normes = Hash::combine(
            $normes,
            '{n}.Norme.id',
            [
                '%s-%s : %s',
                '{n}.Norme.norme',
                '{n}.Norme.numero',
                '{n}.Norme.libelle'
            ]
        );

        if ($norme_id !== null) {
            if (array_key_exists($norme_id, $options_normes) === false) {
                $normes = $this->Norme->find('all', [
                    'conditions' => [
                        'id' => $norme_id
                    ],
                    'fields' => [
                        'id',
                        'norme',
                        'numero',
                        'libelle',
                        'description'
                    ],
                    'order' => [
                        'norme',
                        'numero'
                    ]
                ]);

                $options_normes += Hash::combine(
                    $normes,
                    '{n}.Norme.id',
                    [
                        '%s-%s : %s',
                        '{n}.Norme.norme',
                        '{n}.Norme.numero',
                        '{n}.Norme.libelle'
                    ]
                );
            }
        }

        $descriptions_normes = Hash::combine(
            $normes,
            '{n}.Norme.id',
            '{n}.Norme.description'
        );

        $this->set(compact('options_normes', 'descriptions_normes'));
    }

    private function getReferentiel($referentiel_id = null)
    {
        $query = [
            'conditions' => [
                'abroger' => false
            ],
            'fields' => [
                'id',
                'name'
            ],
            'order' => [
                'name ASC'
            ]
        ];
        $options_referentiel = $this->Referentiel->find('list', $query);

        if ($referentiel_id !== null) {
            if (array_key_exists($referentiel_id, $options_referentiel) === false) {
                $query = [
                    'conditions' => [
                        'id' => $referentiel_id
                    ],
                    'fields' => [
                        'id',
                        'name'
                    ]
                ];
                $options_referentiel = $this->Referentiel->find('list', $query);
            }
        }

        $this->set(compact('options_referentiel'));
    }

    /**
     * Récupére en BDD les options défini dans le formulaire en rapport avec les champs sous-finalité, base légale,
     * décision automatique, transfert hors ue, données sensible
     *
     * @param int $formulaire_id ID du formulaire utilisé
     *
     * @access private
     *
     * @created 25/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getOptionsFields($formulaire_id)
    {
        $useFieldsFormulaire = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $formulaire_id
            ],
            'fields' => [
                'usesousfinalite',
                'usebaselegale',
                'usedecisionautomatisee',
                'usetransferthorsue',
                'usedonneessensible'
            ]
        ]);

        $useAllExtensionFiles = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $formulaire_id
            ],
            'fields' => [
                'useallextensionfiles'
            ]
        ]);

        $usePIA = $this->Formulaire->find('first', [
            'conditions' => [
                'id' => $formulaire_id
            ],
            'fields' => [
                'usepia'
            ]
        ]);

        $this->set(compact('useFieldsFormulaire', 'useAllExtensionFiles', 'usePIA'));
    }

    /**
     * Récupére en BDD les champs défini dans le formulaire en fonction des différents onglet
     * Ajoute les valeurs par défault défini dans le formulaire
     *
     * @param int $formulaire_id ID du formulaire utilisé
     * @param bool $useValueDefault Permet l'utilisation de valeur par défault défini dans le formulaire
     *
     * @access private
     *
     * @created 25/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getFormulaireFields($formulaire_id, $useValueDefault = false)
    {
        $formulaireFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => false,
                'champ_soustraitant' => false,
            ],
            'fields' => [
                'type',
                'ligne',
                'colonne',
                'details'
            ],
            'order' => [
                'colonne ASC',
                'ligne ASC'
            ]
        ]);

        $coresponsableFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => true,
                'champ_soustraitant' => false,
            ],
            'fields' => [
                'type',
                'ligne',
                'colonne',
                'details'
            ],
            'order' => [
                'colonne ASC',
                'ligne ASC'
            ]
        ]);

        $soustraitantFields = $this->Champ->find('all', [
            'conditions' => [
                'formulaire_id' => $formulaire_id,
                'champ_coresponsable' => false,
                'champ_soustraitant' => true,
            ],
            'fields' => [
                'type',
                'ligne',
                'colonne',
                'details'
            ],
            'order' => [
                'colonne ASC',
                'ligne ASC'
            ]
        ]);

        $fields =  [
          'formulaire' =>  $formulaireFields,
          'coresponsable' =>  $coresponsableFields,
          'soustraitant' =>  $soustraitantFields,
        ];

        if ($useValueDefault === true) {
            foreach ($fields as $field) {
                $details = Hash::extract($field, '{n}.Champ.details');
                foreach ($details as $detail) {
                    $detail = (array)json_decode($detail);

                    if (!empty($detail['default'])) {
                        $this->request->data['WebdpoFiche'][$detail['name']] = $detail['default'];
                    }
                }
            }
        }

        $this->set(compact('fields'));
    }

    /**
     * Récupére en BDD les fichiers associés au traitement
     *
     * @param int $fiche_id ID du traitement
     *
     * @access private
     *
     * @created 27/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getFilesSave($fiche_id)
    {
        $filesSave = $this->Fichier->find('all', [
            'conditions' => [
                'fiche_id' => $fiche_id
            ]
        ]);

        if (!empty($filesSave)) {
            foreach ($filesSave as $key => $file) {
                if (!isset($this->request->data['Fichier']['typage_'.$file['Fichier']['id']])) {
                    $this->request->data['Fichier']['typage_' . $file['Fichier']['id']] = $file['Fichier']['typage_id'];
                }
            }
        }

        $this->set(compact('filesSave'));
    }

    /**
     * Récupére en BDD les valeurs des champs du traitement
     *
     * @param int $fiche_id ID du traitement
     *
     * @access private
     *
     * @created 27/02/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getValueFields($fiche_id, $rt_externe = false)
    {
        $prefix = 'rt_';
        if ($rt_externe === true) {
            $prefix = 'st_';
        }

        $changeArrayFields = [
            'declarantpersonnenom',
            'declarantservice',
            'declarantpersonneemail',
            'declarantpersonnefix',
            'declarantpersonneportable',
            $prefix.'organisation_raisonsociale',
            $prefix.'organisation_telephone',
            $prefix.'organisation_fax',
            $prefix.'organisation_adresse',
            $prefix.'organisation_email',
            $prefix.'organisation_sigle',
            $prefix.'organisation_siret',
            $prefix.'organisation_ape',
            $prefix.'organisation_civiliteresponsable',
            $prefix.'organisation_nomresponsable',
            $prefix.'organisation_prenomresponsable',
            $prefix.'organisation_fonctionresponsable',
            $prefix.'organisation_emailresponsable',
            $prefix.'organisation_telephoneresponsable',
            $prefix.'organisation_nom_complet_dpo',
            $prefix.'organisation_numerodpo',
            $prefix.'organisation_emaildpo',
            $prefix.'organisation_telephonefixe_dpo',
            $prefix.'organisation_telephoneportable_dpo'
        ];

        $valeurs = $this->Valeur->find('all', [
            'conditions' => [
                'fiche_id' => $fiche_id
            ],
            'fields' => [
                'id',
                'valeur',
                'champ_name'
            ]
        ]);

        foreach ($valeurs as $key => $value) {
            if ($this->Fiche->isJson($value['Valeur']['valeur']) === true) {
                $valueDecode = json_decode($value['Valeur']['valeur'], true);

//                if (is_object($valueDecode)) {
//                    $valueDecode = json_decode(json_encode($valueDecode), true);
//                }

                if ($value['Valeur']['champ_name'] === 'coresponsabilitefields') {
                    $this->request->data['WebdpoCoresponsable'] = $valueDecode;
                } else if ($value['Valeur']['champ_name'] === 'soustraitancefields') {
                    $this->request->data['WebdpoSoustraitance'] = $valueDecode;
                } else {
                    $this->request->data['WebdpoFiche'][$value['Valeur']['champ_name']] = $valueDecode;
                }
            } else {
                if (in_array($value['Valeur']['champ_name'], $changeArrayFields) && in_array($this->request->params['action'], ['edit', 'show']) === true ) {
                    $this->request->data['Trash'][$value['Valeur']['champ_name']] = $value['Valeur']['valeur'];
                } else {
                    $this->request->data['WebdpoFiche'][$value['Valeur']['champ_name']] = $value['Valeur']['valeur'];
                }
            }
        }
    }

    private function deductValueFieldObligationPia($data)
    {
        foreach (Fiche::LISTING_FIELDS_TRAITEMENT_NO_REQUIRED_PIA as $listingFieldsPiaNoRequired) {
            if (!empty($data['WebdpoFiche'][$listingFieldsPiaNoRequired])) {
                if ($data['WebdpoFiche'][$listingFieldsPiaNoRequired] == 'Oui') {
                    return false;
                }
            } else {
                return null;
            }
        }

        foreach (Fiche::LISTING_FIELDS_TRAITEMENT_REQUIRED_PIA as $listingFieldsPiaRequired) {
            if (!empty($data['WebdpoFiche'][$listingFieldsPiaRequired])) {
                if ($data['WebdpoFiche'][$listingFieldsPiaRequired] == 'Oui') {
                    return true;
                }
            } else {
                return null;
            }
        }

        foreach (Fiche::LISTING_FIELDS_TRAITEMENT_REQUIRED_PIA as $listingFieldsPiaRequired) {
            if (!empty($data['WebdpoFiche'][$listingFieldsPiaRequired])) {
                if ($data['WebdpoFiche'][$listingFieldsPiaRequired] == 'Oui') {
                    return true;
                }
            } else {
                return null;
            }
        }

        if (empty($data['WebdpoFiche']['criteres'])) {
            return null;
        }

        $countOptionsChecked = count($data['WebdpoFiche']['criteres']);
        if ($countOptionsChecked >= 2) {
            return true;
        }

        if (empty($data['WebdpoFiche']['traitement_considere_risque'])) {
            return null;
        }
        if ($countOptionsChecked == 1) {
            if ($data['WebdpoFiche']['traitement_considere_risque'] == 'Oui') {
                return true;
            }

            if ($data['WebdpoFiche']['traitement_considere_risque'] == 'Non') {
                return false;
            }
        }

        return null;
    }

    /**
     * Récupere tous les types d'annexe associé à l'entité
     *
     * @return mixed
     *
     * @access protected
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 22/04/2020
     * @version V2.0.0
     */
    protected function _typages()
    {
        $query = [
            'conditions' => [],
            'fields' => [
                'id',
                'libelle'
            ],
            'order' => [
                'Typage.libelle ASC'
            ]
        ];

        $subQuery = [
            'alias' => 'typages_organisations',
            'fields' => [
                'typages_organisations.typage_id'
            ],
            'conditions' => [
                'typages_organisations.typage_id = Typage.id',
                'typages_organisations.organisation_id' => $this->Session->read('Organisation.id')
            ]
        ];
        $sql = $this->TypageOrganisation->sql($subQuery);

        $query['conditions'][] = "Typage.id IN ( {$sql} )";

        $typages = $this->Typage->find('list', $query);

        return $typages;
    }

    /**
     *
     *
     * @access public
     *
     * @created 01/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function getOrganisationForSt()
    {
        $organisation = $this->Organisation->find('first', [
            'joins' => [
                $this->Organisation->join('Dpo', ['type' => 'INNER']),
            ],
            'conditions' => [
                'Organisation.id' => $this->Session->read('Organisation.id'),
                'Dpo.id = Organisation.dpo'
            ],
            'fields' => [
                'Organisation.raisonsociale',
                'Organisation.telephone',
                'Organisation.fax',
                'Organisation.adresse',
                'Organisation.email',
                'Organisation.sigle',
                'Organisation.siret',
                'Organisation.ape',
                'Organisation.civiliteresponsable',
                'Organisation.prenomresponsable',
                'Organisation.nomresponsable',
                'Organisation.emailresponsable',
                'Organisation.telephoneresponsable',
                'Organisation.fonctionresponsable',
                'Organisation.numerodpo',
                $this->Organisation->Dpo->vfNomComplet(),
                'Dpo.email',
                'Dpo.telephonefixe',
                'Dpo.telephoneportable',
            ]
        ]);

        foreach ($organisation['Organisation'] as $key => $value) {
            $this->request->data['Trash']['st_organisation_'.$key] = $value;
        }

        foreach ($organisation['Dpo'] as $key => $value) {
            $this->request->data['Trash']['st_organisation_'.$key.'_dpo'] = $value;
        }
    }

    /**
     * Enregistre l'historique de la fiche
     *
     * @param string $content : texte de l'historique
     * @param int $fiche_id : ID de la fiche
     * @return bool
     *
     * @access private
     *
     * @created 23/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _saveHistorique($content, $fiche_id)
    {
        $this->Historique->create([
            'Historique' => [
                'content' => $content,
                'fiche_id' => $fiche_id
            ]
        ]);
        $success = $this->Historique->save(null, ['atomic' => false]);

        return $success;
    }

    /**
     * Enregistre l'état de la fiche
     *
     * @param int $fiche_id : ID de la fiche
     * @param int $etat_id : ID du nouveau état de la fiche
     * @param int $previous_user_id : ID de l'ancien utilisateur détenteur de la fiche
     * @param int $user_id : ID du nouveau utilisateur détenteur de la fiche
     * @return bool
     *
     * @access private
     *
     * @created 23/06/2020
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function _saveEtatFiche($fiche_id, $etat_id, $previous_user_id, $user_id)
    {
        $this->EtatFiche->create([
            'EtatFiche' => [
                'fiche_id' => $fiche_id,
                'etat_id' => $etat_id,
                'previous_user_id' => $previous_user_id,
                'user_id' => $user_id
            ]
        ]);
        $success = $this->EtatFiche->save(null, ['atomic' => false]);

        return $success;
    }

    public function ajax_update_listing_responsable()
    {
        $this->autoRender = false;

        if ($this->request->is('get')) {
            return json_encode($this->_responsables());
        }

        return null;
    }

    public function ajax_update_listing_soustraitant()
    {
        $this->autoRender = false;

        if ($this->request->is('get')) {

            return json_encode($this->_soustraitants());
        }

        return null;
    }

    /**
     *
     * @access public
     *
     * @created 09/02/2021
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function dupliquerTraitement()
    {
        $this->autoRender = false;

        if ($this->request->is('POST')) {
            $data = $this->request->data;

            $fiche = $this->Fiche->find('first', [
                'contain' => [
                    'Fichier'
                ],
                'conditions' => [
                    'id' => $data['Fiche']['id']
                ],
                'fields' => [
                    'id',
                    'form_id',
                    'coresponsable',
                    'soustraitance'
                ]
            ]);

            $query = [
                'contain' => [
                    'FormulaireOrganisation' => [
                        'Organisation' => [
                            'id',
                            'raisonsociale',
                            'order' => ['raisonsociale']
                        ]
                    ],
                ],
                'conditions' => [
                    'Formulaire.id' => $fiche['Fiche']['form_id'],
                    $this->Formulaire->getFormulaireConditionOrganisation($data['Fiche']['organisationcible'])
                ],
                'fields' => [
                    'Formulaire.id',
                    'Formulaire.libelle',
                    'Formulaire.description',
                    'Formulaire.rt_externe',
                    'Formulaire.createdbyorganisation',
                    'Formulaire.created',
                    'Formulaire.archive'
                ],
                'order' => [
                    'Formulaire.libelle ASC'
                ]
            ];
            $formulaireAssocier = $this->Formulaire->find('first', $query);

            if (empty($formulaireAssocier)) {
                $this->Session->setFlash("Le formulaire utilisé pour ce traitement n'est pas associé à l'entité cible", 'flashwarning');
                $this->redirect($this->Referers->get());
            }

            if ($fiche['Fiche']['coresponsable'] === true) {
                $coresponsableFiche = $this->Coresponsable->find('all', [
                    'conditions' => [
                        'fiche_id' => $fiche['Fiche']['id']
                    ]
                ]);

                if (!empty($coresponsableFiche)) {
                    $coresponsableFiche = Hash::extract($coresponsableFiche, '{n}.Coresponsable.responsable_id');

                    $queryResponsable = [
                        'conditions' => [
                            $this->Responsable->getConditionOrganisation($data['Fiche']['organisationcible']),
                            'Responsable.id' => $coresponsableFiche
                        ]
                    ];
                    $countResponsables = $this->Responsable->find('count', $queryResponsable);

                    if ($countResponsables !== count($coresponsableFiche)) {
                        $this->Session->setFlash("Le traitement utilise des co-responsables qui ne sont pas associés à l'entité cible", 'flashwarning');
                        $this->redirect($this->Referers->get());
                    }
                }
            }

            if ($fiche['Fiche']['soustraitance'] === true) {
                $soustraitanceFiche = $this->Soustraitance->find('all', [
                    'conditions' => [
                        'fiche_id' => $fiche['Fiche']['id']
                    ]
                ]);

                if (!empty($soustraitanceFiche)) {
                    $soustraitanceFiche = Hash::extract($soustraitanceFiche, '{n}.Soustraitance.soustraitant_id');

                    $querySoustraitant = [
                        'conditions' => [
                            $this->Soustraitant->getConditionOrganisation($data['Fiche']['organisationcible']),
                            'Soustraitant.id' => $soustraitanceFiche
                        ]
                    ];
                    $countSoustraitants = $this->Soustraitant->find('count', $querySoustraitant);

                    if ($countSoustraitants !== count($soustraitanceFiche)) {
                        $this->Session->setFlash("Le traitement utilise des sous-traitants qui ne sont pas associés à l'entité cible", 'flashwarning');
                        $this->redirect($this->Referers->get());
                    }
                }
            }

            if (!empty($fiche['Fichier'])) {
                $typagesAnnexe = Hash::extract($fiche['Fichier'], '{n}.typage_id');

                foreach ($typagesAnnexe as $typageAnnexe) {
                    if (!empty($typageAnnexe)) {
                        $typageOrganisation = $this->TypageOrganisation->find('first', [
                            'conditions' => [
                               'TypageOrganisation.organisation_id' => $data['Fiche']['organisationcible'],
                               'TypageOrganisation.typage_id' => $typageAnnexe
                            ]
                        ]);

                        if (empty($typageOrganisation)) {
                            $this->Session->setFlash("Le traitement utilise des typages pour les annexes qui ne sont pas associés à l'entité cible", 'flashwarning');
                            $this->redirect($this->Referers->get());
                        }
                    }
                }
            }

            $this->Fiche->begin();

            $success = $this->copyFiche($data);

            if ($success == true) {
                $this->Fiche->commit();
                $this->Session->setFlash("Le traitement a bien été dupliqué", 'flashsuccess');
            } else {
                $this->Fiche->rollback();
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            }

            $this->redirect($this->Referers->get());
        }
    }

    /**
     * @param $data
     * @return bool
     *
     * @access protected
     *
     * @created 09/02/2021
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function copyFiche($data)
    {
        $success = true;

        $id = $data['Fiche']['id'];

        $ficheToDuplicate = $this->Fiche->find('first', [
            'contain' => [
                'Fichier'
            ],
            'conditions' => [
                'Fiche.id' => $id,
            ],
            'fields' => [
                'Fiche.id',
                'Fiche.form_id',
                'Fiche.norme_id',
                'Fiche.coresponsable',
                'Fiche.soustraitance',
                'Fiche.obligation_pia',
                'Fiche.realisation_pia',
                'Fiche.depot_pia',
                'Fiche.rt_externe',
                'Fiche.transfert_hors_ue',
                'Fiche.donnees_sensibles',
                'Fiche.coresponsable',
                'Fiche.soustraitance',
            ]
        ]);

        if (!empty($ficheToDuplicate)) {
            $user = $this->User->find('first', [
                'conditions' => [
                    'User.id' => $data['Fiche']['usercible']
                ],
                'fields' => [
                    'id',
                    'nom_complet_court',
                    'email',
                    'telephonefixe',
                    'telephoneportable',
                ]
            ]);

            $fields = $this->Valeur->find('all', [
                'conditions' => [
                    'Valeur.fiche_id' => $ficheToDuplicate['Fiche']['id'],
                    'Valeur.champ_name !=' => [
                        'declarantpersonnenom',
                        'declarantpersonneemail',
                        'declarantpersonneportable',
                        'declarantpersonnefix',
                    ]
                ],
                'fields' => [
                    'valeur',
                    'champ_name',
                ]
            ]);

            if (!empty($user) && !empty($fields)) {
                // C'est un nouveau traitement en renseignant les infos
                unset($ficheToDuplicate['Fiche']['id']);
                $ficheToDuplicate['Fiche']['numero'] = null;
                $ficheToDuplicate['Fiche']['user_id'] = $user['User']['id'];
                $ficheToDuplicate['Fiche']['organisation_id'] = (int)$data['Fiche']['organisationcible'];

                $this->Fiche->create($ficheToDuplicate);
                $success = $success && false !== $this->Fiche->save(null, ['atomic' => false]);
            } else {
                $success = false;
            }
        } else {
            $success = false;
        }

        if ($success == false) {
            return $success;
        }

        array_push($fields, [
            'Valeur' => [
                'valeur' => $user['User']['nom_complet_court'],
                'champ_name' => 'declarantpersonnenom'
            ]
        ]);

        $valeurServiceLibelle = 'Aucun service';
        if (!empty($data['Fiche']['userservicecible'])) {
            $service = $this->Service->find('first', [
                'conditions' => [
                    'Service.id' => $data['Fiche']['userservicecible']
                ],
                'fields' => [
                    'Service.libelle'
                ]
            ]);

            if (!empty($service['Service']['libelle'])) {
                $valeurServiceLibelle = $service['Service']['libelle'];
            }
        }
        array_push($fields, [
            'Valeur' => [
                'valeur' => $valeurServiceLibelle,
                'champ_name' => 'declarantservice'
            ]
        ]);

        array_push($fields, [
            'Valeur' => [
                'valeur' => $user['User']['email'],
                'champ_name' => 'declarantpersonneemail'
            ]
        ]);

        if (!empty($user['User']['telephoneportable'])) {
            array_push($fields, [
                'Valeur' => [
                    'valeur' => $user['User']['telephoneportable'],
                    'champ_name' => 'declarantpersonneportable'
                ]
            ]);
        }

        if (!empty($user['User']['telephonefixe'])) {
            array_push($fields, [
                'Valeur' => [
                    'valeur' => $user['User']['telephonefixe'],
                    'champ_name' => 'declarantpersonnefix'
                ]
            ]);
        }

        // On recupere l'id du traitement qu'on vien d'enregistré
        $fiche_id = $this->Fiche->getLastInsertId();

        // On vérifie que le traitement à dupliquer n'a pas de co-responsabilité
        if ($ficheToDuplicate['Fiche']['coresponsable'] === true) {
            $coresponsablesFiche = $this->Coresponsable->find('all', [
                'conditions' => [
                    'fiche_id' => $id
                ]
            ]);

            foreach ($coresponsablesFiche as $coresponsableFiche) {
                $this->Coresponsable->create([
                    'fiche_id' => $fiche_id,
                    'responsable_id' => $coresponsableFiche['Coresponsable']['responsable_id']
                ]);
                $success = $success && false !== $this->Coresponsable->save(null, ['atomic' => false]);
            }
        }

        // On vérifie que le traitement à dupliquer n'a pas de sous-traitance
        if ($ficheToDuplicate['Fiche']['soustraitance'] === true) {
            $soustraitancesFiche = $this->Soustraitance->find('all', [
                'conditions' => [
                    'fiche_id' => $id
                ]
            ]);

            foreach ($soustraitancesFiche as $soustraitanceFiche) {
                $this->Soustraitance->create([
                    'fiche_id' => $fiche_id,
                    'soustraitant_id' => $soustraitanceFiche['Soustraitance']['soustraitant_id']
                ]);
                $success = $success && false !== $this->Soustraitance->save(null, ['atomic' => false]);
            }
        }

        foreach ($fields as $field) {
            $this->Valeur->create([
                'fiche_id' => $fiche_id,
                'valeur' => $field['Valeur']['valeur'],
                'champ_name' => $field['Valeur']['champ_name'],
            ]);
            $success = $success && false !== $this->Valeur->save(null, ['atomic' => false]);
        }

        if ($success == true && !empty($ficheToDuplicate['Fichier'])) {
            $success = $success && false !== $this->Fichier->copyFicheAnnexeFiles(
                $ficheToDuplicate['Fichier'],
                $fiche_id
            );
        }

        if ($success == true) {
            $contentHistorique = __d('historique', 'historique.creationTraitement') . ' ' . $user['User']['nom_complet_court'];

            $success = $success && false !== $this->_saveHistorique(
                $contentHistorique,
                $fiche_id
            );
        }

        if ($success == true) {
            $success = $success && false !== $this->_saveEtatFiche(
                $fiche_id,
                EtatFiche::ENCOURS_REDACTION,
                $user['User']['id'],
                $user['User']['id']
            );
        }

        return $success;
    }

    protected function updateBreadcrumbs($referers)
    {
        $refererController = Router::parse($referers);
        $updateBreadcrumbs = null;
        $cases = [
            'encours_redaction' => __d('pannel', 'pannel.titreTraitementEnCoursRedaction'),
            'attente' => __d('pannel', 'pannel.titreTraitementEnAttente'),
            'refuser' => __d('pannel', 'pannel.titreTraitementRefuser'),
            'archives' => __d('pannel', 'pannel.titreTraitementValidee'),
            'recuValidation' => __d('pannel', 'pannel.titreTraitementRecuValidation'),
            'recuConsultation' => __d('pannel', 'pannel.titreTraitementConsultation'),
            'consulte' => __d('pannel', 'pannel.titreTraitementVu'),
            'initialisation' => __d('pannel', 'pannel.titreInitialisationTraitement'),
            'all_traitements' => __d('pannel', 'pannel.titreAllTraitement'),
        ];

        if (array_key_exists($refererController['action'], $cases)) {
            $updateBreadcrumbs = [
                $cases[$refererController['action']] => [
                    'controller' => $refererController['controller'],
                    'action' => $refererController['action'],
                    'prepend' => true
                ]
            ];
        }

        if ($refererController['action'] === 'index' &&
            $refererController['controller'] === 'registres'
        ) {
            if (!empty($refererController['named'])) {
                $action = $refererController['action'] . DS . key($refererController['named']) . ':' . $refererController['named']['Registre.typeRegistre'];
            } else {
                $action = $refererController['action'];
            }

            $updateBreadcrumbs = [
                __d('registre', 'registre.titreRegistre') . $this->Session->read('Organisation.raisonsociale') => [
                    'controller' => $refererController['controller'],
                    'action' => $action,
                    'prepend' => true
                ]
            ];
        }

        $this->set(compact('updateBreadcrumbs'));
    }
}
