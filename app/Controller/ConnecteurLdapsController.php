<?php

/**
 * ConnecteurLdapsController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class ConnecteurLdapsController extends AppController {

    public $uses = [
        'ConnecteurLdap'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Droits->assertAuthorized([ListeDroit::GESTION_MAINTENANCE]);
    }

    /**
     * Permets l'ajout et la modification du connecteur LDAP
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/03/2018
     * @version V1.0.0
     */
    public function add()
    {
        $this->set('title', __d('connecteur_ldap', 'connecteur_ldap.titreLdap'));
        
        $this->set('options', $this->ConnecteurLdap->enums());
        
        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect([
                    'controller' => 'connecteurs',
                    'action' => 'index'
                ]);
            }

            $success = true;
            $data = $this->request->data;
            $info['message'] = __d('connecteur_ldap', 'connecteur_ldap.flasherrorErreurEnregistrementConnecteur');
            $info['statutMessage'] = 'flasherror';
            $this->Service->begin();

            $data['ConnecteurLdap']['organisation_id'] = $this->Session->read('Organisation.id');

            $this->ConnecteurLdap->create($data);
            $success = $success && false !== $this->ConnecteurLdap->save(null, ['atomic' => false]);

            if ($success == true) {
                if (empty($data['ConnecteurLdap']['id'])) {
                    $data['ConnecteurLdap']['id'] = $this->ConnecteurLdap->getLastInsertID();
                }

                if (isset($data['ConnecteurLdap']['certificat']) &&
                    !empty($data['ConnecteurLdap']['id']) &&
                    file_exists($data['ConnecteurLdap']['certificat']['tmp_name'])
                ) {
                    $info = $this->ConnecteurLdap->saveFileCertificat(
                        $data['ConnecteurLdap']['certificat'],
                        $data['ConnecteurLdap']['id']
                    );
                    $success = false !== $info['success'];
                }
            }

            if ($success == true) {
                $this->ConnecteurLdap->commit();
                $this->Session->setFlash(__d('connecteur_ldap', 'connecteur_ldap.flashsuccessConnecteurEnregistrer'), 'flashsuccess');
                
                $this->redirect([
                    'controller' => 'connecteurs',
                    'action' => 'index'
                ]);
            } else {
                $this->ConnecteurLdap->rollback();
                $this->Session->setFlash($info['message'], $info['statutMessage']);
            }
        } else {
            $query = [
                'conditions' => [
                    'organisation_id' => $this->Session->read('Organisation.id')
                ]
            ];
            $this->request->data = $this->ConnecteurLdap->find('first', $query);
        }
    }

    public function deleteFile($id, $urlFile) {
        $this->autoRender = false;

        $this->ConnecteurLdap->begin();

        $cheminFile =  CHEMIN_CERTIFICATS . $urlFile;

        $this->ConnecteurLdap->id = $id;
        $record = [
            'certificat_url' => null,
            'certificat_name' => null
        ];
        $success = $this->ConnecteurLdap->save($record, ['atomic' => false]) !== false
            && unlink($cheminFile) === true;

        if ($success == true) {
            $this->ConnecteurLdap->commit();
            $this->Session->setFlash(__d('norme', 'norme.flashsuccessFichierSupprimer'), 'flashsuccess');
        } else {
            $this->ConnecteurLdap->rollback();
            $this->Session->setFlash(__d('norme', 'norme.flasherrorFichierSupprimer'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
    
}
