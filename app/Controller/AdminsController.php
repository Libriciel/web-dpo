<?php

/**
 * AdminsController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

class AdminsController extends AppController {

    public $uses = [
        'Admin',
        'User'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        // Seul le rôle Superutilisateur peut accéder à ce contrôleur.
        if ($this->Droits->isSu() !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * Fonction index pour l'affichage de tous les "super-admin" de l'application.
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @modified 15/11/2019
     * @version V1.1.0
     */
    public function index() {
        $this->set('title', __d('admin', 'admin.titreSuperAdministrateur'));

        $query = [
            'contain' => [
                'User'
            ],
            'fields' => [
                'Admin.id',
                'Admin.user_id',
                'User.id',
                'User.username',
                'User.civilite',
                'User.nom',
                'User.prenom',
            ],
            'order' => 'User.nom_complet ASC',
            'limit' => 20
        ];

        $this->paginate = $query;
        $admins = $this->paginate($this->Admin);

        $this->set('admins', $admins);
    }
    
    public function add() {
        return $this->edit();
    }

    /**
     * Affiche le formulaire d'ajout d'utilisateur, ou enregistre l'utilisateur et ses droits
     * 
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function edit($id = null) {
        $title = 'add' === $this->request->params['action']
            ? __d('admin', 'admin.titreAjouterSuperAdmin')
            : __d('admin', 'admin.titreEditerSuperAdmin');
        $this->set(compact('title'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect(['action' => 'index']);
            }
            
            $success = true;
            $this->Admin->begin();
            
            // Travail préparatoire dans le cas d'une modification
            if ($this->request->params['action'] === 'edit') {
                $this->request->data['User']['id'] = $id;

                $password = (string)Hash::get($this->request->data, 'User.password');
                $passwd = (string)Hash::get($this->request->data, 'User.passwd');
                
                if ($password === '' && $password === $passwd) {
                    unset($this->request->data['User']['password'], $this->request->data['User']['passwd']);
                }
            }

            $this->User->create($this->request->data);
            $success = false !== $this->User->save(null, ['atomic' => false]) && $success;
            
            if ($success == true && $this->request->params['action'] != 'edit' ) {
                $userId = $this->User->getInsertID();
                
                $admin = [
                    'Admin' => [
                        'user_id' => $userId
                    ]
                ];
                $success = false !== $this->Admin->save($admin, ['atomic' => false]) && $success;
            }
            
            if ($success == true) {
                $this->Admin->commit();
                $this->Session->setFlash(__d('admin', 'admin.flashsuccessSuperAdminEnregistrer'), 'flashsuccess');
                    
                $this->redirect($this->Referers->get());
            } else {
                $this->Admin->rollback();
                $this->Session->setFlash(__d('admin', 'admin.flasherrorErreurSuperAdminEnregistrer'), 'flasherror');
            }
        } else if ($this->request->params['action'] === 'edit') {
            $query = [
                'conditions' => [
                    'User.id' => $id
                ],
                'fields' => [
                    'id',
                    'civilite',
                    'nom',
                    'prenom',
                    'username',
                    'email',
                    'telephonefixe',
                    'telephoneportable',
                ]
            ];
            $user = $this->User->find('first', $query);
            
            if (empty($user) === true) {
                throw new NotFoundException();
            }
            
            $this->request->data = $user;
        }
        
        $forcePassword = $this->User->getMinUserPasswordStrength($id);
        $this->set(compact('forcePassword'));
        
        $minEntropie = $this->_entropiePassword($id);
        $this->set(compact('minEntropie'));
        
        $options = array_merge($this->User->enums());
        $this->set(compact('options'));
        
        $this->view = 'edit';
    }

    private function _entropiePassword($id) {
        $forcePassword = 5;

        $thresholds = array_flip(PasswordStrengthMeterAnssi::thresholds());
        return $thresholds[$forcePassword];
    }
    
    /**
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function delete($idAdmin, $idUser) {
        // La même logique que dans la vue et que dans UsersController
        if ($idUser == 1 || $idUser == $this->Session->read('Auth.User.id')) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        if ($this->Admin->exists($idAdmin) === false) {
            throw new NotFoundException();
        }

        $success = true;
        $this->Admin->begin();

        $success = $success && false !== $this->Admin->delete($idAdmin);

        if ($success == true) {
            $success = $success && false !== $this->User->delete($idUser);
        }

        if ($success == true) {
            $this->Admin->commit();
            $this->Session->setFlash(__d('admin', 'admin.flashsuccessPrivilegeRetire'), 'flashsuccess');
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect([
            'controller' => 'admins',
            'action' => 'index'
        ]);
    }

    /**
     *
     */
    public function changeViewLogin()
    {
        $this->set('title', "Personnalisation de la page de connexion");

        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {
                // On verifie si le dossier existe. Si c'est pas le cas on le cree
                if (!file_exists(CHEMIN_CONFIGURATION_LOGIN)) {
                    mkdir(CHEMIN_CONFIGURATION_LOGIN, 0777, true);
                }

                $this->request->data['showCustom'] = filter_var( $this->request->data['showCustom'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['showInformation'] = filter_var( $this->request->data['showInformation'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['informationText'] = str_replace("'", "&#39;", $this->request->data['informationText']);
                $this->request->data['subText'] = str_replace("'", "&#39;", $this->request->data['subText']);
                $valueConfig = json_encode( $this->request->data );

                $maConfigLogin = fopen(FICHIER_CONFIGURATION_LOGIN, 'w+');
                fputs($maConfigLogin, $valueConfig);
                fclose($maConfigLogin);

                return new CakeResponse([
                    'body'=> json_encode([
                        'val' => 'La configuration de la page de connexion a été enregistrée'
                    ]),
                    'status' => 200
                ]);
            } else {
                return new CakeResponse([
                    'body'=> json_encode([
                        'val' => 'Une erreur est survenue lors de l\'enregistrement. Merci de contacter votre administrateur'
                    ]),
                    'status' => 500
                ]);
            }
        } else {
            $filename = FICHIER_CONFIGURATION_LOGIN;
            if (file_exists($filename)) {
                $fileConfigLogin = fopen($filename, 'r');
                $configLoginExistante = fread($fileConfigLogin, filesize($filename));
                fclose($fileConfigLogin);
            } else {
                $configLoginExistante = '';
            }
        }

        $this->set(compact('configLoginExistante'));
    }

}
