<?php

/**
 * ResponsablesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class ResponsablesController extends AppController {

    public $uses = [
        'Responsable',
        'Coresponsable',
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);

        if ($action === 'ajax_add') {
            $this->Droits->assertAuthorized([ListeDroit::GESTION_CORESPONSABLE_TRAITEMENT]);
        } else {
            $this->Droits->assertAuthorized([ListeDroit::GESTION_CORESPONSABLE]);
        }
    }

    /**
     * Retourne les résultats du moteur de recherche, que ce soit pour la liste complète ou pour la liste de ceux
     * présents dans l'entité.
     *
     * @return array
     */
    protected function _getSearchResults()
    {
        $paginate = [
            'contain' => [
                'Organisation' => [
                    'id',
                    'raisonsociale',
                    'order' => ['raisonsociale']
                ],
                'Fiche'=> [
                    'id'
                ],
            ],
            'conditions' => [],
            'order' => [
                'Responsable.raisonsocialestructure ASC'
            ]
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        // Applications des filtres
        if ($this->request->is('post') || false === empty($search)) {
            if ($this->request->params['action'] === 'index') {
                // Filtrer par entité associée
                if (!empty($this->request->data['Filtre']['organisation'])) {
                    $paginate['conditions'][] = $this->Responsable->getConditionOrganisation($this->request->data['Filtre']['organisation']);
                }
                // Filtrer par entité créatrice
                $createdbyorganisation = (string)Hash::get($this->request->data, 'Filtre.createdbyorganisation');
                if ($createdbyorganisation !== '') {
                    $paginate['conditions'][] = ['Responsable.createdbyorganisation' => $createdbyorganisation];
                }
            }

            $filters = [
                // Filtrer par raison sociale (du responsable)
                'Filtre.raisonsocialestructure' => 'Responsable.raisonsocialestructure',
                // Filtrer par numéro SIRET (du responsable)
                'Filtre.siretstructure' => 'Responsable.siretstructure',
                // Filtrer par code APE (du responsable)
                'Filtre.apestructure' => 'Responsable.apestructure',
            ];
            foreach ($filters as $filter => $path) {
                $value = (string)Hash::get($this->request->data, $filter);
                if ($value !== '') {
                    $paginate['conditions'][$path] = $value;
                }
            }
        }

        // Ajout de conditions suivant l'utilisateur connecté et l'action
        if ($this->request->params['action'] === 'entite') {
            $paginate['conditions'][] = $this->Responsable->getConditionOrganisation($this->Session->read('Organisation.id'));
        }

        $this->paginate = $paginate;
        return $this->paginate($this->Responsable);
    }

    /**
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function index()
    {
        $this->set('title', __d('responsable', 'responsable.titreGestionResponsableApplication'));

        if ($this->request->is('post')) {
            // Affectation
            if (!empty($this->request->data['ResponsableOrganisation']['organisation_id']) && isset($this->request->data['ResponsableOrganisation']) === true) {
                $success = true;
                $this->Responsable->ResponsableOrganisation->Organisation->begin();

                $organisations_ids = Hash::extract($this->request->data, 'ResponsableOrganisation.organisation_id');
                $responsables_ids = Hash::extract($this->request->data, 'ResponsableOrganisation.responsable_id');
                foreach ($organisations_ids as $organisation_id) {
                    $responsableEntite = $this->Responsable->ResponsableOrganisation->find('list', [
                        'conditions' => [
                            'organisation_id' => $organisation_id
                        ],
                        'fields' => [
                            'responsable_id'
                        ]
                    ]);

                    $diff = array_diff($responsableEntite, $responsables_ids);

                    if (!empty($diff)) {
                        $resultResponsable = array_merge($responsables_ids, $responsableEntite);
                    } else {
                        $resultResponsable = $responsables_ids;
                    }

                    $data = [
                        'Organisation' => [
                            'id' => $organisation_id,
                        ],
                        'Responsable' => [
                            'Responsable' => $resultResponsable
                        ]
                    ];
                    $success = $success && false !== $this->Responsable->ResponsableOrganisation->Organisation->save($data, ['atomic' => false]);
                }

                if ($success === true) {
                    $this->Responsable->ResponsableOrganisation->Organisation->commit();
                    $this->Session->setFlash(__d('responsable', 'responsable.flashsuccessSousTraitantAffecterEnregistrer'), 'flashsuccess');
                } else {
                    $this->Responsable->ResponsableOrganisation->Organisation->rollback();
                    $this->Session->setFlash(__d('responsable', 'responsable.flasherrorErreurEnregistrementSousTraitantAffecter'), 'flasherror');
                }

                unset($this->request->data['ResponsableOrganisation']);
            }
        }

        $this->set([
            'mesOrganisations' => $this->WebcilUsers->mesOrganisations('list'),
            'options' => $this->_optionsFiltre(),
            'responsables' => $this->_getSearchResults(),
        ]);
    }

    protected function _optionsFiltre($organisation_id = null)
    {
        $options = [
            'apestructure' => $this->Responsable->getStringOptionList('apestructure', $organisation_id),
            'organisations' => $this->Responsable->Organisation->find('list', ['order' => ['Organisation.raisonsociale ASC']]),
            'raisonsocialestructure' => $this->Responsable->getStringOptionList('raisonsocialestructure', $organisation_id),
            'siretstructure' => $this->Responsable->getStringOptionList('siretstructure', $organisation_id),
        ];

        return $options;
    }

    /**
     * 
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function entite()
    {
        $this->set('title', __d('responsable', 'responsable.titreGestionResponsableEntitee'));

        $this->set([
            'options' => $this->_optionsFiltre($this->Session->read('Organisation.id')),
            'responsables' => $this->_getSearchResults(),
        ]);
    }

    /**
     * Fonction qui permet l'ajout d'une nouvelle norme
     * 
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function add()
    {
        $this->edit(null);
    }
    
    /**
     * Fonction qui permet la modification d'une norme
     * 
     * @param int $id
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function edit($id)
    {
        if (in_array($this->request->params['action'], ['add', 'ajax_add'])) {
            $this->set('title', __d('responsable', 'responsable.titreAjouterResponsable'));
        } else {
            $responsable = $this->Droits->getAndCheckLinkedOrganisationsRecord('Responsable', $id, false);
            $this->set('title', __d('responsable', 'responsable.titreModifierResponsable'));
        }

        $cannotModified = false;

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            if (in_array($this->request->params['action'], ['add', 'ajax_add'])) {
                $data = $this->request->data;
                $data['Responsable']['createdbyorganisation'] = $this->Droits->isSu() ? null : $this->Session->read('Organisation.id');
            } else {
                $data = $this->request->data;

                $responsableUseInFiche = $this->Coresponsable->find('all', [
                    'conditions' => [
                        'responsable_id' => $id
                    ]
                ]);

                if (!empty($responsableUseInFiche)) {
                    unset($data['Responsable']['raisonsocialestructure']);
                    unset($data['Responsable']['siretstructure']);
                }

                foreach (['id', 'createdbyorganisation'] as $fieldName) {
                    $data['Responsable'][$fieldName] = $responsable['Responsable'][$fieldName];
                }
            }

            $success = true;
            $this->Responsable->begin();

            $this->Responsable->create($data);
            $success = $success && false !== $this->Responsable->save(null, ['atomic' => true]);

            if ($success == true) {
                $this->Responsable->commit();

                if ($this->request->params['action'] === 'ajax_add') {
                    $this->layout = null;
                    $this->autoRender = false;

                    $this->response->type('application/json');
                    $this->response->body(json_encode(['id' => $this->Responsable->getLastInsertID()]));
                    return $this->response->statusCode(201);
                } else {
                    $this->Session->setFlash(__d('responsable', 'responsable.flashsuccessSaveResponsable'), 'flashsuccess');
                    $this->redirect($this->Referers->get());
                }
            } else {
                $this->Responsable->rollback();
                $this->Session->setFlash(__d('responsable', 'responsable.flasherrorSaveResponsable'), 'flasherror');
            }
        } elseif ($this->request->params['action'] === 'add') {
            $this->request->data['Organisation']['Organisation'] = $this->Droits->isSu() ? null : $this->Session->read('Organisation.id');
        } else {
            $this->request->data = $responsable;

            $responsableUseInFiche = $this->Coresponsable->find('all', [
               'conditions' => [
                   'responsable_id' => $id
               ]
            ]);

            if (!empty($responsableUseInFiche)) {
                $cannotModified = true;
            }
        }

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations', 'cannotModified'));

        if ($this->request->params['action'] === 'ajax_add') {
            $this->view = 'ajax_add';
        } else  {
            $this->view = 'edit';
        }
    }
    
    /**
     * Permet la visualisation des informations d'un responsable
     * 
     * @param int $id | Id du responsable
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function show($id)
    {
        $this->set('title', __d('responsable', 'responsable.titreVisualiserResponsable'));

        $query = [
            'fields' => $this->Responsable->fields(),
            'contain' => [
                'Organisation' => [
                    'fields' => [
                        'id'
                    ]
                ]
            ],
            'conditions' => [
                'Responsable.id' => $id
            ]
        ];
        $record = $this->Responsable->find('first', $query);

        if (empty($record) === true) {
            throw new NotFoundException();
        }

        if ('Back' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $record['Organisation'] = ['Organisation' => Hash::extract($record, 'Organisation.{n}.id')];

        $this->request->data = $record;

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations'));
        $this->set('cannotModified', false);

        $this->view = 'edit';
    }
    
    /**
     * Permet de supprimer un responsable
     * 
     * @param int $id | Id du responsable
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function delete($id)
    {
        $this->Droits->getAndCheckLinkedOrganisationsRecord('Responsable', $id, true);

        $responsableUseInFiche = $this->Coresponsable->find('all', [
            'conditions' => [
                'responsable_id' => $id
            ]
        ]);

        if (empty($responsableUseInFiche)) {
            $this->Responsable->begin();

            if (false !== $this->Responsable->delete($id)) {
                $this->Responsable->commit();
                $this->Session->setFlash(__d('responsable', 'responsable.flashsuccessSuppressionResponsableEntite'), 'flashsuccess');
            } else {
                $this->Responsable->rollback();
                $this->Session->setFlash(__d('responsable', 'responsable.flasherrorErreurSuppressionResponsableEntite'), 'flasherror');
            }
        } else {
            $this->Session->setFlash(__d('responsable', 'responsable.flasherrorErreurCannotDeleteResponsableUseFiche'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
    
    /**
     * Fonction qui permet de supprimer un responsable liée à l'entité
     * 
     * @param int $id
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function dissocierResponsable($id)
    {
        $this->Droits->assertRecordAuthorized('Responsable', $id, ['superadmin' => true]);

        $this->Responsable->begin();

        $success = false !== $this->Responsable->ResponsableOrganisation->deleteAll([
                'ResponsableOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
                'ResponsableOrganisation.responsable_id' => $id
            ]);

        if ($success == true) {
            $this->Responsable->commit();
            $this->Session->setFlash(__d('responsable', 'responsable.flashsuccessDissocierResponsableEntite'), 'flashsuccess');
        } else {
            $this->Responsable->rollback();
            $this->Session->setFlash(__d('responsable', 'responsable.flasherrorErreurDissocierResponsableEntite'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 01/07/2020
     * @version v2.0.0
     */
    public function ajax_add()
    {
        if ($this->request->is('post')) {
            $data['Responsable'] = $this->request->data;
            $data['Organisation']['Organisation'] = [$this->Session->read('Organisation.id')];

            $this->layout  = 'ajax_add';
            $this->request->data = $data;
            $this->add();
        }
    }
}
