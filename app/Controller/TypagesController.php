<?php

/**
 * TypagesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class TypagesController extends AppController {

    public $uses = [
        'Typage'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Droits->assertAuthorized([ListeDroit::GESTION_TYPAGE]);
    }

    /**
     * Retourne les résultats du moteur de recherche, que ce soit pour la liste complète ou pour la liste de ceux
     * présents dans l'entité.
     *
     * @return array
     */
    protected function _getSearchResults() {
        $paginate = [
            'contain' => [
                'Organisation' => [
                    'id',
                    'raisonsociale',
                    'order' => ['raisonsociale']
                ]
            ],
            'conditions' => [],
            'order' => [
                'Typage.libelle ASC'
            ]
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        // Applications des filtres
        if ($this->request->is('post') && $this->request->params['action'] === 'index' || false === empty($search)) {
            // Filtrer par entité associée
            if (!empty($this->request->data['Filtre']['organisation'])) {
                $paginate['conditions'][] = $this->Typage->getConditionOrganisation($this->request->data['Filtre']['organisation']);
            }
            // Filtrer par entité créatrice
            $createdbyorganisation = (string)Hash::get($this->request->data, 'Filtre.createdbyorganisation');
            if ($createdbyorganisation !== '') {
                $paginate['conditions'][] = ['Typage.createdbyorganisation' => $createdbyorganisation];
            }
        }

        // Ajout de conditions suivant l'utilisateur connecté et l'action
        if ($this->request->params['action'] === 'entite') {
            $paginate['conditions'][] = $this->Typage->getConditionOrganisation($this->Session->read('Organisation.id'));
        }

        $this->paginate = $paginate;
        return $this->paginate($this->Typage);
    }

    protected function _optionsFiltre($organisation_id = null) {
        $options = [
            'organisations' => $this->Typage->Organisation->find('list', ['order' => ['Organisation.raisonsociale ASC']]),
        ];

        return $options;
    }

    /**
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public function index()
    {
        $this->set('title', __d('typage', 'typage.titreListeTypages'));

        if ($this->request->is('post')) {
            if (isset($this->request->data['TypageOrganisation']) && empty($this->request->data['TypageOrganisation']['organisation_id']) === false) {
                $success = true;
                $this->Typage->TypageOrganisation->begin();

                $dataOrganisations_ids = Hash::extract($this->request->data, 'TypageOrganisation.organisation_id');
                $typage_ids = Hash::extract($this->request->data, 'TypageOrganisation.typage_id');

                foreach ($typage_ids as $typage_id) {
                    $organisationTypage = $this->Typage->TypageOrganisation->find('list', [
                        'conditions' => [
                            'TypageOrganisation.typage_id' => $typage_id
                        ],
                        'fields' => [
                            'TypageOrganisation.organisation_id'
                        ]
                    ]);

                    $result = array_intersect($organisationTypage, $dataOrganisations_ids);
                    $organisations_ids = array_diff($dataOrganisations_ids, $result);

                    foreach ($organisations_ids as $organisation_id) {
                        $data = [
                            'TypageOrganisation' => [
                                'typage_id' => $typage_id,
                                'organisation_id' => $organisation_id
                            ]
                        ];

                        $this->Typage->TypageOrganisation->create($data);
                        $success = $success && false !== $this->Typage->TypageOrganisation->save(null, ['atomic' => false]);
                    }
                }

                if ($success == true) {
                    $this->Typage->TypageOrganisation->commit();
                    $this->Session->setFlash(__d('typage', 'typage.flashsuccessTypageAffecterEnregistrer'), 'flashsuccess');
                } else {
                    $this->Typage->TypageOrganisation->rollback();
                    $this->Session->setFlash(__d('typage', 'typage.flasherrorErreurEnregistrementTypageAffecter'), 'flasherror');
                }

                unset($this->request->data['TypageOrganisation']);
            }
        }

        $this->set([
            'mesOrganisations' => $this->WebcilUsers->mesOrganisations('list'),
            'options' => $this->_optionsFiltre(),
            'typages' => $this->_getSearchResults(),
        ]);
    }

    /**
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 28/04/2020
     * @version V2.0.0
     */
    public function entite() {
        $this->set('title', __d('typage', 'typage.titreGestionTypageEntitee'));

        $this->set([
            'typages' => $this->_getSearchResults(),
        ]);
    }

    /**
     * Fonction qui permet l'ajout d'une nouvelle norme
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public function add()
    {
        return $this->edit(null);
    }

    /**
     * Fonction qui permet la modification d'une norme
     *
     * @param int $id
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public function edit($id)
    {
        if ($this->request->params['action'] === 'add') {
            $this->set('title', __d('typage', 'typage.titreAddType'));
        } else {
            $typage = $this->Droits->getAndCheckLinkedOrganisationsRecord('Typage', $id, false);
            $this->set('title', __d('typage', 'typage.titreeditType'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->Typage->begin();

            if ($this->request->params['action'] === 'add') {
                $data = $this->request->data;
                $data['Typage']['createdbyorganisation'] = $this->Session->read('Organisation.id');
            } else {
                $data = $typage;
                $data['Typage']['libelle'] = Hash::get($this->request->data, 'Typage.libelle');
                unset($data['Typage']['created'], $data['Typage']['modified']);
            }

            $this->Typage->create($data);
            $success = $success && false !== $this->Typage->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Typage->commit();

                $this->Session->setFlash(__d('typage', 'typage.flashsuccesSaveTypage'), 'flashsuccess');
                $this->redirect($this->Referers->get());
            } else {
                $this->Typage->rollback();

                $this->Session->setFlash(__d('typage', 'typage.flasherrorSaveTypage'), 'flasherror');
            }
        } elseif ($this->request->params['action'] === 'edit') {
            $this->request->data = $typage;
        }

        $this->view = 'edit';
    }

    /**
     * Fonction qui permet de supprimer un type d'annexe liée à l'entité
     *
     * @param int $type_id
     * @throws ForbiddenException
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public function dissocier($type_id) {
        $this->Typage->TypageOrganisation->begin();

        $success = false !== $this->Typage->TypageOrganisation->deleteAll([
                'TypageOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
                'TypageOrganisation.typage_id' => $type_id
            ]);

        if ($success == true) {
            $this->Typage->TypageOrganisation->commit();
            $this->Session->setFlash(__d('typage', 'typage.flashsuccessDissocier'), 'flashsuccess');
        } else {
            $this->Typage->TypageOrganisation->rollback();
            $this->Session->setFlash(__d('typage', 'typage.flasherrorErreurDissocier'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Permet de supprimer un type d'annexe
     *
     * @param int $id | Id du type d'annexe
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 22/04/2020
     * @version V2.0.0
     */
    public function delete($id) {
        $this->Droits->getAndCheckLinkedOrganisationsRecord('Typage', $id, true);

        $this->Typage->begin();
        if ($this->Typage->delete($id) === true) {
            $this->Typage->commit();
            $this->Session->setFlash(__d('typage', 'typage.flashsuccessSuppressionTypage'), 'flashsuccess');
        } else {
            $this->Typage->rollback();
            $this->Session->setFlash(__d('typage', 'typage.flasherrorErreurSuppressionTypage'), 'flasherror');
        }

        $this->redirect(['action' => 'index']);
    }
}
