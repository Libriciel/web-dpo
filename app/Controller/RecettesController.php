<?php

/**
 * RecettesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.0.0
 * @package     App.Controller
 */

App::uses('AuthComponent', 'Controller/Component');

class RecettesController extends AppController {

    public $uses = [
        'Organisation'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        // Seul le rôle Superutilisateur peut accéder à ce contrôleur, uniquement en mode debug et sur une instance de recette
        $recette = preg_match('/^https{0,1}:\/\/(localhost|.*\.recette\.libriciel\.fr)[:\/]/', Router::url( $this->here, true )) === 1;
        if ($this->Droits->isSu() !== true || (int)Configure::read('debug') < 1 || $recette === false) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    protected function _getOrganisations() {
        $query = [
            'fields' => [
                'Organisation.id',
                'Organisation.raisonsociale'
            ],
            'contain' => [
                'Responsable' => [
                    'fields' => ['Responsable.nom_complet'],
                    'order' => ['Responsable.nom_complet']
                ],
                'Role' => [
                    'fields' => ['Role.libelle'],
                    'order' => ['Role.libelle']
                ],
                'Service' => [
                    'fields' => ['Service.libelle'],
                    'order' => ['Service.libelle']
                ],
                'Soustraitant' => [
                    'fields' => ['Soustraitant.raisonsocialestructure'],
                    'order' => ['Soustraitant.raisonsocialestructure']
                ],
                'Typage' => [
                    'fields' => ['Typage.libelle'],
                    'order' => ['Typage.libelle']
                ],
                'User' => [
                    'fields' => ['User.username'],
                    'order' => ['User.username']
                ],
            ],
            'order' => [
                'Organisation.raisonsociale'
            ]
        ];

        return $this->Organisation->find('all', $query);
    }

    protected function _getFormulaires() {
        $Formulaire = ClassRegistry::init('Formulaire');

        $Formulaire->belongsTo['Organisation'] = [
            'className' => 'Organisation',
            'foreignKey' => 'organisations_id',
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'counterCache' => null
        ];

        $query = [
            'fields' => [
                'Organisation.raisonsociale',
                'Formulaire.libelle',
                'Formulaire.active',
                'Formulaire.rt_externe',
            ],
            'contain' => [
                'Organisation'
            ],
            'order' => [
                'Organisation.raisonsociale',
                'Formulaire.rt_externe',
                'Formulaire.libelle',
                'Formulaire.id'
            ]
        ];
        return $Formulaire->find('all', $query);
}

    protected function _getUsers() {
        $query = [
            'fields' => array_merge(
                [
                    'User.username',
                    'User.nom_complet',
                    'User.email',
                    'User.notification',
                    'User.password',
                    '("Admin"."id" IS NOT NULL) AS "User__is_superadmin"',
                    '(CASE
                        WHEN "User"."username" = \'superadmin\' THEN \'admin\'
                        WHEN "Admin"."id" IS NOT NULL THEN \'Voitures2ColoSoupersos\'
                        ELSE \'bateauavionmotovoiture\'
                    END) AS "User__pwd"',
                ],
                $this->Organisation->User->Admin->fields()
            ),
            'contain' => [
                'Organisation' => [
                    'fields' => [
                        'Organisation.raisonsociale',
                        '("Organisation"."dpo" = "OrganisationUser"."user_id") AS "User__is_dpo"',
                    ],
                    'order' => [
                        'Organisation.raisonsociale'
                    ],
                ],

            ],
            'joins' => [
                $this->Organisation->User->join('Admin', ['type' => 'LEFT OUTER'])
            ],
            'conditions' => [],
            'order' => [
                'User.username'
            ]
        ];
        $users = $this->Organisation->User->find('all', $query);

        foreach ($users as $idx => $user) {
            foreach ((array)Hash::extract($user, 'Organisation.{n}.OrganisationUser') as $i => $ou) {
                $query = [
                    'conditions' => [
                        'OrganisationUser.id' => $ou['id']
                    ],
                    'contain' => [
                        'OrganisationUserRole' => [
                            'Role' => [
                                'fields' => 'Role.libelle',
                                'order' => 'Role.libelle',
                            ]
                        ],
                        'OrganisationUserService' => [
                            'Service' => [
                                'fields' => 'Service.libelle',
                                'order' => 'Service.libelle',
                            ]
                        ],
                    ]
                ];
                $users[$idx]['Organisation'][$i]['OrganisationUser'] = array_merge(
                    $users[$idx]['Organisation'][$i],
                    $this->Organisation->OrganisationUser->find('first', $query)
                );
            }
        }

        // Si le mot de passe n'est pas un de ceux auxquels on s'attend, on le note pour avertir dans la vue
        foreach (array_keys($users) as $idx) {
            $users[$idx]['User']['password'] = AuthComponent::password($users[$idx]['User']['pwd']) === $users[$idx]['User']['password'];
        }

        return $users;
    }

    public function index() {
        $formulaires = $this->_getFormulaires();
        $organisations = $this->_getOrganisations();
        $users = $this->_getUsers();

        $this->set(compact('formulaires', 'organisations', 'users'));
    }
}
