<?php

/**
 * ModeleExtraitRegistreController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ListeDroit', 'Model');

class ModeleExtraitRegistresController extends AppController {

//    public $uses = [
//        'ModeleExtraitRegistre',
//        'ModeleExtraitRegistreOrganisation',
//    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

//        $action = Inflector::underscore($this->request->params['action']);

        if ($this->Droits->isSu() === true || true !== $this->Droits->authorized([ListeDroit::GESTION_MODELE])) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     *
     * @created 26/12/2016
     * @version V1.0.0
     */
    public function add()
    {
        if ($this->request->is('post') === false) {
            throw new MethodNotAllowedException();
        }

        if ('Cancel' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $data = $this->request->data;
        $success = true;
        $this->ModeleExtraitRegistre->begin();

        if (empty($data['modeleExtraitRegistre']['entity_id'])) {
            $success = false;
            $info['message'] = __d('default', 'default.flasherrorEnregistrementErreur');
            $info['statutMessage'] = 'flasherror';
        }

        if ($success === true) {
            $info = $this->ModeleExtraitRegistre->saveFileModeleExtraitRegistre(
                $data['ModeleExtraitRegistre']['modeleExtraitRegistre']
            );
            $success = $success && false !== $info['success'];

            if ($success == true) {
                foreach($data['modeleExtraitRegistre']['entity_id'] as $organisation_id) {
                    $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->create([
                        'ModeleExtraitRegistreOrganisation' => [
                            'modele_extrait_registre_id' => $this->ModeleExtraitRegistre->getLastInsertID(),
                            'organisation_id' => $organisation_id
                        ]
                    ]);
                    $success = $success && false !== $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->save(null, ['atomic' => false]);
                }

                if ($success == false) {
                    $info['message'] = __d('default', 'default.flasherrorEnregistrementErreur');
                    $info['statutMessage'] = 'flasherror';
                }
            }
        }

        if ($success == true) {
            $this->ModeleExtraitRegistre->commit();
            $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleEnregistrer'), 'flashsuccess');
        } else {
            $this->ModeleExtraitRegistre->rollback();
            $this->Session->setFlash($info['message'], $info['statutMessage']);
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Fonction pour téléchargé le modele de l'extrait de registre
     * 
     * @param type $file
     * @return type
     * 
     * @access public
     * @created 28/12/2016
     * @version V1.0.0
     */
    public function download($file, $nameFile) {
        $this->response->file(CHEMIN_MODELES_EXTRAIT . $file, [
            'download' => true,
            'name' => $nameFile
        ]);
        
        return $this->response;
    }
    
    /**
     * Permet de supprimer en bdd de le model associer par qu'ontre on ne 
     * supprime dans aucun cas le fichier enregistré
     * 
     * @param type $file --> c'est le nom du model (en générale 15614325.odt)
     * qui est enregistré dans app/webroot/files/models
     */

    /**
     * Permet de supprimer en bdd de le model associer par qu'ontre on ne
     * supprime dans aucun cas le fichier enregistré
     *
     * @param $id
     *
     * @access public
     *
     * @modified 27/11/2020
     * @version V2.1.0
     */
    public function delete($id)
    {
        $success = true;
        $this->ModeleExtraitRegistre->begin();

        $modeles = $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->find('all', [
            'conditions' => [
                'modele_extrait_registre_id' => $id
            ]
        ]);

        if (empty($modeles)) {
            $success = $success && false !== $this->ModeleExtraitRegistre->deleteAll(['id' => $id], ['atomic' => false]);
        }

        if ($success == true) {
            $this->ModeleExtraitRegistre->commit();
            $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleSupprimer'), 'flashsuccess');
        } else {
            $this->ModeleExtraitRegistre->rollback();
            $this->Session->setFlash(__d('modele', 'modele.flasherrorErreurSupprimerModele'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
    
    public function index()
    {
        $this->set('title', __d('modele_extrait', 'modele_extrait.titreModeleExtraitRegistre'));

        if ($this->request->is('post')) {
            if (isset($this->request->data['ModeleExtraitRegistre']) &&
                empty($this->request->data['ModeleExtraitRegistre']['organisation_id']) === false
            ) {
                $success = true;
                $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->begin();

                $organisations_ids = Hash::extract($this->request->data, 'ModeleExtraitRegistre.organisation_id');

                foreach ($organisations_ids as $organisation_id) {
                    $data = [
                        'ModeleExtraitRegistreOrganisation' => [
                            'modele_extrait_registre_id' => $this->request->data['ModeleExtraitRegistre']['modeleextraitregistre_id'],
                            'organisation_id' => $organisation_id,
                        ]
                    ];

                    $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->create($data);
                    $success = $success && false !== $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->save(null, ['atomic' => false]);
                }

                if ($success == true) {
                    $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->commit();
                    $this->Session->setFlash(__d('modele_extrait', 'modele_extrait.flashsuccessModeleExtraitAffecterEnregistrer'), 'flashsuccess');
                } else {
                    $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->rollback();
                    $this->Session->setFlash(__d('modele_extrait', 'modele_extrait.flasherrorErreurEnregistrementModeleExtraitAffecter'), 'flasherror');
                }
            }

            unset($this->request->data['ModeleExtraitRegistre']);
        }

        $this->set([
            'mesOrganisations' => $this->_getOrganisationsNoModeleExtrait(),
//            'options' => $this->_optionsFiltre(),
            'modelesExtrait' => $this->_getSearchResults(),
        ]);
    }

    public function entite()
    {
        $this->set('title', __d('modele_extrait', 'modele_extrait.titreModeleExtraitRegistreOrganisation'));

        $this->set([
            'mesOrganisations' => $this->WebcilUsers->mesOrganisations('list'),
//            'options' => $this->_optionsFiltre(),
            'modelesExtrait' => $this->_getSearchResults(),
        ]);
    }

    protected function _getSearchResults()
    {
        $query = [
            'contain' => [
                'ModeleExtraitRegistreOrganisation' => [
                    'Organisation' => [
                        'id',
                        'raisonsociale',
                        'order' => ['raisonsociale']
                    ]
                ],
            ],
            'conditions' => [],
            'order' => [
                'ModeleExtraitRegistre.name_modele ASC'
            ]
        ];

        if ($this->request->params['action'] === 'entite') {
            $query['conditions'][] = $this->ModeleExtraitRegistre->getModeleExtraitRegistreConditionOrganisation($this->Session->read('Organisation.id'));
        }

        $this->paginate = $query;
        return $this->paginate($this->ModeleExtraitRegistre);
    }

    protected function _getOrganisationsNoModeleExtrait()
    {
        return $this->Organisation->find('list', [
            'fields' => [
                'Organisation.id',
                'Organisation.raisonsociale',
            ],
            'contain' => [
                'ModeleExtraitRegistreOrganisation'
            ],
            'conditions' => [
                'Organisation.id' => array_keys($this->WebcilUsers->mesOrganisations('list')),
                'ModeleExtraitRegistreOrganisation.id' => null
            ]
        ]);
    }

    public function dissocier()
    {
        $this->ModeleExtraitRegistre->begin();

        $success = false !== $this->ModeleExtraitRegistre->ModeleExtraitRegistreOrganisation->deleteAll([
            'ModeleExtraitRegistreOrganisation.organisation_id' => $this->Session->read('Organisation.id')
        ]);

        if ($success == true) {
            $this->ModeleExtraitRegistre->commit();
            $this->Session->setFlash(__d('modele_extrait', 'modele_extrait.flashsuccessDissocier'), 'flashsuccess');
        } else {
            $this->ModeleExtraitRegistre->rollback();
            $this->Session->setFlash(__d('modele_extrait', 'modele_extrait.flasherrorErreurDissocier'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

}
