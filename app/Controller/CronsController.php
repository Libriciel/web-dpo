<?php

/**
 * CronsController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('AppTools', 'Lib');
App::uses('ListeDroit', 'Model');
App::uses('CakeTime', 'Utility');

class CronsController extends AppController {

    public $uses = [
        'Cron'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Droits->assertAuthorized([ListeDroit::GESTION_MAINTENANCE]);
    }

    /**
     * Vue détaillée des crons (tâches planifiées)
     * 
     * @version 4.3
     * @param type $id
     * @return type
     */
    public function index() {
        $this->set('title', __d('cron', 'cron.titreListeTachesAutomatiques'));
        
        $tachesAutos = $this->Cron->find('all', [
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ],
            'order' => array('Cron.nom ASC')
        ]);
        $this->set('tachesAutos', $tachesAutos);
    }

    /**
     * Activation / Desactivation de la tâche automatique
     * 
     * @param int $id
     * @param type $state
     * 
     * @access public
     * @created 30/03/2018
     * @version V1.0.0
     */
    public function toggle($id, $state = null) {
        $this->Droits->assertRecordAuthorized('Cron', $id);
        
        $success = true;
        $this->Cron->begin();

        $this->Cron->id = $id;

        $success = $success && $this->Cron->updateAll([
            'active' => (int) !$state
                ], [
            'id' => $id
        ]) !== false;

        if ($success == true) {
            $this->Cron->commit();
            $this->Session->setFlash(__d('cron', 'cron.flashsuccessTacheAutoEnregistrer'), 'flashsuccess');

            $this->redirect($this->Referers->get());
        } else {
            $this->Cron->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }
    }
    
    /**
     * 
     * @param type $id
     * @throws ForbiddenException
     */
    public function deverrouiller($id) {
        $this->Droits->assertRecordAuthorized('Cron', $id);
        
        $success = true;
        $this->Cron->begin();

        $this->Cron->id = $id;

        $success = $success && $this->Cron->updateAll([
            'lock' => 'false'
                ], [
            'id' => $id
        ]) !== false;

        if ($success == true) {
            $this->Cron->commit();
            $this->Session->setFlash(__d('cron', 'cron.flashsuccessTacheDeverrouiller'), 'flashsuccess');

            $this->redirect($this->Referers->get());
        } else {
            $this->Cron->rollback();
            $this->Session->setFlash(__d('cron', 'cron.flasherrorTacheDeverrouiller'), 'flasherror');
        }
    }
    
    /**
     * Planification de l'execution d'une tâche planifiée
     * 
     * @version v1.0.0
     * @param type $id
     * @return type
     */
    public function planifier($id) {
        $this->Droits->assertRecordAuthorized('Cron', $id);
        
        $tacheAuto = $this->Cron->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);
        $this->set('tacheAuto', $tacheAuto); 
        
        $this->set('title', __d('cron', 'cron.titrePlanificationTachesAutomatiques') . $tacheAuto['Cron']['nom']);
        
        $options = [];
        for ($i = 1; $i <= 30; $i ++) {
            $options['jours'][$i] = $i;
        }
        
        for ($i = 1; $i <= 23; $i ++) {
            $options['heures'][$i] = $i;
        }
        
        for ($i = 5; $i <= 55; $i = $i + 5) {
            $options['minutes'][$i] = $i;
        }
        $this->set('options', $options);
        
        $execution_duration = AppTools::durationToArray($tacheAuto['Cron']['execution_duration']);
        $this->set('execution_duration', $execution_duration); 
        
        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }
            
            $duration = [
                'day' => $this->request->data['Cron']['jours_execution_duration'],
                'hour' => $this->request->data['Cron']['heures_execution_duration'],
                'minute' => $this->request->data['Cron']['minutes_execution_duration']
            ];
            
            $success = true;
            $this->Cron->begin();

            $success = $success && $this->Cron->updateAll([
                'next_execution_time' => "'".date('Y-m-d H:i:s', strtotime(str_replace('/', '-',$this->request->data['Cron']['nextexecutiontime'])))."'",
                'execution_duration' => "'".AppTools::arrayToDuration($duration)."'"
                    ], [
                'id' => $this->request->data['Cron']['id']
                    ]
            ) !== false;
            
            if ($success === true) {
                $this->Cron->commit();
                $this->Session->setFlash(__d('cron', 'cron.flashsuccessCronEnregistrer'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                $this->Cron->rollback();
                $this->Session->setFlash(__d('cron', 'cron.flasherrorErreurCronEnregistrementUser'), 'flasherror');
            }
        }
    }

    /**
     * Fonction d'exécution du cron $id
     */
    public function executer($id) {
        $this->Droits->assertRecordAuthorized('Cron', $id);

        $cron = $this->Cron->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);
        
        // excécutions
        if (empty($cron)) {
            $this->Session->setFlash(__d('cron', 'cron.flasherrorIdTacheInvalide'), 'flasherror');
        } elseif ($cron['Cron']['lock']) {
            $this->Session->setFlash(__d('cron', 'cron.flasherrorIdTacheVerrouiller'), 'flasherror');
        } else {
            $cmd = 'nohup nice -n 10 ' . APP . 'Console' . DS . 'cake Cron runId ' . $id . ' >/dev/null 2>&1  & echo $!';
            $pid = shell_exec($cmd);
            
            sleep(3);
            $this->Session->setFlash(__d('cron', 'cron.flashsuccessIdTacheEnCoursExecution'), 'flashsuccess');
        }
        
        return $this->redirect($this->Referers->get());
    }    
        
}
