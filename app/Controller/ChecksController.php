<?php

/**
 * ChecksController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');
App::uses('WebdpoChecks', 'Utility');

class ChecksController extends AppController {

    public $uses = ['User'];

    public $helpers = [
        'Checks' => [
            'className' => 'WebdpoChecks'
        ]
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        // Seul le rôle Superutilisateur peut accéder à ce contrôleur.
        if ($this->Droits->isSu() !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function index()
    {
        $this->Session->delete('Organisation');
        $this->set('title', __d('checks', 'checks.titreIndex'));

        $results = WebdpoChecks::results();
        $this->set(compact('results'));
    }   
}
