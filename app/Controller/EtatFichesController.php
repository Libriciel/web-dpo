<?php

/**
 * EtatFichesController : Controller de l'état de validation des fiches
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Controller
 */

class EtatFichesController extends AppController
{
    public $components = [
        'Jetons'
    ];

    public $uses = [
        'Commentaire',
        'EtatFiche',
        'ExtraitRegistre',
        'Fiche',
        'Historique',
        'ModeleExtraitRegistre',
        'Notification',
        'Organisation',
        'Pannel',
        'TraitementRegistre',
        'User',
        'Valeur'
    ];

    /**
     * Envoie ou renvoie d'un traitement en validation et crée les états
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 11/06/2021
     * @version V2.1.0
     */
    public function sendValidation()
    {
        if ($this->request->is(['POST', 'PUT'])) {
            $data = $this->request->data;

            // On vérifie que la ressource ne soit pas prise
            $takenJeton = $this->Jetons->takenJeton($data['EtatFiche']['ficheNum']);
            if ($takenJeton['success'] === false) {
                $this->Session->setFlash($takenJeton['message'], 'flashwarning');
                return $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->EtatFiche->begin();

            // On met EtatFiche.actif a false en fonction de l'id
            $success = $this->EtatFiche->updateAllIfFound(
                [
                    'actif' => false
                ],
                [
                    'id' => $data['EtatFiche']['etatFiche'],
                    'etat_id <>' => EtatFiche::DEMANDE_AVIS
                ]
            ) !== false;

            if ($success == true) {
                $idEncoursValid = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'fiche_id' => $data['EtatFiche']['ficheNum'],
                        'etat_id' => EtatFiche::ENCOURS_VALIDATION,
                        'user_id' => $this->Auth->user('id'),
                        'actif' => false
                    ]
                ]);

                if (!empty($idEncoursValid)) {
                    $this->EtatFiche->create([
                        'EtatFiche' => [
                            'fiche_id' => $data['EtatFiche']['ficheNum'],
                            'etat_id' => EtatFiche::VALIDER,
                            'user_id' => $this->Auth->user('id'),
                            'previous_user_id' => $this->Auth->user('id'),
                            'actif' => false
                        ]
                    ]);
                    $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);

                    $messageHistorique = __d('historique', 'historique.valideEnvoieTraitement');

                    $success = $success && $this->Commentaire->updateAll(
                        [
                            'etat_fiches_id' => $this->EtatFiche->id
                        ],
                        [
                            'etat_fiches_id' => $data['EtatFiche']['etatFiche']
                        ]
                    ) !== false;
                } else {
                    $messageHistorique = __d('historique', 'historique.envoieTraitement');
                }

                $traitementSendValidation = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'id' => $data['EtatFiche']['etatFiche'],
                        'fiche_id' => $data['EtatFiche']['ficheNum']
                    ]
                ]);
                if (empty($traitementSendValidation)) {
                    $success = false;
                }

                if ($success == true) {
                    $userLogged_id = $this->Auth->user('id');

                    if ($userLogged_id !== $traitementSendValidation['EtatFiche']['user_id']) {
                        $previous_user_id = $traitementSendValidation['EtatFiche']['user_id'];

                        $this->EtatFiche->create([
                            'EtatFiche' => [
                                'fiche_id' => $traitementSendValidation['EtatFiche']['fiche_id'],
                                'etat_id' => EtatFiche::TRAITEMENT_SEND_USER_SERVICE_DECLARANT,
                                'user_id' => $userLogged_id,
                                'previous_user_id' => $previous_user_id,
                                'previous_etat_id' => $traitementSendValidation['EtatFiche']['id'],
                                'actif' => false
                            ]
                        ]);
                        $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);
                    } else {
                        $previous_user_id = $userLogged_id;
                    }

                    if ($success === true) {
                        $this->EtatFiche->create([
                            'EtatFiche' => [
                                'fiche_id' => $traitementSendValidation['EtatFiche']['fiche_id'],
                                'etat_id' => EtatFiche::ENCOURS_VALIDATION,
                                'previous_user_id' => $previous_user_id,
                                'user_id' => $data['EtatFiche']['destinataire']
                            ]
                        ]);
                        $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);
                    }

                    if ($success == true) {
                        $this->Notification->create([
                            'Notification' => [
                                'user_id' => $data['EtatFiche']['destinataire'],
                                'content' => Notification::VALIDATION_DEMANDEE,
                                'fiche_id' => $data['EtatFiche']['ficheNum']
                            ]
                        ]);
                        $success = false !== $this->Notification->save(null, ['atomic' => false]);

                        if ($success === true) {
                            $this->Notifications->sendEmail($data['EtatFiche']['destinataire']);
                        }

                        if ($success == true) {
                            $destinataire = $this->User->find('first', [
                                'conditions' => [
                                    'id' => $data['EtatFiche']['destinataire']
                                ]
                            ]);

                            $this->Historique->create([
                                'Historique' => [
                                    'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' ' . $messageHistorique . ' ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'] . ' ' . __d('historique', 'historique.validation'),
                                    'fiche_id' => $data['EtatFiche']['ficheNum']
                                ]
                            ]);
                            $success = false !== $this->Historique->save(null, ['atomic' => false]);
                        }
                    }
                }
            }

            if ($success == true) {
                $this->EtatFiche->commit();
                $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementEnvoyerValidation'), 'flashsuccess');

                $this->requestAction([
                    'controller' => 'pannel',
                    'action' => 'supprimerLaNotif',
                    $data['EtatFiche']['ficheNum']
                ]);
            } else {
                $this->EtatFiche->rollback();
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            }
        }

        $this->redirect([
            'controller' => 'pannel',
            'action' => 'index'
        ]);
    }

    /**
     * Envoie une fiche en réorientation
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 11/06/2021
     * @version V2.1.0
     */
    public function reorientation()
    {
        $data = $this->request->data;

        $success = true;
        $error = __d('default', 'default.flasherrorEnregistrementErreur');
        $this->EtatFiche->begin();

        $traitement = $this->EtatFiche->find('first', [
            'conditions' => [
                'id' => $data['EtatFiche']['etatFiche']
            ]
        ]);

        if ($traitement['EtatFiche']['user_id'] == $data['EtatFiche']['destinataire']) {
            $success = false;
            $error = "Le traitement est déjà chez le valideur concerné";
        }

        if ($success == true) {
            $success = $success && false !== $this->EtatFiche->delete($data['EtatFiche']['etatFiche']);
            $this->EtatFiche->create([
                'EtatFiche' => [
                    'fiche_id' => $data['EtatFiche']['ficheNum'],
                    'etat_id' => 2,
                    'previous_user_id' => $this->Auth->user('id'),
                    'user_id' => $data['EtatFiche']['destinataire']
                ]
            ]);
            $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);
        }

        if ($success == true) {
            $destinataire = $this->User->find('first', [
                'conditions' => [
                    'id' => $data['EtatFiche']['destinataire']
                ]
            ]);

            $this->Historique->create([
                'Historique' => [
                    'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' réoriente la fiche vers ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'] . ' pour validation',
                    'fiche_id' => $data['EtatFiche']['ficheNum']
                ]
            ]);
            $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

            if ($success == true) {
                $success = $success && false !== $this->Notifications->del(2, $data['EtatFiche']['ficheNum']);

                if ($success == true) {
                    $this->Notification->create([
                        'Notification' => [
                            'user_id' => $data['EtatFiche']['destinataire'],
                            'content' => Notification::VALIDATION_DEMANDEE,
                            'fiche_id' => $data['EtatFiche']['ficheNum']
                        ]
                    ]);
                    $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);
                }
            }
        }

        if ($success == true) {
            $this->EtatFiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementRedirige'), 'flashsuccess');

            $this->requestAction([
                'controller' => 'pannel',
                'action' => 'supprimerLaNotif',
                $data['EtatFiche']['ficheNum']
            ]);
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash($error, 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Gère le refus de validation et le commentaire associé
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function refuse()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            if (empty($this->request->data('EtatFiche.commentaireRepondre'))) {
                $this->Session->setFlash(__d('default', 'default.flasherrorChampVide'), 'flasherror');
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->EtatFiche->begin();

            $idEncoursValid = $this->EtatFiche->find('first', [
                'conditions' => [
                    'EtatFiche.fiche_id' => $this->request->data['EtatFiche']['ficheNum'],
                    'EtatFiche.etat_id' => EtatFiche::ENCOURS_VALIDATION,
                    'EtatFiche.actif' => true
                ],
                'fields' => 'id'
            ]);
            $id = $idEncoursValid['EtatFiche']['id'];
            $this->EtatFiche->id = $id;

            $infoTraitement = $this->Fiche->find('first', [
                'conditions' => [
                    'Fiche.id' => $this->request->data['EtatFiche']['ficheNum']
                ],
                'fields' => ['id'],
                'contain' => [
                    'User' => array('id')
                ]
            ]);
            $idFiche = $infoTraitement['Fiche']['id'];
            $idDestinataire = $infoTraitement['User']['id'];

            $success = $success && $this->EtatFiche->updateAll([
                    'actif' => false
                ], [
                        'id' => $id
                    ]
                ) !== false;

            if ($success == true) {
                $this->EtatFiche->create([
                    'EtatFiche' => [
                        'etat_id' => EtatFiche::REFUSER,
                        'previous_user_id' => $this->Auth->user('id'),
                        'user_id' => $idDestinataire,
                        'fiche_id' => $idFiche
                    ]
                ]);
                $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);

                if ($success == true) {
                    $this->Commentaire->create([
                        'Commentaire' => [
                            'etat_fiches_id' => $this->EtatFiche->id,
                            'content' => $this->request->data['EtatFiche']['commentaireRepondre'],
                            'user_id' => $this->Auth->user('id'),
                            'destinataire_id' => $idDestinataire
                        ]
                    ]);
                    $success = $success && false !== $this->Commentaire->save(null, ['atomic' => false]);

                    if ($success == true) {
                        $this->Historique->create([
                            'Historique' => [
                                'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' refuse le traitement',
                                'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                            ]
                        ]);
                        $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

                        if ($success == true) {
                            $this->Notification->create([
                                'Notification' => [
                                    'user_id' => $idDestinataire,
                                    'content' => EtatFiche::REFUSER,
                                    'fiche_id' => $idFiche
                                ]
                            ]);
                            $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);

                            if ($success === true) {
                                $this->Notifications->sendEmail($idDestinataire);
                            }
                        }
                    }
                }
            }

            if ($success == true) {
                $this->EtatFiche->commit();
                $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementRefuse'), 'flashsuccess');

                $this->requestAction([
                    'controller' => 'pannel',
                    'action' => 'supprimerLaNotif',
                    $idFiche
                ]);
            } else {
                $this->EtatFiche->rollback();
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            }

            $this->redirect($this->Referers->get());
        }
    }

    /**
     * Gère l'envoie en consultation
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 11/06/2021
     * @version V2.1.0
     */
    public function askAvis()
    {
        if ($this->request->is(['POST', 'PUT'])) {
            $data = $this->request->data;

            $takenJeton = $this->Jetons->takenJeton($data['EtatFiche']['ficheNum']);
            if ($takenJeton['success'] === false) {
                $this->Session->setFlash($takenJeton['message'], 'flashwarning');
                return $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->EtatFiche->begin();

            $count = $this->EtatFiche->find('count', [
                'conditions' => [
                    'fiche_id' => $data['EtatFiche']['ficheNum'],
                    'etat_id' => EtatFiche::DEMANDE_AVIS,
                    'user_id' => $data['EtatFiche']['destinataire'],
//                    'previous_user_id' => $this->Auth->user('id'),
                    'actif' => true
                ]
            ]);

            if ($count > 0) {
                $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashwarningTraitementDejaAttenteUser'), 'flashwarning');
                $this->redirect([
                    'controller' => 'pannel',
                    'action' => 'index'
                ]);
            } else {
                /* Si le traitement est en attente de validation et qu'on l'envoie
                 * pour consultation on se change pas l'état "actif" du traitement
                 * pour qu'il reste en attende de validation
                 */
                $traitementEnCoursValidation = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'fiche_id' => $data['EtatFiche']['ficheNum'],
                        'etat_id' => EtatFiche::ENCOURS_VALIDATION,
                        'user_id' => $this->Auth->user('id'),
                        'actif' => true
                    ]
                ]);

                $reorientationDemandeAvis = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'id' => $data['EtatFiche']['etatFiche'],
                        'fiche_id' => $data['EtatFiche']['ficheNum'],
                        'etat_id' => EtatFiche::DEMANDE_AVIS,
                        'actif' => true
                    ]
                ]);

                if (empty($traitementEnCoursValidation) &&
                    empty($reorientationDemandeAvis)
                ) {
                    $success = $success && $this->EtatFiche->updateAll(
                        [
                            'actif' => false
                        ],
                        [
                            'id' => $data['EtatFiche']['etatFiche']
                        ]
                    ) !== false;
                }

                if ($success == true) {
                    if (empty($reorientationDemandeAvis)) {
                        $traitementSendConsultation = $this->EtatFiche->find('first', [
                            'conditions' => [
                                'id' => $data['EtatFiche']['etatFiche'],
                                'fiche_id' => $data['EtatFiche']['ficheNum']
                            ]
                        ]);
                        if (empty($traitementSendConsultation)) {
                            $success = false;
                        }

                        if ($success == true) {
                            $userLogged_id = $this->Auth->user('id');

                            if ($userLogged_id !== $traitementSendConsultation['EtatFiche']['user_id']) {
                                $previous_user_id = $traitementSendConsultation['EtatFiche']['user_id'];
                            } else {
                                $previous_user_id = $userLogged_id;
                            }
                        }

                        if ($success == true) {
                            $this->EtatFiche->create([
                                'EtatFiche' => [
                                    'fiche_id' => $traitementSendConsultation['EtatFiche']['fiche_id'],
                                    'etat_id' => EtatFiche::DEMANDE_AVIS,
                                    'previous_user_id' => $previous_user_id,
                                    'user_id' => $data['EtatFiche']['destinataire'],
                                    'previous_etat_id' => $traitementSendConsultation['EtatFiche']['id']
                                ]
                            ]);
                            $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);
                        }
                    } else {
                        $success = false !== $this->Notification->deleteAll([
                            'Notification.user_id' => $reorientationDemandeAvis['EtatFiche']['user_id'],
                            'Notification.content' => Notification::DEMANDE_AVIS,
                            'Notification.fiche_id' => $reorientationDemandeAvis['EtatFiche']['fiche_id'],
                            'Notification.vu' => false
                        ]);

                        if ($success === true) {
                            $success = $this->EtatFiche->updateAll(
                                [
                                    'user_id' => $data['EtatFiche']['destinataire']
                                ],
                                [
                                    'id' => $data['EtatFiche']['etatFiche']
                                ]
                            ) !== false;
                        }
                    }

                    if ($success == true) {
                        $destinataire = $this->User->find('first', [
                            'conditions' => [
                                'id' => $data['EtatFiche']['destinataire']
                            ]
                        ]);

                        if (!empty($reorientationDemandeAvis)) {
                            $precedentDestinataire = $this->User->find('first', [
                                'conditions' => [
                                    'id' => $reorientationDemandeAvis['EtatFiche']['user_id']
                                ]
                            ]);

                            $this->Historique->create([
                                'Historique' => [
                                    'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' ' . __d('historique', 'historique.annulerDemandeAvis') . ' ' . $precedentDestinataire['User']['prenom'] . ' ' . $precedentDestinataire['User']['nom'],
                                    'fiche_id' => $data['EtatFiche']['ficheNum']
                                ]
                            ]);
                            $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);
                        }

                        $this->Historique->create([
                            'Historique' => [
                                'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' ' . __d('historique', 'historique.demandeAvis') . ' ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'],
                                'fiche_id' => $data['EtatFiche']['ficheNum']
                            ]
                        ]);
                        $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

                        if ($success == true) {
                            $this->Notification->create([
                                'Notification' => [
                                    'user_id' => $data['EtatFiche']['destinataire'],
                                    'content' => Notification::DEMANDE_AVIS,
                                    'fiche_id' => $data['EtatFiche']['ficheNum']
                                ]
                            ]);
                            $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);

                            if ($success === true) {
                                $this->Notifications->sendEmail($data['EtatFiche']['destinataire']);
                            }
                        }
                    }
                }

                if ($success == true) {
                    $this->EtatFiche->commit();
                    $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementEnvoyerAvis'), 'flashsuccess');

                    $this->requestAction([
                        'controller' => 'pannel',
                        'action' => 'supprimerLaNotif',
                        $data['EtatFiche']['ficheNum']
                    ]);
                } else {
                    $this->EtatFiche->rollback();
                    $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
                }
            }
        }

        $this->redirect([
            'controller' => 'pannel',
            'action' => 'index'
        ]);
    }

    public function answerAvis()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            if (empty($this->request->data('EtatFiche.commentaireRepondre'))) {
                $this->Session->setFlash(__d('default', 'default.flasherrorChampVide'), 'flasherror');
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->EtatFiche->begin();

            $traitementEnReponse = $this->EtatFiche->find('first', [
                'conditions' => [
                    'id' => $this->request->data['EtatFiche']['etatFiche']
                ]
            ]);

            $previousTraitement = $this->EtatFiche->find('first', [
                'conditions' => [
                    'id' => $traitementEnReponse['EtatFiche']['previous_etat_id']
                ]
            ]);

            $traitements = $this->EtatFiche->find('all', [
                'conditions' => [
                    'id <>' => [
                        $traitementEnReponse['EtatFiche']['id'],
                        $traitementEnReponse['EtatFiche']['previous_etat_id']
                    ],
                    'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                ]
            ]);

            switch ($previousTraitement['EtatFiche']['etat_id']) {
                case EtatFiche::ENCOURS_REDACTION:
                    $etat_fiches_id = $traitementEnReponse['EtatFiche']['previous_etat_id'];
                    break;

                case EtatFiche::ENCOURS_VALIDATION:
                    if ($previousTraitement['EtatFiche']['actif'] == true) {
                        $etat_fiches_id = $traitementEnReponse['EtatFiche']['previous_etat_id'];
                        $actif = false;
                    }
                    break;

                case EtatFiche::REPLACER_REDACTION:
                case EtatFiche::TRAITEMENT_INITIALISE_REDIGER:
                    $etat_fiches_id = $traitementEnReponse['EtatFiche']['previous_etat_id'];
                    $actif = true;
                    break;

                case EtatFiche::REPONSE_AVIS:
                    $lastEtatTraitement = $this->EtatFiche->find('first', [
                        'conditions' => [
                            'etat_id' => [
                                EtatFiche::ENCOURS_REDACTION,
                                EtatFiche::REPLACER_REDACTION,
                                EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                            ],
                            'fiche_id' => $this->request->data['EtatFiche']['ficheNum'],
                            'actif' => false
                        ]
                    ]);

                    $etat_fiches_id = $lastEtatTraitement['EtatFiche']['id'];
                    $actif = true;
                    break;

                default:
                    break;
            }

            if (empty($traitements)) {
                $actif = true;
            } else {
                foreach ($traitements as $traitement) {
                    if ($traitement['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_VALIDATION && $traitement['EtatFiche']['actif'] == true) {
                        $actif = false;
                    }

                    if ($traitement['EtatFiche']['etat_id'] == EtatFiche::VALIDER && $traitement['EtatFiche']['actif'] == false) {
                        $etat_fiches_id = $traitement['EtatFiche']['id'];
                        $actif = false;
                    }

                    if ($traitement['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_VALIDATION && $traitement['EtatFiche']['actif'] == true) {
                        foreach ($traitements as $fiche) {
                            if ($fiche['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_REDACTION && $fiche['EtatFiche']['actif'] == false) {
                                $etat_fiches_id = $fiche['EtatFiche']['id'];
                                $actif = false;
                            }

                            if ($fiche['EtatFiche']['etat_id'] == EtatFiche::VALIDER && $fiche['EtatFiche']['actif'] == false) {
                                $etat_fiches_id = $fiche['EtatFiche']['id'];
                                $actif = false;
                            }
                        }
                    }

                    //@todo
                    foreach ($traitements as $fiche) {
                        if ($previousTraitement['EtatFiche']['etat_id'] == EtatFiche::REPONSE_AVIS &&
                            $previousTraitement['EtatFiche']['actif'] == false &&
                            $fiche['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_REDACTION
                        ) {
                            $etat_fiches_id = $fiche['EtatFiche']['id'];
                            $actif = true;
                        }

                        if ($previousTraitement['EtatFiche']['etat_id'] == EtatFiche::REPONSE_AVIS &&
                            $previousTraitement['EtatFiche']['actif'] == false &&
                            $fiche['EtatFiche']['etat_id'] == EtatFiche::REPLACER_REDACTION
                        ) {
                            $etat_fiches_id = $fiche['EtatFiche']['id'];
                            $actif = true;
                        }
                    }
                }
            }

            $success = $success && $this->EtatFiche->updateAll(
                [
                    'actif' => false
                ],
                [
                    'id' => $this->request->data['EtatFiche']['etatFiche']
                ]
            ) !== false;

            $this->EtatFiche->create([
                'EtatFiche' => [
                    'fiche_id' => $this->request->data['EtatFiche']['ficheNum'],
                    'etat_id' => EtatFiche::REPONSE_AVIS,
                    'user_id' => $this->request->data['EtatFiche']['previousUserId'],
                    'previous_user_id' => $this->Auth->user('id'),
                    'actif' => $actif
                ]
            ]);
            $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Commentaire->create([
                    'Commentaire' => [
                        'etat_fiches_id' => $etat_fiches_id,
                        'content' => $this->request->data['EtatFiche']['commentaireRepondre'],
                        'user_id' => $this->Auth->user('id'),
                        'destinataire_id' => $this->request->data['EtatFiche']['previousUserId']
                    ]
                ]);
                $success = $success && false !== $this->Commentaire->save(null, ['atomic' => false]);

                if ($success == true) {
                    $destinataire = $this->User->find('first', [
                        'conditions' => [
                            'id' => $this->request->data['EtatFiche']['previousUserId']
                        ]
                    ]);

                    $this->Historique->create([
                        'Historique' => [
                            'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' ' . __d('historique', 'historique.repondDemandeAvis') . ' ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'],
                            'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                        ]
                    ]);
                    $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

                    if ($success == true) {
                        $this->Notification->create([
                            'Notification' => [
                                'user_id' => $this->request->data['EtatFiche']['previousUserId'],
                                'content' => Notification::COMMENTAIRE_AJOUTE,
                                'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                            ]
                        ]);
                        $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);

                        if ($success === true) {
                            $this->Notifications->sendEmail($this->request->data['EtatFiche']['previousUserId']);
                        }
                    }
                }
            }

            if ($success == true) {
                $this->EtatFiche->commit();
                $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessCommentaireAjouter'), 'flashsuccess');

                $this->requestAction([
                    'controller' => 'pannel',
                    'action' => 'supprimerLaNotif',
                    $this->request->data['EtatFiche']['ficheNum']
                ]);
            } else {
                $this->EtatFiche->rollback();
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            }

            $this->redirect($this->Referers->get());
        }
    }

    /**
     * Fonction permettant de répondre à la demande de consultation
     */
    public function repondreCommentaire()
    {
        if ($this->request->is('POST')) {
            $success = true;
            $this->Commentaire->begin();

            $this->Commentaire->create([
                'Commentaire' => [
                    'etat_fiches_id' => $this->request->data['EtatFiche']['etat_fiche_id'],
                    'content' => $this->request->data['EtatFiche']['commentaire'],
                    'user_id' => $this->Auth->user('id'),
                    'destinataire_id' => $this->Auth->user('id')
                ]
            ]);
            $success = $success && false !== $this->Commentaire->save(null, ['atomic' => false]);

            if ($success == true) {

                $this->Historique->create([
                    'Historique' => [
                        'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' répond à l\'avis',
                        'fiche_id' => $this->request->data['EtatFiche']['fiche_id']
                    ]
                ]);
                $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

                if ($success == true) {
                    foreach (json_decode($this->request->data['EtatFiche']['idUserCommentaire']) as $idUserCommentaire) {
                        $this->Notification->create([
                            'Notification' => [
                                'user_id' => $idUserCommentaire,
                                'content' => Notification::COMMENTAIRE_AJOUTE,
                                'fiche_id' => $this->request->data['EtatFiche']['fiche_id']
                            ]
                        ]);
                        $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);
                    }
                }
            }

            if ($success == true) {
                $this->EtatFiche->commit();
                $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessCommentaireEnregistre'), 'flashsuccess');
            } else {
                $this->EtatFiche->rollback();
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            }

            $this->redirect($this->referer());
        }
    }

    /**
     * Gère la remise en rédaction d'une fiche refusée
     *
     * @param int $id
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function relaunch($id)
    {
        if (!$id) {
            $this->Session->setFlash(__d('default', 'default.flasherrorTraitementInexistant'), 'flasherror');
            $this->redirect([
                'controller' => 'pannel',
                'action' => 'index'
            ]);
        }

        $success = true;
        $this->EtatFiche->begin();

        $this->requestAction([
            'controller' => 'pannel',
            'action' => 'supprimerLaNotif',
            $id
        ]);

        $success = $success && $this->EtatFiche->updateAll([
                'actif' => false
            ], [
                    'fiche_id' => $id
                ]
            ) !== false;

        if ($success == true) {
            $this->EtatFiche->create([
                'EtatFiche' => [
                    'fiche_id' => $id,
                    'etat_id' => 8,
                    'previous_user_id' => $this->Auth->user('id'),
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
            $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Historique->create([
                    'Historique' => [
                        'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' replace le traitement en rédaction',
                        'fiche_id' => $id
                    ]
                ]);
                $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);
            }
        }

        if ($success == true) {
            $this->EtatFiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementReplacerRedaction'), 'flashsuccess');
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect([
            'controller' => 'pannel',
            'action' => 'index'
        ]);
    }

    /**
     * Gère l'envoie en validation au DPO
     *
     * @param int $id
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 11/06/2021
     * @version V2.1.0
     */
    public function dpoValid($id)
    {
        $takenJeton = $this->Jetons->takenJeton($id);
        if ($takenJeton['success'] === false) {
            $this->Session->setFlash($takenJeton['message'], 'flashwarning');
            return $this->redirect($this->Referers->get());
        }

        $success = true;
        $this->EtatFiche->begin();

        $traitement = $this->EtatFiche->find('first', [
            'conditions' => [
                'EtatFiche.fiche_id' => $id,
                'EtatFiche.actif' => true,
                'EtatFiche.etat_id' => [
                    EtatFiche::ENCOURS_REDACTION,
                    EtatFiche::DEMANDE_AVIS,
                    EtatFiche::ENCOURS_VALIDATION,
                    EtatFiche::REPLACER_REDACTION
                ]
            ]
        ]);

        if (empty($traitement)) {
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
            $this->redirect([
                'controller' => 'pannel',
                'action' => 'index'
            ]);
        }

        $success = $this->EtatFiche->updateAll(
            [
                'actif' => false
            ],
            [
                'id' => $traitement['EtatFiche']['id']
            ]
        ) !== false;

        if ($success == true) {
            $dpo = $this->Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $this->Session->read('Organisation.id')
                ],
                'fields' => 'dpo'
            ]);
            $idDpo = $dpo['Organisation']['dpo'];

            if ($idDpo != null) {
                $userLogged_id = $this->Auth->user('id');

                if ($traitement['EtatFiche']['etat_id'] !== EtatFiche::REPLACER_REDACTION &&
                    $traitement['EtatFiche']['etat_id'] !== EtatFiche::ENCOURS_REDACTION &&
                    $traitement['EtatFiche']['user_id'] === $userLogged_id
                ) {
                    $this->EtatFiche->create([
                        'EtatFiche' => [
                            'fiche_id' => $id,
                            'etat_id' => EtatFiche::VALIDER,
                            'user_id' => $userLogged_id,
                            'previous_user_id' => $userLogged_id,
                            'actif' => false
                        ]
                    ]);
                    $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);

                    if ($success == true) {
                        $success = $this->Commentaire->updateAll(
                            [
                                'etat_fiches_id' => $this->EtatFiche->id
                            ],
                            [
                                'etat_fiches_id' => $traitement['EtatFiche']['id']
                            ]
                        ) !== false;
                    }
                }

                if ($userLogged_id !== $traitement['EtatFiche']['user_id']) {
                    $previous_user_id = $traitement['EtatFiche']['user_id'];

                    $this->EtatFiche->create([
                        'EtatFiche' => [
                            'fiche_id' => $traitement['EtatFiche']['fiche_id'],
                            'etat_id' => EtatFiche::TRAITEMENT_SEND_USER_SERVICE_DECLARANT,
                            'user_id' => $userLogged_id,
                            'previous_user_id' => $previous_user_id,
                            'previous_etat_id' => $traitement['EtatFiche']['id'],
                            'actif' => false
                        ]
                    ]);
                    $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);
                } else {
                    $previous_user_id = $userLogged_id;
                }

                if ($success == true) {
                    $this->EtatFiche->create([
                        'EtatFiche' => [
                            'fiche_id' => $id,
                            'etat_id' => EtatFiche::ENCOURS_VALIDATION,
                            'previous_user_id' => $previous_user_id,
                            'user_id' => $idDpo
                        ]
                    ]);
                    $success = false !== $this->EtatFiche->save(null, ['atomic' => false]);
                }

                if ($success == true) {
                    $this->Historique->create([
                        'Historique' => [
                            'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' ' . __d('historique', 'historique.valideEnvoieTraitementDPO'),
                            'fiche_id' => $id
                        ]
                    ]);
                    $success = false !== $this->Historique->save(null, ['atomic' => false]);

                    if ($success == true) {
                        $this->Notification->create([
                            'Notification' => [
                                'user_id' => $dpo['Organisation']['dpo'],
                                'content' => Notification::VALIDATION_DEMANDEE,
                                'fiche_id' => $id
                            ]
                        ]);
                        $success = false !== $this->Notification->save(null, ['atomic' => false]);

                        if ($success === true) {
                            $this->Notifications->sendEmail($dpo['Organisation']['dpo']);
                        }
                    }
                }
            }
        }

        if ($success == true) {
            $this->EtatFiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementEnvoyerDPO'), 'flashsuccess');

            $this->requestAction([
                'controller' => 'pannel',
                'action' => 'supprimerLaNotif',
                $id
            ]);
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect([
            'controller' => 'pannel',
            'action' => 'index'
        ]);
    }

    /**
     * Insère dans le registre le traiment
     * Gère la validation du DPO
     *
     * @param int $id
     * @param string|null $numero
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 24/09/2020
     * @modified 08/04/2021
     * @modified 11/06/2021
     * @version V2.1.0
     */
    public function insertRegistre($fiche_id, $numero)
    {
        if (empty($fiche_id)) {
            throw new NotFoundException();
        }

        if (true !== $this->Droits->authorized($this->Droits->isDpo())) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $takenJeton = $this->Jetons->takenJeton($fiche_id);
        if ($takenJeton['success'] === false) {
            $this->Session->setFlash($takenJeton['message'], 'flashwarning');
            return $this->redirect($this->Referers->get());
        }


        $organisation_id = $this->Session->read('Organisation.id');
        $messageError = __d('default', 'default.flasherrorEnregistrementErreur');

        $this->EtatFiche->begin();

        $dataReturn = $this->saveNumeroFicheRegistre($organisation_id, $fiche_id, $numero);

        if ($dataReturn['success'] === true) {
            $success = true;

            $success = $success && $this->insertValueEntiteInFiche($organisation_id, $fiche_id);

            if ($success === true) {
                $idEncoursValid = $this->EtatFiche->find('first', [
                    'conditions' => [
                        'EtatFiche.fiche_id' => $fiche_id,
                        'OR' => [
                            [
                                'EtatFiche.etat_id' => EtatFiche::ENCOURS_REDACTION
                            ],
                            [
                                'EtatFiche.etat_id' => EtatFiche::ENCOURS_VALIDATION
                            ],
                            [
                                'EtatFiche.etat_id' => EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                            ]
                        ],
                        'EtatFiche.actif' => true
                    ],
                    'fields' => ['id'],
                    'contain' => [
                        'Fiche' => [
                            'user_id'
                        ]
                    ]
                ]);

                $success = $success && $this->EtatFiche->updateAll(
                    [
                        'actif' => false
                    ],
                    [
                        'id' => $idEncoursValid['EtatFiche']['id']
                    ]
                ) !== false;

                if ($success === true) {
                    if (!empty($idEncoursValid)) {
                        $this->EtatFiche->create([
                            'EtatFiche' => [
                                'fiche_id' => $fiche_id,
                                'etat_id' => EtatFiche::VALIDER_DPO,
                                'user_id' => $this->Auth->user('id'),
                                'previous_user_id' => $this->Auth->user('id'),
                                'actif' => true
                            ]
                        ]);
                        $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);

                        if ($success === true) {
                            $this->Notification->create([
                                'Notification' => [
                                    'user_id' => $idEncoursValid['Fiche']['user_id'],
                                    'content' => Notification::TRAITEMENT_VALIDE,
                                    'fiche_id' => $fiche_id
                                ]
                            ]);
                            $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);

                            if ($success === true) {
                                $this->Notifications->sendEmail($idEncoursValid['Fiche']['user_id']);
                            }

                            if ($success === true) {
                                $this->Historique->create([
                                    'Historique' => [
                                        'content' => $this->Auth->user('prenom') . ' ' . $this->Auth->user('nom') . ' valide le traitement et l\'insère au registre',
                                        'fiche_id' => $fiche_id
                                    ]
                                ]);
                                $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);
                            }
                        }
                    }
                }
            }
        } else {
            $messageError = $dataReturn['message'];
        }

        if ($success === true) {
            $this->EtatFiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementEngregistreRegistre'), 'flashsuccess');

            $this->requestAction([
                'controller' => 'pannel',
                'action' => 'supprimerLaNotif',
                $idEncoursValid['Fiche']['id']
            ]);

            $ficheRT = $this->Fiche->find('first', [
                'conditions' => [
                    'id' => $fiche_id
                ],
                'fields' => [
                    'rt_externe'
                ]
            ]);

            $action = 'registreActivite';
            if ($ficheRT['Fiche']['rt_externe'] === true) {
                $action = 'registreSoustraitance';
            }

            $this->redirect([
                'controller' => 'registres',
                'action' => $action
            ]);
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash($messageError, 'flasherror');

            $this->redirect($this->Referers->get());
        }
    }

    /**
     * @param int $organisation_id : ID de l'organisation en session
     * @param int $fiche_id : ID de la fiche
     * @param string|null $dataNumero : numero du traitement au registre défini par l'utilisateur
     * @return array
     *
     * @access private
     *
     * @created 24/09/2020
     * @version V2.1.0
     */
    private function saveNumeroFicheRegistre($organisation_id, $fiche_id, $dataNumero)
    {
        if ($dataNumero === 'null') {
            $dataNumero = null;
        }

        $success = true;
        $dataReturn = [];

        $organisation = $this->Organisation->find('first', [
            'conditions' => [
                'Organisation.id' => $organisation_id
            ],
            'fields' => [
                'Organisation.prefixenumero',
                'Organisation.numeroregistre'
            ]
        ]);

        if (!isset($dataNumero)) {
            $numeroRegistre = $organisation['Organisation']['numeroregistre'] + 1;
            $numeroGenerate = $organisation['Organisation']['prefixenumero'] . $numeroRegistre;

            while ($this->Fiche->find('count', [
                    'conditions' => [
                        'organisation_id' => $organisation_id,
                        'numero' => $numeroGenerate
                    ]
                ]) !== 0
            ) {
                $numeroRegistre++;
                $numeroGenerate = $organisation['Organisation']['prefixenumero'] . $numeroRegistre;
            }

            $this->Organisation->id = $organisation_id;
            $dataOrganisation = [
                'id' => $organisation_id,
                'numeroregistre' => $numeroRegistre
            ];
            $success = $success && $this->Organisation->save($dataOrganisation, ['atomic' => false]) !== false;

            if ($success === false) {
                $dataReturn = [
                    'success' => false,
                    'message' => __d('default', 'default.flasherrorEnregistrementErreur')
                ];
                return $dataReturn;
            }
        } else {
            $nbStringPrefix = strlen($organisation['Organisation']['prefixenumero']);
            $extractDataNumero = substr($dataNumero,0, $nbStringPrefix);

            if ($extractDataNumero === $organisation['Organisation']['prefixenumero']) {
                $dataReturn = [
                    'success' => false,
                    'message' => "Le numéro d'enregistrement que vous avez renseigné, ne peut pas commencer comme le préfixe du registre."
                ];
                return $dataReturn;
            } else {
                $numeroGenerate = $dataNumero;
            }
        }

        $dataFiche = [
            'id' => $fiche_id,
            'numero' => $numeroGenerate
        ];
        $success = $success && $this->Fiche->save($dataFiche, ['atomic' => false]) !== false;

        if ($success === true) {
            $dataReturn = [
                'success' => true,
                'message' => null
            ];
        } else {
            $dataReturn = [
                'success' => false,
                'message' => sprintf("Le numéro d'enregistrement %s est déjà présent au registre. Merci de modifier le numéro d'enregistrement.", $numeroGenerate)
            ];
        }

        return $dataReturn;
    }

    protected function insertValueEntiteInFiche($organisation_id, $fiche_id)
    {
        $success = true;

        $entite = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $organisation_id
            ],
            'fields' => [
                'raisonsociale',
                'telephone',
                'fax',
                'adresse',
                'email',
                'sigle',
                'siret',
                'ape',
                'civiliteresponsable',
                'nomresponsable',
                'prenomresponsable',
                'emailresponsable',
                'telephoneresponsable',
                'fonctionresponsable',
                'dpo',
                'numerodpo',
                'emaildpo',
            ]
        ]);

        $userDPO = $this->User->find('first', [
            'conditions' => [
                'id' => $entite['Organisation']['dpo']
            ],
            'fields' => [
                'nom_complet',
                'telephonefixe',
                'telephoneportable'
            ]
        ]);

        unset($entite['Organisation']['dpo']);

        $ficheIsRT = $this->Fiche->find('first', [
            'conditions' => [
                'Fiche.id' => $fiche_id
            ],
            'fields' => [
                'rt_externe'
            ]
        ]);

        $prefixChampName = 'rt_organisation_';
        if ($ficheIsRT['Fiche']['rt_externe'] === true) {
            $prefixChampName = 'st_organisation_';
        }

        $entite = Hash::extract($entite, 'Organisation');
        $keyEntite = array_keys($entite);

        foreach ($keyEntite as $val) {
            if ($success === true) {
                if (!empty($entite[$val])) {
                    $this->Valeur->create([
                        'fiche_id' => $fiche_id,
                        'valeur' => $entite[$val],
                        'champ_name'=> $prefixChampName.$val
                    ]);
                    $success = $success && false !== $this->Valeur->save(null, ['atomic' => false]);
                }
            }
        }

        if ($success === true) {
            $userDPO = Hash::extract($userDPO, 'User');
            $keyUserDPO = array_keys($userDPO);

            foreach ($keyUserDPO as $val) {
                if ($success === true) {
                    if (!empty($userDPO[$val])) {
                        if ($val === 'nom_complet') {
                            $userDPO['dpo'] = $userDPO[$val];
                            unset($userDPO[$val]);
                            $val = 'dpo';
                            $champ_name = 'nom_complet_dpo';
                        }

                        if ($val === 'telephoneportable') {
                            $userDPO['portableDpo'] = $userDPO[$val];
                            unset($userDPO[$val]);
                            $val = 'portableDpo';
                            $champ_name = 'telephoneportable_dpo';
                        }

                        if ($val === 'telephonefixe') {
                            $userDPO['fixDpo'] = $userDPO[$val];
                            unset($userDPO[$val]);
                            $val = 'fixDpo';
                            $champ_name = 'telephonefixe_dpo';
                        }

                        $this->Valeur->create([
                            'fiche_id' => $fiche_id,
                            'valeur' => $userDPO[$val],
                            'champ_name'=> $prefixChampName.$champ_name
                        ]);
                        $success = $success && false !== $this->Valeur->save(null, ['atomic' => false]);
                    }
                }
            }
        }

        return $success;
    }

    /**
     * Envoie d'un traitement en cour d'initialisation en rédaction
     *
     * @access public
     * @created 10/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function sendRedaction()
    {
        $success = true;
        $this->EtatFiche->begin();

        // On change le rédacteur du traitement
        $success = $this->_updateRedacteurTraitementInitialise(
            $this->request->data['EtatFiche']['ficheNum'],
            $this->request->data['EtatFiche']['destinataire']
        );

        // On met EtatFiche.actif a false en fonction de l'id
        $success = $success && $this->EtatFiche->updateAllIfFound(
            [
                'actif' => false
            ],
            [
                'id' => $this->request->data['EtatFiche']['etatFiche'],
            ]
        ) !== false;

        if ($success === true) {
            $messageHistorique = __d('historique', 'historique.envoieTraitementInitialiserAuRedacteur');

            $this->EtatFiche->create([
                'EtatFiche' => [
                    'fiche_id' => $this->request->data['EtatFiche']['ficheNum'],
                    'etat_id' => EtatFiche::TRAITEMENT_INITIALISE_RECU,
                    'previous_user_id' => $this->Auth->user('id'),
                    'user_id' => $this->request->data['EtatFiche']['destinataire']
                ]
            ]);
            $success = $success && false !== $this->EtatFiche->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Notification->create([
                    'Notification' => [
                        'user_id' => $this->request->data['EtatFiche']['destinataire'],
                        'content' => Notification::TRAITEMENT_INITIALISE,
                        'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                    ]
                ]);
                $success = $success && false !== $this->Notification->save(null, ['atomic' => false]);

                if ($success === true) {
                    $this->Notifications->sendEmail($this->request->data['EtatFiche']['destinataire']);
                }

                if ($success == true) {
                    $destinataire = $this->User->find('first', [
                        'conditions' => [
                            'id' => $this->request->data['EtatFiche']['destinataire']
                        ]
                    ]);

                    $this->Historique->create([
                        'Historique' => [
                            'content' => $this->Auth->user('nom_complet') . ' ' . $messageHistorique . ' ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'] . ' ' . __d('historique', 'historique.redaction'),
                            'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
                        ]
                    ]);
                    $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);

                    // @todo
//                    if ($success == true) {
//                        $this->Historique->create([
//                            'Historique' => [
//                                'content' => __d('historique', 'historique.redactionInitialisationTraitement') . ' ' . $destinataire['User']['prenom'] . ' ' . $destinataire['User']['nom'] . ' ' . __d('historique', 'historique.redactiontraitementInitialisationTraitement'),
//                                'fiche_id' => $this->request->data['EtatFiche']['ficheNum']
//                            ]
//                        ]);
//                        $success = $success && false !== $this->Historique->save(null, ['atomic' => false]);
//                    }
                }
            }
        }

        if ($success == true) {
            $this->EtatFiche->commit();
            $this->Session->setFlash(__d('etat_fiche', 'etat_fiche.flashsuccessTraitementEnvoyerRedaction'), 'flashsuccess');

            $this->requestAction([
                'controller' => 'pannel',
                'action' => 'supprimerLaNotif',
                $this->request->data['EtatFiche']['ficheNum']
            ]);
        } else {
            $this->EtatFiche->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Mise à jour du rédacteur du traitement initialisé
     *
     * @param $organisation_id int
     * @return bool
     *
     * @access private
     * @created 03/12/2019
     * @version V1.1.0
     */
    private function _updateRedacteurTraitementInitialise($fiche_id, $destinataire)
    {
        $success = true;

        $values = $this->Valeur->find('all', [
            'conditions' => [
                'fiche_id' => $fiche_id,
                'champ_name' => [
                    'declarantpersonnenom',
                    'declarantpersonneemail',
                    'declarantpersonnefix',
                    'declarantpersonneportable',
                ]
            ]
        ]);

        if (!empty($values)) {
            foreach ($values as $valeurFiche) {
                $success = $success && false !== $this->Valeur->delete($valeurFiche['Valeur']['id'], ['atomic' => false]);
            }
        }

        $user = $this->User->find('first', [
            'conditions' => [
                'id' => $destinataire
            ],
            'fields' => [
                'id',
                'nom_complet_court',
                'email',
                'telephonefixe',
                'telephoneportable',
            ]
        ]);

        $concordance = [
            'nom_complet_court' => 'declarantpersonnenom',
            'email' => 'declarantpersonneemail',
            'telephonefixe' => 'declarantpersonnefix',
            'telephoneportable' => 'declarantpersonneportable',
        ];

        foreach ($user['User'] as $key => $valeur) {
            if (isset($valeur) &&
                isset($concordance[$key]) &&
                $success === true
            ) {
                $this->Valeur->create([
                    'fiche_id' => $fiche_id,
                    'valeur' => $valeur,
                    'champ_name' => $concordance[$key]
                ]);
                $success = false !== $this->Valeur->save(null, ['atomic' => false]);

            }
        }

        if ($success === true) {
            $this->Fiche->id = $fiche_id;

            $record = [
                'id' => $fiche_id,
                'user_id' => $user['User']['id'],
                'service_id' => null,
                'partage' => null
            ];
            $success = false !== $this->Fiche->save($record, ['atomic' => false]);
        }

        return $success;
    }

}
