<?php

/**
 * SoustraitantsController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class SoustraitantsController extends AppController {

    public $uses = [
        'Soustraitant',
        'Soustraitance',
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);

        if ($action === 'ajax_add') {
            $this->Droits->assertAuthorized([ListeDroit::GESTION_SOUSTRAITANT_TRAITEMENT]);
        } else {
            $this->Droits->assertAuthorized([ListeDroit::GESTION_SOUSTRAITANT]);
        }
    }

    /**
     * Retourne les résultats du moteur de recherche, que ce soit pour la liste complète ou pour la liste de ceux
     * présents dans l'entité.
     *
     * @return array
     */
    protected function _getSearchResults() {
        $paginate = [
            'contain' => [
                'Organisation' => [
                    'id',
                    'raisonsociale',
                    'order' => ['raisonsociale']
                ],
                'Fiche'=> [
                    'id'
                ],
            ],
            'conditions' => [],
            'order' => [
                'Soustraitant.raisonsocialestructure ASC'
            ]
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        // Applications des filtres
        if ($this->request->is('post') || false === empty($search)) {
            if ($this->request->params['action'] === 'index') {
                // Filtrer par entité associée
                if (!empty($this->request->data['Filtre']['organisation'])) {
                    $paginate['conditions'][] = $this->Soustraitant->getConditionOrganisation($this->request->data['Filtre']['organisation']);
                }
                // Filtrer par entité créatrice
                $createdbyorganisation = (string)Hash::get($this->request->data, 'Filtre.createdbyorganisation');
                if ($createdbyorganisation !== '') {
                    $paginate['conditions'][] = ['Soustraitant.createdbyorganisation' => $createdbyorganisation];
                }
            }

            $filters = [
                // Filtrer par raison sociale (du sous-traitant)
                'Filtre.raisonsociale' => 'Soustraitant.raisonsocialestructure',
                // Filtrer par numéro SIRET (du sous-traitant)
                'Filtre.siretstructure' => 'Soustraitant.siretstructure',
                // Filtrer par code APE (du sous-traitant)
                'Filtre.ape' => 'Soustraitant.apestructure',
            ];
            foreach ($filters as $filter => $path) {
                $value = (string)Hash::get($this->request->data, $filter);
                if ($value !== '') {
                    $paginate['conditions'][$path] = $value;
                }
            }
        }

        // Ajout de conditions suivant l'utilisateur connecté et l'action
        if ($this->request->params['action'] === 'entite') {
            $paginate['conditions'][] = $this->Soustraitant->getConditionOrganisation($this->Session->read('Organisation.id'));
        }

        $this->paginate = $paginate;
        return $this->paginate($this->Soustraitant);
    }

    /**
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function index() {
        $this->set('title', __d('soustraitant', 'soustraitant.titreGestionSoustraitantApplication'));

        if ($this->request->is('post')) {
            // Affectation
            if (!empty($this->request->data['SoustraitantOrganisation']['organisation_id']) && isset($this->request->data['SoustraitantOrganisation']) === true) {
                $success = true;
                $this->Soustraitant->SoustraitantOrganisation->Organisation->begin();

                $organisations_ids = Hash::extract($this->request->data, 'SoustraitantOrganisation.organisation_id');
                $soustraitants_ids = Hash::extract($this->request->data, 'SoustraitantOrganisation.soustraitant_id');
                foreach ($organisations_ids as $organisation_id) {
                    $soustraitantEntite = $this->Soustraitant->SoustraitantOrganisation->find('list', [
                        'conditions' => [
                            'organisation_id' => $organisation_id
                        ],
                        'fields' => [
                            'soustraitant_id'
                        ]
                    ]);

                    $diff = array_diff($soustraitantEntite, $soustraitants_ids);

                    if (!empty($diff)) {
                        $resultSoustraitant = array_merge($soustraitants_ids, $soustraitantEntite);
                    } else {
                        $resultSoustraitant = $soustraitants_ids;
                    }

                    $data = [
                       'Organisation' => [
                           'id' => $organisation_id,
                       ],
                        'Soustraitant' => [
                            'Soustraitant' => $resultSoustraitant
                        ]
                    ];
                    $success = $success && false !== $this->Soustraitant->SoustraitantOrganisation->Organisation->save($data, ['atomic' => false]);
                }

                if ($success === true) {
                    $this->Soustraitant->SoustraitantOrganisation->Organisation->commit();
                    $this->Session->setFlash(__d('soustraitant', 'soustraitant.flashsuccessSousTraitantAffecterEnregistrer'), 'flashsuccess');
                } else {
                    $this->Soustraitant->SoustraitantOrganisation->Organisation->rollback();
                    $this->Session->setFlash(__d('soustraitant', 'soustraitant.flasherrorErreurEnregistrementSousTraitantAffecter'), 'flasherror');
                }

                unset($this->request->data['SoustraitantOrganisation']);
            }
        }

        $this->set([
            'mesOrganisations' => $this->WebcilUsers->mesOrganisations('list'),
            'options' => $this->_optionsFiltre(),
            'soustraitants' => $this->_getSearchResults(),
        ]);
    }

    /**
     * Retourne les options à utiliser dans les formulaires de recherche.
     *
     * @param int|null $organisation_id
     * @return array
     */
    protected function _optionsFiltre($organisation_id = null) {
        $options = [
            'apestructure' => $this->Soustraitant->getStringOptionList('apestructure', $organisation_id),
            'organisations' => $this->Soustraitant->Organisation->find('list', ['order' => ['Organisation.raisonsociale ASC']]),
            'raisonsocialestructure' => $this->Soustraitant->getStringOptionList('raisonsocialestructure', $organisation_id),
            'siretstructure' => $this->Soustraitant->getStringOptionList('siretstructure', $organisation_id),
        ];

        return $options;
    }

    /**
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function entite() {
        $this->set('title', __d('soustraitant', 'soustraitant.titreGestionSoustraitantEntitee'));

        $this->set([
            'options' => $this->_optionsFiltre($this->Session->read('Organisation.id')),
            'soustraitants' => $this->_getSearchResults(),
        ]);
    }

    /**
     * Fonction qui permet l'ajout d'un nouveau sous-traitant
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function add() {
        $this->edit(null);
    }

    /**
     * Fonction qui permet la modification d'un sous-traitant
     *
     * @param int $id
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function edit($id) {
        if (in_array($this->request->params['action'], ['add', 'ajax_add'])) {
            $this->set('title', __d('soustraitant', 'soustraitant.titreAjouterSoustraitant'));
        } else {
            $soustraitant = $this->Droits->getAndCheckLinkedOrganisationsRecord('Soustraitant', $id, false);
            $this->set('title', __d('soustraitant', 'soustraitant.titreModificationSoustraitant'));
        }

        $cannotModified = false;

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            if (in_array($this->request->params['action'], ['add', 'ajax_add'])) {
                $data = $this->request->data;
                $data['Soustraitant']['createdbyorganisation'] = $this->Droits->isSu() ? null : $this->Session->read('Organisation.id');
            } else {
                $data = $this->request->data;

                $soustraitantUseInFiche = $this->Soustraitance->find('all', [
                    'conditions' => [
                        'soustraitant_id' => $id
                    ]
                ]);

                if (!empty($soustraitantUseInFiche)) {
                    unset($data['Responsable']['raisonsocialestructure']);
                    unset($data['Responsable']['siretstructure']);
                }

                foreach (['id', 'createdbyorganisation'] as $fieldName) {
                    $data['Soustraitant'][$fieldName] = $soustraitant['Soustraitant'][$fieldName];
                }
            }

            $success = true;
            $this->Soustraitant->begin();

            $this->Soustraitant->create($data);
            $success = $success && false !== $this->Soustraitant->save(null, ['atomic' => true]);

            if ($success == true) {
                $this->Soustraitant->commit();

                if ($this->request->params['action'] === 'ajax_add') {
                    $this->layout = null;
                    $this->autoRender = false;

                    $this->response->type('application/json');
                    $this->response->body(json_encode(['id' => $this->Soustraitant->getLastInsertID()]));
                    return $this->response->statusCode(201);
                } else {
                    $this->Session->setFlash(__d('soustraitant', 'soustraitant.flashsuccessSaveSoustraitant'), 'flashsuccess');
                    $this->redirect($this->Referers->get());
                }
            } else {
                $this->Soustraitant->rollback();
                $this->Session->setFlash(__d('soustraitant', 'soustraitant.flasherrorSaveSoustraitant'), 'flasherror');
            }
        } elseif ($this->request->params['action'] === 'add') {
            $this->request->data['Organisation']['Organisation'] = $this->Droits->isSu() ? null : $this->Session->read('Organisation.id');
        } else {
            $this->request->data = $soustraitant;

            $soustraitantUseInFiche = $this->Soustraitance->find('all', [
                'conditions' => [
                    'soustraitant_id' => $id
                ]
            ]);

            if (!empty($soustraitantUseInFiche)) {
                $cannotModified = true;
            }
        }

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations', 'cannotModified'));

        if ($this->request->params['action'] === 'ajax_add') {
            $this->view = 'ajax_add';
        } else  {
            $this->view = 'edit';
        }
    }

    /**
     * Permet la visualisation des informations d'un sous-traitant
     *
     * @param int $id | Id du sous-traitant
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function show($id) {
        $this->set('title', __d('soustraitant', 'soustraitant.titreVisualisationSoustraitant'));

        $query = [
            'fields' => $this->Soustraitant->fields(),
            'contain' => [
                'Organisation' => [
                    'fields' => [
                        'id'
                    ]
                ]
            ],
            'conditions' => [
                'Soustraitant.id' => $id
            ]
        ];
        $record = $this->Soustraitant->find('first', $query);

        if (empty($record) === true) {
            throw new NotFoundException();
        }

        if ('Back' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $record['Organisation'] = ['Organisation' => Hash::extract($record, 'Organisation.{n}.id')];

        $this->request->data = $record;

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations'));
        $this->set('cannotModified', false);

        $this->view = 'edit';
    }

    /**
     * Permet de supprimer un sous-traitant
     *
     * @access public
     *
     * @param int $id | Id du sous-traitant
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 12/04/2018
     * @version v1.0.0
     *
     * @modified 16/06/2021
     * @version v2.1.0
     */
    public function delete($id)
    {
        $this->Droits->getAndCheckLinkedOrganisationsRecord('Soustraitant', $id, true);

        $soustraitantUseInFiche = $this->Soustraitance->find('all', [
            'conditions' => [
                'soustraitant_id' => $id
            ]
        ]);

        if (empty($soustraitantUseInFiche)) {
            $this->Soustraitant->begin();

            if (false !== $this->Soustraitant->delete($id)) {
                $this->Soustraitant->commit();
                $this->Session->setFlash(__d('soustraitant', 'soustraitant.flashsuccessSuppressionSoustraitantEntite'), 'flashsuccess');
            } else {
                $this->Soustraitant->rollback();
                $this->Session->setFlash(__d('soustraitant', 'soustraitant.flasherrorErreurSuppressionSoustraitantEntite'), 'flasherror');
            }
        } else {
            $this->Session->setFlash(__d('soustraitant', 'soustraitant.flasherrorErreurCannotDeleteSoustraitantUseFiche'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Fonction qui permet de supprimer un sous-traitant liée à l'entité
     *
     * @param int $id
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 12/04/2018
     * @version v1.0.0
     */
    public function dissocierSoustraitant($id) {
        $this->Droits->assertRecordAuthorized('Soustraitant', $id, ['superadmin' => true]);

        $this->Soustraitant->begin();

        $success = false !== $this->Soustraitant->SoustraitantOrganisation->deleteAll([
            'SoustraitantOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
            'SoustraitantOrganisation.soustraitant_id' => $id
        ]);

        if ($success == true) {
            $this->Soustraitant->commit();
            $this->Session->setFlash(__d('soustraitant', 'soustraitant.flashsuccessDissocierSoustraitantEntite'), 'flashsuccess');
        } else {
            $this->Soustraitant->rollback();
            $this->Session->setFlash(__d('soustraitant', 'soustraitant.flasherrorErreurDissocierSoustraitantEntite'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 08/07/2020
     * @version v2.0.0
     */
    public function ajax_add()
    {
        if ($this->request->is('post')) {
            $data['Soustraitant'] = $this->request->data;
            $data['Organisation']['Organisation'] = [$this->Session->read('Organisation.id')];

            $this->layout  = 'ajax';
            $this->request->data = $data;
            $this->add();
        }
    }
}
