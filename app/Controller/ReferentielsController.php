<?php

App::uses('ListeDroit', 'Model');

class ReferentielsController extends AppController
{
    public $uses = [
        'Referentiel'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $listeDroitIndex = [
            ListeDroit::CREER_REFERENTIEL,
            ListeDroit::VISUALISER_REFERENTIEL,
            ListeDroit::MODIFIER_REFERENTIEL,
            ListeDroit::ABROGER_REFERENTIEL
        ];

        if ($action === 'index') {
            $this->Droits->assertAuthorized($listeDroitIndex);
        } elseif ($action === 'add') {
            $this->Droits->assertAuthorized([ListeDroit::CREER_REFERENTIEL]);
            $this->Droits->isSu();
        } elseif ($action === 'edit') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_REFERENTIEL]);
            $this->Droits->isSu();
        } elseif ($action === 'show') {
            $this->Droits->assertAuthorized([ListeDroit::VISUALISER_REFERENTIEL]);
            $this->Droits->isSu();
        } elseif ($action === 'abroger') {
            $this->Droits->assertAuthorized([ListeDroit::ABROGER_REFERENTIEL]);
            $this->Droits->isSu();
        } elseif ($action === 'download') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_REFERENTIEL, ListeDroit::VISUALISER_REFERENTIEL]);
            $this->Droits->isSu();
        } elseif ($action === 'delete_file_save') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_REFERENTIEL]);
            $this->Droits->isSu();
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function index()
    {
        $this->set('title', __d('referentiel', 'referentiel.titreIndex'));

        $query = [
            'order' => 'Referentiel.name ASC',
            'limit' => 20
        ];

        $this->paginate = $query;
        $referentiels = $this->paginate('Referentiel');

        $this->set('referentiels', $referentiels);
    }

    public function add()
    {
        $this->set('title', __d('referentiel', 'referentiel.titreAdd'));

        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $data = $this->request->data;
            $success = true;
            $info['message'] = __d('referentiel','referentiel.flasherrorErreurEnregistrementNorme');
            $info['statutMessage'] = 'flasherror';

            // Tentative de sauvegarde
            $this->Referentiel->begin();

            $this->Referentiel->create([
                'name' => $data['Referentiel']['name'],
                'description' => $data['Referentiel']['description']
            ]);
            $success = $success && false !== $this->Referentiel->save(null, ['atomic' => false]);

            if ($success === true &&
                isset($data['Referentiel']['fichier']) &&
                file_exists($data['Referentiel']['fichier']['tmp_name'])
            ) {
                $info = $this->Referentiel->saveFileReferentiel($data['Referentiel']['fichier'], $this->Referentiel->getLastInsertId());
                $success = false !== $info['success'];
            }

            if ($success === true) {
                $this->Referentiel->commit();
                $this->Session->setFlash(__d('referentiel', 'referentiel.flashsuccessReferentielEnregistrer'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                $this->Referentiel->rollback();
                $this->Session->setFlash($info['message'], $info['statutMessage']);
            }
        }

        $this->view = 'edit';
    }

    public function edit($id)
    {
        $this->Droits->assertRecordExists('Referentiel', $id);

        $referentiel = $this->Referentiel->findById($id);
        $this->set('title', __d('referentiel', 'referentiel.titreEdit') . $referentiel['Referentiel']['name']);

        if ($this->request->is(['post', 'put'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());

                $this->Session->delete('Auth.User.uuid');
            }

            $data = $this->request->data;

            // Tentative de sauvegarde
            $this->Referentiel->begin();
            $success = true;

            $this->Referentiel->id = $id;
            $this->Referentiel->create([
                'id' => $id,
                'name' => $data['Referentiel']['name'],
                'description' => $data['Referentiel']['description']
            ]);
            $success = $success && false !== $this->Referentiel->save(null, ['atomic' => false]);

            if ($success === true &&
                isset($data['Referentiel']['fichier']) &&
                file_exists($data['Referentiel']['fichier']['tmp_name'])
            ) {
                $info = $this->Referentiel->saveFileReferentiel($data['Referentiel']['fichier'], $this->Referentiel->getLastInsertId());
                $success = false !== $info['success'];
            }

            if ($success === true) {
                $this->Referentiel->commit();
                $this->Session->setFlash(__d('referentiel', 'referentiel.flashsuccessReferentielEnregistrer'), 'flashsuccess');

                $this->Session->delete('Auth.User.uuid');

                $this->redirect($this->Referers->get());
            } else {
                $this->Referentiel->rollback();
                $this->Session->setFlash(__d('referentiel', 'referentiel.flasherrorErreurEnregistrementReferentiel'), 'flasherror');
            }
        } else {
            $this->request->data = $referentiel;
        }

        $this->set(compact('referentiel'));
    }

    public function show($id)
    {
        if ('Back' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $referentiel = $this->Referentiel->find('first', [
            'conditions' => [
                'id' => $id
            ],
            'fields' => [
                'name',
                'description',
                'fichier',
                'name_fichier'
            ]
        ]);
        $this->set('title', $referentiel['Referentiel']['name']);

        $this->request->data = $referentiel;
    }

    /**
     * Fonction qui permet d'abroger d'une norme.
     *
     * @param int $id
     *
     * @access public
     *
     * @created 22/10/2021
     * @version V2.1.1
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function abroger($id, $state = null)
    {
        $this->Droits->assertRecordExists('Referentiel', $id);

        $this->Referentiel->begin();

        $abroger = (int) !$state;

        $this->Referentiel->id = $id;
        $success = $this->Referentiel->updateAll(
            [
                'abroger' => $abroger
            ],
            [
                'id' => $id
            ]
        ) !== false;

        if ($success == true) {
            $this->Referentiel->commit();

            if ($abroger == true) {
                $this->Session->setFlash(__d('referentiel', 'referentiel.flashsuccessReferentielAbroger'), 'flashsuccess');
            } else {
                $this->Session->setFlash(__d('referentiel', 'referentiel.flashsuccessReferentielRevocationAbroger'), 'flashsuccess');
            }

            $this->redirect($this->Referers->get());
        } else {
            $this->Referentiel->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }
    }

    /**
     * Permet de télécharger le fichier sur le serveur
     *
     * @param string $urlFichier : nom du fichier sur le serveur
     * @param string $nameFichier : nom du fichier déposer par l'utilisateur
     * @return file téléchargement du fichier
     *
     * @access public
     *
     * @created 25/10/2021
     * @version V2.1.1
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function download($urlFichier, $nameFichier)
    {
        $path = CHEMIN_REFERENTIELS . $urlFichier;
        if (file_exists($path) === false) {
            $path = CHEMIN_NEW_REFERENTIELS . DS . $urlFichier;
            if (file_exists($path) === false) {
                throw new NotFoundException();
            }
        }

        $this->response->file($path, ['download' => true, 'name' => $nameFichier]);
        return $this->response;
    }

    /**
     * Fonction qui permet de supprimer le fichier associé au référentiel
     * Supprimer les informations en BDD et le fichier sur le serveur
     *
     * @param int $id
     * @param string $urlFile : nom du fichier sur le serveur
     *
     * @access public
     *
     * @created 25/10/2021
     * @version V2.1.1
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function deleteFileSave($id, $urlFile)
    {
        $this->autoRender = false;

        $success = true;
        $this->Referentiel->begin();

        $this->Referentiel->id = $id;
        $record = [
            'id' => $id,
            'fichier' => null,
            'name_fichier' => null
        ];
        $success = false !== $this->Referentiel->save($record, ['atomic' => false]) && $success;

        if ($success === true) {
            $cheminFile =  CHEMIN_REFERENTIELS . $urlFile;

            if (file_exists($cheminFile) === false) {
                $cheminFile =  CHEMIN_NEW_REFERENTIELS . $urlFile;
            }

            $success = unlink($cheminFile);
        }

        if ($success == true) {
            $this->Referentiel->commit();
            $this->Session->setFlash(__d('referentiel', 'referentiel.flashsuccessFichierSupprimer'), 'flashsuccess');
        } else {
            $this->Referentiel->rollback();
            $this->Session->setFlash(__d('referentiel', 'referentiel.flasherrorFichierSupprimer'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
}
