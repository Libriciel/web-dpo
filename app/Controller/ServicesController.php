<?php

/**
 * ServicesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class ServicesController extends AppController {

    public $uses = [
        'Service',
        'OrganisationUserService',
        'Organisation',
        'OrganisationUser',
        'OrganisationUserService',
        'User',
        'Fiche'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $listeDroitIndex = [ListeDroit::CREER_SERVICE, ListeDroit::MODIFIER_SERVICE, ListeDroit::SUPPRIMER_SERVICE];

        if (in_array($action, ['add', 'import_services', 'download']) === true) {
            $this->Droits->assertAuthorized([ListeDroit::CREER_SERVICE]);
        } elseif ($action === 'edit') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_SERVICE]);
        } elseif ($action === 'index') {
            $this->Droits->assertAuthorized($listeDroitIndex);
        } elseif ($action === 'delete') {
            $this->Droits->assertAuthorized([ListeDroit::SUPPRIMER_SERVICE]);
        } elseif ($action === 'dissocier') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_UTILISATEUR]);
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     *
     * @created 18/06/2015
     * @version V1.0.0
     *
     * @modified 26/03/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function index()
    {
        $this->set('title', __d('service', 'service.titreService') . $this->Session->read('Organisation.raisonsociale'));

        $query = [
            'fields' => array_merge(
                $this->Service->fields(),
                [
                    $this->Service->vfLinkedUsersCount(),
                    $this->Service->vfLinkedFichesCount(),
                ]
            ),
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ],
            'order' => 'libelle ASC'
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        if ($this->request->is('post') || false === empty($search)) {
            $data = $this->request->data;

            if (isset($data['Filtre'])) {
                // Filtre par nom du service
                $service_id = $data['Filtre']['service'];
                if (!empty($service_id)) {
                    $query['conditions']['Service.id'] = $service_id;
                }
            }

            if (isset($data['ServiceUser']) &&
                empty($data['ServiceUser']['user_id']) === false &&
                empty($data['ServiceUser']['service_id']) === false
            ) {
                $success = true;
                $this->Service->OrganisationUserService->begin();

                $dataOrganisationUsers_ids = Hash::extract($data, 'ServiceUser.user_id');
                $service_ids = Hash::extract($data, 'ServiceUser.service_id');

                foreach ($service_ids as $service_id) {
                    $serviceUser = $this->Service->OrganisationUserService->find('list', [
                        'conditions' => [
                            'OrganisationUserService.service_id' => $service_id
                        ],
                        'fields' => [
                            'OrganisationUserService.organisation_user_id'
                        ]
                    ]);

                    $result = array_intersect($serviceUser, $dataOrganisationUsers_ids);
                    $organisationUsers_ids = array_diff($dataOrganisationUsers_ids, $result);

                    foreach ($organisationUsers_ids as $organisationUser_id) {
                        $data = [
                            'OrganisationUserService' => [
                                'service_id' => $service_id,
                                'organisation_user_id' => $organisationUser_id,
                            ]
                        ];

                        $this->Service->OrganisationUserService->create($data);
                        $success = $success && false !== $this->Service->OrganisationUserService->save(null, ['atomic' => false]);
                    }
                }

                if ($success == true) {
                    $this->Service->OrganisationUserService->commit();
                    $this->Session->setFlash(__d('service', 'service.flashsuccessServicesAffecterUserEnregistrer'), 'flashsuccess');
                } else {
                    $this->Service->OrganisationUserService->rollback();
                    $this->Session->setFlash(__d('service', 'service.flasherrorErreurEnregistrementServicesAffecterUser'), 'flasherror');
                }

                unset($this->request->data['ServiceUser']);
            }
        }

        $this->paginate = $query;
        $serv = $this->paginate($this->Service);

        $users = $this->User->find('list', [
            'joins' => [
                $this->User->join('OrganisationUser')
            ],
            'conditions' => [
                'OrganisationUser.organisation_id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'OrganisationUser.id',
                'User.nom_complet',
            ]
        ]);

        $options['services'] = $this->Service->find('list', [
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'id',
                'libelle'
            ],
            'order' => 'libelle ASC'
        ]);

        $this->set(compact('serv', 'users', 'options'));
    }

    /**
     * @access public
     * @created 18/06/2015
     * @version V1.0.0
     */
    public function add() {
        $this->set('title', __d('service', 'service.titreAjouterService'));

        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }
            
            $success = true;
            $this->Service->begin();

            $this->Service->create($this->request->data);
            $success = $success && false !== $this->Service->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Service->commit();
                $this->Session->setFlash(__d('service', 'service.flashsuccessServiceEnregistrer'), 'flashsuccess');
                
                $this->redirect($this->Referers->get());
            } else {
                $this->Service->rollback();
                $this->Session->setFlash(__d('service', 'service.flasherrorErreurEnregistrementService'), 'flasherror');
            }
        }

        $this->view = 'edit';
    }

    /**
     * Modification du libelle d'un service
     *
     * @param int|null $id
     *
     * @access public
     *
     * @created 18/06/2015
     * @version V1.0.0
     *
     * @modified  26/03/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function edit($id = null)
    {
        $nbFicheUseService = $this->Fiche->find('count', [
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id'),
                'service_id' => $id
            ]
        ]);

        if ($nbFicheUseService !== 0) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->Droits->assertRecordAuthorized('Service', $id, ['superadmin' => true]);

        $this->set('title', __d('service', 'service.titreModifierrService'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }
            
            $success = true;
            $this->Service->begin();
            
            $this->request->data['Service']['id'] = $id;
            
            $this->Service->create($this->request->data);
            $success = $success && false !== $this->Service->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->Service->commit();
                $this->Session->setFlash(__d('service', 'service.flashsuccessServiceEnregistrer'), "flashsuccess");
                
                $this->redirect($this->Referers->get());
            } else {
                $this->Service->rollback();
                $this->Session->setFlash(__d('service', 'service.flasherrorErreurEnregistrementService'), 'flasherror');
            }
        } else {
            $this->request->data = $this->Service->findById($id);
        }
    }

    /**
     * @param int|null $id
     *
     * @access public
     * @created 18/06/2015
     * @version V1.0.0
     */
    public function delete($id = null) {
        $this->Droits->assertRecordAuthorized('Service', $id, ['superadmin' => true]);

        // Le service ne doit être lié à aucun utilisateur
        $query = [
            'fields' => [
                $this->Service->vfLinkedUsersCount(),
                $this->Service->vfLinkedFichesCount(),
            ],
            'conditions' => ['Service.id' => $id]
        ];
        $service = $this->Service->find('first', $query);

        if ($service['Service']['users_count'] > 0 ||
            $service['Service']['fiches_count'] > 0
        ) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->set('title', 'Supprimer un service');

        $this->Service->begin();
        if ($this->Service->delete($id, false) == true) {
            $this->Service->commit();
            $this->Session->setFlash(__d('service', 'service.flashsuccessServiceSupprimer'), 'flashsuccess');
        } else {
            $this->Service->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorErreurSupprimerService'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Fonction qui permet de supprimer l'association entre un service et tous les utilisateurs rattachés à ce service
     *
     * @param int $service_id
     * @throws ForbiddenException
     *
     * @access public
     *
     * @created 26/03/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function dissocier($service_id)
    {
        $this->Service->OrganisationUserService->begin();

        $success = $this->Service->OrganisationUserService->deleteAll([
            'OrganisationUserService.service_id' => $service_id,
        ]);

        if ($success == true) {
            $this->Service->OrganisationUserService->commit();
            $this->Session->setFlash(__d('service', 'service.flashsuccessDissocier'), 'flashsuccess');
        } else {
            $this->Service->OrganisationUserService->rollback();
            $this->Session->setFlash(__d('service', 'service.flasherrorErreurDissocier'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     *  Fonction permettant l'import d'une liste de service via un fichier CSV
     *
     * @access public
     *
     * @created 26/03/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function import_services()
    {
        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $data = $this->request->data;

            if (empty($data['Service']['Service']['tmp_name']) ||
                $data['Service']['Service']['error'] !== 0 ||
                file_exists($data['Service']['Service']['tmp_name'] === false)
            ) {
                $this->Session->setFlash(__d('default', 'default.flashAucunFichier'), 'flashwarning');
                $this->redirect($this->Referers->get());
            }

            $path_parts = pathinfo($data['Service']['Service']['name']);
            if ($path_parts['extension'] !== 'csv') {
                $this->Session->setFlash(__d('default', 'default.flashExtensionNonValide'), 'flashwarning');
                $this->redirect($this->Referers->get());
            }

            $services = [];
            $start_row = 2; //define start row
            $i = 1; //define row count flag
            $fileCSV = fopen($data['Service']['Service']['tmp_name'], "r");
            if ($fileCSV !== false) {
                while (($data = fgetcsv($fileCSV, 1000, ",")) !== false) {
                    if ($i >= $start_row) {
                        if (!empty($data[0])) {
                            $services[] = trim($data[0]);
                        }
                    }
                    $i++;
                }

                fclose($fileCSV);
            }

            if (!empty($services)) {
                $success = true;
                $this->Service->begin();

                foreach ($services as $service) {
                    $data = [
                        'Service' => [
                            'libelle' => $service,
                            'organisation_id' => $this->Session->read('Organisation.id'),
                        ]
                    ];

                    $this->Service->create($data);
                    $success = $success && false !== $this->Service->save(null, ['atomic' => false]);
                }

                if ($success == true) {
                    $this->Service->commit();
                    $this->Session->setFlash(__d('service', 'service.flashsuccessImportServiceEnregistrer'), 'flashsuccess');

                    $this->redirect($this->Referers->get());
                } else {
                    $this->Service->rollback();
                    $this->Session->setFlash(__d('service', 'service.flasherrorErreurEnregistrementImportService'), 'flasherror');
                }
            }
        }

        $this->redirect($this->Referers->get());
    }

    public function download()
    {
        $this->response->file(FILE_EXEMPLE_IMPORT_SERVICES, [
            'download' => true
        ]);

        return $this->response;
    }

}
