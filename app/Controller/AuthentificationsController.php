<?php

/**
 * AuthentificationsController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class AuthentificationsController extends AppController {

    public $uses = [
        'Authentification'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);

        if ($action === 'add') {
            $this->Droits->assertAuthorized([ListeDroit::GESTION_MAINTENANCE]);
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * Permets l'ajout et la modification du connecteur LDAP
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/03/2018
     * @version V1.0.0
     */
    public function add() {
        $this->set('title', __d('authentification', 'authentification.titreAuthentification'));

        //@fixme : pb avec la fonction "CAS". Suppression de l'option de connexion "CAS" le temps de passer en v3
        $options = $this->Authentification->enums();
        unset($options['Authentification']['type']['CAS']);
        $this->set('options', $options);
        
        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect([
                    'controller' => 'connecteurs',
                    'action' => 'index'
                ]);
            }

            $success = true;
            $this->Authentification->begin();
            
//            if (!empty($this->request->data['Authentification']['name_fichier'])) {
//                $file = $this->request->data['Authentification']['name_fichier'];
//            }
            $this->request->data['Authentification']['name_fichier'] = null;
            $this->request->data['Authentification']['uri'] = null;

            $this->request->data['Authentification']['organisation_id'] = $this->Session->read('Organisation.id');
            $this->Authentification->create($this->request->data);
            $success = $success && false !== $this->Authentification->save(null, ['atomic' => false]);
            
//            if (!empty($file)) {
//                $success = $success && $this->Authentification->saveFile($file, $this->request->data['Authentification']['id']);
//            }

            if ($success === true) {
                $this->Authentification->commit();
                $this->Session->setFlash(__d('authentification', 'authentification.flashsuccessConnecteurEnregistrer'), 'flashsuccess');
                
                $this->redirect([
                    'controller' => 'connecteurs',
                    'action' => 'index'
                ]);
            } else {
                $this->Authentification->rollback();
                $this->Session->setFlash(__d('authentification', 'authentification.flasherrorErreurEnregistrementConnecteur'), 'flasherror');
            }
        } else {
            $query = [
                'conditions' => [
                    'organisation_id' => $this->Session->read('Organisation.id')
                ]
            ];
            $this->request->data = $this->Authentification->find('first', $query);
        }
    }
    
}
