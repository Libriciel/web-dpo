<?php

/**
 * DroitsComponent
 *
 * Component de gestion des droits
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     Component
 */

App::uses('EtatFiche', 'Model');
App::uses('Inflector', 'Utility');
App::uses('LinkedOrganisationInterface', 'Model/Interface');
App::uses('ListeDroit', 'Model');

class DroitsComponent extends Component {

    public $components = [
        'Session',
        'WebcilUsers'
    ];

    /**
     * Fonction de vérification des droits dans le tableau en Session
     * 
     * @param type $level
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function authorized($level)
    {
        $table = (array)$this->Session->read('Droit.liste');
        
        if (is_array($level)) {
            foreach ($level as $value) {
                foreach ($table as $valeur) {
                    if ($valeur == $value) {
                        return true;
                    }
                }
            }
        } else {
            if (in_array($level, $table)) {
                return true;
            }
        }
        
        if ($this->isSu()) {
            return true;
        }
        
        return false;
    }

    /**
     * Verification du propriétaire de la fiche
     * 
     * @param int $idFiche
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function isOwner($idFiche)
    {
        $Fiche = ClassRegistry::init('Fiche');
        
        $id_user_fiche = $Fiche->find('first', [
            'conditions' => ['id' => $idFiche],
            'fields' => ['user_id']
        ]);
        
        if (isset($id_user_fiche['Fiche']['user_id']) && $id_user_fiche['Fiche']['user_id'] == $this->Session->read('Auth.User.id')) {
            return true;
        }
        
        return false;
    }

    public function isLogged() {
        return empty($this->Session->read('Auth.User.id')) === false;
    }

    /**
     * Vérification si l'user est DPO de son organisation
     * 
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @modified 08/10/2019
     * @version V1.1.0
     */
    public function isDpo()
    {
        if ($this->Session->read('Organisation.id') != null) {
            $Organisation = ClassRegistry::init('Organisation');

            $dpoDeclarer_id = $Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'Organisation.dpo'
                ]
            ]);

            if ($dpoDeclarer_id['Organisation']['dpo'] == $this->Session->read('Auth.User.id')) {
                if ($this->authorized(ListeDroit::LISTE_DROITS_DPO_MINIMUM) == true) {
                    return true;
                }
            }
        }
        
        return false;
    }

    /**
     * Verification si la fiche est visible par l'user
     * 
     * @param int $id
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function isReadable($id)
    {
        $Fiche = ClassRegistry::init('Fiche');
        $EtatFiche = ClassRegistry::init('EtatFiche');

        $infoFiche = $Fiche->find('first', [
            'conditions' => ['id' => $id],
            'fields' => [
                'id',
                'organisation_id',
                'user_id'
            ]
        ]);
        
        if ($this->isSu()) {
            return true;
        } elseif ($infoFiche['Fiche']['organisation_id'] == $this->Session->read('Organisation.id') && $infoFiche['Fiche']['user_id'] == $this->Session->read('Auth.User.id')) {
            return true;
        } elseif ($infoFiche['Fiche']['organisation_id'] == $this->Session->read('Organisation.id') && $this->Session->read('Auth.User.id') == $this->Session->read('Organisation.dpo')) {
            return true;
        } else {
            $validations = $EtatFiche->find('all', [
                'conditions' => [
                    'fiche_id' => $id,
                    'etat_id' => EtatFiche::ENCOURS_VALIDATION
                ]
            ]);
            
            $consultations = $EtatFiche->find('all', [
                'conditions' => [
                    'fiche_id' => $id,
                    'etat_id' => EtatFiche::DEMANDE_AVIS
                ]
            ]);
            
            foreach ($validations as $key => $value) {
                if ($value['EtatFiche']['user_id'] == $this->Session->read('Auth.User.id') || $value['EtatFiche']['previous_user_id'] == $this->Session->read('Auth.User.id')) {
                    return true;
                }
            }
            
            foreach ($consultations as $key => $value) {
                if ($value['EtatFiche']['user_id'] == $this->Session->read('Auth.User.id') || $value['EtatFiche']['previous_user_id'] == $this->Session->read('Auth.User.id')) {
                    return true;
                }
            }
        }
        
        return false;
    }

    /**
     * Verification si l'user a le droit de modifier une fiche
     * 
     * @param int $id
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function isEditable($id)
    {
        $Fiche = ClassRegistry::init('Fiche');
        $EtatFiche = ClassRegistry::init('EtatFiche');

        $infoFiche = $Fiche->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);

        $infoEtat = $EtatFiche->find('count', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => [
                    EtatFiche::VALIDER_DPO,
                    EtatFiche::ARCHIVER
                ]
            ]
        ]);

        $infoValidateur = $EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => [
                    EtatFiche::ENCOURS_VALIDATION
                ],
                'actif' => true 
            ]
        ]);
        
        if (!empty($infoValidateur)) {
            if ($infoValidateur['EtatFiche']['user_id'] == $this->Session->read('Auth.User.id')) {
                return true;
            }
        }

        $organisation_id = Hash::get($infoFiche, 'Fiche.organisation_id');
        $user_id = Hash::get($infoFiche, 'Fiche.user_id');

        if ($organisation_id == $this->Session->read('Organisation.id') &&
            $user_id == $this->Session->read('Auth.User.id') &&
            $infoEtat < 1
        ) {
            return true;
        }
        
        if ($organisation_id == $this->Session->read('Organisation.id') &&
           ($this->Session->read('Auth.User.id') == $this->Session->read('Organisation.dpo') || $this->isSu())
        ) {
            return true;
        }

        $etatFiche = $EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => EtatFiche::INITIALISATION_TRAITEMENT,
                'actif' => true
            ]
        ]);
        if (!empty($etatFiche) && true === $this->authorized([ListeDroit::INITIALISATION_TRAITEMENT])) {
            return true;
        }

        $etatFiche = $EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => [
                    EtatFiche::ENCOURS_REDACTION,
                    EtatFiche::REPONSE_AVIS,
                    EtatFiche::REPLACER_REDACTION,
                    EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                ],
                'actif' => true
            ]
        ]);
        $servicesUser = $this->WebcilUsers->services('list', [
            'restrict' => true,
            'user' => true,
            'fields' => [
                'Service.id',
                'Service.id'
            ]
        ]);
        if (!empty($etatFiche) &&
            true === $this->authorized([ListeDroit::REDIGER_TRAITEMENT]) &&
            $infoFiche['Fiche']['partage'] === true &&
            in_array($infoFiche['Fiche']['service_id'], $servicesUser) === true
        ) {
            return true;
        }

        $etatFiche = $EtatFiche->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => EtatFiche::DEMANDE_AVIS,
                'actif' => true
            ]
        ]);
        if (!empty($etatFiche) &&
            true === $this->authorized([ListeDroit::MODIFIER_TRAITEMENT_CONSULTATION])
        ) {
            return true;
        }

        return false;
    }

    /**
     * Verification si l'user peut supprimer une fiche
     * 
     * @param int $id
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function isDeletable($id)
    {
        $Fiche = ClassRegistry::init('Fiche');
        $EtatFiche = ClassRegistry::init('EtatFiche');
        
        $infoEtat = $EtatFiche->find('count', [
            'conditions' => [
                'fiche_id' => $id,
                'etat_id' => [
                    EtatFiche::VALIDER_DPO,
                    EtatFiche::ARCHIVER
                ]
            ]
        ]);
        
        $infoFiche = $Fiche->find('first', [
            'conditions' => ['id' => $id]
        ]);
        
        if ($infoFiche['Fiche']['organisation_id'] == $this->Session->read('Organisation.id') && $infoFiche['Fiche']['user_id'] == $this->Session->read('Auth.User.id') && $infoEtat < 1) {
            return true;
        }
        
        return false;
    }

    /**
     * Vérification du super utilisateur
     * 
     * @return boolean
     * 
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function isSu()
    {
        if ($this->Session->read('Su')) {
            return true;
        }
        
        return false;
    }

    /**
     * Vérification de l'organisation du rôle
     * 
     * @param int $id
     * @return boolean
     *
     * @deprecated
     *
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     */
    public function currentOrgaRole($id)
    {
        $Role = ClassRegistry::init('Role');
        $verification = $Role->find('first', [
            'conditions' => ['id' => $id],
            'fields' => 'organisation_id'
        ]);
        
        if ($verification['Role']['organisation_id'] == $this->Session->read('Organisation.id')) {
            return true;
        }
        
        return false;
    }

    public function assertNotSu()
    {
        if ($this->isSu() === true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function assertSu()
    {
        if ($this->isSu() !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function assertNotDpo()
    {
        if ($this->isDpo() === true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function assertDpo()
    {
        if ($this->isDpo() !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function assertLogged()
    {
        if ($this->isLogged() !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    public function assertAuthorized($level)
    {
        if ($this->authorized($level) !== true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * Vérification de l'existence de l'enregistrement.
     *
     * @param $modelClass
     * @param $id
     */
    public function assertRecordExists($modelClass, $id) {
        $model = ClassRegistry::init($modelClass);
        if ($id !== null) {
            $query = [
                'fields' => [$model->primaryKey],
                'contain' => false,
                'conditions' => [
                    "{$model->alias}.{$model->primaryKey}" => $id
                ]
            ];
            $record = $model->find('first', $query);
            if (empty($record) === true) {
                throw new NotFoundException();
            }
        }
    }

    public function assertRecordAuthorized($modelClass, $id, array $params = []) {
        $params += ['superadmin' => false,'dpo' => false];
        $model = ClassRegistry::init($modelClass);

        // Vérification de l'existence de l'enregistrement.
        if ($id !== null) {
            $this->assertRecordExists($modelClass, $id);

            //@info: on ne bypass jamais pour le DPO ?
            $bypass = (
                ($params['superadmin'] === true && $this->isSu() === true)
                || ($params['dpo'] === true && $this->isDpo() === true)
            );
            if ($bypass !== true) {
                if ($modelClass === 'Organisation') {
                    $organisations = [$id];
                } else {
                    if (is_subclass_of($model, 'LinkedOrganisationInterface') === false) {
                        $msgid = 'La classe de modèle %s doit implémenter l\'interface %s pour utiliser la méthode %s';
                        throw new RuntimeException(sprintf($msgid, $modelClass, 'LinkedOrganisationInterface', __METHOD__));
                    }
                    $organisations = $model->getLinkedOrganisationsIds($id);
                }
                if (in_array($this->Session->read('Organisation.id'), $organisations) !== true) {
                    throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
                }
            }
        }
    }

    /**
     * Check si l'organisation en cours à un DPO déclaré
     *
     * @return bool
     *
     * @access public
     *
     * @created 25/02/2020
     * @version V1.1.0
     */
    public function existDPO()
    {
        if ($this->Session->read('Organisation.id') != null) {
            $Organisation = ClassRegistry::init('Organisation');
            $organisationDPO = $Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'Organisation.dpo',
                    'Organisation.numerodpo'
                ]
            ]);

            if (!empty($organisationDPO['Organisation']['dpo'] && !empty($organisationDPO['Organisation']['numerodpo']))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Récupère et renvoie un enregistrement en fonction de l'utilisateur connecté (SU ou utilisateur appartenant à
     * l'entité ayant créé l'enregistrement).
     * L'enregistrement doit être lié à la classe Organisation par une relation hasAndBelongsToMany.
     * Vérifie le cas échéant que celui-ci n'est associé à aucun entité.
     *
     * Utilisé pour vérification avant modification ou suppression d'un sous-traitant.
     *
     * @throws NotFoundException
     * @throws ForbiddenException
     *
     * @todo: à utiliser pour d'autres contrôleurs
     *
     * @param string $modelClass Le nom de la classe de modèle de l'enregistrement
     * @param int $id L'id de l'enregistrement
     * @param bool $checkUnused Doit-on vérifier qu'aucune organisation n'est liée à l'enregistrement ?
     * @return array
     */
    public function getAndCheckLinkedOrganisationsRecord($modelClass, $id, $checkUnused)
    {
        $model = ClassRegistry::init($modelClass);
        $query = [
            'fields' => $model->fields(),
            'contain' => [
                'Organisation' => [
                    'fields' => [
                        'id'
                    ]
                ]
            ],
            'conditions' => [
                "{$model->alias}.{$model->primaryKey}" => $id
            ]
        ];
        $record = $model->find('first', $query);

        if (empty($record) === true) {
            throw new NotFoundException();
        }

        $record['Organisation'] = ['Organisation' => Hash::extract($record, 'Organisation.{n}.id')];

        if ($checkUnused === true && count($record['Organisation']['Organisation']) > 0) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        // A-t-on les droits sur l'enregistrement ?
        if ($this->isSu() === false) {
            $creator = Hash::get($record, "{$model->alias}.createdbyorganisation");
            if ($creator === null || $creator !== $this->Session->read('Organisation.id')) {
                throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
            }
        }

        return $record;
    }
}
