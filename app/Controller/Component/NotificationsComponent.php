<?php

/**
 * NotificationsComponent
 *
 * Component de gestion des notifications
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     Component
 */

class NotificationsComponent extends Component {

    /**
     * @param $content
     * @param $fiche_id
     * @param $user_id
     * @return bool
     * @throws Exception
     *
     * @access public
     *
     * @created 29/04/2015
     * @version V0.9.0
     *
     * @modified 24/09/2020
     * @version V2.1.0
     */
    public function add($content, $fiche_id, $user_id)
    {
        $notification = ClassRegistry::init('Notification');
        $user = ClassRegistry::init('User');

        $userInfo = $user->find('first', [
            'conditions' => [
                'id' => $user_id
            ]
        ]);

        $notification->create([
            'Notification' => [
                'fiche_id' => $fiche_id,
                'content' => $content,
                'user_id' => $user_id,
                'vu' => false
            ]
        ]);

        if ($notification->save(null, ['atomic' => true])) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Envoi un mail de notification si l'utilisateur a autoriser les notifs
     * 
     * @param int $user_id ID de l'utilisateur destinataire du mail
     *
     * @access public
     * @modified  24/09/2020
     * @version V2.0.0
     */
    public function sendEmail($user_id)
    {
        $user = ClassRegistry::init('User');
        
        $userInfo = $user->find('first', [
            'conditions' => [
                'id' => $user_id
            ],
            'fields' => [
                'notification',
                'email',
                'nom_complet'
            ]
        ]);
        
        if ($userInfo['User']['notification'] === true) {
            $mailMessage = sprintf(__d('email', "email.message"), $userInfo['User']['nom_complet'], $_SERVER['HTTP_REFERER']);

            $Email = new CakeEmail();
            $Email->config('email');
            $Email->to($userInfo['User']['email']);
            $Email->subject(__d('email', "email.subject"));
            $Email->send($mailMessage);
        } 
    }

    /**
     * @param type $content
     * @param int $id
     * 
     * @access public
     * @created 29/04/2015
     * @version V0.9.0
     */
    public function del($content, $id)
    {
        $notification = ClassRegistry::init('Notification');
        $notification->deleteAll([
            'content' => $content,
            'fiche_id' => $id
        ]);
    }

}
