<?php

/**
 * JetonsComponent
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v2.1.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     Component
 */

class JetonsComponent extends Component
{
    public $Controller = null;

    public $components = [
        'Session'
    ];

    /**
    * On initialise le modèle Jetonfonction si Configure::write( 'Jetons.disabled' ) n'est pas à true.
    *
    * @param Controller $controller Controller with components to initialize
    * @return void
    */
    public function initialize(Controller $controller)
    {
        $settings = $this->settings;
        parent::initialize($controller, $settings);
        $this->Controller = $controller;

        if (Configure::read('Jetons.disabled')) {
            return;
        }

        $this->Jeton = ClassRegistry::init( 'Jeton');
    }

    public function get($params = array())
    {
        if (Configure::read( 'Jetons.disabled')) {
            return true;
        }

        $params = Hash::merge(['controller' => $this->Controller->name, 'action' => $this->Controller->action], $params);
        $controllerName = $params['controller'];
        $actionName = $params['action'];
        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $this->Jeton->begin();

        $sq = $this->Jeton->sql([
            'alias' => 'jetons',
            'fields' => [
                'jetons.id',
                'jetons.php_sid',
                'jetons.user_id',
                'jetons.modified',
            ],
            'conditions' => [
                'jetons.controller' => $controllerName,
                'jetons.action' => $actionName,
                'jetons.params' => $params,
                // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
                // SELECT même si cacheQueries est à false.
                '( \''.microtime( true ).'\' IS NOT NULL )'
            ],
            'recursive' => -1
        ]);

        $sq = "{$sq} FOR UPDATE";
        $result = @$this->Jeton->query($sq);

        if ($result === false) {
            $this->Jeton->rollback();
            $this->Controller->cakeError('error500');
            return;
        }

        $fonctionNonVerrouillee = (
            empty($result)
            || $result[0]['jetons']['php_sid'] == $this->Session->id()
            || ( strtotime( $result[0]['jetons']['modified'] ) < strtotime( '-'.readTimeout().' seconds' ) )
        );

        if ($fonctionNonVerrouillee) {
            $jetonfonction = [
                'Jeton' => [
                    'controller' => $controllerName,
                    'action' => $actionName,
                    'params' => $params,
                    'php_sid' => $this->Session->id(),
                    'user_id' => $this->Session->read('Auth.User.id')
                ]
            ];

            if (!empty( $result ) && !empty( $result[0]['jetons']['id'])) {
                $jetonfonction['Jetonfonction']['id'] = $result[0]['jetons']['id'];
            }

            $this->Jeton->create($jetonfonction);
            if (!$this->Jeton->save(null, ['atomic' => false])) {
                $this->Jeton->rollback();
                $this->Controller->cakeError( 'error500' );
            }

            $this->Jeton->commit();
        } else {
            $this->Jeton->rollback();

            $lockingUser = $this->Jeton->User->find('first', [
                'conditions' => [
                    'User.id' => $result[0]['jetons']['user_id']
                ],
                'recursive' => -1
            ]);

            throw new LockedActionException(
                'Action verrouillée',
                401,
                [
                    'time' => (strtotime( $result[0]['jetons']['modified'] ) + readTimeout()),
                    'user' => $lockingUser['User']['username']
                ]
            );
        }

        return true;
    }

    /**
     * On relache un (ensemble de) jeton(s).
     *
     * @access public
     *
     * @modified 11/06/2021
     * @version V2.1.0
     *
     * @param mixed $dossiers Un id de dossier ou un array d'ids de dossiers.
     * @return boolean
     */
    public function release($params = array())
    {
        if (Configure::read('Jetons.disabled')) {
            return true;
        }

        $params = Hash::merge(['controller' => $this->Controller->name, 'action' => $this->Controller->action], $params);
        $controllerName = $params['controller'];
        $actionName = $params['action'];
        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $this->Jeton->begin();

        $conditions = [
            'jetons.controller' => $controllerName,
            'jetons.action' => $actionName,
            'jetons.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            '( \''.microtime( true ).'\' IS NOT NULL )'
        ];

        $sq = $this->Jeton->sql([
            'alias' => 'jetons',
            'fields' => [
                'jetons.id',
                'jetons.php_sid',
                'jetons.user_id',
                'jetons.modified'
            ],
            'conditions' => $conditions,
            'recursive' => -1
        ]);

        $sq = "{$sq} FOR UPDATE";

        $results = @$this->Jeton->query($sq);
        if ($results === false) {
            $this->Jeton->rollback();
            die('Erreur étrange');
            return false;
        }

        if ($this->Jeton->deleteAll($conditions, false, false) == false) {
            $this->Jeton->rollback();
            die( 'Erreur étrange' );
            return false;
        }

        $this->Jeton->commit();

        return true;
    }

    /**
     * Retourne une condition concernant l'instant pivot en-dessous duquel les connections sont
     * considérées comme étant expirées.
     *
     * @return string
     */
    protected function _conditionsValid()
    {
        return ['modified >=' => strftime('%Y-%m-%d %H:%M:%S', strtotime('-'.readTimeout().' seconds'))];
    }

    /**
     * Vérifie si une pour un certain contrôleur, une certaine action et d'éventuels paramètres, un autre
     * utilisateur a déjà bloqué l'accès à la fonction.
     *
     * @access private
     *
     * @modified 11/06/2021
     * @version V2.1.0
     *
     * @param mixed $params
     * @return boolean
     */
    private function locked($params = array())
    {
        if (Configure::read( 'Jetons.disabled')) {
            return true;
        }

        $params = Hash::merge(['controller' => $this->Controller->name, 'action' => $this->Controller->action], $params );
//        $controllerName = $params['controller'];
//        $actionName = $params['action'];
        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $conditions = [
//            'jetons.controller' => $controllerName,
//            'jetons.action' => $actionName,
            'jetons.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            '( \''.microtime( true ).'\' IS NOT NULL )'
        ];

        $sq = $this->Jeton->sql([
            'alias' => 'jetons',
            'fields' => [
                'jetons.id',
                'jetons.php_sid',
                'jetons.user_id',
                'jetons.modified',
            ],
            'conditions' => $conditions,
            'recursive' => -1
        ]);

        $results = @$this->Jeton->query($sq);
        if ($results === false) {
            die('Erreur étrange');
            return false;
        }

        $fonctionNonVerrouillee = (
            empty($results)
            || $results[0]['jetons']['php_sid'] == $this->Session->id()
            || ( strtotime( $results[0]['jetons']['modified'] ) < strtotime( '-'.readTimeout().' seconds' ) )
        );

        return $fonctionNonVerrouillee === false;
    }

    /**
     * @access private
     *
     * @modified 11/06/2021
     * @version V2.1.0
     *
     * @param array $params
     * @return array|bool
     */
    private function lockingUser($params = array())
    {
        if (Configure::read( 'Jetons.disabled')) {
            return true;
        }

        $params = Hash::merge(['controller' => $this->Controller->name, 'action' => $this->Controller->action], $params );
//        $controllerName = $params['controller'];
//        $actionName = $params['action'];
        unset($params['controller'], $params['action']);

        $params = serialize(!empty($params) ? (array)$params : null);

        $conditions = [
//            'Jeton.controller' => $controllerName,
//            'Jeton.action' => $actionName,
            'Jeton.params' => $params,
            // INFO: si on get et que l'on release dans la même page, il ne fera pas le second
            // SELECT même si cacheQueries est à false.
            '( \''.microtime( true ).'\' IS NOT NULL )'
        ];

        $query = [
            'fields' => [
                'Jeton.modified',
                $this->Jeton->User->vfNomComplet()
            ],
            'contain' => ['User'],
            'conditions' => $conditions
        ];
        $result = $this->Jeton->find('first', $query);
        if (empty($result) === false) {
            $result['Jeton']['time'] = (strtotime( $result['Jeton']['modified'] ) + readTimeout());
        }

        return $result;
    }

    /**
     * @access public
     *
     * @created  11/06/2021
     * @version V2.1.0
     *
     * @param $fiche_id
     * @return array
     */
    public function takenJeton($fiche_id)
    {
        $return = [
            'success' => true,
            'message' => ''
        ];

        if ($this->locked($fiche_id) === true) {
            $user = $this->lockingUser($fiche_id);

            $message = sprintf(__d('fiche', 'fiche.flashwarningTraitementLocked'),
                $user['User']['nom_complet'],
                strftime('%d/%m/%Y à %H:%M:%S', $user['Jeton']['time'])
            );

            $return = [
                'success' => false,
                'message' => $message
            ];
        }

        return $return;
    }

    /**
     * Called before Controller::redirect().  Allows you to replace the url that will
     * be redirected to with a new url. The return of this method can either be an array or a string.
     *
     * If the return is an array and contains a 'url' key.  You may also supply the following:
     *
     * - `status` The status code for the redirect
     * - `exit` Whether or not the redirect should exit.
     *
     * If your response is a string or an array that does not contain a 'url' key it will
     * be used as the new url to redirect to.
     *
     * @param Controller $controller Controller with components to beforeRedirect
     * @param string|array $url Either the string or url array that is being redirected to.
     * @param integer $status The status code of the redirect
     * @param boolean $exit Will the script exit.
     * @return array|null Either an array or null.
     * @link @link http://book.cakephp.org/2.0/en/controllers/components.html#Component::beforeRedirect
     */
    public function beforeRedirect(Controller $controller, $url, $status = null, $exit = true)
    {
        return [
            'url' => $url,
            'status' => $status,
            'exit' => $exit
        ];
    }
}
