<?php

/**
 * BanettesComponent
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @package     Component
 */

App::uses('Component', 'Controller');
App::uses('Model', 'EtatFiche');

class BanettesComponent extends Component {

    /**
     * Components utilisés par ce component.
     *
     * @var array
     */
    public $components = [
        'Session',
        'WebcilUsers'
    ];

    public $Fiche = null;
    
    /**
     *
     * @param ComponentCollection $collection
     * @param array $settings
     */
    public function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);

        $this->Fiche = ClassRegistry::init('Fiche');
    }
    
    /**
     * 
     * @return array()
     * 
     * @access protected
     * @created 18/07/2017
     * @version V1.0.0
     */
    protected function _query($etatFicheActif = true) {
        $query = [
            'fields' => array_merge(
                $this->Fiche->fields(),
                $this->Fiche->EtatFiche->fields()
            ),
            'joins' => [
                $this->Fiche->join(
                    'EtatFiche',
                    [
                        'type' => 'INNER',
                        'conditions'  => true === $etatFicheActif
                            ? ['EtatFiche.actif' => true]
                            : []
                    ]
                )
            ],
            'contain' => [
                'User' => [
                    'fields' => [
                        'id',
                        'nom',
                        'prenom',
                        $this->Fiche->User->vfNomComplet()
                    ]
                ],
                'Valeur' => [
                    'conditions' => [
                        'champ_name' => [
                            'outilnom',
                            'finaliteprincipale',
//                            'declarantservice'
                        ]
                    ],
                    'fields' => [
                        'champ_name',
                        'valeur'
                    ]
                ],
                'Norme' => [
                    'fields' => [
                        'id',
                        'norme',
                        'numero',
                        'libelle',
                        'description',
//                        'name_fichier',
//                        'fichier'
                    ]
                ],
                'Referentiel' => [
                    'fields' => [
                        'id',
                        'name'
                    ],
                ],
                'Service' => [
                    'fields' => [
                        'libelle'
                    ]
                ]
            ],
            'conditions' => [
                'Fiche.organisation_id' => $this->Session->read('Organisation.id')
            ],
            'order' => [
                'Fiche.created' => 'DESC',
                'Fiche.id' => 'DESC'
            ]
        ];
                
        return $query;
    }

    /**
     * On ne veut pas voir apparaître les traitements envoyés dans un ou plusieurs
     * états (ex. EtatFiche::DEMANDE_AVIS) et qui ont également changé d'état
     * (plusieurs états actifs).
     * 
     * @param integer|array $etats
     * @return string
     */
    protected function _queryConditionNotEtatsParalleles($etats) {
        $subquery = [
            'alias' => 'etats_fiches',
            'fields' => ['etats_fiches.fiche_id'],
            'conditions' => [
                'etats_fiches.fiche_id = EtatFiche.fiche_id',
                'NOT' => [
                    'etats_fiches.etat_id' => $etats
                ],
                'etats_fiches.actif' => true
            ]
        ];
        $sql = $this->Fiche->EtatFiche->sql($subquery);

        return "EtatFiche.fiche_id NOT IN ( {$sql} )";
    }

    /**
     * Traitement en cours de rédaction
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryEncoursRedaction() {
        $query = $this->_query();

        $query['conditions'] += [
            [
                'OR' => [
                    [
                        'EtatFiche.etat_id' => [
                            EtatFiche::ENCOURS_REDACTION,
                            EtatFiche::REPONSE_AVIS,
                            EtatFiche::REPLACER_REDACTION,
                            EtatFiche::TRAITEMENT_INITIALISE_RECU,
                            EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                        ],
                        'EtatFiche.actif' => true,
                        'EtatFiche.user_id' => $this->Session->read('Auth.User.id')
                    ],
                    [
                        'EtatFiche.etat_id' => EtatFiche::DEMANDE_AVIS,
                        'EtatFiche.actif' => true,
                        'EtatFiche.previous_user_id' => $this->Session->read('Auth.User.id'),
                        $this->_queryConditionNotEtatsParalleles(EtatFiche::DEMANDE_AVIS)
                    ]
                ]
            ]
        ];

        return $query;          
    }

    public function queryPartageServices() {
        $query = $this->_query();

        $servicesUser = $this->WebcilUsers->services('list', [
            'restrict' => true,
            'user' => true,
            'fields' => [
                'Service.id',
                'Service.id'
            ]
        ]);

        $query['conditions'] += [
            [
                'OR' => [
                    [
                        'EtatFiche.etat_id' => [
                            EtatFiche::ENCOURS_REDACTION,
                            EtatFiche::REPONSE_AVIS,
                            EtatFiche::REPLACER_REDACTION,
                            EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                        ],
                        'EtatFiche.actif' => true
                    ],
                    [
                        'EtatFiche.etat_id' => EtatFiche::DEMANDE_AVIS,
                        'EtatFiche.actif' => true,
                        $this->_queryConditionNotEtatsParalleles(EtatFiche::DEMANDE_AVIS)
                    ]
                ]
            ],
            'Fiche.user_id !=' => $this->Session->read('Auth.User.id'),
            'Fiche.service_id' => $servicesUser,
            'Fiche.partage' => true
        ];

        return $query;
    }

    /**
     * Traitement en attente de validation
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryAttente() {
        $query = $this->_query();

        $query['conditions'] += [
            'Fiche.user_id' => $this->Session->read('Auth.User.id'),
            'EtatFiche.etat_id' => EtatFiche::ENCOURS_VALIDATION
        ];

        return $query;          
    }

    /**
     * Traitements reçus pour validation
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryRecuValidation() {
        $query = $this->_query();
        
        $query['conditions'] += [
            [
                'OR' => [
                    [
                        'EtatFiche.etat_id' => EtatFiche::ENCOURS_VALIDATION,
                        'EtatFiche.actif' => true,
                        'EtatFiche.user_id' => $this->Session->read('Auth.User.id')
                    ],
//                    [
//                        'EtatFiche.etat_id' => EtatFiche::DEMANDE_AVIS,
//                        'EtatFiche.actif' => true,
//                        'EtatFiche.previous_user_id' => $this->Session->read('Auth.User.id'),
//                        $this->_queryConditionNotEtatsParalleles(EtatFiche::DEMANDE_AVIS)
//                    ]
                ]
            ]
        ];
        
        return $query;          
    }
    
    /**
     * Traitement refusées
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryRefuser() {
        $query = $this->_query();

        $query['conditions'] += [
            'Fiche.user_id' => $this->Session->read('Auth.User.id'),
            'EtatFiche.etat_id' => EtatFiche::REFUSER
        ];

        return $query;          
    }
    
    /**
     * Traitements reçus pour consultation
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryRecuConsultation() {
        $query = $this->_query(false);

        $query['conditions'] += [
            'EtatFiche.user_id' => $this->Session->read('Auth.User.id'),
            'EtatFiche.etat_id' => EtatFiche::DEMANDE_AVIS,
            'EtatFiche.actif' => true
        ];

        return $query;
    }
    
    /**
     * Traitement validés et insérés au registre ou archivé au registre
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryArchives() {
        $query = $this->_query();

        $query['conditions'] += [
            'EtatFiche.etat_id' => [
                EtatFiche::VALIDER_DPO,
                EtatFiche::ARCHIVER,
                EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE
            ],
            'EtatFiche.actif' => true,
            'Fiche.user_id' => $this->Session->read('Auth.User.id')
        ];

        return $query;
    }
    
    /**
     * Etat des traitements passés en ma possession
     * 
     * @return array()
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function queryConsulte() {
        $query = $this->_query();

        $subQuery = [
            'alias' => 'etats_fiches',
            'fields' => ['etats_fiches.id'],
            'conditions' => [
                'etats_fiches.fiche_id = Fiche.id',
                'etats_fiches.actif' => false,
                'etats_fiches.user_id' => $this->Session->read('Auth.User.id'),
                //$this->_queryConditionNotEtatsParalleles(EtatFiche::DEMANDE_AVIS)
            ]
        ];
        $sql = $this->Fiche->EtatFiche->sql($subQuery);

        $query['conditions'] += [
            'EtatFiche.user_id <>' => $this->Session->read('Auth.User.id'),
            "EXISTS( {$sql} )"
        ];
            
        return $query;
    }

    /**
     * Traitement initialisé par le DPO
     *
     * @return array()
     *
     * @access public
     * @created 10/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function queryInitialisationTraitement() {
        $query = $this->_query();

        $query['conditions'] += [
            [
                'OR' => [
                    [
                        'EtatFiche.etat_id' => [
                            EtatFiche::INITIALISATION_TRAITEMENT
                        ],
                        'EtatFiche.actif' => true,
//                        'EtatFiche.user_id' => $this->Session->read('Auth.User.id')
                    ],
                    [
                        'EtatFiche.etat_id' => EtatFiche::DEMANDE_AVIS,
                        'EtatFiche.actif' => true,
                        'EtatFiche.previous_user_id' => $this->Session->read('Auth.User.id'),
                        $this->_queryConditionNotEtatsParalleles(EtatFiche::DEMANDE_AVIS)
                    ]
                ]
            ]
        ];

        return $query;
    }

    /**
     * Tous les traitements en cours dans l'entité
     *
     * @return array()
     *
     * @access public
     * @created 10/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function queryAllTraitements() {
        $query = $this->_query();

        $query['conditions'] += [
            'EtatFiche.etat_id !=' => [
                EtatFiche::VALIDER_DPO,
                EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE,
                EtatFiche::ARCHIVER
            ],
            'EtatFiche.actif' => true
        ];

        return $query;
    }
}
