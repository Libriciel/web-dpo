<?php

/**
 * UsersController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');
App::uses('Folder', 'Utility');

use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

class UsersController extends AppController {

    public $uses = [
        'Admin',
        'AuthComponent',
        'Authentification',
        'ConnecteurLdap',
        'Droit',
        'ListeDroit',
        'Organisation',
        'OrganisationUser',
        'OrganisationUserService',
        'OrganisationUserRole',
        'Role',
        'RoleDroit',
        'Service',
        'User'
    ];

    public $helpers = [
        'Controls'
    ];

    public $components = [
        'RequestHandler'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $listeDroitIndex = [ListeDroit::CREER_UTILISATEUR, ListeDroit::MODIFIER_UTILISATEUR, ListeDroit::SUPPRIMER_UTILISATEUR];
        $noLoginRequired = ['ajax_password', 'cas_login', 'cas_logout', 'login', 'logout'];
        $anyLoggedInUser = ['changepassword', 'preferences'];

        if ($this->Droits->isSu() !== true) {
            if ($action === 'add') {
                $this->Droits->assertAuthorized([ListeDroit::CREER_UTILISATEUR]);
            } elseif ($action === 'edit') {
                $this->Droits->assertAuthorized([ListeDroit::MODIFIER_UTILISATEUR]);
            } elseif ($action === 'index') {
                $this->Droits->assertAuthorized($listeDroitIndex);
            } elseif ($action === 'delete') {
                $this->Droits->assertAuthorized([ListeDroit::SUPPRIMER_UTILISATEUR]);
            } elseif ($action === 'view') {
                $this->Droits->assertAuthorized($listeDroitIndex);
            } elseif (in_array($action, $noLoginRequired) === false && in_array($action, $anyLoggedInUser) === false) {
                throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
            }
        }
    }

    /**
     * Liste des utilisateurs (Liste des utilisateurs de l'application)
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @edit 05/03/2019
     * @version V1.0.2
     */
    public function index()
    {
        $title = 'index' === $this->request->params['action']
            ? __d('user', 'user.titreListeUser')
            : __d('user', 'user.titreUsersApplication');
        $this->set(compact('title'));

        $conditions = [];
        if ('admin_index' !== $this->request->params['action']) {
            $conditions = [
                'OrganisationUser.organisation_id' => $this->Session->read('Organisation.id')
            ];
        } else {
            $conditions = [
                'OrganisationUser.organisation_id !=' => null
            ];
        }

        $query = [
            'fields' => [
                'User.id',
                'User.is_dpo',
                'User.username',
                'User.nom_complet'
            ],
            'joins' => [
                $this->User->join('OrganisationUser', ['type' => 'LEFT OUTER'])
            ],
            'contain' => [
                'Organisation' => [
                    'fields' => [
                        'Organisation.raisonsociale',
                        '("OrganisationUser"."id" IS NOT NULL AND "Organisation"."dpo" IS NOT NULL AND "Organisation"."dpo" = "OrganisationUser"."user_id") AS "OrganisationUser__is_dpo"',
                    ],
                    'order' => ['Organisation.raisonsociale ASC']
                ]
            ],
            'conditions' => $conditions,
            'group' => $this->User->fields(),
            'order' => 'User.nom_complet_court ASC',
            'limit' => 20
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        // Conditions venant implicitement de l'action, de l'utilisateur connecté et des filtres
        if ($this->request->is('post') || false === empty($search)) {
            // Filtre par entité
            $organisation_id = (string)Hash::get($this->request->data, 'users.organisation');
            if ('' !== $organisation_id) {
                $query['conditions']['OrganisationUser.organisation_id'] = $organisation_id;
            }

            // Filtre par DPO
            $dpo = (string)Hash::get($this->request->data, 'users.dpo');
            if ('' !== $dpo) {
                $subQuery = [
                    'alias' => 'organisations',
                    'fields' => ['organisations.id'],
                    'conditions' => [
                        'organisations.dpo = User.id',
                        'organisations.id' => array_keys(
                            $this->WebcilUsers->organisations(
                                'list',
                                ['restrict' => 'index' === $this->request->params['action']]
                            )
                        )
                    ]
                ];
                $sql = $this->User->Organisation->sql($subQuery);
                $query['conditions'][] = '1' === $dpo ? "EXISTS ({$sql})" : "NOT EXISTS ({$sql})";
            }

            // Filtre par utilisateur
            $user_id = (string)Hash::get($this->request->data, 'users.nom');
            if ('' !== $user_id) {
                $query['conditions']['User.id'] = $user_id;
            }

            // Filtre par identifiant
            $username = trim((string)Hash::get($this->request->data, 'users.username'));
            if ('' !== $username) {
                $query['conditions']['UPPER(User.username) LIKE'] = mb_convert_case(str_replace('*', '%', $username), MB_CASE_UPPER);
            }

            // Filtre par service
            $service = (string)Hash::get($this->request->data, 'users.service');
            if ('' !== $service) {
                $subQuery = [
                    'alias' => 'organisation_user_services',
                    'fields' => ['organisation_user_services.organisation_user_id'],
                    'joins' => [
                        words_replace(
                            $this->User->OrganisationUser->OrganisationUserService->join('Service', ['type' => 'INNER']),
                            ['Service' => 'services', 'OrganisationUserService' => 'organisation_user_services']
                        )
                    ],
                    'conditions' => [
                        'services.libelle' => $service
                    ]
                ];
                $sql = $this->User->OrganisationUser->OrganisationUserService->sql($subQuery);
                $query['conditions'][] = "OrganisationUser.id IN ({$sql})";
            }

            // Filtre par profil
            $profil = (string)Hash::get($this->request->data, 'users.profil');
            if ('' !== $profil) {
                $subQuery = [
                    'alias' => 'organisation_user_roles',
                    'fields' => ['organisation_user_roles.organisation_user_id'],
                    'joins' => [
                        words_replace(
                            $this->User->OrganisationUser->OrganisationUserRole->join('Role', ['type' => 'INNER']),
                            ['Role' => 'roles', 'OrganisationUserRole' => 'organisation_user_roles']
                        )
                    ],
                    'conditions' => [
                        'roles.libelle' => $profil
                    ]
                ];
                $sql = $this->User->OrganisationUser->OrganisationUserRole->sql($subQuery);
                $query['conditions'][] = "OrganisationUser.id IN ({$sql})";
            }
        }

        $this->paginate = $query;
        $results = $this->paginate('User');

        foreach($results as $resultIdx => $result) {
            if (true === Hash::check($result, 'Organisation.{n}.OrganisationUser')) {
                foreach($result['Organisation'] as $orgIdx => $organisation) {
                    // Roles
                    $query = [
                        'fields' => [
                            'Role.libelle'
                        ],
                        'contain' => [
                            'Role'
                        ],
                        'conditions' => [
                            'OrganisationUserRole.organisation_user_id' => $organisation['OrganisationUser']['id']
                        ],
                        'order' => ['Role.libelle']
                    ];
                    $role = $this->User->OrganisationUser->OrganisationUserRole->find('first', $query);
                    $results[$resultIdx]['Organisation'][$orgIdx]['OrganisationUser']['Role'] = Hash::get($role, 'Role');

                    // Services
                    $query = [
                        'fields' => [
                            'Service.libelle'
                        ],
                        'contain' => [
                            'Service'
                        ],
                        'conditions' => [
                            'OrganisationUserService.organisation_user_id' => $organisation['OrganisationUser']['id']
                        ],
                        'order' => ['Service.libelle']
                    ];
                    $services = $this->User->OrganisationUser->OrganisationUserService->find('all', $query);
                    $results[$resultIdx]['Organisation'][$orgIdx]['OrganisationUser']['Service'] = Hash::extract($services, '{n}.Service');
                }
            }
        }

        // Possède-t-on au moins un service ?
        $hasService = [] !== $this->Service->find('first', ['fields' => ['id']]);

        // Options
        $restrict = 'index' === $this->request->params['action'] ? true : false;
        $options = [
            'organisations' => $this->WebcilUsers->organisations('list', ['restrict' => $restrict]),
            'roles' => $this->WebcilUsers->roles('list', ['restrict' => $restrict]),
            'services' => $this->WebcilUsers->services('list', ['restrict' => $restrict]),
            'users' => $this->WebcilUsers->users('list', ['restrict' => $restrict]),
            'dpo' => [0 => 'Non', 1 => 'Oui']
        ];

        $this->set(compact('results', 'hasService', 'options'));
        $this->view = 'index';
    }

    /**
     * Affiche les informations sur un utilisateur
     *
     * @param int $id
     * @throws NotFoundException
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function view($id) {
        $this->Droits->assertRecordAuthorized('User', $id, ['superadmin' => true]);

        $this->set('title', 'Voir l\'utilisateur');
        $this->set('user', $this->User->read(null, $id));
    }

    /**
     * Affiche le formulaire d'ajout d'utilisateur, ou enregistre l'utilisateur et ses droits
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function add() {
        return $this->edit();
    }

    /**
     * Modification d'un utilisateur en tant qu'administrateur
     *
     * @param int $id
     * @throws NotFoundException
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @edit 12/03/2019
     * @version V1.0.2
     */
    public function edit($id = null)
    {
        $this->Droits->assertRecordAuthorized('User', $id, ['superadmin' => true]);
        $edit = false;

        $title = 'add' === $this->request->params['action']
            ? __d('user', 'user.titreAjouterUser')
            : __d('user', 'user.titreEditerUser');
        $this->set(compact('title'));

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            // Tentative de sauvegarde
            $this->User->begin();
            $success = true;

            $data = $this->request->data;

            // Travail préparatoire dans le cas d'une modification
            if ($this->request->params['action'] === 'edit') {
                $data['User']['id'] = $id;

                if (isset($data['User']['password'])) {
                    $password = (string)Hash::get($data, 'User.password');
                    $passwd = (string)Hash::get($data, 'User.passwd');

                    if ('' === $password && $password === $passwd) {
                        unset($data['User']['password'], $data['User']['passwd']);
                    }
                } else {
                    $this->request->data['User']['ldap'] = true;

                    $data['User']['ldap'] = true;
                    unset($data['User']['username']);
                }

                // On n'opère que sur les organisations que je peux voir
                $conditions = [
                    'user_id' => $id,
                    'organisation_id' => array_keys($mesOrganisations)
                ];
                $success = $this->User->OrganisationUser->deleteAll($conditions) && $success;
            }

            $this->User->create($data);
            $success = $this->User->save(null, ['atomic' => false]) && $success;

            // Validation spéciale d'une "faussse" HABTM
            $tmp = array_filter((array)Hash::get($data, 'User.organisation_id'));
            if(true === empty($tmp)) {
                $this->User->invalidate('organisation_id', __d('database', 'Validate::notBlank'));
                $success = false;
            }

            if ($success === true) {
                // On n'opère que sur les organisations que je peux voir
                $organisations = array_intersect(
                    array_filter((array)Hash::get($data, 'User.organisation_id')),
                    array_keys($mesOrganisations)
                );

                foreach($organisations as $organisation_id) {
                    // Organisation
                    $record = [
                        'OrganisationUser' => [
                            'organisation_id' => $organisation_id,
                            'user_id' => $this->User->id
                        ]
                    ];

                    $this->User->OrganisationUser->create($record);
                    $success = $this->User->OrganisationUser->save(null, ['atomic' => false]) && $success;

                    // Services
                    $services = array_filter((array)Hash::get($data, "User.{$organisation_id}.service_id"));
                    foreach($services as $service_id) {
                        $record = [
                            'OrganisationUserService' => [
                                'organisation_user_id' => $this->User->OrganisationUser->id,
                                'service_id' => $service_id
                            ]
                        ];
                        $this->User->OrganisationUser->OrganisationUserService->create($record);
                        $success = $this->User->OrganisationUser->OrganisationUserService->save(null, ['atomic' => false]) && $success;
                    }

                    // Role
                    $role_id = Hash::get($data, "User.{$organisation_id}.role_id");
                    $record = [
                        'OrganisationUserRole' => [
                            'organisation_user_id' => $this->User->OrganisationUser->id,
                            'role_id' => $role_id
                        ]
                    ];

                    if(true === empty($role_id)) {
                        $success = false;
                    }
                    $this->User->OrganisationUser->OrganisationUserRole->create($record);
                    $success = $this->User->OrganisationUser->OrganisationUserRole->save(null, ['atomic' => false]) && $success;

                    // Droits
                    $query = [
                        'conditions' => [
                            'RoleDroit.role_id' => $role_id
                        ]
                    ];
                    $droits = $this->RoleDroit->find('all', $query);

                    foreach($droits as $droit) {
                        $record = [
                            'Droit' => [
                                'organisation_user_id' => $this->User->OrganisationUser->id,
                                'liste_droit_id' => Hash::get($droit, 'RoleDroit.liste_droit_id')
                            ]
                        ];

                        $this->Droit->create($record);
                        $success = false !== $this->Droit->save(null, ['atomic' => false]) && $success;
                    }
                }
            }

            if ($success === true) {
                $this->User->commit();
                $this->Session->setFlash(__d('user', 'user.flashsuccessUserEnregistrer'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                $this->User->rollback();
                $this->Session->setFlash(__d('user', 'user.flasherrorErreurEnregistrementUser'), 'flasherror');
            }
        } else if('edit' === $this->request->params['action']) {
            $query = [
                'conditions' => [
                    'User.id' => $id
                ]
            ];
            $user = $this->User->find('first', $query);

            if(true === empty($user)) {
                throw new NotFoundException();
            }
            unset($user['User']['password']);
            $user['User']['organisation_id'] = [];

            $query = [
                'contain' => [
                    'OrganisationUserRole',
                    'OrganisationUserService'
                ],
                'conditions' => [
                    'OrganisationUser.user_id' => $id
                ]
            ];
            $records = $this->User->OrganisationUser->find('all', $query);

            foreach($records as $record) {
                $organisation_id = Hash::get($record, 'OrganisationUser.organisation_id');
                $user['User']['organisation_id'][] = $organisation_id;
                if(false === isset($user['User'][$organisation_id]['service_id'])) {
                    $user['User'][$organisation_id]['service_id'] = [];
                }
                $user['User'][$organisation_id]['service_id'][] = Hash::get($record, 'OrganisationUserService.service_id');
                if(false === isset($user['User'][$organisation_id]['role_id'])) {
                    $user['User'][$organisation_id]['role_id'] = [];
                }
                $user['User'][$organisation_id]['role_id'] = array_merge(
                    $user['User'][$organisation_id]['role_id'],
                    Hash::extract($record, 'OrganisationUserRole.{n}.role_id')
                );
            }

            $this->request->data = $user;

            $edit = true;
        }

        $forcePassword = $this->User->getMinUserPasswordStrength($id);//@fixme: si aucune entité sélectionnée -> 0
        $this->set(compact('forcePassword'));

        $minEntropie = $this->_entropiePassword($id);
        $this->set(compact('minEntropie'));

        $exists = [];
        foreach (ListeDroit::LISTE_DROITS_DPO_MINIMUM as $value) {
            $exists[] = sprintf('EXISTS(SELECT * FROM role_droits INNER JOIN liste_droits ON ( role_droits.liste_droit_id = liste_droits.id ) WHERE liste_droits.value = %d AND role_droits.role_id = Role.id)', $value);
        }

        $roleDPO_id = $this->Role->find('all', [
            'fields' => [
                'Role.id'
            ],
            'conditions' => [
                $exists
            ]
        ]);
        $rolesDPO_id = Hash::extract($roleDPO_id, '{n}.Role.id');

        if ($edit === true) {
            $rolesUser_id = Hash::extract($records, '{n}.OrganisationUserRole.{n}.role_id');

            $conditionsProfilsUser = [
                'Role.id !=' => array_diff($rolesDPO_id, $rolesUser_id)
            ];
        } else {
            $conditionsProfilsUser = [
//                'Role.id <>' => $rolesDPO_id
                'Role.id !=' => $rolesDPO_id
            ];
        }

        $options = array_merge(
            $this->User->enums(),
            [
                'organisation_id' => $mesOrganisations,
                'service_id' => $this->WebcilUsers->services('list', [
                    'fields' => [
                        'id',
                        'libelle',
                        'organisation_id'
                    ]
                ]),
                'role_id' => $this->WebcilUsers->roles('list', [
                    'fields' => [
                        'id',
                        'libelle',
                        'organisation_id'
                    ],
                    'conditions' => $conditionsProfilsUser
                ]),
                'force' => $this->Organisation->find('list', [
                    'conditions' => [
                        'Organisation.id' => array_keys($mesOrganisations)
                    ],
                    'fields' => [
                        'Organisation.id',
                        'Organisation.force'
                    ]
                ])
            ]
        );

        $this->set(compact('options', 'rolesDPO_id'));
        $this->view = 'edit';
    }

    /**
     * Modification du mot de passe par un utilisateur connecté
     *
     * @access public
     * @created 03/02/2016
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @edit 10/04/2019
     * @version V1.0.2
     */
    public function changepassword() {
        if ($this->Auth->user('ldap') === true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->set('title', __d('user', 'user.titreModificationPassword'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $user = $this->User->find('first', [
                'conditions' => [
                    'id' => $this->Auth->user('id')
                ],
                'fields' => [
                    'id',
                    'ldap',
                    'password'
                ]
            ]);

            // Tentative de sauvegarde
            $this->User->begin();
            $success = true;
            $data = $this->request->data;

            $query = [
                'conditions' => [
                    'OrganisationUser.user_id' => $this->Auth->user('id')
                ],
                'fields' => [
                    'organisation_id'
                ]
            ];
            $organisationsUser = $this->User->OrganisationUser->find('all', $query);

            $data['User'] += [
                'organisation_id' => Hash::extract($organisationsUser, '{n}.OrganisationUser.organisation_id')
            ];

            $passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);
            if ($passwordHasher->hash($data['User']['old_password']) != $user['User']['password']) {
                $success = false;
                $this->User->invalidate('old_password', 'Le mot de passe actuel n\'est pas correct');
            }

            if ($success === true) {
                $data['User']['id'] = $this->Auth->user('id');
                $this->User->create($data);
                $success = false !== $this->User->save(null, ['atomic' => false]);
            }

            if ($success == true) {
                $this->User->commit();
//                $this->Session->setFlash(__d('user', 'user.flashsuccessUserEnregistrerReconnecter'), "flashsuccess");
                $this->Session->write('successMessage', __d('user', 'user.flashsuccessUserEnregistrerReconnecter'));

                $this->redirect([
                    'controller' => 'users',
                    'action' => 'logout'
                ]);
            } else {
                $this->User->rollback();
                $this->Session->setFlash(__d('user', 'user.flasherrorErreurEnregistrementUser'), "flasherror");
            }
        }

        $forcePassword = $this->User->getMinUserPasswordStrength($this->Auth->user('id'));
        $this->set(compact('forcePassword'));

        $minEntropie = $this->_entropiePassword($this->Auth->user('id'));
        $this->set(compact('minEntropie'));
    }

    private function _entropiePassword($id) {
        $forcePassword = $this->User->getMinUserPasswordStrength($id);

        $thresholds = array_flip(PasswordStrengthMeterAnssi::thresholds());
        return $thresholds[$forcePassword];
    }

    /**
     * Suppression d'un utilisateur
     *
     * @param int|null $id
     * @throws NotFoundException
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function delete($id) {
        $this->Droits->assertRecordAuthorized('User', $id, ['superadmin' => true]);

        // La même logique que dans la vue et que dans AdminsController
        if ($id == 1 || $id == $this->Session->read('Auth.User.id')) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $query = [
            'fields' => [
                'Admin.id',
                'User.id',
                'User.nom',
                'User.prenom',
                'User.is_dpo',
            ],
            'contain' => [
                'Admin'
            ],
            'conditions' => [
                'User.id' => $id
            ]
        ];
        $user = $this->User->find('first',  $query);

        if (empty($user) === true) {
            throw new NotFoundException();
        }

        // Impossible de supprimer un Superadmin ou un DPO
        if (empty($user['Admin']['id']) === false || $user['User']['is_dpo'] === true) {
            $msgid = 'Impossible de supprimer un Superadmin ou un DPO';
            throw new RuntimeException(sprintf($msgid, $id));
        }

        $success = true;
        $this->User->begin();

        // 1. Mise à jour de EtatFiche.user_id, puis de EtatFiche.previous_user_id avec l'id du DPO de l'entité
        foreach (['user_id', 'previous_user_id'] as $fieldName) {
            $query = [
                'fields' => [
                    'EtatFiche.id',
                    'Organisation.dpo',
                ],
                'joins' => [
                    $this->User->Fiche->join('EtatFiche', ['type' => 'INNER']),
                    $this->User->Fiche->join('Organisation', ['type' => 'INNER'])
                ],
                'conditions' => [
                    "EtatFiche.{$fieldName}" => $id,
                ],
            ];
            foreach ($this->User->Fiche->find('all', $query) as $fiche) {
                $success = $success && $this->User->Fiche->EtatFiche->updateAll(
                        [$fieldName => $fiche['Organisation']['dpo']],
                        ['id' => $fiche['EtatFiche']['id']]
                    );
            }
        }

        // 2. Mise à jour de Fiche.user_id avec l'id du DPO de l'entité
        $query = [
            'fields' => [
                'Fiche.id',
                'Organisation.dpo',
            ],
            'joins' => [
                $this->User->Fiche->join('Organisation', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Fiche.user_id' => $id,
            ],
        ];
        foreach ($this->User->Fiche->find('all', $query) as $fiche) {
            $success = $success && $this->User->Fiche->updateAll(
                    ['user_id' => $fiche['Organisation']['dpo']],
                    ['id' => $fiche['Fiche']['id']]
                );
        }

        $success = $success && $this->User->Fiche->EtatFiche->Commentaire->updateAll(
                ['user_id' => null],
                ['user_id' => $id]
            );

        $success = $success && $this->User->Fiche->EtatFiche->Commentaire->updateAll(
                ['destinataire_id' => null],
                ['destinataire_id' => $id]
            );

        $success = $success && $this->User->Notification->deleteAll(['user_id' => $id], false);

        $success = $success && $this->User->delete($id);

        if ($success == true) {
            $this->User->commit();
            $this->Session->setFlash(__d('user', 'user.flashsuccessUserSupprimer'), 'flashsuccess');
        } else {
            $this->User->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @access public
     * @return type
     */
    public function casLogin()
    {
        if ($this->Auth->login()) {
            return $this->_login();
        }

        return $this->redirect(['admin' => false, 'prefix' => false, 'controller' => 'user', 'action' => 'login']);
    }

    /**
     * Page de login
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @edit 17/10/2019
     * @version V1.1.0
     */
    public function login()
    {
//        $hashpass = AuthComponent::password("theog");
//        debug($hashpass);
//        die;

        $this->layout = 'login';
        $error = false;
        $successMessage = '';

        if (!empty($this->Session->read('successMessage'))) {
            $successMessage = $this->Session->read('successMessage');
            $this->Session->delete('successMessage');
        }

        if ($this->request->is('post')) {
            $autorisationLogin = false;

            if (Configure::read('MODE_CASE_SENSITIVE') === true) {
                $queryCondition = [
                    'username' => $this->request->data('User.username')
                ];
            } else {
                $queryCondition = [
                    'LOWER(username)' => strtolower($this->request->data('User.username'))
                ];
            }

            $idUser = $this->User->find('first', [
                'conditions' => $queryCondition,
                'fields' => [
                    'id',
                    'ldap',
                    'password',
                    'username'
                ]
            ]);

            if (!empty($idUser)) {
                if ($idUser['User']['ldap'] == true) {
                    $query = [
                        'fields' => [
                            'ConnecteurLdap.password',
                            'ConnecteurLdap.login',
                            'ConnecteurLdap.host',
                            'ConnecteurLdap.port',
                            'ConnecteurLdap.use',
                            'ConnecteurLdap.type',
                            'ConnecteurLdap.host_fall_over',
                            'ConnecteurLdap.basedn',
                            'ConnecteurLdap.tls',
                            'ConnecteurLdap.version',
                            'ConnecteurLdap.account_suffix',
                            'ConnecteurLdap.certificat_url',
                            'ConnecteurLdap.certificat_name',
                            'ConnecteurLdap.username',
                            'ConnecteurLdap.note',
                            'ConnecteurLdap.nom',
                            'ConnecteurLdap.prenom',
                            'ConnecteurLdap.email',
                            'ConnecteurLdap.telfixe',
                            'ConnecteurLdap.telmobile',
                            'ConnecteurLdap.active',
                            'Authentification.port',
                            'Authentification.use',
                            'Authentification.type',
                            'Authentification.serveur',
                            'Authentification.uri',
                            'Authentification.name_fichier'
                        ],
                        'joins' => [
                            $this->OrganisationUser->join('Organisation', ['type' => 'INNER']),
                            $this->OrganisationUser->Organisation->join('ConnecteurLdap', ['type' => 'INNER']),
                            $this->OrganisationUser->Organisation->join('Authentification', ['type' => 'INNER']),
                        ],
                        'conditions' => [
                            'OrganisationUser.user_id' => $idUser['User']['id'],
                            'ConnecteurLdap.use' => true,
                            'Authentification.use' => true
                        ]
                    ];
                    $infoUser = $this->OrganisationUser->find('all', $query);

                    if (!empty($infoUser)) {
                        foreach ($infoUser as $connecteurLdap) {
                            if ($autorisationLogin === false) {
                                $connecteurLdap['ConnecteurLdap']['fields']['User'] = [
                                    'username' => $connecteurLdap['ConnecteurLdap']['username'],
                                    'note' => $connecteurLdap['ConnecteurLdap']['note'],
                                    'nom' => $connecteurLdap['ConnecteurLdap']['nom'],
                                    'prenom' => $connecteurLdap['ConnecteurLdap']['prenom'],
                                    'email' => $connecteurLdap['ConnecteurLdap']['email'],
                                    'telfixe' => $connecteurLdap['ConnecteurLdap']['telfixe'],
                                    'telmobile' => $connecteurLdap['ConnecteurLdap']['telmobile'],
                                    'active' => $connecteurLdap['ConnecteurLdap']['active']
                                ];

                                unset ($connecteurLdap['ConnecteurLdap']['username']);
                                unset ($connecteurLdap['ConnecteurLdap']['note']);
                                unset ($connecteurLdap['ConnecteurLdap']['nom']);
                                unset ($connecteurLdap['ConnecteurLdap']['prenom']);
                                unset ($connecteurLdap['ConnecteurLdap']['email']);
                                unset ($connecteurLdap['ConnecteurLdap']['telfixe']);
                                unset ($connecteurLdap['ConnecteurLdap']['telmobile']);
                                unset ($connecteurLdap['ConnecteurLdap']['active']);

                                Configure::write('LdapManager.Ldap', $connecteurLdap['ConnecteurLdap']);

                                if (!empty($connecteurLdap['ConnecteurLdap']['certificat_url'])) {
                                    Configure::write('LdapManager.Ldap.certificat', CHEMIN_CERTIFICATS . $connecteurLdap['ConnecteurLdap']['certificat_url']);
                                }

                                Configure::write('AuthManager.Authentification', $connecteurLdap['Authentification']);

                                $autorisationLogin = $this->Auth->login();
                            }
                        }
                    }
                } else {
                    if (Configure::read('MODE_CASE_SENSITIVE') === false) {
                        $this->request->data['User']['username'] = $idUser['User']['username'];
                    }

                    $autorisationLogin = $this->Auth->login();

                    // si la connexion à echoué on test avec l'ancien système de connexion (sha1) et si c'est bon MAJ du hash
                    if ($autorisationLogin === false) {
                        $oldHash = Security::hash($this->request->data['User']['password'], 'sha1', OLD_SALT);

                        if ($oldHash === $idUser['User']['password']) {
                            $this->User->id = $idUser['User']['id'];
                            $data = [
                                'User' => [
                                    'id' => $idUser['User']['id'],
                                    'password' => $this->request->data['User']['password']
                                ]
                            ];
                            $this->User->create($data);
                            $success = false !== $this->User->save(null, false);

                            if ($success === true) {
                                $autorisationLogin = $this->Auth->login();
                            }
                        }
                    }
                }
            }

            if ($autorisationLogin === true) {
                $this->_cleanSession();

                $su = $this->Admin->find('count', [
                    'conditions' => [
                        'user_id' => $this->Auth->user('id')
                    ]
                ]);

                if ($su) {
                    $this->Session->write('Su', true);
                } else {
                    $this->Session->write('Su', false);
                }

                if (!empty($this->Session->read('Auth.User.uuid'))) {
                    $this->Session->delete('Auth.User.uuid');
                }

                $this->redirect([
                    'controller' => 'organisations',
                    'action' => 'selectorganisation'
                ]);
            } else {
                $error = true;
//                $this->Session->setFlash(__d('user', 'user.flasherrorNameUserPasswordInvalide'), 'flasherror');
            }
        } else {
            if ($this->Session->check('Auth.User.id')) {
                $this->redirect($this->Referers->get());
            }
        }

        $filename = FICHIER_CONFIGURATION_LOGIN;
        if (file_exists($filename)) {
            $fileConfigLogin = fopen($filename, 'r');
            $configLoginExistante = fread($fileConfigLogin, filesize($filename));
            fclose($fileConfigLogin);
        } else {
            $configLoginExistante = '';
        }

        $this->set(compact('error' , 'configLoginExistante', 'successMessage'));
    }

    /**
     * @access public
     */
    public function casLogout()
    {
        $this->layout = 'login';
    }

    /**
     * Page de deconnexion
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function logout() {
        $folder = new Folder(CHEMIN_PIECE_JOINT_TMP . $this->Session->read('Auth.User.id'));
        $folder->delete();

        $this->User->Jeton->deleteAll([
            'user_id' => $this->Auth->user('id')
        ]);

        foreach (array_keys($this->Session->read()) as $key) {
            if (!in_array($key, ['Config', 'Message', 'successMessage'])) {
                $this->Session->delete($key);
            }
        }

        $this->redirect($this->Auth->logout());
    }

    /**
     * Fonction de suppression du cache (sinon pose des problemes lors du login)
     *
     * @access protected
     * @created 17/06/2015
     * @version V1.0.0
     */
    protected function _cleanSession() {
        $this->Session->delete('Organisation');
    }

    /**
     * Fonction de création du tableau de droits pour le add et edit user
     *
     * @param int|null $id
     * @return type
     *
     * @access protected
     * @created 17/06/2015
     * @version V1.0.0
     */
    protected function _createTable($id = null) {
        $tableau = ['Organisation' => []];

        if ($this->Droits->isSu()) {
            $organisations = $this->Organisation->find('all');
        } else {
            $organisations = $this->Organisation->find('all', [
                'conditions' => [
                    'id' => $this->Session->read('Organisation.id')
                ]
            ]);
        }

        foreach ($organisations as $key => $value) {
            $tableau['Organisation'][$value['Organisation']['id']]['infos'] = [
                'raisonsociale' => $value['Organisation']['raisonsociale'],
                'id' => $value['Organisation']['id']
            ];

            $roles = $this->Role->find('all', [
                'recursive' => -1,
                'conditions' => ['organisation_id' => $value['Organisation']['id']]
            ]);

            $tableau['Organisation'][$value['Organisation']['id']]['roles'] = [];

            foreach ($roles as $clef => $valeur) {
                $tableau['Organisation'][$value['Organisation']['id']]['roles'][$valeur['Role']['id']] = [
                    'infos' => [
                        'id' => $valeur['Role']['id'],
                        'libelle' => $valeur['Role']['libelle'],
                        'organisation_id' => $valeur['Role']['organisation_id']
                    ]
                ];

                $droitsRole = $this->RoleDroit->find('all', [
                    'recursive' => -1,
                    'conditions' => ['role_id' => $valeur['Role']['id']]
                ]);

                foreach ($droitsRole as $k => $val) {
                    $tableau['Organisation'][$value['Organisation']['id']]['roles'][$valeur['Role']['id']]['droits'][$val['RoleDroit']['id']] = $val['RoleDroit'];
                }
            }
        }

        if ($id != null) {
            $this->set("userid", $id);

            $organisationUser = $this->OrganisationUser->find('all', [
                'conditions' => [
                    'user_id' => $id
                ],
                'contain' => [
                    'Droit'
                ]
            ]);

            foreach ($organisationUser as $key => $value) {
                $tableau['Orgas'][] = $value['OrganisationUser']['organisation_id'];

                $userroles = $this->OrganisationUserRole->find('all', [
                    'conditions' => [
                        'OrganisationUserRole.organisation_user_id' => $value['OrganisationUser']['id']
                    ]
                ]);

                foreach ($userroles as $cle => $val) {
                    $tableau['UserRoles'][] = $val['OrganisationUserRole']['role_id'];
                }

                foreach ($value['Droit'] as $clef => $valeur) {
                    $tableau['User'][$value['OrganisationUser']['organisation_id']][] = $valeur['liste_droit_id'];
                }

                $servicesUsers = $this->OrganisationUserService->find('all', [
                    'conditions' => [
                        'OrganisationUserService.organisation_user_id' => $value['OrganisationUser']['id']
                    ]
                ]);

                if (!empty($servicesUsers)) {
                    foreach ($servicesUsers as $serviceUser) {
                        $tableau['UserService'][] = $serviceUser['OrganisationUserService']['service_id'];
                    }
                }
            }

            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }

        $listedroits = $this->ListeDroit->find('all', [
            'recursive' => -1
        ]);

        $ld = [];

        foreach ($listedroits as $c => $v) {
            $ld[$v['ListeDroit']['value']] = $v['ListeDroit']['libelle'];
        }

        $retour = [
            'tableau' => $tableau,
            'listedroits' => $ld
        ];

        return $retour;
    }

    /**
     * Liste des utilisateurs de l'application
     */
    public function admin_index() {
        $this->index();
    }

    public function ajax_password() {
        // @fixme: vérifier que l'on soit en ajax
        $this->autoRender = false;
        $password = $this->request->data['password'];
        $json = [
            'entropie' => PasswordStrengthMeterAnssi::entropy($password),
            'force' => PasswordStrengthMeterAnssi::strength($password),
        ];
        return json_encode($json);
    }

    /**
     * Fonction permettant que l'utilisateur connecté modifie ses informations personnelles
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @add 09/04/2019
     * @version V1.0.2
     */
    public function preferences()
    {
        if ($this->Auth->user('ldap') === true) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->User->id = $this->Auth->user('id');

        if (!$this->User->exists()) {
            throw new NotFoundException('User Invalide');
        }

        $this->set('title', __d('user', 'user.titreModificationInfoUser'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            // Tentative de sauvegarde
            $this->User->begin();
            $success = true;

            $dataUser = $this->User->find('first', [
                'conditions' => [
                    'User.id' => $this->Auth->user('id')
                ],
                'fields' => [
                    'User.id',
                    'User.ldap'
                ]
            ]);

            $dataUser['User'] = array_merge($this->request->data('User'), $dataUser['User']);
            unset($dataUser['User']['username']);

            $this->User->create($dataUser);
            $success = $success && false !== $this->User->save(null, ['atomic' => false]);

            if ($success == true) {
                $this->User->commit();
                $this->Session->setFlash(__d('user', 'user.flashsuccessUserEnregistrer'), "flashsuccess");
//                $this->Flash->success(__d('user', 'user.flashsuccessUserEnregistrer'));

                $this->redirect($this->Referers->get());
            } else {
                $this->User->rollback();
                $this->Session->setFlash(__d('user', 'user.flasherrorErreurEnregistrementUser'), 'flasherror');
//                $this->Flash->error(__d('user', 'user.flasherrorErreurEnregistrementUser'));
            }
        } else {
            $infoUser = $this->User->find('first', [
                'conditions' => [
                    'id' => $this->Auth->user('id')
                ],
                'fields' => [
                    'civilite',
                    'nom',
                    'prenom',
                    'username',
                    'email',
                    'telephonefixe',
                    'telephoneportable',
                    'notification'
                ]
            ]);
            $this->request->data = $infoUser;
        }

        $this->set('options', $this->User->enums());
    }
}
