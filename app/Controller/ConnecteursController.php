<?php

/**
 * ConnecteursController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class ConnecteursController extends AppController {

    private $connecteurs = [
        1 => [
            'connecteur' => 'LDAP',
            'controller' => 'connecteurLdaps',
            'action' => 'add'
        ],
        2 => [
            'connecteur' => 'Authentification (LDAP)',
            'controller' => 'authentifications',
            'action' => 'add'
        ],
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $this->Droits->assertAuthorized([ListeDroit::GESTION_MAINTENANCE]);
    }
    
    /**
     * Permet l'affichage de la liste des connecteurs
     * 
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/03/2018
     * @version V1.0.0
     */
    public function index() {
        $this->set('title', __d('connecteur', 'connecteur.titreGestionConnecteurs'));
        
        $this->set('connecteurs', $this->connecteurs);
    }
    
}
