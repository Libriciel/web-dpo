<?php

/**
 * AppController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @package     App.Controller
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('WebcilTranslator', 'Utility');

class AppController extends Controller
{

    public $uses = [
        'Organisation',
        'Droit',
        'OrganisationUser',
        'Notification',
        'Pannel',
        'Service',
        'User',
        'Formulaire'
    ];

    public $components = [
        'Session',
        'Droits',
        'Notifications',
        'Auth' => [
            'loginAction' => [
                'admin' => false,
                'plugin' => null,
                'controller' => 'users',
                'action' => 'login'
            ],
            'authError' => 'Vous n\'êtes pas autorisé à effectuer cette action !',
            'loginRedirect' => [
                'controller' => 'organisations',
                'action' => 'change'

            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
             'authenticate' => [
                //Paramètre à passer pour tous les objets d'authentifications
                'all' => [
                    'userModel' => 'User'
                ],
                'AuthManager.Ldap' => [
                    'except' => ['superadmin']
                ],
                 'Form' => [
                     'passwordHasher' => [
                         'className' => 'Simple',
                         'hashType' => 'sha256'
                     ]
                 ]
             ]
        ],
        'Translator.TranslatorAutoload' => [
            'translatorClass' => 'WebcilTranslator'
        ],
        'Referers',
        'WebcilUsers'
    ];

    public $helpers = [
        'WebdpoMenu',
        'WebcilForm'
    ];

    /**
     * @access public
     * @created 29/04/2015
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @edit 26/02/2019
     * @version V1.0.2
     */
    public function beforeFilter()
    {
        $locale = Configure::read('Config.language');

        if ($locale && file_exists(APP . 'View' . DS . $locale . DS . $this->viewPath)) {
            $this->viewPath = $locale . DS . $this->viewPath;
        }

        $referer = $this->referer();
        $userId = $this->Auth->user('id');
//        $organisations = $this->WebcilUsers->organisations();
        $droits =  $this->Session->read('Droit.liste');

        $query = [
            'fields' => [
                'Organisation.id',
                'Organisation.raisonsociale'
            ]
        ];
        $organisations = $this->WebcilUsers->organisations('all', $query);

        $userCo = $this->User->find('first', [
            'conditions' => [
                'id' => $userId
            ],
            'fields' => [
                'User.id',
                'User.nom_complet',
                'User.ldap'
            ]
        ]);

        // bouton de notification dans la nav-barre
        $vuNotifications = $this->Notification->find('all', [
            'conditions' => [
                'Notification.user_id' => $userId,
                'Notification.vu' => false,
                'Fiche.organisation_id' => $this->Session->read('Organisation.id')
            ],
            'contain' => [
                'Fiche' => [
                    'Valeur' => [
                        'conditions' => [
                            'champ_name' => 'outilnom'
                        ],
                        'fields' => [
                            'champ_name', 
                            'valeur'
                        ]
                    ]
                ]
            ],
            'order' => [
                'Notification.content'
            ]
        ]);

        $this->set(compact('referer', 'userCo', 'userId', 'organisations', 'droits', 'vuNotifications'));
    }

    /**
     * @access public
     * @created 26/06/2015
     * @version V1.0.0
     */
    public function beforeRender()
    {
        parent::beforeRender();

        $query = [
            'contain' => [
                'FormulaireOrganisation' => [
                    'Organisation' => [
                        'id',
                        'raisonsociale',
                        'order' => ['raisonsociale']
                    ]
                ],
            ],
            'conditions' => [
                'Formulaire.active' => true,
                $this->Formulaire->getFormulaireConditionOrganisation($this->Session->read('Organisation.id'), true)
            ],
            'order' => [
                'Formulaire.libelle ASC'
            ]
        ];

        $queryFormulairesActifs = $query;
        $queryFormulairesActifs['conditions']['rt_externe'] = false;
        $formulaires_actifs = $this->Formulaire->find('all', $queryFormulairesActifs);

        $queryFormulairesSoustraitances = $query;
        $queryFormulairesSoustraitances['conditions']['rt_externe'] = true;
        $formulaires_soustraitances = $this->Formulaire->find('all', $queryFormulairesSoustraitances);

        $this->set(compact('formulaires_actifs', 'formulaires_soustraitances'));

        $serviceUser = $this->OrganisationUser->find('all', [
            'conditions' => [
                'user_id' => $this->Auth->user('id'),
                'organisation_id' => $this->Session->read('Organisation.id')
            ],
            'contain' => [
                'OrganisationUserService' => [
                    'Service'
                ]
            ]
        ]);
        $userServices = Hash::extract($serviceUser, '{n}.OrganisationUserService.Service');
        $this->set('userServices', $userServices);

        $serviceEntitee = $this->Service->find('all', [
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ]
        ]);
        $this->set('serviceEntitee', $serviceEntitee);

        $listeOrganisations = $this->Organisation->find('list', [
            'fields' => [
                'id',
                'raisonsociale'
            ],
            'order' => [
                'raisonsociale ASC'
            ]
        ]);
        $this->set(compact('listeOrganisations'));

        $countServicesUser = $this->WebcilUsers->services('count', [
            'restrict' => true,
            'user' => true
        ]);
        $this->set(compact('countServicesUser'));
    }

}
