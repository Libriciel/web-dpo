<?php

/**
 * NormesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class NormesController extends AppController {

    public $uses = [
        'Norme'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $listeDroitIndex = [ListeDroit::CREER_NORME, ListeDroit::VISUALISER_NORME, ListeDroit::MODIFIER_NORME, ListeDroit::ABROGER_NORME];

        if (in_array($action, ['abroger', 'revoquer_abrogation'], true) === true) {
            $this->Droits->assertAuthorized([ListeDroit::ABROGER_NORME]);
        } elseif ($action === 'add') {
            $this->Droits->assertAuthorized([ListeDroit::CREER_NORME]);
        } elseif ($action === 'download') {
            $this->Droits->assertLogged();
        } elseif ($action === 'edit') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_NORME]);
        } elseif ($action === 'index') {
            $this->Droits->assertAuthorized($listeDroitIndex);
        } elseif ($action === 'delete_file_save') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_NORME]);
        } else {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }
    
    /**
     * Permet l'affichage de la liste des normes
     * 
     * @throws ForbiddenException
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function index() {
        $this->set('title', __d('norme', 'norme.titreListeNorme'));

        $query = [
            'order' => 'Norme.norme ASC',
            'limit' => 20
        ];

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }
        
        // Conditions venant implicitement de l'action, de l'utilisateur connecté et des filtres
        if ($this->request->is('post') || false === empty($search)) {
            // Filtre par norme
            $filtreNorme = (string)Hash::get($this->request->data, 'normes.norme');
            if ('' !== $filtreNorme) {
                $query['conditions']['norme'] = $filtreNorme;
            }
            
            // Filtre par numero
            $filtreNumero = (string)Hash::get($this->request->data, 'normes.numero');
            if ('' !== $filtreNumero) {
                $query['conditions']['numero'] = $filtreNumero;
            }
            
            // Filtre par numero
            $filtreAbroger = (string)Hash::get($this->request->data, 'normes.abroger');
            if ('' !== $filtreAbroger) {
                $query['conditions']['abroger'] = $filtreAbroger;
            }
            
            // Filtrer par nom de norme
            $filtreNomNorme = (string)Hash::get($this->request->data, 'normes.nomnorme');
            if ('' !== $filtreNomNorme) {
                $query['conditions']['id'] = $filtreNomNorme;
            }
        }
        
        $this->paginate = $query;
        $normes = $this->paginate('Norme');
        $this->set('normes', $normes);
        
        $fields = ['numero'];
        $numeros = $this->Norme->find('all', ['fields' => $fields, 'group' => $fields, 'order' => $fields]);
        $numeros = Hash::combine($numeros, '{n}.Norme.numero', '{n}.Norme.numero');
        $this->set(compact('numeros'));

        $options = $this->Norme->enums();
        $this->set(compact('options'));
        
        $abroger = [
            true => 'Oui',
            false => 'Non'
        ];
        $this->set(compact('abroger'));
        
        $queryFiltreNorme = [
            'conditions' => [
                'abroger' => false
            ],
            'fields' => ['id', 'norme', 'numero', 'libelle', 'description'],
            'order' => ['norme', 'numero']
        ];
        $normesFiltre = $this->Norme->find('all', $queryFiltreNorme);

        $options_normes = Hash::combine(
            $normesFiltre,
            '{n}.Norme.id',
            [
                '%s-%s : %s',
                '{n}.Norme.norme',
                '{n}.Norme.numero',
                '{n}.Norme.libelle'
            ]
        );
        $this->set(compact('options_normes'));
    }

    /**
     * Fonction qui permet l'ajout d'une nouvelle norme
     * 
     * @throws ForbiddenException
     * 
     * @access public
     *
     * @created 20/12/2017
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V1.0.0
     *
     * @modified 07/04/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function add()
    {
        $this->set('title', __d('norme', 'norme.titreAjouterNorme'));
        
        $options = $this->Norme->enums();
        $this->set(compact('options'));
        
        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $data = $this->request->data;
            $info['message'] = __d('norme','norme.flasherrorErreurEnregistrementNorme');
            $info['statutMessage'] = 'flasherror';
            $this->Norme->begin();
            
            $this->Norme->create([
                'norme' => $data['Norme']['norme'],
                'numero' => $data['Norme']['numero'],
                'libelle' => nl2br($data['Norme']['libelle']),
                'description' => nl2br($data['Norme']['description'])
            ]);
            $success = $success && false !== $this->Norme->save(null, ['atomic' => false]);

            if ($success === true &&
                isset($data['Norme']['fichier']) &&
                file_exists($data['Norme']['fichier']['tmp_name'])
            ) {
                $info = $this->Norme->saveFileNorme($data['Norme']['fichier'], $this->Norme->getLastInsertId());
                $success = false !== $info['success'];
            }
            
            if ($success === true) {
                $this->Norme->commit();
                $this->Session->setFlash(__d('norme', 'norme.flashsuccessNormeEnregistrer'), 'flashsuccess');
                
                $this->redirect($this->Referers->get());
            } else {
                $this->Norme->rollback();
                $this->Session->setFlash($info['message'], $info['statutMessage']);
            }
        }
    }

    /**
     * Fonction qui permet la modification d'une norme
     * 
     * @param int $id
     * @throws ForbiddenException
     * 
     * @access public
     *
     * @created 20/12/2017
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V1.0.0
     *
     * @modified 07/04/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function edit($id)
    {
        $this->Droits->assertRecordExists('Norme', $id);

        $norme = $this->Norme->findById($id);
        $this->set('title', __d('norme', 'norme.titreModifierNorme') . $norme['Norme']['norme'] . '-' . $norme['Norme']['numero']);
        
        $norme['Norme']['libelle'] = strip_tags($norme['Norme']['libelle'], '<br />');
        $norme['Norme']['description'] = strip_tags($norme['Norme']['description'], '<br />');
        $this->set(compact('norme'));

        $options = $this->Norme->enums();
        $this->set(compact('options'));

        if ($this->request->is(['post', 'put'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $data = $this->request->data;
            $info['message'] = __d('norme','norme.flasherrorErreurEnregistrementNorme');
            $info['statutMessage'] = 'flasherror';
            $this->Norme->begin();
            
            $this->Norme->id = $id;
            $data['Norme']['id'] = $id;
            
            $this->Norme->create([
                'id' => $data['Norme']['id'],
                'norme' => $data['Norme']['norme'],
                'numero' => $data['Norme']['numero'],
                'libelle' => nl2br($data['Norme']['libelle']),
                'description' => nl2br($data['Norme']['description'])
            ]);
            $success = $success && false !== $this->Norme->save(null, ['atomic' => false]);

            if ($success === true &&
                isset($data['Norme']['fichier']) &&
                file_exists($data['Norme']['fichier']['tmp_name'])
            ) {
                $info = $this->Norme->saveFileNorme($data['Norme']['fichier'], $id);
                $success = false !== $info['success'];
            }

            if ($success == true) {
                $this->Norme->commit();
                $this->Session->setFlash(__d('norme', 'norme.flashsuccessNormeModifier'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                unset($this->request->data['Norme']['fichier']);

                $this->Norme->rollback();
                $this->Session->setFlash($info['message'], $info['statutMessage']);
            }
        } else {
            $this->request->data = $norme;
        }
    }
    
    /**
     * Fonction qui permet d'abroger d'une norme. 
     * 
     * @param int $id
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function abroger($id) {
        $this->Droits->assertRecordExists('Norme', $id);

        $this->Norme->id = $id;
        if ($this->Norme->save(['abroger' => true], ['atomic' => true]) !== false) {
            $this->Session->setFlash(__d('norme', 'norme.flashsuccessAbrogerEnregistrer'), 'flashsuccess');
        } else {
            $this->Session->setFlash(__d('norme', 'norme.flasherrorAbrogerErreur'), 'flasherror');
        }
        $this->redirect($this->Referers->get());
    }
    
    /**
     * Fonction qui permet de révoquer l'abrogation d'une norme. 
     * 
     * @param int $id
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function revoquerAbrogation($id) {
        $this->Droits->assertRecordExists('Norme', $id);

        $this->Norme->id = $id;
        if ($this->Norme->save(['abroger' => false], ['atomic' => true]) !== false) {
            $this->Session->setFlash(__d('norme', 'norme.flashsuccessRevoquerAbrogationEnregistrer'), 'flashsuccess');
        } else {
            $this->Session->setFlash(__d('norme', 'norme.flasherrorRevoquerAbrogationErreur'), 'flasherror');
        }
        $this->redirect($this->Referers->get());
    }
    
    /**
     * Permet de télécharger le fichier sur le serveur
     * 
     * @param char $urlFichier : nom du fichier sur le serveur
     * @param char $nameFichier : nom du fichier déposer par l'utilisateur
     * @return téléchargement du fichier
     * 
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function download($urlFichier, $nameFichier) {
        $path = CHEMIN_NORMES . $urlFichier;
        if (file_exists($path) === false) {
            $path = CHEMIN_FICHIER . NORMES . DS . $urlFichier;
            if (file_exists($path) === false) {
                throw new NotFoundException();
            }
        }

        $this->response->file($path, ['download' => true, 'name' => $nameFichier]);
        return $this->response;
    }

    /**
     * Fonction qui permet de supprimer le fichier associé à la norme
     * Supprimer les informations en BDD et le fichier sur le serveur
     * 
     * @param int $id
     * @param char $urlFile : nom du fichier sur le serveur
     * 
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 20/12/2017
     * @version V1.0.0
     */
    public function deleteFileSave($id, $urlFile) {
        $this->autoRender = false;

        $success = true;

        $this->Norme->begin();

        $record = [
            'id' => $id,
            'fichier' => null,
            'name_fichier' => null
        ];
        $success = false !== $this->Norme->save($record, ['atomic' => false]) && $success;

        if ($success === true) {
            $cheminFile =  CHEMIN_NORMES . $urlFile;

            if (file_exists($cheminFile) === false) {
                $cheminFile =  CHEMIN_FICHIER . NORMES . DS . $urlFile;
            }

            $success = unlink($cheminFile);
        }

        if ($success == true) {
            $this->Norme->commit();
            $this->Session->setFlash(__d('norme', 'norme.flashsuccessFichierSupprimer'), 'flashsuccess');
        } else {
            $this->Norme->rollback();
            $this->Session->setFlash(__d('norme', 'norme.flasherrorFichierSupprimer'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
}
