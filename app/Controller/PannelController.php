<?php

/**
 * PannelController : Controller du pannel
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('EtatFiche', 'Model');
App::uses('ListeDroit', 'Model');

class PannelController extends AppController {

    public $uses = [
        'Pannel',
        'Fiche',
        'Users',
        'OrganisationUser',
        'Droit',
        'EtatFiche',
        'Commentaire',
        'Modification',
        'Notification',
        'Historique',
        'Organisation',
        'Valeur',
        'Norme',
        'Formulaire',
        'Soustraitant',
        'Referentiel',
        'Responsable',
        'Typage',
    ];

    public $components = [
        'Droits',
        'Banettes'
    ];

    public $helpers = [
        'Banettes'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $anyone = ['drop_notif', 'index', 'technique', 'valid_notif', 'partage_services', 'ajax_get_history'];

        if ($action === 'all_traitements') {
            $this->Droits->assertAuthorized([ListeDroit::CONSULTER_ALL_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'archives') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'attente') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'consulte') {
            $this->Droits->assertAuthorized([ListeDroit::VALIDER_TRAITEMENT, ListeDroit::VISER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'encours_redaction') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'get_historique') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'initialisation') {
            $this->Droits->assertAuthorized([ListeDroit::INITIALISATION_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'notif_afficher') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'parcours') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'recu_consultation') {
            $this->Droits->assertAuthorized([ListeDroit::VISER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'recu_validation') {
            $this->Droits->assertAuthorized([ListeDroit::VALIDER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'refuser') {
            $this->Droits->assertAuthorized([ListeDroit::REDIGER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'supprimer_la_notif') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'superadmin') {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        } elseif ($action === 'ajax_listing_users_organisation') {
            $this->Droits->assertAuthorized([ListeDroit::DUPLIQUER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'ajax_listing_services_user') {
            $this->Droits->assertAuthorized([ListeDroit::DUPLIQUER_TRAITEMENT]);
            $this->Droits->assertNotSu();
        } elseif (in_array($action, $anyone) === false) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @deprecated 14/04/2021 v2.1.0
     */
    public function superadmin() {
        // @todo : issues/255
//        if (!empty($this->Session->read('Organisation'))) {
//            $this->Session->delete('Organisation');
//        }

        $this->set('title', __d('default', 'default.titreAdministrationApplication'));
    }

    public function technique() {
        if (true === $this->Droits->authorized([ListeDroit::REDIGER_TRAITEMENT, ListeDroit::VALIDER_TRAITEMENT, ListeDroit::VISER_TRAITEMENT])) {
            $this->redirect(['action' => 'index']);
        }

        $this->set('title', __d('default', 'default.titreAdministrationDeEntite'));
    }
    
    /**
     * Accueil de la page, listing des fiches et de leurs catégories
     * 
     * @access public
     * @created 02/12/2015
     * @version V1.0.0
     */
    public function index() {
        $this->Referers->clear();
        $this->Session->delete('Auth.User.uuid');
        $this->Session->delete('Fiche');

        if ($this->Droits->isSu() === true) {
            return $this->superadmin();
        }

        if (true !== $this->Droits->authorized([ListeDroit::REDIGER_TRAITEMENT, ListeDroit::VALIDER_TRAITEMENT, ListeDroit::VISER_TRAITEMENT])) {
            $this->redirect(['action' => 'technique']);
        }
        
        $banettes = [];
        $limit = 5;
        
        $this->set('title', __d('pannel', 'pannel.titreTraitement'));
        
        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        $nbUserConsultant = count($return['consultants']);
        
        $enumsNormes = $this->_enumsNorme();
        
        $this->_optionsNormes();
              
        if ($this->Droits->authorized(ListeDroit::REDIGER_TRAITEMENT)) {
            // En cours de rédaction
            $query = $this->Banettes->queryEnCoursRedaction();
            $banettes['encours_redaction'] = [
                'results' => $this->getResultTraitement($query, $limit),
                'count' => $this->Fiche->find('count', $query),
                'nbUserValideur' => $nbUserValideur,
                'nbUserConsultant' => $nbUserConsultant,
                'enumsNormes' => $enumsNormes
            ];

            $servicesUser = $this->WebcilUsers->services('list', [
                'restrict' => true,
                'user' => true,
                'fields' => [
                    'Service.id',
                    'Service.libelle'
                ]
            ]);

            if (!empty($servicesUser)) {
                $query = $this->Banettes->queryPartageServices();
                $banettes['service'] = [
                    'results' => $this->getResultTraitement($query, $limit),
                    'count' => $this->Fiche->find('count', $query),
                    'nbUserValideur' => $nbUserValideur,
                    'nbUserConsultant' => $nbUserConsultant,
                    'enumsNormes' => $enumsNormes
                ];
            }

            // En attente
            $query = $this->Banettes->queryAttente();
            $banettes['attente'] = [
                'results' => $this->getResultTraitement($query, $limit),
                'count' => $this->Fiche->find('count', $query),
                'nbUserValideur' => $nbUserValideur,
                'enumsNormes' => $enumsNormes
            ];
            
            // Traitement refusés
            $query = $this->Banettes->queryRefuser();
            $banettes['refuser'] = [
                'results' => $this->getResultTraitement($query, $limit),
                'count' => $this->Fiche->find('count', $query),
                'enumsNormes' => $enumsNormes
            ];
            
            // Mes traitements validés et insérés au registre
//            $query = $this->Banettes->queryArchives();
//            $banettes['archives'] = [
//                'results' => $this->Fiche->find('all', $query + ['limit' => $limit]),
//                'count' => $this->Fiche->find('count', $query)
//            ];
        }

        // Etat des traitements passés en ma possession
//        if ($this->Droits->authorized([ListeDroit::VALIDER_TRAITEMENT, ListeDroit::VISER_TRAITEMENT])) {
//            $query = $this->Banettes->queryConsulte();
//            $banettes['consulte'] = [
//                'results' => $this->Fiche->find('all', $query + ['limit' => $limit]),
//                'count' => $this->Fiche->find('count', $query),
//                'nbUserValideur' => $nbUserValideur
//            ];
//        }
        
        // Traitement reçu pour validation
        if ($this->Droits->authorized(ListeDroit::VALIDER_TRAITEMENT)) {
            $query = $this->Banettes->queryRecuValidation();
            $banettes['recuValidation'] = [
                'results' => $this->getResultTraitement($query, $limit),
                'count' => $this->Fiche->find('count', $query),
                'nbUserValideur' => $nbUserValideur,
                'nbUserConsultant' => $nbUserConsultant,
                'enumsNormes' => $enumsNormes
            ];
        }
        
        // Traitement reçu pour consultation
        if ($this->Droits->authorized(ListeDroit::VISER_TRAITEMENT)) {
            $query = $this->Banettes->queryRecuConsultation();
            $banettes['recuConsultation'] = [
                'results' => $this->getResultTraitement($query, $limit),
                'count' => $this->Fiche->find('count', $query),
                'enumsNormes' => $enumsNormes
            ];
        }
        
        $this->set(compact('banettes'));

        $notifications = $this->Notification->find('all', [
            'conditions' => [
                'Notification.user_id' => $this->Auth->user('id'),
                'Notification.vu' => false,
                'Notification.afficher' => false
            ],
            'contain' => [
                'Fiche' => [
                    'Valeur' => [
                        'conditions' => [
                            'champ_name' => 'outilnom'
                        ],
                        'fields' => [
                            'champ_name', 
                            'valeur'
                        ]
                    ]
                ]
            ],
            'order' => [
                'Notification.content'
            ]
        ]);
        $this->set('notifications', $notifications);

        $nameOrganisation = [];

        foreach ($notifications as $key => $value) {
            $nameOrganisation[$key] = $this->Organisation->find('first', [
                'conditions' => ['id' => $value['Fiche']['organisation_id']],
                'fields' => ['raisonsociale']
            ]);
        }
        $this->set('nameOrganisation', $nameOrganisation);
        
        $this->set('validants', $return['validants']);
        $this->set('consultants', $return['consultants']);
    }

    /**
     * Fonction qui récupère tous les traitements en cours de rédaction
     * 
     * @access public
     * @created 13/02/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function encours_redaction()
    {
        $this->set('title', __d('pannel', 'pannel.titreTraitementEnCoursRedaction'));
        
        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        $nbUserConsultant = count($return['consultants']);
        
        // En cours de rédaction
        $query = $this->Banettes->queryEnCoursRedaction();
        $banettes['encours_redaction'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => $nbUserValideur,
            'nbUserConsultant' => $nbUserConsultant,
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);

        $this->set('validants', $return['validants']);
        $this->set('consultants', $return['consultants']);

        $this->_optionsNormes();
    }

    public function partageServices()
    {
        $this->set('title', __d('pannel', 'pannel.titreTraitementEnCoursRedaction'));

        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        $nbUserConsultant = count($return['consultants']);

        $this->_optionsNormes();

        // En cours de rédaction partager avec mes services
        $query = $this->Banettes->queryPartageServices();
        $banettes['service'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => $nbUserValideur,
            'nbUserConsultant' => $nbUserConsultant,
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);

        $this->set('validants', $return['validants']);
        $this->set('consultants', $return['consultants']);
    }

    /**
     * Fonction qui récupère tous les traitements en attente
     * 
     * @access public
     * @created 13/02/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function attente() {
        $this->set('title', __d('pannel', 'pannel.titreTraitementEnAttente'));

        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        
        // En attente
        $query = $this->Banettes->queryAttente();
        $banettes['attente'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => $nbUserValideur,
            'enumsNormes' => $this->_enumsNorme()
        ];
        
        $this->set('banettes', $banettes);
        $this->set('validants', $return['validants']);
    }

    /**
     * Fonction qui récupère tous les traitements refusés
     * 
     * @access public
     * @created 13/02/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function refuser() {
        $this->set('title', __d('pannel', 'pannel.titreTraitementRefuser'));

        // Traitement refusés
        $query = $this->Banettes->queryRefuser();
        $banettes['refuser'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);
    }

    /**
     * Fonction qui récupère tous les traitements reçus pour validation
     * 
     * @access public
     * @created 13/02/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function recuValidation() {
        $this->Session->delete('Auth.User.uuid');

        $this->set('title', __d('pannel', 'pannel.titreTraitementRecuValidation'));
        
        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        $nbUserConsultant = count($return['consultants']);
        
        $this->_optionsNormes();
        
        // Traitement reçu pour validation
        $query = $this->Banettes->queryRecuValidation();
        $banettes['recuValidation'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => $nbUserValideur,
            'nbUserConsultant' => $nbUserConsultant,
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);
        
        $this->set('validants', $return['validants']);
        $this->set('consultants', $return['consultants']);
    }

    /**
     * Fonction qui récupère tous les traitements reçus pour consultation
     * 
     * @access public
     * @created 13/02/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function recuConsultation() {
        $this->set('title', __d('pannel', 'pannel.titreTraitementConsultation'));

        // Traitement reçu pour consultation
        $query = $this->Banettes->queryRecuConsultation();
        $banettes['recuConsultation'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);
    }

    /**
     * Requète récupérant les fiches validées par le DPO
     * 
     * @access public
     * @created 02/12/2015
     * @version V1.0.0
     */
    public function archives() {
        $this->set('title', __d('pannel', 'pannel.titreTraitementValidee'));

        // Mes traitements validés et insérés au registre
        $query = $this->Banettes->queryArchives();
        $banettes['archives'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);
    }

    /**
     * Fonction appelée pour le composant parcours, permettant d'afficher le parcours parcouru par une fiche et les
     * commentaires liés (uniquement ceux visibles par l'utilisateur)
     * 
     * @param int $id
     * @return mixed
     * 
     * @access private
     *
     * @created 02/12/2015
     * @version V1.0.0
     *
     * @version V2.1.3
     * @modified 24/01/2022
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function parcours($id)
    {
        $this->Droits->assertRecordAuthorized('Fiche', $id);

        $parcours = $this->EtatFiche->find('all', [
            'conditions' => [
                'EtatFiche.fiche_id' => $id,
            ],
            'contain' => [
                'Modification' => [
                    'id',
                    'modif',
                    'created'
                ],
                'Fiche' => [
                    'id',
                    'organisation_id',
                    'user_id',
                    'created',
                    'modified',
                    'User' => [
                        'id',
                        'nom',
                        'prenom',
                        'nom_complet'
                    ],
                ],
                'User' => [
                    'id',
                    'nom_complet'
                ],
                'Commentaire' => [
                    'User' => [
                        'id',
                        'nom',
                        'prenom',
                        'nom_complet'
                    ]
                ],
            ],
            'order' => [
                'EtatFiche.id DESC'
            ]
        ]);

        if (!empty($parcours)) {
            foreach ($parcours as $key => $parcour) {
                if ($parcour['EtatFiche']['etat_id'] === EtatFiche::TRAITEMENT_INITIALISE_RECU) {
                    $previousUser = $this->User->find('first', [
                        'conditions' => [
                            'User.id' => $parcour['EtatFiche']['previous_user_id']
                        ],
                        'fields' => [
                            'User.id',
                            'User.nom_complet'
                        ]
                    ]);
                    if (!empty($previousUser)) {
                        unset($parcours[$key]['User']);
                        $parcours[$key] += $previousUser;
                    }
                }
            }
        }

        return $parcours;
    }

    /**
     * Fonction permettant d'afficher tout les traitements passer par le DPO 
     * ou le valideur ou l'administrateur
     * 
     * @access public
     * @created 10/05/2016
     * @version V1.0.0
     */
    public function consulte() {
        $this->set('title', __d('pannel', 'pannel.titreTraitementVu'));

        $return = $this->_listValidants();
        $nbUserValideur = count($return['validants']);
        
        // Etat des traitements passés en ma possession
        $query = $this->Banettes->queryConsulte();
        $banettes['consulte'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => $nbUserValideur,
            'enumsNormes' => $this->_enumsNorme()
        ];
        $this->set('banettes', $banettes);

        $this->set('validants', $return['validants']);
    }

    /**
     * @param int $id
     * @return type
     * 
     * @access private
     *
     * @created 02/12/2015
     * @version V1.0.0
     *
     * @version V2.1.3
     * @modified 24/01/2022
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getHistorique($id)
    {
        $this->Droits->assertRecordAuthorized('Fiche', $id);

        $historique = $this->Historique->find('all', [
            'conditions' => ['fiche_id' => $id],
            'order' => [
                'created DESC',
                'id DESC'
            ]
        ]);

        return $historique;
    }

    /**
     * Fonction de suppression de toute les notifications d'un utilisateur
     * 
     * @access public
     * @created 02/12/2015
     * @version V1.0.0
     */
    public function dropNotif() {
        $success = true;
        $this->Notification->begin();

        $success = $success && $this->Notification->deleteAll([
                    'Notification.user_id' => $this->Auth->user('id'),
                    false
        ]);

        if ($success == true) {
            $this->Notification->commit();
        } else {
            $this->Notification->rollback();
        }

        $this->redirect($this->referer());
    }

    /**
     * Fonction de suppression d'une notification d'un utilisateur
     * 
     * @access public
     * @created 20/01/2016     * @version V1.0.0
     */
    public function supprimerLaNotif($idFiche)
    {
        $success = true;
        $this->Notification->begin();

        $success = $success && $this->Notification->deleteAll([
            'Notification.fiche_id' => $idFiche,
            'Notification.user_id' => $this->Auth->user('id')
        ]);

        if ($success == true) {
            $this->Notification->commit();
        } else {
            $this->Notification->rollback();
        }
    }

    /**
     * Permet de mettre dans la base de donner les notifications deja afficher 
     * quand on fermer la pop-up avec le bouton FERMER
     * 
     * @access public
     * @created 02/12/2015
     * @version V1.0.0
     */
    public function validNotif() {
        $success = true;
        $this->Notification->begin();

        $success = $success && $this->Notification->updateAll([
                    'Notification.afficher' => true
                        ], [
                    'Notification.user_id' => $this->Auth->user('id')
                ]) !== false;

        if ($success == true) {
            $this->Notification->commit();
        } else {
            $this->Notification->rollback();
        }

        $this->redirect($this->referer());
    }

    /**
     * Permet de mettre en base les notifs deja afficher
     * 
     * @param int $idFicheEnCourAffigage
     * 
     * @access public
     *
     * @created 20/01/2016
     * @version V1.0.0
     *
     * @modified 18/03/2021
     * @version V2.0.3
     */
    public function notifAfficher($idFicheEnCourAffigage)
    {
        $findNotification = $this->Notification->find('count', [
            'conditions' => [
                'user_id' => $this->Auth->user('id'),
                'fiche_id' => $idFicheEnCourAffigage
            ]
        ]);
        if ($findNotification === 0) {
            throw new NotFoundException();
        }

        $success = true;
        $this->Notification->begin();

        $success = $this->Notification->updateAll(
            [
                'Notification.afficher' => true
            ],
            [
                'Notification.user_id' => $this->Auth->user('id'),
                'Notification.fiche_id' => $idFicheEnCourAffigage
            ]
        ) !== false;

        if ($success == true) {
            $this->Notification->commit();
        } else {
            $this->Notification->rollback();
        }
    }

    /**
     * @return type
     * 
     * @access protected
     * @created 02/12/2015
     * @version V1.0.0
     */
    protected function _listValidants() {
        // Requète récupérant les utilisateurs ayant le droit de consultation
        $queryConsultants = [
            'fields' => [
                'User.id',
                'User.nom',
                'User.prenom'
            ],
            'joins' => [
                $this->Droit->join('OrganisationUser', ['type' => "INNER"]),
                $this->Droit->OrganisationUser->join('User', ['type' => "INNER"])
            ],
            'recursive' => -1,
            'conditions' => [
                'OrganisationUser.organisation_id' => $this->Session->read('Organisation.id'),
                'User.id != ' . $this->Auth->user('id'),
                'Droit.liste_droit_id' => ListeDroit::VISER_TRAITEMENT
            ],
        ];
        $consultants = $this->Droit->find('all', $queryConsultants);
        $consultants = Hash::combine($consultants, '{n}.User.id', [
                    '%s %s',
                    '{n}.User.prenom',
                    '{n}.User.nom'
        ]);
        $return = ['consultants' => $consultants];


        // Requète récupérant les utilisateurs ayant le droit de validation
        if ($this->Session->read('Organisation.dpo') != null) {
            $dpo = $this->Session->read('Organisation.dpo');
        } else {
            $dpo = 0;
        }

        $queryValidants = [
            'fields' => [
                'User.id',
                'User.nom',
                'User.prenom'
            ],
            'joins' => [
                $this->Droit->join('OrganisationUser', ['type' => "INNER"]),
                $this->Droit->OrganisationUser->join('User', ['type' => "INNER"])
            ],
            'conditions' => [
                'OrganisationUser.organisation_id' => $this->Session->read('Organisation.id'),
                'NOT' => [
                    'User.id' => [
                        $this->Auth->user('id'),
                        $dpo
                    ]
                ],
                'Droit.liste_droit_id' => ListeDroit::VALIDER_TRAITEMENT
            ]
        ];
        $validants = $this->Droit->find('all', $queryValidants);
        $validants = Hash::combine($validants, '{n}.User.id', [
                    '%s %s',
                    '{n}.User.prenom',
                    '{n}.User.nom'
        ]);
        $return['validants'] = $validants;

        return $return;
    }

    /**
     * 
     * @param type $id
     * 
     * @access protected
     * @created 07/03/2017
     * @version V1.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _typeDeclarationRemplie($id) {
        $typeDeclaration = $this->Valeur->find('first', [
            'conditions' => [
                'fiche_id' => $id,
                'champ_name' => 'typedeclaration'
            ]
        ]);

        if (!empty($typeDeclaration)) {
            if ($typeDeclaration['Valeur']['valeur'] != ' ') {
                $remplie = 'true';
            } else {
                $remplie = 'false';
            }
        } else {
            $remplie = 'false';
        }

        return($remplie);
    }
    
    protected function _enumsNorme() {
        return ($this->Norme->enums());
    }
    
    public function checkEmail(){
        App::uses('CakeEmail', 'Network/Email');
        
        $Email = new CakeEmail('email');
        //$Email->from('noreply@adullact.org');
        $Email->subject("email test web-DPO");
        $Email->to("theo.guillon@libriciel.coop");

        $result = null;

        debug("envoie de l'e-mail");
        try {
            $result = $Email->send("contenu du mail de test");
        } catch (Exception $e) {
            debug($e->getMessage());
        }

        debug($result);
        $this->autoRender = false;
    }

    /**
     * Fonction qui récupère tous les traitements en cours de rédaction
     *
     * @access public
     * @created 10/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function initialisation()
    {
        $this->set('title', __d('pannel', 'pannel.titreInitialisationTraitement'));

        $redacteurs = $this->_listRedacteurs();

        // Initialisation d'un traitement
        $query = $this->Banettes->queryInitialisationTraitement();
        $banettes['initialisation'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'nbUserValideur' => 0,
            'nbUserConsultant' => 0,
            'enumsNormes' => $this->_enumsNorme()
        ];

        $this->set(compact('banettes', 'redacteurs'));
    }

    /**
     * Liste des rédacteurs de l'entité
     *
     * @return type
     *
     * @access protected
     * @created 11/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _listRedacteurs($organisation_id = '')
    {
        $notUser_id = '';
        if (empty($organisation_id)) {
            $organisation_id = $this->Session->read('Organisation.id');
            $notUser_id = $this->Auth->user('id');
        }

        // Requète récupérant les utilisateurs ayant le droit de rédaction
        $queryRedacteurs = [
            'fields' => [
                'User.id',
                'User.nom',
                'User.prenom',
                $this->User->vfNomComplet()
            ],
            'joins' => [
                $this->Droit->join('OrganisationUser', ['type' => "INNER"]),
                $this->Droit->OrganisationUser->join('User', ['type' => "INNER"])
            ],
            'recursive' => -1,
            'conditions' => [
                'OrganisationUser.organisation_id' => $organisation_id,
                'User.id !=' => $notUser_id,
                'Droit.liste_droit_id' => ListeDroit::REDIGER_TRAITEMENT
            ],
        ];
        $droitsRedacteurs = $this->Droit->find('all', $queryRedacteurs);
        $redacteurs = Hash::combine($droitsRedacteurs, '{n}.User.id', [
            '%s %s',
            '{n}.User.prenom',
            '{n}.User.nom'
        ]);

        return $redacteurs;
    }

    /**
     * Visualisation de tous les traitements de l'entité
     *
     * @return void
     *
     * @access public
     *
     * @created 12/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @modified 21/01/2022
     * @version V2.1.3
     */
    public function all_traitements()
    {
        $this->set('title', __d('pannel', 'pannel.titreAllTraitement'));

        // Tous les traitements
        $query = $this->Banettes->queryAllTraitements();

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        if ($this->request->is('post') || false === empty($search)) {
            $dataFiltre = $this->request->data['Filtre'];
            $conditions = [];

            // Filtre sur l'utilisateur à l'origine du traitement
            if (!empty($dataFiltre['filtreEtatTraitement'])) {
                $conditions['EtatFiche.etat_id'] = $dataFiltre['filtreEtatTraitement'];
            }

            // Filtre sur l'utilisateur à l'origine du traitement
            if (!empty($dataFiltre['user'])) {
                $conditions['Fiche.user_id'] = $dataFiltre['user'];
            }

            // Filtre sur le nom du traitement
            if (!empty($dataFiltre['outil'])) {
                $conditionValeur[] = [
                    'Valeur.champ_name' => 'outilnom',
                    'NOACCENTS_UPPER( Valeur.valeur ) LIKE' => '%'.str_replace(
                            '*',
                            '%',
                            trim(noaccents_upper($dataFiltre['outil']))
                        ).'%'
                ];

                $subQuery = [
                    'alias' => 'valeurs',
                    'fields' => ['valeurs.fiche_id'],
                    'conditions' => $conditionValeur,
                    'contain' => false
                ];

                $sql = words_replace($this->Fiche->Valeur->sql($subQuery), [
                    'Valeur' => 'valeurs'
                ]);
                $condition[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur le service à l'origine du traitement
            if (!empty($dataFiltre['service'])) {
                $conditions['Fiche.service_id'] = $dataFiltre['service'];
            }

            // Filtre sur une norme
            if (!empty($dataFiltre['norme'])) {
                $conditions['Fiche.norme_id'] = $dataFiltre['norme'];
            }

            // Filtre sur un référentiel
            if (!empty($dataFiltre['referentiel'])) {
                $condition['Fiche.referentiel_id'] = $dataFiltre['referentiel'];
            }

            // Filtre sur les formulaires
            if (!empty($dataFiltre['formulaire'])) {
                $conditions['Fiche.form_id'] = $dataFiltre['formulaire'];
            }

            // Filtre sur l'obligation d'un AIPD
            if (!empty($dataFiltre['obligation_pia'])) {
                $conditions['Fiche.obligation_pia'] = $dataFiltre['obligation_pia'];
            }

            // Filtre sur la réalisation d'un AIPD
            if (!empty($dataFiltre['realisation_pia'])) {
                $conditions['Fiche.realisation_pia'] = $dataFiltre['realisation_pia'];
            }

            // Filtre sur le dépot d'un AIPD
            if (!empty($dataFiltre['depot_pia'])) {
                $conditions['Fiche.depot_pia'] = $dataFiltre['depot_pia'];
            }

            // Filtre sur le transfert hors ue
            if (!empty($dataFiltre['transfert_hors_ue'])) {
                $conditions['Fiche.transfert_hors_ue'] = $dataFiltre['transfert_hors_ue'];
            }

            // Filtre sur les données sensibles
            if (!empty($dataFiltre['donnees_sensibles'])) {
                $conditions['Fiche.donnees_sensibles'] = $dataFiltre['donnees_sensibles'];
            }

            // Filtre sur la soustraitance
            if (!empty($dataFiltre['soustraitance'])) {
                $conditions['Fiche.soustraitance'] = $dataFiltre['soustraitance'];
            }

            // Filtre par sous-traitant
            if (!empty($dataFiltre['soustraitant'])) {
                $subQuery = [
                    'alias' => 'soustraitances',
                    'fields' => ['soustraitances.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'soustraitances.soustraitant_id' => $dataFiltre['soustraitant']
                    ]
                ];
                $sql = $this->Soustraitance->sql($subQuery);
                $conditions[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur la co-responsabilité
            if (!empty($dataFiltre['coresponsable'])) {
                $conditions['Fiche.coresponsable'] = $dataFiltre['coresponsable'];
            }

            // Filtre par responsable
            if (!empty($dataFiltre['responsable'])) {
                $subQuery = [
                    'alias' => 'coresponsables',
                    'fields' => ['coresponsables.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'coresponsables.responsable_id' => $dataFiltre['responsable']
                    ]
                ];
                $sql = $this->Coresponsable->sql($subQuery);
                $conditions[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur la decision automatisee
            if (!empty($dataFiltre['decisionAutomatisee'])) {
                $subQuery = [
                    'alias' => 'valeurs',
                    'fields' => ['valeurs.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'valeurs.champ_name' => 'decisionAutomatisee',
                        'valeurs.valeur' => $dataFiltre['decisionAutomatisee']
                    ]
                ];
                $sql = $this->Valeur->sql($subQuery);
                $conditions[] = "Fiche.id IN ( {$sql} )";
            }

            // Filtre sur le type d'annexe
            if (!empty($dataFiltre['type_annexe'])) {
                $subQuery = [
                    'alias' => 'fichiers',
                    'fields' => ['fichiers.fiche_id'],
                    'contain' => false,
                    'conditions' => [
                        'fichiers.typage_id' => $dataFiltre['type_annexe']
                    ]
                ];
                $sql = $this->Fichier->sql($subQuery);
                $conditions[] = "Fiche.id IN ( {$sql} )";
            }

            $query['conditions'] += $conditions;
        }

        $options = [
            'etatTraitement' => [
                EtatFiche::ENCOURS_REDACTION => __d('pannel', 'pannel.textEtatEnCoursRedaction'),
                EtatFiche::ENCOURS_VALIDATION => __d('pannel', 'pannel.textEtatEnAttente'),
                EtatFiche::REFUSER => __d('pannel', 'pannel.textEtatRefuser'),
                EtatFiche::DEMANDE_AVIS => __d('pannel', 'pannel.textEtatDemandeAvis'),
                EtatFiche::REPLACER_REDACTION => __d('pannel', 'pannel.textEtatReplacerRedaction'),
                EtatFiche::REPONSE_AVIS => __d('pannel', 'pannel.textEtatConsulte'),
                EtatFiche::INITIALISATION_TRAITEMENT => __d('pannel', 'pannel.textEtatInitialisationTraitement'),
                EtatFiche::TRAITEMENT_INITIALISE_RECU => __d('pannel', 'pannel.textEtatTraitementInitialiseEnAttenteRedaction')
            ],
            'users' => $this->WebcilUsers->users('list', ['restrict' => true]),
            'services' => $this->WebcilUsers->services('list', [
                'restrict' => true,
                'fields' => [
                    'Service.id',
                    'Service.libelle'
                ]
            ]),
            'formulaires' => $this->Formulaire->find('list', [
                'Formulaire.id' => 'Formulaire.libelle',
                'contain' => [
                    'FormulaireOrganisation' => [
                        'Organisation' => [
                            'id',
                            'raisonsociale',
                            'order' => ['raisonsociale']
                        ]
                    ],
                ],
                'conditions' => [
                    $this->Formulaire->getFormulaireConditionOrganisation($this->Session->read('Organisation.id'))
                ],
            ]),
            'all_soustraitants' => $this->Soustraitant->find('list', [
                'restrict' => true,
                'fields' => [
                    'Soustraitant.id',
                    'Soustraitant.raisonsocialestructure',
                ]
            ]),
            'all_responsables' => $this->Responsable->find('list', [
                'restrict' => true,
                'fields' => [
                    'Responsable.id',
                    'Responsable.raisonsocialestructure',
                ]
            ]),
            'types_annexe' => $this->Typage->find('list', [
                'restrict' => true,
                'fields' => [
                    'Typage.id',
                    'Typage.libelle',
                ]
            ]),
        ];

        $this->_optionsNormes();

        $banettes['allTraitements'] = [
            'results' => $this->getResultTraitement($query),
            'count' => $this->Fiche->find('count', $query),
            'enumsNormes' => $this->_enumsNorme(),
        ];

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');

        $this->set(compact('banettes', 'options', 'mesOrganisations'));
    }

    /**
     * @return false|string|null
     *
     * @access public
     *
     * @created  02/01/2021
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @version V2.1.0
     */
    public function ajax_listing_users_organisation()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            return json_encode($this->_listRedacteurs($this->request->data['organisation_id']));
        }

        return null;
    }

    public function ajax_listing_services_user()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $serviceUser = $this->OrganisationUser->find('first', [
                'conditions' => [
                    'user_id' => $this->request->data['user_id'],
                    'organisation_id' => $this->request->data['organisation_id']
                ],
                'contain' => [
                    'OrganisationUserService' => [
                        'Service'
                    ]
                ]
            ]);

            $userServices = array_combine(
                Hash::extract($serviceUser, 'OrganisationUserService.{n}.Service.id'),
                Hash::extract($serviceUser, 'OrganisationUserService.{n}.Service.libelle')
            );

            return json_encode($userServices);
        }

        return null;
    }

    private function _optionsNormes()
    {
        $queryNormes = [
            'conditions' => ['abroger' => false],
//            'fields' => ['id', 'norme', 'numero', 'libelle', 'description'],
            'fields' => ['id', 'norme', 'numero', 'libelle'],
            'order' => ['norme', 'numero']
        ];
        $normes = $this->Norme->find('all', $queryNormes);

        $options_normes = Hash::combine(
            $normes,
            '{n}.Norme.id',
            [
                '%s-%s : %s',
                '{n}.Norme.norme',
                '{n}.Norme.numero',
                '{n}.Norme.libelle'
            ]
        );

        $options_referentiels = $this->Referentiel->find('list', [
            'fields' => [
                'id',
                'name'
            ],
            'order' => [
                'name ASC'
            ]
        ]);

        $this->set(compact('options_normes', 'options_referentiels'));
    }

    /**
     * @access private
     *
     * @param $query
     * @param int $limit
     * @return array
     *
     * @version V2.1.3
     * @created  24/01/2022
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    private function getResultTraitement ($query, $limit = 10)
    {
        $query['limit'] = $limit;
        $this->paginate = $query;
        return $this->paginate($this->Fiche);
    }

    /**
     * @return void
     *
     * @version V2.1.3
     * @created  24/01/2022
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function ajax_get_history()
    {
        if ($this->request->is('POST')) {
            $this->layout  = 'ajax';
            $fiche_id = $this->request->data['fiche_id'];

            $data = [
                'id' => $fiche_id,
                'parcours' => $this->parcours($fiche_id),
                'historique' => $this->getHistorique($fiche_id)
            ];
            $this->set($data);
        }
    }
}
