<?php

/**
 * ModelePresentationController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('ListeDroit', 'Model');

class ModelePresentationsController extends AppController {

    public $uses = [
        'ModelePresentation',
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

//        $action = Inflector::underscore($this->request->params['action']);

        if ($this->Droits->isSu() === true || true !== $this->Droits->authorized([ListeDroit::GESTION_MODELE])) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * @access public
     *
     * @created 26/12/2016
     * @version V1.0.0
     *
     * @modified 27/11/2020
     * @version V2.1.0
     */
    public function add()
    {
        if ($this->request->is('post') === false) {
            throw new MethodNotAllowedException();
        }

        if ('Cancel' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $data = $this->request->data;

        $success = true;
        $this->ModelePresentation->begin();

        $info = $this->ModelePresentation->saveFileModelePresentation(
            $data['ModelePresentation']['modelePresentation'],
            $this->Session->read('Organisation.id')
        );
        $success = $success && false !== $info['success'];

        if ($success == true) {
            $this->ModelePresentation->commit();
            $this->Session->setFlash(__d('modele', 'modele.flashsuccessModeleEnregistrer'), 'flashsuccess');
        } else {
            $this->ModelePresentation->rollback();
            $this->Session->setFlash($info['message'], $info['statutMessage']);
        }

        $this->redirect($this->Referers->get());
    }
    
    /**
     * Fonction pour téléchargé le modele de l'extrait de registre
     * 
     * @param type $file
     * @return type
     * 
     * @access public
     * @created 28/12/2016
     * @version V1.0.0
     */
    public function download($file, $nameFile) {
        $this->response->file(CHEMIN_MODELES_PRESENTATIONS . $file, [
            'download' => true,
            'name' => $nameFile
        ]);
        
        return $this->response;
    }
    
    /**
     * Permet de supprimer en bdd de le model associer par qu'ontre on ne 
     * supprime dans aucun cas le fichier enregistré
     * 
     * @param type $file --> c'est le nom du model (en générale 15614325.odt)
     * qui est enregistré dans app/webroot/files/models
     */
    public function delete($file) {
        $modeles = $this->ModelePresentation->find('all', [
            'conditions' => [
                'fichier' => $file
            ]
        ]);
        
        if ($modeles) {
            $isDeleted = $this->ModelePresentation->deleteAll([
                'fichier' => $file
            ]);

            if ($isDeleted) {
                $this->Session->setFlash(__d('modele_presentation', 'modele_presentation.flashsuccessModeleSupprimer'), 'flashsuccess');
            } else {
                $this->Session->setFlash(__d('modele_presentation', 'modele_presentation.flasherrorErreurSupprimerModele'), 'flasherror');
            }
        } else {
            $this->Session->setFlash(__d('modele_presentation', 'modele_presentation.flasherrorModeleInexistant'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }
    
    public function index() {
        $this->set('title', __d('modele_presentation', 'modele_presentation.titreModelePresentation'));

        $modelesPresentation = $this->ModelePresentation->find('first', [
            'conditions' => [
                'organisations_id' => $this->Session->read('Organisation.id')
            ]
        ]);

        if (!empty($modelesPresentation)) {
            foreach ($modelesPresentation as $modelePresentation) {
                if (!file_exists(CHEMIN_MODELES_PRESENTATIONS . $modelePresentation['fichier'])) {
                    $this->ModelePresentation->delete($modelePresentation['id']);

                    $modelesPresentation = [];
                }
            }
        }
        
        $this->set(compact('modelesPresentation'));
    }

    /**
     *
     * @access public
     * @created 26/04/2016
     * @version V1.0.2
     */
    public function infoVariable()
    {
        if ($this->Session->read('Organisation.dpo') === null) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }

        $this->set('title', __d('modele', 'modele.titreInfoVariableModele'));

        //Information sur l'organisation
        $organisation = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'raisonsociale',
                'telephone',
                'fax',
                'adresse',
                'email',
                'sigle',
                'siret',
                'ape',
                'dpo',
                'numerodpo',
                'emaildpo'
            ]
        ]);

        // Responsable de l'entité
        $responsableEntite = $this->Organisation->find('first', [
            'conditions' => [
                'id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'civiliteresponsable',
                'nomresponsable',
                'prenomresponsable',
                'emailresponsable',
                'telephoneresponsable',
                'fonctionresponsable',
                'responsable_nom_complet',
                'responsable_nom_complet_court'
            ]
        ]);

        // Information sur le DPO de l'organisation
        $userDPO = $this->User->find('first', [
            'conditions' => [
                'id' => $organisation['Organisation']['dpo']
            ],
            'fields' => [
                'civilite',
                'nom',
                'prenom',
                'telephonefixe',
                'telephoneportable',
                'nom_complet',
                'nom_complet_court'
            ]
        ]);

        if (isset($organisation['Organisation']['dpo']) === true) {
            unset($organisation['Organisation']['dpo']);
        }

        if (isset($organisation['Organisation']['numerodpo']) === true) {
            $userDPO['User'] += [
                'numerodpo' => $organisation['Organisation']['numerodpo'],
                'emaildpo' => $organisation['Organisation']['emaildpo']
            ];

            unset($organisation['Organisation']['numerodpo']);
            unset($organisation['Organisation']['emaildpo']);
        }

        $variablesOrganisation = [];
        foreach ($organisation['Organisation'] as $key => $valueOrganisation) {
            $variablesOrganisation['Entite'][$key] = [
                'organisation_'.$key => $valueOrganisation
            ];
        }

        foreach ($responsableEntite['Organisation'] as $key => $valueResponsable) {
            $variablesOrganisation['Responsable'][$key] = [
                'organisation_'.$key => $valueResponsable
            ];
        }

        foreach ($userDPO['User'] as $key => $valueUserDPO) {
            $variablesOrganisation['Dpo'][$key] = [
                'dpo_'.$key => $valueUserDPO
            ];
        }
        $this->set(compact('variablesOrganisation'));
    }
}
