<?php

/**
 * ArticlesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class ArticlesController extends AppController {

    public $uses = [
        'Article',
        'ArticleOrganisation',
        'Fichierarticle',
        'Fichier',
        'Organisation'
    ];

    /**
     * Vérification de l'accès aux actions.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);

        if (in_array($action, ['index', 'entite', 'show', 'dissocierArticle']) === true) {
            $this->Droits->assertAuthorized([
                ListeDroit::CREER_ARTICLE_FAQ,
                ListeDroit::MODIFIER_ARTICLE_FAQ,
                ListeDroit::SUPPRIMER_ARTICLE_FAQ
            ]);
            $this->Droits->isSu();
        } elseif ($action === 'add') {
            $this->Droits->assertAuthorized([ListeDroit::CREER_ARTICLE_FAQ]);
            $this->Droits->isSu();
        } elseif ($action === 'edit') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_ARTICLE_FAQ]);
            $this->Droits->isSu();
        } elseif (in_array($action, ['delete', 'deleteFile', 'deleteRecordingFile']) === true) {
            $this->Droits->assertAuthorized([ListeDroit::SUPPRIMER_ARTICLE_FAQ]);
            $this->Droits->isSu();
        } elseif (in_array($action, ['faq', 'consulter']) === true) {
            $this->Droits->assertAuthorized([ListeDroit::CONSULTER_ARTICLE_FAQ]);
            $this->Droits->assertNotSu();
        } elseif ($action === 'saveFileTmp') {
            $this->Droits->assertAuthorized([
                ListeDroit::CREER_ARTICLE_FAQ,
                ListeDroit::MODIFIER_ARTICLE_FAQ,
            ]);
            $this->Droits->isSu();
        } elseif ($action === 'download') {
            $this->Droits->assertAuthorized([
                ListeDroit::CREER_ARTICLE_FAQ,
                ListeDroit::MODIFIER_ARTICLE_FAQ,
                ListeDroit::CONSULTER_ARTICLE_FAQ,
            ]);
            $this->Droits->isSu();
        }
    }

    /**
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function index()
    {
        $this->Session->delete('Auth.User.uuid');

        $this->set('title', __d('article', 'article.titreIndex'));

        // "Transformation" des paramètres nommés en request params pour le filtre et la pagination
        $search = Hash::expand((array)Hash::get($this->request->params, 'named'));
        if (false === empty($search)) {
            $this->request->data = $search;
        }

        $conditions = [];
        if ($this->request->is('post') || false === empty($search)) {
            // Affectation
            if (!empty($this->request->data['ArticleOrganisation']['organisation_id'])) {
                if (isset($this->request->data['ArticleOrganisation']) === true) {
                    $success = true;
                    $this->Article->begin();

                    $organisations_ids = Hash::extract($this->request->data, 'ArticleOrganisation.organisation_id');
                    $articles_ids = Hash::extract($this->request->data, 'ArticleOrganisation.article_id');
                    foreach ($organisations_ids as $organisation_id) {
                        $articleEntite = $this->ArticleOrganisation->find('list', [
                            'conditions' => [
                                'organisation_id' => $organisation_id
                            ],
                            'fields' => [
                                'article_id'
                            ]
                        ]);

                        $diff = array_diff($articleEntite, $articles_ids);

                        $resultArticle = [];
                        if (!empty($diff)) {
                            $resultArticle = array_merge($articles_ids, $articleEntite);
                        } else {
                            $resultArticle = $articles_ids;
                        }

                        $data = [
                            'Organisation' => [
                                'id' => $organisation_id,
                            ],
                            'Article' => [
                                'Article' => $resultArticle
                            ]
                        ];

                        $this->Organisation->create($data);
                        $success = $success && false !== $this->Organisation->save(null, ['atomic' => false]);
                    }

                    if ($success == true) {
                        $this->Article->commit();
                        $this->Session->setFlash(__d('article', 'article.flashsuccessArticleAffecterEnregistrer'), 'flashsuccess');

                        $this->redirect(['action' => 'index']);
                    } else {
                        $this->Article->rollback();
                        $this->Session->setFlash(__d('article', 'article.flasherrorErreurEnregistrementArticleAffecter'), 'flasherror');
                    }
                }
            }

            // Filtre
            if (isset($this->request->data['Filtre']) === true) {
                $conditions = $this->_findFiltre($conditions);
            }
        }

        $query = [
            'fields' => [
                'Article.id',
                'Article.name',
                'Article.createdbyorganisation_id',
                'Createdbyorganisation.raisonsociale'
            ],
            'contain' => [
                'Createdbyorganisation',
                'Organisation' => [
                    'raisonsociale',
                    'order' => ['raisonsociale']
                ]
            ],
            'conditions' => $conditions,
            'order' => [
                'Article.name ASC',
                'Createdbyorganisation.raisonsociale ASC',
            ]
        ];

        $this->paginate = $query;

        $articles = $this->paginate($this->Article);
        $this->set(compact('articles'));

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations'));

        $options = $this->_optionsFiltre();
        $this->set(compact('options'));
    }

    /**
     * @param $conditions
     * @return mixed
     *
     * @access private
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    private function _findFiltre($conditions)
    {
        // Filtre sur le titre de l'article
        if (!empty($this->request->data['Filtre']['name'])) {
            $conditions['Article.name'] = $this->request->data['Filtre']['name'];
        }

        // Filtre sur l'entité à l'origine de article
        if (!empty($this->request->data['Filtre']['createdbyorganisation_id'])) {
            $conditions['Article.createdbyorganisation_id'] = $this->request->data['Filtre']['createdbyorganisation_id'];
        }

        return ($conditions);
    }

    /**
     * @param null $organisation_id
     * @return array
     *
     * @access private
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    private function _optionsFiltre($organisation_id = null)
    {
        $query = [];
        $condition = [];

        if ($organisation_id !== null) {
            $query = [
                'conditions' => $condition,
                'order' => [
                    'Article.raisonsociale ASC'
                ]
            ];;

            $subQuery = [
                'alias' => 'articles_organisations',
                'fields' => [
                    'articles_organisations.article_id'
                ],
                'conditions' => [
                    'articles_organisations.organisation_id' => $organisation_id
                ]
            ];
            $sql = $this->ArticleOrganisation->sql($subQuery);
            $query['conditions'][] = "Article.id IN ( {$sql} )";
        }

        $query['fields'] = 'name';
        $nameArticles = $this->Article->find('list', $query);

        $options = [
            'name' => array_combine($nameArticles, $nameArticles)
        ];

        return ($options);
    }

    /**
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function entite()
    {
        $this->set('title', __d('article', 'article.titreIndex'));

        $organisation_id =  $this->Session->read('Organisation.id');

        $conditions = [];
        $limit = 20;
        $maxLimit = 100;

        if ($this->request->is('post')) {
            // Filtre
            if (isset($this->request->data['Filtre']) === true) {
                $conditions = $this->_findFiltre($conditions);

                // Filtre sur le nombre de traitement à l'affichage
                if (!empty($this->request->data['Filtre']['nbAffichage'])) {
                    $limit = $this->request->data['Filtre']['nbAffichage'];
                } else {
                    $limit = 20;
                }

                if( $limit > $maxLimit ) {
                    $maxLimit = $limit;
                }
            }
        }

        $query = [
            'conditions' => $conditions,
            'order' => [
                'Article.name ASC'
            ],
            'limit' => $limit,
            'maxLimit' => $maxLimit
        ];

        $subQuery = [
            'alias' => 'articles_organisations',
            'fields' => [
                'articles_organisations.article_id'
            ],
            'conditions' => [
                'articles_organisations.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->ArticleOrganisation->sql($subQuery);
        $query['conditions'][] = "Article.id IN ( {$sql} )";

        $this->paginate = $query;
        $articles = $this->paginate($this->Article);
        $this->set(compact('articles'));

//        $options = $this->_optionsFiltre($organisation_id);
//        $this->set(compact('options'));
    }

    /**
     * @throws Exception
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function add()
    {
        return $this->edit();
    }

    /**
     * @param int $id
     * @throws Exception
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 07/08/2020
     * @version V2.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function edit($id = null)
    {
        if ($this->request->params['action'] === 'add') {
            $this->set('title', __d('article', 'article.titreAjouterArticle'));
        } elseif ($this->request->params['action'] === 'edit') {
            $this->set('title', __d('article', 'article.titreModificationArticle'));
        }

        if (!empty($this->Session->read('Auth.User.uuid'))) {
            $dir = CHEMIN_PIECE_JOINT_ARTICLE_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid');
            $files = $this->Fichier->scan_dir($dir);

            $this->set('files', $files);
        }

        if ($this->request->is(['post', 'put'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());

                $this->Session->delete('Auth.User.uuid');
            }

            $data = $this->request->data;

            if ($this->request->params['action'] === 'add') {
                if (true !== $this->Droits->authorized($this->Droits->isSu() || ListeDroit::CREER_ARTICLE_FAQ)) {
                    throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
                }

                $data['Article']['createdbyorganisation_id'] = $this->Session->read('Organisation.id');
            } elseif ($this->request->params['action'] === 'edit') {
                if (true !== $this->Droits->authorized($this->Droits->isSu() || ListeDroit::MODIFIER_ARTICLE_FAQ)) {
                    throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
                }

                $data['Article']['id'] = $id;
            }

            // Tentative de sauvegarde
            $this->Article->begin();
            $success = true;

            $this->Article->create($data);
            $success = $success && $this->Article->save(null, ['atomic' => false]);

            if ($success === true) {
                if (!empty($this->Session->read('Auth.User.uuid'))) {
                    $success = $success && false !== $this->Fichierarticle->transfereSave(
                        $this->Article->id,
                        $this->Session->read('Auth.User.uuid'),
                        $this->Session->read('Auth.User.id')
                    );
                }
            }

            if ($success === true) {
                $this->Article->commit();
                $this->Session->setFlash(__d('article', 'article.flashsuccessArticleEnregistrer'), 'flashsuccess');

                $this->Session->delete('Auth.User.uuid');

                $this->redirect($this->Referers->get());
            } else {
                $this->Article->rollback();
                $this->Session->setFlash(__d('article', 'article.flasherrorErreurEnregistrementArticle'), 'flasherror');
            }
        }

        if ($this->request->params['action'] === 'edit') {
            if (empty($this->request->data)) {
                $article = $this->Article->find('first', [
                    'conditions' => [
                        'id' => $id
                    ]
                ]);

                $this->request->data = $article;
            }

            $articleOrganisation = $this->ArticleOrganisation->find('all', [
                'conditions' => [
                    'article_id' => $id
                ]
            ]);
            $organisationArticleSelect = Hash::extract($articleOrganisation, '{n}.ArticleOrganisation.organisation_id');

            $optionsAffecter = [
                'Organisation' => [
                    'Organisation' => $organisationArticleSelect
                ]
            ];
            $this->request->data += $optionsAffecter;

            $filesSave = $this->Fichierarticle->find('all', [
                'conditions' => [
                    'article_id' => $id
                ]
            ]);
            $this->set(compact('filesSave'));
        }

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');

        $this->set(compact('mesOrganisations'));

        $this->view = 'edit';
    }

    /**
     * @param int $id
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function show($id)
    {
        if ('Back' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }
        
        $this->set('title', __d('article', 'article.titreVisualisationArticle'));

        $article = $this->Article->find('first', [
            'conditions' => [
                'id' => $id
            ]
        ]);
        $this->request->data = $article;

        $filesSave = $this->Fichierarticle->find('all', [
            'conditions' => [
                'article_id' => $id
            ]
        ]);
        $this->set(compact('filesSave'));

        $articleOrganisation = $this->ArticleOrganisation->find('all', [
            'conditions' => [
                'article_id' => $id
            ]
        ]);
        $organisationArticleSelect = Hash::extract($articleOrganisation, '{n}.ArticleOrganisation.organisation_id');

        $optionsAffecter = [
            'Organisation' => [
                'Organisation' => $organisationArticleSelect
            ]
        ];
        $this->request->data += $optionsAffecter;

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('mesOrganisations'));
    }

    /**
     * @param int $id
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function delete($id)
    {
        $associationArticle = $this->ArticleOrganisation->find('all', [
            'conditions' => [
                'article_id' => $id
            ]
        ]);

        if (empty($associationArticle)) {
            $success = true;
            $this->Article->begin();

            $files = $this->Fichierarticle->find('all', [
                'conditions' => [
                    'article_id' => $id
                ],
                'fields' => [
                    'url'
                ]
            ]);

            $success = $success && false !== $this->Article->deleteAll([
                'Article.id' => $id
            ]);

            if ($success == true) {
                $this->Article->commit();

                foreach ($files as $file) {
                    $this->_deleteFileDisk($file['Fichierarticle']['url']);
                }

                $this->Session->setFlash(__d('article', 'article.flashsuccessSuppressionArticleEntite'), 'flashsuccess');
            } else {
                $this->Article->rollback();
                $this->Session->setFlash(__d('article', 'article.flasherrorErreurSuppressionArticleEntite'), 'flasherror');
            }
        } else {
            $this->Session->setFlash(__d('article', 'article.flasherrorErreurAssociationExistanteArticleEntite'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Fonction qui permet de dissocier un article de la FAQ liée à l'entité
     *
     * @param int $articleId
     * @throws ForbiddenException
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 25/04/2019
     * @version v1.0.2
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function dissocierArticle($articleId)
    {
        $success = true;
        $this->ArticleOrganisation->begin();

        $success = $success && false !== $this->ArticleOrganisation->deleteAll([
                'ArticleOrganisation.organisation_id' => $this->Session->read('Organisation.id'),
                'ArticleOrganisation.article_id' => $articleId
            ]);

        if ($success == true) {
            $this->ArticleOrganisation->commit();
            $this->Session->setFlash(__d('article', 'article.flashsuccessDissocierArticleEntite'), 'flashsuccess');
        } else {
            $this->ArticleOrganisation->rollback();
            $this->Session->setFlash(__d('article', 'article.flasherrorErreurDissocierArticleEntite'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * @param $organisation_id
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function faq($organisation_id)
    {
        $this->set('title', __d('article', 'article.titreIndex'));

        $condition = [];
        $limit = 20;
        $maxLimit = 100;

        $query = [
            'conditions' => $condition,
            'order' => [
                'Article.name ASC'
            ],
            'limit' => $limit,
            'maxLimit' => $maxLimit
        ];

        $subQuery = [
            'alias' => 'articles_organisations',
            'fields' => [
                'articles_organisations.article_id'
            ],
            'conditions' => [
                'articles_organisations.organisation_id' => $organisation_id
            ]
        ];
        $sql = $this->ArticleOrganisation->sql($subQuery);
        $query['conditions'][] = "Article.id IN ( {$sql} )";

        $this->paginate = $query;
        $articles = $this->paginate($this->Article);
        $this->set(compact('articles'));
    }

    /**
     * @param int $id
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function consulter($id)
    {
        if ('Back' === Hash::get($this->request->data, 'submit')) {
            $this->redirect($this->Referers->get());
        }

        $article = $this->Article->find('first', [
            'conditions' => [
                'id' => $id
            ],
            'fields' => [
                'name',
                'description'
            ]
        ]);

        $this->set('title', $article['Article']['name']);

        $filesSave = $this->Fichierarticle->find('all', [
            'conditions' => [
                'article_id' => $id
            ]
        ]);
        $this->set(compact('filesSave'));

        $this->request->data = $article;
    }

    /**
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function saveFileTmp()
    {
        if (!empty($this->request->params['form']['fichiers'])) {
            if (empty($this->Session->read('Auth.User.uuid'))) {
                $this->Session->write('Auth.User.uuid', CakeText::uuid());
            }

            $this->autoRender = false;

            // On verifie si le dossier file existe. Si c'est pas le cas on le cree
            create_arborescence_files($this->Session->read('Auth.User.id'));

            $json = [];
            foreach ($this->request->params['form']['fichiers']['tmp_name'] as $key => $tmpFile) {
                $dir = CHEMIN_PIECE_JOINT_ARTICLE_TMP . $this->Session->read('Auth.User.id') . DS . $this->Session->read('Auth.User.uuid');

                if (!is_dir($dir)) {
                    mkdir($dir);
                }

                $filename = $this->request->params['form']['fichiers']['name'][$key];
                $path = $dir.DS.$filename;

                $mime = mime_content_type($tmpFile);
                $accepted = Configure::read('allFileAnnexeAcceptedTypes');

                if (in_array($mime, $accepted) === true) {
                    move_uploaded_file($tmpFile, $path);

                    $json[] = [
                        'status' => 'success',
                        'filename' => $filename,
                        'path' => $path
                    ];
                } else {
                    $json[] = [
                        'status' => 'error',
                    ];
                }
            }

            header('Content-type: "application/json"');
            echo json_encode($json);
        }
    }

    /**
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @modified 14/04/2021
     * @version V2.1.0
     */
    public function deleteFile()
    {
        $this->autoRender = false;

        $file = new File(CHEMIN_PIECE_JOINT_ARTICLE_TMP . $this->Session->read('Auth.User.id') . DS  . $this->Session->read('Auth.User.uuid') . DS . $this->request->data('filename') );
        $file->delete();
    }

    /**
     * Gère le téléchargement des pieces jointes d'une fiche
     *
     * @param int|null $url
     *
     * @access public
     *
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function download($url = null, $nomFile = 'file.odt')
    {
        $this->response->file(CHEMIN_PIECE_JOINT_ARTICLE . $url, [
            'download' => true,
            'name' => $nomFile
        ]);

        return $this->response;
    }

    /**
     * Suppression de l'enregistrement de la présente d'un fichier dans la table
     * "fichiers"
     *
     * @access public
     *
     * @created 19/10/2018
     * @version V1.0.1
     */
    public function deleteRecordingFile()
    {
        $this->autoRender = false;

        $success = true;
        $this->Fichierarticle->begin();

        $success = $success && false !== $this->Fichierarticle->delete($this->request->data('idFile'));

        if ($success == true) {
            $this->Fichierarticle->commit();

            $this->_deleteFileDisk($this->request->data('urlFile'));
            $this->Session->setFlash(__d('annexe', 'annexe.flashsuccessAnnexeSupprimer'), 'flashsuccess');
        } else {
            $this->Fichierarticle->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorPasDroitPage'), 'flasherror');
        }
    }

    protected function _deleteFileDisk($urlFile)
    {
        $targetFileDelete = CHEMIN_PIECE_JOINT_ARTICLE . $urlFile;

        if (file_exists($targetFileDelete) === true) {
            $file = new File($targetFileDelete);
            $file->delete();
        }
    }
}
