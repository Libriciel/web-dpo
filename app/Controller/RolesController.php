<?php

/**
 * RolesController
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');

class RolesController extends AppController {

    public $uses = [
        'Authentification',
        'ConnecteurLdap',
        'Role',
        'ListeDroit',
        'RoleDroit',
        'OrganisationUserRole',
    ];
    
    public $components = [
        'LdapManager.GroupManager',
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $listeDroitIndex = [ListeDroit::CREER_PROFIL, ListeDroit::MODIFIER_PROFIL, ListeDroit::SUPPRIMER_PROFIL];
        $action = Inflector::underscore($this->request->params['action']);

        if ($this->Droits->isSu() !== true) {
            if ($action === 'add') {
                $this->Droits->assertAuthorized([ListeDroit::CREER_PROFIL]);
            } elseif ($action === 'edit') {
                $this->Droits->assertAuthorized([ListeDroit::MODIFIER_PROFIL]);
            } elseif ($action === 'index') {
                $this->Droits->assertAuthorized($listeDroitIndex);
            } elseif ($action === 'delete') {
                $this->Droits->assertAuthorized([ListeDroit::SUPPRIMER_PROFIL]);
            } elseif ($action === 'reattribution_roles') {
                $this->Droits->assertAuthorized($listeDroitIndex);
            } else {
                throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
            }
        }
    }

    /**
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function index() {
        $this->set('title', __d('role', 'role.titreListeProfil'));

        $roles = $this->Role->find('all', [
            'fields' => array_merge(
                $this->Role->fields(),
                [
                    $this->Role->vfLinkedUsersCount()
                ]
            ),
            'contain' => [
                'ListeDroit' => [
                    'fields' => ['ListeDroit.libelle'],
                    'conditions' => [
                        'NOT' => [
                            'ListeDroit.id' => [
                                ListeDroit::CREER_ORGANISATION
                            ]
                        ]
                    ],
                    'order' => 'ListeDroit.libelle ASC'
                ]
            ],
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ]
        ]);

        $roleDPO_id = null;
        foreach ($roles as $role) {
            if ($this->_roleIsDpoRole($role['Role']['id']) === true) {
                $roleDPO_id = $role['Role']['id'];
            }
        }

        $this->set(compact('roles', 'roleDPO_id'));
    }

    private function _roleIsDpoRole($role_id)
    {
        $droitsDPO = ListeDroit::LISTE_DROITS_DPO_MINIMUM;

        $exists = [];
        foreach ($droitsDPO as $value) {
            $exists[] = sprintf('EXISTS(SELECT * FROM role_droits INNER JOIN liste_droits ON ( role_droits.liste_droit_id = liste_droits.id ) WHERE liste_droits.value = %d AND role_droits.role_id = Role.id)', $value);
        }

        $roleDPO_id = $this->Role->find('first', [
            'fields' => [
                'Role.id'
            ],
            'conditions' => [
                'Role.organisation_id' => $this->Session->read('Organisation.id'),
                $exists
            ]
        ]);

        if (empty($roleDPO_id) === false && $role_id == $roleDPO_id['Role']['id']) {
            return true;
        }

        return false;
    }

    /**
     * Retourne les options à envoyer à la vue dans les méthodes add et edit.
     *
     * return array
     */
    protected function _optionsAddEdit($role_id = null)
    {
        $noViewDroit = [
            ListeDroit::INSERER_TRAITEMENT_REGISTRE,
            ListeDroit::MODIFIER_TRAITEMENT_REGISTRE
        ];

        if ($role_id != null) {
            $roleIsDPO = $this->_roleIsDpoRole($role_id);

            if ($roleIsDPO === true) {
                $noViewDroit = [];
                $noViewDroit = ListeDroit::LISTE_DROITS_DPO_MINIMUM;
            }
        }

        $noViewDroit[] = ListeDroit::CREER_ORGANISATION;

        $options = [
            'ListeDroit' => [
                'ListeDroit' => $this->ListeDroit->find(
                    'list',
                    [
                        'conditions' => [
                            'NOT' => [
                                'ListeDroit.id' => $noViewDroit
                            ]
                        ],
                        'order' => 'id'
                    ]
                )
            ]
        ];

        return $options;
    }

    /**
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function add() {
        $this->set('title', __d('role', 'role.titreAjouterProfil'));

        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }
            
            $this->request->data['Role']['organisation_id'] = $this->Session->read('Organisation.id');

            $success = true;
            $this->Role->begin();

            $this->Role->create($this->request->data);

            if (false !== $this->Role->save(null, ['atomic' => false])) {
                $this->Role->commit();
                $this->Session->setFlash(__d('role', 'role.flashsuccessProfilEnregistrer'), 'flashsuccess');
                $this->redirect($this->Referers->get());
            } else {
                $this->Role->rollback();
                $this->Session->setFlash(__d('role', 'role.flasherrorErreurEnregistrementProfil'), 'flasherror');
            }
        }

        $this->set('options', $this->_optionsAddEdit());
    }

    /**
     * @param int $id
     * @throws NotFoundException
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function edit($id) {
        $this->Droits->assertRecordAuthorized('Role', $id, ['superadmin' => true]);

        $this->set('title', __d('role', 'role.titreEditerProfil'));

        if ($this->request->is(['post', 'put'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $data = $this->request->data;

            $modelGroup = $data['Role']['ModelGroup'];
            unset($data['Role']['ModelGroup']);

            $roleIsDPO = $this->_roleIsDpoRole($id);

            if ($roleIsDPO === false) {
                if (isset($modelGroup)) {
                    $this->GroupManager->setGroupsRole($id, $modelGroup);
                }
            }
            
            $this->Role->id = $id;
            $data['Role']['id'] = $id;

            $success = true;
            $this->Role->begin();

            $roleIsDPO = $this->_roleIsDpoRole($id);
            if ($roleIsDPO === true) {
                if (empty($data['ListeDroit']['ListeDroit'])) {
                    $data['ListeDroit']['ListeDroit'] = ListeDroit::LISTE_DROITS_DPO_MINIMUM;
                } else {
                    $data['ListeDroit']['ListeDroit'] = array_merge($data['ListeDroit']['ListeDroit'], ListeDroit::LISTE_DROITS_DPO_MINIMUM);
                }
            }

            $this->Role->create($data);
            $success = $this->Role->save(null, ['atomic' => false]) && $success;

            if ($success === true) {
                $this->Role->commit();
                
                $query = [
                    'fields' => ['OrganisationUserRole.id'],
                    'conditions' => [
                        'OrganisationUserRole.role_id' => $id
                    ]
                ];
                $exists = $this->Role->OrganisationUserRole->find('first', $query);
                $message = true === empty($exists)
                    ? 'role.flashsuccessProfilModifier'
                    : 'role.flashsuccessProfilModifierActualiser';
                $this->Session->setFlash(__d('role', $message), 'flashsuccess');
                $this->redirect($this->Referers->get());
            } else {
                $this->Role->rollback();
                $this->Session->setFlash(__d('role', 'role.flasherrorErreurModificationProfil'), 'flasherror');
            }
        } else {
            $role = $this->Role->findById($id);
            $this->request->data = $role;

            $this->request->data['ListeDroit']['ListeDroit'] = Hash::extract(
                $this->RoleDroit->find(
                    'all',
                    [
                        'fields' => 'liste_droit_id',
                        'conditions' => ['role_id' => $id]
                    ]
                ),
                '{n}.RoleDroit.liste_droit_id'
            );
        }
        
        $authentificationLdap = $this->Authentification->find('first', [
           'conditions' => [
               'organisation_id' => $this->Session->read('Organisation.id')
           ],
           'fields' => [
               'use'
           ]
        ]);
        
        $useLdap = $this->ConnecteurLdap->find('first', [
            'conditions' => [
                'organisation_id' => $this->Session->read('Organisation.id')
            ],
           'fields' => [
               'use'
           ]
        ]);
        
        if ($useLdap == true && $authentificationLdap == true && $this->_roleIsDpoRole($id) === false) {
            $ldap = true;
        } else {
            $ldap = false;
        }
        $this->set('ldap', $ldap);
        
        $this->GroupManager->getGroupsRole($id);

        $this->set('options', $this->_optionsAddEdit($id));
    }

    /**
     * Suppression d'un rôle
     *
     * @param int|null $id
     * @return type
     * @throws NotFoundException
     * @throws RuntimeException
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function delete($id) {
        $this->Droits->assertRecordAuthorized('Role', $id, ['superadmin' => true]);

        $role = $this->Role->find('first',[
            'fields' => array(
                'Role.id',
                $this->Role->vfLinkedUsersCount()
            ),
            'conditions' => [
                'Role.id' => $id
            ]
        ]);

        if ($this->_roleIsDpoRole($id) === true) {
            throw new RuntimeException(__d('role', 'role.exceptionProfilNonSuppressible'));
        }

        if ($role['Role']['users_count'] > 0) {
            throw new RuntimeException(__d('role', 'role.exceptionProfilNonSuppressible'));
        }

        $this->Role->begin();
        if ($this->Role->delete($id) !== false) {
            $this->Role->commit();
            $this->Session->setFlash(__d('role', 'role.flashsuccessProfilSupprimer'), 'flashsuccess');
        } else {
            $this->Role->rollback();
            $this->Session->setFlash(__d('role', 'role.flasherrorErreurSupprimerProfil'), 'flasherror');
        }

        return $this->redirect($this->Referers->get());
    }

    /**
     * Réattribuer des roles aux utilisateurs après modification d'un profil
     * FIXME ne devrait pas avoir besoin de reforcer les droits
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @access public
     * @created 14/11/2016
     * @version V1.0.0
     */
    public function reattributionRoles($idRole) {
        $this->Droits->assertRecordAuthorized('Role', $idRole, ['superadmin' => true]);

        // Récupération des droit en fonction de l'id du profil
        $rolesDroit = $this->RoleDroit->find('all', [
            'conditions' => [
                'role_id' => $idRole
            ]
        ]);

        // Récupération des utilisateurs qui utilise le profil e, fonction de l'id du profil
        $usersRole = $this->OrganisationUserRole->find('all', [
            'conditions' => [
                'role_id' => $idRole
            ]
        ]);

        // Si des utilisateur utilise le profil en question
        if (!empty($usersRole)) {
            $success = true;
            $this->Droit->begin();

            // Pour chaque utilisateur utilisant le profil en question
            foreach ($usersRole as $userRole) {
                // Suppression de tout les droits de l'utilisateur
                $success = $success && false !== $this->Droit->deleteAll(
                                [
                            'organisation_user_id' => $userRole['OrganisationUserRole']['organisation_user_id']
                                ], false
                );

                if ($success == true) {
                    // Pour chaque droit du profil
                    foreach ($rolesDroit as $roleDroit) {
                        if ($success == true) {
                            $this->Droit->clear();

                            // Création dans la table Droit chaque droit du profil en fonction de l'id de l'organisation de l'utilisateur
                            $this->Droit->create([
                                'organisation_user_id' => $userRole['OrganisationUserRole']['organisation_user_id'],
                                'liste_droit_id' => $roleDroit['RoleDroit']['liste_droit_id']
                            ]);

                            //Sauvegarde en base de données des droits
                            $success = $success && false !== $this->Droit->save(null, ['atomic' => false]);
                        }
                    }
                }
            }

            if ($success == true) {
                // L'opération c'est bien passé
                $this->Droit->commit();
                $this->Session->setFlash(__d('role', 'role.flashsuccessProfilReattribuer'), 'flashsuccess');
            } else {
                $this->Session->setFlash(__d('role', 'role.flasherrorErreurReattributionRole'), 'flasherror');
                $this->Droit->rollback();
            }
        } else {
            //Aucun utilisateur n'utilise le profil en question
            $this->Session->setFlash(__d('role', 'role.flashwarningAucunUtilisateurUtiliseProfil'), 'flashwarning');
        }

        $this->redirect($this->Referers->get());
    }

}
