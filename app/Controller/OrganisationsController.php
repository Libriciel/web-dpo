<?php

/**
 * OrganisationsController : Controller des organisations
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Controller
 */

App::uses('ListeDroit', 'Model');
App::uses('WebdpoChecks', 'Utility');

class OrganisationsController extends AppController
{

    public $uses = [
        'Cron',
        'Droit',
        'Organisation',
        'OrganisationUser',
        'OrganisationUserRoles',
        'Responsable',
        'Role',
        'RoleDroit',
        'User',
        'Valeur',
        'EtatFiche'
    ];

    /**
     * Vérification de l'accès aux actions en fonction du profil de l'utilisateur connecté.
     * Les vérifications de l'accès aux enregistrements se font dans les méthodes d'actions.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        $action = Inflector::underscore($this->request->params['action']);
        $anyone = ['change', 'changenotification', 'selectorganisation', 'politiquedeconfidentialite'];

        if ($action === 'add') {
            $this->Droits->assertSu();
        } elseif ($action === 'administrer') {
            $this->Droits->assertSu();
        } elseif ($action === 'delete') {
            $this->Droits->assertSu();
        } elseif ($action === 'edit') {
            $this->Droits->assertAuthorized([ListeDroit::MODIFIER_ORGANISATION]);
        } elseif ($action === 'gestionrgpd') {
            $this->Droits->assertSu();
        } elseif ($action === 'index') {
            $this->Droits->assertSu();
        } elseif ($action === 'politiquepassword') {
            $this->Droits->assertAuthorized([ListeDroit::CREER_UTILISATEUR]);
        } elseif ($action === 'politiquedeconfidentialite') {
            $this->Droits->assertNotSu();
        } elseif ($action === 'show') {
            $this->Droits->assertSu();
        } elseif (in_array($action, $anyone) === false) {
            throw new ForbiddenException(__d('default', 'default.flasherrorPasDroitPage'));
        }
    }

    /**
     * Accueil de la page, listing des organisations
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function index()
    {
        $this->set('title', __d('organisation', 'organisation.titreIndexEntite'));

        $query = [
            'conditions' => [],
            'contain' => [
                'OrganisationUser' => [
                    'conditions' => [
                        'Organisation.id'
                    ]
                ]
            ],
            'fields' => [
                'Organisation.id',
                'Organisation.raisonsociale'
            ],
            'order' => [
                'raisonsociale ASC'
            ]
        ];

        if ($this->request->is('post')) {
            // Filtre
            if (isset($this->request->data['Filtre']) === true) {
                // Filtre sur l'entité
                if (!empty($this->request->data['Filtre']['organisation'])) {
                    $query['conditions'] += [
                        'Organisation.id' => $this->request->data['Filtre']['organisation']
                    ];
                }
            }
        }

        $this->paginate = $query;
        $organisations = $this->paginate($this->Organisation);

        foreach ($organisations as $key => $value) {
            $organisations[$key]['Count'] = $this->OrganisationUser->find('count', [
                'conditions' => [
                    'organisation_id' => $value['Organisation']['id']
                ]
            ]);
        }

        $mesOrganisations = $this->WebcilUsers->mesOrganisations('list');
        $this->set(compact('organisations', 'mesOrganisations'));
    }

    /**
     * Gère l'ajout d'organisation
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 25/02/2019
     * @version v1.0.2
     */
    public function add()
    {
        $this->set('title', __d('organisation', 'organisation.titreAddEntite'));

        if ($this->request->is('post')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $success = true;
            $this->Organisation->begin();

            $noOrganisation = $this->Organisation->find('first');
            if (empty($noOrganisation)) {
                $this->request->data['Organisation']['rgpd'] = true;
            }

            $success = false !== $this->Organisation->saveAddEditForm($this->request->data) && $success;

            if ($success === true) {
                $idOrganisation = $this->Organisation->getInsertID();
                
                $success = $this->_insertRoles($idOrganisation);
                
                if ($success == true) {
                    $success = $this->_insertTacheAuto($idOrganisation);
                }
                
                if ($success == true) {
                    $success = $this->_insertResponsable($idOrganisation);
                }
            }

            if ($success == true) {
                $this->Organisation->commit();

                $compte = $this->Organisation->find('count');

                if ($compte > 1) {
                    $this->Session->setFlash(__d('organisation', 'organisation.flashsuccessEntiteEnregistrer'), 'flashsuccess');
                    $this->redirect($this->Referers->get());
                } else {
                    $this->Session->write('successMessage', __d('organisation', 'organisation.flashsuccessEntiteEnregistrer'));
                    $this->redirect([
                        'controller' => 'users',
                        'action' => 'logout'
                    ]);
                }
            } else {
                $this->Organisation->rollback();
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorErreurEnregistrementSEF'), 'flasherror');
            }
        }

        $options = $this->Organisation->enums();
        $this->set(compact('options'));
    }

    /**
     *
     * @param int $id
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function delete($id)
    {
        $this->Droits->assertRecordAuthorized('Organisation', $id, ['superadmin' => true]);

        $this->Organisation->begin();

		if (true === $this->Organisation->delete($id)) {
            if ($this->Session->read('Organisation.id') == $id) {
                $this->Session->delete('Organisation');
            }

			$this->Organisation->commit();
            $this->Session->setFlash(__d('organisation', 'organisation.flashsuccessEntiteSupprimer'), 'flashsuccess');

        } else {
            $this->Organisation->rollback();
            $this->Session->setFlash(__d('organisation', 'organisation.flasherrorSupprimerOrganisationImpossible'), 'flasherror');
        }

        $this->redirect($this->Referers->get());
    }

    /**
     * Gère l'affichage des informations d'une organisation
     *
     * @param int $id
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function show($id)
    {
        $this->Droits->assertRecordAuthorized('Organisation', $id, ['superadmin' => true]);

        if ($this->request->is(['post', 'put'])) {
            if ('Back' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }
        }

        if (!$id) {
            $this->Session->setFlash(__d('default', 'default.flasherrorTraitementInexistant'), 'flasherror');
            $this->redirect([
                'controller' => 'organisations',
                'action' => 'index'
            ]);
        } else {
//            $users = $this->OrganisationUser->find('all', [
//                'conditions' => [
//                    'OrganisationUser.organisation_id' => $id
//                ],
//                'contain' => [
//                    'User' => [
//                        'id',
//                        'nom',
//                        'prenom',
//                        'nom_complet'
//                    ]
//                ]
//            ]);
//            $array_users = [];
//            foreach ($users as $key => $value) {
//                $array_users[$value['User']['id']] = $value['User']['nom_complet'];
//            }
//            $this->set(compact('array_users'));

            $organisation = $this->Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $id
                ],
                'contain' => [
                    'Dpo' => [
                        'id',
                        'civilite',
                        'nom',
                        'prenom',
                        'email'
                    ]
                ]
            ]);
            
            if (!$organisation) {
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorEntiteInexistant'), 'flasherror');
                $this->redirect($this->Referers->get());
            }
            
            $this->set('title', 'Informations générales - ' . $organisation['Organisation']['raisonsociale']);
        }
        if (!$this->request->data) {
            $this->request->data = $organisation;
        }
    }

    /**
     * Gère l'édition d'une organisation
     *
     * @param type $id
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function edit($id)
    {
        $this->Droits->assertRecordAuthorized('Organisation', $id, ['superadmin' => true]);

        if ($this->request->is(['post', 'put'])) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $dpoIdNow = $this->Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $id
                ],
                'fields' => [
                    'dpo'
                ]
            ]);

            $success = true;
            $this->Organisation->begin();

            $this->Organisation->id = $id;

            $data = $this->request->data;

            $data['Organisation']['id'] = $id;

            $success = false !== $this->Organisation->saveAddEditForm($data) && $success;

            if (false === empty($data['Organisation']['dpo'])) {
                $newRoleOldDPO = null;

                if (!empty($dpoIdNow['Organisation']['dpo']) && $dpoIdNow['Organisation']['dpo'] != $data['Organisation']['dpo']) {
                    $newRoleOldDPO = $data['Organisation']['newRoleOldDPO'];
                }

                $success = $this->_attributionRoleDPO($this->request->data('Organisation.dpo'), $id, $newRoleOldDPO) && $success;
            }

            if ($success == true) {
                $this->Organisation->commit();
                $this->Session->setFlash(__d('organisation', 'organisation.flashsuccessEntiteModifier'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                $this->Organisation->rollback();
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorErreurMoficationEntite'), 'flasherror');
            }
        } else {
            $this->request->data = $this->Organisation->findById($id);

            if (true === empty($this->request->data)) {
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorEntiteInexistant'), 'flasherror');

                $this->redirect([
                    'controller' => 'organisations',
                    'action' => 'index'
                ]);
            }
        }

        // @info: nom original de l'organisation + id et extension pour le logo
        $organisation = $this->Organisation->findById($id);
        $this->set('title', __d('organisation', 'organisation.titreModifiactionEntite') . $organisation['Organisation']['raisonsociale']);

        $options = $this->Organisation->enums();

        $users = $this->OrganisationUser->find('all', [
            'conditions' => [
                'OrganisationUser.organisation_id' => $id
            ],
            'contain' => [
                'User' => [
                    'id',
                    'civilite',
                    'nom',
                    'prenom',
                    'email',
                    'nom_complet'
                ]
            ]
        ]);

        if (!empty($users)) {
            // Construction de la liste déroulante avec les utilisateurs de l'entitée
            $array_users = [];
            $idUsers = [];
            foreach ($users as $key => $value) {
                $array_users[$value['User']['id']] = $value['User']['nom_complet'];
                $idUsers[] = $value['User']['id'];
            }
            
            // On récupére en BDD tout les utilisateurs qui sont présent dans l'entitée
            $informationsUsers = $this->User->find('all', [
                'conditions' => [
                    'id' => $idUsers
                ],
                'fields' => [
                    'id',
                    'nom',
                    'prenom',
                    'email'
                ]
            ]);

            // On reformate le tableau
            $result = Hash::combine($informationsUsers, '{n}.User.id', '{n}.User');
            $result = Hash::remove($result, '{n}.id');

            $this->set('informationsUsers', $result);
        } else {
            $array_users = null;
            $this->set('informationsUsers', []);
        }

        $roles = $this->Role->find('list', [
            'conditions' => [
                'organisation_id' => $id,
                'libelle !=' => 'DPO'
            ],
            'fields' => [
                'id',
                'libelle'
            ]
        ]);

        $this->set(compact('organisation', 'options', 'array_users', 'roles'));
    }

    /**
     * @param $idDPO
     * @param $idOrganisation
     * @return bool
     */
    private function _attributionRoleDPO($idDPO, $idOrganisation, $newRoleForOldDPO = null)
    {
        $success = true;
        $droitsDPO = ListeDroit::LISTE_DROITS_DPO_MINIMUM;

        $exists = [];
        foreach ($droitsDPO as $value) {
            $exists[] = sprintf('EXISTS(SELECT * FROM role_droits INNER JOIN liste_droits ON ( role_droits.liste_droit_id = liste_droits.id ) WHERE liste_droits.value = %d AND role_droits.role_id = Role.id)', $value);
        }

        $roleDPO_id = $this->Role->find('first', [
            'fields' => [
                'Role.id'
            ],
            'conditions' => [
                'Role.organisation_id' => $idOrganisation,
                $exists
            ]
        ]);

        $organisationUserOldDPO_id = $this->OrganisationUser->find('first', [
            'joins' => [
                $this->OrganisationUser->join('OrganisationUserRole', ['type' => 'INNER']),
            ],
            'conditions' => [
                'OrganisationUserRole.role_id' => $roleDPO_id['Role']['id'],
            ],
            'fields' => [
                'id'
            ]
        ]);

        // On récupére l'id du nouveau DPO dans l'organisation
        $organisationUserNewDPO_id = $this->OrganisationUser->find('first', [
            'conditions' => [
                'user_id' => $idDPO,
                'organisation_id' => $idOrganisation
            ],
            'fields' => [
                'id'
            ]
        ]);

        // Ancien DPO
        if (!empty($organisationUserOldDPO_id['OrganisationUser']['id']) && !empty($newRoleForOldDPO)) {
            // On supprime tout les droits de l'ancien DPO
            $success = $this->Droit->deleteAll([
                'organisation_user_id' => $organisationUserOldDPO_id['OrganisationUser']['id']
            ]);

            if ($success == true) {
                // Liste des droits du profils selectionner pour l'ancien DPO
                $query = [
                    'conditions' => [
                        'RoleDroit.role_id' => $newRoleForOldDPO
                    ]
                ];
                $listeDroitsProfil = $this->RoleDroit->find('all', $query);

                // On attribue les droits du profils selectionné pour l'ancien DPO
                foreach ($listeDroitsProfil as $listeDroitProfil) {
                    $this->Droit->create([
                        'Droit' => [
                            'organisation_user_id' => $organisationUserOldDPO_id['OrganisationUser']['id'],
                            'liste_droit_id' => $listeDroitProfil['RoleDroit']['liste_droit_id']
                        ]
                    ]);
                    $success = $success && false !== $this->Droit->save(null, ['atomic' => false]);
                }

                // On supprime l'ancien role de l'ancien DPO
                $success = $success && $this->OrganisationUserRoles->deleteAll([
                    'organisation_user_id' => $organisationUserOldDPO_id['OrganisationUser']['id']
                ]);

                // On lui attribut un nouveau role selectionné à l'ancien DPO
                $this->OrganisationUserRoles->create([
                    'OrganisationUserRoles' => [
                        'organisation_user_id' => $organisationUserOldDPO_id['OrganisationUser']['id'],
                        'role_id' => $newRoleForOldDPO
                    ]
                ]);
                $success = $success && false !== $this->OrganisationUserRoles->save(null, ['atomic' => false]);
            }
        }

        // Nouveau DPO
        if ($success == true) {
            // On supprime tout les droits du nouveau DPO
            $success = $success && $this->Droit->deleteAll([
                'organisation_user_id' => $organisationUserNewDPO_id['OrganisationUser']['id']
            ]);

            // Liste des droits du profils DPO
            $query = [
                'conditions' => [
                    'RoleDroit.role_id' => $roleDPO_id['Role']['id']
                ]
            ];
            $listeDroitsDPO = $this->RoleDroit->find('all', $query);

            // On attribue les droits du profils DPO au nouveau DPO
            foreach($listeDroitsDPO as $droit) {
                $record = [
                    'Droit' => [
                        'organisation_user_id' => $organisationUserNewDPO_id['OrganisationUser']['id'],
                        'liste_droit_id' => Hash::get($droit, 'RoleDroit.liste_droit_id')
                    ]
                ];

                $this->Droit->create($record);
                $success = false !== $this->Droit->save(null, ['atomic' => false]) && $success;
            }

            if ($success == true) {
                // On supprime l'ancien role du nouveau DPO
                $success = $success &&  $this->OrganisationUserRoles->deleteAll([
                    'organisation_user_id' => $organisationUserNewDPO_id['OrganisationUser']['id']
                ]);

                // On attribue le profil "DPO' au nouceau DPO
                $this->OrganisationUserRoles->create([
                    'OrganisationUserRoles' => [
                        'organisation_user_id' => $organisationUserNewDPO_id['OrganisationUser']['id'],
                        'role_id' => $roleDPO_id['Role']['id']
                    ]
                ]);
                $success = $success && false !== $this->OrganisationUserRoles->save(null, ['atomic' => false]);
            }
        }

        return $success;
    }

    /**
     * Change l'organisation si besoin et redirige vers la bonne view
     *
     * @param int|null $id
     * @param string|null $controller
     * @param string|null $action
     * @param int|0 $idFicheNotification
     *
     * @access public
     * @created 08/01/2016
     * @version V1.0.0
     */
    public function changenotification($id = null, $controller = null, $action = null, $idFicheNotification = 0)
    {
        $success = true;
        $this->Notification->begin();

        $idArray = $this->OrganisationUser->find('first', [
            'conditions' => [
                'OrganisationUser.user_id' => $this->Auth->user('id')
            ]
        ]);

        $success = $success && $this->Notification->updateAll([
                    'Notification.vu' => true,
                        ], [
                    'Notification.user_id' => $this->Auth->user('id'),
                    'Notification.fiche_id' => $idFicheNotification
                ]) !== false;

        if ($success == true) {
            $this->Notification->commit();
            if (empty($idArray) === true || $id != $idArray['OrganisationUser']['organisation_id']) {
                $redirect = 1;
                $this->Session->write('idFicheNotification', $idFicheNotification);
                $this->redirect([
                    'controller' => 'organisations',
                    'action' => 'change',
                    $id,
                    $redirect,
                    $controller,
                    $action
                ]);
            } else {
                $this->Session->write('idFicheNotification', $idFicheNotification);
                $this->redirect([
                    'controller' => $controller,
                    'action' => $action,
                    $idFicheNotification
                ]);
            }
        } else {
            $this->Formulaire->rollback();
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }
    }

    /**
     * Changement d'organisation
     *
     * @param int|null $id
     * @param type|null $redirect
     * @param type|null $controller
     * @param type|null $action
     *
     * @access public
     *
     * @created 17/06/2015
     * @version V1.0.0
     *
     * @modified 09/02/2021
     * @version V2.1.0
     */
    public function selectorganisation()
    {
        $this->layout = 'login';
        
        if ($this->request->is('post')) {
            $this->redirect([
                'controller' => 'organisations',
                'action' => 'change',
                $this->request->data('Organisation.organisationcible')
            ]);
        }

        $organisationsUser = $this->OrganisationUser->find('list', [
            'conditions' => [
                'user_id' => $this->Session->read('Auth.User.id'),
            ],
            'fields' => [
                'id',
                'organisation_id'
            ]
        ]);
        
        if (count($organisationsUser) > 1) {
            $orgaListe = $this->Organisation->find('list', [
                'conditions' => [
                    'id' => $organisationsUser
                ],
                'fields' => [
                    'id',
                    'raisonsociale'
                ],
                'order' => [
                    'raisonsociale ASC'
                ]
            ]);

            $filename = FICHIER_CONFIGURATION_LOGIN;
            if (file_exists($filename)) {
                $fileConfigLogin = fopen($filename, 'r');
                $configLoginExistante = fread($fileConfigLogin, filesize($filename));
                fclose($fileConfigLogin);
            } else {
                $configLoginExistante = '';
            }

            $this->set(compact('orgaListe', 'configLoginExistante'));
        } else {
            $this->redirect([
                'controller' => 'organisations',
                'action' => 'change'
            ]);
        }
    }
    
    /**
     * Changement d'organisation
     *
     * @param int|null $id
     * @param type|null $redirect
     * @param type|null $controller
     * @param type|null $action
     *
     * @access public
     * @created 17/06/2015
     * @version V1.0.0
     */
    public function change($id = null, $redirect = 0, $controller = null, $action = null)
    {
        // @todo : issues/255
//        $sessionSuperadminWrite = false;

        if (isset($this->request->data['Organisation']['organisationcible'])) {
            $id = $this->request->data['Organisation']['organisationcible'];
//            $sessionSuperadminWrite = true; // @todo : issues/255
        }

        if ($id === 'null') {
            $id = trim((string)Hash::get($this->request->data, 'Organisation.organisationcible'));
            $id = $id === '' ? null : $id;
        }

        $userId = $this->Auth->user('id');

        if ($id == null) {
            $idArray = $this->OrganisationUser->find('first', [
                'conditions' => [
                    'OrganisationUser.user_id' => $userId
                ]
            ]);
            
            if (!empty($idArray)) {
                $id = $idArray['OrganisationUser']['organisation_id'];
            } else {
                if ($this->Droits->isSu()) {
                    $compte = $this->Organisation->find('count');
                    
                    if ($compte == 0) {
                        $this->Session->setFlash(__d('organisation', 'organisation.flashwarningAucuneEntite'), 'flashwarning');
                        $this->redirect([
                            'controller' => 'organisations',
                            'action' => 'add'
                        ]);
                    } else {
                        $idOrga = $this->Organisation->find('first');
                        $id = $idOrga['Organisation']['id'];
                    }
                } else {
                    $this->Session->setFlash(__d('organisation', 'organisation.flasherrorUserAucuneEntite'), 'flasherror');
                    $this->redirect([
                        'controller' => 'users',
                        'action' => 'logout'
                    ]);
                }
            }
        }

        // @todo : issues/255
//        if ($this->Session->read('Su') === true && $sessionSuperadminWrite === true) {
            $change = $this->Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $id
                ]
            ]);
            $this->Session->write('Organisation', $change['Organisation']);
//        }

        $service = $this->OrganisationUser->find('all', [
            'conditions' => [
                'user_id' => $userId,
                'organisation_id' => $id
            ],
            'contain' => [
                'OrganisationUserService' => [
                    'Service'
                ]
            ]
        ]);
        $serviceUser = Hash::extract($service, '{n}.OrganisationUserService.Service');
        $serviceUser = Hash::combine($serviceUser, '{n}.id', '{n}.libelle');
        $this->Session->write('User.service', $serviceUser);

        if(!empty($this->Session->read('Auth.User.uuid'))){
            $this->Session->delete('Auth.User.uuid');
        }

        $droits = $this->Droit->find('all', [
            'conditions' => [
                'OrganisationUser.user_id' => $userId,
                'OrganisationUser.organisation_id' => $id
            ],
            'contain' => [
                'ListeDroit' => [
                    'value'
                ],
                'OrganisationUser' => [
                    'id'
                ]
            ]
        ]);

        $result = [];
        foreach ($droits as $value) {
            array_push($result, $value['ListeDroit']['value']);
        }

        if (empty($result) && !$this->Droits->isSu()) {
            $this->Session->setFlash(__d('organisation', 'organisation.flasherrorAucunDroitEntite'), 'flasherror');
            $this->redirect([
                'controller' => 'pannel',
                'action' => 'index'
            ]);
        } elseif ($this->Droits->isSu() === true && $redirect == 0) {
            $this->Session->write('Droit.liste', $result);
            $this->redirect([
                'controller' => 'checks'
            ]);
        } else {
            if ($redirect != 1) {
                $this->Session->write('Droit.liste', $result);
                $this->redirect([
                    'controller' => 'pannel',
                    'action' => 'index'
                ]);
            } else {
                $this->Session->write('Droit.liste', $result);

                if ($controller != null && $action != null) {
                    $this->redirect([
                        'controller' => $controller,
                        'action' => $action,
                    ]);
                } else {
                    $this->redirect($this->referer());
                }
            }
        }
    }

    /**
     * Création du responsable lors de la création d'une nouvelle entité
     *
     * @param int $idOrganisation | id de l'organisation créé
     * @return type
     */
    protected function _insertResponsable($idOrganisation)
    {
        $responsable = $this->Responsable->find('first', [
            'conditions' => [
                'siretstructure' => str_replace(' ', '', $this->request->data('Organisation.siret'))
            ]
        ]);

        $success = true;

        if (empty($responsable)) {
            $this->Responsable->create([
                'nomresponsable' => $this->request->data('Organisation.nomresponsable'),
                'prenomresponsable' => $this->request->data('Organisation.prenomresponsable'),
                'emailresponsable' => $this->request->data('Organisation.emailresponsable'),
                'telephoneresponsable' => $this->request->data('Organisation.telephoneresponsable'),
                'fonctionresponsable' => $this->request->data('Organisation.fonctionresponsable'),
                'raisonsocialestructure' => $this->request->data('Organisation.raisonsociale'),
                'siretstructure' => $this->request->data('Organisation.siret'),
                'apestructure' => $this->request->data('Organisation.ape'),
                'telephonestructure' => $this->request->data('Organisation.telephone'),
                'faxstructure' => $this->request->data('Organisation.fax'),
                'adressestructure' => $this->request->data('Organisation.adresse'),
                'emailstructure' => $this->request->data('Organisation.email'),
                'createdbyorganisation' => $idOrganisation
            ]);
            $success = $success && false !== $this->Responsable->save(null, ['atomic' => false]);

            if ($success == true) {
                $success = $success && $this->Organisation->updateAll([
                        'responsable_id' => $this->Responsable->id
                    ], [
                        'id' => $idOrganisation
                    ]) !== false;
            }
        }

        return $success;
    }

    /**
     * @param int|null $id
     *
     * @access protected
     * @created 29/04/2015
     * @version V1.0.0
     */
    protected function _insertRoles($id = null)
    {
        if ($id != null) {
            $success = true;

            $data = [
                [
                    'Role' => [
                        'libelle' => 'Rédacteur',
                        'organisation_id' => $id
                    ],
                    'Droit' => ListeDroit::LISTE_DROITS_REDACTEUR
                ],
                [
                    'Role' => [
                        'libelle' => 'Valideur',
                        'organisation_id' => $id
                    ],
                    'Droit' => ListeDroit::LISTE_DROITS_VALIDEUR
                ],
                [
                    'Role' => [
                        'libelle' => 'Consultant',
                        'organisation_id' => $id
                    ],
                    'Droit' => ListeDroit::LISTE_DROITS_CONSULTANT
                ],
                [
                    'Role' => [
                        'libelle' => 'Administrateur',
                        'organisation_id' => $id
                    ],
                    'Droit' => ListeDroit::LISTE_DROITS_ADMINISTRATEUR
                ],
                [
                    'Role' => [
                        'libelle' => 'DPO',
                        'organisation_id' => $id
                    ],
                    'Droit' => ListeDroit::LISTE_DROITS_DPO
                ]
            ];

            foreach ($data as $key => $value) {
                if ($success == true) {
                    $this->Role->create($value['Role']);

                    $success = $success && false !== $this->Role->save(null, ['atomic' => false]);

                    $last = $this->Role->getInsertID();

                    if ($success == true) {
                        foreach ($value['Droit'] as $valeur) {
                            if ($success == true) {
                                $this->RoleDroit->create([
                                    'RoleDroit' => [
                                        'role_id' => $last,
                                        'liste_droit_id' => $valeur
                                    ]
                                ]);

                                $success = $success && false !== $this->RoleDroit->save(null, ['atomic' => false]);
                            }
                        }
                    }
                }
            }

            if ($success == true) {
                return (true);
            } else {
                $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
                return (false);
            }
        }
    }

    protected function _insertTacheAuto($id)
    {
        $crons = [
            [
                'Cron' => [
                    'organisation_id' => $id,
                    'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
                    'action' => 'syncLdap',
                    'active' => 'false',
                    'lock' => 'false'
                ]
            ],
            [
                'Cron' => [
                    'organisation_id' => $id,
                    'nom' => 'Conversion des annexes',
                    'action' => 'conversionAnnexes',
                    'active' => 'true',
                    'lock' => 'false',
                    'next_execution_time' => date('Y-m-d H:i'),
                    'execution_duration' => 'PT20M',
                ]
            ],
        ];

        $results = Hash::flatten($this->Cron->saveAll($crons, ['atomic' => false]));
        $success = in_array(false, $results, true) === false;

        if ($success !== true) {
            $this->Session->setFlash(__d('default', 'default.flasherrorEnregistrementErreur'), 'flasherror');
        }

        return $success;
    }

    /**
     * @fixme: dans l'entité/l'organisation
     */
    public function politiquepassword()
    {
        $this->set('title', __d('organisation', 'organisation.titrePolitiquePassword'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            $this->Organisation->id = $this->Session->read('Organisation.id');
            $success = $this->Organisation->saveField('force', Hash::get($this->request->data, 'Organisation.force'));

            if (false !== $success) {
                $this->Session->setFlash(__d('organisation', 'organisation.flashsuccessForcePasswordEnregistrer'), 'flashsuccess');
                $this->redirect($this->Referers->get());
            } else {
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorForcePasswordEnregistrer'), 'flasherror');
            }
        }

        if (empty($this->request->data) === true) {
            $this->request->data = $this->Organisation->find('first', [
                'conditions' => [
                    'id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'force'
                ]
            ]);
        }

//        $genereteAnssiPasswd = PasswordAnssi::generate();
//        debug($genereteAnssiPasswd);
//        
//        $genereteAnssiPasswd = "correcthorsebatterystaple";
//        
//        $entropie = PasswordAnssi::entropyBits($genereteAnssiPasswd);
//        debug($entropie);
//        
//        $force = PasswordAnssi::strength($genereteAnssiPasswd);
//        debug($force);
//        $exemples = [
//            'argfhyrn*kf5' => null,
//            // 1
//            '012345678901' => null,
//            'azertyuiop' => null,
//            // 2
//            '01234567890123456789' => null,
//            'Ym6;Q99,Ui' => null,
//            // 3
//            'azertyuiopazertyuiop' => null,
//            // 5
//            'bureau chaise colonie souper' => null,
//            'correcthorsebatterystaple' => null,
//        ];
//        foreach(array_keys($exemples) as $exemple) {
//            $exemples[$exemple] = [
//                'entropie' => PasswordAnssi::entropyBits($exemple),
//                'force' => PasswordAnssi::strength($exemple)
//            ];
//        }
//        debug($exemples);
    }

    /**
     *
     * @param type $id
     */
    public function administrer()
    {
        $this->set('title', __d('default', 'default.titreAdministration') . ': ' . $this->Session->read('Organisation.raisonsociale'));
    }

    /**
     * Affichage politique de configentialité
     *
     * @access public
     *
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     * @created 06/03/2019
     * @version V1.0.2
     */
    public function politiquedeconfidentialite()
    {
        $this->set('title', __d('organisation', 'organisation.titrePolitiqueDeConfidentialite'));

        $query = [
            'conditions' => [
                'Organisation.id' => $this->Session->read('Organisation.id')
            ],
            'fields' => [
                'Organisation.raisonsociale',
                'Organisation.telephone',
                'Organisation.adresse',
                'Organisation.email',
                'Organisation.siret',
                'Organisation.ape',
                'Organisation.responsable_nom_complet',
                'Organisation.fonctionresponsable',
                'Organisation.dpo',
                'Organisation.emaildpo'
            ],
            'contain' => [
                'Dpo' => [
                    'fields' => [
                        'nom_complet' => $this->Organisation->Dpo->getVirtualFieldSql('nom_complet')
                    ]
                ]
            ]
        ];
        $organisationResponsableTraitement = $this->Organisation->find('first', $query);

        $organisationHebergeur = [];
        if (Configure::read('MODE_SAAS') === false) {
            // L'application est héberger par le client
            $organisationHebergeur = $this->Organisation->find('first', [
                'conditions' => [
                    'Organisation.rgpd' => true
                ],
                'fields' => [
                    'Organisation.raisonsociale',
                    'Organisation.telephone',
                    'Organisation.adresse',
                    'Organisation.email',
                    'Organisation.siret',
                    'Organisation.ape',
                    'Organisation.responsable_nom_complet',
                    'Organisation.fonctionresponsable',
                ],
            ]);
        }

        $this->set(compact('organisationResponsableTraitement', 'organisationHebergeur'));
    }

    /**
     *
     */
    public function gestionrgpd()
    {
        $this->set('title', __d('organisation', 'organisation.titreEntiteeResponsablePlateForme'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if ('Cancel' === Hash::get($this->request->data, 'submit')) {
                $this->redirect($this->Referers->get());
            }

            // Tentative de sauvegarde
            $this->Organisation->begin();
            $success = true;

            $idOrganisationRGPDSelect = $this->request->data('Organisation.rgpd');

            if (empty($idOrganisationRGPDSelect)) {
                $success = false;
            }

            $rgpdTrue = $this->Organisation->find('all', [
                'fields' => [
                    'Organisation.id'
                ],
                'conditions' => [
                    'Organisation.rgpd' => true
                ]
            ]);

            if (!empty($rgpdTrue)) {
                foreach ($rgpdTrue as $rgpdTrueEntiteid) {
                    $this->Organisation->id = $rgpdTrueEntiteid['Organisation']['id'];
                    $success = false !== $this->Organisation->save(['rgpd' => false], ['atomic' => false]) && $success;
                }
            }

            if ($success === true) {
                $this->Organisation->id = $idOrganisationRGPDSelect;
                $success = false !== $this->Organisation->save(['rgpd' => true], ['atomic' => false]) && $success;
            }

            if ($success === true) {
                $this->Organisation->commit();
                $this->Session->setFlash(__d('organisation', 'organisation.flashsuccessEntiteeResponsablePlateFormeEnregistrer'), 'flashsuccess');

                $this->redirect($this->Referers->get());
            } else {
                $this->Organisation->rollback();
                $this->Session->setFlash(__d('organisation', 'organisation.flasherrorErreurEnregistrementSEF'), 'flasherror');
            }
        }

        $listeOrganisations = $this->Organisation->find('list');

        $organisationRGPD = $this->Organisation->find('first', [
            'fields' => [
                'Organisation.id'
            ],
            'conditions' => [
                'Organisation.rgpd' => true
            ]
        ]);

        if (empty($organisationRGPD['Organisation']['id'])) {
            $organisationRGPD = null;
        }

        $this->set(compact('listeOrganisations', 'organisationRGPD'));
    }
}
