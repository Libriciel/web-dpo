<?php
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('CakeSession', 'Model/Datasource');
App::uses('Configure', 'Core');

/**
 * La classe UserFriendlyExceptionHandler permet de remplacer le gestionnaire d'exceptions pour transformer celles-ci
 * en messages flash lorsque l'exception est de type 403 (ForbiddenException) ou 404 (MissingActionException,
 * MissingControllerException, MissingPluginException ou NotFoundException).
 *
 * Usage, dans le fichier bootstrap.php:
 * App::uses('UserFriendlyExceptionHandler', 'Lib');
 * UserFriendlyExceptionHandler::setUp();
 */
class UserFriendlyExceptionHandler
{
    public static function setUp() {
        $originalExceptionHandler = Configure::read('Exception.handler');

        Configure::write('Exception.handler', function ($exception) use ($originalExceptionHandler) {
            static::customExceptionHandler($exception, $originalExceptionHandler);
        });
    }

    public static function customExceptionHandler($exception, $originalExceptionHandler) {
        $request = new CakeRequest();

        if ($request->is('ajax') === true) {
            $originalExceptionHandler($exception);
        }

        $message = $exception->getMessage();

        if ($exception instanceof ForbiddenException) {
            if ($message === 'Forbidden') {
                $message = 'Vous n\'avez pas le droit d\'acceder à cette page';
            }
        } elseif ($exception instanceof MissingActionException) {
            if (preg_match('/^Action .* could not be found/', $message) === 1) {
                $message = 'Page non trouvée';
            }
        } elseif ($exception instanceof MissingControllerException) {
            if (preg_match('/^Controller class .* could not be found/', $message) === 1) {
                $message = 'Page non trouvée';
            }
        } elseif ($exception instanceof MissingPluginException) {
            if (preg_match('/^Plugin .* could not be found/', $message) === 1) {
                $message = 'Page non trouvée';
            }
        } elseif ($exception instanceof NotFoundException) {
            if ($message === 'Not Found') {
                $message = 'Enregistrement non trouvé';
            }
        } else {
            $originalExceptionHandler($exception);
        }

        $connected = empty(CakeSession::read('Auth.User.id')) === false;
        $superadmin = $connected && CakeSession::read('Su');
        $current = Router::url(null, false);
        $redirect = CakeSession::read('Auth.redirect');

        if ($connected === false) {
            $redirect = '/users/login';
        } elseif ($redirect === null) {
            $redirect = $request->referer(true);
        }

        // @see app/Config/routes.php et app/Controller/PannelController.php
        if ($connected === true && $redirect === null && in_array($current, ['/', '/pannel', '/pannel/index']) === false) {
            if ($superadmin === true) {
                $redirect = '/pannel/superadmin';
            } else {
                $redirect = '/pannel/index';
            }
        }

        if ($redirect !== null && $redirect !== $current) {
            $count = count(Hash::extract((array)CakeSession::read(), 'Message.flash.{n}'));
            //$exceptions = Hash::extract(CakeSession::read(), 'Message.flash.{n}[element=/error/]');

            $flash = ['message' => $message, 'element' => 'flasherror'];
            CakeSession::write(sprintf("Message.flash.%d", $count), $flash);

            $response = new CakeResponse();
            $response->header('Location', Router::url($redirect, true));
            $response->statusCode(302);
            $response->send();
        } else {
            $originalExceptionHandler($exception);
        }
    }
}
