<?php
/**
* Exception lancée lorsqu'une action bloquante est déjà effectuée par un
* autre utilisateur.
*
* @package app.Lib.Error
*/
class LockedActionException extends CakeException
{

    /**
        * Constructor
        *
        * @param type $message
        * @param type $code
        * @param type $params
        */
    public function __construct( $message, $code = 401, $params = array( ) ) {
            parent::__construct( $message, $code, $params );
            $this->_attributes = $params;
    }

}
