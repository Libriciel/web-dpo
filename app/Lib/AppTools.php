<?php

/**
 * AppTools
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Lib
 */

class AppTools {

    /**
     * Ajout ou soustrait un délai (xs:duration) à une date
     * 
     * @param string $date date
     * @param string $duration délai sous la forme xs:duration
     * @param string $format format de sortie de la date
     * @param string $operateur 'add' pour l'ajout et 'sub' pour la soustraction du délai
     * @return string résultat formaté ou null en cas d'erreur
     */
    public static function addSubDurationToDate($date, $duration, $format = 'Y-m-d', $operateur = 'add') {
        // initialisation
        $ret = null;
        try {
            $thisDate = new DateTime($date);
            $thisDuration = new DateInterval($duration);
            
            if ($operateur == 'add') {
                $thisDate->add($thisDuration);
            } elseif ($operateur == 'sub') {
                $thisDate->sub($thisDuration);
            }
            
            $ret = $thisDate->format($format);
        } catch (Exception $e) {
            debug('Fonction webdelib::addSubDurationToDate : ' . $e->getMessage());
        }
        
        return $ret;
    }

    /**
     * formate une date issue de la base de donnée
     * 
     * @param string $dateBD date issue de la lecture d'un enregistrement en base de données
     * @param string $format format de sortie utilisée par la fonction date()
     * @return string date mise en forme
     */
    public static function timeFormat($dateBD, $format = 'Y-m-d') {
        if (empty($dateBD)) {
            return '';
        }
        
        $dateTime = strtotime($dateBD);
        
        return date($format, $dateTime);
    }

    /**
     * formate une xs:duration sous forme litérale
     * 
     * @param string $duration délai sous la forme xs:duration
     * @return string délai mise en forme litérale
     */
    public static function durationToString($duration) {
        // initialisation
        $format = [];
        
        if (empty($duration)) {
            return '';
        }
        
        $thisDuration = new DateInterval($duration);
        $annees = $thisDuration->y;
        $mois = $thisDuration->m;
        $jours = $thisDuration->d;
        $heures = $thisDuration->h;
        $minutes = $thisDuration->i;
        $secondes = $thisDuration->s;
        
        if (!empty($annees)) {
            $format[] = $annees > 1 ? '%y ans' : '%y an';
        }
        if (!empty($mois)) {
            $format[] = '%m mois';
        }
        if (!empty($jours)) {
            $format[] = $jours > 1 ? '%d jours' : '%d jour';
        }
        if (!empty($heures)) {
            $format[] = $heures > 1 ? '%h heures' : '%h heure';
        }
        if (!empty($minutes)) {
            $format[] = $minutes > 1 ? '%i minutes' : '%i minute';
        }
        if (!empty($secondes)) {
            $format[] = $secondes > 1 ? '%s secondes' : '%s seconde';
        }

        if (empty($format)) {
            return '';
        } else {
            return $thisDuration->format(implode(', ', $format));
        }
    }

    /**
     * transforme une xs:duration sous forme de tableau
     * 
     * @param string $duration délai sous la forme xs:duration
     * @return array délai sous forme de tableau array('year', 'month', 'day', 'hour', 'minute', 'seconde')
     */
    public static function durationToArray($duration) {
        // initialisation
        $ret = ['year' => 0, 'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 'seconde' => 0];

        if (!empty($duration)) {
            $thisDuration = new DateInterval($duration);
            $ret['year'] = $thisDuration->y;
            $ret['month'] = $thisDuration->m;
            $ret['day'] = $thisDuration->d;
            $ret['hour'] = $thisDuration->h;
            $ret['minute'] = $thisDuration->i;
            $ret['seconde'] = $thisDuration->s;
        }

        return $ret;
    }

    /**
     * transforme une tableau (array('year', 'month', ...)) en xs:duration ('D1Y...')
     * @param array $duration délai sous forme de tableau array('year', 'month', 'day', 'hour', 'minute', 'seconde')
     * @return string délai sous la forme xs:duration
     */
    public static function arrayToDuration($duration) {
        // initialisation
        $ret = $periode = $temps = '';
        $defaut = ['year' => 0, 'month' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 'seconde' => 0];

        if (empty($duration) || !is_array($duration)) {
            return '';
        }

        $duration = array_merge($defaut, $duration);
        if (!empty($duration['year'])) {
            $periode .= $duration['year'] . 'Y';
        }
        if (!empty($duration['month'])) {
            $periode .= $duration['month'] . 'M';
        }
        if (!empty($duration['day'])) {
            $periode .= $duration['day'] . 'D';
        }
        if (!empty($duration['hour'])) {
            $temps .= $duration['hour'] . 'H';
        }
        if (!empty($duration['minute'])) {
            $temps .= $duration['minute'] . 'M';
        }
        if (!empty($duration['seconde'])) {
            $temps .= $duration['seconde'] . 'S';
        }

        if (!empty($periode) || !empty($temps)) {
            $ret = 'P' . $periode;
            if (!empty($temps)) {
                $ret .= 'T' . $temps;
            }
        }

        return $ret;
    }
    
}
