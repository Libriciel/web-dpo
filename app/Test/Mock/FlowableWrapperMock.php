<?php

namespace App\Test\Mock;

use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Hash;
use Ramsey\Uuid\Uuid;
use WrapperFlowable\Api\FlowableFacade;
use WrapperFlowable\Api\FlowableWrapperInterface;
use WrapperFlowable\Model\Entity\InstanciableInterface;

/**
 * Class FlowableWrapperMock
 *
 * Call FlowableWrapperMock::reset() on each test setup to reset
 */
class FlowableWrapperMock implements FlowableWrapperInterface
{
    /**
     * @var FlowableWrapperMock|null
     */
    protected static $_instance = null;

    /**
     * @var array[]
     */
    protected static $calls = [];

    protected function __construct()
    {
        self::reset();
    }

    public static function getInstance()
    {
        return self::$_instance ?? new FlowableWrapperMock();
    }

    /**
     * Reset the calls iterations
     * @return void
     */
    public static function reset(): void
    {
        self::$calls = [];

        foreach (get_class_methods(self::class) as $method) {
            self::$calls[self::class . '::' . $method] = 0;
        }
    }

    public function getDeployments(): array
    {
        return [];
    }

    public function getDeployment(string $deploymentId): array
    {
        return [];
    }

    /**
     * @param array $data data
     * @return array
     * @throws \Exception
     */
    public function createDeployment(array $data): array
    {
        // based on the name given at the deployment creation
        return [
            '4 -> 5 -> 6' => [ // test doublon avec nom déjà existant
                'id' => '',
                'name' => $data['deploymentName'],
                'deploymentTime' => '2019-10-15T09:42:58.744Z',
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000001',
            ],
            'not instancied circuit' => [ // test doublon avec nom déjà existant
                'id' => '',
                'name' => $data['deploymentName'],
                'deploymentTime' => '2019-10-15T09:42:58.744Z',
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000001',
            ],
            'newCircuit' => [
                'id' => '',
                'name' => $data['deploymentName'],
                'deploymentTime' => '2019-10-15T09:42:58.744Z',
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000002',
            ],
            'modified name' => [
                'id' => '',
                'name' => $data['deploymentName'],
                'deploymentTime' => '2019-10-15T09:42:58.744Z',
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000003',
            ],
        ][$data['deploymentName']];
    }

    public function deleteDeployment(string $deploymentId): void
    {
    }

    public function createInstance(string $processDefinitionKey): array
    {
        return [
            'businessKey' => Uuid::uuid4()->toString(),
        ];
    }

    public function getInstanceHistoryByBusinessKey(string $businessKey): array
    {
        self::$calls[__METHOD__]++;
        switch (self::$calls[__METHOD__]) {
            case 1:
            case 2:
                return [
                    'endActivityId' => FlowableFacade::STATUS_PENDING,
                ];
            case 3:
                return [
                    'endActivityId' => FlowableFacade::STATUS_APPROVED,
                ];
        }
    }

    /**
     * @param InstanciableInterface $entity
     * @param int $userId
     * @return array
     * @throws \Exception
     */
    public function getInstanceDetails(InstanciableInterface $entity, int $userId = -1): array
    {
        if ($entity->getWorkflow() === null) {
            return [];
        }

        return [
            1 => [ // once instancied
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 4,
            ],
            3 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 4,
            ],
            4 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 4 || $userId === 5,
            ],
            5 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 5,
            ],
            6 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 5,
            ],
            7 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 4,
            ],
            8 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 4 || $userId === 5,
            ],
            9 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 5,
            ],
            10 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => false,
                'isRejected' => false,
                'currentStepIndex' => 0,
                'canPerformAnAction' => $userId === 5,
            ],
            11 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            12 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            13 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            14 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            15 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            16 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            17 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            18 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => false,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            21 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => true,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
            22 => [
                'steps' => $this->getSteps($entity->getWorkflow()->getProcessDefinitionKey()),
                'isOver' => true,
                'isRejected' => true,
                'currentStepIndex' => null,
                'canPerformAnAction' => false,
            ],
        ][$entity->id];
    }

    public function deleteInstance(string $key): void
    {
    }

    /**
     * @param int $userId
     * @return array
     * @throws \Exception
     */
    public function getTasksByCandidateUser(int $userId): array
    {
        return [
            1 => [],
            4 => [
                [
                    'id' => 'task1',
                    'name' => '4',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '4, 5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '4',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0007-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '4, 5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
            ],
            5 => [
                [
                    'id' => 'task1',
                    'name' => '4, 5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0005-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0006-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '4, 5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0009-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
                [
                    'id' => 'task1',
                    'name' => '5',
                    'createTime' => '2019-11-04T07:59:44.481Z',
                    'taskDefinitionKey' => 'task_validation',
                    'parentTaskId' => null,
                    'executionId' => Uuid::uuid4(),
                    'processInstanceId' => Uuid::uuid4(),
                    'processDefinitionId' => Uuid::uuid4(),
                    'business_id' => Uuid::uuid4(),
                    'variables' => [
                        [
                            'name' => 'business_id',
                            'value' => 'projects-0010-aaaa-aaaa-aaaaaaaaaaaa',
                        ],
                    ],
                ],
            ],
        ][$userId];
    }

    public function getTask(string $taskId): array
    {
        return [];
    }

    public function getTaskVariables(string $taskId): array
    {
        return [
            [
                'name' => 'business_id',
                'value' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            [
                'name' => 'users_candidate',
                'value' => '4',
            ],
        ];
    }

    public function getHistoryTaskById(string $taskId): array
    {
        return [
            'id' => 'task1',
            'name' => '4',
            'createTime' => '2019-11-04T07:59:44.481Z',
            'taskDefinitionKey' => 'task_validation',
            'parentTaskId' => null,
            'executionId' => '10357d39-fed9-11e9-9526-0242ac120009',
            'processInstanceId' => '1035561b-fed9-11e9-9526-0242ac120009',
            'processDefinitionId' => '6e475b80-cfae-11e9-9e19-0242ac150005',
            'business_id' => '77aaeb32-8b12-4803-9ff3-76a8021d211e',
            'variables' => [
                [
                    'name' => 'business_id',
                    'value' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
                ],
                [
                    'name' => 'users_candidate',
                    'value' => '4',
                ],
            ],
        ];
    }

    public function getTaskHistoryByBusinessId(string $businessId): array
    {
        return [];
    }

    public function getAdminTaskByBusinessId(string $businessId): array
    {
        return [
            [
                'id' => '',
            ],
        ];
    }

    public function executeAnActionOnTask(array $data): void
    {
    }

    public function executeAnActionForwardOnTask(string $taskId): void
    {
    }

    public function filterInstancesWhereUserIsCurrent(int $userId, array $instanceIds): array
    {
        self::$calls[__METHOD__]++;
        switch (self::$calls[__METHOD__]) {
            case 1:
                $data = [
                    1 => [],
                    2 => [],
                    4 => [
                        'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0007-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
                    ],
                    5 => [
                        'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0005-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0006-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0009-aaaa-aaaa-aaaaaaaaaaaa',
                        'projects-0010-aaaa-aaaa-aaaaaaaaaaaa',
                    ],
                    6 => [],
                ];
                break;
            default:
                throw new NotFoundException();
        }

        return array_intersect($instanceIds, $data[$userId]);
    }

    public function isUserCurrentForInstance(int $userId, string $instanceId): bool
    {
        return [
            4 => [
                'projects-0003-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0004-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0007-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0008-aaaa-aaaa-aaaaaaaaaaaa' => true,
            ],
            5 => [
                'projects-0004-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0005-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0006-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0008-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0009-aaaa-aaaa-aaaaaaaaaaaa' => true,
                'projects-0010-aaaa-aaaa-aaaaaaaaaaaa' => true,
            ],
            6 => [
            ],
        ][$userId][$instanceId] ?? false;
    }

    public function getInstancesByProcessDefinitionId(string $processDefinitionId): array
    {
        // NOTE: for now only the count is used (to compute workflow.is_deletable), so the return is pretty simple
        return [
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001' => [
                'project03',
                'project05',
                'project07',
                'project09',
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000002' => [
                'project04',
                'project08',
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000003' => [
                'project06',
                'project10',
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000004' => [
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000005' => [
            ],
            // created dynamically and return does not matter
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000001' => [
            ],
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000002' => [
            ],
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000003' => [
            ],
        ][$processDefinitionId];
    }

    public function getSteps(string $processDefinitionKey): array
    {
        return [
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001' => [
                [
                    'name' => '4',
                    'validators' => [4],
                ],
                [
                    'name' => '5',
                    'validators' => [5],
                ],
                [
                    'name' => '6',
                    'validators' => [6],
                ],
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000002' => [
                [
                    'name' => '4, 5',
                    'validators' => [4, 5],
                ],
                [
                    'name' => '6',
                    'validators' => [6],
                ],
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000003' => [
                [
                    'name' => '5',
                    'validators' => [5],
                ],
                [
                    'name' => '6',
                    'validators' => [6],
                ],
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000004' => [
                [
                    'name' => '4',
                    'validators' => [4],
                ],
            ],
            'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000005' => [
                [
                    'name' => '4',
                    'validators' => [4],
                ],
            ],
            // created dynamically and return does not matter
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000001' => [
//                [
//                    'name' => '5',
//                    'validators' => [5]
//                ],
//                [
//                    'name' => '6',
//                    'validators' => [6]
//                ]
            ],
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000002' => [
                [
                    'name' => 'Etape 1',
                    'validators' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'validators' => [5, 6],
                ],
                [
                    'name' => 'Etape 3',
                    'validators' => [4],
                ],
            ],
            'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000003' => [
                [
                    'name' => 'new etape 1',
                    'validators' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'validators' => [4],
                ],
            ],
        ][$processDefinitionKey];
    }

    public function getCurrentUsersCadidatesIdsByBusinessKey(string $businessKey)
    {
        return [];
    }
}
