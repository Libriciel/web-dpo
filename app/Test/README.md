# CakePHP 2

```bash
$ docker-compose exec web-dpo-db /bin/bash
# psql -U webdpo
CLUSTER admins USING admins_pkey;
CLUSTER articles USING articles_pkey;
CLUSTER articles_organisations USING articles_organisations_pkey;
CLUSTER authentifications USING authentifications_pkey;
CLUSTER champs USING champs_pkey;
CLUSTER commentaires USING commentaires_pkey;
CLUSTER connecteur_ldaps USING connecteur_ldaps_pkey;
CLUSTER coresponsables USING coresponsables_pkey;
CLUSTER crons USING crons_pkey;
CLUSTER droits USING droits_pkey;
CLUSTER etat_fiches USING etat_fiches_pkey;
CLUSTER etats USING etats_pkey;
CLUSTER extrait_registres USING extrait_registres_pkey;
CLUSTER fiches USING fiches_pkey;
CLUSTER fichierarticles USING fichierarticles_pkey;
CLUSTER fichiers USING fichiers_pkey;
CLUSTER formulaires USING formulaires_pkey;
CLUSTER historiques USING historiques_pkey;
CLUSTER ldapm_groups USING ldapm_groups_pkey;
CLUSTER ldapm_models_groups USING ldapm_models_groups_pkey;
CLUSTER liste_droits USING liste_droits_pkey;
CLUSTER modele_extrait_registres USING modele_extrait_registres_pkey;
CLUSTER modele_presentations USING modele_presentations_pkey;
CLUSTER modeles USING modeles_pkey;
CLUSTER modifications USING modifications_pkey;
CLUSTER normes USING normes_pkey;
CLUSTER notifications USING notifications_pkey;
CLUSTER organisation_user_roles USING organisation_user_roles_pkey;
CLUSTER organisation_user_services USING organisation_user_services_pkey;
CLUSTER organisations USING organisations_pkey;
CLUSTER organisations_users USING organisations_users_pkey;
CLUSTER responsables USING responsables_pkey;
CLUSTER responsables_organisations USING responsables_organisations_pkey;
CLUSTER role_droits USING role_droits_pkey;
CLUSTER roles USING roles_pkey;
CLUSTER services USING services_pkey;
CLUSTER soustraitances USING soustraitances_pkey;
CLUSTER soustraitants USING soustraitants_pkey;
CLUSTER soustraitants_organisations USING soustraitants_organisations_pkey;
CLUSTER traitement_registres USING traitement_registres_pkey;
CLUSTER typages USING typages_pkey;
CLUSTER typages_organisations USING typages_organisations_pkey;
CLUSTER users USING users_pkey;
CLUSTER valeurs USING valeurs_pkey;
```

```bash
vendors/cakephp/cakephp/lib/Cake/Console/cake Postgres.postgres_maintenance all -app app
vendors/cakephp/cakephp/lib/Cake/Console/cake bake fixture all --count=500 --records --schema -app app

vendors/cakephp/cakephp/lib/Cake/Console/cake bake fixture soustraitants --count=500 --records --schema -app app
vendors/cakephp/cakephp/lib/Cake/Console/cake bake fixture soustraitants_organisations --count=500 --records --schema -app app
```

| Contrôleur                                            | Fait | Remarques                               |
| ---                                                   | ---  | ---                                     |
| `app/Controller/AdminsController.php`                 | Oui  |                                         |
| `app/Controller/AppController.php`                    |      |                                         |
| `app/Controller/ArticlesController.php`               |      | Il manque des TFA pour remplir la BDD   |
| `app/Controller/AuthentificationsController.php`      | Oui  |                                         |
| `app/Controller/ChecksController.php`                 | Oui  |                                         |
| `app/Controller/ConnecteurLdapsController.php`        | Oui  |                                         |
| `app/Controller/ConnecteursController.php`            | Oui  |                                         |
| `app/Controller/CronsController.php`                  | Oui  |                                         |
| `app/Controller/EtatFichesController.php`             |      |                                         |
| `app/Controller/FichesController.php`                 | Oui  |                                         |
| `app/Controller/FormulairesController.php`            | Oui  |                                         |
| `app/Controller/ModeleExtraitRegistresController.php` |      | Problèmes pour tester avec des fichiers |
| `app/Controller/ModelePresentationsController.php`    |      | Problèmes pour tester avec des fichiers |
| `app/Controller/ModelesController.php`                |      | Problèmes pour tester avec des fichiers |
| `app/Controller/NormesController.php`                 | Oui  |                                         |
| `app/Controller/OrganisationsController.php`          | Oui  |                                         |
| `app/Controller/PannelController.php`                 | Oui  |                                         |
| `app/Controller/RegistresController.php`              | Oui  |                                         |
| `app/Controller/ResponsablesController.php`           |      |                                         |
| `app/Controller/RolesController.php`                  | Oui  |                                         |
| `app/Controller/ServicesController.php`               | Oui  |                                         |
| `app/Controller/SoustraitantsController.php`          | Oui  |                                         |
| `app/Controller/TypagesController.php`                | Oui  |                                         |
| `app/Controller/UsersController.php`                  | Oui  |                                         |

```bash
controller_actions() {
    grep -i --color=none "^\s*public\s\+function" ${1} \
        | sed 's/^\s*public\s\+function\s\+\(\S\+\)\s*(.*$/\1/g' \
        | sort    
}
controller_actions app/Controller/FormulairesController.php
#controller_actions app/Controller/ModeleExtraitRegistresController.php
#controller_actions app/Controller/ModelesController.php
#controller_actions app/Controller/OrganisationsController.php
controller_actions app/Controller/PannelController.php
#controller_actions app/Controller/UsersController.php
controller_actions app/Controller/RolesController.php
controller_actions app/Controller/FichesController.php
controller_actions app/Controller/CronsController.php
controller_actions app/Controller/SoustraitantsController.php
controller_actions app/Controller/RegistresController.php
controller_actions app/Controller/ResponsablesController.php
```

Création du squelette à partir des noms de méthodes des contrôleurs.

```
    public function dataAccess_\1() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/responsables/\1/1'],
            [302, 'Administrateur.ibleu', '/responsables/\1/1'],
            [302, 'DPO.nroux', '/responsables/\1/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/responsables/\1/1'],
            [403, 'DPO.hvermeil', '/responsables/\1/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/responsables/\1/666'],
            [404, 'Administrateur.ibleu', '/responsables/\1/666'],
            [404, 'DPO.nroux', '/responsables/\1/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/responsables/\1/1'],
            [403, 'Valideur.cnoir', '/responsables/\1/1'],
            [403, 'Consultant.mrose', '/responsables/\1/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/responsables/\1/666'],
            [403, 'Valideur.cnoir', '/responsables/\1/666'],
            [403, 'Consultant.mrose', '/responsables/\1/666'],
        ];
    }

    /**
     * @dataProvider dataAccess_\1
     */
    public function testAccess_\1($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

```

Récupération de la liste des utilisateurs pour peupler `$_usernames` dans les tests

```php
// @fixme: les rôles de Superadmin sont vides
$user = ClassRegistry::init('User');
$usernames = [];
$defaults = [];
$query = [
    'fields' => [
        'User.id',
        'User.username',
        'OrganisationUser.organisation_id',
        'Role.libelle'
    ],
    'joins' => [
        $user->join('OrganisationUser', ['type' => 'LEFT OUTER']),
        $user->OrganisationUser->join('OrganisationUserRole', ['type' => 'LEFT OUTER']),
        $user->OrganisationUser->OrganisationUserRole->join('Role', ['type' => 'LEFT OUTER']),
    ],
    'order' => [
        'User.username' => 'asc'
    ]
];
foreach ($user->find('all', $query) as $record) {
    $usernames[$record['User']['username']] = [
        'user_id' => $record['User']['id'],
        'organisation_id' => $record['OrganisationUser']['organisation_id'],
        'role' => $record['Role']['libelle'],
    ];
    if ($record['OrganisationUser']['organisation_id'] === 1) {
        $defaults[$record['Role']['libelle']] = $record['User']['username'];
    }
}
ksort($defaults);

debug($defaults);
debug($usernames);
```
