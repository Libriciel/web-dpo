<?php
App::uses( 'ControllerAccessTrait', 'Test/Trait/Controller' );
App::uses( 'Inflector', 'Utility' );

/**
 * Trait TestControllerAccessTrait
 * @see http://mark-story.com/posts/view/testing-cakephp-controllers-the-hard-way
 * @deprecated see ControllerTestCaseAccessTrait
 */
trait ControllerAccessAssertionTrait
{
    use ControllerAccessTrait;

    protected $_defaultExceptionMessages = [
        'ForbiddenException' => 'Vous n\'avez pas le droit d\'acceder à cette page',
        'NotFoundException' => 'Not Found',
    ];

    public function setUp() {
        touch(CakeTestSession::getSessionFilePath(CakeTestSession::$testSessionId));
        $classname = preg_replace('/ControllerTest$/', 'TestsController', __CLASS__);
        $this->_setTestControllerClassname($classname);
    }

    public function assertAccess(array $url, $granted, array $params = []) {
        $url += [
            'controller' => Inflector::underscore(preg_replace('/ControllerTest$/', '', __CLASS__)),
            'action' => null
        ];
        $params += [
            'exception' => 'ForbiddenException',
            'exceptionMessage' => null,
            'organisation_id' => null,
            'role' => null,
            'user_id' => null,
            'data' => [],
            'username' => null,
        ];
        if ($params['exceptionMessage'] === null && isset($this->_defaultExceptionMessages[$params['exception']]) === true) {
            $params['exceptionMessage'] = $this->_defaultExceptionMessages[$params['exception']];
        }
        if ($params['username'] !== null) {
            if (array_key_exists($params['username'], $this->_usernames) === false) {
                $msgid = 'L\'utilisateur "%s" n\'existe pas';
                $msgstr = sprintf($msgid, $params['username']);
                throw new RuntimeException($msgstr);
            }
            $params['organisation_id'] = $this->_usernames[$params['username']]['organisation_id'];
            $params['user_id'] = $this->_usernames[$params['username']]['user_id'];
            if ($params['role'] === null) {
                $params['role'] = $this->_usernames[$params['username']]['role'];
            } elseif ($params['role'] !== $this->_usernames[$params['username']]['role']) {
                $msgid = 'L\'utilisateur "%s" possède le rôle "%s" et ne peut donc pas tester avec le rôle "%s"';
                $msgstr = sprintf($msgid, $params['username'], $this->_usernames[$params['username']]['role'], $params['role']);
                throw new RuntimeException($msgstr);
            }
        } elseif ($params['role'] !== null) {
            if ($params['organisation_id'] === null) {
                $params['organisation_id'] = 1;
            }
            if ($params['organisation_id'] === 1 && $params['user_id'] === null) {
                $params['user_id'] = $this->_usernames[$this->_defaultRoleUsername[$params['role']]]['user_id'];
            }
        }

        $this->_setupController($url, $params['data']);
        if ($params['role'] !== null) {
            $setupUserMethod = sprintf('_setup%s', replace_accents($params['role']));
        }
        if (/*$params['organisation_id'] !== null &&*/ $params['user_id'] !== null) {
            static::$setupUserMethod($params['organisation_id'], $params['user_id']);
        }

        if ($granted === false) {
            $this->setExpectedException($params['exception'], $params['exceptionMessage']);
        }

        $action = $url['action'];
        unset($url['controller'], $url['action']);
        call_user_func_array([$this->controller, 'beforeFilter'], []);
        call_user_func_array([$this->controller, $action], $url);

        if ($granted === true) {
            $this->assertEquals(200, $this->controller->response->statusCode());
        }
    }
}
