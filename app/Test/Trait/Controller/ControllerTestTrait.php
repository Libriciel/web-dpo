<?php

/**
 * @deprecated see ControllerTestCaseAccessTrait
 */
trait ControllerTestTrait
{
//    public $autoRender = false;

    public $redirect = [];

    public function redirect($url, $status = null, $exit = true) {
        $this->redirect = compact('url', 'status', 'exit');
    }

    public function render($action = null, $layout = null, $file = null) {
        $this->render = compact('action', 'layout', 'file');
    }

    public function _stop($status = 0) {
        $this->stop = $status;
    }
}
