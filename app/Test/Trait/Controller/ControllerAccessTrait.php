<?php
App::uses( 'CakeTestSession', 'CakeTest.Model/Datasource' );

/**
 * @deprecated see ControllerTestCaseAccessTrait
 */
trait ControllerAccessTrait
{
    /**
     * Le contrôleur.
     *
     * @var Controller
     */
    public $controller;

    /**
     * Le nom de la classe du contrôleur que l'on veut tester.
     * Celle-ci sera en général surchargée.
     *
     * @var string
     */
    protected $_testControllerClassname;

    protected function _setTestControllerClassname($testControllerClassname)
    {
        $this->_testControllerClassname = $testControllerClassname;
    }

    /**
     * Récupère les droits effectifs en base de données pour un profil au sein d'une entité.
     */
    protected function _getListeDroitByRole($organisation_id, $role) {
        $roleDroit = ClassRegistry::init('RoleDroit');
        $query = [
            'fields' => ['RoleDroit.liste_droit_id'],
            'joins' => [
                $roleDroit->join('Role', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Role.libelle' => $role,
                'Role.organisation_id' => $organisation_id,
            ]
        ];
        return $roleDroit->find('list', $query);
    }

    protected function _setupController(array $url, array $data = []) {
        $request = new CakeRequest( Router::url($url), false );
        $request->addParams($url);
        foreach ($url as $key => $value) {
            if (in_array($key, ['plugin', 'controller', 'action'], true) === false) {
                $request->params['pass'][$key] = $value;
            }
        }
        if (empty($data) === false) {
            $request->data = $data;
        }

        $response = new CakeResponse();

        $this->controller = new $this->_testControllerClassname( $request, $response );
        $this->controller->constructClasses();
        $this->controller->Components->init( $this->controller );
        // @todo: c'est moche, c'est un cas très particulier
        if (array_key_exists('LdapManager.GroupManager', $this->controller->components) === true) {
            App::uses('GroupManagerComponent', 'LdapManager.Controller/Component');
            $this->controller->GroupManager = $this->getMock(
                'GroupManagerComponent',
                ['getGroupsRole'],
                [],
                '',
                false,
                false
            );
        }

        CakeTestSession::start();
        CakeTestSession::destroy();
    }

    /**
     * Les username par défaut pour organisation_id 1, en fonction du rôle.
     */
    protected $_defaultRoleUsername = [
        'Administrateur' => 'ibleu',
        'Consultant' => 'mrose',
        'DPO' => 'nroux',
        'Rédacteur' => 'rjaune',
        'Superadministrateur' => 'superadmin',
        'Valideur' => 'cnoir'
    ];

    protected $_usernames = [
        'amauve' => [
            'user_id' => 13,
            'organisation_id' => 2,
            'role' => 'Valideur'
        ],
        'cnoir' => [
            'user_id' => 9,
            'organisation_id' => 1,
            'role' => 'Valideur'
        ],
        'findigo' => [
            'user_id' => 4,
            'organisation_id' => 2,
            'role' => 'Administrateur'
        ],
        'hvermeil' => [
            'user_id' => 15,
            'organisation_id' => 2,
            'role' => 'DPO'
        ],
        'ibleu' => [
            'user_id' => 3,
            'organisation_id' => 1,
            'role' => 'Administrateur'
        ],
        'jgris' => [
            'user_id' => 5,
            'organisation_id' => 3,
            'role' => 'Administrateur'
        ],
        'lsepia' => [
            'user_id' => 6,
            'organisation_id' => 4,
            'role' => 'Administrateur'
        ],
        'mazur' => [
            'user_id' => 14,
            'organisation_id' => 2,
            'role' => 'Consultant'
        ],
        'mrose' => [
            'user_id' => 10,
            'organisation_id' => 1,
            'role' => 'Consultant'
        ],
        'mrubis' => [
            'user_id' => 7,
            'organisation_id' => 5,
            'role' => 'Administrateur'
        ],
        'nroux' => [
            'user_id' => 11,
            'organisation_id' => 1,
            'role' => 'DPO'
        ],
        'pmagenta' => [
            'user_id' => 12,
            'organisation_id' => 2,
            'role' => 'Rédacteur'
        ],
        'rjaune' => [
            'user_id' => 8,
            'organisation_id' => 1,
            'role' => 'Rédacteur'
        ],
        'superadmin' => [
            'user_id' => 1,
            'organisation_id' => null,
            'role' => 'Superadministrateur'
        ],
        'superadmin2' => [
            'user_id' => 2,
            'organisation_id' => null,
            'role' => 'Superadministrateur'
        ]
    ];

    protected function _setupSuperadministrateur($organisation_id = null, $user_id = null) {
        /*if ($organisation_id === null) {
            $organisation_id = 1;
        }*/
        if (/*$organisation_id === 1 &&*/ $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['Superadministrateur']]['user_id'];
        }
        $session = [
            'Su' => true,
            'Auth.User.id' => $user_id,
            'Auth.User.Admin.id' => $user_id,
            'Auth.User.Admin.user_id' => $user_id,
            'Droit' => [
                'liste' => []
            ],
            'User' => ['service' => []]
        ];
        if ($organisation_id !== null) {
            $session['Organisation.id'] = $organisation_id;//@fixme
        }
        CakeTestSession::write($session);
    }

    protected function _setupAdministrateur($organisation_id = null, $user_id = null) {
        if ($organisation_id === null) {
            $organisation_id = 1;
        }
        if ($organisation_id === 1 && $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['Administrateur']]['user_id'];
        }
        $session = [
            'Su' => false,
            'Organisation.id' => $organisation_id,
            'Auth.User.id' => $user_id,
            'Droit' => [
                'liste' => $this->_getListeDroitByRole($organisation_id, 'Administrateur')
            ],
            'User' => ['service' => []]
        ];
        CakeTestSession::write($session);
    }

    protected function _setupRedacteur($organisation_id = null, $user_id = null) {
        if ($organisation_id === null) {
            $organisation_id = 1;
        }
        if ($organisation_id === 1 && $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['Rédacteur']]['user_id'];
        }
        $session = [
            'Su' => false,
            'Organisation.id' => $organisation_id,
            'Auth.User.id' => $user_id,
            'Droit' => [
                'liste' => $this->_getListeDroitByRole($organisation_id, 'Rédacteur')
            ],
            'User' => ['service' => []]
        ];
        CakeTestSession::write($session);
    }

    protected function _setupValideur($organisation_id = null, $user_id = null) {
        if ($organisation_id === null) {
            $organisation_id = 1;
        }
        if ($organisation_id === 1 && $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['Valideur']]['user_id'];
        }
        $session = [
            'Su' => false,
            'Organisation.id' => $organisation_id,
            'Auth.User.id' => $user_id,
            'Droit' => [
                'liste' => $this->_getListeDroitByRole($organisation_id, 'Valideur')
            ],
            'User' => ['service' => []]
        ];
        CakeTestSession::write($session);
    }

    protected function _setupConsultant($organisation_id = null, $user_id = null) {
        if ($organisation_id === null) {
            $organisation_id = 1;
        }
        if ($organisation_id === 1 && $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['Consultant']]['user_id'];
        }
        $session = [
            'Su' => false,
            'Organisation.id' => $organisation_id,
            'Auth.User.id' => $user_id,
            'Droit' => [
                'liste' => $this->_getListeDroitByRole($organisation_id, 'Consultant')
            ],
            'User' => ['service' => []]
        ];
        CakeTestSession::write($session);
    }

    protected function _setupDpo($organisation_id = null, $user_id = null) {
        if ($organisation_id === null) {
            $organisation_id = 1;
        }
        if ($organisation_id === 1 && $user_id === null) {
            $user_id = $this->_usernames[$this->_defaultRoleUsername['DPO']]['user_id'];
        }

        $session = [
            'Su' => false,
            'Organisation.id' => $organisation_id,
            'Auth.User.id' => $user_id,
            'Droit' => [
                'liste' => $this->_getListeDroitByRole($organisation_id, 'DPO')
            ],
            'User' => ['service' => []]
        ];
        CakeTestSession::write($session);
    }
}
