<?php
trait ControllerTestCaseAccessTrait
{
    /**
     * Récupère les droits effectifs en base de données pour un profil au sein d'une entité.
     */
    protected function _getListeDroitByRole($organisation_id, $role) {
        $roleDroit = ClassRegistry::init('RoleDroit');
        $query = [
            'fields' => ['RoleDroit.liste_droit_id'],
            'joins' => [
                $roleDroit->join('Role', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Role.libelle' => $role,
                'Role.organisation_id' => $organisation_id,
            ]
        ];
        return $roleDroit->find('list', $query);
    }

    // dans logout
    // debug($this->Session->read());die();
    protected function _setupUserSession($username) {
        // @todo: ajouter les utilisateurs manquants
        $sessions = [
            'Superadministrateur.superadmin' => [
                'Su' => true,
                'Auth.User.id' => 1,
                'Auth.User.Admin.id' => 1,
                'Auth.User.Admin.user_id' => 1,
                'Droit' => [
                    'liste' => []
                ],
                'User' => ['service' => []]
            ],
            'Superadministrateur.superadmin2' => [
                'Su' => true,
                'Auth.User.id' => 2,
                'Auth.User.Admin.id' => 2,
                'Auth.User.Admin.user_id' => 2,
                'Droit' => [
                    'liste' => []
                ],
                'User' => ['service' => []]
            ],
            // Entité Libriciel SCOP
            'Administrateur.ibleu' => [
                'Su' => false,
                'Organisation.id' => 1,
                'Auth.User.id' => 3,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(1, 'Administrateur')
                ],
                'User' => ['service' => []]
            ],
            'DPO.nroux' => [
                'Su' => false,
                'Organisation.id' => 1,
                'Auth.User.id' => 11,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(1, 'DPO')
                ],
                'User' => ['service' => []]
            ],
            'Rédacteur.rjaune' => [
                'Su' => false,
                'Organisation.id' => 1,
                'Auth.User.id' => 8,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(1, 'Rédacteur')
                ],
                'User' => ['service' => []]
            ],
            'Valideur.cnoir' => [
                'Su' => false,
                'Organisation.id' => 1,
                'Auth.User.id' => 9,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(1, 'Valideur')
                ],
                'User' => ['service' => []]
            ],
            'Consultant.mrose' => [
                'Su' => false,
                'Organisation.id' => 1,
                'Auth.User.id' => 10,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(1, 'Consultant')
                ],
                'User' => ['service' => []]
            ],
            // Entité "Montpellier Méditerranée Métropole"
            'Administrateur.findigo' => [
                'Su' => false,
                'Organisation.id' => 2,
                'Auth.User.id' => 4,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(2, 'Administrateur')
                ],
                'User' => ['service' => []]
            ],
            'DPO.hvermeil' => [
                'Su' => false,
                'Organisation.id' => 2,
                'Auth.User.id' => 15,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(2, 'DPO')
                ],
                'User' => ['service' => []]
            ],
            'Rédacteur.pmagenta' => [
                'Su' => false,
                'Organisation.id' => 2,
                'Auth.User.id' => 12,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(2, 'Rédacteur')
                ],
                'User' => ['service' => []]
            ],
            'Valideur.amauve' => [
                'Su' => false,
                'Organisation.id' => 2,
                'Auth.User.id' => 13,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(2, 'Valideur')
                ],
                'User' => ['service' => []]
            ],
            'Consultant.mazur' => [
                'Su' => false,
                'Organisation.id' => 2,
                'Auth.User.id' => 14,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(2, 'Consultant')
                ],
                'User' => ['service' => []]
            ],
            // Entité "Métropole Européenne de Lille"
            'Administrateur.jgris' => [
                'Su' => false,
                'Organisation.id' => 3,
                'Auth.User.id' => 5,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(3, 'Administrateur')
                ],
                'User' => ['service' => []]
            ],
            'DPO.vpaille' => [
                'Su' => false,
                'Organisation.id' => 3,
                'Auth.User.id' => 19,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(3, 'DPO')
                ],
                'User' => ['service' => []]
            ],
            'Rédacteur.sorange' => [
                'Su' => false,
                'Organisation.id' => 3,
                'Auth.User.id' => 16,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(3, 'Rédacteur')
                ],
                'User' => ['service' => []]
            ],
            'Valideur.plavande' => [
                'Su' => false,
                'Organisation.id' => 3,
                'Auth.User.id' => 17,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(3, 'Valideur')
                ],
                'User' => ['service' => []]
            ],
            'Consultant.mlilas' => [
                'Su' => false,
                'Organisation.id' => 3,
                'Auth.User.id' => 18,
                'Droit' => [
                    'liste' => $this->_getListeDroitByRole(3, 'Consultant')
                ],
                'User' => ['service' => []]
            ],
        ];
        if ($username === null) {
            $session = [];//@todo: pour supprimer le contenu d'une session, voir WebcilUsersComponentTest::tearDown
        } else {
            // @todo: if not set
            $session = $sessions[$username];
        }

        CakeSession::write($session);
    }

    public function assertActionAccess($expectedStatus, $user, $url, $options = []) {
        $options += ['method' => 'GET', 'session' => []];
        $this->_setupUserSession($user);
        if (empty($options['session']) === false) {
            CakeSession::write($options['session']);
        }
        if (in_array($expectedStatus, [200, 301, 302], true)) {
            $this->testAction($url, $options);
            $this->assertSame($expectedStatus, $this->controller->response->statusCode());
        } else {
            // @see https://book.cakephp.org/2/en/development/exceptions.html#built-in-exceptions-for-cakephp
            $exceptions = [
                403 => 'ForbiddenException',
                404 => 'NotFoundException',
                405 => 'MethodNotAllowedException',
                500 => 'RuntimeException',
            ];
            $this->setExpectedException($exceptions[$expectedStatus]);
            $this->testAction($url, $options);
        }
    }
}
