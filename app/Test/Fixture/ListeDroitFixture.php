<?php
/**
 * ListeDroit Fixture
 */
class ListeDroitFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = [
	    'model' => 'ListeDroit'
    ];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'libelle' => 'Rédiger un traitement',
			'value' => 1,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 2,
			'libelle' => 'Valider un traitement',
			'value' => 2,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 3,
			'libelle' => 'Viser un traitement',
			'value' => 3,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 4,
			'libelle' => 'Consulter le registre',
			'value' => 4,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 5,
			'libelle' => 'Insérer un traitement dans le registre',
			'value' => 5,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 6,
			'libelle' => 'Modifier un traitement du registre',
			'value' => 6,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 7,
			'libelle' => 'Télécharger un traitement du registre',
			'value' => 7,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 8,
			'libelle' => 'Créer un utilisateur',
			'value' => 8,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 9,
			'libelle' => 'Modifier un utilisateur',
			'value' => 9,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 10,
			'libelle' => 'Supprimer un utilisateur',
			'value' => 10,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 11,
			'libelle' => 'Créer une organisation',
			'value' => 11,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 12,
			'libelle' => 'Modifier une organisation',
			'value' => 12,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 13,
			'libelle' => 'Créer un profil',
			'value' => 13,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 14,
			'libelle' => 'Modifier un profil',
			'value' => 14,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 15,
			'libelle' => 'Supprimer un profil',
			'value' => 15,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		],
		[
			'id' => 16,
			'libelle' => 'Créer un service',
			'value' => 16,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 17,
			'libelle' => 'Modifier un service',
			'value' => 17,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 18,
			'libelle' => 'Supprimer un service',
			'value' => 18,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 19,
			'libelle' => 'Créer une norme',
			'value' => 19,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 20,
			'libelle' => 'Visualiser une norme',
			'value' => 20,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 21,
			'libelle' => 'Modifier une norme',
			'value' => 21,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 22,
			'libelle' => 'Abroger une norme',
			'value' => 22,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 23,
			'libelle' => 'Gestion des sous-traitants',
			'value' => 23,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 24,
			'libelle' => 'Gestion des co-responsables',
			'value' => 24,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 25,
			'libelle' => 'Gestion de la maitenance',
			'value' => 25,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 26,
			'libelle' => 'Gestion des modèles',
			'value' => 26,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 27,
			'libelle' => 'Gestion des formulaires',
			'value' => 27,
			'created' => '2020-06-10 00:15:34.49941',
			'modified' => '2020-06-10 00:15:34.49941'
		],
		[
			'id' => 28,
			'libelle' => 'Créer un article dans la FAQ',
			'value' => 28,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		],
		[
			'id' => 29,
			'libelle' => 'Modifier un article dans la FAQ',
			'value' => 29,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		],
		[
			'id' => 30,
			'libelle' => 'Consulter la FAQ',
			'value' => 30,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		],
		[
			'id' => 31,
			'libelle' => 'Supprimer un article dans la FAQ',
			'value' => 31,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		],
		[
			'id' => 32,
			'libelle' => 'Gestion du typage des annexes',
			'value' => 32,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		],
        [
            'id' => 33,
            'libelle' => 'Consulter tous les traitements dans l\'entité',
            'value' => 33,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 34,
            'libelle' => 'Gestion des co-responsables lors de la déclaration d\'un traitement',
            'value' => 34,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 35,
            'libelle' => 'Gestion des sous-traitants lors de la déclaration d\'un traitement',
            'value' => 35,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 36,
            'libelle' => 'Dupliquer un traitement',
            'value' => 36,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 37,
            'libelle' => 'Initialisation d\'un traitement',
            'value' => 37,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 38,
            'libelle' => 'Générer un traitement en cours de rédaction',
            'value' => 38,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
        [
            'id' => 39,
            'libelle' => 'Modifier un traitement reçu en consultation',
            'value' => 39,
            'created' => '2020-06-10 00:15:34.567556',
            'modified' => '2020-06-10 00:15:34.567556'
        ],
	];

}
