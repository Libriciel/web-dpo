<?php
/**
 * ModeleExtraitRegistre Fixture
 */
class ModeleExtraitRegistreFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = [
	    'model' => 'ModeleExtraitRegistre'
    ];

/**
 * Records
 *
 * @var array
 */
	public $records = [
        [
            'id' => 1,
            'name_modele' => 'modele_extrait_registre.odt',
            'fichier' => '1623938409.odt',
            'created' => '2020-06-10 00:15:34.390532',
            'modified' => '2020-06-10 00:15:34.390532'
        ],
    ];

}
