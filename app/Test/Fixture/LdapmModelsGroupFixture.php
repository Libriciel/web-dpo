<?php
/**
 * LdapmModelsGroup Fixture
 */
class LdapmModelsGroupFixture extends CakeTestFixture {

    public $fields = array(
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'ldapm_groups_id' => ['type' => 'integer'],
        'model' => ['type' => 'string', 'length' => 255, 'default' => null],
        'foreign_key' => ['type' => 'integer'],
    );

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
    );

}
