<?php
	/**
	 * Code source de la classe AdminFixture.
	 *
	 * PHP 5.3
	 *
	 * @package     app.Test.Fixture
	 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
	 */
	/**
	 * La classe AdminFixture ...
	 *
	 * @package app.Test.Fixture
	 */
	class AdminFixture extends CakeTestFixture
	{

		/**
		 * On importe la définition de la table, pas les enregistrements.
		 *
		 * @var array
		 */
		public $import = [
			'model' => 'Admin',
			'records' => false
		];

		/**
		 * Définition des enregistrements.
		 *
		 * @var array
		 */
		public $records = [
			1 => [
				'user_id' => 1,
				'created' => '2017-07-25 13:55:49',
				'modified' => '2017-07-25 13:55:49'
			]
		];

	}
?>