<?php
/**
 * ModeleExtraitRegistreOrganisation Fixture
 */
class ModeleExtraitRegistreOrganisationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = [
	    'model' => 'ModeleExtraitRegistreOrganisation'
    ];

/**
 * Records
 *
 * @var array
 */
	public $records = [
        [
            'id' => 1,
            'modele_extrait_registre_id' => 1,
            'organisation_id' => 1
        ],
        [
            'id' => 2,
            'modele_extrait_registre_id' => 1,
            'organisation_id' => 2
        ]
    ];

}
