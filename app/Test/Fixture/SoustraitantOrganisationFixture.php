<?php
/**
 * SoustraitantOrganisation Fixture
 */
class SoustraitantOrganisationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'SoustraitantOrganisation');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 8,
			'soustraitant_id' => 2,
			'organisation_id' => 1
		),
		array(
			'id' => 9,
			'soustraitant_id' => 6,
			'organisation_id' => 1
		),
		array(
			'id' => 10,
			'soustraitant_id' => 8,
			'organisation_id' => 1
		),
	);

}
