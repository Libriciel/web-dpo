<?php
/**
 * ArticleOrganisation Fixture
 */
class ArticleOrganisationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ArticleOrganisation');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
