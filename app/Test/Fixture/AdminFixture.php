<?php
/**
 * Admin Fixture
 */
class AdminFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Admin');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 2,
			'user_id' => 2,
			'created' => '2020-06-10 00:17:04',
			'modified' => '2020-06-10 00:17:04'
		),
	);

}
