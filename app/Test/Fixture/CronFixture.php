<?php
/**
 * Cron Fixture
 */
class CronFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Cron');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'organisation_id' => 1,
			'active' => 0,
			'lock' => 0,
			'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
			'action' => 'syncLdap',
			'next_execution_time' => null,
			'execution_duration' => null,
			'last_execution_start_time' => null,
			'last_execution_end_time' => null,
			'last_execution_report' => null,
			'last_execution_status' => null,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 2,
			'organisation_id' => 1,
			'active' => 1,
			'lock' => 0,
			'nom' => 'Conversion des annexes',
			'action' => 'conversionAnnexes',
			'next_execution_time' => '2020-06-10 01:16:00',
			'execution_duration' => 'PT20M',
			'last_execution_start_time' => '2020-06-10 01:00:01',
			'last_execution_end_time' => '2020-06-10 01:00:01',
			'last_execution_report' => 'Aucune annexe à convertir',
			'last_execution_status' => 'SUCCES',
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 01:00:01'
		),
		array(
			'id' => 3,
			'organisation_id' => 2,
			'active' => 0,
			'lock' => 0,
			'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
			'action' => 'syncLdap',
			'next_execution_time' => null,
			'execution_duration' => null,
			'last_execution_start_time' => null,
			'last_execution_end_time' => null,
			'last_execution_report' => null,
			'last_execution_status' => null,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 4,
			'organisation_id' => 2,
			'active' => 1,
			'lock' => 0,
			'nom' => 'Conversion des annexes',
			'action' => 'conversionAnnexes',
			'next_execution_time' => '2020-06-10 01:16:00',
			'execution_duration' => 'PT20M',
			'last_execution_start_time' => '2020-06-10 01:00:01',
			'last_execution_end_time' => '2020-06-10 01:00:01',
			'last_execution_report' => 'Aucune annexe à convertir',
			'last_execution_status' => 'SUCCES',
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 01:00:01'
		),
		array(
			'id' => 5,
			'organisation_id' => 3,
			'active' => 0,
			'lock' => 0,
			'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
			'action' => 'syncLdap',
			'next_execution_time' => null,
			'execution_duration' => null,
			'last_execution_start_time' => null,
			'last_execution_end_time' => null,
			'last_execution_report' => null,
			'last_execution_status' => null,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 6,
			'organisation_id' => 3,
			'active' => 1,
			'lock' => 0,
			'nom' => 'Conversion des annexes',
			'action' => 'conversionAnnexes',
			'next_execution_time' => '2020-06-10 01:16:00',
			'execution_duration' => 'PT20M',
			'last_execution_start_time' => '2020-06-10 01:00:01',
			'last_execution_end_time' => '2020-06-10 01:00:01',
			'last_execution_report' => 'Aucune annexe à convertir',
			'last_execution_status' => 'SUCCES',
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 01:00:01'
		),
		array(
			'id' => 7,
			'organisation_id' => 4,
			'active' => 0,
			'lock' => 0,
			'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
			'action' => 'syncLdap',
			'next_execution_time' => null,
			'execution_duration' => null,
			'last_execution_start_time' => null,
			'last_execution_end_time' => null,
			'last_execution_report' => null,
			'last_execution_status' => null,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 8,
			'organisation_id' => 4,
			'active' => 1,
			'lock' => 0,
			'nom' => 'Conversion des annexes',
			'action' => 'conversionAnnexes',
			'next_execution_time' => '2020-06-10 01:16:00',
			'execution_duration' => 'PT20M',
			'last_execution_start_time' => '2020-06-10 01:00:01',
			'last_execution_end_time' => '2020-06-10 01:00:01',
			'last_execution_report' => 'Aucune annexe à convertir',
			'last_execution_status' => 'SUCCES',
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 01:00:01'
		),
		array(
			'id' => 9,
			'organisation_id' => 5,
			'active' => 0,
			'lock' => 0,
			'nom' => 'LDAP : Synchronisation des utilisateurs et groupes',
			'action' => 'syncLdap',
			'next_execution_time' => null,
			'execution_duration' => null,
			'last_execution_start_time' => null,
			'last_execution_end_time' => null,
			'last_execution_report' => null,
			'last_execution_status' => null,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 10,
			'organisation_id' => 5,
			'active' => 1,
			'lock' => 0,
			'nom' => 'Conversion des annexes',
			'action' => 'conversionAnnexes',
			'next_execution_time' => '2020-06-10 01:16:00',
			'execution_duration' => 'PT20M',
			'last_execution_start_time' => '2020-06-10 01:00:01',
			'last_execution_end_time' => '2020-06-10 01:00:01',
			'last_execution_report' => 'Aucune annexe à convertir',
			'last_execution_status' => 'SUCCES',
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 01:00:01'
		),
	);

}
