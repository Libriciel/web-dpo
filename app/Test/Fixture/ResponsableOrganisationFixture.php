<?php
/**
 * ResponsableOrganisation Fixture
 */
class ResponsableOrganisationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ResponsableOrganisation');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 7,
			'responsable_id' => 14,
			'organisation_id' => 1
		),
		array(
			'id' => 8,
			'responsable_id' => 12,
			'organisation_id' => 1
		),
		array(
			'id' => 9,
			'responsable_id' => 8,
			'organisation_id' => 1
		),
	);

}
