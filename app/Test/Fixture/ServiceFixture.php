<?php
/**
 * Service Fixture
 */
class ServiceFixture extends CakeTestFixture
{

    /**
     * Import
     *
     * @var array
     */
    public $import = [
        'model' => 'Service'
    ];

    /**
     * Records
     *
     * @var array
     */
	public $records = [
		[
			'id' => 1,
			'libelle' => 'Direction Administration, Finances et Moyens Généraux',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:17:52',
			'modified' => '2020-06-10 00:17:52'
		],
		[
			'id' => 2,
			'libelle' => 'Direction du Développement',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:17:55',
			'modified' => '2020-06-10 00:17:55'
		],
		[
			'id' => 3,
			'libelle' => 'Direction Commerciale et Marketing',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:17:58',
			'modified' => '2020-06-10 00:17:58'
		],
		[
			'id' => 4,
			'libelle' => 'Direction Service Clients',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:18:01',
			'modified' => '2020-06-10 00:18:01'
		],
		[
			'id' => 5,
			'libelle' => 'Service d\'urbanisme',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:18:03',
			'modified' => '2020-06-10 00:18:03'
		],
		[
			'id' => 6,
			'libelle' => 'Service de voirie',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:18:06',
			'modified' => '2020-06-10 00:18:06'
		],
		[
			'id' => 7,
			'libelle' => 'Service des transports',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:18:09',
			'modified' => '2020-06-10 00:18:09'
		],
		[
			'id' => 8,
			'libelle' => 'Service du traitement de l\'eau',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:18:11',
			'modified' => '2020-06-10 00:18:11'
		],
		[
			'id' => 9,
			'libelle' => 'Service d\'urbanisme',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:18:14',
			'modified' => '2020-06-10 00:18:14'
		],
		[
			'id' => 10,
			'libelle' => 'Service de voirie',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:18:16',
			'modified' => '2020-06-10 00:18:16'
		],
		[
			'id' => 11,
			'libelle' => 'Service des transports',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:18:19',
			'modified' => '2020-06-10 00:18:19'
		],
		[
			'id' => 12,
			'libelle' => 'Service du traitement de l\'eau',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:18:21',
			'modified' => '2020-06-10 00:18:21'
		],
	];

}
