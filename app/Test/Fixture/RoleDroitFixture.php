<?php
/**
 * RoleDroit Fixture
 */
class RoleDroitFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'RoleDroit');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'role_id' => 1,
			'liste_droit_id' => 1
		),
		array(
			'id' => 2,
			'role_id' => 1,
			'liste_droit_id' => 4
		),
		array(
			'id' => 3,
			'role_id' => 1,
			'liste_droit_id' => 7
		),
		array(
			'id' => 4,
			'role_id' => 1,
			'liste_droit_id' => 20
		),
		array(
			'id' => 5,
			'role_id' => 1,
			'liste_droit_id' => 30
		),
		array(
			'id' => 6,
			'role_id' => 2,
			'liste_droit_id' => 2
		),
		array(
			'id' => 7,
			'role_id' => 2,
			'liste_droit_id' => 4
		),
		array(
			'id' => 8,
			'role_id' => 2,
			'liste_droit_id' => 7
		),
		array(
			'id' => 9,
			'role_id' => 2,
			'liste_droit_id' => 20
		),
		array(
			'id' => 10,
			'role_id' => 2,
			'liste_droit_id' => 30
		),
		array(
			'id' => 11,
			'role_id' => 3,
			'liste_droit_id' => 3
		),
		array(
			'id' => 12,
			'role_id' => 3,
			'liste_droit_id' => 4
		),
		array(
			'id' => 13,
			'role_id' => 3,
			'liste_droit_id' => 20
		),
		array(
			'id' => 14,
			'role_id' => 3,
			'liste_droit_id' => 30
		),
		array(
			'id' => 15,
			'role_id' => 4,
			'liste_droit_id' => 1
		),
		array(
			'id' => 16,
			'role_id' => 4,
			'liste_droit_id' => 2
		),
		array(
			'id' => 17,
			'role_id' => 4,
			'liste_droit_id' => 3
		),
		array(
			'id' => 18,
			'role_id' => 4,
			'liste_droit_id' => 4
		),
		array(
			'id' => 19,
			'role_id' => 4,
			'liste_droit_id' => 7
		),
		array(
			'id' => 20,
			'role_id' => 4,
			'liste_droit_id' => 8
		),
		array(
			'id' => 21,
			'role_id' => 4,
			'liste_droit_id' => 9
		),
		array(
			'id' => 22,
			'role_id' => 4,
			'liste_droit_id' => 10
		),
		array(
			'id' => 23,
			'role_id' => 4,
			'liste_droit_id' => 12
		),
		array(
			'id' => 24,
			'role_id' => 4,
			'liste_droit_id' => 13
		),
		array(
			'id' => 25,
			'role_id' => 4,
			'liste_droit_id' => 14
		),
		array(
			'id' => 26,
			'role_id' => 4,
			'liste_droit_id' => 15
		),
		array(
			'id' => 27,
			'role_id' => 4,
			'liste_droit_id' => 16
		),
		array(
			'id' => 28,
			'role_id' => 4,
			'liste_droit_id' => 17
		),
		array(
			'id' => 29,
			'role_id' => 4,
			'liste_droit_id' => 18
		),
		array(
			'id' => 30,
			'role_id' => 4,
			'liste_droit_id' => 19
		),
		array(
			'id' => 31,
			'role_id' => 4,
			'liste_droit_id' => 21
		),
		array(
			'id' => 32,
			'role_id' => 4,
			'liste_droit_id' => 22
		),
		array(
			'id' => 33,
			'role_id' => 4,
			'liste_droit_id' => 23
		),
		array(
			'id' => 34,
			'role_id' => 4,
			'liste_droit_id' => 24
		),
		array(
			'id' => 35,
			'role_id' => 4,
			'liste_droit_id' => 25
		),
		array(
			'id' => 36,
			'role_id' => 4,
			'liste_droit_id' => 26
		),
		array(
			'id' => 37,
			'role_id' => 4,
			'liste_droit_id' => 27
		),
		array(
			'id' => 38,
			'role_id' => 4,
			'liste_droit_id' => 28
		),
		array(
			'id' => 39,
			'role_id' => 4,
			'liste_droit_id' => 29
		),
		array(
			'id' => 40,
			'role_id' => 4,
			'liste_droit_id' => 30
		),
		array(
			'id' => 41,
			'role_id' => 4,
			'liste_droit_id' => 31
		),
		array(
			'id' => 42,
			'role_id' => 4,
			'liste_droit_id' => 32
		),
		array(
			'id' => 43,
			'role_id' => 5,
			'liste_droit_id' => 1
		),
		array(
			'id' => 44,
			'role_id' => 5,
			'liste_droit_id' => 2
		),
		array(
			'id' => 45,
			'role_id' => 5,
			'liste_droit_id' => 3
		),
		array(
			'id' => 46,
			'role_id' => 5,
			'liste_droit_id' => 4
		),
		array(
			'id' => 47,
			'role_id' => 5,
			'liste_droit_id' => 5
		),
		array(
			'id' => 48,
			'role_id' => 5,
			'liste_droit_id' => 6
		),
		array(
			'id' => 49,
			'role_id' => 5,
			'liste_droit_id' => 7
		),
		array(
			'id' => 50,
			'role_id' => 5,
			'liste_droit_id' => 8
		),
		array(
			'id' => 51,
			'role_id' => 5,
			'liste_droit_id' => 9
		),
		array(
			'id' => 52,
			'role_id' => 5,
			'liste_droit_id' => 10
		),
		array(
			'id' => 53,
			'role_id' => 5,
			'liste_droit_id' => 12
		),
		array(
			'id' => 54,
			'role_id' => 5,
			'liste_droit_id' => 13
		),
		array(
			'id' => 55,
			'role_id' => 5,
			'liste_droit_id' => 14
		),
		array(
			'id' => 56,
			'role_id' => 5,
			'liste_droit_id' => 15
		),
		array(
			'id' => 57,
			'role_id' => 5,
			'liste_droit_id' => 16
		),
		array(
			'id' => 58,
			'role_id' => 5,
			'liste_droit_id' => 17
		),
		array(
			'id' => 59,
			'role_id' => 5,
			'liste_droit_id' => 18
		),
		array(
			'id' => 60,
			'role_id' => 5,
			'liste_droit_id' => 19
		),
		array(
			'id' => 61,
			'role_id' => 5,
			'liste_droit_id' => 21
		),
		array(
			'id' => 62,
			'role_id' => 5,
			'liste_droit_id' => 22
		),
		array(
			'id' => 63,
			'role_id' => 5,
			'liste_droit_id' => 23
		),
		array(
			'id' => 64,
			'role_id' => 5,
			'liste_droit_id' => 24
		),
		array(
			'id' => 65,
			'role_id' => 5,
			'liste_droit_id' => 25
		),
		array(
			'id' => 66,
			'role_id' => 5,
			'liste_droit_id' => 26
		),
		array(
			'id' => 67,
			'role_id' => 5,
			'liste_droit_id' => 27
		),
		array(
			'id' => 68,
			'role_id' => 5,
			'liste_droit_id' => 28
		),
		array(
			'id' => 69,
			'role_id' => 5,
			'liste_droit_id' => 29
		),
		array(
			'id' => 70,
			'role_id' => 5,
			'liste_droit_id' => 30
		),
		array(
			'id' => 71,
			'role_id' => 5,
			'liste_droit_id' => 31
		),
		array(
			'id' => 72,
			'role_id' => 5,
			'liste_droit_id' => 32
		),
		array(
			'id' => 73,
			'role_id' => 6,
			'liste_droit_id' => 1
		),
		array(
			'id' => 74,
			'role_id' => 6,
			'liste_droit_id' => 4
		),
		array(
			'id' => 75,
			'role_id' => 6,
			'liste_droit_id' => 7
		),
		array(
			'id' => 76,
			'role_id' => 6,
			'liste_droit_id' => 20
		),
		array(
			'id' => 77,
			'role_id' => 6,
			'liste_droit_id' => 30
		),
		array(
			'id' => 78,
			'role_id' => 7,
			'liste_droit_id' => 2
		),
		array(
			'id' => 79,
			'role_id' => 7,
			'liste_droit_id' => 4
		),
		array(
			'id' => 80,
			'role_id' => 7,
			'liste_droit_id' => 7
		),
		array(
			'id' => 81,
			'role_id' => 7,
			'liste_droit_id' => 20
		),
		array(
			'id' => 82,
			'role_id' => 7,
			'liste_droit_id' => 30
		),
		array(
			'id' => 83,
			'role_id' => 8,
			'liste_droit_id' => 3
		),
		array(
			'id' => 84,
			'role_id' => 8,
			'liste_droit_id' => 4
		),
		array(
			'id' => 85,
			'role_id' => 8,
			'liste_droit_id' => 20
		),
		array(
			'id' => 86,
			'role_id' => 8,
			'liste_droit_id' => 30
		),
		array(
			'id' => 87,
			'role_id' => 9,
			'liste_droit_id' => 1
		),
		array(
			'id' => 88,
			'role_id' => 9,
			'liste_droit_id' => 2
		),
		array(
			'id' => 89,
			'role_id' => 9,
			'liste_droit_id' => 3
		),
		array(
			'id' => 90,
			'role_id' => 9,
			'liste_droit_id' => 4
		),
		array(
			'id' => 91,
			'role_id' => 9,
			'liste_droit_id' => 7
		),
		array(
			'id' => 92,
			'role_id' => 9,
			'liste_droit_id' => 8
		),
		array(
			'id' => 93,
			'role_id' => 9,
			'liste_droit_id' => 9
		),
		array(
			'id' => 94,
			'role_id' => 9,
			'liste_droit_id' => 10
		),
		array(
			'id' => 95,
			'role_id' => 9,
			'liste_droit_id' => 12
		),
		array(
			'id' => 96,
			'role_id' => 9,
			'liste_droit_id' => 13
		),
		array(
			'id' => 97,
			'role_id' => 9,
			'liste_droit_id' => 14
		),
		array(
			'id' => 98,
			'role_id' => 9,
			'liste_droit_id' => 15
		),
		array(
			'id' => 99,
			'role_id' => 9,
			'liste_droit_id' => 16
		),
		array(
			'id' => 100,
			'role_id' => 9,
			'liste_droit_id' => 17
		),
		array(
			'id' => 101,
			'role_id' => 9,
			'liste_droit_id' => 18
		),
		array(
			'id' => 102,
			'role_id' => 9,
			'liste_droit_id' => 19
		),
		array(
			'id' => 103,
			'role_id' => 9,
			'liste_droit_id' => 21
		),
		array(
			'id' => 104,
			'role_id' => 9,
			'liste_droit_id' => 22
		),
		array(
			'id' => 105,
			'role_id' => 9,
			'liste_droit_id' => 23
		),
		array(
			'id' => 106,
			'role_id' => 9,
			'liste_droit_id' => 24
		),
		array(
			'id' => 107,
			'role_id' => 9,
			'liste_droit_id' => 25
		),
		array(
			'id' => 108,
			'role_id' => 9,
			'liste_droit_id' => 26
		),
		array(
			'id' => 109,
			'role_id' => 9,
			'liste_droit_id' => 27
		),
		array(
			'id' => 110,
			'role_id' => 9,
			'liste_droit_id' => 28
		),
		array(
			'id' => 111,
			'role_id' => 9,
			'liste_droit_id' => 29
		),
		array(
			'id' => 112,
			'role_id' => 9,
			'liste_droit_id' => 30
		),
		array(
			'id' => 113,
			'role_id' => 9,
			'liste_droit_id' => 31
		),
		array(
			'id' => 114,
			'role_id' => 9,
			'liste_droit_id' => 32
		),
		array(
			'id' => 115,
			'role_id' => 10,
			'liste_droit_id' => 1
		),
		array(
			'id' => 116,
			'role_id' => 10,
			'liste_droit_id' => 2
		),
		array(
			'id' => 117,
			'role_id' => 10,
			'liste_droit_id' => 3
		),
		array(
			'id' => 118,
			'role_id' => 10,
			'liste_droit_id' => 4
		),
		array(
			'id' => 119,
			'role_id' => 10,
			'liste_droit_id' => 5
		),
		array(
			'id' => 120,
			'role_id' => 10,
			'liste_droit_id' => 6
		),
		array(
			'id' => 121,
			'role_id' => 10,
			'liste_droit_id' => 7
		),
		array(
			'id' => 122,
			'role_id' => 10,
			'liste_droit_id' => 8
		),
		array(
			'id' => 123,
			'role_id' => 10,
			'liste_droit_id' => 9
		),
		array(
			'id' => 124,
			'role_id' => 10,
			'liste_droit_id' => 10
		),
		array(
			'id' => 125,
			'role_id' => 10,
			'liste_droit_id' => 12
		),
		array(
			'id' => 126,
			'role_id' => 10,
			'liste_droit_id' => 13
		),
		array(
			'id' => 127,
			'role_id' => 10,
			'liste_droit_id' => 14
		),
		array(
			'id' => 128,
			'role_id' => 10,
			'liste_droit_id' => 15
		),
		array(
			'id' => 129,
			'role_id' => 10,
			'liste_droit_id' => 16
		),
		array(
			'id' => 130,
			'role_id' => 10,
			'liste_droit_id' => 17
		),
		array(
			'id' => 131,
			'role_id' => 10,
			'liste_droit_id' => 18
		),
		array(
			'id' => 132,
			'role_id' => 10,
			'liste_droit_id' => 19
		),
		array(
			'id' => 133,
			'role_id' => 10,
			'liste_droit_id' => 21
		),
		array(
			'id' => 134,
			'role_id' => 10,
			'liste_droit_id' => 22
		),
		array(
			'id' => 135,
			'role_id' => 10,
			'liste_droit_id' => 23
		),
		array(
			'id' => 136,
			'role_id' => 10,
			'liste_droit_id' => 24
		),
		array(
			'id' => 137,
			'role_id' => 10,
			'liste_droit_id' => 25
		),
		array(
			'id' => 138,
			'role_id' => 10,
			'liste_droit_id' => 26
		),
		array(
			'id' => 139,
			'role_id' => 10,
			'liste_droit_id' => 27
		),
		array(
			'id' => 140,
			'role_id' => 10,
			'liste_droit_id' => 28
		),
		array(
			'id' => 141,
			'role_id' => 10,
			'liste_droit_id' => 29
		),
		array(
			'id' => 142,
			'role_id' => 10,
			'liste_droit_id' => 30
		),
		array(
			'id' => 143,
			'role_id' => 10,
			'liste_droit_id' => 31
		),
		array(
			'id' => 144,
			'role_id' => 10,
			'liste_droit_id' => 32
		),
		array(
			'id' => 145,
			'role_id' => 11,
			'liste_droit_id' => 1
		),
		array(
			'id' => 146,
			'role_id' => 11,
			'liste_droit_id' => 4
		),
		array(
			'id' => 147,
			'role_id' => 11,
			'liste_droit_id' => 7
		),
		array(
			'id' => 148,
			'role_id' => 11,
			'liste_droit_id' => 20
		),
		array(
			'id' => 149,
			'role_id' => 11,
			'liste_droit_id' => 30
		),
		array(
			'id' => 150,
			'role_id' => 12,
			'liste_droit_id' => 2
		),
		array(
			'id' => 151,
			'role_id' => 12,
			'liste_droit_id' => 4
		),
		array(
			'id' => 152,
			'role_id' => 12,
			'liste_droit_id' => 7
		),
		array(
			'id' => 153,
			'role_id' => 12,
			'liste_droit_id' => 20
		),
		array(
			'id' => 154,
			'role_id' => 12,
			'liste_droit_id' => 30
		),
		array(
			'id' => 155,
			'role_id' => 13,
			'liste_droit_id' => 3
		),
		array(
			'id' => 156,
			'role_id' => 13,
			'liste_droit_id' => 4
		),
		array(
			'id' => 157,
			'role_id' => 13,
			'liste_droit_id' => 20
		),
		array(
			'id' => 158,
			'role_id' => 13,
			'liste_droit_id' => 30
		),
		array(
			'id' => 159,
			'role_id' => 14,
			'liste_droit_id' => 1
		),
		array(
			'id' => 160,
			'role_id' => 14,
			'liste_droit_id' => 2
		),
		array(
			'id' => 161,
			'role_id' => 14,
			'liste_droit_id' => 3
		),
		array(
			'id' => 162,
			'role_id' => 14,
			'liste_droit_id' => 4
		),
		array(
			'id' => 163,
			'role_id' => 14,
			'liste_droit_id' => 7
		),
		array(
			'id' => 164,
			'role_id' => 14,
			'liste_droit_id' => 8
		),
		array(
			'id' => 165,
			'role_id' => 14,
			'liste_droit_id' => 9
		),
		array(
			'id' => 166,
			'role_id' => 14,
			'liste_droit_id' => 10
		),
		array(
			'id' => 167,
			'role_id' => 14,
			'liste_droit_id' => 12
		),
		array(
			'id' => 168,
			'role_id' => 14,
			'liste_droit_id' => 13
		),
		array(
			'id' => 169,
			'role_id' => 14,
			'liste_droit_id' => 14
		),
		array(
			'id' => 170,
			'role_id' => 14,
			'liste_droit_id' => 15
		),
		array(
			'id' => 171,
			'role_id' => 14,
			'liste_droit_id' => 16
		),
		array(
			'id' => 172,
			'role_id' => 14,
			'liste_droit_id' => 17
		),
		array(
			'id' => 173,
			'role_id' => 14,
			'liste_droit_id' => 18
		),
		array(
			'id' => 174,
			'role_id' => 14,
			'liste_droit_id' => 19
		),
		array(
			'id' => 175,
			'role_id' => 14,
			'liste_droit_id' => 21
		),
		array(
			'id' => 176,
			'role_id' => 14,
			'liste_droit_id' => 22
		),
		array(
			'id' => 177,
			'role_id' => 14,
			'liste_droit_id' => 23
		),
		array(
			'id' => 178,
			'role_id' => 14,
			'liste_droit_id' => 24
		),
		array(
			'id' => 179,
			'role_id' => 14,
			'liste_droit_id' => 25
		),
		array(
			'id' => 180,
			'role_id' => 14,
			'liste_droit_id' => 26
		),
		array(
			'id' => 181,
			'role_id' => 14,
			'liste_droit_id' => 27
		),
		array(
			'id' => 182,
			'role_id' => 14,
			'liste_droit_id' => 28
		),
		array(
			'id' => 183,
			'role_id' => 14,
			'liste_droit_id' => 29
		),
		array(
			'id' => 184,
			'role_id' => 14,
			'liste_droit_id' => 30
		),
		array(
			'id' => 185,
			'role_id' => 14,
			'liste_droit_id' => 31
		),
		array(
			'id' => 186,
			'role_id' => 14,
			'liste_droit_id' => 32
		),
		array(
			'id' => 187,
			'role_id' => 15,
			'liste_droit_id' => 1
		),
		array(
			'id' => 188,
			'role_id' => 15,
			'liste_droit_id' => 2
		),
		array(
			'id' => 189,
			'role_id' => 15,
			'liste_droit_id' => 3
		),
		array(
			'id' => 190,
			'role_id' => 15,
			'liste_droit_id' => 4
		),
		array(
			'id' => 191,
			'role_id' => 15,
			'liste_droit_id' => 5
		),
		array(
			'id' => 192,
			'role_id' => 15,
			'liste_droit_id' => 6
		),
		array(
			'id' => 193,
			'role_id' => 15,
			'liste_droit_id' => 7
		),
		array(
			'id' => 194,
			'role_id' => 15,
			'liste_droit_id' => 8
		),
		array(
			'id' => 195,
			'role_id' => 15,
			'liste_droit_id' => 9
		),
		array(
			'id' => 196,
			'role_id' => 15,
			'liste_droit_id' => 10
		),
		array(
			'id' => 197,
			'role_id' => 15,
			'liste_droit_id' => 12
		),
		array(
			'id' => 198,
			'role_id' => 15,
			'liste_droit_id' => 13
		),
		array(
			'id' => 199,
			'role_id' => 15,
			'liste_droit_id' => 14
		),
		array(
			'id' => 200,
			'role_id' => 15,
			'liste_droit_id' => 15
		),
		array(
			'id' => 201,
			'role_id' => 15,
			'liste_droit_id' => 16
		),
		array(
			'id' => 202,
			'role_id' => 15,
			'liste_droit_id' => 17
		),
		array(
			'id' => 203,
			'role_id' => 15,
			'liste_droit_id' => 18
		),
		array(
			'id' => 204,
			'role_id' => 15,
			'liste_droit_id' => 19
		),
		array(
			'id' => 205,
			'role_id' => 15,
			'liste_droit_id' => 21
		),
		array(
			'id' => 206,
			'role_id' => 15,
			'liste_droit_id' => 22
		),
		array(
			'id' => 207,
			'role_id' => 15,
			'liste_droit_id' => 23
		),
		array(
			'id' => 208,
			'role_id' => 15,
			'liste_droit_id' => 24
		),
		array(
			'id' => 209,
			'role_id' => 15,
			'liste_droit_id' => 25
		),
		array(
			'id' => 210,
			'role_id' => 15,
			'liste_droit_id' => 26
		),
		array(
			'id' => 211,
			'role_id' => 15,
			'liste_droit_id' => 27
		),
		array(
			'id' => 212,
			'role_id' => 15,
			'liste_droit_id' => 28
		),
		array(
			'id' => 213,
			'role_id' => 15,
			'liste_droit_id' => 29
		),
		array(
			'id' => 214,
			'role_id' => 15,
			'liste_droit_id' => 30
		),
		array(
			'id' => 215,
			'role_id' => 15,
			'liste_droit_id' => 31
		),
		array(
			'id' => 216,
			'role_id' => 15,
			'liste_droit_id' => 32
		),
		array(
			'id' => 217,
			'role_id' => 16,
			'liste_droit_id' => 1
		),
		array(
			'id' => 218,
			'role_id' => 16,
			'liste_droit_id' => 4
		),
		array(
			'id' => 219,
			'role_id' => 16,
			'liste_droit_id' => 7
		),
		array(
			'id' => 220,
			'role_id' => 16,
			'liste_droit_id' => 20
		),
		array(
			'id' => 221,
			'role_id' => 16,
			'liste_droit_id' => 30
		),
		array(
			'id' => 222,
			'role_id' => 17,
			'liste_droit_id' => 2
		),
		array(
			'id' => 223,
			'role_id' => 17,
			'liste_droit_id' => 4
		),
		array(
			'id' => 224,
			'role_id' => 17,
			'liste_droit_id' => 7
		),
		array(
			'id' => 225,
			'role_id' => 17,
			'liste_droit_id' => 20
		),
		array(
			'id' => 226,
			'role_id' => 17,
			'liste_droit_id' => 30
		),
		array(
			'id' => 227,
			'role_id' => 18,
			'liste_droit_id' => 3
		),
		array(
			'id' => 228,
			'role_id' => 18,
			'liste_droit_id' => 4
		),
		array(
			'id' => 229,
			'role_id' => 18,
			'liste_droit_id' => 20
		),
		array(
			'id' => 230,
			'role_id' => 18,
			'liste_droit_id' => 30
		),
		array(
			'id' => 231,
			'role_id' => 19,
			'liste_droit_id' => 1
		),
		array(
			'id' => 232,
			'role_id' => 19,
			'liste_droit_id' => 2
		),
		array(
			'id' => 233,
			'role_id' => 19,
			'liste_droit_id' => 3
		),
		array(
			'id' => 234,
			'role_id' => 19,
			'liste_droit_id' => 4
		),
		array(
			'id' => 235,
			'role_id' => 19,
			'liste_droit_id' => 7
		),
		array(
			'id' => 236,
			'role_id' => 19,
			'liste_droit_id' => 8
		),
		array(
			'id' => 237,
			'role_id' => 19,
			'liste_droit_id' => 9
		),
		array(
			'id' => 238,
			'role_id' => 19,
			'liste_droit_id' => 10
		),
		array(
			'id' => 239,
			'role_id' => 19,
			'liste_droit_id' => 12
		),
		array(
			'id' => 240,
			'role_id' => 19,
			'liste_droit_id' => 13
		),
		array(
			'id' => 241,
			'role_id' => 19,
			'liste_droit_id' => 14
		),
		array(
			'id' => 242,
			'role_id' => 19,
			'liste_droit_id' => 15
		),
		array(
			'id' => 243,
			'role_id' => 19,
			'liste_droit_id' => 16
		),
		array(
			'id' => 244,
			'role_id' => 19,
			'liste_droit_id' => 17
		),
		array(
			'id' => 245,
			'role_id' => 19,
			'liste_droit_id' => 18
		),
		array(
			'id' => 246,
			'role_id' => 19,
			'liste_droit_id' => 19
		),
		array(
			'id' => 247,
			'role_id' => 19,
			'liste_droit_id' => 21
		),
		array(
			'id' => 248,
			'role_id' => 19,
			'liste_droit_id' => 22
		),
		array(
			'id' => 249,
			'role_id' => 19,
			'liste_droit_id' => 23
		),
		array(
			'id' => 250,
			'role_id' => 19,
			'liste_droit_id' => 24
		),
		array(
			'id' => 251,
			'role_id' => 19,
			'liste_droit_id' => 25
		),
		array(
			'id' => 252,
			'role_id' => 19,
			'liste_droit_id' => 26
		),
		array(
			'id' => 253,
			'role_id' => 19,
			'liste_droit_id' => 27
		),
		array(
			'id' => 254,
			'role_id' => 19,
			'liste_droit_id' => 28
		),
		array(
			'id' => 255,
			'role_id' => 19,
			'liste_droit_id' => 29
		),
		array(
			'id' => 256,
			'role_id' => 19,
			'liste_droit_id' => 30
		),
		array(
			'id' => 257,
			'role_id' => 19,
			'liste_droit_id' => 31
		),
		array(
			'id' => 258,
			'role_id' => 19,
			'liste_droit_id' => 32
		),
		array(
			'id' => 259,
			'role_id' => 20,
			'liste_droit_id' => 1
		),
		array(
			'id' => 260,
			'role_id' => 20,
			'liste_droit_id' => 2
		),
		array(
			'id' => 261,
			'role_id' => 20,
			'liste_droit_id' => 3
		),
		array(
			'id' => 262,
			'role_id' => 20,
			'liste_droit_id' => 4
		),
		array(
			'id' => 263,
			'role_id' => 20,
			'liste_droit_id' => 5
		),
		array(
			'id' => 264,
			'role_id' => 20,
			'liste_droit_id' => 6
		),
		array(
			'id' => 265,
			'role_id' => 20,
			'liste_droit_id' => 7
		),
		array(
			'id' => 266,
			'role_id' => 20,
			'liste_droit_id' => 8
		),
		array(
			'id' => 267,
			'role_id' => 20,
			'liste_droit_id' => 9
		),
		array(
			'id' => 268,
			'role_id' => 20,
			'liste_droit_id' => 10
		),
		array(
			'id' => 269,
			'role_id' => 20,
			'liste_droit_id' => 12
		),
		array(
			'id' => 270,
			'role_id' => 20,
			'liste_droit_id' => 13
		),
		array(
			'id' => 271,
			'role_id' => 20,
			'liste_droit_id' => 14
		),
		array(
			'id' => 272,
			'role_id' => 20,
			'liste_droit_id' => 15
		),
		array(
			'id' => 273,
			'role_id' => 20,
			'liste_droit_id' => 16
		),
		array(
			'id' => 274,
			'role_id' => 20,
			'liste_droit_id' => 17
		),
		array(
			'id' => 275,
			'role_id' => 20,
			'liste_droit_id' => 18
		),
		array(
			'id' => 276,
			'role_id' => 20,
			'liste_droit_id' => 19
		),
		array(
			'id' => 277,
			'role_id' => 20,
			'liste_droit_id' => 21
		),
		array(
			'id' => 278,
			'role_id' => 20,
			'liste_droit_id' => 22
		),
		array(
			'id' => 279,
			'role_id' => 20,
			'liste_droit_id' => 23
		),
		array(
			'id' => 280,
			'role_id' => 20,
			'liste_droit_id' => 24
		),
		array(
			'id' => 281,
			'role_id' => 20,
			'liste_droit_id' => 25
		),
		array(
			'id' => 282,
			'role_id' => 20,
			'liste_droit_id' => 26
		),
		array(
			'id' => 283,
			'role_id' => 20,
			'liste_droit_id' => 27
		),
		array(
			'id' => 284,
			'role_id' => 20,
			'liste_droit_id' => 28
		),
		array(
			'id' => 285,
			'role_id' => 20,
			'liste_droit_id' => 29
		),
		array(
			'id' => 286,
			'role_id' => 20,
			'liste_droit_id' => 30
		),
		array(
			'id' => 287,
			'role_id' => 20,
			'liste_droit_id' => 31
		),
		array(
			'id' => 288,
			'role_id' => 20,
			'liste_droit_id' => 32
		),
		array(
			'id' => 289,
			'role_id' => 21,
			'liste_droit_id' => 1
		),
		array(
			'id' => 290,
			'role_id' => 21,
			'liste_droit_id' => 4
		),
		array(
			'id' => 291,
			'role_id' => 21,
			'liste_droit_id' => 7
		),
		array(
			'id' => 292,
			'role_id' => 21,
			'liste_droit_id' => 20
		),
		array(
			'id' => 293,
			'role_id' => 21,
			'liste_droit_id' => 30
		),
		array(
			'id' => 294,
			'role_id' => 22,
			'liste_droit_id' => 2
		),
		array(
			'id' => 295,
			'role_id' => 22,
			'liste_droit_id' => 4
		),
		array(
			'id' => 296,
			'role_id' => 22,
			'liste_droit_id' => 7
		),
		array(
			'id' => 297,
			'role_id' => 22,
			'liste_droit_id' => 20
		),
		array(
			'id' => 298,
			'role_id' => 22,
			'liste_droit_id' => 30
		),
		array(
			'id' => 299,
			'role_id' => 23,
			'liste_droit_id' => 3
		),
		array(
			'id' => 300,
			'role_id' => 23,
			'liste_droit_id' => 4
		),
		array(
			'id' => 301,
			'role_id' => 23,
			'liste_droit_id' => 20
		),
		array(
			'id' => 302,
			'role_id' => 23,
			'liste_droit_id' => 30
		),
		array(
			'id' => 303,
			'role_id' => 24,
			'liste_droit_id' => 1
		),
		array(
			'id' => 304,
			'role_id' => 24,
			'liste_droit_id' => 2
		),
		array(
			'id' => 305,
			'role_id' => 24,
			'liste_droit_id' => 3
		),
		array(
			'id' => 306,
			'role_id' => 24,
			'liste_droit_id' => 4
		),
		array(
			'id' => 307,
			'role_id' => 24,
			'liste_droit_id' => 7
		),
		array(
			'id' => 308,
			'role_id' => 24,
			'liste_droit_id' => 8
		),
		array(
			'id' => 309,
			'role_id' => 24,
			'liste_droit_id' => 9
		),
		array(
			'id' => 310,
			'role_id' => 24,
			'liste_droit_id' => 10
		),
		array(
			'id' => 311,
			'role_id' => 24,
			'liste_droit_id' => 12
		),
		array(
			'id' => 312,
			'role_id' => 24,
			'liste_droit_id' => 13
		),
		array(
			'id' => 313,
			'role_id' => 24,
			'liste_droit_id' => 14
		),
		array(
			'id' => 314,
			'role_id' => 24,
			'liste_droit_id' => 15
		),
		array(
			'id' => 315,
			'role_id' => 24,
			'liste_droit_id' => 16
		),
		array(
			'id' => 316,
			'role_id' => 24,
			'liste_droit_id' => 17
		),
		array(
			'id' => 317,
			'role_id' => 24,
			'liste_droit_id' => 18
		),
		array(
			'id' => 318,
			'role_id' => 24,
			'liste_droit_id' => 19
		),
		array(
			'id' => 319,
			'role_id' => 24,
			'liste_droit_id' => 21
		),
		array(
			'id' => 320,
			'role_id' => 24,
			'liste_droit_id' => 22
		),
		array(
			'id' => 321,
			'role_id' => 24,
			'liste_droit_id' => 23
		),
		array(
			'id' => 322,
			'role_id' => 24,
			'liste_droit_id' => 24
		),
		array(
			'id' => 323,
			'role_id' => 24,
			'liste_droit_id' => 25
		),
		array(
			'id' => 324,
			'role_id' => 24,
			'liste_droit_id' => 26
		),
		array(
			'id' => 325,
			'role_id' => 24,
			'liste_droit_id' => 27
		),
		array(
			'id' => 326,
			'role_id' => 24,
			'liste_droit_id' => 28
		),
		array(
			'id' => 327,
			'role_id' => 24,
			'liste_droit_id' => 29
		),
		array(
			'id' => 328,
			'role_id' => 24,
			'liste_droit_id' => 30
		),
		array(
			'id' => 329,
			'role_id' => 24,
			'liste_droit_id' => 31
		),
		array(
			'id' => 330,
			'role_id' => 24,
			'liste_droit_id' => 32
		),
		array(
			'id' => 331,
			'role_id' => 25,
			'liste_droit_id' => 1
		),
		array(
			'id' => 332,
			'role_id' => 25,
			'liste_droit_id' => 2
		),
		array(
			'id' => 333,
			'role_id' => 25,
			'liste_droit_id' => 3
		),
		array(
			'id' => 334,
			'role_id' => 25,
			'liste_droit_id' => 4
		),
		array(
			'id' => 335,
			'role_id' => 25,
			'liste_droit_id' => 5
		),
		array(
			'id' => 336,
			'role_id' => 25,
			'liste_droit_id' => 6
		),
		array(
			'id' => 337,
			'role_id' => 25,
			'liste_droit_id' => 7
		),
		array(
			'id' => 338,
			'role_id' => 25,
			'liste_droit_id' => 8
		),
		array(
			'id' => 339,
			'role_id' => 25,
			'liste_droit_id' => 9
		),
		array(
			'id' => 340,
			'role_id' => 25,
			'liste_droit_id' => 10
		),
		array(
			'id' => 341,
			'role_id' => 25,
			'liste_droit_id' => 12
		),
		array(
			'id' => 342,
			'role_id' => 25,
			'liste_droit_id' => 13
		),
		array(
			'id' => 343,
			'role_id' => 25,
			'liste_droit_id' => 14
		),
		array(
			'id' => 344,
			'role_id' => 25,
			'liste_droit_id' => 15
		),
		array(
			'id' => 345,
			'role_id' => 25,
			'liste_droit_id' => 16
		),
		array(
			'id' => 346,
			'role_id' => 25,
			'liste_droit_id' => 17
		),
		array(
			'id' => 347,
			'role_id' => 25,
			'liste_droit_id' => 18
		),
		array(
			'id' => 348,
			'role_id' => 25,
			'liste_droit_id' => 19
		),
		array(
			'id' => 349,
			'role_id' => 25,
			'liste_droit_id' => 21
		),
		array(
			'id' => 350,
			'role_id' => 25,
			'liste_droit_id' => 22
		),
		array(
			'id' => 351,
			'role_id' => 25,
			'liste_droit_id' => 23
		),
		array(
			'id' => 352,
			'role_id' => 25,
			'liste_droit_id' => 24
		),
		array(
			'id' => 353,
			'role_id' => 25,
			'liste_droit_id' => 25
		),
		array(
			'id' => 354,
			'role_id' => 25,
			'liste_droit_id' => 26
		),
		array(
			'id' => 355,
			'role_id' => 25,
			'liste_droit_id' => 27
		),
		array(
			'id' => 356,
			'role_id' => 25,
			'liste_droit_id' => 28
		),
		array(
			'id' => 357,
			'role_id' => 25,
			'liste_droit_id' => 29
		),
		array(
			'id' => 358,
			'role_id' => 25,
			'liste_droit_id' => 30
		),
		array(
			'id' => 359,
			'role_id' => 25,
			'liste_droit_id' => 31
		),
		array(
			'id' => 360,
			'role_id' => 25,
			'liste_droit_id' => 32
		),
        array(
            'id' => 361,
            'role_id' => 5,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 362,
            'role_id' => 5,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 363,
            'role_id' => 5,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 364,
            'role_id' => 10,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 365,
            'role_id' => 10,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 366,
            'role_id' => 10,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 367,
            'role_id' => 15,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 368,
            'role_id' => 15,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 369,
            'role_id' => 15,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 370,
            'role_id' => 4,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 371,
            'role_id' => 4,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 372,
            'role_id' => 4,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 373,
            'role_id' => 4,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 374,
            'role_id' => 4,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 375,
            'role_id' => 4,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 376,
            'role_id' => 4,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 377,
            'role_id' => 5,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 378,
            'role_id' => 5,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 379,
            'role_id' => 5,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 380,
            'role_id' => 5,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 381,
            'role_id' => 5,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 382,
            'role_id' => 5,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 383,
            'role_id' => 5,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 384,
            'role_id' => 9,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 385,
            'role_id' => 9,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 386,
            'role_id' => 9,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 387,
            'role_id' => 9,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 389,
            'role_id' => 9,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 390,
            'role_id' => 9,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 391,
            'role_id' => 9,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 392,
            'role_id' => 10,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 393,
            'role_id' => 10,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 394,
            'role_id' => 10,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 395,
            'role_id' => 10,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 396,
            'role_id' => 10,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 397,
            'role_id' => 10,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 398,
            'role_id' => 10,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 399,
            'role_id' => 14,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 400,
            'role_id' => 14,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 401,
            'role_id' => 14,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 402,
            'role_id' => 14,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 403,
            'role_id' => 14,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 404,
            'role_id' => 14,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 405,
            'role_id' => 14,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 406,
            'role_id' => 15,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 407,
            'role_id' => 15,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 408,
            'role_id' => 15,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 409,
            'role_id' => 15,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 410,
            'role_id' => 15,
           'liste_droit_id' => 37
        ),
        array(
            'id' => 411,
            'role_id' => 15,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 412,
            'role_id' => 15,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 413,
            'role_id' => 19,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 414,
            'role_id' => 19,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 415,
            'role_id' => 19,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 416,
            'role_id' => 19,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 417,
            'role_id' => 19,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 418,
            'role_id' => 19,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 419,
            'role_id' => 19,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 420,
            'role_id' => 20,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 421,
            'role_id' => 20,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 422,
            'role_id' => 20,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 423,
            'role_id' => 20,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 424,
            'role_id' => 20,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 425,
            'role_id' => 20,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 426,
            'role_id' => 20,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 427,
            'role_id' => 24,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 428,
            'role_id' => 24,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 429,
            'role_id' => 24,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 430,
            'role_id' => 24,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 431,
            'role_id' => 24,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 432,
            'role_id' => 24,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 433,
            'role_id' => 24,
            'liste_droit_id' => 39
        ),
        array(
            'id' => 434,
            'role_id' => 25,
            'liste_droit_id' => 33
        ),
        array(
            'id' => 435,
            'role_id' => 25,
            'liste_droit_id' => 34
        ),
        array(
            'id' => 436,
            'role_id' => 25,
            'liste_droit_id' => 35
        ),
        array(
            'id' => 437,
            'role_id' => 25,
            'liste_droit_id' => 36
        ),
        array(
            'id' => 438,
            'role_id' => 25,
            'liste_droit_id' => 37
        ),
        array(
            'id' => 439,
            'role_id' => 25,
            'liste_droit_id' => 38
        ),
        array(
            'id' => 440,
            'role_id' => 25,
            'liste_droit_id' => 39
        ),
	);

}
