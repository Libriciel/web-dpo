<?php
/**
 * LdapmGroup Fixture
 */
class LdapmGroupFixture extends CakeTestFixture {

    public $fields = array(
        'id' => ['type' => 'integer', 'key' => 'primary'],
        'parent_id' => ['type' => 'integer'],
        'lft' => ['type' => 'integer'],
        'rght' => ['type' => 'integer'],
        'name' => ['type' => 'string', 'length' => 500],
        'dn' => 'text'
    );

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
    );

}
