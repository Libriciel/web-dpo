<?php
/**
 * TypageOrganisation Fixture
 */
class TypageOrganisationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'TypageOrganisation');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'typage_id' => 1,
			'organisation_id' => 1
		),
		array(
			'id' => 3,
			'typage_id' => 2,
			'organisation_id' => 1
		),
	);

}
