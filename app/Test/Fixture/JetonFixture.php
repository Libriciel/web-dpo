<?php
/**
 * Jeton Fixture
 */
class JetonFixture extends CakeTestFixture {

    /**
     * Import
     *
     * @var array
     */
	public $import = [
	    'model' => 'Jeton'
    ];

    /**
     * Records
     *
     * @var array
     */
	public $records = [];

}
