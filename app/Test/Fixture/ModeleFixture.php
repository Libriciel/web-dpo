<?php
/**
 * Modele Fixture
 */
class ModeleFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = [
	    'model' => 'Modele'
    ];

/**
 * Records
 *
 * @var array
 */
	public $records = [
        [
            'id' => 1,
            'name_modele' => 'modele_formulaire.odt',
            'formulaire_id' => 1,
            'fichier' => '1623937979.odt',
            'created' => '2020-06-10 00:15:34.390532',
            'modified' => '2020-06-10 00:15:34.390532'
        ],
    ];
}
