<?php
/**
 * Soustraitance Fixture
 */
class SoustraitanceFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Soustraitance');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
