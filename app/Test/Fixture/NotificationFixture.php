<?php
/**
 * Notification Fixture
 */
class NotificationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Notification');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 9,
			'content' => 2,
			'fiche_id' => 2,
			'vu' => 0,
			'afficher' => 1,
			'created' => '2020-06-10 00:29:19',
			'modified' => '2020-06-10 00:29:19'
		),
		array(
			'id' => 3,
			'user_id' => 11,
			'content' => 2,
			'fiche_id' => 3,
			'vu' => 0,
			'afficher' => 1,
			'created' => '2020-06-10 00:29:30',
			'modified' => '2020-06-10 00:29:30'
		),
		array(
			'id' => 5,
			'user_id' => 8,
			'content' => 4,
			'fiche_id' => 4,
			'vu' => 0,
			'afficher' => 1,
			'created' => '2020-06-10 00:29:43',
			'modified' => '2020-06-10 00:29:43'
		),
		array(
			'id' => 8,
			'user_id' => 8,
			'content' => 3,
			'fiche_id' => 5,
			'vu' => 0,
			'afficher' => 1,
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 9,
			'user_id' => 10,
			'content' => 1,
			'fiche_id' => 6,
			'vu' => 0,
			'afficher' => 1,
			'created' => '2020-06-10 00:30:08',
			'modified' => '2020-06-10 00:30:08'
		),
		array(
			'id' => 13,
			'user_id' => 8,
			'content' => 5,
			'fiche_id' => 8,
			'vu' => 0,
			'afficher' => 0,
			'created' => '2020-06-10 00:30:35',
			'modified' => '2020-06-10 00:30:35'
		),
		array(
			'id' => 14,
			'user_id' => 8,
			'content' => 6,
			'fiche_id' => 10,
			'vu' => 0,
			'afficher' => 0,
			'created' => '2020-06-10 00:30:48',
			'modified' => '2020-06-10 00:30:48'
		),
        array(
            'id' => 15,
            'user_id' => 3,
            'content' => 2,
            'fiche_id' => 1,
            'vu' => 0,
            'afficher' => 0,
            'created' => '2020-06-10 00:30:48',
            'modified' => '2020-06-10 00:30:48'
        ),
        array(
            'id' => 16,
            'user_id' => 11,
            'content' => 2,
            'fiche_id' => 1,
            'vu' => 0,
            'afficher' => 0,
            'created' => '2020-06-10 00:30:48',
            'modified' => '2020-06-10 00:30:48'
        ),
        array(
            'id' => 17,
            'user_id' => 8,
            'content' => 2,
            'fiche_id' => 1,
            'vu' => 0,
            'afficher' => 0,
            'created' => '2020-06-10 00:30:48',
            'modified' => '2020-06-10 00:30:48'
        ),
        array(
            'id' => 18,
            'user_id' => 9,
            'content' => 2,
            'fiche_id' => 1,
            'vu' => 0,
            'afficher' => 0,
            'created' => '2020-06-10 00:30:48',
            'modified' => '2020-06-10 00:30:48'
        ),
        array(
            'id' => 19,
            'user_id' => 10,
            'content' => 2,
            'fiche_id' => 1,
            'vu' => 0,
            'afficher' => 0,
            'created' => '2020-06-10 00:30:48',
            'modified' => '2020-06-10 00:30:48'
        ),
	);

}
