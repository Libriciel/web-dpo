<?php
/**
 * Historique Fixture
 */
class HistoriqueFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Historique');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 1,
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 2,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 2,
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 3,
			'content' => 'Rébecca JAUNE a envoyé le traitement à Christian NOIR pour validation',
			'fiche_id' => 2,
			'created' => '2020-06-10 00:29:19',
			'modified' => '2020-06-10 00:29:19'
		),
		array(
			'id' => 4,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 3,
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 5,
			'content' => 'Rébecca JAUNE a envoyé le traitement à Christian NOIR pour validation',
			'fiche_id' => 3,
			'created' => '2020-06-10 00:29:26',
			'modified' => '2020-06-10 00:29:26'
		),
		array(
			'id' => 6,
			'content' => 'Christian NOIR valide et envoie le traitement pour validation du DPO',
			'fiche_id' => 3,
			'created' => '2020-06-10 00:29:30',
			'modified' => '2020-06-10 00:29:30'
		),
		array(
			'id' => 7,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 4,
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 8,
			'content' => 'Rébecca JAUNE a envoyé le traitement à Christian NOIR pour validation',
			'fiche_id' => 4,
			'created' => '2020-06-10 00:29:38',
			'modified' => '2020-06-10 00:29:38'
		),
		array(
			'id' => 9,
			'content' => 'Christian NOIR refuse le traitement',
			'fiche_id' => 4,
			'created' => '2020-06-10 00:29:43',
			'modified' => '2020-06-10 00:29:43'
		),
		array(
			'id' => 10,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 5,
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 11,
			'content' => 'Rébecca JAUNE a envoyé le traitement à Christian NOIR pour validation',
			'fiche_id' => 5,
			'created' => '2020-06-10 00:29:51',
			'modified' => '2020-06-10 00:29:51'
		),
		array(
			'id' => 12,
			'content' => 'Christian NOIR valide et envoie le traitement pour validation du DPO',
			'fiche_id' => 5,
			'created' => '2020-06-10 00:29:55',
			'modified' => '2020-06-10 00:29:55'
		),
		array(
			'id' => 13,
			'content' => 'Nicole ROUX valide le traitement et l\'insère au registre',
			'fiche_id' => 5,
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 14,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 6,
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 15,
			'content' => 'Rébecca JAUNE a demandé l\'avis de Michèle ROSE',
			'fiche_id' => 6,
			'created' => '2020-06-10 00:30:08',
			'modified' => '2020-06-10 00:30:08'
		),
		array(
			'id' => 16,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 7,
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 17,
			'content' => 'Rébecca JAUNE a envoyé le traitement à Christian NOIR pour validation',
			'fiche_id' => 7,
			'created' => '2020-06-10 00:30:15',
			'modified' => '2020-06-10 00:30:15'
		),
		array(
			'id' => 18,
			'content' => 'Christian NOIR refuse le traitement',
			'fiche_id' => 7,
			'created' => '2020-06-10 00:30:19',
			'modified' => '2020-06-10 00:30:19'
		),
		array(
			'id' => 19,
			'content' => 'Rébecca JAUNE replace le traitement en rédaction',
			'fiche_id' => 7,
			'created' => '2020-06-10 00:30:23',
			'modified' => '2020-06-10 00:30:23'
		),
		array(
			'id' => 20,
			'content' => 'Création du traitement par Rébecca JAUNE',
			'fiche_id' => 8,
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 21,
			'content' => 'Rébecca JAUNE a demandé l\'avis de Michèle ROSE',
			'fiche_id' => 8,
			'created' => '2020-06-10 00:30:32',
			'modified' => '2020-06-10 00:30:32'
		),
		array(
			'id' => 22,
			'content' => 'Michèle ROSE a répondu à la demande d\'avis de Rébecca JAUNE',
			'fiche_id' => 8,
			'created' => '2020-06-10 00:30:35',
			'modified' => '2020-06-10 00:30:35'
		),
		array(
			'id' => 23,
			'content' => 'Initialisation du traitement par Nicole ROUX',
			'fiche_id' => 9,
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 24,
			'content' => 'Initialisation du traitement par Nicole ROUX',
			'fiche_id' => 10,
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 25,
			'content' => 'Nicole ROUX le DPO, a envoyé le traitement initialisé à Rébecca JAUNE pour rédaction',
			'fiche_id' => 10,
			'created' => '2020-06-10 00:30:48',
			'modified' => '2020-06-10 00:30:48'
		),
		array(
			'id' => 26,
			'content' => 'Rédaction par Rébecca JAUNE du traitement initialisé par le DPO',
			'fiche_id' => 10,
			'created' => '2020-06-10 00:30:48',
			'modified' => '2020-06-10 00:30:48'
		),
	);

}
