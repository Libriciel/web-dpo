<?php
/**
 * Champ Fixture
 */
class ChampFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Champ');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'formulaire_id' => 1,
			'type' => 'input',
			'ligne' => 1,
			'colonne' => 1,
			'details' => '{"name":"test","placeholder":"Test","label":"Test","default":"","obligatoire":false}',
			'created' => '2020-06-10 00:29:03',
			'modified' => '2020-06-10 00:29:03',
			'champ_coresponsable' => 0,
			'champ_soustraitant' => 0
		),
	);

}
