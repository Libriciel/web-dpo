<?php
/**
 * Modification Fixture
 */
class ModificationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Modification');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
