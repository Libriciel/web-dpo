<?php
/**
 * FormulaireOrganisation Fixture
 */
class FormulaireOrganisationFixture extends CakeTestFixture
{

    /**
     * Import
     *
     * @var array
     */
    public $import = [
        'model' => 'FormulaireOrganisation'
    ];

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'formulaire_id' => 1,
            'organisation_id' => 1,
            'active' => 1,
        ],
//        [
//            'id' => 2,
//            'formulaire_id' => 1,
//            'organisation_id' => 2,
//            'active' => 1,
//        ],
//        [
//            'id' => 3,
//            'formulaire_id' => 1,
//            'organisation_id' => 3,
//            'active' => 0,
//        ],
    ];

}

