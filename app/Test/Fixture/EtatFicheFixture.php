<?php
/**
 * EtatFiche Fixture
 */
class EtatFicheFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'EtatFiche');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'fiche_id' => 1,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 2,
			'fiche_id' => 2,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 3,
			'fiche_id' => 2,
			'etat_id' => 2,
			'user_id' => 9,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:29:19',
			'modified' => '2020-06-10 00:29:19'
		),
		array(
			'id' => 4,
			'fiche_id' => 3,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 5,
			'fiche_id' => 3,
			'etat_id' => 2,
			'user_id' => 9,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:26',
			'modified' => '2020-06-10 00:29:26'
		),
		array(
			'id' => 6,
			'fiche_id' => 3,
			'etat_id' => 3,
			'user_id' => 9,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:30',
			'modified' => '2020-06-10 00:29:30'
		),
		array(
			'id' => 7,
			'fiche_id' => 3,
			'etat_id' => 2,
			'user_id' => 11,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:29:30',
			'modified' => '2020-06-10 00:29:30'
		),
		array(
			'id' => 8,
			'fiche_id' => 4,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 9,
			'fiche_id' => 4,
			'etat_id' => 2,
			'user_id' => 9,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:38',
			'modified' => '2020-06-10 00:29:38'
		),
		array(
			'id' => 10,
			'fiche_id' => 4,
			'etat_id' => 4,
			'user_id' => 8,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:29:43',
			'modified' => '2020-06-10 00:29:43'
		),
		array(
			'id' => 11,
			'fiche_id' => 5,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 12,
			'fiche_id' => 5,
			'etat_id' => 2,
			'user_id' => 9,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:51',
			'modified' => '2020-06-10 00:29:51'
		),
		array(
			'id' => 13,
			'fiche_id' => 5,
			'etat_id' => 3,
			'user_id' => 9,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:55',
			'modified' => '2020-06-10 00:29:55'
		),
		array(
			'id' => 14,
			'fiche_id' => 5,
			'etat_id' => 2,
			'user_id' => 11,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:29:55',
			'modified' => '2020-06-10 00:29:55'
		),
		array(
			'id' => 15,
			'fiche_id' => 5,
			'etat_id' => 5,
			'user_id' => 11,
			'previous_user_id' => 11,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 16,
			'fiche_id' => 6,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 17,
			'fiche_id' => 6,
			'etat_id' => 6,
			'user_id' => 10,
			'previous_user_id' => 8,
			'previous_etat_id' => 16,
			'actif' => 1,
			'created' => '2020-06-10 00:30:08',
			'modified' => '2020-06-10 00:30:08'
		),
		array(
			'id' => 18,
			'fiche_id' => 7,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 19,
			'fiche_id' => 7,
			'etat_id' => 2,
			'user_id' => 9,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:15',
			'modified' => '2020-06-10 00:30:15'
		),
		array(
			'id' => 20,
			'fiche_id' => 7,
			'etat_id' => 4,
			'user_id' => 8,
			'previous_user_id' => 9,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:19',
			'modified' => '2020-06-10 00:30:19'
		),
		array(
			'id' => 21,
			'fiche_id' => 7,
			'etat_id' => 8,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:30:23',
			'modified' => '2020-06-10 00:30:23'
		),
		array(
			'id' => 22,
			'fiche_id' => 8,
			'etat_id' => 1,
			'user_id' => 8,
			'previous_user_id' => 8,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 23,
			'fiche_id' => 8,
			'etat_id' => 6,
			'user_id' => 10,
			'previous_user_id' => 8,
			'previous_etat_id' => 22,
			'actif' => 0,
			'created' => '2020-06-10 00:30:32',
			'modified' => '2020-06-10 00:30:32'
		),
		array(
			'id' => 24,
			'fiche_id' => 8,
			'etat_id' => 10,
			'user_id' => 8,
			'previous_user_id' => 10,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:30:35',
			'modified' => '2020-06-10 00:30:35'
		),
		array(
			'id' => 25,
			'fiche_id' => 9,
			'etat_id' => 11,
			'user_id' => 11,
			'previous_user_id' => 11,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 26,
			'fiche_id' => 10,
			'etat_id' => 11,
			'user_id' => 11,
			'previous_user_id' => 11,
			'previous_etat_id' => null,
			'actif' => 0,
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 27,
			'fiche_id' => 10,
			'etat_id' => 12,
			'user_id' => 8,
			'previous_user_id' => 11,
			'previous_etat_id' => null,
			'actif' => 1,
			'created' => '2020-06-10 00:30:48',
			'modified' => '2020-06-10 00:30:48'
		),
	);

}
