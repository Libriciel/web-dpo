<?php
/**
 * Valeur Fixture
 */
class ValeurFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Valeur');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'fiche_id' => 1,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 2,
			'fiche_id' => 1,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 3,
			'fiche_id' => 1,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 4,
			'fiche_id' => 1,
			'valeur' => 'Traitement en cours de rédaction',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 5,
			'fiche_id' => 1,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 6,
			'fiche_id' => 1,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 7,
			'fiche_id' => 1,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "En cours de rédaction"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 8,
			'fiche_id' => 1,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10'
		),
		array(
			'id' => 9,
			'fiche_id' => 2,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 10,
			'fiche_id' => 2,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 11,
			'fiche_id' => 2,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 12,
			'fiche_id' => 2,
			'valeur' => 'Traitement en cours de validation',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 13,
			'fiche_id' => 2,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 14,
			'fiche_id' => 2,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 15,
			'fiche_id' => 2,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "En cours de validation"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 16,
			'fiche_id' => 2,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15'
		),
		array(
			'id' => 17,
			'fiche_id' => 3,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 18,
			'fiche_id' => 3,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 19,
			'fiche_id' => 3,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 20,
			'fiche_id' => 3,
			'valeur' => 'Traitement validé',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 21,
			'fiche_id' => 3,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 22,
			'fiche_id' => 3,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 23,
			'fiche_id' => 3,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Validé"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 24,
			'fiche_id' => 3,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23'
		),
		array(
			'id' => 25,
			'fiche_id' => 4,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 26,
			'fiche_id' => 4,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 27,
			'fiche_id' => 4,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 28,
			'fiche_id' => 4,
			'valeur' => 'Traitement refusé',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 29,
			'fiche_id' => 4,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 30,
			'fiche_id' => 4,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 31,
			'fiche_id' => 4,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Refusé"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 32,
			'fiche_id' => 4,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35'
		),
		array(
			'id' => 33,
			'fiche_id' => 5,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 34,
			'fiche_id' => 5,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 35,
			'fiche_id' => 5,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 36,
			'fiche_id' => 5,
			'valeur' => 'Traitement validé par le DPO',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 37,
			'fiche_id' => 5,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 38,
			'fiche_id' => 5,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 39,
			'fiche_id' => 5,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Validé par le DPO"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 40,
			'fiche_id' => 5,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:47'
		),
		array(
			'id' => 41,
			'fiche_id' => 5,
			'valeur' => 'Libriciel SCOP',
			'champ_name' => 'rt_organisation_raisonsociale',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 42,
			'fiche_id' => 5,
			'valeur' => '0400000000',
			'champ_name' => 'rt_organisation_telephone',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 43,
			'fiche_id' => 5,
			'valeur' => '836 Rue du Mas de Verchant, 34000 Montpellier',
			'champ_name' => 'rt_organisation_adresse',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 44,
			'fiche_id' => 5,
			'valeur' => 'contact-ls_recette-dp@libriciel.coop',
			'champ_name' => 'rt_organisation_email',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 45,
			'fiche_id' => 5,
			'valeur' => '18000000000000',
			'champ_name' => 'rt_organisation_siret',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 46,
			'fiche_id' => 5,
			'valeur' => '6202B',
			'champ_name' => 'rt_organisation_ape',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 47,
			'fiche_id' => 5,
			'valeur' => 'Mme.',
			'champ_name' => 'rt_organisation_civiliteresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 48,
			'fiche_id' => 5,
			'valeur' => 'VERT',
			'champ_name' => 'rt_organisation_nomresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 49,
			'fiche_id' => 5,
			'valeur' => 'Céline',
			'champ_name' => 'rt_organisation_prenomresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 50,
			'fiche_id' => 5,
			'valeur' => 'cvert_recette-dp@libriciel.coop',
			'champ_name' => 'rt_organisation_emailresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 51,
			'fiche_id' => 5,
			'valeur' => '0600000000',
			'champ_name' => 'rt_organisation_telephoneresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 52,
			'fiche_id' => 5,
			'valeur' => 'Président-directeur Général',
			'champ_name' => 'rt_organisation_fonctionresponsable',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 53,
			'fiche_id' => 5,
			'valeur' => '001',
			'champ_name' => 'rt_organisation_numerodpo',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 54,
			'fiche_id' => 5,
			'valeur' => 'nroux_recette-dp@libriciel.coop',
			'champ_name' => 'rt_organisation_email_dpo',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 55,
			'fiche_id' => 5,
			'valeur' => 'Mme. Nicole ROUX',
			'champ_name' => 'rt_organisation_nom_complet_dpo',
			'created' => '2020-06-10 00:29:59',
			'modified' => '2020-06-10 00:29:59'
		),
		array(
			'id' => 56,
			'fiche_id' => 6,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 57,
			'fiche_id' => 6,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 58,
			'fiche_id' => 6,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 59,
			'fiche_id' => 6,
			'valeur' => 'Traitement en demande d\'avis',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 60,
			'fiche_id' => 6,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 61,
			'fiche_id' => 6,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 62,
			'fiche_id' => 6,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Demande d\'avis"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 63,
			'fiche_id' => 6,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04'
		),
		array(
			'id' => 64,
			'fiche_id' => 7,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 65,
			'fiche_id' => 7,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 66,
			'fiche_id' => 7,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 67,
			'fiche_id' => 7,
			'valeur' => 'Traitement replacé en rédaction',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 68,
			'fiche_id' => 7,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 69,
			'fiche_id' => 7,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 70,
			'fiche_id' => 7,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Replacer en rédaction"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 71,
			'fiche_id' => 7,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11'
		),
		array(
			'id' => 72,
			'fiche_id' => 8,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 73,
			'fiche_id' => 8,
			'valeur' => 'Direction Administration, Finances et Moyens Généraux',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 74,
			'fiche_id' => 8,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 75,
			'fiche_id' => 8,
			'valeur' => 'Traitement en réponse à la demande d\'avis',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 76,
			'fiche_id' => 8,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 77,
			'fiche_id' => 8,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 78,
			'fiche_id' => 8,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Réponse à la demande d\'avis"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 79,
			'fiche_id' => 8,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28'
		),
		array(
			'id' => 80,
			'fiche_id' => 9,
			'valeur' => 'Nicole ROUX',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 81,
			'fiche_id' => 9,
			'valeur' => 'Direction Service Clients',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 82,
			'fiche_id' => 9,
			'valeur' => 'nroux_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 83,
			'fiche_id' => 9,
			'valeur' => 'Traitement initialisé par le DPO',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 84,
			'fiche_id' => 9,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 85,
			'fiche_id' => 9,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 86,
			'fiche_id' => 9,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Initialisation du traitement par le DPO"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 87,
			'fiche_id' => 9,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38'
		),
		array(
			'id' => 88,
			'fiche_id' => 10,
			'valeur' => 'Rébecca JAUNE',
			'champ_name' => 'declarantpersonnenom',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:48'
		),
		array(
			'id' => 89,
			'fiche_id' => 10,
			'valeur' => 'Direction Service Clients',
			'champ_name' => 'declarantservice',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 90,
			'fiche_id' => 10,
			'valeur' => 'rjaune_recette-dp@libriciel.coop',
			'champ_name' => 'declarantpersonneemail',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:48'
		),
		array(
			'id' => 91,
			'fiche_id' => 10,
			'valeur' => 'Traitement initialisé et envoyé en rédaction',
			'champ_name' => 'outilnom',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 92,
			'fiche_id' => 10,
			'valeur' => 'Non',
			'champ_name' => 'transfertHorsUe',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 93,
			'fiche_id' => 10,
			'valeur' => 'Non',
			'champ_name' => 'donneesSensible',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 94,
			'fiche_id' => 10,
			'valeur' => 'Tester le parcours du traitement jusqu\'à l\'état "Rédaction du traitement initialisé"',
			'champ_name' => 'finaliteprincipale',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
		array(
			'id' => 95,
			'fiche_id' => 10,
			'valeur' => 'Non',
			'champ_name' => 'decisionAutomatisee',
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:43'
		),
	);

}
