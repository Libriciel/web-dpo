<?php
/**
 * OrganisationUser Fixture
 */
class OrganisationUserFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrganisationUser');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 3, //ibleu
			'organisation_id' => 1, //Libriciel SCOP
			'created' => '2020-06-10 00:17:12',
			'modified' => '2020-06-10 00:17:12'
		),
		array(
			'id' => 2,
			'user_id' => 4, //findigo
			'organisation_id' => 2, //Montpellier Méditerranée Métropole
			'created' => '2020-06-10 00:17:16',
			'modified' => '2020-06-10 00:17:16'
		),
		array(
			'id' => 3,
			'user_id' => 5, //jgris
			'organisation_id' => 3, //Métropole Européenne de Lille
			'created' => '2020-06-10 00:17:21',
			'modified' => '2020-06-10 00:17:21'
		),
		array(
			'id' => 4,
			'user_id' => 6, //lsepia
			'organisation_id' => 4, //Metz Métropole
			'created' => '2020-06-10 00:17:25',
			'modified' => '2020-06-10 00:17:25'
		),
		array(
			'id' => 5,
			'user_id' => 7, //mrubis
			'organisation_id' => 5, //Eurométropole de Strasbourg
			'created' => '2020-06-10 00:17:30',
			'modified' => '2020-06-10 00:17:30'
		),
		array(
			'id' => 6,
			'user_id' => 8, //rjaune
			'organisation_id' => 1, //Libriciel SCOP
			'created' => '2020-06-10 00:18:24',
			'modified' => '2020-06-10 00:18:24'
		),
		array(
			'id' => 7,
			'user_id' => 9, //cnoir
			'organisation_id' => 1, //Libriciel SCOP
			'created' => '2020-06-10 00:18:28',
			'modified' => '2020-06-10 00:18:28'
		),
		array(
			'id' => 8,
			'user_id' => 10, //mrose
			'organisation_id' => 1, //Libriciel SCOP
			'created' => '2020-06-10 00:18:31',
			'modified' => '2020-06-10 00:18:31'
		),
		array(
			'id' => 9,
			'user_id' => 11, //nroux
			'organisation_id' => 1, //Libriciel SCOP
			'created' => '2020-06-10 00:18:34',
			'modified' => '2020-06-10 00:18:34'
		),
		array(
			'id' => 10,
			'user_id' => 12, //pmagenta
			'organisation_id' => 2, //Montpellier Méditerranée Métropole
			'created' => '2020-06-10 00:18:38',
			'modified' => '2020-06-10 00:18:38'
		),
		array(
			'id' => 11,
			'user_id' => 13, //amauve
			'organisation_id' => 2, //Montpellier Méditerranée Métropole
			'created' => '2020-06-10 00:18:40',
			'modified' => '2020-06-10 00:18:40'
		),
		array(
			'id' => 12,
			'user_id' => 14, //mazur
			'organisation_id' => 2, //Montpellier Méditerranée Métropole
			'created' => '2020-06-10 00:18:43',
			'modified' => '2020-06-10 00:18:43'
		),
		array(
			'id' => 13,
			'user_id' => 15, //hvermeil
			'organisation_id' => 2, //Montpellier Méditerranée Métropole
			'created' => '2020-06-10 00:18:46',
			'modified' => '2020-06-10 00:18:46'
		),
		array(
			'id' => 14,
			'user_id' => 16, //sorange
			'organisation_id' => 3, //Métropole Européenne de Lille
			'created' => '2020-06-10 00:18:49',
			'modified' => '2020-06-10 00:18:49'
		),
		array(
			'id' => 15,
			'user_id' => 17, //plavande
			'organisation_id' => 3, //Métropole Européenne de Lille
			'created' => '2020-06-10 00:18:52',
			'modified' => '2020-06-10 00:18:52'
		),
		array(
			'id' => 16,
			'user_id' => 18, //mlilas
			'organisation_id' => 3, //Métropole Européenne de Lille
			'created' => '2020-06-10 00:18:55',
			'modified' => '2020-06-10 00:18:55'
		),
		array(
			'id' => 17,
			'user_id' => 19, //vpaille
			'organisation_id' => 3, //Métropole Européenne de Lille
			'created' => '2020-06-10 00:18:58',
			'modified' => '2020-06-10 00:18:58'
		),
	);

}
