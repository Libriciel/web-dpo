<?php
/**
 * Etat Fixture
 */
class EtatFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Etat');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'libelle' => 'En cours de rédaction',
			'value' => 1,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 2,
			'libelle' => 'En cours de validation',
			'value' => 2,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 3,
			'libelle' => 'Validée',
			'value' => 3,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 4,
			'libelle' => 'Refusée',
			'value' => 4,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 5,
			'libelle' => 'Validée par le DPO',
			'value' => 5,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 6,
			'libelle' => 'Demande d avis',
			'value' => 6,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 7,
			'libelle' => 'Archivée',
			'value' => 7,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 8,
			'libelle' => 'Replacer en rédaction',
			'value' => 8,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 9,
			'libelle' => 'Modification du traitement inséré au registre',
			'value' => 9,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 10,
			'libelle' => 'Réponse à la demande d avis',
			'value' => 10,
			'created' => '2020-06-10 00:15:34.390532',
			'modified' => '2020-06-10 00:15:34.390532'
		),
		array(
			'id' => 11,
			'libelle' => 'Initialisation du traitement par le DPO',
			'value' => 11,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
		array(
			'id' => 12,
			'libelle' => 'Rédaction du traitement initialisé',
			'value' => 12,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
	);

}
