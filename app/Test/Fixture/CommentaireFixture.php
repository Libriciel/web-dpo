<?php
/**
 * Commentaire Fixture
 */
class CommentaireFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Commentaire');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'etat_fiches_id' => 10,
			'content' => 'Commentaire de refus par cnoir',
			'user_id' => 9,
			'destinataire_id' => 8,
			'created' => '2020-06-10 00:29:43',
			'modified' => '2020-06-10 00:29:43'
		),
		array(
			'id' => 2,
			'etat_fiches_id' => 20,
			'content' => 'Commentaire de refus par cnoir',
			'user_id' => 9,
			'destinataire_id' => 8,
			'created' => '2020-06-10 00:30:19',
			'modified' => '2020-06-10 00:30:19'
		),
		array(
			'id' => 3,
			'etat_fiches_id' => 22,
			'content' => 'Avis du consultant mrose',
			'user_id' => 10,
			'destinataire_id' => 8,
			'created' => '2020-06-10 00:30:35',
			'modified' => '2020-06-10 00:30:35'
		),
	);

}
