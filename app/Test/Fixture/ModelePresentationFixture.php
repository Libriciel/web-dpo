<?php
/**
 * ModelePresentation Fixture
 */
class ModelePresentationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'ModelePresentation');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
