<?php
/**
 * Article Fixture
 */
class ArticleFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Article');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
