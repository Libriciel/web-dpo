<?php
/**
 * Authentification Fixture
 */
class AuthentificationFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Authentification');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
	);

}
