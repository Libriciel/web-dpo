<?php
/**
 * Formulaire Fixture
 */
class FormulaireFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Formulaire');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'libelle' => 'Formulaire basique',
			'active' => 1,
			'description' => 'Formulaire basique permettant de créer des traitements dans les différents états',
			'soustraitant' => 0,
			'created' => '2020-06-10 00:29:00',
			'modified' => '2020-06-10 00:29:03',
			'usesousfinalite' => 0,
			'usebaselegale' => 0,
			'usedecisionautomatisee' => 0,
			'usetransferthorsue' => 0,
			'usedonneessensible' => 0,
			'useallextensionfiles' => 0,
			'usepia' => 0,
			'oldformulaire' => 0,
			'rt_externe' => 0,
            'createdbyorganisation' => 2,
            'archive' => false
		),
		array(
			'id' => 2,
			'libelle' => 'Formulaire en cours de création',
			'active' => 0,
			'description' => null,
			'soustraitant' => 0,
			'created' => '2020-06-10 00:29:08',
			'modified' => '2020-06-10 00:29:08',
			'usesousfinalite' => 0,
			'usebaselegale' => 0,
			'usedecisionautomatisee' => 0,
			'usetransferthorsue' => 0,
			'usedonneessensible' => 0,
			'useallextensionfiles' => 0,
			'usepia' => 0,
			'oldformulaire' => 0,
			'rt_externe' => 0,
            'createdbyorganisation' => 1, //Libiriciel SCOP
            'archive' => false
		),
	);

}
