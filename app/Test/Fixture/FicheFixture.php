<?php
/**
 * Fiche Fixture
 */
class FicheFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Fiche');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 2,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:29:15',
			'modified' => '2020-06-10 00:29:15',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 3,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:29:23',
			'modified' => '2020-06-10 00:29:23',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 4,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:29:35',
			'modified' => '2020-06-10 00:29:35',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 5,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => 15,
			'numero' => 'DPO-1',
			'coresponsable' => 0,
			'created' => '2020-06-10 00:29:47',
			'modified' => '2020-06-10 00:29:59',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 6,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:30:04',
			'modified' => '2020-06-10 00:30:04',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 7,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:30:11',
			'modified' => '2020-06-10 00:30:11',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 8,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:30:28',
			'modified' => '2020-06-10 00:30:28',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 9,
			'user_id' => 11,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:30:38',
			'modified' => '2020-06-10 00:30:38',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
		array(
			'id' => 10,
			'user_id' => 8,
			'form_id' => 1,
			'organisation_id' => 1,
			'norme_id' => null,
			'numero' => null,
			'coresponsable' => 0,
			'created' => '2020-06-10 00:30:43',
			'modified' => '2020-06-10 00:30:48',
			'soustraitance' => 0,
			'obligation_pia' => 0,
			'realisation_pia' => 0,
			'depot_pia' => 0,
			'rt_externe' => 0
		),
	);

}
