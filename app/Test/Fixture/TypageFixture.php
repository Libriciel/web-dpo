<?php
/**
 * Typage Fixture
 */
class TypageFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Typage');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'libelle' => 'Sous-traitance',
			'createdbyorganisation' => null,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
		array(
			'id' => 2,
			'libelle' => 'Co-responsabilité',
			'createdbyorganisation' => null,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
		array(
			'id' => 3,
			'libelle' => 'AIPD',
			'createdbyorganisation' => null,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
		array(
			'id' => 4,
			'libelle' => 'Autre',
			'createdbyorganisation' => null,
			'created' => '2020-06-10 00:15:34.567556',
			'modified' => '2020-06-10 00:15:34.567556'
		),
		array(
			'id' => 6,
			'libelle' => 'Non utilisé général',
			'createdbyorganisation' => 4,
			'created' => '2020-06-10 00:23:41',
			'modified' => '2020-06-10 00:23:41'
		),
		array(
			'id' => 8,
			'libelle' => 'Non utilisé LS',
			'createdbyorganisation' => 1,
			'created' => '2020-06-10 00:23:47',
			'modified' => '2020-06-10 00:23:47'
		),
	);

}
