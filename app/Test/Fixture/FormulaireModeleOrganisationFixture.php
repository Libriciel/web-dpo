<?php
/**
 * FormulaireModeleOrganisation Fixture
 */
class FormulaireModeleOrganisationFixture extends CakeTestFixture
{

    /**
     * Import
     *
     * @var array
     */
    public $import = [
        'model' => 'FormulaireModeleOrganisation'
    ];

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'formulaire_id' => 1,
            'modele_id' => 1,
            'organisation_id' => 1,
        ],
        [
            'id' => 2,
            'formulaire_id' => 1,
            'modele_id' => 1,
            'organisation_id' => 2,
        ],
        [
            'id' => 3,
            'formulaire_id' => 1,
            'modele_id' => 1,
            'organisation_id' => 3,
        ],
    ];

}

