<?php
/**
 * OrganisationUserRole Fixture
 */
class OrganisationUserRoleFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'OrganisationUserRole');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'organisation_user_id' => 1,
			'role_id' => 4,
			'created' => '2020-06-10 00:17:12',
			'modified' => '2020-06-10 00:17:12'
		),
		array(
			'id' => 2,
			'organisation_user_id' => 2,
			'role_id' => 9,
			'created' => '2020-06-10 00:17:16',
			'modified' => '2020-06-10 00:17:16'
		),
		array(
			'id' => 3,
			'organisation_user_id' => 3,
			'role_id' => 14,
			'created' => '2020-06-10 00:17:21',
			'modified' => '2020-06-10 00:17:21'
		),
		array(
			'id' => 4,
			'organisation_user_id' => 4,
			'role_id' => 19,
			'created' => '2020-06-10 00:17:25',
			'modified' => '2020-06-10 00:17:25'
		),
		array(
			'id' => 5,
			'organisation_user_id' => 5,
			'role_id' => 24,
			'created' => '2020-06-10 00:17:30',
			'modified' => '2020-06-10 00:17:30'
		),
		array(
			'id' => 6,
			'organisation_user_id' => 6,
			'role_id' => 1,
			'created' => '2020-06-10 00:18:24',
			'modified' => '2020-06-10 00:18:24'
		),
		array(
			'id' => 7,
			'organisation_user_id' => 7,
			'role_id' => 2,
			'created' => '2020-06-10 00:18:28',
			'modified' => '2020-06-10 00:18:28'
		),
		array(
			'id' => 8,
			'organisation_user_id' => 8,
			'role_id' => 3,
			'created' => '2020-06-10 00:18:31',
			'modified' => '2020-06-10 00:18:31'
		),
		array(
			'id' => 10,
			'organisation_user_id' => 10,
			'role_id' => 6,
			'created' => '2020-06-10 00:18:38',
			'modified' => '2020-06-10 00:18:38'
		),
		array(
			'id' => 11,
			'organisation_user_id' => 11,
			'role_id' => 7,
			'created' => '2020-06-10 00:18:40',
			'modified' => '2020-06-10 00:18:40'
		),
		array(
			'id' => 12,
			'organisation_user_id' => 12,
			'role_id' => 8,
			'created' => '2020-06-10 00:18:43',
			'modified' => '2020-06-10 00:18:43'
		),
		array(
			'id' => 14,
			'organisation_user_id' => 14,
			'role_id' => 11,
			'created' => '2020-06-10 00:18:49',
			'modified' => '2020-06-10 00:18:49'
		),
		array(
			'id' => 15,
			'organisation_user_id' => 15,
			'role_id' => 12,
			'created' => '2020-06-10 00:18:52',
			'modified' => '2020-06-10 00:18:52'
		),
		array(
			'id' => 16,
			'organisation_user_id' => 16,
			'role_id' => 13,
			'created' => '2020-06-10 00:18:55',
			'modified' => '2020-06-10 00:18:55'
		),
		array(
			'id' => 18,
			'organisation_user_id' => 9,
			'role_id' => 5,
			'created' => '2020-06-10 00:19:01',
			'modified' => '2020-06-10 00:19:01'
		),
		array(
			'id' => 19,
			'organisation_user_id' => 13,
			'role_id' => 10,
			'created' => '2020-06-10 00:19:04',
			'modified' => '2020-06-10 00:19:04'
		),
		array(
			'id' => 20,
			'organisation_user_id' => 17,
			'role_id' => 15,
			'created' => '2020-06-10 00:19:06',
			'modified' => '2020-06-10 00:19:06'
		),
	);

}
