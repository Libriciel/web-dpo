<?php
/**
 * OrganisationUserService Fixture
 */
class OrganisationUserServiceFixture extends CakeTestFixture
{

    /**
     * Import
     *
     * @var array
     */
	public $import = [
	    'model' => 'OrganisationUserService'
    ];

    /**
     * Records
     *
     * @var array
     */
	public $records = [
		[
			'id' => 1,
			'organisation_user_id' => 6,
			'service_id' => 1,
			'created' => '2020-06-10 00:18:24',
			'modified' => '2020-06-10 00:18:24'
		],
		[
			'id' => 2,
			'organisation_user_id' => 7,
			'service_id' => 2,
			'created' => '2020-06-10 00:18:28',
			'modified' => '2020-06-10 00:18:28'
		],
		[
			'id' => 3,
			'organisation_user_id' => 8,
			'service_id' => 3,
			'created' => '2020-06-10 00:18:31',
			'modified' => '2020-06-10 00:18:31'
		],
		[
			'id' => 4,
			'organisation_user_id' => 9,
			'service_id' => 4,
			'created' => '2020-06-10 00:18:34',
			'modified' => '2020-06-10 00:18:34'
		],
		[
			'id' => 5,
			'organisation_user_id' => 10,
			'service_id' => 5,
			'created' => '2020-06-10 00:18:38',
			'modified' => '2020-06-10 00:18:38'
		],
		[
			'id' => 6,
			'organisation_user_id' => 11,
			'service_id' => 6,
			'created' => '2020-06-10 00:18:40',
			'modified' => '2020-06-10 00:18:40'
		],
		[
			'id' => 7,
			'organisation_user_id' => 12,
			'service_id' => 7,
			'created' => '2020-06-10 00:18:43',
			'modified' => '2020-06-10 00:18:43'
		],
		[
			'id' => 8,
			'organisation_user_id' => 13,
			'service_id' => 8,
			'created' => '2020-06-10 00:18:46',
			'modified' => '2020-06-10 00:18:46'
		],
		[
			'id' => 9,
			'organisation_user_id' => 14,
			'service_id' => 11,
			'created' => '2020-06-10 00:18:49',
			'modified' => '2020-06-10 00:18:49'
		],
		[
			'id' => 10,
			'organisation_user_id' => 15,
			'service_id' => 11,
			'created' => '2020-06-10 00:18:52',
			'modified' => '2020-06-10 00:18:52'
		],
		[
			'id' => 11,
			'organisation_user_id' => 16,
			'service_id' => 11,
			'created' => '2020-06-10 00:18:55',
			'modified' => '2020-06-10 00:18:55'
		],
		[
			'id' => 12,
			'organisation_user_id' => 17,
			'service_id' => 11,
			'created' => '2020-06-10 00:18:58',
			'modified' => '2020-06-10 00:18:58'
		],
	];

}
