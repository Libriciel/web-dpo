<?php
/**
 * Role Fixture
 */
class RoleFixture extends CakeTestFixture {

/**
 * Import
 *
 * @var array
 */
	public $import = array('model' => 'Role');

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'libelle' => 'Rédacteur',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 2,
			'libelle' => 'Valideur',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 3,
			'libelle' => 'Consultant',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 4,
			'libelle' => 'Administrateur',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 5,
			'libelle' => 'DPO',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:16:04',
			'modified' => '2020-06-10 00:16:04'
		),
		array(
			'id' => 6,
			'libelle' => 'Rédacteur',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 7,
			'libelle' => 'Valideur',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 8,
			'libelle' => 'Consultant',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 9,
			'libelle' => 'Administrateur',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 10,
			'libelle' => 'DPO',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:16:08',
			'modified' => '2020-06-10 00:16:08'
		),
		array(
			'id' => 11,
			'libelle' => 'Rédacteur',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 12,
			'libelle' => 'Valideur',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 13,
			'libelle' => 'Consultant',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 14,
			'libelle' => 'Administrateur',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 15,
			'libelle' => 'DPO',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:16:11',
			'modified' => '2020-06-10 00:16:11'
		),
		array(
			'id' => 16,
			'libelle' => 'Rédacteur',
			'organisation_id' => 4,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 17,
			'libelle' => 'Valideur',
			'organisation_id' => 4,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 18,
			'libelle' => 'Consultant',
			'organisation_id' => 4,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 19,
			'libelle' => 'Administrateur',
			'organisation_id' => 4,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 20,
			'libelle' => 'DPO',
			'organisation_id' => 4,
			'created' => '2020-06-10 00:16:14',
			'modified' => '2020-06-10 00:16:14'
		),
		array(
			'id' => 21,
			'libelle' => 'Rédacteur',
			'organisation_id' => 5,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 22,
			'libelle' => 'Valideur',
			'organisation_id' => 5,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 23,
			'libelle' => 'Consultant',
			'organisation_id' => 5,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 24,
			'libelle' => 'Administrateur',
			'organisation_id' => 5,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 25,
			'libelle' => 'DPO',
			'organisation_id' => 5,
			'created' => '2020-06-10 00:16:17',
			'modified' => '2020-06-10 00:16:17'
		),
		array(
			'id' => 31,
			'libelle' => 'Lecteur',
			'organisation_id' => 1,
			'created' => '2020-06-10 00:19:09',
			'modified' => '2020-06-10 00:19:09'
		),
		array(
			'id' => 32,
			'libelle' => 'Lecteur',
			'organisation_id' => 2,
			'created' => '2020-06-10 00:19:12',
			'modified' => '2020-06-10 00:19:12'
		),
		array(
			'id' => 33,
			'libelle' => 'Lecteur',
			'organisation_id' => 3,
			'created' => '2020-06-10 00:19:15',
			'modified' => '2020-06-10 00:19:15'
		),
	);

}
