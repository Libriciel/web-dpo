<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe ResponsablesController.
 *
 * ./cake_utils.sh tests app Controller/ResponsablesController
 *
 * @package app.Test.Case.Controller
 */
class ResponsablesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.ListeDroit',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Responsable',
        'app.ResponsableOrganisation',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Responsables');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/responsables/add'],
            [200, 'Administrateur.ibleu', '/responsables/add'],
            [200, 'DPO.nroux', '/responsables/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/responsables/add'],
            [403, 'Valideur.cnoir', '/responsables/add'],
            [403, 'Consultant.mrose', '/responsables/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/responsables/delete/1'],
            [302, 'Administrateur.ibleu', '/responsables/delete/1'],
            [302, 'DPO.nroux', '/responsables/delete/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/responsables/delete/1'],
            [403, 'DPO.hvermeil', '/responsables/delete/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/responsables/delete/666'],
            [404, 'Administrateur.ibleu', '/responsables/delete/666'],
            [404, 'DPO.nroux', '/responsables/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/responsables/delete/1'],
            [403, 'Valideur.cnoir', '/responsables/delete/1'],
            [403, 'Consultant.mrose', '/responsables/delete/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/responsables/delete/666'],
            [403, 'Valideur.cnoir', '/responsables/delete/666'],
            [403, 'Consultant.mrose', '/responsables/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDissocierResponsable() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant et associé
            [302, 'Superadministrateur.superadmin', '/responsables/dissocierResponsable/8'],
            [302, 'Administrateur.ibleu', '/responsables/dissocierResponsable/8'],
            [302, 'DPO.nroux', '/responsables/dissocierResponsable/8'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/responsables/dissocierResponsable/8'],
            [403, 'DPO.hvermeil', '/responsables/dissocierResponsable/8'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/responsables/dissocierResponsable/666'],
            [404, 'Administrateur.ibleu', '/responsables/dissocierResponsable/666'],
            [404, 'DPO.nroux', '/responsables/dissocierResponsable/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/responsables/dissocierResponsable/8'],
            [403, 'Valideur.cnoir', '/responsables/dissocierResponsable/8'],
            [403, 'Consultant.mrose', '/responsables/dissocierResponsable/8'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/responsables/dissocierResponsable/666'],
            [403, 'Valideur.cnoir', '/responsables/dissocierResponsable/666'],
            [403, 'Consultant.mrose', '/responsables/dissocierResponsable/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDissocierResponsable
     */
    public function testAccessDissocierResponsable($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/responsables/edit/1'],
            [200, 'Administrateur.ibleu', '/responsables/edit/1'],
            [200, 'DPO.nroux', '/responsables/edit/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/responsables/edit/1'],
            [403, 'DPO.hvermeil', '/responsables/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/responsables/edit/666'],
            [404, 'Administrateur.ibleu', '/responsables/edit/666'],
            [404, 'DPO.nroux', '/responsables/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/responsables/edit/1'],
            [403, 'Valideur.cnoir', '/responsables/edit/1'],
            [403, 'Consultant.mrose', '/responsables/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/responsables/edit/666'],
            [403, 'Valideur.cnoir', '/responsables/edit/666'],
            [403, 'Consultant.mrose', '/responsables/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEntite() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/responsables/entite'],
            [200, 'Administrateur.ibleu', '/responsables/entite'],
            [200, 'DPO.nroux', '/responsables/entite'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/responsables/entite'],
            [403, 'Valideur.cnoir', '/responsables/entite'],
            [403, 'Consultant.mrose', '/responsables/entite'],
        ];
    }

    /**
     * @dataProvider dataAccessEntite
     */
    public function testAccessEntite($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/responsables/index'],
            [200, 'Administrateur.ibleu', '/responsables/index'],
            [200, 'DPO.nroux', '/responsables/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/responsables/index'],
            [403, 'Valideur.cnoir', '/responsables/index'],
            [403, 'Consultant.mrose', '/responsables/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessShow() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/responsables/show/1'],
            [200, 'Administrateur.ibleu', '/responsables/show/1'],
            [200, 'DPO.nroux', '/responsables/show/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/responsables/show/666'],
            [404, 'Administrateur.ibleu', '/responsables/show/666'],
            [404, 'DPO.nroux', '/responsables/show/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/responsables/show/1'],
            [403, 'Valideur.cnoir', '/responsables/show/1'],
            [403, 'Consultant.mrose', '/responsables/show/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/responsables/show/666'],
            [403, 'Valideur.cnoir', '/responsables/show/666'],
            [403, 'Consultant.mrose', '/responsables/show/666'],
        ];
    }

    /**
     * @dataProvider dataAccessShow
     */
    public function testAccessShow($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
