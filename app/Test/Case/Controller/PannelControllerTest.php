<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe PannelController.
 *
 * ./cake_utils.sh tests app Controller/PannelController
 *
 * @package app.Test.Case.Controller
 */
class PannelControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Commentaire',
        'app.Droit',
        'app.EtatFiche',
        'app.Fiche',
        'app.Historique',
        'app.ListeDroit',
        'app.Modification',
        'app.Norme',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Pannel');
    }

    public function dataAccessAllTraitements() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'DPO.nroux', '/pannel/all_traitements'],
            [200, 'Administrateur.ibleu', '/pannel/all_traitements'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/all_traitements'],
            [403, 'Rédacteur.rjaune', '/pannel/all_traitements'],
            [403, 'Valideur.cnoir', '/pannel/all_traitements'],
            [403, 'Consultant.mrose', '/pannel/all_traitements'],
        ];
    }

    /**
     * @dataProvider dataAccessAllTraitements
     */
    public function testAccessAllTraitements($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessArchives() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/archives'],
            [200, 'DPO.nroux', '/pannel/archives'],
            [200, 'Rédacteur.rjaune', '/pannel/archives'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/archives'],
            [403, 'Valideur.cnoir', '/pannel/archives'],
            [403, 'Consultant.mrose', '/pannel/archives'],
        ];
    }

    /**
     * @dataProvider dataAccessArchives
     */
    public function testAccessArchives($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAttente() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/attente'],
            [200, 'DPO.nroux', '/pannel/attente'],
            [200, 'Rédacteur.rjaune', '/pannel/attente'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/attente'],
            [403, 'Valideur.cnoir', '/pannel/attente'],
            [403, 'Consultant.mrose', '/pannel/attente'],
        ];
    }

    /**
     * @dataProvider dataAccessAttente
     */
    public function testAccessAttente($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessConsulte() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/consulte'],
            [200, 'DPO.nroux', '/pannel/consulte'],
            [200, 'Valideur.cnoir', '/pannel/consulte'],
            [200, 'Consultant.mrose', '/pannel/consulte'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/consulte'],
            [403, 'Rédacteur.rjaune', '/pannel/consulte'],
        ];
    }

    /**
     * @dataProvider dataAccessConsulte
     */
    public function testAccessConsulte($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDropNotif() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/pannel/dropNotif'],
            [302, 'Administrateur.ibleu', '/pannel/dropNotif'],
            [302, 'DPO.nroux', '/pannel/dropNotif'],
            [302, 'Rédacteur.rjaune', '/pannel/dropNotif'],
            [302, 'Valideur.cnoir', '/pannel/dropNotif'],
            [302, 'Consultant.mrose', '/pannel/dropNotif'],
        ];
    }

    /**
     * @dataProvider dataAccessDropNotif
     */
    public function testAccessDropNotif($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEncoursRedaction() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/encours_redaction'],
            [200, 'DPO.nroux', '/pannel/encours_redaction'],
            [200, 'Rédacteur.rjaune', '/pannel/encours_redaction'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/encours_redaction'],
            [403, 'Valideur.cnoir', '/pannel/encours_redaction'],
            [403, 'Consultant.mrose', '/pannel/encours_redaction'],
        ];
    }

    /**
     * @dataProvider dataAccessEncoursRedaction
     */
    public function testAccessEncoursRedaction($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessGetHistorique() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/pannel/getHistorique/1'],
            [200, 'DPO.nroux', '/pannel/getHistorique/1'],
            [200, 'Rédacteur.rjaune', '/pannel/getHistorique/1'],
            [200, 'Valideur.cnoir', '/pannel/getHistorique/1'],
            [200, 'Consultant.mrose', '/pannel/getHistorique/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/pannel/getHistorique/1'],
            [403, 'DPO.hvermeil', '/pannel/getHistorique/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/pannel/getHistorique/666'],
            [404, 'DPO.nroux', '/pannel/getHistorique/666'],
            [404, 'Rédacteur.rjaune', '/pannel/getHistorique/666'],
            [404, 'Valideur.cnoir', '/pannel/getHistorique/666'],
            [404, 'Consultant.mrose', '/pannel/getHistorique/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/pannel/getHistorique/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/pannel/getHistorique/666'],
        ];
    }

    /**
     * @dataProvider dataAccessGetHistorique
     */
    public function testAccessGetHistorique($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/pannel/index'],
            [200, 'Administrateur.ibleu', '/pannel/index'],
            [200, 'DPO.nroux', '/pannel/index'],
            [200, 'Rédacteur.rjaune', '/pannel/index'],
            [200, 'Valideur.cnoir', '/pannel/index'],
            [200, 'Consultant.mrose', '/pannel/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessInitialisation() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'DPO.nroux', '/pannel/initialisation'],
            [200, 'Administrateur.ibleu', '/pannel/initialisation'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/initialisation'],
            [403, 'Rédacteur.rjaune', '/pannel/initialisation'],
            [403, 'Valideur.cnoir', '/pannel/initialisation'],
            [403, 'Consultant.mrose', '/pannel/initialisation'],
        ];
    }

    /**
     * @dataProvider dataAccessInitialisation
     */
    public function testAccessInitialisation($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessNotifAfficher() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/pannel/notifAfficher/1'],
            [200, 'DPO.nroux', '/pannel/notifAfficher/1'],
            [200, 'Rédacteur.rjaune', '/pannel/notifAfficher/1'],
            [200, 'Valideur.cnoir', '/pannel/notifAfficher/1'],
            [200, 'Consultant.mrose', '/pannel/notifAfficher/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [404, 'Administrateur.findigo', '/pannel/notifAfficher/2'],
            [404, 'DPO.hvermeil', '/pannel/notifAfficher/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/pannel/notifAfficher/666'],
            [404, 'DPO.nroux', '/pannel/notifAfficher/666'],
            [404, 'Rédacteur.rjaune', '/pannel/notifAfficher/666'],
            [404, 'Valideur.cnoir', '/pannel/notifAfficher/666'],
            [404, 'Consultant.mrose', '/pannel/notifAfficher/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/pannel/notifAfficher/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/pannel/notifAfficher/666'],
        ];
    }

    /**
     * @dataProvider dataAccessNotifAfficher
     */
    public function testAccessNotifAfficher($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessParcours() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/pannel/parcours/1'],
            [200, 'DPO.nroux', '/pannel/parcours/1'],
            [200, 'Rédacteur.rjaune', '/pannel/parcours/1'],
            [200, 'Valideur.cnoir', '/pannel/parcours/1'],
            [200, 'Consultant.mrose', '/pannel/parcours/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/pannel/parcours/1'],
            [403, 'DPO.hvermeil', '/pannel/parcours/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/pannel/parcours/666'],
            [404, 'DPO.nroux', '/pannel/parcours/666'],
            [404, 'Rédacteur.rjaune', '/pannel/parcours/666'],
            [404, 'Valideur.cnoir', '/pannel/parcours/666'],
            [404, 'Consultant.mrose', '/pannel/parcours/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/pannel/parcours/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/pannel/parcours/666'],
        ];
    }

    /**
     * @dataProvider dataAccessParcours
     */
    public function testAccessParcours($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRecuConsultation() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/recuConsultation'],
            [200, 'DPO.nroux', '/pannel/recuConsultation'],
            [200, 'Consultant.mrose', '/pannel/recuConsultation'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/recuConsultation'],
            [403, 'Rédacteur.rjaune', '/pannel/recuConsultation'],
            [403, 'Valideur.cnoir', '/pannel/recuConsultation'],
        ];
    }

    /**
     * @dataProvider dataAccessRecuConsultation
     */
    public function testAccessRecuConsultation($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRecuValidation() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/recuValidation'],
            [200, 'DPO.nroux', '/pannel/recuValidation'],
            [200, 'Valideur.cnoir', '/pannel/recuValidation'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/recuValidation'],
            [403, 'Rédacteur.rjaune', '/pannel/recuValidation'],
            [403, 'Consultant.mrose', '/pannel/recuValidation'],
        ];
    }

    /**
     * @dataProvider dataAccessRecuValidation
     */
    public function testAccessRecuValidation($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRefuser() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/pannel/refuser'],
            [200, 'DPO.nroux', '/pannel/refuser'],
            [200, 'Rédacteur.rjaune', '/pannel/refuser'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/refuser'],
            [403, 'Valideur.cnoir', '/pannel/refuser'],
            [403, 'Consultant.mrose', '/pannel/refuser'],
        ];
    }

    /**
     * @dataProvider dataAccessRefuser
     */
    public function testAccessRefuser($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessSuperadmin() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/pannel/superadmin'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/pannel/superadmin'],
            [403, 'DPO.nroux', '/pannel/superadmin'],
            [403, 'Rédacteur.rjaune', '/pannel/superadmin'],
            [403, 'Valideur.cnoir', '/pannel/superadmin'],
            [403, 'Consultant.mrose', '/pannel/superadmin'],
        ];
    }

    /**
     * @dataProvider dataAccessSuperadmin
     */
    public function testAccessSuperadmin($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessSupprimerLaNotif() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/pannel/supprimerLaNotif/1'],
            [200, 'DPO.nroux', '/pannel/supprimerLaNotif/1'],
            [200, 'Rédacteur.rjaune', '/pannel/supprimerLaNotif/1'],
            [200, 'Valideur.cnoir', '/pannel/supprimerLaNotif/1'],
            [200, 'Consultant.mrose', '/pannel/supprimerLaNotif/1'],
            // Un utilisateur mono-collectivité peut accéder à l'enregistrement d'une autre collectivité
            [200, 'Administrateur.findigo', '/pannel/supprimerLaNotif/1'],
            [200, 'DPO.hvermeil', '/pannel/supprimerLaNotif/1'],
            // 1.2. Enregistrement inexistant
            [200, 'Administrateur.ibleu', '/pannel/supprimerLaNotif/666'],
            [200, 'DPO.nroux', '/pannel/supprimerLaNotif/666'],
            [200, 'Rédacteur.rjaune', '/pannel/supprimerLaNotif/666'],
            [200, 'Valideur.cnoir', '/pannel/supprimerLaNotif/666'],
            [200, 'Consultant.mrose', '/pannel/supprimerLaNotif/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/pannel/supprimerLaNotif/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/pannel/supprimerLaNotif/666'],
        ];
    }

    /**
     * @dataProvider dataAccessSupprimerLaNotif
     */
    public function testAccessSupprimerLaNotif($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessTechnique() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Administrateur.ibleu', '/pannel/technique'],
            [302, 'DPO.nroux', '/pannel/technique'],
            [302, 'Superadministrateur.superadmin', '/pannel/technique'],
            [302, 'Rédacteur.rjaune', '/pannel/technique'],
            [302, 'Valideur.cnoir', '/pannel/technique'],
            [302, 'Consultant.mrose', '/pannel/technique'],
        ];
    }

    /**
     * @dataProvider dataAccessTechnique
     */
    public function testAccessTechnique($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessValidNotif() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Administrateur.ibleu', '/pannel/validNotif'],
            [302, 'DPO.nroux', '/pannel/validNotif'],
            [302, 'Superadministrateur.superadmin', '/pannel/validNotif'],
            [302, 'Rédacteur.rjaune', '/pannel/validNotif'],
            [302, 'Valideur.cnoir', '/pannel/validNotif'],
            [302, 'Consultant.mrose', '/pannel/validNotif'],
        ];
    }

    /**
     * @dataProvider dataAccessValidNotif
     */
    public function testAccessValidNotif($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
