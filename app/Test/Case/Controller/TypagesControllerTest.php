<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe TypagesController.
 *
 * ./cake_utils.sh tests app Controller/TypagesController
 *
 * @package app.Test.Case.Controller
 */
class TypagesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.ListeDroit',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Role',
        'app.RoleDroit',
        'app.Typage',
        'app.TypageOrganisation',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Typages');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/typages/add'],
            [200, 'Administrateur.ibleu', '/typages/add'],
            [200, 'DPO.nroux', '/typages/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/typages/add'],
            [403, 'Valideur.cnoir', '/typages/add'],
            [403, 'Consultant.mrose', '/typages/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            // 1.1.1. Enregistrement existant et attaché à des entités
            [403, 'Superadministrateur.superadmin', '/typages/delete/1'],
            [403, 'Administrateur.ibleu', '/typages/delete/1'],
            [403, 'DPO.nroux', '/typages/delete/1'],
            // 1.1.2. Enregistrement existant et non attaché à des entités
            [302, 'Superadministrateur.superadmin', '/typages/delete/3'],
            //[302, 'Administrateur.ibleu', '/typages/delete/3'],
            //[302, 'DPO.nroux', '/typages/delete/3'],
            // 2.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/typages/delete/666'],
            [404, 'Administrateur.ibleu', '/typages/delete/666'],
            [404, 'DPO.nroux', '/typages/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/typages/delete/3'],
            [403, 'Valideur.cnoir', '/typages/delete/3'],
            [403, 'Consultant.mrose', '/typages/delete/3'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/typages/delete/666'],
            [403, 'Valideur.cnoir', '/typages/delete/666'],
            [403, 'Consultant.mrose', '/typages/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDissocier() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/typages/dissocier/1'],
            [302, 'Administrateur.ibleu', '/typages/dissocier/1'],
            [302, 'DPO.nroux', '/typages/dissocier/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/typages/dissocier/2'],
            [403, 'DPO.hvermeil', '/typages/dissocier/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/typages/dissocier/666'],
            [404, 'Administrateur.ibleu', '/typages/dissocier/666'],
            [404, 'DPO.nroux', '/typages/dissocier/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/typages/dissocier/1'],
            [403, 'Valideur.cnoir', '/typages/dissocier/1'],
            [403, 'Consultant.mrose', '/typages/dissocier/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/typages/dissocier/666'],
            [403, 'Valideur.cnoir', '/typages/dissocier/666'],
            [403, 'Consultant.mrose', '/typages/dissocier/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDissocier
     */
    public function xtestAccessDissocier($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/typages/edit/3'],
            //[302, 'Administrateur.ibleu', '/typages/edit/3'],
            //[302, 'DPO.nroux', '/typages/edit/3'],
            // 2.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/typages/edit/666'],
            [404, 'Administrateur.ibleu', '/typages/edit/666'],
            [404, 'DPO.nroux', '/typages/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/typages/edit/3'],
            [403, 'Valideur.cnoir', '/typages/edit/3'],
            [403, 'Consultant.mrose', '/typages/edit/3'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/typages/edit/666'],
            [403, 'Valideur.cnoir', '/typages/edit/666'],
            [403, 'Consultant.mrose', '/typages/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/typages/index'],
            [200, 'Administrateur.ibleu', '/typages/index'],
            [200, 'DPO.nroux', '/typages/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/typages/index'],
            [403, 'Valideur.cnoir', '/typages/index'],
            [403, 'Consultant.mrose', '/typages/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
