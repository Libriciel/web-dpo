<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe CronsController.
 *
 * ./cake_utils.sh tests app Controller/CronsController
 *
 * @package app.Test.Case.Controller
 */
class CronsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Cron',
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Crons');
    }

    public function dataAccessDeverrouiller() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/crons/deverrouiller/1', ['session' => ['Organisation.id' => 1]]],
            [302, 'Administrateur.ibleu', '/crons/deverrouiller/1'],
            [302, 'DPO.nroux', '/crons/deverrouiller/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/crons/deverrouiller/1'],
            [403, 'DPO.hvermeil', '/crons/deverrouiller/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/crons/deverrouiller/666', ['session' => ['Organisation.id' => 1]]],
            [404, 'Administrateur.ibleu', '/crons/deverrouiller/666'],
            [404, 'DPO.nroux', '/crons/deverrouiller/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/crons/deverrouiller/1'],
            [403, 'Valideur.cnoir', '/crons/deverrouiller/1'],
            [403, 'Consultant.mrose', '/crons/deverrouiller/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/crons/deverrouiller/666'],
            [403, 'Valideur.cnoir', '/crons/deverrouiller/666'],
            [403, 'Consultant.mrose', '/crons/deverrouiller/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDeverrouiller
     */
    public function testAccessDeverrouiller($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessExecuter() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/crons/executer/1', ['session' => ['Organisation.id' => 1]]],
            [302, 'Administrateur.ibleu', '/crons/executer/1'],
            [302, 'DPO.nroux', '/crons/executer/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/crons/executer/1'],
            [403, 'DPO.hvermeil', '/crons/executer/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/crons/executer/666', ['session' => ['Organisation.id' => 1]]],
            [404, 'Administrateur.ibleu', '/crons/executer/666'],
            [404, 'DPO.nroux', '/crons/executer/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/crons/executer/1'],
            [403, 'Valideur.cnoir', '/crons/executer/1'],
            [403, 'Consultant.mrose', '/crons/executer/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/crons/executer/666'],
            [403, 'Valideur.cnoir', '/crons/executer/666'],
            [403, 'Consultant.mrose', '/crons/executer/666'],
        ];
    }

    /**
     * @dataProvider dataAccessExecuter
     */
    public function testAccessExecuter($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/crons/index'],
            [200, 'Administrateur.ibleu', '/crons/index'],
            [200, 'DPO.nroux', '/crons/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/crons/index'],
            [403, 'Valideur.cnoir', '/crons/index'],
            [403, 'Consultant.mrose', '/crons/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessPlanifier() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/crons/planifier/1', ['session' => ['Organisation.id' => 1]]],
            [200, 'Administrateur.ibleu', '/crons/planifier/1'],
            [200, 'DPO.nroux', '/crons/planifier/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/crons/planifier/1'],
            [403, 'DPO.hvermeil', '/crons/planifier/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/crons/planifier/666', ['session' => ['Organisation.id' => 1]]],
            [404, 'Administrateur.ibleu', '/crons/planifier/666'],
            [404, 'DPO.nroux', '/crons/planifier/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/crons/planifier/1'],
            [403, 'Valideur.cnoir', '/crons/planifier/1'],
            [403, 'Consultant.mrose', '/crons/planifier/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/crons/planifier/666'],
            [403, 'Valideur.cnoir', '/crons/planifier/666'],
            [403, 'Consultant.mrose', '/crons/planifier/666'],
        ];
    }

    /**
     * @dataProvider dataAccessPlanifier
     */
    public function testAccessPlanifier($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessToggle() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/crons/toggle/1', ['session' => ['Organisation.id' => 1]]],
            [302, 'Administrateur.ibleu', '/crons/toggle/1'],
            [302, 'DPO.nroux', '/crons/toggle/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/crons/toggle/1'],
            [403, 'DPO.hvermeil', '/crons/toggle/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/crons/toggle/666', ['session' => ['Organisation.id' => 1]]],
            [404, 'Administrateur.ibleu', '/crons/toggle/666'],
            [404, 'DPO.nroux', '/crons/toggle/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/crons/toggle/1'],
            [403, 'Valideur.cnoir', '/crons/toggle/1'],
            [403, 'Consultant.mrose', '/crons/toggle/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/crons/toggle/666'],
            [403, 'Valideur.cnoir', '/crons/toggle/666'],
            [403, 'Consultant.mrose', '/crons/toggle/666'],
        ];
    }

    /**
     * @dataProvider dataAccessToggle
     */
    public function testAccessToggle($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
