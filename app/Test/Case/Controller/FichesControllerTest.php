<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe FichesController.
 *
 * ./cake_utils.sh tests app Controller/FichesController
 *
 * @package app.Test.Case.Controller
 */
class FichesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Champ',
        'app.Commentaire',
        'app.Coresponsable',
        'app.EtatFiche',
        'app.Fiche',
        'app.Fichier',
        'app.Formulaire',
        'app.FormulaireOrganisation',
        'app.Jeton',
        'app.Modele',
        'app.Modification',
        'app.Norme',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserService',
        'app.ResponsableOrganisation',
        'app.Responsable',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.Soustraitance',
        'app.Soustraitant',
        'app.SoustraitantOrganisation',
        'app.Typage',
        'app.TypageOrganisation',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Fiches');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/fiches/add/1'],
            [200, 'DPO.nroux', '/fiches/add/1'],
            [200, 'Rédacteur.rjaune', '/fiches/add/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [200, 'Administrateur.findigo', '/fiches/add/1'],
            [200, 'DPO.hvermeil', '/fiches/add/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Rédacteur.rjaune', '/fiches/add/666'],
            [404, 'Administrateur.ibleu', '/fiches/add/666'],
            [404, 'DPO.nroux', '/fiches/add/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/fiches/add/1'],
            [403, 'Valideur.cnoir', '/fiches/add/1'],
            [403, 'Consultant.mrose', '/fiches/add/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/fiches/add/666'],
            [403, 'Valideur.cnoir', '/fiches/add/666'],
            [403, 'Consultant.mrose', '/fiches/add/666'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function dataAccessArchive() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Administrateur.ibleu', '/fiches/archive/1'],
//            [403, 'DPO.nroux', '/fiches/archive/1'],
//            [403, 'Rédacteur.rjaune', '/fiches/archive/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/archive/1'],
//            [403, 'DPO.hvermeil', '/fiches/archive/1'],
//            // 1.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/archive/666'],
//            [403, 'Administrateur.ibleu', '/fiches/archive/666'],
//            [403, 'DPO.nroux', '/fiches/archive/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Superadministrateur.superadmin', '/fiches/archive/1'],
//            [403, 'Valideur.cnoir', '/fiches/archive/1'],
//            [403, 'Consultant.mrose', '/fiches/archive/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Superadministrateur.superadmin', '/fiches/archive/666'],
//            [403, 'Valideur.cnoir', '/fiches/archive/666'],
//            [403, 'Consultant.mrose', '/fiches/archive/666'],
//        ];
//    }

//    /**
//     * @dataProvider dataAccessArchive
//     */
//    public function testAccessArchive($expectedStatus, $user, $url, $options = []) {
//        $this->assertActionAccess($expectedStatus, $user, $url, $options);
//    }

    public function dataAccessDelete() {
        return [
            // @info: Admin, DPO et rédacteur peuvent rédiger un traitement
            // @info: Fiches créées par les utilisateurs 8 (rjaune) et 11 (nroux)
            // @info: la fiche ne peut pas être dans l'état VALIDER_DPO (5) ou EtatFiche::ARCHIVER (7) qui n'existe plus
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            //@todo: ibleu n'a créé aucune fiche
            //[302, 'Administrateur.ibleu', '/fiches/delete/1'],
            [302, 'DPO.nroux', '/fiches/delete/9'],
            [302, 'Rédacteur.rjaune', '/fiches/delete/1'],
            // Un utilisateur ne peut pas supprimer une fiche qu'il n'a pas créé
            [403, 'Administrateur.ibleu', '/fiches/delete/1'],
            [403, 'DPO.nroux', '/fiches/delete/1'],
            [403, 'Rédacteur.rjaune', '/fiches/delete/9'],
            // Un utilisateur ne peut pas supprimer une fiche qu'il a pas créé mais qui est en état "" ou ""
            //@todo: ibleu n'a créé aucune fiche
            //[403, 'Administrateur.ibleu', '/fiches/delete/1'],
            //@todo: nroux n'a créé aucune fiche se trouvant dans cet état
            //[403, 'DPO.nroux', '/fiches/delete/1'],
            [403, 'Rédacteur.rjaune', '/fiches/delete/5'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/fiches/delete/1'],
            [403, 'DPO.hvermeil', '/fiches/delete/1'],
            [403, 'Rédacteur.pmagenta', '/fiches/delete/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/fiches/delete/666'],
            [404, 'DPO.nroux', '/fiches/delete/666'],
            [404, 'Rédacteur.rjaune', '/fiches/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/fiches/delete/1'],
            [403, 'Valideur.cnoir', '/fiches/delete/1'],
            [403, 'Consultant.mrose', '/fiches/delete/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/fiches/delete/666'],
            [403, 'Valideur.cnoir', '/fiches/delete/666'],
            [403, 'Consultant.mrose', '/fiches/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function testAccessDeleteFile() {
//        $this->markTestSkipped('Aucune idée concernant la façon de tester, s\'agissant de fichiers');
//    }

//    public function testAccessDeleteRecordingFile() {
//        $this->markTestSkipped('Aucune idée concernant la façon de tester, s\'agissant de fichiers');
//    }

//    public function testAccessDownload() {
//        $this->markTestSkipped('Aucune idée concernant la façon de tester, s\'agissant de fichiers');
//    }

//    public function testAccessDownloadFileExtrait() {
//        $this->markTestSkipped('Aucune idée concernant la façon de tester, s\'agissant de fichiers');
//    }

//    public function testAccessDownloadFileTraitement() {
//        $this->markTestSkipped('Aucune idée concernant la façon de tester, s\'agissant de fichiers');
//    }

    public function dataAccessEdit() {
        // @info: Admin, DPO et rédacteur peuvent rédiger un traitement
        // @info: Fiches créées par les utilisateurs 8 (rjaune) et 11 (nroux)
        // @info: la fiche ne peut pas être dans l'état VALIDER_DPO (5) ou EtatFiche::ARCHIVER (7) qui n'existe plus
        // @info: si elle se trouve dans l'état ENCOURS_VALIDATION (2) alors l'état doit appartenir à l'utilssateur actuellement connecté
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant, créé par l'utilisateur
            //@todo: ibleu n'a créé aucune fiche
            //[200, 'Administrateur.ibleu', '/fiches/edit/1'],
            [200, 'DPO.nroux', '/fiches/edit/3'],
            [200, 'Rédacteur.rjaune', '/fiches/edit/3'],
            [200, 'Valideur.cnoir', '/fiches/edit/2'],
            // Un utilisateur ne peut pas modifier une fiche qu'il a créé mais qui dans un état "VALIDER_DPO"
            [403, 'Rédacteur.rjaune', '/fiches/edit/5'],
            // Un utilisateur ne peut pas modifier une fiche qu'il n'a pas créé
            [403, 'Administrateur.ibleu', '/fiches/edit/3'],
            [403, 'DPO.nroux', '/fiches/edit/1'],
            [403, 'Rédacteur.rjaune', '/fiches/edit/9'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/fiches/edit/1'],
            [403, 'DPO.hvermeil', '/fiches/edit/1'],
            [403, 'Rédacteur.pmagenta', '/fiches/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/fiches/edit/666'],
            [404, 'DPO.nroux', '/fiches/edit/666'],
            [404, 'Rédacteur.rjaune', '/fiches/edit/666'],
            [404, 'Valideur.cnoir', '/fiches/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/fiches/edit/1'],
            [403, 'Consultant.mrose', '/fiches/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/fiches/edit/666'],
            [404, 'Consultant.mrose', '/fiches/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function dataAccessExport() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/fiches/export/1'],
//            [302, 'Administrateur.ibleu', '/fiches/export/1'],
//            [302, 'DPO.nroux', '/fiches/export/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/export/2'],
//            [403, 'DPO.hvermeil', '/fiches/export/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/fiches/export/666'],
//            [404, 'Administrateur.ibleu', '/fiches/export/666'],
//            [404, 'DPO.nroux', '/fiches/export/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/fiches/export/1'],
//            [403, 'Valideur.cnoir', '/fiches/export/1'],
//            [403, 'Consultant.mrose', '/fiches/export/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/export/666'],
//            [403, 'Valideur.cnoir', '/fiches/export/666'],
//            [403, 'Consultant.mrose', '/fiches/export/666'],
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessExport
//     */
//    public function testAccessExport($expectedStatus, $user, $url, $options = []) {
//        $this->markTestIncomplete();
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//    }

//    public function dataAccessGenereExtraitRegistre() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/fiches/genereExtraitRegistre/1'],
//            [302, 'Administrateur.ibleu', '/fiches/genereExtraitRegistre/1'],
//            [302, 'DPO.nroux', '/fiches/genereExtraitRegistre/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/genereExtraitRegistre/2'],
//            [403, 'DPO.hvermeil', '/fiches/genereExtraitRegistre/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/fiches/genereExtraitRegistre/666'],
//            [404, 'Administrateur.ibleu', '/fiches/genereExtraitRegistre/666'],
//            [404, 'DPO.nroux', '/fiches/genereExtraitRegistre/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/fiches/genereExtraitRegistre/1'],
//            [403, 'Valideur.cnoir', '/fiches/genereExtraitRegistre/1'],
//            [403, 'Consultant.mrose', '/fiches/genereExtraitRegistre/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/genereExtraitRegistre/666'],
//            [403, 'Valideur.cnoir', '/fiches/genereExtraitRegistre/666'],
//            [403, 'Consultant.mrose', '/fiches/genereExtraitRegistre/666'],
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessGenereExtraitRegistre
//     */
//    public function testAccessGenereExtraitRegistre($expectedStatus, $user, $url, $options = []) {
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//        $this->markTestIncomplete();
//    }

//    public function dataAccessGenereTraitement() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/fiches/genereTraitement/1'],
//            [302, 'Administrateur.ibleu', '/fiches/genereTraitement/1'],
//            [302, 'DPO.nroux', '/fiches/genereTraitement/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/genereTraitement/2'],
//            [403, 'DPO.hvermeil', '/fiches/genereTraitement/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/fiches/genereTraitement/666'],
//            [404, 'Administrateur.ibleu', '/fiches/genereTraitement/666'],
//            [404, 'DPO.nroux', '/fiches/genereTraitement/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/fiches/genereTraitement/1'],
//            [403, 'Valideur.cnoir', '/fiches/genereTraitement/1'],
//            [403, 'Consultant.mrose', '/fiches/genereTraitement/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/genereTraitement/666'],
//            [403, 'Valideur.cnoir', '/fiches/genereTraitement/666'],
//            [403, 'Consultant.mrose', '/fiches/genereTraitement/666'],
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessGenereTraitement
//     */
//    public function testAccessGenereTraitement($expectedStatus, $user, $url, $options = []) {
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//        $this->markTestIncomplete();
//    }

//    public function dataAccessGenereTraitementNonVerrouiller() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/fiches/genereTraitementNonVerrouiller/1'],
//            [302, 'Administrateur.ibleu', '/fiches/genereTraitementNonVerrouiller/1'],
//            [302, 'DPO.nroux', '/fiches/genereTraitementNonVerrouiller/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/genereTraitementNonVerrouiller/2'],
//            [403, 'DPO.hvermeil', '/fiches/genereTraitementNonVerrouiller/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/fiches/genereTraitementNonVerrouiller/666'],
//            [404, 'Administrateur.ibleu', '/fiches/genereTraitementNonVerrouiller/666'],
//            [404, 'DPO.nroux', '/fiches/genereTraitementNonVerrouiller/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/fiches/genereTraitementNonVerrouiller/1'],
//            [403, 'Valideur.cnoir', '/fiches/genereTraitementNonVerrouiller/1'],
//            [403, 'Consultant.mrose', '/fiches/genereTraitementNonVerrouiller/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/genereTraitementNonVerrouiller/666'],
//            [403, 'Valideur.cnoir', '/fiches/genereTraitementNonVerrouiller/666'],
//            [403, 'Consultant.mrose', '/fiches/genereTraitementNonVerrouiller/666'],
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessGenereTraitementNonVerrouiller
//     */
//    public function testAccessGenereTraitementNonVerrouiller($expectedStatus, $user, $url, $options = []) {
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//        $this->markTestIncomplete();
//    }

    public function dataAccessIndex() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/fiches/index'],
            [302, 'Administrateur.ibleu', '/fiches/index'],
            [302, 'DPO.nroux', '/fiches/index'],
            [302, 'Rédacteur.rjaune', '/fiches/index'],
            [302, 'Valideur.cnoir', '/fiches/index'],
            [302, 'Consultant.mrose', '/fiches/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function dataAccessSaveFileTmp() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/fiches/saveFileTmp/1'],
//            [302, 'Administrateur.ibleu', '/fiches/saveFileTmp/1'],
//            [302, 'DPO.nroux', '/fiches/saveFileTmp/1'],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/fiches/saveFileTmp/2'],
//            [403, 'DPO.hvermeil', '/fiches/saveFileTmp/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/fiches/saveFileTmp/666'],
//            [404, 'Administrateur.ibleu', '/fiches/saveFileTmp/666'],
//            [404, 'DPO.nroux', '/fiches/saveFileTmp/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/fiches/saveFileTmp/1'],
//            [403, 'Valideur.cnoir', '/fiches/saveFileTmp/1'],
//            [403, 'Consultant.mrose', '/fiches/saveFileTmp/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/fiches/saveFileTmp/666'],
//            [403, 'Valideur.cnoir', '/fiches/saveFileTmp/666'],
//            [403, 'Consultant.mrose', '/fiches/saveFileTmp/666'],
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessSaveFileTmp
//     */
//    public function testAccessSaveFileTmp($expectedStatus, $user, $url, $options = []) {
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//        $this->markTestIncomplete();
//    }

    public function dataAccessShow() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/fiches/show/1'],
            [200, 'DPO.nroux', '/fiches/show/1'],
            [200, 'Rédacteur.rjaune', '/fiches/show/1'],
            [200, 'Valideur.cnoir', '/fiches/show/1'],
            [200, 'Consultant.mrose', '/fiches/show/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/fiches/show/1'],
            [403, 'DPO.hvermeil', '/fiches/show/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/fiches/show/666'],
            [404, 'DPO.nroux', '/fiches/show/666'],
            [404, 'Rédacteur.rjaune', '/fiches/show/666'],
            [404, 'Valideur.cnoir', '/fiches/show/666'],
            [404, 'Consultant.mrose', '/fiches/show/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/fiches/show/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/fiches/show/666'],
        ];
    }

    /**
     * @dataProvider dataAccessShow
     */
    public function testAccessShow($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
