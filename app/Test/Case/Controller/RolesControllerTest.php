<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );
App::uses('LdapManagerAppModel', 'LdapManager.Model');

/**
 * Tests d'intégration de la classe AdminsController.
 *
 * ./cake_utils.sh tests app Controller/RolesController
 *
 * @package app.Test.Case.Controller
 */
class RolesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Authentification',
        'app.ConnecteurLdap',
        'app.Droit',
        'app.Fiche',
        'app.LdapmGroup',
        'app.LdapmModelsGroup',
        'app.ListeDroit',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Roles');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/roles/add'],
            [200, 'Administrateur.ibleu', '/roles/add'],
            [200, 'DPO.nroux', '/roles/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/roles/add'],
            [403, 'Valideur.cnoir', '/roles/add'],
            [403, 'Consultant.mrose', '/roles/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/roles/delete/31'],
            [302, 'Administrateur.ibleu', '/roles/delete/31'],
            // Un administrateur mono-collectivité ne peut pas supprimer un rôle d'une autre collectivité
            [403, 'Administrateur.findigo', '/roles/delete/31'],
            [302, 'DPO.nroux', '/roles/delete/31'],
            // Un DPO mono-collectivité ne peut pas supprimer un rôle d'une autre collectivité
            [403, 'DPO.hvermeil', '/roles/delete/31'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/roles/delete/666'],
            [404, 'Administrateur.ibleu', '/roles/delete/666'],
            [404, 'DPO.nroux', '/roles/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/roles/delete/31'],
            [403, 'Valideur.cnoir', '/roles/delete/31'],
            [403, 'Consultant.mrose', '/roles/delete/31'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/roles/delete/666'],
            [403, 'Valideur.cnoir', '/roles/delete/666'],
            [403, 'Consultant.mrose', '/roles/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/roles/edit/1'],
            [200, 'Administrateur.ibleu', '/roles/edit/1'],
            // Un administrateur mono-collectivité ne peut pas modifier un rôle d'une autre collectivité
            [403, 'Administrateur.findigo', '/roles/edit/1'],
            [200, 'DPO.nroux', '/roles/edit/1'],
            // Un DPO mono-collectivité ne peut pas modifier un rôle d'une autre collectivité
            [403, 'DPO.hvermeil', '/roles/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/roles/edit/666'],
            [404, 'Administrateur.ibleu', '/roles/edit/666'],
            [404, 'DPO.nroux', '/roles/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/roles/edit/1'],
            [403, 'Valideur.cnoir', '/roles/edit/1'],
            [403, 'Consultant.mrose', '/roles/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/roles/edit/666'],
            [403, 'Valideur.cnoir', '/roles/edit/666'],
            [403, 'Consultant.mrose', '/roles/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/roles/index'],
            [200, 'Administrateur.ibleu', '/roles/index'],
            [200, 'DPO.nroux', '/roles/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/roles/index'],
            [403, 'Valideur.cnoir', '/roles/index'],
            [403, 'Consultant.mrose', '/roles/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessReattributionRoles() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/roles/reattributionRoles/1'],
            [302, 'Administrateur.ibleu', '/roles/reattributionRoles/1'],
            // Un administrateur d'une autre entité ne peut accéder
            [403, 'Administrateur.findigo', '/roles/reattributionRoles/1'],
            [302, 'DPO.nroux', '/roles/reattributionRoles/1'],
            // Un DPO d'une autre entité ne peut accéder
            [403, 'DPO.hvermeil', '/roles/reattributionRoles/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/roles/reattributionRoles/666'],
            [404, 'Administrateur.ibleu', '/roles/reattributionRoles/666'],
            [404, 'DPO.nroux', '/roles/reattributionRoles/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/roles/reattributionRoles/1'],
            [403, 'Valideur.cnoir', '/roles/reattributionRoles/1'],
            [403, 'Consultant.mrose', '/roles/reattributionRoles/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/roles/reattributionRoles/666'],
            [403, 'Valideur.cnoir', '/roles/reattributionRoles/666'],
            [403, 'Consultant.mrose', '/roles/reattributionRoles/666'],
        ];
    }

    /**
     * @dataProvider dataAccessReattributionRoles
     */
    public function testAccessReattributionRoles($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
