<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe RegistresController.
 *
 * ./cake_utils.sh tests app Controller/RegistresController
 *
 * @package app.Test.Case.Controller
 */
class RegistresControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Admin',
        'app.EtatFiche',
        'app.Fiche',
        'app.Fichier',
        'app.Formulaire',
        'app.FormulaireOrganisation',
        'app.ListeDroit',
        'app.Modification',
        'app.Notification',
        'app.Norme',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Responsable',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.Soustraitant',
        'app.TraitementRegistre',
        'app.Typage',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Registres');
    }

    public function dataAccessAdd() {
        $dataExists = ['Registre' => ['idfiche' => 1, 'numero' => '', 'norme' => 'Test']];
        $dataNotExists = ['Registre' => ['idfiche' => 666, 'numero' => '', 'norme' => 'Test']];
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'DPO.nroux', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'DPO.hvermeil', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            // 1.2. Enregistrement inexistant
            [404, 'DPO.nroux', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Administrateur.ibleu', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Rédacteur.rjaune', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Valideur.cnoir', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Consultant.mrose', '/registres/add', ['method' => 'POST', 'data' => $dataExists]],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Administrateur.ibleu', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Rédacteur.rjaune', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Valideur.cnoir', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Consultant.mrose', '/registres/add', ['method' => 'POST', 'data' => $dataNotExists]],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        $dataExists = ['Registre' => ['idEditRegistre' => 1, 'motif' => '...']];
        $dataNotExists = ['Registre' => ['idEditRegistre' => 666, 'motif' => '...']];
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'DPO.nroux', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'DPO.hvermeil', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            // 1.2. Enregistrement inexistant
            [404, 'DPO.nroux', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Administrateur.ibleu', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Rédacteur.rjaune', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Valideur.cnoir', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Consultant.mrose', '/registres/edit', ['method' => 'POST', 'data' => $dataExists]],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Administrateur.ibleu', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Rédacteur.rjaune', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Valideur.cnoir', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Consultant.mrose', '/registres/edit', ['method' => 'POST', 'data' => $dataNotExists]],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function dataAccessImprimer() {
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Superadministrateur.superadmin', '/registres/imprimer/'.json_encode([1])],
//            /*[302, 'Administrateur.ibleu', '/registres/imprimer/'.json_encode([1])],
//            [302, 'DPO.nroux', '/registres/imprimer/'.json_encode([1])],
//            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
//            [403, 'Administrateur.findigo', '/registres/imprimer/2'],
//            [403, 'DPO.hvermeil', '/registres/imprimer/2'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Superadministrateur.superadmin', '/registres/imprimer/666'],
//            [404, 'Administrateur.ibleu', '/registres/imprimer/666'],
//            [404, 'DPO.nroux', '/registres/imprimer/666'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Rédacteur.rjaune', '/registres/imprimer/'.json_encode([1])],
//            [403, 'Valideur.cnoir', '/registres/imprimer/'.json_encode([1])],
//            [403, 'Consultant.mrose', '/registres/imprimer/'.json_encode([1])],
//            // 2.2. Enregistrement inexistant
//            [403, 'Rédacteur.rjaune', '/registres/imprimer/666'],
//            [403, 'Valideur.cnoir', '/registres/imprimer/666'],
//            [403, 'Consultant.mrose', '/registres/imprimer/666'],*/
//        ];
//    }
//
//    /**
//     * @dataProvider dataAccessImprimer
//     */
//    public function testAccessImprimer($expectedStatus, $user, $url, $options = []) {
//        $this->markTestIncomplete('Il manque des entrées dans la table traitement_registres et des modèles');
//        //$this->assertActionAccess($expectedStatus, $user, $url, $options);
//    }

    public function dataAccessIndex() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/registres/index'],
            [200, 'Administrateur.ibleu', '/registres/index'],
            [200, 'DPO.nroux', '/registres/index'],
            [200, 'Rédacteur.rjaune', '/registres/index'],
            [200, 'Valideur.cnoir', '/registres/index'],
            [200, 'Consultant.mrose', '/registres/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRegistreActivite() {
        return [
            // Utilisateurs pouvant accéder à la fonctionnalité
            [302, 'Administrateur.ibleu', '/registres/registreActivite'],
            [302, 'DPO.nroux', '/registres/registreActivite'],
            [302, 'Rédacteur.rjaune', '/registres/registreActivite'],
            [302, 'Valideur.cnoir', '/registres/registreActivite'],
            [302, 'Consultant.mrose', '/registres/registreActivite'],
            // Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/registres/registreActivite'],
        ];
    }

    /**
     * @dataProvider dataAccessRegistreActivite
     */
    public function testAccessRegistreActivite($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRegistreSoustraitance() {
        return [
            // Utilisateurs pouvant accéder à la fonctionnalité
            [302, 'Administrateur.ibleu', '/registres/registreSoustraitance'],
            [302, 'DPO.nroux', '/registres/registreSoustraitance'],
            [302, 'Rédacteur.rjaune', '/registres/registreSoustraitance'],
            [302, 'Valideur.cnoir', '/registres/registreSoustraitance'],
            [302, 'Consultant.mrose', '/registres/registreSoustraitance'],
            // Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/registres/registreSoustraitance'],
        ];
    }

    /**
     * @dataProvider dataAccessRegistreSoustraitance
     */
    public function testAccessRegistreSoustraitance($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
