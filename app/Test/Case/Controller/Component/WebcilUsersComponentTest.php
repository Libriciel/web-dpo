<?php
App::uses('AppController', 'Controller');
App::uses('ControllerTestCaseAccessTrait', 'Test/Trait/Controller');

/**
 * Tests d'intégration de la classe AdminsController.
 *
 * ./cake_utils.sh tests app Controller/Component/WebcilUsersComponent
 *
 * @package app.Test.Case.Controller
 */
class WebcilUsersComponentTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    // @todo: dans un trait plus général
    protected static $_datetimeRegexp = '/^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/';
    protected static $_logoRegexp = '/^[0-9]+\/[0-9]{10}$/';

    public $fixtures = [
        'app.Admin',
        'app.ListeDroit',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.OrganisationUserService',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.User'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->controller = $this->generate('Users');
    }

    public function tearDown()
    {
        foreach (array_keys($this->controller->Session->read()) as $name) {
            $this->controller->Session->delete($name);
        }
        parent::tearDown();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::organisations avec un rôle Superadministrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsSuperadministrateurList() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->organisations('list');
        $expected = [
            5 => 'Eurométropole de Strasbourg',
            1 => 'Libriciel SCOP',
            3 => 'Métropole Européenne de Lille',
            4 => 'Metz Métropole',
            2 => 'Montpellier Méditerranée Métropole',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurFirst() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->organisations('first');
        $expected = [
            'Organisation' => [
                'id' => 5,
                'raisonsociale' => 'Eurométropole de Strasbourg',
                'telephone' => '0444444444',
                'fax' => NULL,
                'adresse' => '1 Parc de l\'Étoile, 67076 Strasbourg',
                'email' => 'contact-es_recette-dp@libriciel.coop',
                'sigle' => NULL,
                'siret' => '59000000000000',
                'ape' => '8411Z',
                'logo' => NULL,
                'nomresponsable' => 'GRENAT',
                'prenomresponsable' => 'Sylvie',
                'emailresponsable' => 'sgrenat_recette-dp@libriciel.coop',
                'telephoneresponsable' => '0644444444',
                'fonctionresponsable' => 'Maire',
                'dpo' => NULL,
                'numerodpo' => NULL,
                'force' => 4,
                'responsable_id' => 5,
                'created' => $result['Organisation']['created'],
                'modified' => $result['Organisation']['modified'],
                'prefixenumero' => 'DPO-',
                'numeroregistre' => 0,
                'civiliteresponsable' => 'Mme.',
                'rgpd' => false,
                'usemodelepresentation' => true,
                'usefieldsredacteur' => false,
                'emaildpo' => 'dpo@ES.test'
            ]
        ];
        $this->assertRegExp( static::$_datetimeRegexp, $result['Organisation']['created']);
        $this->assertRegExp( static::$_datetimeRegexp, $result['Organisation']['modified']);
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurQueryFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->organisations('query', ['fields' => ['id', 'raisonsociale']]);
        $expected = [
            'fields' => ['id','raisonsociale'],
            'conditions' => [],
            'order' => ['Organisation.raisonsociale ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurListDroitsCreeUtilisateur() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->organisations('list',['droits' => ListeDroit::CREER_UTILISATEUR]);
        $expected = [
            5 => 'Eurométropole de Strasbourg',
            1 => 'Libriciel SCOP',
            3 => 'Métropole Européenne de Lille',
            4 => 'Metz Métropole',
            2 => 'Montpellier Méditerranée Métropole',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function dataOrganisationsSuperadministrateurListRestrictTrue() {
        return [
            [null, []],
            [1, [1 => 'Libriciel SCOP']],
        ];
    }

    /**
     * @dataProvider dataOrganisationsSuperadministrateurListRestrictTrue
     */
    public function testOrganisationsSuperadministrateurListRestrictTrue($organisation_id, $expected) {
        $this->_setupUserSession('Superadministrateur.superadmin');
        if ($organisation_id !== null) {
            $this->controller->Session->write(['Organisation.id' => $organisation_id]);
        }
        $result = $this->controller->WebcilUsers->organisations( 'list', [ 'restrict' => true ] );
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function dataOrganisationsSuperadministrateurListRestrictTrueDroitsCreeUtilisateur() {
        return [
            [null, []],
            [1, [1 => 'Libriciel SCOP']],
        ];
    }

    /**
     * @dataProvider dataOrganisationsSuperadministrateurListRestrictTrueDroitsCreeUtilisateur
     */
    public function testOrganisationsSuperadministrateurListRestrictTrueDroitsCreeUtilisateur($organisation_id, $expected) {
        $this->_setupUserSession('Superadministrateur.superadmin');
        if ($organisation_id !== null) {
            $this->controller->Session->write(['Organisation.id' => $organisation_id]);
        }
        $result = $this->controller->WebcilUsers->organisations( 'list', [ 'restrict' => true, 'droits' => ListeDroit::CREER_UTILISATEUR ] );
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::organisations avec un rôle Administrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsAdministrateurList() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations('list');
        $expected = [
            1 => 'Libriciel SCOP',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurFirst() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations('first');
        $expected = [
            'Organisation' => [
                'id' => 1,
                'raisonsociale' => 'Libriciel SCOP',
                'telephone' => '0400000000',
                'fax' => NULL,
                'adresse' => '836 Rue du Mas de Verchant, 34000 Montpellier',
                'email' => 'contact-ls_recette-dp@libriciel.coop',
                'sigle' => NULL,
                'siret' => '18000000000000',
                'ape' => '6202B',
                'logo' => $result['Organisation']['logo'],
                'nomresponsable' => 'VERT',
                'prenomresponsable' => 'Céline',
                'emailresponsable' => 'cvert_recette-dp@libriciel.coop',
                'telephoneresponsable' => '0600000000',
                'fonctionresponsable' => 'Président-directeur Général',
                'dpo' => 11,
                'numerodpo' => '001',
                'force' => 4,
                'responsable_id' => 1,
                'created' => $result['Organisation']['created'],
                'modified' => $result['Organisation']['modified'],
                'prefixenumero' => 'DPO-',
                'numeroregistre' => 1,
                'civiliteresponsable' => 'Mme.',
                'rgpd' => true,
                'usemodelepresentation' => true,
                'usefieldsredacteur' => false,
                'emaildpo' => 'dpo@libriciel.test'
            ]
        ];
        $this->assertRegExp( static::$_logoRegexp, $result['Organisation']['logo']);
        $this->assertRegExp( static::$_datetimeRegexp, $result['Organisation']['created']);
        $this->assertRegExp( static::$_datetimeRegexp, $result['Organisation']['modified']);
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurQueryFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations('query', ['fields' => ['id', 'raisonsociale']]);
        $expected = [
            'fields' => ['id','raisonsociale'],
            'conditions' => [
                'EXISTS( SELECT "organisations_users"."id" AS "organisations_users__id" FROM "public"."organisations_users" AS "organisations_users"   WHERE "organisations_users"."organisation_id" = "Organisation"."id" AND "organisations_users"."user_id" = 3 )'
            ],
            'order' => ['Organisation.raisonsociale ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurListDroitsCreeUtilisateur() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations('list', ['droits' => ListeDroit::CREER_UTILISATEUR]);
        $expected = [
            1 => 'Libriciel SCOP',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurListRestrictTrue() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations( 'list', [ 'restrict' => true ] );
        $expected = [
            1 => 'Libriciel SCOP',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurListRestrictTrueDroitsCreeUtilisateur() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->organisations( 'list', [ 'restrict' => true, 'droits' => ListeDroit::CREER_UTILISATEUR ] );
        $expected = [
            1 => 'Libriciel SCOP',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::roles avec un rôle Superadministrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsSuperadministrateurRolesList() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->roles('list');
        $expected = [
            'Administrateur' => 'Administrateur',
            'Consultant' => 'Consultant',
            'DPO' => 'DPO',
            'Rédacteur' => 'Rédacteur',
            'Valideur' => 'Valideur',
            'Lecteur' => 'Lecteur'
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurRolesListFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->roles('list', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                4 => 'Administrateur',
                3 => 'Consultant',
                5 => 'DPO',
                31 => 'Lecteur',
                1 => 'Rédacteur',
                2 => 'Valideur',
            ],
            2 => [
                9 => 'Administrateur',
                8 => 'Consultant',
                10 => 'DPO',
                32 => 'Lecteur',
                6 => 'Rédacteur',
                7 => 'Valideur',
            ],
            3 => [
                14 => 'Administrateur',
                13 => 'Consultant',
                15 => 'DPO',
                33 => 'Lecteur',
                11 => 'Rédacteur',
                12 => 'Valideur',
            ],
            4 => [
                19 => 'Administrateur',
                18 => 'Consultant',
                20 => 'DPO',
                16 => 'Rédacteur',
                17 => 'Valideur',
            ],
            5 => [
                24 => 'Administrateur',
                23 => 'Consultant',
                25 => 'DPO',
                21 => 'Rédacteur',
                22 => 'Valideur',
            ],
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurRolesQueryFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->roles('query', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            'fields' => ['id', 'libelle', 'organisation_id'],
            'conditions' => [],
            'joins' => [
                [
                    'table' => '"organisations"',
                    'alias' => 'Organisation',
                    'type' => 'INNER',
                    'conditions' => '"Role"."organisation_id" = "Organisation"."id"'
                ]
            ],
            'order' => ['Role.libelle ASC', 'Role.id ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

//    public function dataOrganisationsSuperadministrateurRolesListFieldsRestrictTrue() {
//        return [
//            [null, []],
//            [
//                1,
//                [
//                    1 => [
//                        4 => 'Administrateur',
//                        3 => 'Consultant',
//                        5 => 'DPO',
//                        31 => 'Lecteur',
//                        1 => 'Rédacteur',
//                        2 => 'Valideur',
//                    ]
//                ]
//            ],
//        ];
//    }
//
//    /**
//     * @dataProvider dataOrganisationsSuperadministrateurRolesListFieldsRestrictTrue
//     */
//    public function testOrganisationsSuperadministrateurRolesListFieldsRestrictTrue($organisation_id, $expected) {
//        $this->_setupUserSession('Superadministrateur.superadmin');
//        if ($organisation_id !== null) {
//            $this->controller->Session->write(['Organisation.id' => $organisation_id]);
//        }
//        $result = $this->controller->WebcilUsers->roles('list', ['restrict' => true, 'fields' => ['id', 'libelle', 'organisation_id']]);
//        $this->assertEquals( $expected, $result, var_export( $result, true ) );
//
//        $this->markTestIncomplete('Il manque des données pour les tests de multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::roles avec un rôle Administrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsAdministrateurRolesList() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->roles('list');
        $expected = [
            'Administrateur' => 'Administrateur',
            'Consultant' => 'Consultant',
            'DPO' => 'DPO',
            'Rédacteur' => 'Rédacteur',
            'Valideur' => 'Valideur',
            'Lecteur' => 'Lecteur',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurRolesListFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->roles('list', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                4 => 'Administrateur',
                3 => 'Consultant',
                5 => 'DPO',
                31 => 'Lecteur',
                1 => 'Rédacteur',
                2 => 'Valideur'
            ],
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurRolesQueryFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->roles('query', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            'fields' => ['id', 'libelle', 'organisation_id'],
            'conditions' => [
                'EXISTS( SELECT "organisations_users"."id" AS "organisations_users__id" FROM "public"."organisations_users" AS "organisations_users"   WHERE "organisations_users"."organisation_id" = "Role"."organisation_id" AND "organisations_users"."user_id" = 3 )',
            ],
            'joins' => [
                [
                    'table' => '"organisations"',
                    'alias' => 'Organisation',
                    'type' => 'INNER',
                    'conditions' => '"Role"."organisation_id" = "Organisation"."id"'
                ]
            ],
            'order' => ['Role.libelle ASC', 'Role.id ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurRolesListFieldsRestrictTrue() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->roles('list', ['restrict' => true, 'fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                4 => 'Administrateur',
                3 => 'Consultant',
                5 => 'DPO',
                31 => 'Lecteur',
                1 => 'Rédacteur',
                2 => 'Valideur',
            ]
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::services avec un rôle Superadministrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsSuperadministrateurServicesList() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->services('list');
        $expected = [
            'Direction Administration, Finances et Moyens Généraux' => 'Direction Administration, Finances et Moyens Généraux',
            'Direction Commerciale et Marketing' => 'Direction Commerciale et Marketing',
            'Direction du Développement' => 'Direction du Développement',
            'Direction Service Clients' => 'Direction Service Clients',
            'Service des transports' => 'Service des transports',
            'Service de voirie' => 'Service de voirie',
            'Service d\'urbanisme' => 'Service d\'urbanisme',
            'Service du traitement de l\'eau' => 'Service du traitement de l\'eau',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurServicesListFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->services('list', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                1 => 'Direction Administration, Finances et Moyens Généraux',
                3 => 'Direction Commerciale et Marketing',
                2 => 'Direction du Développement',
                4 => 'Direction Service Clients',
            ],
            2 => [
                7 => 'Service des transports',
                6 => 'Service de voirie',
                5 => 'Service d\'urbanisme',
                8 => 'Service du traitement de l\'eau',
            ],
            3 => [
                11 => 'Service des transports',
                10 => 'Service de voirie',
                9 => 'Service d\'urbanisme',
                12 => 'Service du traitement de l\'eau',
            ]
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurServicesQueryFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->services('query', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            'fields' => ['id', 'libelle', 'organisation_id'],
            'conditions' => [],
            'joins' => [
                [
                    'table' => '"organisations"',
                    'alias' => 'Organisation',
                    'type' => 'INNER',
                    'conditions' => '"Service"."organisation_id" = "Organisation"."id"'
                ]
            ],
            'order' => ['Service.libelle ASC', 'Service.id ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function dataOrganisationsSuperadministrateurServicesListFieldsRestrictTrue() {
        return [
            [null, []],
            [
                1,
                [
                    1 => [
                        1 => 'Direction Administration, Finances et Moyens Généraux',
                        3 => 'Direction Commerciale et Marketing',
                        2 => 'Direction du Développement',
                        4 => 'Direction Service Clients',
                    ]
                ]
            ],
        ];
    }

    /**
     * @dataProvider dataOrganisationsSuperadministrateurServicesListFieldsRestrictTrue
     */
    public function testOrganisationsSuperadministrateurServicesListFieldsRestrictTrue($organisation_id, $expected) {
        $this->_setupUserSession('Superadministrateur.superadmin');
        if ($organisation_id !== null) {
            $this->controller->Session->write(['Organisation.id' => $organisation_id]);
        }
        $result = $this->controller->WebcilUsers->services('list', ['restrict' => true, 'fields' => ['id', 'libelle', 'organisation_id']]);
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::services avec un rôle Administrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsAdministrateurServicesList() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->services('list');
        $expected = [
            'Direction Administration, Finances et Moyens Généraux' => 'Direction Administration, Finances et Moyens Généraux',
            'Direction Commerciale et Marketing' => 'Direction Commerciale et Marketing',
            'Direction du Développement' => 'Direction du Développement',
            'Direction Service Clients' => 'Direction Service Clients',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurServicesListFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->services('list', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                1 => 'Direction Administration, Finances et Moyens Généraux',
                3 => 'Direction Commerciale et Marketing',
                2 => 'Direction du Développement',
                4 => 'Direction Service Clients',
            ]
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurServicesQueryFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->services('query', ['fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            'fields' => ['id', 'libelle', 'organisation_id'],
            'conditions' => [
                'EXISTS( SELECT "organisations_users"."id" AS "organisations_users__id" FROM "public"."organisations_users" AS "organisations_users"   WHERE "organisations_users"."organisation_id" = "Service"."organisation_id" AND "organisations_users"."user_id" = 3 )',
            ],
            'joins' => [
                [
                    'table' => '"organisations"',
                    'alias' => 'Organisation',
                    'type' => 'INNER',
                    'conditions' => '"Service"."organisation_id" = "Organisation"."id"'
                ]
            ],
            'order' => ['Service.libelle ASC', 'Service.id ASC']
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurServicesListFieldsRestrictTrue() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->services('list', ['restrict' => true, 'fields' => ['id', 'libelle', 'organisation_id']]);
        $expected = [
            1 => [
                1 => 'Direction Administration, Finances et Moyens Généraux',
                3 => 'Direction Commerciale et Marketing',
                2 => 'Direction du Développement',
                4 => 'Direction Service Clients',
            ]
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::users avec un rôle Superadministrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsSuperadministrateurUsersList() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->users('list');
        $expected = [
            13 => 'M. André MAUVE',
            9 => 'M. Christian NOIR',
            4 => 'Mme. Françoise INDIGO',
            15 => 'Mme. Hélène VERMEIL',
            3 => 'Mme. Inès BLEU',
            5 => 'M. Jean GRIS',
            6 => 'M. Louis SEPIA',
            7 => 'Mme. Madeleine RUBIS',
            18 => 'Mme. Marguerite LILAS',
            10 => 'Mme. Michèle ROSE',
            14 => 'Mme. Monique AZUR',
            11 => 'Mme. Nicole ROUX',
            17 => 'M. Patrick LAVANDE',
            12 => 'M. Pierre MAGENTA',
            8 => 'Mme. Rébecca JAUNE',
            16 => 'Mme. Stéphanie ORANGE',
            19 => 'Mme. Valérie PAILLE',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurUsersListFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->users('list', ['fields' => ['id', 'username']]);
        $expected = [
            13 => 'amauve',
            9 => 'cnoir',
            4 => 'findigo',
            15 => 'hvermeil',
            3 => 'ibleu',
            5 => 'jgris',
            6 => 'lsepia',
            7 => 'mrubis',
            18 => 'mlilas',
            10 => 'mrose',
            14 => 'mazur',
            11 => 'nroux',
            17 => 'plavande',
            12 => 'pmagenta',
            8 => 'rjaune',
            16 => 'sorange',
            19 => 'vpaille',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsSuperadministrateurUsersQueryFields() {
        $this->_setupUserSession('Superadministrateur.superadmin');
        $result = $this->controller->WebcilUsers->users('query', ['fields' => ['id', 'username']]);
        $expected = [
            'fields' => ['id', 'username'],
            'conditions' => [
                [
                    'User.id NOT IN ( SELECT "admins"."user_id" AS "admins__user_id" FROM "public"."admins" AS "admins"   WHERE "admins"."user_id" = "User"."id" )',
                ]
            ],
            'order' => ['User.nom_complet_court ASC'],
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

//    public function dataOrganisationsSuperadministrateurUsersListFieldsRestrictTrue() {
//        return [
//            [null, []],
//            [
//                1,
//                [
//                    9 => 'cnoir',
//                    3 => 'ibleu',
//                    10 => 'mrose',
//                    11 => 'nroux',
//                    8 => 'rjaune',
//                ]
//            ],
//        ];
//    }
//
//    /**
//     * @dataProvider dataOrganisationsSuperadministrateurUsersListFieldsRestrictTrue
//     */
//    public function testOrganisationsSuperadministrateurUsersListFieldsRestrictTrue($organisation_id, $expected) {
//        $this->_setupUserSession('Superadministrateur.superadmin');
//        if ($organisation_id !== null) {
//            $this->controller->Session->write(['Organisation.id' => $organisation_id]);
//        }
//        $result = $this->controller->WebcilUsers->users('list', ['restrict' => true, 'fields' => ['id', 'username']]);
//        $this->assertEquals( $expected, $result, var_export( $result, true ) );
//
//        $this->markTestIncomplete('Il manque des données pour les tests de multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests de la méthode WebcilUsers::users avec un rôle Administrateur
    //------------------------------------------------------------------------------------------------------------------

    public function testOrganisationsAdministrateurUsersList() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->users('list');
        $expected = [
            9 => 'M. Christian NOIR',
            3 => 'Mme. Inès BLEU',
            10 => 'Mme. Michèle ROSE',
            11 => 'Mme. Nicole ROUX',
            8 => 'Mme. Rébecca JAUNE',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurUsersListFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->users('list', ['fields' => ['id', 'username']]);
        $expected = [
            9 => 'cnoir',
            3 => 'ibleu',
            10 => 'mrose',
            11 => 'nroux',
            8 => 'rjaune',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurUsersQueryFields() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->users('query', ['fields' => ['id', 'username']]);
        $expected = [
            'fields' => ['id', 'username'],
            'conditions' => [
                [
                    'User.id NOT IN ( SELECT "admins"."user_id" AS "admins__user_id" FROM "public"."admins" AS "admins"   WHERE "admins"."user_id" = "User"."id" )',
                ],
                'User.id IN ( SELECT "organisations_users"."user_id" AS "organisations_users__user_id" FROM "public"."organisations_users" AS "organisations_users"   WHERE "organisations_users"."organisation_id" IN ( SELECT "organisations"."organisation_id" AS "organisations__organisation_id" FROM "public"."organisations_users" AS "organisations"   WHERE "organisations"."user_id" = 3  GROUP BY "organisations"."organisation_id" ) AND "organisations_users"."user_id" = "User"."id" )',
            ],
            'order' => ['User.nom_complet_court ASC'],
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    public function testOrganisationsAdministrateurUsersListFieldsRestrictTrue() {
        $this->_setupUserSession('Administrateur.ibleu');
        $result = $this->controller->WebcilUsers->users('list', ['restrict' => true, 'fields' => ['id', 'username']]);
        $expected = [
            9 => 'cnoir',
            3 => 'ibleu',
            10 => 'mrose',
            11 => 'nroux',
            8 => 'rjaune',
        ];
        $this->assertEquals( $expected, $result, var_export( $result, true ) );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tests avec un rôle Administrateur en mode multi-entités
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Test de la méthode WebcilUsersComponent::organisations() pour un
     * Administrateur ayant accès à plusieurs entité.
     */
//    public function testOrganisationsAdministrateurMultiOrganisations() {
//        $this->markTestIncomplete('Il manque des données et des tests pour les administrateurs multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }

    /**
     * Test de la méthode WebcilUsersComponent::roles() pour un
     * Administrateur ayant accès à plusieurs entité.
     */
//    public function testRolesAdministrateurMultiOrganisations() {
//        $this->markTestIncomplete('Il manque des données et des tests pour les administrateurs multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }

    /**
     * Test de la méthode WebcilUsersComponent::services() pour un
     * Administrateur ayant accès à plusieurs entité.
     */
//    public function testServicesAdministrateurMultiOrganisations() {
//        $this->markTestIncomplete('Il manque des données et des tests pour les administrateurs multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }

    /**
     * Test de la méthode WebcilUsersComponent::users() pour un
     * Administrateur ayant accès à plusieurs entité.
     */
//    public function testUsersAdministrateurMultiOrganisations() {
//        $this->markTestIncomplete('Il manque des données et des tests pour les administrateurs multi-entités (cf. WebcilUsersComponentTest.php.todo');
//    }
}
