<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe AdminsController.
 *
 * ./cake_utils.sh tests app Controller/ServicesController
 *
 * @package app.Test.Case.Controller
 */
class ServicesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserService',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Services');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/services/add'],
            [200, 'Administrateur.ibleu', '/services/add'],
            [200, 'DPO.nroux', '/services/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/services/add'],
            [403, 'Valideur.cnoir', '/services/add'],
            [403, 'Consultant.mrose', '/services/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete()
    {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/services/delete/9'],
            [302, 'Administrateur.jgris', '/services/delete/9'],
            // Un Administrateur ne peut pas supprimer un service lorsque des utilisateurs y sont liés
            [403, 'Administrateur.ibleu', '/services/delete/1'],
            // Un Administrateur ne peut pas supprimer un enregistrement d'une autre entité
            [403, 'Administrateur.findigo', '/services/delete/9'],
            //@todo: ajouter un DPO sur l'entité 3
            //[302, 'DPO.', '/services/delete/9'],
            // Un DPO ne peut pas supprimer un enregistrement d'une autre entité
            [403, 'DPO.nroux', '/services/delete/9'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/services/delete/666'],
            [404, 'Administrateur.ibleu', '/services/delete/666'],
            [404, 'DPO.nroux', '/services/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/services/delete/9'],
            [403, 'Valideur.cnoir', '/services/delete/9'],
            [403, 'Consultant.mrose', '/services/delete/9'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/services/delete/666'],
            [403, 'Valideur.cnoir', '/services/delete/666'],
            [403, 'Consultant.mrose', '/services/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit()
    {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/services/edit/1'],
            [200, 'Administrateur.ibleu', '/services/edit/1'],
            // Un Administrateur ne peut pas modifier un enregistrement d'une autre entité
            [403, 'Administrateur.findigo', '/services/edit/1'],
            [200, 'DPO.nroux', '/services/edit/1'],
            // Un DPO ne peut pas modifier un enregistrement d'une autre entité
            [403, 'DPO.hvermeil', '/services/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/services/edit/666'],
            [404, 'Administrateur.ibleu', '/services/edit/666'],
            [404, 'DPO.nroux', '/services/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/services/edit/1'],
            [403, 'Valideur.cnoir', '/services/edit/1'],
            [403, 'Consultant.mrose', '/services/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/services/edit/666'],
            [403, 'Valideur.cnoir', '/services/edit/666'],
            [403, 'Consultant.mrose', '/services/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex()
    {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/services/index'],
            [200, 'Administrateur.ibleu', '/services/index'],
            [200, 'DPO.nroux', '/services/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/services/index'],
            [403, 'Valideur.cnoir', '/services/index'],
            [403, 'Consultant.mrose', '/services/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
