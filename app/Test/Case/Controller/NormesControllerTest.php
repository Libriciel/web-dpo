<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe NormesController.
 *
 * ./cake_utils.sh tests app Controller/NormesController
 *
 * @package app.Test.Case.Controller
 */
class NormesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.Norme',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Normes');
    }

    public function dataAccessAbroger() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/formulaires/abroger/1'],
            [302, 'Administrateur.ibleu', '/formulaires/abroger/1'],
            [302, 'DPO.nroux', '/formulaires/abroger/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/formulaires/abroger/666'],
            [404, 'Administrateur.ibleu', '/formulaires/abroger/666'],
            [404, 'DPO.nroux', '/formulaires/abroger/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/formulaires/abroger/1'],
            [403, 'Valideur.cnoir', '/formulaires/abroger/1'],
            [403, 'Consultant.mrose', '/formulaires/abroger/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/formulaires/abroger/666'],
            [403, 'Valideur.cnoir', '/formulaires/abroger/666'],
            [403, 'Consultant.mrose', '/formulaires/abroger/666'],
        ];
    }

    /**
     * @dataProvider dataAccessAbroger
     */
    public function testAccessAbroger($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/formulaires/add'],
            [200, 'Administrateur.ibleu', '/formulaires/add'],
            [200, 'DPO.nroux', '/formulaires/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/formulaires/add'],
            [403, 'Valideur.cnoir', '/formulaires/add'],
            [403, 'Consultant.mrose', '/formulaires/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDeleteFileSave() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/formulaires/deleteFileSave/1'],
            [302, 'Administrateur.ibleu', '/formulaires/deleteFileSave/1'],
            [302, 'DPO.nroux', '/formulaires/deleteFileSave/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/formulaires/deleteFileSave/2'],
            [403, 'DPO.hvermeil', '/formulaires/deleteFileSave/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/formulaires/deleteFileSave/666'],
            [404, 'Administrateur.ibleu', '/formulaires/deleteFileSave/666'],
            [404, 'DPO.nroux', '/formulaires/deleteFileSave/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/formulaires/deleteFileSave/1'],
            [403, 'Valideur.cnoir', '/formulaires/deleteFileSave/1'],
            [403, 'Consultant.mrose', '/formulaires/deleteFileSave/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/formulaires/deleteFileSave/666'],
            [403, 'Valideur.cnoir', '/formulaires/deleteFileSave/666'],
            [403, 'Consultant.mrose', '/formulaires/deleteFileSave/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDeleteFileSave
     */
    public function xtestAccessDeleteFileSave($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDownload() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            // 1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            [200, 'Administrateur.ibleu', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            [200, 'DPO.nroux', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            [200, 'Rédacteur.rjaune', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            [200, 'Valideur.cnoir', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            [200, 'Consultant.mrose', '/formulaires/download/1513602672.pdf/AU-001.pdf'],
            // 2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/formulaires/download/666.pdf/666.pdf'],
            [404, 'Administrateur.ibleu', '/formulaires/download/666.pdf/666.pdf'],
            [404, 'DPO.nroux', '/formulaires/download/666.pdf/666.pdf'],
            [404, 'Rédacteur.rjaune', '/formulaires/download/666.pdf/666.pdf'],
            [404, 'Valideur.cnoir', '/formulaires/download/666.pdf/666.pdf'],
            [404, 'Consultant.mrose', '/formulaires/download/666.pdf/666.pdf'],
        ];
    }

    /**
     * @dataProvider dataAccessDownload
     */
    public function testAccessDownload($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/formulaires/edit/1'],
            [302, 'Administrateur.ibleu', '/formulaires/edit/1'],
            [302, 'DPO.nroux', '/formulaires/edit/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/formulaires/edit/2'],
            [403, 'DPO.hvermeil', '/formulaires/edit/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/formulaires/edit/666'],
            [404, 'Administrateur.ibleu', '/formulaires/edit/666'],
            [404, 'DPO.nroux', '/formulaires/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/formulaires/edit/1'],
            [403, 'Valideur.cnoir', '/formulaires/edit/1'],
            [403, 'Consultant.mrose', '/formulaires/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/formulaires/edit/666'],
            [403, 'Valideur.cnoir', '/formulaires/edit/666'],
            [403, 'Consultant.mrose', '/formulaires/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function xtestAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/formulaires/index'],
            [200, 'Administrateur.ibleu', '/formulaires/index'],
            [200, 'DPO.nroux', '/formulaires/index'],
            [200, 'Rédacteur.rjaune', '/formulaires/index'],
            [200, 'Valideur.cnoir', '/formulaires/index'],
            [200, 'Consultant.mrose', '/formulaires/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessRevoquerAbrogation() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/formulaires/revoquerAbrogation/1'],
            [302, 'Administrateur.ibleu', '/formulaires/revoquerAbrogation/1'],
            [302, 'DPO.nroux', '/formulaires/revoquerAbrogation/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/formulaires/revoquerAbrogation/666'],
            [404, 'Administrateur.ibleu', '/formulaires/revoquerAbrogation/666'],
            [404, 'DPO.nroux', '/formulaires/revoquerAbrogation/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/formulaires/revoquerAbrogation/1'],
            [403, 'Valideur.cnoir', '/formulaires/revoquerAbrogation/1'],
            [403, 'Consultant.mrose', '/formulaires/revoquerAbrogation/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/formulaires/revoquerAbrogation/666'],
            [403, 'Valideur.cnoir', '/formulaires/revoquerAbrogation/666'],
            [403, 'Consultant.mrose', '/formulaires/revoquerAbrogation/666'],
        ];
    }

    /**
     * @dataProvider dataAccessRevoquerAbrogation
     */
    public function testAccessRevoquerAbrogation($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
