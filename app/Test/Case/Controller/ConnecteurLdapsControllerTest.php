<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe ConnecteurLdapsController.
 *
 * ./cake_utils.sh tests app Controller/ConnecteurLdapsController
 *
 * @package app.Test.Case.Controller
 */
class ConnecteurLdapsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.ConnecteurLdap',
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('ConnecteurLdaps');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/connecteur_ldaps/add'],
            [200, 'Administrateur.ibleu', '/connecteur_ldaps/add'],
            [200, 'DPO.nroux', '/connecteur_ldaps/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/connecteur_ldaps/add'],
            [403, 'Valideur.cnoir', '/connecteur_ldaps/add'],
            [403, 'Consultant.mrose', '/connecteur_ldaps/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
