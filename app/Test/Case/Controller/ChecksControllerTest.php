<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe ChecksController.
 *
 * ./cake_utils.sh tests app Controller/ChecksController
 *
 * @package app.Test.Case.Controller
 */
class ChecksControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Checks');
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/checks/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/checks/index'],
            [403, 'DPO.nroux', '/checks/index'],
            [403, 'Rédacteur.rjaune', '/checks/index'],
            [403, 'Valideur.cnoir', '/checks/index'],
            [403, 'Consultant.mrose', '/checks/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
