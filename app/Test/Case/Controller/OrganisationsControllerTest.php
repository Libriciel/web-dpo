<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe AdminsController.
 *
 * ./cake_utils.sh tests app Controller/OrganisationsController
 *
 * @package app.Test.Case.Controller
 */
class OrganisationsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Article',
        'app.ArticleOrganisation',
        'app.Commentaire',
        'app.Coresponsable',
        'app.Cron',
        'app.Droit',
        'app.EtatFiche',
        'app.FormulaireModeleOrganisation',
        'app.Fiche',
        'app.Fichier',
        'app.ListeDroit',
        'app.Modification',
        'app.ModeleExtraitRegistreOrganisation',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.OrganisationUserService',
        'app.ResponsableOrganisation',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.Soustraitance',
        'app.SoustraitantOrganisation',
        'app.Typage',
        'app.TypageOrganisation',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Organisations');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/organisations/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/organisations/add'],
            [403, 'DPO.nroux', '/organisations/add'],
            [403, 'Rédacteur.rjaune', '/organisations/add'],
            [403, 'Valideur.cnoir', '/organisations/add'],
            [403, 'Consultant.mrose', '/organisations/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAdministrer() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/organisations/administrer'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/organisations/administrer'],
            [403, 'DPO.nroux', '/organisations/administrer'],
            [403, 'Rédacteur.rjaune', '/organisations/administrer'],
            [403, 'Valideur.cnoir', '/organisations/administrer'],
            [403, 'Consultant.mrose', '/organisations/administrer'],
        ];
    }

    /**
     * @dataProvider dataAccessAdministrer
     */
    public function testAccessAdministrer($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessChange() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/organisations/change'],
            [302, 'Administrateur.ibleu', '/organisations/change'],
            [302, 'DPO.nroux', '/organisations/change'],
            [302, 'Rédacteur.rjaune', '/organisations/change'],
            [302, 'Valideur.cnoir', '/organisations/change'],
            [302, 'Consultant.mrose', '/organisations/change'],
        ];
    }

    /**
     * @dataProvider dataAccessChange
     */
    public function testAccessChange($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessChangenotification() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/organisations/changenotification'],
            [302, 'Administrateur.ibleu', '/organisations/changenotification'],
            [302, 'DPO.nroux', '/organisations/changenotification'],
            [302, 'Rédacteur.rjaune', '/organisations/changenotification'],
            [302, 'Valideur.cnoir', '/organisations/changenotification'],
            [302, 'Consultant.mrose', '/organisations/changenotification'],
        ];
    }

    /**
     * @dataProvider dataAccessChangenotification
     */
    public function testAccessChangenotification($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/organisations/delete/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/organisations/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Administrateur.ibleu', '/organisations/delete/1'],
            [403, 'DPO.nroux', '/organisations/delete/1'],
            [403, 'Rédacteur.rjaune', '/organisations/delete/1'],
            [403, 'Valideur.cnoir', '/organisations/delete/1'],
            [403, 'Consultant.mrose', '/organisations/delete/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Administrateur.ibleu', '/organisations/delete/666'],
            [403, 'DPO.nroux', '/organisations/delete/666'],
            [403, 'Rédacteur.rjaune', '/organisations/delete/666'],
            [403, 'Valideur.cnoir', '/organisations/delete/666'],
            [403, 'Consultant.mrose', '/organisations/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/organisations/edit/1'],
            [200, 'Administrateur.ibleu', '/organisations/edit/1'],
            // Un administrateur ne peut accéder à une autre entité que la sienne
            [403, 'Administrateur.findigo', '/organisations/edit/1'],
            [200, 'DPO.nroux', '/organisations/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/organisations/edit/666'],
            [404, 'Administrateur.ibleu', '/organisations/edit/666'],
            [404, 'DPO.nroux', '/organisations/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/organisations/edit/1'],
            [403, 'Valideur.cnoir', '/organisations/edit/1'],
            [403, 'Consultant.mrose', '/organisations/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/organisations/edit/666'],
            [403, 'Valideur.cnoir', '/organisations/edit/666'],
            [403, 'Consultant.mrose', '/organisations/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessGestionrgpd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/organisations/gestionrgpd'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/organisations/gestionrgpd'],
            [403, 'DPO.nroux', '/organisations/gestionrgpd'],
            [403, 'Rédacteur.rjaune', '/organisations/gestionrgpd'],
            [403, 'Valideur.cnoir', '/organisations/gestionrgpd'],
            [403, 'Consultant.mrose', '/organisations/gestionrgpd'],
        ];
    }

    /**
     * @dataProvider dataAccessGestionrgpd
     */
    public function testAccessGestionrgpd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/organisations/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/organisations/index'],
            [403, 'DPO.nroux', '/organisations/index'],
            [403, 'Rédacteur.rjaune', '/organisations/index'],
            [403, 'Valideur.cnoir', '/organisations/index'],
            [403, 'Consultant.mrose', '/organisations/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessPolitiquedeconfidentialite() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/organisations/politiquedeconfidentialite'],
            [200, 'DPO.nroux', '/organisations/politiquedeconfidentialite'],
            [200, 'Rédacteur.rjaune', '/organisations/politiquedeconfidentialite'],
            [200, 'Valideur.cnoir', '/organisations/politiquedeconfidentialite'],
            [200, 'Consultant.mrose', '/organisations/politiquedeconfidentialite'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/organisations/politiquedeconfidentialite'],
        ];
    }

    /**
     * @dataProvider dataAccessPolitiquedeconfidentialite
     */
    public function testAccessPolitiquedeconfidentialite($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessPolitiquepassword() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/organisations/politiquepassword'],
            [200, 'Administrateur.ibleu', '/organisations/politiquepassword'],
            [200, 'DPO.nroux', '/organisations/politiquepassword'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/organisations/politiquepassword'],
            [403, 'Valideur.cnoir', '/organisations/politiquepassword'],
            [403, 'Consultant.mrose', '/organisations/politiquepassword'],
        ];
    }

    /**
     * @dataProvider dataAccessPolitiquepassword
     */
    public function testAccessPolitiquepassword($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessSelectorganisation() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/organisations/selectorganisation'],
            [302, 'Administrateur.ibleu', '/organisations/selectorganisation'],
            [302, 'DPO.nroux', '/organisations/selectorganisation'],
            [302, 'Rédacteur.rjaune', '/organisations/selectorganisation'],
            [302, 'Valideur.cnoir', '/organisations/selectorganisation'],
            [302, 'Consultant.mrose', '/organisations/selectorganisation'],
        ];
    }

    /**
     * @dataProvider dataAccessSelectorganisation
     */
    public function testAccessSelectorganisation($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessShow() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/organisations/show/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/organisations/show/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Administrateur.ibleu', '/organisations/show/1'],
            [403, 'DPO.nroux', '/organisations/show/1'],
            [403, 'Rédacteur.rjaune', '/organisations/show/1'],
            [403, 'Valideur.cnoir', '/organisations/show/1'],
            [403, 'Consultant.mrose', '/organisations/show/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Administrateur.ibleu', '/organisations/show/666'],
            [403, 'DPO.nroux', '/organisations/show/666'],
            [403, 'Rédacteur.rjaune', '/organisations/show/666'],
            [403, 'Valideur.cnoir', '/organisations/show/666'],
            [403, 'Consultant.mrose', '/organisations/show/666'],
        ];
    }

    /**
     * @dataProvider dataAccessShow
     */
    public function testAccessShow($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
