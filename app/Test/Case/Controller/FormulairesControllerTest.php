<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe FormulairesController.
 *
 * ./cake_utils.sh tests app Controller/FormulairesController
 *
 * @package app.Test.Case.Controller
 */
class FormulairesControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Champ',
        'app.Fiche',
        'app.ListeDroit',
        'app.Modele',
        'app.Formulaire',
        'app.FormulaireOrganisation',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Formulaires');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/formulaires/add/2'],
            [200, 'DPO.nroux', '/formulaires/add/2'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/formulaires/add/2'],
            [403, 'DPO.hvermeil', '/formulaires/add/2'],
            // On ne peut pas accéder à enregistrement activé ou possédant des fiches liées
            [403, 'Administrateur.ibleu', '/formulaires/add/1'],
            [403, 'DPO.nroux', '/formulaires/add/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/add/666'],
            [404, 'DPO.nroux', '/formulaires/add/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/add/2'],
            [403, 'Rédacteur.rjaune', '/formulaires/add/2'],
            [403, 'Valideur.cnoir', '/formulaires/add/2'],
            [403, 'Consultant.mrose', '/formulaires/add/2'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/add/666'],
            [403, 'Rédacteur.rjaune', '/formulaires/add/666'],
            [403, 'Valideur.cnoir', '/formulaires/add/666'],
            [403, 'Consultant.mrose', '/formulaires/add/666'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAddFirst() {
        $data = ['Formulaire' => ['libelle' => 'Test', 'description' => '']];
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [302, 'Administrateur.ibleu', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
            [302, 'DPO.nroux', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
            // Accès avec la mauvaise méthode
            [405, 'Administrateur.ibleu', '/formulaires/addFirst'],
            [405, 'DPO.nroux', '/formulaires/addFirst'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
            [403, 'Rédacteur.rjaune', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
            [403, 'Valideur.cnoir', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
            [403, 'Consultant.mrose', '/formulaires/addFirst', ['method' => 'POST', 'data' => $data]],
        ];
    }

    /**
     * @dataProvider dataAccessAddFirst
     */
    public function testAccessAddFirst($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Administrateur.ibleu', '/formulaires/delete/2'],
            [302, 'DPO.nroux', '/formulaires/delete/2'],
            // On ne peut pas supprimer un enregistrement activé ou possédant des fiches liées
            [404, 'Administrateur.ibleu', '/formulaires/delete/1'],
            [404, 'DPO.nroux', '/formulaires/delete/1'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/formulaires/delete/2'],
            [403, 'DPO.hvermeil', '/formulaires/delete/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/delete/666'],
            [404, 'DPO.nroux', '/formulaires/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/delete/2'],
            [403, 'Rédacteur.rjaune', '/formulaires/delete/2'],
            [403, 'Valideur.cnoir', '/formulaires/delete/2'],
            [403, 'Consultant.mrose', '/formulaires/delete/2'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/delete/666'],
            [403, 'Rédacteur.rjaune', '/formulaires/delete/666'],
            [403, 'Valideur.cnoir', '/formulaires/delete/666'],
            [403, 'Consultant.mrose', '/formulaires/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDupliquer() {
        $dataExists = ['Formulaire' => ['id' => 1, 'libelle' => 'Test', 'description' => '', 'rt_externe' => false]];
        $dataNotExists = ['Formulaire' => ['id' => 666, 'libelle' => 'Test', 'description' => '', 'rt_externe' => false]];
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Administrateur.ibleu', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            [302, 'DPO.nroux', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            // Un utilisateur mono-collectivité peut accéder à l'enregistrement d'une autre collectivité
            [302, 'Administrateur.findigo', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            [302, 'DPO.hvermeil', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            // Accès avec la mauvaise méthode
            [405, 'Administrateur.ibleu', '/formulaires/dupliquer'],
            [405, 'DPO.nroux', '/formulaires/dupliquer'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
            [404, 'DPO.nroux', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Rédacteur.rjaune', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Valideur.cnoir', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            [403, 'Consultant.mrose', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataExists]],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Rédacteur.rjaune', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Valideur.cnoir', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
            [403, 'Consultant.mrose', '/formulaires/dupliquer', ['method' => 'POST', 'data' => $dataNotExists]],
        ];
    }

    /**
     * @dataProvider dataAccessDupliquer
     */
    public function testAccessDupliquer($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

//    public function dataAccessDupliquerOrganisation() {
//        $dataExists = ['Formulaire' => ['id' => 1, 'libelle' => 'Test', 'description' => '', 'organisationCible' => 2]];
//        $dataNotExists = ['Formulaire' => ['id' => 666, 'libelle' => 'Test', 'description' => '', 'organisationCible' => 2]];
//
//        return [
//            // 1. Utilisateurs pouvant accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [302, 'Administrateur.ibleu', '/formulaires/dupliquerOrganisation/1'],
//            [302, 'DPO.nroux', '/formulaires/dupliquerOrganisation/1'],
//            // 1.2. Enregistrement inexistant
//            [404, 'Administrateur.ibleu', '/formulaires/dupliquerOrganisation/666'],
//            [404, 'DPO.nroux', '/formulaires/dupliquerOrganisation/666'],
//            // 1.3 Accès avec la mauvaise méthode
//            [405, 'Administrateur.ibleu', '/formulaires/dupliquerOrganisation/1'],
//            [405, 'DPO.nroux', '/formulaires/dupliquerOrganisation/1'],
//            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
//            // 1.1. Enregistrement existant
//            [403, 'Superadministrateur.superadmin', '/formulaires/dupliquerOrganisation/1'],
//            [403, 'Rédacteur.rjaune', '/formulaires/dupliquerOrganisation/1'],
//            [403, 'Valideur.cnoir', '/formulaires/dupliquerOrganisation/1'],
//            [403, 'Consultant.mrose', '/formulaires/dupliquerOrganisation/1'],
//            // 2.2. Enregistrement inexistant
//            [403, 'Superadministrateur.superadmin', '/formulaires/dupliquerOrganisation/666'],
//            [403, 'Rédacteur.rjaune', '/formulaires/dupliquerOrganisation/666'],
//            [403, 'Valideur.cnoir', '/formulaires/dupliquerOrganisation/666'],
//            [403, 'Consultant.mrose', '/formulaires/dupliquerOrganisation/666'],
//        ];
//    }

    /**
     * @dataProvider dataAccessDupliquerOrganisation
     */
//    public function testAccessDupliquerOrganisation($expectedStatus, $user, $url, $options = []) {
//        $this->markTestIncomplete('Il manque des données pour les tests de multi-entités');
//        $this->assertActionAccess($expectedStatus, $user, $url, $options);
//    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/formulaires/edit/2'],
            [200, 'DPO.nroux', '/formulaires/edit/2'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/formulaires/edit/2'],
            [403, 'DPO.hvermeil', '/formulaires/edit/2'],
            // On ne peut pas modifier un enregistrement activé ou possédant des fiches liées
            [403, 'Administrateur.ibleu', '/formulaires/edit/1'],
            [403, 'DPO.nroux', '/formulaires/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/edit/666'],
            [404, 'DPO.nroux', '/formulaires/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/edit/2'],
            [403, 'Rédacteur.rjaune', '/formulaires/edit/2'],
            [403, 'Valideur.cnoir', '/formulaires/edit/2'],
            [403, 'Consultant.mrose', '/formulaires/edit/2'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/edit/666'],
            [403, 'Rédacteur.rjaune', '/formulaires/edit/666'],
            [403, 'Valideur.cnoir', '/formulaires/edit/666'],
            [403, 'Consultant.mrose', '/formulaires/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Administrateur.ibleu', '/formulaires/index/1'],
            [200, 'DPO.nroux', '/formulaires/index/1'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Superadministrateur.superadmin', '/formulaires/index/1'],
            [403, 'Rédacteur.rjaune', '/formulaires/index/1'],
            [403, 'Valideur.cnoir', '/formulaires/index/1'],
            [403, 'Consultant.mrose', '/formulaires/index/1'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessShow() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Administrateur.ibleu', '/formulaires/show/1'],
            [200, 'DPO.nroux', '/formulaires/show/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/show/666'],
            [404, 'DPO.nroux', '/formulaires/show/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/show/1'],
            [403, 'Rédacteur.rjaune', '/formulaires/show/1'],
            [403, 'Valideur.cnoir', '/formulaires/show/1'],
            [403, 'Consultant.mrose', '/formulaires/show/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/show/666'],
            [403, 'Rédacteur.rjaune', '/formulaires/show/666'],
            [403, 'Valideur.cnoir', '/formulaires/show/666'],
            [403, 'Consultant.mrose', '/formulaires/show/666'],
        ];
    }

    /**
     * @dataProvider dataAccessShow
     */
    public function testAccessShow($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessToggle() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Administrateur.ibleu', '/formulaires/toggle/1'],
            [302, 'DPO.nroux', '/formulaires/toggle/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Administrateur.ibleu', '/formulaires/toggle/666'],
            [404, 'DPO.nroux', '/formulaires/toggle/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Superadministrateur.superadmin', '/formulaires/toggle/1'],
            [403, 'Rédacteur.rjaune', '/formulaires/toggle/1'],
            [403, 'Valideur.cnoir', '/formulaires/toggle/1'],
            [403, 'Consultant.mrose', '/formulaires/toggle/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Superadministrateur.superadmin', '/formulaires/toggle/1'],
            [403, 'Rédacteur.rjaune', '/formulaires/toggle/666'],
            [403, 'Valideur.cnoir', '/formulaires/toggle/666'],
            [403, 'Consultant.mrose', '/formulaires/toggle/666'],
        ];
    }

    /**
     * @dataProvider dataAccessToggle
     */
    public function testAccessToggle($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
