<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe UsersController.
 *
 * ./cake_utils.sh tests app Controller/UsersController
 *
 * @package app.Test.Case.Controller
 */
class UsersControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Admin',
        'app.Commentaire',
        'app.Droit',
        'app.EtatFiche',
        'app.Fiche',
        'app.Jeton',
        'app.ListeDroit',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.OrganisationUserService',
        'app.Role',
        'app.RoleDroit',
        'app.Service',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Users');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/add'],
            [200, 'Administrateur.ibleu', '/users/add'],
            [200, 'DPO.nroux', '/users/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/users/add'],
            [403, 'Valideur.cnoir', '/users/add'],
            [403, 'Consultant.mrose', '/users/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAdminIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/admin_index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/users/admin_index'],
            [403, 'DPO.nroux', '/users/admin_index'],
            [403, 'Rédacteur.rjaune', '/users/admin_index'],
            [403, 'Valideur.cnoir', '/users/admin_index'],
            [403, 'Consultant.mrose', '/users/admin_index'],
        ];
    }

    /**
     * @dataProvider dataAccessAdminIndex
     */
    public function testAccessAdminIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessAjaxPassword() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/ajax_password'],
            [200, 'Administrateur.ibleu', '/users/ajax_password'],
            [200, 'DPO.nroux', '/users/ajax_password'],
            [200, 'Rédacteur.rjaune', '/users/ajax_password'],
            [200, 'Valideur.cnoir', '/users/ajax_password'],
            [200, 'Consultant.mrose', '/users/ajax_password'],
        ];
    }

    /**
     * @dataProvider dataAccessAjaxPassword
     */
    public function testAccessAjaxPassword($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, ['method' => 'POST', 'data' => ['password' => 'test']]);
    }

    public function testAccessCasLogin() {
        // Tout utilisateur non connecté peut accéder à la fonctionnalité
        $this->assertActionAccess(302, null, '/users/login');
    }

    /**
     * @dataProvider dataAccessLogout
     */
    public function xtestAccessCasLogout($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessChangepassword() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/changepassword'],
            [200, 'Administrateur.ibleu', '/users/changepassword'],
            [200, 'DPO.nroux', '/users/changepassword'],
            [200, 'Rédacteur.rjaune', '/users/changepassword'],
            [200, 'Valideur.cnoir', '/users/changepassword'],
            [200, 'Consultant.mrose', '/users/changepassword'],
        ];
    }

    /**
     * @dataProvider dataAccessChangepassword
     */
    public function testAccessChangepassword($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [302, 'Superadministrateur.superadmin', '/users/delete/4'],
            // Personne ne peut supprimer un Superadministrateur
            [500, 'Superadministrateur.superadmin', '/users/delete/2'],
            // Un utilisateur ne peut pas se supprimer lui-même
            [403, 'Administrateur.ibleu', '/users/delete/3'],
            // Un administrateur mono-collectivité ne peut pas supprimer un utilisateur d'une autre collectivité
            [403, 'Administrateur.ibleu', '/users/delete/15'],
            // Mais il peut supprimer un autre utilisateur de sa collectivité
            [302, 'Administrateur.ibleu', '/users/delete/8'],
            // Le DPO n'a pas le droit de supprimer un Superadministrateur
            [403, 'DPO.nroux', '/users/delete/2'],
            // Mais il a le droit de supprimer tout autre utilisateur au sein de sa collectivité
            [302, 'DPO.nroux', '/users/delete/3'],
            // Sauf lui-même bien entendu
            [403, 'DPO.nroux', '/users/delete/11'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/users/delete/666'],
            [404, 'Administrateur.ibleu', '/users/delete/666'],
            [404, 'DPO.nroux', '/users/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/users/delete/3'],
            [403, 'Valideur.cnoir', '/users/delete/3'],
            [403, 'Consultant.mrose', '/users/delete/3'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/users/delete/666'],
            [403, 'Valideur.cnoir', '/users/delete/666'],
            [403, 'Consultant.mrose', '/users/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/users/edit/9'],
            [200, 'Administrateur.ibleu', '/users/edit/9'],
            // L'administrateur mono-collectivité ne peut pas modifier un utilisateur d'une autre collectivité
            [403, 'Administrateur.ibleu', '/users/edit/12'],
            [200, 'DPO.nroux', '/users/edit/9'],
            // Le DPO mono-collectivité ne peut pas modifier un utilisateur d'une autre collectivité
            [403, 'DPO.nroux', '/users/edit/12'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/users/edit/666'],
            [404, 'Administrateur.ibleu', '/users/edit/666'],
            [404, 'DPO.nroux', '/users/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/users/edit/9'],
            [403, 'Valideur.cnoir', '/users/edit/9'],
            [403, 'Consultant.mrose', '/users/edit/9'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/users/edit/666'],
            [403, 'Valideur.cnoir', '/users/edit/666'],
            [403, 'Consultant.mrose', '/users/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/index'],
            [200, 'Administrateur.ibleu', '/users/index'],
            [200, 'DPO.nroux', '/users/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/users/index'],
            [403, 'Valideur.cnoir', '/users/index'],
            [403, 'Consultant.mrose', '/users/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function testAccessLogin() {
        // Tout utilisateur non connecté peut accéder à la fonctionnalité
        $this->assertActionAccess(302, null, '/users/login');
    }

    public function dataAccessLogout() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [302, 'Superadministrateur.superadmin', '/users/logout'],
            [302, 'Administrateur.ibleu', '/users/logout'],
            [302, 'DPO.nroux', '/users/logout'],
            [302, 'Rédacteur.rjaune', '/users/logout'],
            [302, 'Valideur.cnoir', '/users/logout'],
            [302, 'Consultant.mrose', '/users/logout'],
        ];
    }

    /**
     * @dataProvider dataAccessLogout
     */
    public function testAccessLogout($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessPreferences() {
        return [
            // Tous les utilisateurs peuvent accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/users/preferences'],
            [200, 'Administrateur.ibleu', '/users/preferences'],
            [200, 'DPO.nroux', '/users/preferences'],
            [200, 'Rédacteur.rjaune', '/users/preferences'],
            [200, 'Valideur.cnoir', '/users/preferences'],
            [200, 'Consultant.mrose', '/users/preferences'],
        ];
    }

    /**
     * @dataProvider dataAccessPreferences
     */
    public function testAccessPreferences($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessView() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/users/view/3'],
            [200, 'Administrateur.ibleu', '/users/view/3'],
            // L'administrateur mono-collectivité ne peut pas voir un utilisateur d'une autre collectivité
            [403, 'Administrateur.ibleu', '/users/view/12'],
            [200, 'DPO.nroux', '/users/view/3'],
            // Le DPO mono-collectivité ne peut pas voir un utilisateur d'une autre collectivité
            [403, 'DPO.nroux', '/users/view/12'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/users/view/666'],
            [404, 'Administrateur.ibleu', '/users/view/666'],
            [404, 'DPO.nroux', '/users/view/666'],
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/users/view/9'],
            [403, 'Valideur.cnoir', '/users/view/9'],
            [403, 'Consultant.mrose', '/users/view/9'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/users/view/666'],
            [403, 'Valideur.cnoir', '/users/view/666'],
            [403, 'Consultant.mrose', '/users/view/666'],
        ];
    }

    /**
     * @dataProvider dataAccessView
     */
    public function testAccessView($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
