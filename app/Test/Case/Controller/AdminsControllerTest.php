<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe AdminsController.
 *
 * ./cake_utils.sh tests app Controller/AdminsController
 *
 * @package app.Test.Case.Controller
 */
class AdminsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Admin',
        'app.EtatFiche',
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Admins');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/admins/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/admins/add'],
            [403, 'DPO.nroux', '/admins/add'],
            [403, 'Rédacteur.rjaune', '/admins/add'],
            [403, 'Valideur.cnoir', '/admins/add'],
            [403, 'Consultant.mrose', '/admins/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessChangeViewLogin() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/admins/ChangeViewLogin'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/admins/ChangeViewLogin'],
            [403, 'DPO.nroux', '/admins/ChangeViewLogin'],
            [403, 'Rédacteur.rjaune', '/admins/ChangeViewLogin'],
            [403, 'Valideur.cnoir', '/admins/ChangeViewLogin'],
            [403, 'Consultant.mrose', '/admins/ChangeViewLogin'],
        ];
    }

    /**
     * @dataProvider dataAccessChangeViewLogin
     */
    public function testAccessChangeViewLogin($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            // Je ne peux pas me supprimer moi-même
            [403, 'Superadministrateur.superadmin', '/admins/delete/1/1'],
            // Un autre Superadministrateur ne peut pas supprimer l'id 1 non  plus
            [403, 'Superadministrateur.superadmin2', '/admins/delete/1/1'],
            // Mais je peux supprimer l'autre Superadministrateur
            [302, 'Superadministrateur.superadmin', '/admins/delete/2/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/admins/delete/666/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Administrateur.ibleu', '/admins/delete/1/1'],
            [403, 'DPO.nroux', '/admins/delete/1/1'],
            [403, 'Rédacteur.rjaune', '/admins/delete/1/1'],
            [403, 'Valideur.cnoir', '/admins/delete/1/1'],
            [403, 'Consultant.mrose', '/admins/delete/1/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Administrateur.ibleu', '/admins/delete/666/666'],
            [403, 'DPO.nroux', '/admins/delete/666/666'],
            [403, 'Rédacteur.rjaune', '/admins/delete/666/666'],
            [403, 'Valideur.cnoir', '/admins/delete/666/666'],
            [403, 'Consultant.mrose', '/admins/delete/666/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/admins/edit/1'],
            [200, 'Superadministrateur.superadmin2', '/admins/edit/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/admins/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Administrateur.ibleu', '/admins/edit/1'],
            [403, 'DPO.nroux', '/admins/edit/1'],
            [403, 'Rédacteur.rjaune', '/admins/edit/1'],
            [403, 'Valideur.cnoir', '/admins/edit/1'],
            [403, 'Consultant.mrose', '/admins/edit/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Administrateur.ibleu', '/admins/edit/666'],
            [403, 'DPO.nroux', '/admins/edit/666'],
            [403, 'Rédacteur.rjaune', '/admins/edit/666'],
            [403, 'Valideur.cnoir', '/admins/edit/666'],
            [403, 'Consultant.mrose', '/admins/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/admins/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Administrateur.ibleu', '/admins/index'],
            [403, 'DPO.nroux', '/admins/index'],
            [403, 'Rédacteur.rjaune', '/admins/index'],
            [403, 'Valideur.cnoir', '/admins/index'],
            [403, 'Consultant.mrose', '/admins/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
