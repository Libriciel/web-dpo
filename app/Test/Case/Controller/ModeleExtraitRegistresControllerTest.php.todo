<?php
App::uses( 'AppController', 'Controller' );
App::uses( 'ModeleExtraitRegistresController', 'Controller' );
App::uses( 'ControllerTestTrait', 'Test/Trait/Controller' );
App::uses( 'ControllerAccessAssertionTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe ModeleExtraitRegistresController.
 *
 * @todo: pour l'instant, les tests ne sont pas faits pour le DPO car on n'a pas de données dans les fixtures pour
 * vérifier quand ça se passe bien.
 *
 * ./cake_utils.sh tests app Controller/ModeleExtraitRegistresController
 *
 * @package app.Test.Case.Controller
 */
class ModeleExtraitRegistresTestsController extends ModeleExtraitRegistresController
{
    use ControllerTestTrait;
}

class ModeleExtraitRegistresControllerTest extends CakeTestCase
{
    use ControllerAccessAssertionTrait;

    public $fixtures = [
        'app.Fiche',
        'app.ModeleExtraitRegistre',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.User',
        'app.Valeur',
    ];

    public function dataAccessAdd() {
        $url = ['action' => 'add'];
        return [
            [false, 'Superadministrateur', $url],
            [false, 'Administrateur', $url],
            [false, 'Redacteur', $url],
            [false, 'Valideur', $url],
            [false, 'Consultant', $url],
            //[false, 'Dpo', $url],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expected, $role, $url) {
        $this->assertAccess($url, $expected, ['role' => $role]);
    }

    public function dataAccessDelete() {
        $url = ['action' => 'delete', 1];
        return [
            [false, 'Superadministrateur', $url],
            [false, 'Administrateur', $url],
            [false, 'Redacteur', $url],
            [false, 'Valideur', $url],
            [false, 'Consultant', $url],
            //[false, 'Dpo', $url],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expected, $role, $url) {
        $this->assertAccess($url, $expected, ['role' => $role]);
    }

    public function dataAccessDownload() {
        $url = ['action' => 'download', 'modele.odt', 'modele.odt'];
        return [
            [false, 'Superadministrateur', $url],
            [false, 'Administrateur', $url],
            [false, 'Redacteur', $url],
            [false, 'Valideur', $url],
            [false, 'Consultant', $url],
            //[false, 'Dpo', $url],
        ];
    }

    /**
     * @dataProvider dataAccessDownload
     */
    public function testAccessDownload($expected, $role, $url) {
        $this->assertAccess($url, $expected, ['role' => $role]);
    }

    public function dataAccessIndex() {
        $url = ['action' => 'index'];
        return [
            [false, 'Superadministrateur', $url],
            [false, 'Administrateur', $url],
            [false, 'Redacteur', $url],
            [false, 'Valideur', $url],
            [false, 'Consultant', $url],
            //[false, 'Dpo', $url],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expected, $role, $url) {
        $this->assertAccess($url, $expected, ['role' => $role]);
    }
}
