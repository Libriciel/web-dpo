<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe AuthentificationsController.
 *
 * ./cake_utils.sh tests app Controller/AuthentificationsController
 *
 * @package app.Test.Case.Controller
 */
class AuthentificationsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Authentification',
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Authentifications');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/authentifications/add'],
            [200, 'Administrateur.ibleu', '/authentifications/add'],
            [200, 'DPO.nroux', '/authentifications/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/authentifications/add'],
            [403, 'Valideur.cnoir', '/authentifications/add'],
            [403, 'Consultant.mrose', '/authentifications/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
