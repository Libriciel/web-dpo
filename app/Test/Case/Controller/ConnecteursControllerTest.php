<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe ConnecteursController.
 *
 * ./cake_utils.sh tests app Controller/ConnecteursController
 *
 * @package app.Test.Case.Controller
 */
class ConnecteursControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.Role',
        'app.RoleDroit',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Connecteurs');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/connecteurs/index'],
            [200, 'Administrateur.ibleu', '/connecteurs/index'],
            [200, 'DPO.nroux', '/connecteurs/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/connecteurs/index'],
            [403, 'Valideur.cnoir', '/connecteurs/index'],
            [403, 'Consultant.mrose', '/connecteurs/index'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
