<?php
App::uses('AppController', 'Controller');
App::uses( 'ControllerTestCaseAccessTrait', 'Test/Trait/Controller' );

/**
 * Tests d'intégration de la classe SoustraitantsController.
 *
 * ./cake_utils.sh tests app Controller/SoustraitantsController
 *
 * @package app.Test.Case.Controller
 */
class SoustraitantsControllerTest extends ControllerTestCase
{
    use ControllerTestCaseAccessTrait;

    public $fixtures = [
        'app.Fiche',
        'app.ListeDroit',
        'app.Notification',
        'app.Organisation',
        'app.OrganisationUser',
        'app.OrganisationUserRole',
        'app.Role',
        'app.RoleDroit',
        'app.Soustraitant',
        'app.SoustraitantOrganisation',
        'app.User',
        'app.Valeur',
    ];

    public function setUp() {
        parent::setUp();
        $this->controller = $this->generate('Soustraitants');
    }

    public function dataAccessAdd() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/soustraitants/add'],
            [200, 'Administrateur.ibleu', '/soustraitants/add'],
            [200, 'DPO.nroux', '/soustraitants/add'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/soustraitants/add'],
            [403, 'Valideur.cnoir', '/soustraitants/add'],
            [403, 'Consultant.mrose', '/soustraitants/add'],
        ];
    }

    /**
     * @dataProvider dataAccessAdd
     */
    public function testAccessAdd($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDelete() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            // 1.1.1. Créé par le Superadmin
            // 1.1.1.1. Non lié à une entité
            [302, 'Superadministrateur.superadmin', '/soustraitants/delete/1'],
            [403, 'Administrateur.ibleu', '/soustraitants/delete/1'],
            [403, 'DPO.nroux', '/soustraitants/delete/1'],
            // 1.1.1.2. Lié à une ou des entités
            [403, 'Superadministrateur.superadmin', '/soustraitants/delete/2'],
            [403, 'Administrateur.ibleu', '/soustraitants/delete/2'],
            [403, 'DPO.nroux', '/soustraitants/delete/2'],
            // 1.1.2. Créé par une entité
            // 1.1.2.1. Non lié à une entité
            [302, 'Superadministrateur.superadmin', '/soustraitants/delete/5'],
            // 1.1.2.1.1. Pour l'entité créatrice
            [302, 'Administrateur.ibleu', '/soustraitants/delete/5'],
            [302, 'DPO.nroux', '/soustraitants/delete/5'],
            // 1.1.2.1.2. Pour une autre entité que l'entité créatrice
            [403, 'Administrateur.findigo', '/soustraitants/delete/5'],
            [403, 'DPO.hvermeil', '/soustraitants/delete/5'],
            // 1.1.2.2. Lié à une ou des entités
            [403, 'Superadministrateur.superadmin', '/soustraitants/delete/6'],
            [403, 'Administrateur.ibleu', '/soustraitants/delete/6'],
            [403, 'DPO.nroux', '/soustraitants/delete/6'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/soustraitants/delete/666'],
            [404, 'Administrateur.ibleu', '/soustraitants/delete/666'],
            [404, 'DPO.nroux', '/soustraitants/delete/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            // 2.1.1. Créé par le Superadmin
            // 2.1.1.1. Non lié à une entité
            [403, 'Rédacteur.rjaune', '/soustraitants/delete/1'],
            [403, 'Valideur.cnoir', '/soustraitants/delete/1'],
            [403, 'Consultant.mrose', '/soustraitants/delete/1'],
            // 2.1.1.2. Lié à une ou des entités
            [403, 'Rédacteur.rjaune', '/soustraitants/delete/2'],
            [403, 'Valideur.cnoir', '/soustraitants/delete/2'],
            [403, 'Consultant.mrose', '/soustraitants/delete/2'],
            // 2.1.2. Créé par une entité
            // 2.1.2.1. Non lié à une entité
            [403, 'Rédacteur.rjaune', '/soustraitants/delete/5'],
            [403, 'Valideur.cnoir', '/soustraitants/delete/5'],
            [403, 'Consultant.mrose', '/soustraitants/delete/5'],
            // 2.1.2.2. Lié à une ou des entités
            [403, 'Rédacteur.rjaune', '/soustraitants/delete/6'],
            [403, 'Valideur.cnoir', '/soustraitants/delete/6'],
            [403, 'Consultant.mrose', '/soustraitants/delete/6'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/soustraitants/delete/666'],
            [403, 'Valideur.cnoir', '/soustraitants/delete/666'],
            [403, 'Consultant.mrose', '/soustraitants/delete/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDelete
     */
    public function testAccessDelete($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessDissocierSoustraitant() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant et associé
            [302, 'Superadministrateur.superadmin', '/soustraitants/dissocierSoustraitant/2'],
            [302, 'Administrateur.ibleu', '/soustraitants/dissocierSoustraitant/2'],
            [302, 'DPO.nroux', '/soustraitants/dissocierSoustraitant/2'],
            // Un utilisateur mono-collectivité ne peut pas accéder à l'enregistrement d'une autre collectivité
            [403, 'Administrateur.findigo', '/soustraitants/dissocierSoustraitant/2'],
            [403, 'DPO.hvermeil', '/soustraitants/dissocierSoustraitant/2'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/soustraitants/dissocierSoustraitant/666'],
            [404, 'Administrateur.ibleu', '/soustraitants/dissocierSoustraitant/666'],
            [404, 'DPO.nroux', '/soustraitants/dissocierSoustraitant/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/soustraitants/dissocierSoustraitant/2'],
            [403, 'Valideur.cnoir', '/soustraitants/dissocierSoustraitant/2'],
            [403, 'Consultant.mrose', '/soustraitants/dissocierSoustraitant/2'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/soustraitants/dissocierSoustraitant/666'],
            [403, 'Valideur.cnoir', '/soustraitants/dissocierSoustraitant/666'],
            [403, 'Consultant.mrose', '/soustraitants/dissocierSoustraitant/666'],
        ];
    }

    /**
     * @dataProvider dataAccessDissocierSoustraitant
     */
    public function testAccessDissocierSoustraitant($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEdit() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            // 1.1.1. Créé par le Superadmin
            // 1.1.1.1. Non lié à une entité
            [200, 'Superadministrateur.superadmin', '/soustraitants/edit/1'],
            [403, 'Administrateur.ibleu', '/soustraitants/edit/1'],
            [403, 'DPO.nroux', '/soustraitants/edit/1'],
            // 1.1.1.2. Lié à une ou des entités
            [200, 'Superadministrateur.superadmin', '/soustraitants/edit/2'],
            [403, 'Administrateur.ibleu', '/soustraitants/edit/2'],
            [403, 'DPO.nroux', '/soustraitants/edit/2'],
            // 1.1.2. Créé par une entité
            // 1.1.2.1. Non lié à une entité
            [200, 'Superadministrateur.superadmin', '/soustraitants/edit/5'],
            // 1.1.2.1.1. Pour l'entité créatrice
            [200, 'Administrateur.ibleu', '/soustraitants/edit/5'],
            [200, 'DPO.nroux', '/soustraitants/edit/5'],
            // 1.1.2.1.2. Pour une autre entité que l'entité créatrice
            [403, 'Administrateur.findigo', '/soustraitants/edit/5'],
            [403, 'DPO.hvermeil', '/soustraitants/edit/5'],
            // 1.1.2.2. Lié à une ou des entités
            [200, 'Superadministrateur.superadmin', '/soustraitants/edit/6'],
            [200, 'Administrateur.ibleu', '/soustraitants/edit/6'],
            [200, 'DPO.nroux', '/soustraitants/edit/6'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/soustraitants/edit/666'],
            [404, 'Administrateur.ibleu', '/soustraitants/edit/666'],
            [404, 'DPO.nroux', '/soustraitants/edit/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            // 2.1.1. Créé par le Superadmin
            // 2.1.1.1. Non lié à une entité
            [403, 'Rédacteur.rjaune', '/soustraitants/edit/1'],
            [403, 'Valideur.cnoir', '/soustraitants/edit/1'],
            [403, 'Consultant.mrose', '/soustraitants/edit/1'],
            // 2.1.1.2. Lié à une ou des entités
            [403, 'Rédacteur.rjaune', '/soustraitants/edit/2'],
            [403, 'Valideur.cnoir', '/soustraitants/edit/2'],
            [403, 'Consultant.mrose', '/soustraitants/edit/2'],
            // 2.1.2. Créé par une entité
            // 2.1.2.1. Non lié à une entité
            [403, 'Rédacteur.rjaune', '/soustraitants/edit/5'],
            [403, 'Valideur.cnoir', '/soustraitants/edit/5'],
            [403, 'Consultant.mrose', '/soustraitants/edit/5'],
            // 2.1.2.2. Lié à une ou des entités
            [403, 'Rédacteur.rjaune', '/soustraitants/edit/6'],
            [403, 'Valideur.cnoir', '/soustraitants/edit/6'],
            [403, 'Consultant.mrose', '/soustraitants/edit/6'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/soustraitants/edit/666'],
            [403, 'Valideur.cnoir', '/soustraitants/edit/666'],
            [403, 'Consultant.mrose', '/soustraitants/edit/666'],
        ];
    }

    /**
     * @dataProvider dataAccessEdit
     */
    public function testAccessEdit($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessEntite() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/soustraitants/entite'],
            [200, 'Administrateur.ibleu', '/soustraitants/entite'],
            [200, 'DPO.nroux', '/soustraitants/entite'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/soustraitants/entite'],
            [403, 'Valideur.cnoir', '/soustraitants/entite'],
            [403, 'Consultant.mrose', '/soustraitants/entite'],
        ];
    }

    /**
     * @dataProvider dataAccessEntite
     */
    public function testAccessEntite($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessIndex() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            [200, 'Superadministrateur.superadmin', '/soustraitants/index'],
            [200, 'Administrateur.ibleu', '/soustraitants/index'],
            [200, 'DPO.nroux', '/soustraitants/index'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            [403, 'Rédacteur.rjaune', '/soustraitants/index'],
            [403, 'Valideur.cnoir', '/soustraitants/index'],
            [403, 'Consultant.mrose', '/soustraitants/index'],
        ];
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }

    public function dataAccessShow() {
        return [
            // 1. Utilisateurs pouvant accéder à la fonctionnalité
            // 1.1. Enregistrement existant
            [200, 'Superadministrateur.superadmin', '/soustraitants/show/1'],
            [200, 'Administrateur.ibleu', '/soustraitants/show/1'],
            [200, 'DPO.nroux', '/soustraitants/show/1'],
            // 1.2. Enregistrement inexistant
            [404, 'Superadministrateur.superadmin', '/soustraitants/show/666'],
            [404, 'Administrateur.ibleu', '/soustraitants/show/666'],
            [404, 'DPO.nroux', '/soustraitants/show/666'],
            // 2. Utilisateurs ne pouvant pas accéder à la fonctionnalité
            // 2.1. Enregistrement existant
            [403, 'Rédacteur.rjaune', '/soustraitants/show/1'],
            [403, 'Valideur.cnoir', '/soustraitants/show/1'],
            [403, 'Consultant.mrose', '/soustraitants/show/1'],
            // 2.2. Enregistrement inexistant
            [403, 'Rédacteur.rjaune', '/soustraitants/show/666'],
            [403, 'Valideur.cnoir', '/soustraitants/show/666'],
            [403, 'Consultant.mrose', '/soustraitants/show/666'],
        ];
    }

    /**
     * @dataProvider dataAccessShow
     */
    public function testAccessShow($expectedStatus, $user, $url, $options = []) {
        $this->assertActionAccess($expectedStatus, $user, $url, $options);
    }
}
