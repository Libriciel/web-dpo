# Liste des responsables créés et utilisés par entités

## Requête SQL

```sql
SELECT
        responsables.id AS "Id",
        responsables.raisonsocialestructure AS "Raison sociale",
        responsables.nomresponsable AS "Nom",
        responsables.prenomresponsable AS "Prénom",
        creator.raisonsociale AS "Créé par",
        organisations.raisonsociale AS "Utilisé par"
    FROM responsables
        LEFT OUTER JOIN organisations AS creator ON (creator.id = responsables.createdbyorganisation)
        LEFT OUTER JOIN responsables_organisations ON (responsables_organisations.responsable_id = responsables.id)
        LEFT OUTER JOIN organisations ON (organisations.id = responsables_organisations.organisation_id)
    ORDER BY responsables.raisonsocialestructure ASC;
```

## Résultats

|  Id |           Raison sociale           |     Nom      |  Prénom  |              Créé par              |  Utilisé par    |
| --- | ---                                | ---          | ---      | ---                                | ---             |
|   6 | Bordeaux Métropole                 | BLANC        | Jeanne   |                                    |  |
|  14 | Commune de Saint-Drézéry           | AMETHYSTE    | Daniel   | Libriciel SCOP                     | Libriciel SCOP |
|  12 | Commune de Saint-Georges-d'Orques  | AMARANTE     | Bernard  | Libriciel SCOP                     | Libriciel SCOP |
|  11 | Commune de Saint-Jean-de-Védas     | AMANDE       | Martine  | Libriciel SCOP                     |  |
|  10 | Commune de Saussan                 | ALEZAN       | Jacques  |                                    |  |
|   9 | Commune de Sussargues              | AIGUE-MARINE | Anne     |                                    |  |
|   8 | Commune de Vendargues              | ACAJOU       | René     |                                    | Libriciel SCOP |
|   5 | Eurométropole de Strasbourg        | GRENAT       | Sylvie   | Eurométropole de Strasbourg        |  |
|   1 | Libriciel SCOP                     | VERT         | Céline   | Libriciel SCOP                     |  |
|   3 | Métropole Européenne de Lille      | ROUGE        | Alain    | Métropole Européenne de Lille      |  |
|   4 | Metz Métropole                     | CYAN         | Isabelle | Metz Métropole                     |  |
|   2 | Montpellier Méditerranée Métropole | CREME        | Cédric   | Montpellier Méditerranée Métropole |  |
