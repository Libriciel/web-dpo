# Liste des services par entité

## Requête SQL

```sql
SELECT
        services.id AS "Id",
        services.libelle AS "Rôle",
        organisations.raisonsociale AS "Entité"
    FROM services
        LEFT OUTER JOIN organisations ON (organisations.id = services.organisation_id)
    ORDER BY organisations.raisonsociale ASC, services.id ASC;
```


## Entité « Libriciel SCOP »

|  Id |      Service      |               Entité                |
| --- | ---            | ---                                 |
|   1 | Direction Administration, Finances et Moyens Généraux | Libriciel SCOP |
|   2 | Direction du Développement                            | Libriciel SCOP |
|   3 | Direction Commerciale et Marketing                    | Libriciel SCOP |
|   4 | Direction Service Clients                             | Libriciel SCOP |

## Entité « Métropole Européenne de Lille »

|  Id |      Service      |               Entité                |
| --- | ---            | ---                                 |
|   9 | Service d'urbanisme                                   | Métropole Européenne de Lille |
|  10 | Service de voirie                                     | Métropole Européenne de Lille |
|  11 | Service des transports                                | Métropole Européenne de Lille |
|  12 | Service du traitement de l'eau                        | Métropole Européenne de Lille |

## Entité « Montpellier Méditerranée Métropole »

|  Id |      Service      |               Entité                |
| --- | ---            | ---                                 |
|   5 | Service d'urbanisme                                   | Montpellier Méditerranée Métropole |
|   6 | Service de voirie                                     | Montpellier Méditerranée Métropole |
|   7 | Service des transports                                | Montpellier Méditerranée Métropole |
|   8 | Service du traitement de l'eau                        | Montpellier Méditerranée Métropole |

