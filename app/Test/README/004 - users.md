# Liste des utilisateurs et de leur rôle par entité

## Requête SQL

```sql
SELECT
        users.id AS "Id",
        users.username AS "Identifiant",
        roles.libelle AS "Rôle",
        organisations.raisonsociale AS "Entité"
    FROM users
        LEFT OUTER JOIN organisations_users ON (organisations_users.user_id = users.id)
        LEFT OUTER JOIN organisations ON (organisations.id = organisations_users.organisation_id)
        LEFT OUTER JOIN organisation_user_roles ON (organisation_user_roles.organisation_user_id = organisations_users.id)
        LEFT OUTER JOIN roles ON (organisation_user_roles.role_id = roles.id)
    ORDER BY organisations.raisonsociale ASC, users.id ASC;
```

## Superadministrateurs

|  Id | Identifiant |      Rôle      |               Entité                |
| --- | ---         | ---            | ---                                 |
|   1 | superadmin  |                |  |
|   2 | superadmin2 |                |  |

## Entité « Eurométropole de Strasbourg »

|  Id | organisation_user_id    | Identifiant |      Rôle      |               Entité                |
| --- | --- | ---         | ---            | ---                                 |
|   7 |  5   | mrubis      | Administrateur | Eurométropole de Strasbourg |

## Entité « Libriciel SCOP »

|  Id | organisation_user_id    | Identifiant |      Rôle      |               Entité                |
| --- | --- | ---         | ---            | ---                                 |
|   3 | 1 | ibleu       | Administrateur | Libriciel SCOP |
|   8 | 6 | rjaune      | Rédacteur      | Libriciel SCOP |
|   9 | 7 | cnoir       | Valideur       | Libriciel SCOP |
|  10 | 8 | mrose       | Consultant     | Libriciel SCOP |
|  11 | 9 | nroux       | DPO            | Libriciel SCOP |

## Entité « Métropole Européenne de Lille »

|  Id | organisation_user_id | Identifiant |      Rôle      |               Entité                |
| --- | --- | ---         | ---            | ---                                 |
|   5 | 3 | jgris       | Administrateur | Métropole Européenne de Lille |
|  16 | 14 | sorange     | Rédacteur      | Métropole Européenne de Lille |
|  17 | 15 | plavande    | Valideur       | Métropole Européenne de Lille |
|  18 | 16 | mlilas      | Consultant     | Métropole Européenne de Lille |
|  19 | 17 | vpaille     | DPO            | Métropole Européenne de Lille |

## Entité « Metz Métropole »

|  Id | organisation_user_id | Identifiant |      Rôle      |               Entité                |
| --- | --- | ---         | ---            | ---                                 |
|   6 | 4 | lsepia      | Administrateur | Metz Métropole |

## Entité « Montpellier Méditerranée Métropole »

|  Id | organisation_user_id | Identifiant |      Rôle      |               Entité                |
| --- | --- | ---         | ---            | ---                                 |
|   4 | 2 | findigo     | Administrateur | Montpellier Méditerranée Métropole |
|  12 | 10 | pmagenta    | Rédacteur      | Montpellier Méditerranée Métropole |
|  13 | 11 | amauve      | Valideur       | Montpellier Méditerranée Métropole |
|  14 | 12 | mazur       | Consultant     | Montpellier Méditerranée Métropole |
|  15 | 13 | hvermeil    | DPO            | Montpellier Méditerranée Métropole |
