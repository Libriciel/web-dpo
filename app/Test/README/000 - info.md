# Tests

## Tests unitaires et d'intégration

```php
# @see https://stackoverflow.com/a/27442175
a="$(TZ=UTC0 printf '%(%s)T\n' '-1')"
./cake_utils.sh tests
elapsedseconds=$(( $(TZ=UTC0 printf '%(%s)T\n' '-1') - a ))
echo "${elapsedseconds}"
```

| Date        | Tests | Assertions | Incomplete | Skipped | Temps    |
| ---         | ---                | ---        | ---     | ---      |
| 15/06/2020  | 1180  | 1097 |     | 94         | 5       | 5m08,00s |

## Tests fonctionnels automatisés

| Date        | Scénarios en succès | Etapes en succès | Temps       |
| ---         | ---                 | ---              | ---         |
| 15/06/2020  | 342 / 342           | 2542 / 2542      | 15m32,19s   |
