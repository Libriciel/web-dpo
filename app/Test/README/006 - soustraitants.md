# Liste des sous-traitants créés et utilisés par entités

## Requête SQL

```sql
SELECT
        soustraitants.id AS "Id",
        soustraitants.raisonsociale AS "Raison sociale",
        creator.raisonsociale AS "Créé par",
        organisations.raisonsociale AS "Utilisé par"
    FROM soustraitants
        LEFT OUTER JOIN organisations AS creator ON (creator.id = soustraitants.createdbyorganisation)
        LEFT OUTER JOIN soustraitants_organisations ON (soustraitants_organisations.soustraitant_id = soustraitants.id)
        LEFT OUTER JOIN organisations ON (organisations.id = soustraitants_organisations.organisation_id)
    ORDER BY soustraitants.raisonsociale ASC;
```

## Résultats

|  Id |                     Raison sociale                     |    Créé par    |  Utilisé par    |
| --- | ---                                                    | ---            | ---             |
|   2 | Commission nationale de concertation                   |                | Libriciel SCOP |
|   1 | Commission nationale de l'informatique et des libertés |                |  |
|   4 | Commission nationale du débat public                   |                |  |
|   5 | Commune d'Assas                                        | Libriciel SCOP |  |
|   6 | Commune de Castelnau-le-Lez                            | Libriciel SCOP | Libriciel SCOP |
|   8 | Commune du Crès                                        | Libriciel SCOP | Libriciel SCOP |
