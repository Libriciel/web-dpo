# Liste des entités

## Requête SQL

```sql
SELECT
        organisations.id AS "Id",
        organisations.raisonsociale AS "Raison sociale"
    FROM organisations
    ORDER BY organisations.raisonsociale ASC;
```

|  Id |           Raison sociale            |
| --- | --- |
|   5 | Eurométropole de Strasbourg |
|   1 | Libriciel SCOP |
|   3 | Métropole Européenne de Lille |
|   4 | Metz Métropole |
|   2 | Montpellier Méditerranée Métropole |
