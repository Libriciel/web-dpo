# Liste des types d'annexes créés et utilisés par entités

## Requête SQL

```sql
SELECT
        typages.id AS "Id",
        typages.libelle AS "Libellé",
        creator.raisonsociale AS "Créé par",
        organisations.raisonsociale AS "Utilisé par"
    FROM typages
        LEFT OUTER JOIN organisations AS creator ON (creator.id = typages.createdbyorganisation)
        LEFT OUTER JOIN typages_organisations ON (typages_organisations.typage_id = typages.id)
        LEFT OUTER JOIN organisations ON (organisations.id = typages_organisations.organisation_id)
    ORDER BY typages.libelle ASC;
```

## Résultats

|  Id |       Libellé       |    Créé par    |  Utilisé par    |
| --- | ---                 | ---            | ---             |
|   3 | AIPD                |                |  |
|   4 | Autre               |                |  |
|   2 | Co-responsabilité   |                | Libriciel SCOP |
|   6 | Non utilisé général | Metz Métropole |  |
|   8 | Non utilisé LS      | Libriciel SCOP |  |
|   1 | Sous-traitance      |                | Libriciel SCOP |
