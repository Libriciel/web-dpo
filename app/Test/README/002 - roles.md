# Liste des rôles par entité

## Requête SQL

```sql
SELECT
        roles.id AS "Id",
        roles.libelle AS "Rôle",
        organisations.raisonsociale AS "Entité"
    FROM roles
        LEFT OUTER JOIN organisations ON (organisations.id = roles.organisation_id)
    ORDER BY organisations.raisonsociale ASC, roles.id ASC;
```

## Entité « Eurométropole de Strasbourg »

|  Id |      Rôle      |               Entité                |
| --- | ---            | ---                                 |
|  21 | Rédacteur      | Eurométropole de Strasbourg |
|  22 | Valideur       | Eurométropole de Strasbourg |
|  23 | Consultant     | Eurométropole de Strasbourg |
|  24 | Administrateur | Eurométropole de Strasbourg |
|  25 | DPO            | Eurométropole de Strasbourg |

## Entité « Libriciel SCOP »

|  Id |      Rôle      |               Entité                |
| --- | ---            | ---                                 |
|   1 | Rédacteur      | Libriciel SCOP |
|   2 | Valideur       | Libriciel SCOP |
|   3 | Consultant     | Libriciel SCOP |
|   4 | Administrateur | Libriciel SCOP |
|   5 | DPO            | Libriciel SCOP |
|  31 | Lecteur        | Libriciel SCOP |

## Entité « Métropole Européenne de Lille »

|  Id |      Rôle      |               Entité                |
| --- | ---            | ---                                 |
|  11 | Rédacteur      | Métropole Européenne de Lille |
|  12 | Valideur       | Métropole Européenne de Lille |
|  13 | Consultant     | Métropole Européenne de Lille |
|  14 | Administrateur | Métropole Européenne de Lille |
|  15 | DPO            | Métropole Européenne de Lille |
|  33 | Lecteur        | Métropole Européenne de Lille |

## Entité « Metz Métropole »

|  Id |      Rôle      |               Entité                |
| --- | ---            | ---                                 |
|  16 | Rédacteur      | Metz Métropole |
|  17 | Valideur       | Metz Métropole |
|  18 | Consultant     | Metz Métropole |
|  19 | Administrateur | Metz Métropole |
|  20 | DPO            | Metz Métropole |

## Entité « Montpellier Méditerranée Métropole »

|  Id |      Rôle      |               Entité                |
| --- | ---            | ---                                 |
|   6 | Rédacteur      | Montpellier Méditerranée Métropole |
|   7 | Valideur       | Montpellier Méditerranée Métropole |
|   8 | Consultant     | Montpellier Méditerranée Métropole |
|   9 | Administrateur | Montpellier Méditerranée Métropole |
|  10 | DPO            | Montpellier Méditerranée Métropole |
|  32 | Lecteur        | Montpellier Méditerranée Métropole |
