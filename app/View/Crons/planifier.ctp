<?php
echo $this->Html->script([
    'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
    'smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js'
]);
echo $this->Html->css('/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min');

$breadcrumbs = [
    __d('cron', 'cron.titreListeTachesAutomatiques') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

echo $this->WebcilForm->create('Cron', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->input('nextexecutiontime',[
            'id' => 'nextexecutiontime',
            'value' => $tacheAuto['Cron']['next_execution_time'],
            'required' => true
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?php
        $delaiexecution = $this->WebcilForm->inputs([
            'jours_execution_duration' => [
                'id' => 'jours_execution_duration',
                'class' => 'usersDeroulant transformSelect form-control',
                'data-placeholder' => " ",
                'options' => $options['jours'],
                'value' => $execution_duration['day'],
                'empty' => true
            ],
            'heures_execution_duration' => [
                'id' => 'heures_execution_duration',
                'class' => 'usersDeroulant transformSelect form-control',
                'data-placeholder' => " ",
                'options' => $options['heures'],
                'value' => $execution_duration['hour'],
                'empty' => true
            ],
            'minutes_execution_duration' => [
                'id' => 'minutes_execution_duration',
                'class' => 'usersDeroulant transformSelect form-control',
                'data-placeholder' => " ",
                'options' => $options['minutes'],
                'value' => $execution_duration['minute'],
                'empty' => true
            ]
        ]);

        echo $this->Html->tag(
            'div',
            '<strong>'.
            __d('cron', 'cron.titreInfoDelaiEntreChaqueExecution') .
            '</strong>' . $delaiexecution,
            ['class' => 'alert alert-info']
        );

        echo $this->WebcilForm->hidden('id', [
            'value' => $tacheAuto['Cron']['id']
        ]);
        ?>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    
    $(document).ready(function () {
    
        $(function () {
            $('#nextexecutiontime').datetimepicker({
                language: 'fr',
                inline: true,
                sideBySide: true,
                autoclose: true
            });
        });
        
    });
    
</script>
