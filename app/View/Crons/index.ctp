<?php
$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
?>

<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <!-- Etat -->
            <th class="col-md-1">
                <?php 
                echo __d('cron', 'cron.titreEtat');
                ?>
            </th>
            
            <!-- Nom -->
            <th class="col-md-3">
                <?php 
                echo __d('cron', 'cron.titreNom');
                ?>
            </th>
            
            <!-- Date d'exécution prévue -->
            <th class="col-md-2">
                <?php 
                echo __d('cron', 'cron.titreDateExecutionPrevue');
                ?>
            </th>
            
            <!-- Délai entre 2 exécutions -->
            <th class="col-md-1">
                <?php 
                echo __d('cron', 'cron.titreDelaiEntreExecution');
                ?>
            </th>
            
            <!-- Date dernière exécution -->
            <th class="col-md-1">
                <?php 
                echo __d('cron', 'cron.titreDateDerniereExecution');
                ?>
            </th>
            
            <!-- Status -->
            <th class="col-md-2">
                <?php 
                echo __d('cron', 'cron.titreStatus');
                ?>
            </th>
            
            <!-- Actions -->
            <th class="col-md-2">
                <?php 
                echo __d('cron', 'cron.titreActions');
                ?>
            </th>
        </tr>
    </thead>
    
    <!-- Info tableau -->
    <tbody>
        <?php
        foreach ($tachesAutos as $tacheAuto){
            if ($tacheAuto['Cron']['lock']) {
                $btndeverrouiller = $this->Html->link('<i class="fa fa-unlock-alt fa-lg"><!----></i>', [
                    'controller' => 'crons',
                    'action' => 'deverrouiller',
                    $tacheAuto['Cron']['id']
                        ], [
                    'class' => 'btn btn-danger btn-sm my-tooltip',
                    'title' => __d('cron', 'cron.commentaireDeverrouillerTacheAuto'),
                    'escape' => false
                ]);
            } else {
                switch ($tacheAuto['Cron']['last_execution_status']) {
                    case 'LOCKED':
                        $tacheAuto['Cron']['statusLibelle'] = '<span class="label label-info" title="' . __('La tâche est verrouillée, ce qui signifie qu\'elle est en cours d\'exécution ou dans un état bloqué suite à une erreur') . '"><i class="fa fa-lock"><!----></i> ' . __('Verrouillée') . '</span>';
                        break;
                    case 'SUCCES':
                        $tacheAuto['Cron']['statusLibelle'] = '<span class="label label-success" title="' . __('Opération exécutée avec succès') . '"><i class="fa fa-check"><!----></i> ' . __('Exécutée avec succès') . '</span>';
                        break;
                    case 'WARNING':
                        $tacheAuto['Cron']['statusLibelle'] = '<span class="label label-warning" title="' . __('Avertissement(s) détecté(s) lors de l\'exécution, voir les détails de la tâche') . '"><i class="fa fa-info"><!----></i> ' . __('Exécutée, en alerte') . '</span>';
                        break;
                    case 'FAILED':
                        $tacheAuto['Cron']['statusLibelle'] = '<span class="label label-danger" title="' . __('Erreur(s) détectée(s) lors de l\'exécution, voir les détails de la tâche') . '"><i class="fa fa-warning"><!----></i> ' . __('Non exécutée : erreur') . '</span>';
                        break;
                    default:
                        $tacheAuto['Cron']['statusLibelle'] = '<span class="label label-default" title="' . __('La tâche n\'a jamais été exécutée') . '">' . __('En attente') . '</span>';
                }
            }
            ?>

            <tr class="d-flex">
                <!-- Etat -->
                <td class="tdleft col-md-1">
                    <?php
                    if ($tacheAuto['Cron']['active'] == true) {
                        $titleBtn = __d('cron', 'cron.commentaireDesactiverTacheAuto');
                    } else {
                        $titleBtn = __d('cron', 'cron.commentaireActiverTacheAuto');
                    }

                    echo $this->element('Buttons/toggle', [
                        'controller' => 'crons',
                        'id' => $tacheAuto['Cron']['id'],
                        'active' => $tacheAuto['Cron']['active'],
                        'titleBtn' => $titleBtn
                    ]);
                    ?>
                </td>
                
                <!-- Nom -->
                <td class="tdleft col-md-3">
                    <?php
                    echo $tacheAuto['Cron']['nom'];
                    ?>
                </td>
                
                <!-- Date d'exécution prévue -->
                <td class="tdleft col-md-2">
                    <?php
                    echo $this->Time->format($tacheAuto['Cron']['next_execution_time'], FORMAT_DATE_HEURE);
                    ?>
                </td>
                
                <!-- Délai entre 2 exécutions -->
                <td class="tdleft col-md-1">
                    <?php
                    $execution_duration = AppTools::durationToArray($tacheAuto['Cron']['execution_duration']);

                    $delais = null;
                    if ($execution_duration['day'] != null) {
                        $delais = $execution_duration['day'] . 'j ';
                    }

                    if ($execution_duration['hour'] != null) {
                        $delais .= $execution_duration['hour'] . 'h ';
                    }

                    if ($execution_duration['minute'] != null) {
                        $delais .= $execution_duration['minute'] . 'min';
                    }

                    echo $delais;
                    ?>
                </td>
                
                <!-- Date dernière exécution -->
                <td class="tdleft col-md-1">
                    <?php
                    echo $this->Time->format($tacheAuto['Cron']['last_execution_start_time'], FORMAT_DATE_HEURE);
                    ?>
                </td>
                
                <!-- Status -->
                <td class="tdleft col-md-2">
                    <?php
                    if (!empty($btndeverrouiller)) {
                        echo $btndeverrouiller;
                    } else {
                        echo $tacheAuto['Cron']['statusLibelle'];
                    }
                    ?>
                </td>

                <!-- Actions -->
                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        if ($tacheAuto['Cron']['active'] == false) {
                            echo $this->Html->link('<i class="fa fa-clock fa-lg"></i>', [
                                'controller' => 'crons',
                                'action' => 'planifier',
                                $tacheAuto['Cron']['id']
                                    ], [
                                'class' => 'btn btn-outline-primary borderless',
                                'title' => __d('cron', 'cron.commentairePlanifierTacheAuto'),
                                'escape' => false
                            ]);
                        } else {
                            echo $this->Html->link('<i class="fa fa-cog fa-lg"></i>', [
                                'controller' => 'crons',
                                'action' => 'executer',
                                $tacheAuto['Cron']['id']
                                    ], [
                                'class' => 'btn btn-outline-primary borderless',
                                'title' => __d('cron', 'cron.commentaireExecuterTacheAuto'),
                                'escape' => false
                            ]);
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
