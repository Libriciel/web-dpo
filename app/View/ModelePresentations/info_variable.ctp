<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);
?>

<h3>
    <?php
    echo("Informations propres au paramétrage de l'organisation");
    ?>
</h3>

<hr/>

<?php
echo $this->element('Modele/tableVariablesEntite');

echo $this->element('Modele/tableVariablesResponsableEntite');

echo $this->element('Modele/tableVariablesDPO');
?>

<table class="table">
    <thead>
        <tr>
            <th class="thleft col-md-5">
                <?php
                echo __d('modele_presentation', 'modele_presentation.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele_presentation', 'modele_presentation.titreTableauNomVariable');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele_presentation', 'modele_presentation.titreTableauValeur');
                ?>
            </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td class="tdleft">
                <?php
                echo __d('modele_presentation', 'modele_presentation.textTableauTitreDocumentGenerer');
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo "titre";
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo TITRE_REGISTRE;
                echo "<br>OU<br>";
                echo TITRE_EXTRAIT_REGISTRE;
                ?>
            </td>
        </tr>

        <tr>
            <td class="tdleft">
                <?php
                echo __d('modele_presentation', 'modele_presentation.textTableauTitreCompletDocument');
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo "titre_complet";
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo TITRE_COMPLET_REGISTRE;
                echo "<br>OU<br>";
                echo TITRE_COMPLET_EXTRAIT_REGISTRE;
                ?>
            </td>
        </tr>
    </tbody>
</table>

<br/>

<h3>
    <?php
    echo("Traitements générés");
    ?>
</h3>

<hr/>

<!-- Texte section warning -->
<div class="alert alert-warning" role="alert">
    <?php
    echo __d('modele_presentation', 'modele_presentation.textInformationSectionTraitementGenere');
    ?>
</div>

<table class="table">
    <thead>
    <tr>
        <th class="thleft col-md-10">
            <?php
            echo __d('modele_presentation', 'modele_presentation.titreTableauNomChamp');
            ?>
        </th>

        <th class="thleft col-md-10">
            <?php
            echo __d('modele_presentation', 'modele_presentation.titreTableauNomVariable');
            ?>
        </th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td class="tdleft">
            <?php
            echo __d('modele_presentation', 'modele_presentation.textTableauTraitementsGeneres');
            ?>
        </td>

        <td class="tdleft">
            <?php
            echo "traitement_genere";
            ?>
        </td>
    </tr>
    </tbody>
</table>

<!-- Bouton Retour -->
<div class="row">
    <div class="col-md-12 top17 text-center">
        <div class="buttons">
            <?php
            echo $this->Html->link('<i class="fa fa-arrow-left fa-lg"></i>' . __d('default', 'default.btnBack'), $referer, [
                'class' => 'btn btn-outline-primary',
                'escape' => false
            ]);
            ?>
        </div>
    </div>
</div>
