<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);
?>

<!-- Tableau modèles de présentation -->
<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <th class="col-md-5">
                <?php 
                echo __d('modele_presentation', 'modele_presentation.titreTableauModele');
                ?>
            </th>
            
            <th class="col-md-5">
                <?php 
                echo __d('modele_presentation', 'modele_presentation.titreTableauFichierModele');
                ?>
            </th>
            
            <th class="col-md-2">
                <?php 
                echo __d('modele_presentation', 'modele_presentation.titreTableauActions');
                ?>
            </th>
        </tr>
    </thead>
    
    <!-- Info tableau -->
    <tbody>
        <tr class="d-flex">
            <td class="tdleft col-md-5">
                <?php
                echo __d('modele_presentation', 'modele_presentation.textTableauPresentationModele');
                ?>
            </td>
        
            <?php
            if (!empty($modelesPresentation)) {
                foreach ($modelesPresentation as $modelePresentation){
                    ?>
                    <!-- Nom du fichier -->
                    <td class="tdleft col-md-5">
                        <i class="far fa-file-alt fa-lg fa-fw"></i>
                        <?php
                            echo $modelePresentation['name_modele'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/download', [
                                'controller' => 'modelePresentations',
                                'fichier' => $modelePresentation['fichier'],
                                'name_modele' => $modelePresentation['name_modele'],
                                'titleBtn' => __d('modele_presentation', 'modele_presentation.commentaireTelechargerModel')
                            ]);

                            echo $this->element('Buttons/delete', [
                                'controller' => 'modelePresentations',
                                'id' => $modelePresentation['fichier'],
                                'titleBtn' => __d('modele_presentation', 'modele_presentation.commentaireSupprimerModel'),
                                'confirmation' => __d('modele_presentation', 'modele_presentation.confirmationSupprimerModel')
                            ]);

                            echo $this->element('Buttons/infoVariable', [
                                'controller' => 'modelePresentations',
                                'titleBtn' => __d('modele_presentation', 'modele_presentation.commentaireVariableModel'),
                            ]);
                            ?>
                        </div>
                    </td>
                    <?php
                }
            } else {
                ?>
                <!-- Aucun modèle pour ce formulaire -->
                <td class="tdleft col-md-5">
                    <?php
                    echo __d('modele_presentation', 'modele_presentation.textTableauAucunModele');
                    ?>
                </td>

                <!-- Actions -->
                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        echo $this->element('Buttons/upload', [
                            'dataTarget' => '#modalUploadModelepresentation',
                            'titleBtn' => __d('modele_presentation', 'modele_presentation.commentaireImporterModel')
                        ]);

                        echo $this->element('Buttons/infoVariable', [
                            'controller' => 'modelePresentations',
                            'titleBtn' => __d('modele_presentation', 'modele_presentation.commentaireVariableModel'),
                        ]);
                        ?>
                    </div>
                </td>
                <?php
            }
            ?>
        </tr>
    </tbody>
</table>

<?php
echo $this->element('Default/modalFileImport', [
    'modal_id' => 'modalUploadModelepresentation',
    'titleModal' => __d('modele_presentation', 'modele_presentation.popupEnvoiModele'),
    'controller' => 'modelePresentation',
]);
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');
        
    });

</script>
