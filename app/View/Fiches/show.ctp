<?php
$breadcrumbs = [
    $title => []
];
if (!empty($updateBreadcrumbs)) {
    $breadcrumbs += $updateBreadcrumbs;
}
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

echo $this->WebcilForm->create('Fiche', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false && $usePIA['Formulaire']['usepia'] === true) {
            ?>
            <!-- Onglet AIPD -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#info_pia">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletPia');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_rt_externe">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletInformationResponsableTraitement');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <!-- Onglet Information générale -->
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#information_traitement">
                <i class="fas fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletInformationTraitement');
                ?>
            </a>
        </li>

        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if (in_array(true, $useFieldsFormulaire['Formulaire'], true)) {
                if ($useFieldsFormulaire['Formulaire']['usesousfinalite'] === true && !empty($this->request->data['WebdpoFiche']['sousFinalite']) ||
                    $useFieldsFormulaire['Formulaire']['usebaselegale'] === true && !empty($this->request->data['WebdpoFiche']['baselegale']) ||
                    $useFieldsFormulaire['Formulaire']['usedecisionautomatisee'] === true && !empty($this->request->data['WebdpoFiche']['decisionAutomatisee']) ||
                    $useFieldsFormulaire['Formulaire']['usetransferthorsue'] === true && !empty($this->request->data['WebdpoFiche']['horsue']) ||
                    $useFieldsFormulaire['Formulaire']['usedonneessensible'] === true && !empty($this->request->data['WebdpoFiche']['donneessensibles'])
                ) {
                    ?>
                    <!-- Onglet Information complémentaire -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#information_complementaire">
                            <i class="fas fa-eye fa-fw"></i>
                            <?php
                            echo __d('fiche', 'fiche.ongletInformationComplementaire');
                            ?>
                        </a>
                    </li>
                    <?php
                }
            }
        }
        ?>

        <!-- Onglet formulaire -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#info_formulaire">
                <i class="fas fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletFormulaire');
                ?>
            </a>
        </li>

        <?php
        if ($this->request->data['Fiche']['coresponsable'] === true) {
            ?>
            <!-- Onglet Co-responsable -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ongletComplementaireCoresponsable">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletCoresponsable');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_st_organisation">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletST');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->request->data['Fiche']['soustraitance'] === true) {
            ?>
            <!-- Onglet Sous-traitance-->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ongletComplementaireSoustraitance">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    if ($rt_externe === false) {
                        echo __d('fiche', 'fiche.ongletSousTraitant');
                    } else {
                        echo __d('fiche', 'fiche.ongletSousTraitanceUlterieur');
                    }
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <!-- Onglet Annexe -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fas fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletAnnexe');
                ?>
            </a>
        </li>

        <?php
        if ($showRegistre == true && $rt_externe === false) {
            ?>
            <!-- Onglet Information sur l'entité -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_rt_organisation">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletEntiteResponsableTraitement');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>

    <div class="tab-content">
        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if ($usePIA['Formulaire']['usepia'] === true) {
                echo $this->element("Fiches/tabs/pia", [
                    'classActivePia' => ''
                ]);
            }
        }

        if ($rt_externe === true) {
            echo $this->element('Fiches/tabs/rt_externe', [
                'classActiveRT' => ''
            ]);
        }

        echo $this->element('Fiches/tabs/informationGenerale', [
            'classActiveInfoGenerale' => 'active'
        ]);

        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if (in_array(true, $useFieldsFormulaire['Formulaire'], true)) {
                if ($useFieldsFormulaire['Formulaire']['usesousfinalite'] === true && !empty($this->request->data['WebdpoFiche']['sousFinalite']) ||
                    $useFieldsFormulaire['Formulaire']['usebaselegale'] === true && !empty($this->request->data['WebdpoFiche']['baselegale']) ||
                    $useFieldsFormulaire['Formulaire']['usedecisionautomatisee'] === true && !empty($this->request->data['WebdpoFiche']['decisionAutomatisee']) ||
                    $useFieldsFormulaire['Formulaire']['usetransferthorsue'] === true && !empty($this->request->data['WebdpoFiche']['horsue']) ||
                    $useFieldsFormulaire['Formulaire']['usedonneessensible'] === true && !empty($this->request->data['WebdpoFiche']['donneessensibles'])
                ) {
                    // Onglet information complémentaire
                    echo $this->element("Fiches/tabs/informationComplementaire");
                }
            }
        }
        ?>

        <!-- Onglet Formulaire -->
        <div id="info_formulaire" class="tab-pane">
            <br>
            <div id="form-container-formulaire" class="form-container col-md-12 row">
                <?php
                echo $this->element('Fiches/tabs/formulaire', [
                    'champs' => $fields['formulaire']
                ]);
                ?>
            </div>
        </div>
        <!-- Fin onglet Formulaire -->

        <?php
        if ($this->request->data['Fiche']['coresponsable'] === true) {
            // Onglet Co-responsable
            echo $this->element('Fiches/tabs/coresponsable');
            // Fin onglet Co-responsable
        }

        if ($rt_externe === true) {
            echo $this->element('Fiches/tabs/st_organisation');
        }

        if ($this->request->data['Fiche']['soustraitance'] === true) {
            // Onglet soustraitance
            echo $this->element('Fiches/tabs/soustraitance');
            // Fin onglet soustraitance
        }

        // Onglet Annexe(s)
        echo $this->element('Fiches/tabs/annexe');
        // Fin onglet Annexe(s)

        if ($showRegistre === true && $rt_externe === false) {
            // Onglet Information sur l'entité
//            echo $this->element('Fiches/tabs/informationEntite');
            echo $this->element('Fiches/tabs/rt_organisation');
        }
        ?>

    </div>

    <div class="row">
        <div class="col-md-12 top17 text-center">
            <div class="buttons">
                <?php
                echo $this->Html->link('<i class="fa fa-arrow-left fa-lg"></i>' . __d('fiche', 'fiche.btnRevenir'), $referer, [
                    'class' => 'btn btn-outline-primary',
                    'escape' => false
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();
?>

<script type="text/javascript">

    $(document).ready(function () {

        $(":input").prop("disabled", true);

        $('.btn').prop("disabled", false);
        $('.close').prop("disabled", false);

        $('.popoverText').popover({
            delay: {
                show: 500,
                hide: 100
            },
            placement: 'right',
            trigger: 'hover',
            container: 'body',
            html: true,
        });

    });

</script>
