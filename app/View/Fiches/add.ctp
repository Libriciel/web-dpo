<?php
echo $this->Html->script([
    'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
    'smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js',
]);
echo $this->Html->css('/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min');

$breadcrumbs = [
    $title => []
];
if (!empty($updateBreadcrumbs)) {
    $breadcrumbs += $updateBreadcrumbs;
}
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

$validationErrorsFichier = null;

if ($initialisationMode === false) {
    echo $this->element("Fiches/validationErrors/add");
} else {
    echo $this->element("Fiches/validationErrors/initialisation");
}

echo $this->WebcilForm->create('Fiche', [
    'type' => 'file',
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);

$classActiveRT = '';
$classActiveInfoGenerale = '';
$classActivePia = '';

if ($rt_externe === true) {
    $classActiveRT = 'active';
} else {
    $classActiveInfoGenerale = 'active';
}

if ($this->request->params['action'] === 'add' && $usePIA['Formulaire']['usepia'] === true) {
    $classActiveRT = '';
    $classActiveInfoGenerale = '';
    $classActivePia = 'active';
}

$fieldsIsRequired = true;
if ($initialisationMode === true) {
    $fieldsIsRequired = false;
}
?>

<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if ($usePIA['Formulaire']['usepia'] === true) {
                ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo $classActivePia ?>" data-toggle="tab" href="#info_pia">
                        <i class="fas fa-pencil-alt fa-fw"></i>
                        <?php
                        echo __d('fiche', 'fiche.ongletPia');
                        ?>
                    </a>
                </li>
                <?php
            }
        }

        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link <?php echo $classActiveRT ?>" data-toggle="tab" href="#onglet_rt_externe">
                    <i class="fas fa-pencil-alt fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletInformationResponsableTraitement');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link <?php echo $classActiveInfoGenerale ?>" data-toggle="tab" href="#information_traitement">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletInformationTraitement');
                ?>
            </a>
        </li>

        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if (in_array(true, $useFieldsFormulaire['Formulaire'], true)) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#information_complementaire">
                        <i class="fas fa-pencil-alt fa-fw"></i>
                        <?php
                        echo __d('fiche', 'fiche.ongletInformationComplementaire');
                        ?>
                    </a>
                </li>
                <?php
            }
        }
        ?>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#info_formulaire">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletFormulaire');
                ?>
            </a>
        </li>

        <li class="nav-item" id="information_coresponsable">
            <a class="nav-link disabled" data-toggle="tab" id="aOngletComplementaireCoresponsable" href="#ongletComplementaireCoresponsable">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletCoresponsable');
                ?>
            </a>
        </li>

        <?php
        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_st_organisation">
                    <i class="fas fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletST');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item" id="information_soustraitance">
            <a class="nav-link disabled" data-toggle="tab" id="aOngletComplementaireSoustraitance" href="#ongletComplementaireSoustraitance">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                if ($rt_externe === false) {
                    echo __d('fiche', 'fiche.ongletSousTraitant');
                } else {
                    echo __d('fiche', 'fiche.ongletSousTraitanceUlterieur');
                }
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletAnnexe');
                ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <?php
        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if ($usePIA['Formulaire']['usepia'] === true) {
                echo $this->element("Fiches/tabs/pia", [
                    'classActivePia' => $classActivePia,
                    'fieldsIsRequired' => $fieldsIsRequired
                ]);
            }
        }

        if ($rt_externe === true) {
            echo $this->element('Fiches/tabs/rt_externe', [
                'classActiveRT' => $classActiveRT,
                'fieldsIsRequired' => $fieldsIsRequired
            ]);
        }

        // Onglet Information Générale
        echo $this->element('Fiches/tabs/informationGenerale', [
            'classActiveInfoGenerale' => $classActiveInfoGenerale,
            'fieldsIsRequired' => $fieldsIsRequired
        ]);
        // Fin onglet Information Générale

        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if (in_array(true, $useFieldsFormulaire['Formulaire'], true)) {
                // Onglet Information complémentaire
                echo $this->element("Fiches/tabs/informationComplementaire", [
                    'fieldsIsRequired' => $fieldsIsRequired
                ]);
                // Fin onglet Information complémentaire
            }
        }
        ?>

        <!-- Onglet Formulaire -->
        <div id="info_formulaire" class="tab-pane">
            <br>
            <div id="form-container-formulaire" class="form-container col-md-12 row">
                <?php
                echo $this->element('Fiches/tabs/formulaire', [
                    'champs' => $fields['formulaire'],
                    'fieldsIsRequired' => $fieldsIsRequired
                ]);
                ?>
            </div>
        </div>
        <!-- Fin onglet Formulaire -->

        <?php
        // Onglet Co-responsable
        echo $this->element('Fiches/tabs/coresponsable');
        // Fin onglet Co-responsable

        if ($rt_externe === true) {
            echo $this->element('Fiches/tabs/st_organisation');
        }

        // Onglet soustraitance
        echo $this->element('Fiches/tabs/soustraitance');
        // Fin onglet soustraitance

        // Onglet Annexe(s)
        echo $this->element('Fiches/tabs/annexe');
        // Fin onglet Annexe(s)
        ?>
    </div>

    <div class="row">
        <!-- Groupe bouton -->
        <div class="col-md-12 top17 text-center">
            <div class="buttons">
                <?php
                // Groupe de boutons
                echo $this->WebcilForm->buttons(['Cancel', 'Save']);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();
?>

<script type="text/javascript">

    $(document).ready(function () {

        // Mise en évidence des onglets ayant des erreurs.
        $('div.form-group .invalid-feedback').closest('div.tab-pane').each(function(idx, pane) {
            let a = $( "a[href='#"+$(pane).attr('id')+"']" );
            $(a).addClass("is-invalid");
            $(a).append("<i class='fa fa-exclamation-circle fa-danger' aria-hidden='true'></i>");
        });

        let errorAnnexe = <?php echo json_encode($validationErrorsFichier); ?>;
        if (errorAnnexe && errorAnnexe.length !== 0) {
            let aAnnexe = $('a[href="#annexe"]');
            $(aAnnexe).parent().addClass("is-invalid");
            $(aAnnexe).append("<i class='fa fa-exclamation-circle fa-danger' aria-hidden='true'></i>");
        }

        // Mask champs
        // Information sur le rédacteur
        $('#declarantpersonneportable').mask('00 00 00 00 00', {
            placeholder: '__ __ __ __ __'
        });
        $('#declarantpersonnefix').mask('00 00 00 00 00', {
            placeholder: '__ __ __ __ __'
        });

        let coresponsable = $("#coresponsable").val();
        displayTabCoresponsables(coresponsable);

        $("#coresponsable").change(function () {
            idCoresponsable = $(this).val();

            displayTabCoresponsables(idCoresponsable);
        });

        let sousTraitance = $("#soustraitance").val();
        displayTabSousTraitances(sousTraitance);

        $("#soustraitance").change(function () {
            let newValSousTraitance = $(this).val();

            displayTabSousTraitances(newValSousTraitance);
        });

        $('.popoverText').popover({
            delay: {
                show: 500,
                hide: 100
            },
            placement: 'right',
            trigger: 'hover',
            container: 'body',
            html: true,
        });

    });

    function displayTabCoresponsables(val){
        if (val == true) {
            $('#aOngletComplementaireCoresponsable').removeClass('disabled');

        } else {
            $('#aOngletComplementaireCoresponsable').addClass('disabled');
        }
    }

    function displayTabSousTraitances(val){
        if (val == true) {
            $('#aOngletComplementaireSoustraitance').removeClass('disabled');
        } else {
            $('#aOngletComplementaireSoustraitance').addClass('disabled');
        }
    }

</script>
