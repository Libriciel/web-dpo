<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);
?>

<!--Bouton de création d'un modele -->
<div class="row text-left">
    <div class="col-md-12 text-left">
        <button id="btnAddModeleToForm" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddModele">
            <span class="fa fa-plus fa-lg"></span>
            <?php
            echo __d('modele', 'modele.btnCreerModele');
            ?>
        </button>
    </div>
</div>

<?php
$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

if (!empty($modeles)) {
    echo $pagination;

    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('modele', 'modele.popupTitreAffecterModeleFormulaire')
    ]);
    ?>

    <!-- Tableau modèles pour l'extrait de registre -->
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1"></th>

                <th class="col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauFichierModele');
                    ?>
                </th>

                <th class="col-md-3">
                    <?php 
                        echo __d('modele', 'modele.titreTableauFormulaire');
                    ?>
                </th>
                
                <!-- Entité -->
                <th class="col-md-3">
                    <?php echo __d('typage', 'typage.titreTableauEntite'); ?>
                </th>
                
                <th class="col-md-2">
                    <?php 
                        echo __d('modele', 'modele.titreTableauActions');
                    ?>
                </th>
            </tr>
        </thead>

        <!-- Info tableau -->
        <tbody>
            <?php
            foreach ($modeles as $key => $value) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer -->
                    <td class="tdleft col-md-1">
                        <?php
                        if (array_key_exists($value['Formulaire']['id'], $mesFormulaires)) {
                            ?>
                            <input id='checkbox<?php echo $value['Modele']['id']; ?>' type="checkbox"
                                   class="modeleCheckbox" value="<?php echo $value['Modele']['id']; ?>" >
                            <?php
                        }
                        ?>
                    </td>

                    <?php
                    if ($value['Modele']['fichier'] != null) {
                        ?>
                        <!-- Nom du fichier -->
                        <td class="tdleft col-md-3">
                            <i class="far fa-file-alt fa-lg fa-fw"></i>
                            <?php
                            echo $value['Modele']['name_modele'];
                            ?>
                        </td>
                        <?php
                    }   else {
                        ?>
                        <!-- Aucun modèle pour ce formulaire -->
                        <td class="tdleft col-md-3">
                            <?php
                            echo __d('modele', 'modele.textTableauAucunModele');
                            ?>
                        </td>
                        <?php
                    }
                    ?>

                    <!-- Nom du formulaire -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $value['Formulaire']['libelle'];
                        ?>
                    </td>

                    <!-- Entité -->
                    <td class="tdleft col-md-3">
                        <?php
                        $organisations = Hash::extract($value, 'FormulaireModeleOrganisation.{n}.Organisation.id');
                        if (empty($organisations) === false) {
                            echo '<ul><li>' . implode('</li><li>', Hash::extract($value, 'FormulaireModeleOrganisation.{n}.Organisation.raisonsociale')) . '</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if ($value['Modele']['fichier'] != null) {
                                echo $this->element('Buttons/download', [
                                    'controller' => 'modeles',
                                    'fichier' => $value['Modele']['fichier'],
                                    'name_modele' => $value['Modele']['name_modele'],
                                    'titleBtn' => __d('modele', 'modele.commentaireTelechargerModel')
                                ]);
                            }

                            if (empty($organisations) === true) {
                                echo $this->element('Buttons/delete', [
                                    'controller' => 'modeles',
                                    'id' => $value['Modele']['id'],
                                    'titleBtn' => __d('modele', 'modele.commentaireSupprimerModel'),
                                    'confirmation' => __d('modele', 'modele.confirmationSupprimerModel')
                                ]);
                            }

                            echo $this->element('Buttons/infoVariable', [
                                'controller' => 'modeles',
                                'id' => $value['Formulaire']['id'],
                                'titleBtn' => __d('modele', 'modele.commentaireVariableModel'),
                            ]);
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
                }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('modele', 'modele.textAucunModele');
            ?>
        </h3>
    </div>
    <?php
}
?>

<!--Bouton de création d'un modele -->
<div class="row text-center">
    <div class="col-md-12 text-center">
        <button id="btnAddModeleToForm" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddModele">
            <span class="fa fa-plus fa-lg"></span>
            <?php
            echo __d('modele', 'modele.btnCreerModele');
            ?>
        </button>
    </div>
</div>

<?php
echo $this->element('Modele/modalAddModeleToFormulaireToEntity', [
    'controller' => 'Modele',
]);

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Modele',
    'modalTitle' => __d('modele', 'modele.popupTitreAffecterModeleFormulaire'),
    'formulaireName' => 'FormulaireModeleOrganisation',
    'fieldName' => 'modele_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'modeleCheckbox',
    'justOneCheckbox' => true,
    'getOptionsEntity' => true
]);
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');

        let formulaires = <?php echo json_encode($mesFormulaires)?>;
        if (formulaires.length === 0) {
            $('#btnAddModeleToForm').attr('disabled', true);
        } else {
            $('#btnAddModeleToForm').removeAttr('disabled');
        }

    });

</script>
