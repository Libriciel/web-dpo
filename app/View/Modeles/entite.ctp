<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

if (!empty($modeles)) {
    ?>
    <!-- Tableau modèles pour l'extrait de registre -->
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <th class="col-md-5">
                    <?php 
                    echo __d('modele', 'modele.titreTableauFormulaire');
                    ?>
                </th>
                
                <th class="col-md-5">
                    <?php 
                    echo __d('modele', 'modele.titreTableauFichierModele');
                    ?>
                </th>

                <th class="col-md-2">
                    <?php 
                    echo __d('modele', 'modele.titreTableauActions');
                    ?>
                </th>
            </tr>
        </thead>

        <!-- Info tableau -->
        <tbody>
            <?php
            foreach ($modeles as $key => $value) {
                ?>
                <tr class="d-flex">
                    <!-- Nom du formulaire -->
                    <td class="tdleft col-md-5">
                        <?php
                        echo $value['Formulaire']['libelle'];
                        ?>
                    </td>

                    <?php
                    if ($value['Modele']['fichier'] != null) {
                        ?>
                        <!-- Nom du fichier -->
                        <td class="tdleft col-md-5">
                            <i class="far fa-file-alt fa-lg fa-fw"></i>
                            <?php
                            echo $value['Modele']['name_modele'];
                            ?>
                        </td>
                        <?php
                    } else {
                        ?>
                        <!-- Aucun modèle pour ce formulaire -->
                        <td class="tdleft col-md-5">
                            <?php
                            echo __d('modele', 'modele.textTableauAucunModele');
                            ?>
                        </td>
                        <?php
                    }
                    ?>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if ($value['Modele']['fichier'] != null) {
                                echo $this->element('Buttons/download', [
                                    'controller' => 'modeles',
                                    'fichier' => $value['Modele']['fichier'],
                                    'name_modele' => $value['Modele']['name_modele'],
                                    'titleBtn' => __d('modele', 'modele.commentaireTelechargerModel')
                                ]);

                                echo $this->element('Buttons/dissocier', [
                                    'controller' => 'modeles',
                                    'id' =>  $value['Modele']['id'],
                                    'titleBtn' => __d('modele', 'modele.btnDissocier'),
                                    'confirmation' => __d('modele', 'modele.confirmationDissocierModele') . ' " ' . $value['Modele']['name_modele'] . ' " du formulaire ?',
                                ]);
                            } else {
                                echo $this->Form->button('<i class="fa fa-upload fa-lg"></i>', [
                                    'escape' => false,
                                    'class' => 'btn btn-outline-dark btn-sm my-tooltip btn-upload-modele',
                                    'title' => __d('modele', 'modele.commentaireImporterModel'),
                                    'data-toggle' => 'modal',
                                    'data-target' => '#modalUploadModele',
                                    'data' => $value['Formulaire']['id']
                                ]);
                            }
                            ?>
                        </div>

                        <?php
                        echo $this->element('Buttons/infoVariable', [
                            'controller' => 'modeles',
                            'id' => $value['Formulaire']['id'],
                            'titleBtn' => __d('modele', 'modele.commentaireVariableModel'),
                        ]);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('modele', 'modele.textAucunModeleMesFormulaires');
            ?>
        </h3>
    </div>
    <?php
}
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');
        
    });

</script>
