<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

if ($usemodelepresentation === false) {
    ?>
    <h3>
        <?php
        echo("Informations propres au paramétrage de l'organisation");
        ?>
    </h3>

    <hr/>

    <?php
    echo $this->element('Modele/tableVariablesEntite');

    echo $this->element('Modele/tableVariablesResponsableEntite');

    echo $this->element('Modele/tableVariablesDPO');
    ?>

    <hr/>
    <br/>
    <?php
}
?>

<h3>
    <?php
    echo("Informations propres au traitement");
    ?>
</h3>

<hr/>

<!-- Texte section -->
<div class="alert alert-warning" role="alert">
    <?php
    echo __d('modele', 'modele.textInformationSectionTraitement');
    ?>
</div>

<hr/>

<?php
if ($rt_externe === true) {
    ?>
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreRTExterne');
            ?>
        </h4>

        <thead>
        <tr>
            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
            <?php
            foreach ($rtVariables as $rtVariable) {
                ?>
                <tr>
                    <td class="tdleft">
                        <?php
                        echo __m('rt_externe' . '.champ' . Inflector::camelize($rtVariable));
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo 'valeur_' . $rtVariable;
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    <br>
    <?php
}

if (isset($optionsUseFormulaire) && $optionsUseFormulaire['Formulaire']['usepia'] === true) {
    ?>
    <!-- Tableur AIPD -->
    <table class="table">
        <h4>
            <?php
            echo ("Onglet AIPD");
            ?>
        </h4>

        <thead>
            <tr>
                <th class="thleft col-md-7">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="thleft col-md-2">
                    <?php
                    echo ('Section');
                    ?>
                </th>

                <th class="thleft col-md-2">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>

                <th class="thleft col-md-1">
                    <?php
                    echo __d('modele', 'modele.titreTableauType');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            $aipdFields = array_merge(
                Fiche::LISTING_FIELDS_TRAITEMENT_NO_REQUIRED_PIA,
                Fiche::LISTING_FIELDS_TRAITEMENT_REQUIRED_PIA
            );

            foreach ($aipdFields as $key => $id) {
                $text = ucwords($id, "_");
                $text = str_replace('_', '', $text);
                ?>

                <tr>
                    <td>
                        <?php
                        echo __d('fiche', 'fiche.champ'.$text);
                        ?>
                    </td>

                    <td>
                    </td>

                    <td>
                        <?php
                        echo "valeur_".$id;
                        ?>
                    </td>

                    <td>
                        <?php
                        echo __d('modele', 'modele.textDeroulantChamp');
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            <tr>
                <td>
                    <?php
                    echo __d('fiche', 'fiche.champCriteres');
                    ?>
                </td>

                <td>
                    <?php
                    echo "criteres";
                    ?>
                </td>

                <td>
                    <?php
                    echo "valeur_criteres";
                    ?>
                </td>

                <td>
                    <?php
                    echo __d('modele', 'modele.textCheckboxChamp');
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?php
                    echo __d('fiche', 'fiche.champTraitementConsidereRisque');
                    ?>
                </td>

                <td>
                </td>

                <td>
                    <?php
                    echo "valeur_traitement_considere_risque";
                    ?>
                </td>

                <td>
                    <?php
                    echo __d('modele', 'modele.textDeroulantChamp');
                    ?>
                </td>
            </tr>
        </tbody>
    </table>

    <br>
    <?php
}

if (!empty($valeurPropreTraitement['declarant'])) {
    ?>
    <!-- Tableau de personne a l'origine de l'utilisation du traitement -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreCreateurTraitement');
            ?>
        </h4>

        <thead>
        <tr>
            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <?php
            foreach ($valeurPropreTraitement['declarant'] as $declarant) {
            ?>
        <tr>
            <td class="tdleft">
                <?php
                echo __d('fiche', 'fiche.champ' . ucfirst($declarant));
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo 'valeur_' . $declarant;
                ?>
            </td>
        </tr>
        <?php
        }
        ?>
        </tr>
        </tbody>
    </table>
    <?php
}
?>

<br>

<?php
if (!empty($valeurPropreTraitement['traitement'])) {
    ?>
    <!-- Tableau pour le nom et le description du traitement -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreNomDescriptionTraitement');
            ?>
        </h4>

        <thead>
            <tr>
                <th class="thleft col-md-10">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="thleft col-md-10">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <?php
                foreach ($valeurPropreTraitement['traitement'] as $traitement) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo __m('fiche' . '.champ' . Inflector::camelize($traitement));
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo 'valeur_' . $traitement;
                            ?>
                        </td>
                    </tr>
                <?php
                }
            ?>
            </tr>
        </tbody>
    </table>
    <?php
}
?>

<br>

<?php
if (isset($optionsUseFormulaire)) {
    if ($optionsUseFormulaire['Formulaire']['usesousfinalite'] === true ||
        $optionsUseFormulaire['Formulaire']['usebaselegale'] === true ||
        $optionsUseFormulaire['Formulaire']['usedecisionautomatisee'] === true ||
        $optionsUseFormulaire['Formulaire']['usetransferthorsue'] === true ||
        $optionsUseFormulaire['Formulaire']['usedonneessensible'] === true
    ) {
        ?>
        <!-- Tableau des variables du formulaire -->
        <table class="table">
            <h4>
                <?php
                echo __d('modele', 'modele.sousTitreVariableInformationComplementaireTraitement');
                ?>
            </h4>

            <thead>
                <tr>
                    <th class="thleft col-md-5">
                        <?php
                        echo __d('modele', 'modele.titreTableauNomChamp');
                        ?>
                    </th>

                    <th class="thleft col-md-2">
                        <?php
                        echo "Section";
                        ?>
                    </th>

                    <th class="thleft col-md-3">
                        <?php
                        echo __d('modele', 'modele.titreTableauNomVariable');
                        ?>
                    </th>

                    <th class="thleft col-md-5">
                        <?php
                        echo __d('modele', 'modele.titreTableauType');
                        ?>
                    </th>
                </tr>
            </thead>

            <tbody>
                <?php
                if ($optionsUseFormulaire['Formulaire']['usesousfinalite'] === true) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Sous finalité';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "sousFinalite";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_sousFinalite";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textPetitChamp');
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if ($optionsUseFormulaire['Formulaire']['usebaselegale'] === true) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Base légale';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "baselegale";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_baselegale";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textCheckboxChamp');
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if ($optionsUseFormulaire['Formulaire']['usedecisionautomatisee'] === true) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Prise de décision automatisé sur le traitement';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_decisionAutomatisee";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textDeroulantChamp');
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Description de la prise de décision automatique';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_descriptionDecisionAutomatisee";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textGrandChamp');
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if ($optionsUseFormulaire['Formulaire']['usetransferthorsue'] === true) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Organisme Destinataire X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "horsue";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_organismeDestinataireHorsUe";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textPetitChamp');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Type De Garantie X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "horsue";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_typeGarantieHorsUe";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textDeroulantChamp');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Pays Destinataire X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "horsue";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_paysDestinataireHorsUe";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textDeroulantChamp');
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if ($optionsUseFormulaire['Formulaire']['usedonneessensible'] === true) {
                    ?>
                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Type De Donnée Sensible X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "donneessensibles";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_typeDonneeSensible";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textDeroulantChamp');
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Description X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "donneessensibles";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_descriptionDonneeSensible";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textGrandChamp');
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="tdleft">
                            <?php
                            echo 'Durée De Conservation X';
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "donneessensibles";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo "valeur_dureeConservationDonneeSensible";
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            echo __d('modele', 'modele.textGrandChamp');
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }
}
?>

<br>

<?php
if (!empty($variables)) {
    ?>
    <!-- Tableau des variables du formulaire -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreVariableFormulaire');
            ?>
        </h4>

        <thead>
        <tr>
            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-2">
                <?php
                echo "Section";
                ?>
            </th>

            <th class="thleft col-md-3">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauType');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($variables as $key => $variable) {
            $details = json_decode($variable['details'], true);

            if (!empty($details['name'])) {
                ?>
                <tr>
                    <td class="tdleft">
                        <?php echo $details['label']; ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        switch ($variable['type']) {
                            case 'checkboxes':
                                echo $details['name'];
                                break;

                            case 'multi-select':
                                echo $details['name'];
                                break;

                            default:
                                break;
                        }
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php echo "valeur_" . $details['name']; ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        switch ($variable['type']) {
                            case 'input':
                                echo __d('modele', 'modele.textPetitChamp');
                                break;

                            case 'textarea':
                                echo __d('modele', 'modele.textGrandChamp');
                                break;

                            case 'date':
                                echo __d('modele', 'modele.textDateChamp');
                                break;

                            case 'checkboxes':
                                echo __d('modele', 'modele.textCheckboxChamp');
                                break;

                            case 'radios':
                                echo __d('modele', 'modele.textRadioChamp');
                                break;

                            case 'deroulant':
                                echo __d('modele', 'modele.textDeroulantChamp');
                                break;

                            case 'multi-select':
                                echo __d('modele', 'modele.textMultiSelectChamp');
                                break;

                            default:
                                break;
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>

    <br>
    <?php
}

if (!empty($coresponsables)) {
    ?>
    <!-- Tableau pour le nom et le description du traitement -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreCoResponsable');
            ?>
        </h4>

        <thead>
            <tr>
                <th class="tdleft col-md-6">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo "Section";
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($coresponsables as $nameField => $nameVariable) {
                ?>
                <tr>
                    <td>
                        <?php
                        echo $nameField;
                        ?>
                    </td>
                    <td>
                        <?php
                        echo 'coresponsables';
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $nameVariable;
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    <br>
    <?php
}

if (!empty($arrayRtOrSt)) {
    ?>
    <!-- Tableau pour le nom et le description du traitement -->
    <table class="table">
        <h4>
            <?php
            if ($rt_externe === true) {
                echo __d('modele', 'modele.sousTitreSoustraitant');
                $model = 'st_organisation';
            } else {
                echo __d('modele', 'modele.sousTitreRT');
                $model = 'rt_organisation';
            }
            ?>
        </h4>

        <thead>
            <tr>
                <th class="tdleft col-md-6">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($arrayRtOrSt as $nameField => $RtOrSt) {
                ?>
                <tr>
                    <td>
                        <?php
                        echo __m($model . '.champ' . Inflector::camelize($RtOrSt));
                        ?>
                    </td>
                    <td>
                        <?php
                        echo 'valeur_' . $RtOrSt;
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    <br/>
    <?php
}

if (!empty($soustraitances)) {
    ?>
    <!-- Tableau pour le nom et le description du traitement -->
    <table class="table">
        <h4>
            <?php
            if ($rt_externe === true) {
                echo __d('modele', 'modele.sousTitreSoustraitanceUlterieur');
            } else {
                echo __d('modele', 'modele.sousTitreSoustraitance');
            }
            ?>
        </h4>

        <thead>
            <tr>
                <th class="tdleft col-md-6">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo "Section";
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($soustraitances as $nameField => $nameVariable) {
                ?>
                <tr>
                    <td>
                        <?php
                        echo $nameField;
                        ?>
                    </td>
                    <td>
                        <?php
                        echo 'soustraitances';
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $nameVariable;
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    <br>
    <?php
}

if ($viewAnnexe === true) {
    ?>
    <!-- Tableau annexe -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreAnnexeTraitement');
            ?>
        </h4>

        <thead>
            <tr>
                <th class="tdleft col-md-6">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="thleft col-md-2">
                    <?php
                    echo ('Section');
                    ?>
                </th>

                <th class="tdleft col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    <?php
                    echo __d('modele', 'modele.textTableauAnnexe');
                    ?>
                </td>

                <td>
                    <?php
                    echo ('fichiers');
                    ?>
                </td>

                <td>
                    <?php
                    echo __d('modele', 'modele.textValeur') . 'annexe';
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?php
                    echo 'Nom du fichier';
                    ?>
                </td>

                <td>
                    <?php
                    echo 'info_fichiers';
                    ?>
                </td>

                <td>
                    <?php
                    echo "valeur_nom_fichier"
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?php
                    echo 'Date de dépot du fichier';
                    ?>
                </td>

                <td>
                    <?php
                    echo 'info_fichiers';
                    ?>
                </td>

                <td>
                    <?php
                    echo "valeur_date_depot"
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?php
                    echo 'Type de fichier';
                    ?>
                </td>

                <td>
                    <?php
                    echo 'info_fichiers';
                    ?>
                </td>

                <td>
                    <?php
                    echo "valeur_type_fichier"
                    ?>
                </td>
            </tr>

            <tr>
                <td>
                    <?php
                    echo 'Taille du fichier en kilobyte (kB)';
                    ?>
                </td>

                <td>
                    <?php
                    echo 'info_fichiers';
                    ?>
                </td>

                <td>
                    <?php
                    echo "valeur_taille_fichier"
                    ?>
                </td>
            </tr>

        </tbody>
    </table>

    <br>
    <?php
}

if ($viewHistorique === true) {
    ?>
    <!-- Tableau Historique -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreHistoriqueTraitement');
            ?>
        </h4>

        <!-- Texte section -->
        <div class="alert alert-warning" role="alert">
            <?php
            echo __d('modele', 'modele.textInformationSectionHistoriques');
            ?>
        </div>

        <thead>
        <tr>
            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-10">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td class="tdleft">
                <?php
                echo __d('modele', 'modele.textTableauHistoriqueCommentaire');
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo("content");
                ?>
            </td>
        </tr>

        <tr>
            <td class="tdleft">
                <?php
                echo __d('modele', 'modele.textTableauHistoriqueDate');
                ?>
            </td>

            <td class="tdleft">
                <?php
                echo("created");
                ?>
            </td>
        </tr>
        </tbody>
    </table>

    <br/>

    <!-- Tableau Filigrane -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreFiligrane');
            ?>
        </h4>

        <div class="alert alert-warning" role="alert">
            <?php
            echo __d('modele', 'modele.textInformationFiligrane');
            ?>
        </div>

        <thead>
            <tr>
                <th class="thleft col-md-10">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td class="tdleft">
                    <?php
                    echo("etat_traitement_alias");
                    ?>
                </td>
            </tr>
        </tbody>
    </table>

    <br/>
    <?php
}
?>

<!-- Bouton Retour -->
<div class="row">
    <div class="col-md-12 top17 text-center">
        <div class="buttons">
            <?php
            echo $this->Html->link('<i class="fa fa-arrow-left fa-lg"></i>' . __d('default', 'default.btnBack'), $referer, array(
                'class' => 'btn btn-outline-primary',
                'escape' => false
            ));
            ?>
        </div>
    </div>
</div>
