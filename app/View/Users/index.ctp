<?php
//echo $this->Html->script('jquery-mask-plugin/dist/jquery.mask.min.js');
echo $this->Html->script('users.js');
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$actionAdminIndex = 'admin_index' === $this->request->params['action'];

$breadcrumbs = [
    $title => []
];
if ($actionAdminIndex === false) {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
} else {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

$pagination = null;

// Filtrer les utilisateur
// Bouton du filtre des utilisateurs
echo $this->element('Buttons/filtre', [
    'titleBtn' => 'Filtrer les utilisateurs'
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('users', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            if ($actionAdminIndex === true) {
                // Filtrer par organisation
                echo $this->Form->input('organisation', [
                    'empty' => true,
                    'data-placeholder' => __d('user', 'user.placeholderChoisirOrganisation'),
                    'class' => 'usersDeroulant form-control',
                    'label' => 'Filtrer par entité',
                    'options' => $options['organisations'],
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);

                echo '</div>'
                    .'<div class="col-md-6">'
                ;
            }

            // Filtrer par organisation
            echo $this->Form->input('dpo', [
                'empty' => true,
                'data-placeholder' => __d('user', 'user.filtreChercherDPO'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('user', 'user.labelFiltreChercherDPO'),
                'options' => $options['dpo'],
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par nom complet
            echo $this->Form->input('nom', [
                'empty' => true,
                'data-placeholder' => __d('user', 'user.filtreChercherUtilisateur'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('user', 'user.labelFiltreChercherUtilisateur'),
                'options' => $options['users'],
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtrer par profil
            echo $this->Form->input('profil', [
                'empty' => true,
                'data-placeholder' => __d('user', 'user.filtreChercherProfil'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('user', 'user.labelFiltreChercherProfil'),
                'options' => $options['roles'],
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtrer par identifiant
            echo $this->Form->input('username', [
                'placeholder' => __d('user', 'user.filtreChercherIdentifiant'),
                'class' => 'form-control',
                'label' => __d('user', 'user.labelFiltreChercherIdentifiant') . ' <i class="glyphicon glyphicon-question-sign help" rel="tooltip" title="Recherche insensible à la casse. Le caractère &quot;*&quot; permet de faire des recherches approchantes. Par exemple, &quot;d.*&quot; cherchera tous les identifiants débutant par &quot;d.&quot;."><!----></i>',
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtrer par service
            echo $this->Form->input('service', [
                'empty' => true,
                'data-placeholder' => __d('user', 'user.filtreChercherService'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('user', 'user.labelFiltreChercherService'),
                'options' => $options['services'],
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
// Ajout d'un nouveau utilisateur en fonction des droits de l'utilisateur connecté pour la création
if ($this->Autorisation->authorized(8, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'users',
        'labelBtn' => __d('user', 'user.btnAjouterUser'),
        'before' => '<div class="text-left">',
    ]);
}

// Résuletats
if (empty($results) === false) {
    $this->Paginator->options([
        'url' => Hash::flatten((array)$this->request->data, '.')
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <?php
                echo $this->Html->tableHeaders(
                    array_filter([
                        [$this->Paginator->sort('User.nom_complet', __d('user', 'user.titreTableauUtilisateur')) => ['class' => 'col-md-2']],
                        [$this->Paginator->sort('User.username', __d('user', 'user.titreTableauIdentifiant')) => ['class' => 'col-md-2']],
                        [__d('user', 'user.titreTableauDpo') => ['class' => 'col-md-1']],
                        [__d('user', 'user.titreTableauEntite') => ['class' => 'col-md-2']],
                        [__d('user', 'user.titreTableauProfil') => ['class' => 'col-md-2']],
                        false === $hasService ? null : [__d('user', 'user.titreTableauService') => ['class' => 'col-md-2']],
                        [__d('user', 'user.titreTableauAction') => ['class' => 'col-md-1']]
                    ])
                );
                ?>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($results as $idx => $result) {
                $organisations = (array)Hash::get($result, 'Organisation');
                $count = count($organisations);
                $rows = 1 < $count ? $count : 1;
                $trOptions = ['class' => 0 === ($idx + 1) % 2 ? 'even' : 'odd'];

                // Si l'utilisateur est DPO de l'organisation, on affiche le logo
                $isDpo = Hash::get($result, 'Organisation.0.OrganisationUser.is_dpo');
                $image = null;
                if (true === $isDpo) {
                    $image = 'oui';
                }

                // Actions
                $actions = '';
                if ($this->Autorisation->authorized(9, $droits)) {
                    // Bouton de modification
                    $actions .= $this->element('Buttons/edit', [
                        'controller' => 'users',
                        'id' => $result['User']['id'],
                        'titleBtn' => __d('user', 'user.commentaireModifierUser')
                    ]);

                    // Bouton de suppression
                    if ($this->Session->read('Auth.User.id') != $result['User']['id']) {
                        if ($this->Autorisation->authorized(10, $droits)) {
                            if ($result['User']['id'] !== 1 && $result['User']['is_dpo'] === false) {
                                // Bouton de suppression
                                $actions .= $this->element('Buttons/delete', [
                                    'controller' => 'users',
                                    'id' => $result['User']['id'],
                                    'titleBtn' => __d('user', 'user.commentaireSupprimerUser'),
                                    'confirmation' => __d('user', 'user.confirmationSupprimerUser') . $result['User']['nom_complet'] . ' ?',
                                ]);
                            }
                        }
                    }
                }

                $services = Hash::extract($result, 'Organisation.0.OrganisationUser.Service.{n}.libelle');
                if (!empty($services)) {
                    $nbServices = count($services);
                    if ($nbServices <= 5) {
                        $services = '<ul><li>' . implode('</li><li>', $services) . '</li></ul>';
                    } else {
                        $services = $this->Html->tag('strong', $nbServices.' services');
                    }
                } else {
                    $services = $this->Html->tag('strong', 'Aucun service');
                }

                $entites = Hash::extract($result, 'Organisation.{n}.raisonsociale');
                $entite = true === empty($entites)
                    ? $this->Html->tag('strong', __d('user', 'user.textTableauAucuneEntite'))
                    : Hash::get($result, 'Organisation.0.raisonsociale');

                echo $this->Html->tableCells(
                    [
                        array_filter(
                            [
                                [$result['User']['nom_complet'], ['rowspan' => $rows, 'class' => 'tdleft']],
                                [$result['User']['username'], ['rowspan' => $rows, 'class' => 'tdleft']],
                                [$image, ['class' => 'tdleft']],
                                [$entite, ['class' => 'tdleft']],
                                [Hash::get($result, 'Organisation.0.OrganisationUser.Role.libelle'), ['class' => 'tdleft']],
                                false === $hasService ? null : [$services, ['class' => 'tdleft']],
                                [$this->Html->tag('div', $actions, ['class' => 'buttons']), ['rowspan' => $rows, 'class' => 'tdleft']]
                            ]
                        )
                    ],
                    $trOptions,
                    $trOptions
                );

                if (1 < $rows) {
                    for ($i = 1; $i < $rows; $i++) {
                        // Si l'utilisateur est DPO de l'organisation, on affiche le logo
                        $isDpo = Hash::get($result, "Organisation.{$i}.OrganisationUser.is_dpo");
                        $image = null;
                        if (true === $isDpo) {
                            $image = 'oui';
                        }

                        $services = Hash::extract($result, "Organisation.{$i}.OrganisationUser.Service.{n}.libelle");
                        if (!empty($services)) {
                            $nbServices = count($services);
                            if ($nbServices <= 5) {
                                $services = '<ul><li>' . implode('</li><li>', $services) . '</li></ul>';
                            } else {
                                $services = $this->Html->tag('strong', $nbServices.' services');
                            }
                        } else {
                            $services = $this->Html->tag('strong', 'Aucun service');
                        }

                        echo $this->Html->tableCells(
                            [
                                array_filter(
                                    [
                                        [$image, ['class' => 'tdleft']],
                                        [
                                            Hash::get($result, "Organisation.{$i}.raisonsociale"),
                                            ['class' => 'tdleft']
                                        ],
                                        [
                                            Hash::get($result, "Organisation.{$i}.OrganisationUser.Role.libelle"),
                                            ['class' => 'tdleft']
                                        ],
                                        false === $hasService ? null : [$services, ['class' => 'tdleft']],
                                    ]
                                )
                            ],
                            $trOptions,
                            $trOptions
                        );

                    }
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            if ([] === Hash::filter($filters)) {
                echo 'admin_index' === $this->request->params['action']
                    ? __d('user', 'user.textAucunUser')
                    : __d('user', 'user.textAucunUserCollectiviter');
            } else {
                echo __d('user', 'user.textAucunUserFiltre');
            }
            ?>
        </h3>
    </div>
    <?php
}

echo $pagination;

// Ajout d'un nouveau utilisateur en fonction des droits de l'utilisateur connecté pour la création
if ($this->Autorisation->authorized(8, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'users',
        'labelBtn' => __d('user', 'user.btnAjouterUser'),
    ]);
}


