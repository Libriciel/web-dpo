<?php
use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

echo $this->Html->script('users.js');
echo $this->Html->css('password-force.css');

$refererActionAdminIndex = '/users/admin_index' === $referer;

if ($refererActionAdminIndex === false) {
    $breadcrumbs = [
        __d('user', 'user.titreListeUser') => [
            'action' => 'index',
            'prepend' => true
        ],
        $title => []
    ];
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

} else {
    $breadcrumbs = [
        __d('user', 'user.titreUsersApplication') => [
            'action' => 'admin_index',
            'prepend' => true
        ],
        $title => []
    ];
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

$thresholds = array_flip(PasswordStrengthMeterAnssi::thresholds());

if ($this->request->params['action'] == 'add') {
    $empty = true;
} else {
    $empty = false;
}

if (isset($this->validationErrors['User']) && !empty($this->validationErrors['User'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <i class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></i>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('User',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        // Organisations
        echo $this->WebcilForm->input('User.organisation_id', [
            'id' => 'deroulant',
            'options' => $options['organisation_id'],
            'label' => [
                'text' => __d('user', 'user.champEntite'),
            ],
            'required' => true,
            'multiple' => 'multiple',
            'placeholder' => false
        ]);

        if (!isset($this->request->data['User']['ldap']) || $this->request->data['User']['ldap'] == false) {
            echo $this->WebcilForm->inputs([
                'User.password' => [
                    'id' => 'hiddenpassword',
                    'value' => '',
                    'type' => 'hidden',
                    'placeholder' => false
                ],
                'User.username' => [
                    'id' => 'username',
                    'autocomplete' => 'off',
                    'required' => true
                ]
            ]);

            if ('edit' === $this->request->params['action']) {
                $libellePassword = __d('user', 'user.champNouveauMotDePasse');
                $placeholder = sprintf(__d('user', "user.placeholderChampNewPassword"), $forcePassword);
            } else {
                $libellePassword = __d('user', 'user.champPassword');
                $placeholder = __d('user', 'user.placeholderChampPasswordAdd');
            }

            // Champ caché pour éviter l'autocomplete du navigateur pour le mot de passe
            $password = $this->WebcilForm->inputs([
                'User.password' => [
                    'id' => 'password',
                    'autocomplete' => 'off',
                    'required' => true,
                    'label' => [
                        'text' => $libellePassword
                    ],
                    'placeholder' => $placeholder,
                    'readonly' => true,
                    'help-text' => __d('user', 'user.helpTextChampPassword'),
                    'after' => '<div class="">
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddForm">
                            <i class="fa fa-question"></i>
                        </button>
                    </div>'
                ],
                'User.passwd' => [
                    'id' => 'passwd',
                    'autocomplete' => 'off',
                    'required' => true,
                    'readonly' => true
                ]
            ]);

            if ('edit' === $this->request->params['action']) {
                echo $this->Html->tag(
                    'div',
                    __d('user', 'user.textInfoMotDePasse') . $password,
                    ['class' => 'alert alert-info']
                );
            } else {
                echo $password;
            }
        } else {
            echo $this->WebcilForm->input('User.username', [
                'id' => 'username',
                'autocomplete' => 'off',
                'required' => true,
                'readonly' => true
            ]);
        }

        if ('edit' === $this->request->params['action']) {
            $empty = false;
        } else {
            $empty = true;
        }

        echo $this->WebcilForm->inputs([
            'User.civilite' => [
                'id' => 'civilite',
                'class' => 'form-control custom-select',
                'options' => $options['User']['civilite'],
                'empty' => $empty,
                'required' => true,
                'placeholder' => false
            ],
            'User.nom' => [
                'id' => 'nom',
                'required' => true
            ],
            'User.prenom' => [
                'id' => 'prenom',
                'required' => true
            ],
            'User.email' => [
                'id' => 'email',
                'required' => true
            ],
            'User.telephonefixe' => [
                'id' => 'telephonefixe'
            ],
            'User.telephoneportable' => [
                'id' => 'telephoneportable'
            ],
            'User.notification' => [
                'id' => 'notification',
                'class' => 'form-control custom-select',
                'options' => [
                    false => 'Non',
                    true => 'Oui'
                ],
                'empty' => false,
                'required' => true,
                'default' => false,
                'placeholder' => false
            ]
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?php
        $role_id_readonly = [];

        foreach($options['organisation_id'] as $organisation_id => $raisonsociale) {
            echo '<fieldset id="organisation-block-'.$organisation_id.'" class="organisation-block">';
            echo $this->Html->tag('legend', $raisonsociale);

            // Services
            $services = (array)Hash::get($options, "service_id.{$organisation_id}");
            if (false === empty($services)) {
                echo $this->WebcilForm->input('User.service_id', [
                    'id' => 'service_id'.$organisation_id,
                    'name' => "data[User][{$organisation_id}][service_id]",
                    'options' => $services,
                    'label' => [
                        'text' => __d('user', 'user.champService'),
                    ],
                    'multiple' => 'multiple',
                    'empty' => false,
                    'placeholder' => false,
                    'value' => (array)Hash::get($this->request->data, "User.{$organisation_id}.service_id")
                ]);
            }

            // Rôle
            $valueRole = array_filter(Hash::extract($this->request->data, "User.{$organisation_id}.role_id"));
            $optionsRole = (array)Hash::get($options, "role_id.{$organisation_id}");
            $hasError = ($this->request->is('post') || $this->request->is('put')) && true === empty($valueRole);

            foreach ($valueRole as $role) {
                foreach ($rolesDPO_id as $roleDPO_id){
                    if ($roleDPO_id == $role) {
                        foreach ($optionsRole as $key => $optionRole) {
                            if ($role != $key) {
                                unset($optionsRole[$key]);
                            }
                        }
                    }
                }
            }

            echo $this->WebcilForm->input('User.role_id', [
                'id' => 'role_id'.$organisation_id,
                'name' => "data[User][{$organisation_id}][role_id]",
                'options' => $optionsRole,
                'class' => 'form-control custom-select'.(true === $hasError ? ' is-invalid' : null),
                'label' => [
                    'text' => __d('user', 'user.champProfilEntite'),
                ],
                'empty' => $empty,
                'placeholder' => false,
                'required' => true,
                'value' => $valueRole,
                'after' => (true === $hasError ? '<div class="invalid-feedback">'.__d('database', 'Validate::notBlank').'</div>' : null)
            ]);

            echo '</fieldset>';
        }
        ?>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();

echo $this->element('modalpassword');
?>

<script type="text/javascript">
    $(document).ready(function () {

        $("#deroulant").select2({
            language: "fr",
            placeholder: "Sélectionnez une ou plusieurs entités",
            allowClear: true
        });

        <?php $selected = (array)Hash::get($this->request->data, 'User.organisation_id');?>
        <?php foreach(array_keys($options['organisation_id']) as $organisation_id): ?>
        $("#<?php echo 'service_id'.$organisation_id;?>").select2({
            language: "fr",
            placeholder: "Sélectionnez un ou plusieurs service",
            allowClear: true
        });

        <?php if(false === in_array($organisation_id, $selected)):?>
        $("#organisation-block-<?php echo $organisation_id;?>").hide();
        <?php endif;?>
        <?php endforeach;?>

        $('#deroulant').change(function(event) {
            var select = $(this),
                values = $.map($(select).children('option'),function(option){return option.value;}),
                selected = $(select).val();

            $.each(values, function(index, value){
                if(-1 !== $.inArray(value, selected)) {
                    $("#organisation-block-"+value).show();
                } else {
                    $("#organisation-block-"+value).hide();
                }
            });
        });

        $("#password").before( "<div id=\"jauge-password\"></div>" );

        var minScore = <?= $minEntropie?>,
            inputPassword = $('#password');

        $('#jauge-password').append(
            $('<div></div>')
                .addClass('progress')
                .height('5px')
                .css({'margin-bottom': '5px'})
                .append(
                    $('<div></div>')
                        .addClass('progress-bar')
                        .attr('role', 'progressbar')
                        .attr('aria-valuenow', 0)
                        .attr('aria-valuemin', 0)
                        .attr('aria-valuemax', <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>))
        );

        inputPassword.keyup(function() {
            var $this = $(this),
                password = $this.val();

            $.ajax({
                url: '<?php echo Router::url(['action' => 'ajax_password']);?>',
                method: 'POST',
                data: {'password': password},
                success: function(data) {
                    try{
                        var content = JSON.parse(data),
                            score = content['entropie'],
                            progressBar = $('#jauge-password').find('.progress .progress-bar');

                        progressBar.removeClass('progress-bar-striped');

                        if (!$this.val()) {
                            $this.parent().find('span.color-dot').removeClass('green')
                                .removeClass('red')
                                .addClass('empty');
                        } else {
                            if (score > minScore) {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('red')
                                    .addClass('green');
                            } else {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('green')
                                    .addClass('red');
                            }
                        }

                        if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_WEAK]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-1');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_MEDIUM]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-2');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-3');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-4');
                        } else {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .addClass('progress-bar-password-force-5');
                        }

                        progressBar
                            .attr('aria-valuenow', score)
                            .width(score / <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?> * 100 + '%');
                    }catch(e){
                        console.error(e);
                        return;
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        inputPassword.change(function() {
            $('#confirm-password').val('').parent().find('span.color-dot')
                .addClass('empty')
                .removeClass('green')
                .removeClass('red');
        });

        $('#confirm-password').keyup(function() {
            if (!$(this).val()) {
                $(this).parent().find('span.color-dot').addClass('empty')
                    .removeClass('green')
                    .removeClass('red');
            } else if ($(this).val() === inputPassword.val()) {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('red')
                    .addClass('green');
            } else {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('green')
                    .addClass('red');
            }
        });

        function updatePasswordInformationsFromSelectedEntite() {
            var force = <?php echo json_encode($options['force'])?>;
            var max = 0;

            if ($('#deroulant').val()) {
                $.each($('#deroulant').val(), function(idx, id) {
                    if (force[id] > max) {
                        max = force[id];
                    }
                });

                $('#password').prop("readonly", false);
                $('#passwd').prop("readonly", false);

                $('#passwordHelpText').text("<?php echo __d('user', "user.helpTextChampPasswordValue"); ?>".replace('%d', max));
                $('#password').attr('placeholder', '');
            } else {
                // @fixme: remettre le msg "Veuillez respecter la politique de mot de passe"
                $('#password').prop("readonly", true);
                $('#passwd').prop("readonly", true);

                $('#passwordHelpText').text("<?php echo __d('user', 'user.helpTextChampPassword'); ?>");
                $('#password').attr('placeholder', '<?php echo __d('user', 'user.placeholderChampPasswordAdd'); ?>');
            }

            try {
                affichageCouleurForceTableau(max);
            } catch(err) {
                console.error("affichageCouleurForceTableau("+max+");");
            }
        }

        $('#deroulant').change(function() {
            updatePasswordInformationsFromSelectedEntite();
        });

        updatePasswordInformationsFromSelectedEntite();
    });
</script>
