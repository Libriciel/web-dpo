<?php
if (empty($configLoginExistante)) {
    ?>
    <ls-lib-login
            logo="/img/logo-black-blue.svg"
            login-username-input-name="data[User][username]"
            login-password-input-name="data[User][password]"
            login-show-error="<?php echo $error; ?>"
            login-success-message="<?php echo $successMessage; ?>" >
    </ls-lib-login>
    <?php
} else {
    ?>
    <ls-lib-login
            logo="/img/logo-black-blue.svg"
            visual-configuration='<?php echo $configLoginExistante; ?>'
            login-username-input-name="data[User][username]"
            login-password-input-name="data[User][password]"
            login-show-error="<?php echo $error; ?>"
            login-success-message="<?php echo $successMessage; ?>" >
    </ls-lib-login>
    <?php
}
