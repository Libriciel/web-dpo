<?php
use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

echo $this->Html->script('users.js');
echo $this->Html->css('password-force.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['User']) && !empty($this->validationErrors['User'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

$thresholds = array_flip(PasswordStrengthMeterAnssi::thresholds());
?>


<div class="users form">
    <?php
    echo $this->WebcilForm->create('User', [
        'autocomplete' => 'off',
        'inputDefaults' => ['div' => false],
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate'
    ]);
    ?>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php
                // Champ caché pour éviter l'autocomplete du navigateur pour le mot de passe
                echo $this->WebcilForm->hidden('old_password1', [
                    'id' => false,
                    'style' => 'display: none;',
                    'type' => 'password',
                    'label' => false
                ]);
                ?>
            </div>

            <?php
            echo $this->WebcilForm->inputs([
                'old_password' => [
                    'id' => 'old_password',
                    'autocomplete' => 'off',
                    'type' => 'password',
                    'required' => true
                ],
                'new_password' => [
                    'id' => 'new_password',
                    'autocomplete' => 'off',
                    'required' => true,
                    'type' => 'password',
                    'label' => [
                        'text' => __d('user', 'user.champNouveauMotDePasse')
                    ],
                    'placeholder' => sprintf(__d('user', "user.placeholderChampNewPassword"), $forcePassword),
                    'help-text' => __d('user', 'user.helpTextChampPassword'),
                    'after' => '<div class="">
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddForm">
                                <i class="fa fa-question"></i>
                            </button>
                        </div>'
                ],
                'new_passwd' => [
                    'id' => 'new_passwd',
                    'autocomplete' => 'off',
                    'type' => 'password',
                    'required' => true
                ]
            ]);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);
echo $this->WebcilForm->end();

echo $this->element('modalpassword');
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("#deroulant").select2({
            language: "fr",
            placeholder: "Sélectionnez une ou plusieurs entitées",
            allowClear: true
        });

        $("#deroulantservice").select2({
            language: "fr",
            placeholder: "Sélectionnez un ou plusieurs service",
            allowClear: true
        });
        
        $("#new_password").before( "<div id=\"jauge-password\"></div>" );
        
        var minScore = <?= $minEntropie?>,
            inputPassword = $('#new_password');
    
        $('#jauge-password').append(
            $('<div></div>')
                .addClass('progress')
                .height('5px')
                .css({'margin-bottom': '5px'})
                .append(
                    $('<div></div>')
                        .addClass('progress-bar')
                        .attr('role', 'progressbar')
                        .attr('aria-valuenow', 0)
                        .attr('aria-valuemin', 0)
                        .attr('aria-valuemax', <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>))
        );

        inputPassword.keyup(function() {
            var $this = $(this),
                password = $this.val();

            $.ajax({
                url: '<?php echo Router::url(['action' => 'ajax_password']);?>',
                method: 'POST',
                data: {'password': password},
                success: function(data) {
                    try{ 
                        var content = JSON.parse(data),
                            score = content['entropie'],
                            progressBar = $('#jauge-password').find('.progress .progress-bar');

                        progressBar.removeClass('progress-bar-striped');

                        if (!$this.val()) {
                            $this.parent().find('span.color-dot').removeClass('green')
                                .removeClass('red')
                                .addClass('empty');
                        } else {
                            if (score > minScore) {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('red')
                                    .addClass('green');
                            } else {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('green')
                                    .addClass('red');
                            }
                        }
                        
                        if (score <= <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_WEAK]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-1');
                        } else if (score <= <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_MEDIUM]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-2');
                        } else if (score <= <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-3');
                        } else if (score <= <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-4'); 
                        } else {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .addClass('progress-bar-password-force-5');
                        }

                        progressBar
                            .attr('aria-valuenow', score)
                            .width(score / <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?> * 100 + '%');
                    }catch(e){
                        console.error(e);
                        return;
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        inputPassword.change(function() {
            $('#confirm-password').val('').parent().find('span.color-dot')
                .addClass('empty')
                .removeClass('green')
                .removeClass('red');
        });
        
        $('#confirm-password').keyup(function() {
            if (!$(this).val()) {
                $(this).parent().find('span.color-dot').addClass('empty')
                    .removeClass('green')
                    .removeClass('red');
            } else if ($(this).val() === inputPassword.val()) {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('red')
                    .addClass('green');
            } else {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('green')
                    .addClass('red');
            }
        });
        
    });

</script>
