<?php
echo $this->Html->script('users.js');

$this->Html->addCrumb($title,
    [
        'controller' => $this->params['controller'],
        'action' => $this->params['action']
    ]
);

if (isset($this->validationErrors['User']) && !empty($this->validationErrors['User'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}
?>

<div class="users form">
    <?php
    echo $this->WebcilForm->create('User', [
        'autocomplete' => 'off',
        'inputDefaults' => ['div' => false],
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate'
    ]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'username' => [
                    'id' => 'username',
                    'autocomplete' => 'off',
                    'required' => true,
                    'readonly' => true
                ],
                'civilite' => [
                    'id' => 'civilite',
                    'options' => $options['User']['civilite'],
                    'empty' => false,
                    'required' => true
                ],
                'nom' => [
                    'id' => 'nom',
                    'required' => true
                ],
                'prenom' => [
                    'id' => 'prenom',
                    'required' => true
                ],
                'email' => [
                    'id' => 'email',
                    'required' => true
                ],
                'telephonefixe' => [
                    'id' => 'telephonefixe'
                ],
                'telephoneportable' => [
                    'id' => 'telephoneportable'
                ],
                'notification' => [
                    'id' => 'notification',
                    'options' => [
                        false => 'Non',
                        true => 'Oui'
                    ],
                    'empty' => false,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);
echo $this->WebcilForm->end();
