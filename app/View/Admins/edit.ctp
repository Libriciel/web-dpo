<?php
use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

echo $this->Html->css('password-force.css');
echo $this->Html->script('admin.js');

$this->Breadcrumbs->breadcrumbs([
    __d('admin', 'admin.titreSuperAdministrateur') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

$thresholds = array_flip(PasswordStrengthMeterAnssi::thresholds());

if ($this->request->params['action'] == 'add'){
    $empty = true;
} else {
    $empty = false;
};

if (isset($this->validationErrors['User']) && !empty($this->validationErrors['User'])) {
    ?>
	<div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
            <span class="sr-only">Error:</span>
            Ces erreurs se sont produites:
            <ul>
                <?php
                    foreach ($this->validationErrors as $donnees) {
                        foreach ($donnees as $champ) {
                            foreach ($champ as $error) {
                                echo '<li>' . $error . '</li>';
                            }
                        }
                    }
                ?>
            </ul>
	</div>
    <?php
}

echo $this->WebcilForm->create('User', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>
    
<div class="users form">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'User.password' => [
                'id' => 'hiddenpassword',
                'value' => '',
                'type' => 'hidden',
                'placeholder' => false
            ],
            'User.username' => [
                'id' => 'username',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]);

        if ($this->request->params['action'] === 'edit') {
           $libellePassword = __d('user', 'user.champNouveauMotDePasse');
           $placeholder = sprintf(__d('user', "user.placeholderChampNewPassword"), $forcePassword);
        } else {
            $libellePassword = __d('user', 'user.champPassword');
            $placeholder = sprintf(__d('user', "user.placeholderChampPassword"), $forcePassword);
        }

        $password = $this->WebcilForm->inputs([
            'User.password' => [
                'id' => 'password',
                'autocomplete' => 'off',
                'required' => true,
                'label' => [
                    'text' => $libellePassword . '<div class="col-md-12 text-center">
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddForm">
                            <span class="fa fa-question fa-lg"><!----></span>
                        </button>
                    </div>'
                ],
                'placeholder' => $placeholder
            ],
            'User.passwd' => [
                'id' => 'passwd',
                'autocomplete' => 'off',
                'required' => true
            ]
        ]);

        if ($this->request->params['action'] === 'edit') {
            echo $this->Html->tag(
                'div',
                __d('user', 'user.textInfoMotDePasse').$password,
                ['class' => 'alert alert-info']
            );
        } else {
            echo $password;
        }

        echo $this->WebcilForm->inputs([
            'User.civilite' => [
                'id' => 'civilite',
                'options' => $options['User']['civilite'],
                'empty' => true,
                'required' => true,
                'placeholder' => false
            ],
            'User.nom' => [
                'id' => 'nom',
                'required' => true
            ],
            'User.prenom' => [
                'id' => 'prenom',
                'required' => true
            ],
            'User.email' => [
                'id' => 'email',
                'required' => true
            ],
            'User.telephonefixe' => [
                'id' => 'telephonefixe'
            ],
            'User.telephoneportable' => [
                'id' => 'telephoneportable'
            ],
            'User.notification' => [
                'id' => 'notification',
                'options' => [false => 'Non', true => 'Oui'],
                'empty' => false,
                'required' => true,
                'placeholder' => false
            ]
        ]);
        ?>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);
echo $this->WebcilForm->end();

echo $this->element('modalpassword');
?>

<script type="text/javascript">
    $(document).ready(function () {
        
        $("#password").before( "<div id=\"jauge-password\"></div>" );
        
        let minScore = <?= $minEntropie?>,
            inputPassword = $('#password');
    
        $('#jauge-password').append(
            $('<div></div>')
                .addClass('progress')
                .height('5px')
                .css({'margin-bottom': '5px'})
                .append(
                    $('<div></div>')
                        .addClass('progress-bar')
                        .attr('role', 'progressbar')
                        .attr('aria-valuenow', 0)
                        .attr('aria-valuemin', 0)
                        .attr('aria-valuemax', <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>))
        );

        inputPassword.keyup(function() {
            let $this = $(this),
                password = $this.val();

            $.ajax({
                url: '<?php echo Router::url(['controller' => 'Users', 'action' => 'ajax_password']);?>',
                method: 'POST',
                data: {'password': password},
                success: function(data) {
                    try{ 
                        let content = JSON.parse(data),
                            score = content['entropie'],
                            progressBar = $('#jauge-password').find('.progress .progress-bar');

                        progressBar.removeClass('progress-bar-striped');

                        if (!$this.val()) {
                            $this.parent().find('span.color-dot').removeClass('green')
                                .removeClass('red')
                                .addClass('empty');
                        } else {
                            if (score > minScore) {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('red')
                                    .addClass('green');
                            } else {
                                $this.parent().find('span.color-dot').removeClass('empty')
                                    .removeClass('green')
                                    .addClass('red');
                            }
                        }
                        
                        if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_WEAK]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-1');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_MEDIUM]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-2');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-4')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-3');
                        } else if (score < <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?>) {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-5')
                                .addClass('progress-bar-password-force-4'); 
                        } else {
                            progressBar
                                .removeClass('progress-bar-password-force-1')
                                .removeClass('progress-bar-password-force-2')
                                .removeClass('progress-bar-password-force-3')
                                .removeClass('progress-bar-password-force-4')
                                .addClass('progress-bar-password-force-5');
                        }

                        progressBar
                            .attr('aria-valuenow', score)
                            .width(score / <?=$thresholds[PasswordStrengthMeterAnssi::STRENGTH_VERY_STRONG]?> * 100 + '%');
                    }catch(e){
                        console.error(e);
                        return;
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        inputPassword.change(function() {
            $('#confirm-password').val('').parent().find('span.color-dot')
                .addClass('empty')
                .removeClass('green')
                .removeClass('red');
        });
        
        $('#confirm-password').keyup(function() {
            if (!$(this).val()) {
                $(this).parent().find('span.color-dot').addClass('empty')
                    .removeClass('green')
                    .removeClass('red');
            } else if ($(this).val() === inputPassword.val()) {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('red')
                    .addClass('green');
            } else {
                $(this).parent().find('span.color-dot').removeClass('empty')
                    .removeClass('green')
                    .addClass('red');
            }
        });
        
    });
</script>
