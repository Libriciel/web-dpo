<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

if (empty($configLoginExistante)) {
    ?>
    <ls-lib-login-config></ls-lib-login-config>
    <?php
} else {
    ?>
    <ls-lib-login-config configuration='<?php echo $configLoginExistante; ?>'></ls-lib-login-config>
    <?php
}
?>

<script type='text/javascript'>
    $(document).ready(function() {

        document.getElementsByTagName('ls-lib-login-config')[0].addEventListener('save', function(e) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/admins/changeViewLogin',
                data: e['detail']
            });
        });

        document.getElementsByTagName('ls-lib-login-config')[0].addEventListener('back', function(e) {
            window.location.href = "<?php echo Configure::read('BaseUrl') . "/checks"; ?>";
        });

    });
</script>

