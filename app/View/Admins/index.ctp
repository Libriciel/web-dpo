<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

echo $this->element('Buttons/add', [
    'controller' => 'admins',
    'labelBtn' => __d('admin','admin.btnAjouterSuperAdmin'),
    'before' => '<div class="text-left">',
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten((array)$this->request->data, '.')
]);
$pagination = $this->element('pagination');

echo $pagination;
?>

<!-- Tableau -->
<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <!-- Utilisateur -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauUtilisateur');
                ?>
            </th>

            <!-- Identifiant -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauIdentifiant');
                ?>
            </th>

            <!-- Profil -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauProfil');
                ?>
            </th>

            <!-- Actions -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauAction');
                ?>
            </th>
        </tr>
    </thead>

    <!-- Info tableau -->
    <tbody>
        <?php
        foreach ($admins as $donnees) {
            ?>
            <tr class="d-flex">
                <!--Utilisateur-->
                <td class="tdleft col-md-3">
                    <?php
                    echo $donnees['User']['civilite'] . ' ' .  $donnees['User']['prenom'] . ' ' . $donnees['User']['nom'];
                    ?>
                </td>

                <!--Identifiant-->
                <td class="tdleft col-md-3">
                    <?php
                    echo $donnees['User']['username'];
                    ?>
                </td>

                <!--Profil de l'utilisateur-->
                <td class="tdleft col-md-3">
                    <?php
                    echo __d('admin', 'admin.textSuperadministrateur');
                    ?>
                </td>

                <!-- Action possible d'effectuer en fonction des droits de l'utilisateur -->
                <td class="tdleft col-md-3">
                    <div class="buttons">
                        <?php
                        // Bouton de modification
                        echo $this->element('Buttons/edit', [
                            'controller' => 'admins',
                            'id' => $donnees['User']['id'],
                            'titleBtn' => __d('user', 'user.commentaireModifierUser')
                        ]);

                        if ($donnees['User']['id'] != 1 &&
                            $donnees['User']['id'] != $this->Session->read('Auth.User.id')
                        ) {
                            // Bouton de suppression
                            echo $this->Html->link('<i class="fa fa-trash fa-lg fa-danger"></i>', [
                                'controller' => 'admins',
                                'action' => 'delete',
                                $donnees['Admin']['id'],
                                $donnees['User']['id']
                            ], [
                                'class' => 'btn',
                                'title' => __d('user', 'user.commentaireSupprimerUser'),
                                'escapeTitle' => false
                            ], __d('user', 'user.confirmationSupprimerUser') . $donnees['User']['prenom'] . ' ' . $donnees['User']['nom'] . ' ?');
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
echo $pagination;

echo $this->element('Buttons/add', [
    'controller' => 'admins',
    'labelBtn' => __d('admin','admin.btnAjouterSuperAdmin')
]);
