<?php
echo $this->Html->script('roles.js');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

//Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
if ($this->Autorisation->authorized(13, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'roles',
        'labelBtn' => __d('role', 'role.btnAjouterProfil'),
        'before' => '<div class="text-left">',
    ]);
}

echo '<br>';

if (!empty($roles)) {
    ?>
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- Profil -->
                <th class="col-md-2">
                    <?php echo __d('role', 'role.titreTableauProfil'); ?>
                </th>

                <!-- Droits -->
                <th class="col-md-8">
                    <?php echo __d('role', 'role.titreTableauDroit'); ?>
                </th>

                <!-- Actions -->
                <th class='col-md-2'>
                    <?php echo __d('role', 'role.titreTableauAction'); ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($roles as $donnees) {
                ?>
                <tr class="d-flex">
                    <!-- Libelle profil -->
                    <td class="tdleft col-md-2">
                        <?php
                        echo $donnees['Role']['libelle'];
                        ?>
                    </td>

                    <!-- Droits accordé au profil -->
                    <td class="tdleft col-md-8">
                        <?php
                        foreach (Hash::extract($donnees, 'ListeDroit.{n}.libelle') as $key => $droit) {
                            echo '<li>' . h($droit) . '</li>';
                        }
                        ?>
                    </td>

                    <!-- Bouton -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if ($this->Autorisation->authorized(14, $droits)) {
                                // Bouton edit
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'roles',
                                    'id' => $donnees['Role']['id'],
                                    'titleBtn' => __d('role', 'role.commentaitreModifierProfil')
                                ]);
                            }

                            if ($this->Autorisation->authorized(15, $droits)) {
                                if ($donnees['Role']['users_count'] == 0) {
                                    if ($donnees['Role']['id'] !== $roleDPO_id) {
                                        //Bouton de suppression
                                        echo $this->element('Buttons/delete', [
                                            'controller' => 'roles',
                                            'id' => $donnees['Role']['id'],
                                            'titleBtn' => __d('role', 'role.commentaireSupprimerProfil'),
                                            'confirmation' => __d('role', 'role.confirmationSupprimerProfil') . $donnees['Role']['libelle'] . ' ?',
                                        ]);
                                    }
                                }
                            }

                            if ($this->Autorisation->authorized([13, 14, 15], $droits)) {
                                if ($donnees['Role']['users_count'] > 0) {
                                    //Bouton permettant de recharger les droits sur tous les utilisateurs de l'entité concernée
                                    echo $this->element('Buttons/reload', [
                                        'controller' => 'roles',
                                        'action' => 'reattributionRoles',
                                        'id' => $donnees['Role']['id'],
                                        'titleBtn' => __d('role', 'role.commentaireActualiserDroitsUtilisateurs'),
                                    ]);
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    echo "<div class='text-center'><h3>Il n'existe aucun profil <small>pour cette entité</small></h3></div>";
}

//Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
if ($this->Autorisation->authorized(13, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'roles',
        'labelBtn' => __d('role', 'role.btnAjouterProfil'),
    ]);
}
