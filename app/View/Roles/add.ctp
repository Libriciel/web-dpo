<?php
$breadcrumbs = [
    __d('role', 'role.titreListeProfil') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Role']) && !empty($this->validationErrors['Role'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <i class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></i>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Role', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'id' => [
                'type' => 'hidden'
            ],
            'libelle' => [
                'id' => 'libelle',
                'required' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <fieldset id="droits-block" class="organisation-block">
            <?php
            echo $this->Html->tag('legend', 'Droits');

            echo $this->WebcilForm->input('ListeDroit.ListeDroit', [
                'label' => [
                    'text' => '',
                    'class' => null
                ],
                'multiple' => 'checkbox',
                'class' => 'form-check',
                'options' => $options['ListeDroit']['ListeDroit'],
            ]);
            ?>
        </fieldset>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
