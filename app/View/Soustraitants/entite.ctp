<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('soustraitant', 'soustraitant.btnFiltrerSoustraitant')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer sur le siret
            echo $this->element('Filtres/Parametrages/siret', [
                'fieldname' => 'siretstructure',
                'options' => $options['siretstructure']
            ]);

            // Filtrer sur le code APE
            echo $this->element('Filtres/Parametrages/ape', [
                'options' => $options['apestructure']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par raison sociale
            echo $this->element('Filtres/Parametrages/raisonsociale', [
                'options' => $options['raisonsocialestructure']
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
$pagination = null;

if (!empty($soustraitants)) {
    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- Structure -->
                <th class="col-md-3">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauStructure');
                    ?>
                </th>

                <!--  Responsable -->
                <th class="col-md-3">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauResponsable');
                    ?>
                </th>

                <!-- Information sur le Dpo -->
                <th class="col-md-3">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauInfoDPO');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-3">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($soustraitants as $soustraitant) {
                ?>
                <tr class="d-flex">
                    <!-- Structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $soustraitant['Soustraitant']['raisonsocialestructure'];

                        echo '<ul><li><strong>Siret : </strong>'.$soustraitant['Soustraitant']['siretstructure'].'</li></ul>';

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephonestructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.faxstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fax : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.emailstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Responsable de la structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        if (empty($soustraitant['Soustraitant' ]['nom_complet']) === false) {
                            echo $soustraitant['Soustraitant' ]['nom_complet'];
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.fonctionresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fonction : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephoneresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.emailresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- DPO de la structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        if (empty($soustraitant['Soustraitant' ]['nom_complet_dpo']) === false) {
                            echo $soustraitant['Soustraitant' ]['nom_complet_dpo'];
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.numerocnil_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Numéro CNIL : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.email_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephonefixe_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone fixe : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephoneportable_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone portable : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-3">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/show', [
                                'controller' => 'soustraitants',
                                'id' => $soustraitant['Soustraitant']['id'],
                                'titleBtn' => __d('soustraitant', 'soustraitant.commentaireBtnVisualiserSoustraitant')
                            ]);

                            echo $this->element('Buttons/dissocier', [
                                'controller' => 'soustraitants',
                                'action' => 'dissocierSoustraitant',
                                'id' => $soustraitant['Soustraitant']['id'],
                                'titleBtn' => __d('soustraitant', 'soustraitant.commentaireBtnSupprimerSoustraitantEntite'),
                                'confirmation' => __d('soustraitant', 'soustraitant.confirmationSupprimerSoustraitantEntite') . ' " ' . $soustraitant['Soustraitant']['raisonsocialestructure'] . ' " de votre entité  ?',
                            ]);
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('soustraitant', 'soustraitant.textAucunSoustraitantEntite')
                : __d('soustraitant', 'soustraitant.textFiltreAucunSoustraitantEntite');
            ?>
        </h3>
    </div>
    <?php
}
