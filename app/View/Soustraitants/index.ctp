<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
if (array_key_exists('Organisation', $this->Session->read()) === true) {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
} else {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('soustraitant', 'soustraitant.btnFiltrerSoustraitant')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par entité associée
            echo $this->element('Filtres/Parametrages/organisation_associee', [
                'options' => $options['organisations']
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $this->element('Filtres/Parametrages/organisation_creatrice', [
                'options' => $options['organisations']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par raison sociale
            echo $this->element('Filtres/Parametrages/raisonsociale', [
                'options' => $options['raisonsocialestructure']
            ]);

            // Filtrer sur le siret
            echo $this->element('Filtres/Parametrages/siret', [
                'fieldname' => 'siretstructure',
                'options' => $options['siretstructure']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer sur le code APE
            echo $this->element('Filtres/Parametrages/ape', [
                'options' => $options['apestructure']
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>
    
<?php
if (!empty($soustraitants)) {
    echo $this->element('Buttons/add', [
        'controller' => 'soustraitants',
        'labelBtn' => __d('soustraitant','soustraitant.btnAjouterSoustraitant'),
        'before' => '<div class="text-left">',
    ]);

    $pagination = null;

    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;

    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('soustraitant', 'soustraitant.btnAffecterEntite')
    ]);
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                    <input id="soustraitantEntiteCheckbox" type="checkbox" class="soustraitantEntiteCheckbox_checkbox" />
                </th>

                <!-- Structure -->
                <th class="col-md-2">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauStructure');
                    ?>
                </th>

                <!--  Responsable -->
                <th class="col-md-2">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauResponsable');
                    ?>
                </th>

                <!-- Information sur le Dpo -->
                <th class="col-md-2">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauInfoDPO');
                    ?>
                </th>

                <!-- Entité associée -->
                <th class="col-md-2">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauEntiteSoustraitant');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('soustraitant', 'soustraitant.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($soustraitants as $soustraitant) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer un soustraitant à une entité -->
                    <td class="tdleft col-md-1">
                        <input id='checkbox<?php echo $soustraitant['Soustraitant']['id']; ?>' type="checkbox" class="soustraitantEntiteCheckbox" value="<?php echo $soustraitant['Soustraitant']['id']; ?>" >
                    </td>

                    <!-- Structure -->
                    <td class="tdleft col-md-2">
                        <?php
                        echo $soustraitant['Soustraitant']['raisonsocialestructure'];

                        $items = Hash::extract($soustraitant, 'Soustraitant.siretstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Siret : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephonestructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.faxstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fax : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.emailstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Responsable de la structure -->
                    <td class="tdleft col-md-2">
                        <?php
                        if (empty($soustraitant['Soustraitant' ]['nom_complet']) === false) {
                            echo $soustraitant['Soustraitant' ]['nom_complet'];
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.fonctionresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fonction : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephoneresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.emailresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- DPO de la structure -->
                    <td class="tdleft col-md-2">
                        <?php
                        if (empty($soustraitant['Soustraitant' ]['nom_complet_dpo']) === false) {
                            echo $soustraitant['Soustraitant' ]['nom_complet_dpo'];
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.numerocnil_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Numéro CNIL : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.email_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephonefixe_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone fixe : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($soustraitant, 'Soustraitant.telephoneportable_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone portable : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Entité associée -->
                    <td class="tdleft col-md-2">
                        <?php
                        $items = Hash::extract($soustraitant, 'Organisation.{n}.raisonsociale');
                        if (empty($items) === false) {
                            echo '<li>'.implode('</li><li>', $items).'</li>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/show', [
                                'controller' => 'soustraitants',
                                'id' => $soustraitant['Soustraitant']['id'],
                                'titleBtn' => __d('soustraitant', 'soustraitant.commentaireBtnVisualiserSoustraitant')
                            ]);

                            if ($this->Session->read('Su') ||
                                $soustraitant['Soustraitant']['createdbyorganisation'] == $this->Session->read('Organisation.id')
                            ) {
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'soustraitants',
                                    'id' => $soustraitant['Soustraitant']['id'],
                                    'titleBtn' => __d('soustraitant', 'soustraitant.commentaireBtnModifierSoustraitant')
                                ]);

                                if (empty($soustraitant['Organisation']) === true && empty($soustraitant['Fiche'])) {
                                    echo $this->element('Buttons/delete', [
                                        'controller' => 'soustraitants',
                                        'id' => $soustraitant['Soustraitant']['id'],
                                        'titleBtn' => __d('soustraitant', 'soustraitant.commentaireBtnSupprimerSoustraitant'),
                                        'confirmation' => __d('soustraitant', 'soustraitant.confirmationSupprimerSoustraitant') . $soustraitant['Soustraitant']['raisonsocialestructure'] . ' ?',
                                    ]);
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('soustraitant', 'soustraitant.textAucunSoustraitant')
                : __d('soustraitant', 'soustraitant.filtreTextAucunSoustraitant');
            ?>
        </h3>
    </div>
    <?php
}

echo $this->element('Buttons/add', [
    'controller' => 'soustraitants',
    'labelBtn' => __d('soustraitant','soustraitant.btnAjouterSoustraitant'),
]);

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Soustraitant',
    'modalTitle' => __d('soustraitant', 'soustraitant.popupTitreAffecterSousTraitant'),
    'formulaireName' => 'SoustraitantOrganisation',
    'fieldName' => 'soustraitant_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'soustraitantEntiteCheckbox'
]);
