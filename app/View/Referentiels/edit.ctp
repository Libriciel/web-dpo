<?php

$breadcrumbs = [
    __d('referentiel', 'referentiel.titreIndex') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Referentiel']) && !empty($this->validationErrors['Referentiel'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Referentiel',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
    'type' => 'file'
]);
?>

<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->WebcilForm->inputs([
            'Referentiel.name' => [
                'id' => 'name',
                'required' => true
            ],
            'Referentiel.description' => [
                'id' => 'description',
                'class' => 'referentiel_description',
                'type' => 'textarea',
                'required' => true
            ]
        ]);

        echo $this->TinyMCE->load('referentiel_description');

        if (empty($this->request->data('Referentiel.fichier'))) {
            // Ajouter le PDF
            echo $this->WebcilForm->input('Referentiel.fichier', [
                'id' => 'logo_file',
                'type' => 'file',
                'class' => 'filestyle',
                'data-text' => ' Ajouter le PDF du référentiel',
                'data-buttonBefore' => 'true',
                'accept' => 'application/pdf',
                'label' => [
                    'text' => false
                ]
            ]);
        } else {
            ?>
            <table class="table" id="render">
                <thead></thead>
                <tbody>
                <tr class="d-flex">
                        <td class="col-md-1">
                            <i class="far fa-file-alt fa-lg"></i>
                        </td>

                        <td class="col-md-9">
                            <?php
                            echo $this->request->data('Referentiel.name_fichier');
                            ?>
                        </td>

                        <td class="col-md-2">
                            <?php
                            echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                                'controller' => 'referentiels',
                                'action' => 'download',
                                $referentiel['Referentiel']['fichier'],
                                $referentiel['Referentiel']['name_fichier']
                            ], [
                                'class' => 'btn btn-outline-dark borderless boutonShow btn-sm my-tooltip',
                                'title' => 'Télécharger le référentiel',
                                'escapeTitle' => false
                            ]);

                            echo $this->Html->link('<i class="fa fa-trash fa-lg"></i>', [
                                'controller' => 'referentiels',
                                'action' => 'deleteFileSave',
                                $referentiel['Referentiel']['id'],
                                $referentiel['Referentiel']['fichier'],
                            ], [
                                'class' => 'btn btn-outline-danger borderless boutonShow btn-sm my-tooltip',
                                'title' => __d('norme','norme.btnSupprimerFichierNorme'),
                                'escapeTitle' => false
                            ]);
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
        }
        ?>
    </div>
</div>

<div class="row">
    <!-- Groupe bouton -->
    <div class="col-md-12 top17 text-center">
        <div class="buttons">
            <?php
            // Groupe de boutons
            echo $this->WebcilForm->buttons(['Cancel', 'Save']);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();
