<?php
$this->Breadcrumbs->breadcrumbs([
    __d('referentiel', 'referentiel.titreIndex') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

echo $this->WebcilForm->create('Article', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);

echo $this->request->data('Referentiel.description');

if (!empty($this->request->data('Referentiel.fichier'))) {
    ?>
    <hr/>
    <div class="col-md-12 top30">
        <h4>
            <?php echo __d('article', 'article.textInfoPieceJointe'); ?>
        </h4>
        <table>
            <tbody>
                <tr>
                    <td class="col-md-1">
                        <i class="far fa-file-alt fa-fw"><!----></i>
                    </td>
                    <td class="col-md-9 tdleft">
                        <?php echo $this->request->data('Referentiel.name_fichier'); ?>
                    </td>
                    <td class="col-md-2">
                        <?php
                        echo $this->Html->link('<span class="fa fa-download fa-lg"><!----></span>', [
                            'controller' => 'referentiels',
                            'action' => 'download',
                            $this->request->data('Referentiel.fichier'),
                            $this->request->data('Referentiel.name_fichier'),
                        ], [
                            'class' => 'btn btn-outline-dark boutonShow btn-sm my-tooltip',
                            'title' => 'Télécharger le fichier',
                            'escapeTitle' => false
                        ]);
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
}
?>

<div style="clear: both">
    <?php
    echo $this->WebcilForm->buttons(['Back']);
    ?>
</div>

<?php
echo $this->WebcilForm->end();
?>
