<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
?>

<div class="text-left">
    <?php
    // Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
    if ($this->Autorisation->authorized(40, $droits)) {
        echo $this->element('Buttons/add', [
            'controller' => 'referentiels',
            'labelBtn' => __d('referentiel', 'referentiel.btnAjouterReferentiel'),
            'before' => '<div class="text-left">',
        ]);
    }
    ?>
</div>

<?php
if (!empty($referentiels)) {
    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- Abroger -->
                <th class="col-md-1">
                    <?php
                    echo __d('referentiel', 'referentiel.titreTableauEtat');
                    ?>
                </th>

                <!-- Libelle -->
                <th class="col-md-8">
                    <?php
                    echo __d('referentiel', 'referentiel.titreTableauLibelle');
                    ?>
                </th>

                <!-- Actions -->
                <th class='col-md-2'>
                    <?php
                    echo __d('referentiel', 'referentiel.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($referentiels as $referentiel) {
                ?>
                <tr class="d-flex">
                    <!-- Status du formulaire -->
                    <td class="tdleft col-md-1">
                        <?php
                        if ($referentiel['Referentiel']['abroger'] === true) {
                            ?>
                            <div class="text-center">
                                <span class="badge badge-danger">
                                    <?php
                                    echo __d('referentiel', 'referentiel.titreTableauAbroger');
                                    ?>
                                </span>
                            </div>
                            <?php
                        }
                        ?>
                    </td>

                    <!-- Libelle -->
                    <td class="tdleft col-md-8">
                        <?php
                        echo $referentiel['Referentiel']['name'];
                        ?>
                    </td>

                    <!-- Bouton -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if (!empty($referentiel['Referentiel']['fichier'])) {
                                echo $this->element('Buttons/download', [
                                    'controller' => 'referentiels',
                                    'fichier' => $referentiel['Referentiel']['fichier'],
                                    'name_modele' => $referentiel['Referentiel']['name_fichier'],
                                    'titleBtn' => 'Télécharger le référentiel'
                                ]);
                            }

                            if ($this->Autorisation->authorized(41, $droits)) {
                                echo $this->element('Buttons/show', [
                                    'controller' => 'referentiels',
                                    'id' => $referentiel['Referentiel']['id'],
                                    'titleBtn' => __d('referentiel', 'referentiel.commentaireBtnVisualiserReferentiel')
                                ]);
                            }

                            if ($this->Autorisation->authorized(42, $droits) &&
                                $referentiel['Referentiel']['abroger'] == false
                            ) {
                                // Bouton de modification
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'referentiels',
                                    'id' => $referentiel['Referentiel']['id'],
                                    'titleBtn' => __d('referentiel', 'referentiel.commentaitreModifierReferentiel')
                                ]);
                            }

                            if ($this->Autorisation->authorized(43, $droits)) {
                                if ($referentiel['Referentiel']['abroger'] == false) {
                                    $titleBtnAbroger = __d('referentiel', 'referentiel.commentaireBtnAbrogerReferentiel');
                                } else {
                                    $titleBtnAbroger = __d('referentiel', 'referentiel.commentaireBtnRevoquerAbrogerReferentiel');
                                }
                                echo $this->element('Buttons/abroger', [
                                    'controller' => 'referentiels',
                                    'id' => $referentiel['Referentiel']['id'],
                                    'active' => $referentiel['Referentiel']['abroger'],
                                    'titleBtn' => $titleBtnAbroger
                                ]);
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('referentiel', 'referentiel.AucunReferentiel')
                : __d('referentiel', 'referentiel.filtreAucunReferentiel');
            ?>
        </h3>
    </div>
    <?php
}

//Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
if ($this->Autorisation->authorized(40, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'referentiels',
        'labelBtn' => __d('referentiel', 'referentiel.btnAjouterReferentiel'),
    ]);
}
