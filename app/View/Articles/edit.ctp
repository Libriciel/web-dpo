<?php
$this->Breadcrumbs->breadcrumbs([
    __d('article', 'article.titreIndex') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

if (isset($this->validationErrors['Article']) && !empty($this->validationErrors['Article']) ||
    isset($this->validationErrors['Fichierarticle']) && !empty($this->validationErrors['Fichierarticle'])
) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            if (isset($this->validationErrors['Article']) && !empty($this->validationErrors['Article'])) {
                foreach ($this->validationErrors['Article'] as $errorChamps) {
                    foreach ($errorChamps as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }

            if (isset($this->validationErrors['Fichierarticle']) && !empty($this->validationErrors['Fichierarticle'])) {
                foreach ($this->validationErrors['Fichierarticle'] as $errorChamps) {
                    foreach ($errorChamps as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Article', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
    'type' => 'file'
]);
?>

<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#article">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('article', 'article.ongletArticle');
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fas fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('article', 'article.ongletAnnexe');
                ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <!-- Onglet concernant l'article -->
        <div id="article" class="tab-pane active">
            <br/>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo $this->WebcilForm->inputs([
                        'Organisation.Organisation' => [
                            'class' => 'form-control usersDeroulant',
                            'options' => $mesOrganisations,
                            'label' => [
                                'text' => __d('article', 'article.champSelectOrganisation'),
                            ],
                            'data-placeholder' => __d('article', 'article.placeholderSelectOrganisation'),
                            'empty' => true,
                            'multiple' => true,
                        ],
                        'Article.name' => [
                            'id' => 'name',
                            'required' => true
                        ],
                        'Article.description' => [
                            'id' => 'description',
                            'class' => 'article_description',
                            'type' => 'textarea'
                        ]
                    ]);

                    echo $this->TinyMCE->load('article_description');
                    ?>
                </div>
            </div>
        </div>
        <!-- Fin onglet concernant l'article-->

        <!-- Onglet Annexe(s) -->
        <div id="annexe" class="tab-pane">
            <br/>
            <h4>
                <?php
                echo __d('article', 'article.textAjouterPieceJointe');
                ?>
            </h4>

            <br/>
            <!-- Texte format fichier accepté -->
            <div class="alert alert-warning" role="alert">
                <?php
                echo __d('article', 'article.textTypeFichierAccepter');
                ?>
            </div>

            <?php
            $i = $this->Html->tag(
                'i',
                '<!---->',
                ['class' => 'fas fa-cloud-upload-alt fa-4x text-primary']
            );

            $p = $this->Html->tag(
                'p',
                ('Glissez-déposez vos fichiers ici')
            );

            echo $this->Html->tag(
                'div',
                $i.$p,
                ['class' => 'dropbox']
            );
            ?>

            <br/>

            <table class="table" id="render">
                <tbody>
                <?php
                if (!empty($files)) {
                    foreach ($files as $key => $file) {
                        ?>
                        <tr id="rowArticle<?php echo $key; ?>">

                            <td class="col-md-1">
                                <i class="far fa-file-alt fa-lg"></i>
                            </td>

                            <td class="col-md-9 tdleft">
                                <?php echo $file ?>
                            </td>

                            <td class="col-md-2">
                                <button  type="button" class="btn btn-warning" onclick="deleteFile('<?php echo $file ?>','<?php echo $key ?>')">
                                    <i class="fa fa-times-circle"></i>
                                    Annuler
                                </button>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>

            <?php
                if (!empty($filesSave)) {
                    ?>
                    <div class="col-md-12 top30">
                        <h4>
                            <?php echo __d('article', 'article.textInfoPieceJointe'); ?>
                        </h4>
                        <table class="table">
                            <tbody>
                            <?php
                            foreach ($filesSave as $val) {
                                ?>
                                <tr id="rowArticle<?php echo $val['Fichierarticle']['id']; ?>">
                                    <td class="col-md-1">
                                        <i class="far fa-file-alt fa-lg"></i>
                                    </td>
                                    <td class="col-md-9 tdleft">
                                        <?php echo $val['Fichierarticle']['nom']; ?>
                                    </td>
                                    <td class="col-md-2 boutonsFile boutonsFile"<?php echo $val['Fichierarticle']['id']; ?>>
                                        <?php
                                        echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                                            'controller' => 'articles',
                                            'action' => 'download',
                                            $val['Fichierarticle']['url'],
                                            $val['Fichierarticle']['nom']
                                        ], [
                                            'class' => 'btn btn-outline-dark boutonShow btn-sm my-tooltip',
                                            'title' => 'Télécharger le fichier',
                                            'escapeTitle' => false
                                        ]);
                                        ?>

                                        <button  type="button" class="btn btn-outline-danger btn-sm my-tooltip left5 btn-del-file" onclick="deleteFileSave('<?php echo $val['Fichierarticle']['id'] ?>','<?php echo $val['Fichierarticle']['url'] ?>')">
                                            <i class="fa fa-trash fa-lg"></i>
                                        </button>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <hr/>
                    <?php
                }
            ?>
        </div>
        <!-- Fin onglet Annexe(s) -->
    </div>

    <div class="row">
        <!-- Groupe bouton -->
        <div class="col-md-12 top17 text-center">
            <div class="buttons">
                <?php
                // Groupe de boutons
                echo $this->WebcilForm->buttons(['Cancel', 'Save']);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();

echo $this->element('Fiches/modal/modalErrorExtentionAnnexe', [
    'extentionsAccepter' => __d('formulaire', 'formulaire.infoExtentionUtilisable')
]);
?>

<script type="text/javascript">

    $(document).ready(function () {
        $.fn.extend({
            logEvents: function() {
                return this.each(function () {
                    $(this).onAny(function(event) {
                        console.log(event.target, event.type);
                    });
                });
            }
        });

        $.fn.onAny = function(cb){
            for (let k in this[0])
                if (k.search('on') === 0)
                    this.on(k.slice(2), function(e){
                        // Probably there's a better way to call a callback function with right context, $.proxy() ?
                        cb.apply(this,[e]);
                    });
            return this;
        };

        $('.dropbox').onAny(function(event) {
            event.preventDefault();
            $(this).removeClass('dragover');
        });

        $('.dropbox').on('dragover', function() {
            $(this).addClass('dragover');
        });

        $('.dropbox').on('dragout, dragleave, dragexit', function() {
            $(this).removeClass('dragover');
        });

        $('.dropbox').on('drop', function(event) {
            let formData = new FormData(),
                files = [];

            for (let key in event.originalEvent.dataTransfer.files) {
                formData.append('fichiers[]', event.originalEvent.dataTransfer.files[key]);
            }

            $.ajax({
                url: '/articles/saveFileTmp',
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    try{
                        let content = JSON.parse(data);
                        for (let key in content) {
                            if (content[key].status === "success") {
                                let tr = $('<tr id="rowArticle' + key + '"></tr>')
                                    .append('<td class="col-md-1"><i class="far fa-file-alt fa-lg"><!----></i></td>')
                                    .append('<td  class="col-md-9 tdleft">' + content[key].filename + '</td>')
                                    .append('<td  class=\"col-md-2\"><button  type="button" class="btn btn-warning" onclick=\"deleteFile(\'' + content[key].filename + '\',\'' + key + '\')\"><i class="fa fa-times-circle"><!----></i> Annuler</button></td>')
                                $('#render').find('tbody').append(tr);
                            } else {
                                $('#errorExtentionAnnexe').modal('show');
                            }
                        }
                    } catch(e){
                        alert("error");
                        return;
                    }
                },
                error: function() {
                    alert('error');
                }
            });
        });

        // Mise en évidence des onglets ayant des erreurs.
        $("div.form-group .error-message").closest("div.tab-pane").each(function(idx, pane) {
            let a = $( "a[href='#"+$(pane).attr('id')+"']" );
            $(a).closest("li").addClass("is-invalid");
            $(a).append("<i class='fa fa-exclamation-circle fa-danger' aria-hidden='true'></i>");
        });
    });

    function deleteFile(file, key){
        let validation = confirm("Voulez vous supprimer l'annexe ?");

        if (validation === true) {
            $.ajax({
                url: '/articles/deleteFile',
                method: 'POST',
                data: {filename: file},
                success: function () {
                    let row = document.getElementById("rowArticle" + key);
                    row.parentNode.removeChild(row);
                },
                error: function () {
                    alert('Erreur lors de la suppression du fichier');
                }
            });
        }
    }

    function deleteFileSave(idFile, urlFile){
        let validation = confirm("Voulez vous supprimer l'annexe ?");

        if (validation === true) {
            $.ajax({
                url: '/articles/deleteRecordingFile',
                method: 'POST',
                data: {
                    idFile: idFile,
                    urlFile: urlFile
                },
                success: function () {
                    let row = document.getElementById("rowArticle" + idFile);
                    row.parentNode.removeChild(row);
                },
                error: function () {
                    alert('Erreur lors de la suppression du fichier');
                }
            });
        }
    }
</script>
