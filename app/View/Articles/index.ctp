<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (!empty($articles)) {
    // Bouton du filtre de la liste
    echo $this->element('Buttons/filtre', [
        'titleBtn' => __d('article', 'article.btnFiltrerArticle')
    ]);

    $filters = $this->request->data;

    unset($filters['sort'], $filters['direction'], $filters['page'], $filters['article']['nbAffichage']);
    if (empty($filters['Article'])) {
        unset($filters['Article']);
    }
    ?>

    <div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
        <?php
        echo $this->Form->create('Filtre', [
            'url' => [
                'controller' => $this->request->params['controller'],
                'action' => $this->request->params['action']
            ],
            'class' => 'search-form'
        ]);
        ?>

        <div class="row col-md-12">
            <div class="col-md-6">
                <?php
                // Filtrer par titre de l'article
                echo $this->Form->input('name', [
                    'class' => 'usersDeroulant form-control',
                    'label' => __d('article', 'article.champFiltreNameArticle'),
                    'empty' => true,
                    'data-placeholder' => __d('article', 'article.placeholderFiltreNameArticle'),
                    'options' => $options['name'],
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
                ?>
            </div>

            <br>

            <div class="col-md-6">
                <?php
                // Filtrer par entité à l'origine de l'article
                echo $this->Form->input('createdbyorganisation_id', [
                    'class' => 'usersDeroulant form-control',
                    'label' => __d('article', 'article.champFiltreOrganisationOrigine'),
                    'empty' => true,
                    'data-placeholder' => __d('article', 'article.placeholderFiltreOrganisationOrigine'),
                    'options' => $mesOrganisations,
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
                ?>
            </div>
        </div>

        <?php
        echo $this->element('Filtres/Parametrages/buttons');
        echo $this->Form->end();
        ?>
    </div>

    <?php
    if ($this->Autorisation->authorized(28, $droits)) {
        echo $this->element('Buttons/add', [
            'controller' => 'articles',
            'labelBtn' => __d('article', 'article.btnAjouterArticle'),
            'before' => '<div class="text-left">',
        ]);
    }

    $pagination = null;

    $this->Paginator->options([
        'url' => Hash::flatten((array)$this->request->data, '.')
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;

    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('article', 'article.btnAffecterArticleAEntite')
    ]);
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                    <input id="articleEntiteCheckbox" type="checkbox" class="articleEntiteCheckbox_checkbox"/>
                </th>

                <!-- Nom de l'article -->
                <th class="col-md-4">
                    <?php
                    echo __d('article', 'article.titreTableauNameArticle');
                    ?>
                </th>

                <!-- Visible par -->
                <th class="col-md-3">
                    <?php
                    echo __d('article', 'article.titreTableauEntiteArticle');
                    ?>
                </th>

                <!-- Créer par -->
                <th class="col-md-2">
                    <?php
                    echo __d('article', 'article.titreTableauEntiteCreatriceArticle');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('article', 'article.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($articles as $article) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer un Article à une entité -->
                    <td class="tdleft col-md-1">
                        <input id='checkbox<?php echo $article['Article']['id']; ?>' type="checkbox"
                               class="articleEntiteCheckbox" value="<?php echo $article['Article']['id']; ?>">
                    </td>

                    <!-- Nom de l'article -->
                    <td class="tdleft col-md-4">
                        <?php
                        echo $article['Article']['name'];
                        ?>
                    </td>

                    <!-- Visible par -->
                    <td class="tdleft col-md-3">
                        <?php
                        $items = Hash::extract($article, 'Organisation.{n}.raisonsociale');
                        if (empty($items) === false) {
                            echo '<li>' . implode('</li><li>', $items) . '</li>';
                        }
                        ?>
                    </td>

                    <!-- Créer par -->
                    <td class="tdleft col-md-2">
                        <?php
                        echo $article['Createdbyorganisation']['raisonsociale'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if ($this->Autorisation->authorized(30, $droits)) {
                                echo $this->element('Buttons/show', [
                                    'controller' => 'articles',
                                    'id' => $article['Article']['id'],
                                    'titleBtn' => __d('article', 'article.commentaireBtnVisualiserArticle')
                                ]);
                            }

                            if ($article['Article']['createdbyorganisation_id'] == $this->Session->read('Organisation.id')) {
                                if ($this->Autorisation->authorized(29, $droits)) {
                                    echo $this->element('Buttons/edit', [
                                        'controller' => 'articles',
                                        'id' => $article['Article']['id'],
                                        'titleBtn' => __d('article', 'article.commentaireBtnModifierArticle')
                                    ]);
                                }

                                if ($this->Autorisation->authorized(31, $droits)) {
                                    if (empty($article['Organisation'])) {
                                        echo $this->element('Buttons/delete', [
                                            'controller' => 'articles',
                                            'id' => $article['Article']['id'],
                                            'titleBtn' => __d('article', 'article.commentaireBtnSupprimerArticle'),
                                            'confirmation' => __d('article', 'article.confirmationSupprimerArticle') . $article['Article']['name'] . ' ?',
                                        ]);
                                    }
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('article', 'article.textAucunArticle');
            ?>
        </h3>
    </div>
    <?php
}

if ($this->Autorisation->authorized(28, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'articles',
        'labelBtn' => __d('article', 'article.btnAjouterArticle'),
    ]);
}

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Article',
    'modalTitle' => __d('article', 'article.popupTitreAffecterArticle'),
    'formulaireName' => 'ArticleOrganisation',
    'fieldName' => 'article_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'articleEntiteCheckbox'
]);
