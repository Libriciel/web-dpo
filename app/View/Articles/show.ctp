<?php
$this->Breadcrumbs->breadcrumbs([
    __d('article', 'article.titreIndex') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

if (isset($this->validationErrors['Article']) && !empty($this->validationErrors['Article'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Article', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#article">
                <i class="fas fa-eye fa-fw"></i>
                <?php
                echo __d('article', 'article.ongletArticle');
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fas fa-eye fa-fw"></i>
                <?php
                echo __d('article', 'article.ongletAnnexe');
                ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <!-- Onglet concernant l'article -->
        <div id="article" class="tab-pane active">
            <br/>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    echo $this->WebcilForm->inputs([
                        'Organisation.Organisation' => [
                            'class' => 'form-control usersDeroulant',
                            'options' => $mesOrganisations,
                            'label' => [
                                'text' => __d('article', 'article.champSelectOrganisation'),
                            ],
                            'data-placeholder' => __d('article', 'article.placeholderSelectOrganisation'),
                            'empty' => true,
                            'multiple' => true,
                            'readonly' => true
                        ],
                        'Article.name' => [
                            'id' => 'name',
                            'required' => true,
                            'readonly' => true
                        ],
                        'Article.description' => [
                            'id' => 'description',
                            'class' => 'article_description',
                            'type' => 'textarea',
                            'required' => true
                        ]
                    ]);

                    echo $this->TinyMCE->load('article_description', true);
                    ?>
                </div>
            </div>
        </div>
        <!-- Fin onglet concernant l'article-->

        <!-- Onglet Annexe(s) -->
        <div id="annexe" class="tab-pane">
            <br/>

            <?php
            if (!empty($filesSave)) {
                ?>
                <div class="col-md-12 top30">
                    <h4>
                        <?php echo __d('article', 'article.textInfoPieceJointe'); ?>
                    </h4>
                    <table class="table">
                        <tbody>
                        <?php
                        foreach ($filesSave as $val) {
                            ?>
                            <tr id="rowFichier<?php echo $val['Fichierarticle']['id']; ?>">
                                <td class="col-md-1">
                                    <i class="far fa-file-alt fa-lg"></i>
                                </td>
                                <td class="col-md-9 tdleft">
                                    <?php echo $val['Fichierarticle']['nom']; ?>
                                </td>
                                <td class="col-md-2 boutonsFile boutonsFile"<?php echo $val['Fichierarticle']['id']; ?>>
                                    <?php
                                    echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                                        'controller' => 'articles',
                                        'action' => 'download',
                                        $val['Fichierarticle']['url'],
                                        $val['Fichierarticle']['nom']
                                    ], [
                                        'class' => 'btn btn-outline-dark boutonShow btn-sm my-tooltip',
                                        'title' => 'Télécharger le fichier',
                                        'escapeTitle' => false
                                    ]);
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <hr/>
                <?php
            } else {
                ?>
                <div class="col-md-12 top30">
                    <h4>
                        <?php echo __d('article', 'article.textInfoAucunePieceJointe'); ?>
                    </h4>
                </div>
                <?php
            }
            ?>
        </div>
        <!-- Fin onglet Annexe(s) -->
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Back']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">

    $("#OrganisationOrganisation").prop("disabled", true);

</script>
