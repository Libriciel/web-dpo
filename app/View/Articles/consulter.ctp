<?php
$this->Breadcrumbs->breadcrumbs([
    __d('article', 'article.titreIndex') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

echo $this->WebcilForm->create('Article', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);

echo $this->request->data('Article.description');

if (!empty($filesSave)) {
    ?>
    <hr/>
    <div class="col-md-12 top30">
        <h4>
            <?php echo __d('article', 'article.textInfoPieceJointe'); ?>
        </h4>
        <table>
            <tbody>
            <tr>
                <?php
                foreach ($filesSave as $val) {
                ?>
            <tr>
                <td class="col-md-1">
                    <i class="far fa-file-alt fa-fw"><!----></i>
                </td>
                <td class="col-md-9 tdleft">
                    <?php echo $val['Fichierarticle']['nom']; ?>
                </td>
                <td class="col-md-2">
                    <?php
                    echo $this->Html->link('<span class="fa fa-download fa-lg"><!----></span>', [
                        'controller' => 'articles',
                        'action' => 'download',
                        $val['Fichierarticle']['url'],
                        $val['Fichierarticle']['nom']
                    ], [
                        'class' => 'btn btn-outline-dark boutonShow btn-sm my-tooltip',
                        'title' => 'Télécharger le fichier',
                        'escapeTitle' => false
                    ]);
                    ?>
                </td>
            </tr>
            <?php
            }
            ?>
            </tr>
            </tbody>
        </table>
    </div>
    <?php
}
?>

<div style="clear: both">
    <?php
    echo $this->WebcilForm->buttons(['Back']);
    ?>
</div>

<?php
echo $this->WebcilForm->end();
?>
