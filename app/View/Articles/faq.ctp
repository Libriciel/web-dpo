<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

if (!empty($articles)) {
    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!--  Nom -->
                <th class="col-md-10">
                    <?php
                    echo __d('article', 'article.titreTableauNameArticle');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('article', 'article.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($articles as $article) {
                ?>
                <tr class="d-flex">
                    <!--  Name -->
                    <td class="tdleft col-md-10">
                        <?php
                        echo $article['Article']['name'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/show', [
                                'controller' => 'articles',
                                'action' => 'consulter',
                                'id' => $article['Article']['id'],
                                'titleBtn' => __d('article', 'article.commentaireBtnVisualiserArticle')
                            ]);
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('article', 'article.textAucunArticleEntite');
            ?>
        </h3>
    </div>
    <?php
}
?>

<script type="text/javascript">

    $(document).ready(function () {

        $("#divFiltrage").hide();

        $("#filtrage").click(function () {
            $("#divFiltrage").slideToggle(400);
        });

    });

</script>
