<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'web-DPO';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>
            <?php
            echo $cakeDescription
            ?>
        </title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
        // CSS
        echo $this->Html->css([
            '/js/node_modules/bootstrap/dist/css/bootstrap.min.css',
        ]);

        // JS
        echo $this->Html->script([
            '/js/node_modules/jquery/dist/jquery.min.js',
            '/js/node_modules/@popperjs/core/dist/umd/popper.min.js',
            '/js/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
            '/js/node_modules/@libriciel/ls-bootstrap-4/dist/ls-bootstrap-4.js',
        ]);
        echo $this->Html->script('fadeflash.js', ['inline' => false]);

        // CSS
        echo $this->Html->css([
            '/js/node_modules/@libriciel/ls-bootstrap-4/dist/ls-bootstrap-4.css',
            '/js/node_modules/components-font-awesome/css/all.min.css',
            'flash.css'
        ]);

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <style>
            .forgot-passwd-lnk {
                display: none;
            }
        </style>
    </head>

    <body>
        <?php
        echo $this->Flash->render();
        //$this->Flash->render('auth');
        echo $this->fetch('content');
        ?>

        <!-- Footer Libriciel SCOP -->
        <ls-lib-footer
                class="ls-login-footer ls-footer-fixed-bottom"
                active="webdpo"
                application_name="<?php echo $cakeDescription . ' ' . VERSION . " / "; ?>">
        </ls-lib-footer>

        <?php
        echo $this->Html->script('/js/node_modules/@libriciel/ls-composants/ls-elements.js');
        ?>
    </body>
</html>
