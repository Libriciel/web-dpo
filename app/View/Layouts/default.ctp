<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'web-DPO';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
        echo $this->Html->charset();
        ?>

        <title>
            <?php
            echo $cakeDescription;
            ?>
        </title>

        <?php
        // CSS
        echo $this->Html->css([
            '/js/node_modules/bootstrap/dist/css/bootstrap.min.css',
        ]);

        // JS
        echo $this->Html->script([
            '/js/node_modules/jquery/dist/jquery.min.js',
            '/js/node_modules/jquery-ui/external/jquery/jquery.js',
            '/js/node_modules/jquery-ui-dist/jquery-ui.min.js',
            'chosen/chosen.jquery.min.js',
            '/js/node_modules/select2/dist/js/select2.full.min.js',
            '/js/node_modules/select2/dist/js/i18n/fr.js',
            '/js/node_modules/select2/dist/js/select2.min.js',
            '/js/node_modules/tinymce/tinymce.min.js',
            '/js/node_modules/tinymce/jquery.tinymce.min.js',
            '/js/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
            '/js/node_modules/@popperjs/core/dist/umd/popper.min.js',
            '/js/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
            '/js/node_modules/@libriciel/ls-bootstrap-4/dist/dp-bootstrap-4.js',
            'formulaire.js',
            'main.js',
        ]);
        echo $this->Html->script('bootstrap-filestyle/src/bootstrap-filestyle.min.js', ['inline' => false]);
        echo $this->Html->script('fadeflash.js', ['inline' => false]);

        echo $this->Html->meta('icon');

        // CSS
        echo $this->Html->css([
            '/js/chosen/chosen.min.css',
            '/js/node_modules/@libriciel/ls-bootstrap-4/dist/dp-bootstrap-4.css',
            '/js/node_modules/jquery-ui/themes/base/all.css',
            '/js/node_modules/components-font-awesome/css/all.min.css',
            '/js/node_modules/select2/dist/css/select2.min.css',
            'main.css',
            'style.css',
            'flash.css',
            'navbar.css'
        ]);

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');

        require_once APP . 'View' . DS . 'Pannel' . DS . 'notification.ctp';
        ?>
    </head>

    <body>
        <div id="container">
            <div id="content">
                <?php
                echo $this->element('header', [
                    'cakeDescription' => $cakeDescription
                ]);

                if ($this->Session->read('Su') === false) {
                    echo $this->element('second_header');
                } else {
                    if (array_key_exists('Organisation', $this->Session->read()) === true) {
                        echo $this->element('second_header_superadmin_administration');
                    } else {
                        echo $this->element('second_header_superadmin');
                    }
                }
                ?>

                <main role="main" class="container-fluid-custom">
                    <?php
                    echo $this->Flash->render();

                    if ($this->Session->read('Su') === false) {
                        $homeBreadcrumb = 'pannel';
                    } else {
                        $homeBreadcrumb = 'checks';
                    }

                    echo $this->Html->getCrumbs(' / ', [
                        'text' => 'Accueil',
                        'url' => [
                            'controller' => $homeBreadcrumb,
                            'action' => 'index'
                        ],
                        'escape' => false
                    ]);

                    if ($this->params['action'] != 'login') {
                        ?>
                        <div class="row head">
                            <?php
                            if (isset($title)) {
                                ?>
                                <div class="col-md-8">
                                    <h2>
                                        <?php
                                        echo $title;
                                        ?>
                                    </h2>
                                </div>
                                <?php
                            }
                            ?>

                            <div class="col-md-4 text-right">
                                <?php
                                if ($this->Session->read('Su') === false || $this->here === '/organisations/administrer') {
                                    if (!empty($this->Session->read('Organisation.logo'))) {
                                        if (file_exists(CHEMIN_LOGOS . $this->Session->read('Organisation.logo'))) {
                                            echo $this->Html->image(CHEMIN_LIEN_SYMBOLIQUE_LOGO_ORGANISATION . $this->Session->read('Organisation.logo'), [
                                                'class' => 'logo-well'
                                            ]);
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }

                    echo $this->fetch('content');
                    ?>
                </main>

                <?php
                if ($this->Session->read('Su') === false) {
                    echo $this->element('Default/modalAddTraitement');
                }

                if ($this->Session->read('Su') === true) {
                    // Pop-up pour administrer une entité
                    echo $this->element('Default/modalAdministrerEntite');
                }

                echo $this->element('footer', [
                    'cakeDescription' => $cakeDescription
                ]);
                ?>
            </div>
        </div>
        <?php
        echo $this->Html->script('/js/node_modules/@libriciel/ls-composants/ls-elements.js');
        ?>
    </body>
</html>

<?php
if (isset($idFicheNotification)) {
    ?>
    <script type="text/javascript">

        $(document).ready(function () {

            openTarget("<?php echo $idFicheNotification ?>");

        });

    </script>
    <?php 
}

