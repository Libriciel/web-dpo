<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Extrait registre';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
        echo $this->Html->charset();
        ?>

        <title>
            <?php
            echo $cakeDescription;
            ?>
        </title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <?php
//        echo $this->Html->meta('icon');
//
//        echo $this->fetch('meta');
//        echo $this->fetch('css');
//        echo $this->fetch('script');
        ?>

        <style>
            * {
                box-sizing: border-box;
            }

            html {
                font-size: 10px;
                font-family: sans-serif;
            }

            body {
                background: #264aa3 fixed repeat;
                color: black;
                font-family: Ubuntu, sans-serif;
                margin: auto;
                min-width: 960px;
                width: 80%;
                font-size: 14px;
                line-height: 1.42857143;
            }

            .head {
                padding-bottom: 25px;
                margin-bottom: 25px;
                margin-top: 25px;
                border-bottom: 1px solid #d3d3d3;
                color: white;
            }

            #footer {
                margin-top: 35px;
                margin-bottom: 35px;
                color: white;
                font-size: 0.9em;
                border-top: 1px solid #d3d3d3;
            }

            .footer-link {
                color: white;
            }

            div#summary {
                font-size: x-large;
                margin-left: auto;
                margin-right: auto;
                text-align: center;
                margin-bottom: 18px;
                font-weight: bold;
            }

            .main {
                border: 1px solid #d3d3d3;
                background-color: #fff;
                padding: 20px;
                border-radius: 3px 3px 0 0;
                margin-top: 20px;
            }

            .register {
                margin-top: 12px;
                margin-bottom: 24px;
                text-align: center;
            }

            .register .controller {
                font-size: 26px;
                font-weight: bold;
                padding-top: 5px;
                padding-bottom: 5px;
            }

            .register .title {
                color: #0088ce;
                font-size: 26px;
                padding-bottom: 5px;
            }

            table {
                background-color: transparent;
            }

            table.processing {
                border-spacing: 1px;
                border-width: 1px;
                border-style: solid;
                border-collapse: collapse;
                margin: auto;
            }
        </style>
    </head>

    <body>
        <div id="container">
            <div id="content">
                <main role="main">
                    <?php
                    if ($this->params['action'] != 'login') {
                        ?>
                        <div class="row head">
                            <?php
                            if (isset($title)) {
                                ?>
                                <div class="col-md-6">
                                    <h2>
                                        <?php
                                        echo $title;
                                        ?>
                                    </h2>
                                </div>
                                <?php
                            }
                            ?>

                            <div class="col-md-6 text-right">
                                <?php
                                if ($this->Session->read('Su') === false || $this->here === '/organisations/administrer') {
                                    if (!empty($this->Session->read('Organisation.logo'))) {
                                        if (file_exists(CHEMIN_LOGOS . $this->Session->read('Organisation.logo'))) {
                                            echo $this->Html->image(CHEMIN_LIEN_SYMBOLIQUE_LOGO_ORGANISATION . $this->Session->read('Organisation.logo'), [
                                                'class' => 'logo-well'
                                            ]);
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }

                    echo $this->fetch('content');
                    ?>
                </main>

                <div id="footer">
                    <div class="text-center">
                        <?php
                        echo 'web-DPO' . ' ' . VERSION;
                        ?>
                        <a href="https://www.libriciel.fr/" class="footer-link" target="_blank"> <?php echo "/ &copy; Libriciel SCOP 2006-" . date('Y'); ?> </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
