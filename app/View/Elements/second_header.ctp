<?php
$items = [
    'Déclarations' => array_merge(
        [
            'disabled' => !($this->Autorisation->authorized(['1', '2', '3', '5'], $this->Session->read('Droit.liste'))),
            'icon' => 'fa-folder-open',
            'Mes déclarations' => [
                __d('default', 'default.sousTitreMesTraitementEnCoursRedaction') => [
                    'disabled' => !($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fa-edit',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'encours_redaction'
                    ]
                ],
                __d('default', 'default.sousTitreTraitementPartagerService') => [
                    'disabled' => !($countServicesUser !== 0 && $this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fa-edit',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'partageServices'
                    ]
                ],
                __d('default', 'default.sousTitreMesTraitementEnAttente') => [
                    'disabled' => !($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fa-clock',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'attente'
                    ]
                ],
                __d('default', 'default.sousTitreMesTraitementRefuser') => [
                    'disabled' => !($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fas fa-times',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'refuser'
                    ]
                ],
                __d('default', 'default.sousTitreTraitementValideesInsereRegistre') => [
                    'disabled' => !($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fas fa-check',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'archives'
                    ]
                ],
            ],
            'Déclarations reçues' => [
                __d('default', 'default.sousTitreMesTraitementRecuValidation') => [
                    'disabled' => !($this->Autorisation->authorized(['2'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fa-check-square',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'recuValidation'
                    ]
                ],
                __d('default', 'default.sousTitreMesTraitementRecuConsultation') => [
                    'disabled' => !($this->Autorisation->authorized(['3'], $this->Session->read('Droit.liste'))),
                    'icon' => 'fa-eye',
                    'url' => [
                        'controller' => 'pannel',
                        'action' => 'recuConsultation'
                    ]
                ],
            ],
            __d('default', 'default.sousTitreTraitementVu') => [
                'disabled' => !($this->Autorisation->authorized(['2', '3'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-inbox',
                'url' => [
                    'controller' => 'pannel',
                    'action' => 'consulte'
                ]
            ],
        ],
        (
        ($this->Autorisation->authorized(['37'], $this->Session->read('Droit.liste')) || $this->Autorisation->authorized(['33'], $this->Session->read('Droit.liste')))
            ? [
            'divider',
            __d('default', 'default.sousTitreInitialisationTraitement') => [
                'disabled' => !($this->Autorisation->authorized(['37'], $this->Session->read('Droit.liste'))),
                'icon' => 'fa-edit',
                'url' => [
                    'controller' => 'pannel',
                    'action' => 'initialisation'
                ]
            ],
            __d('default', 'default.sousTitreAllTraitements') => [
                'disabled' => !($this->Autorisation->authorized(['33'], $this->Session->read('Droit.liste'))),
                'icon' => 'fa-eye',
                'url' => [
                    'controller' => 'pannel',
                    'action' => 'all_traitements'
                ]
            ]
        ]
            : []
        ),
        (
        ($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste')))
            ? [
            'divider',
            __d('pannel', 'pannel.btnCreerTraitement') => [
                'disabled' => !($this->Autorisation->authorized(['1'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-plus',
                'url' => [
                    '#' => '#'
                ],
                'data-toggle' => 'modal',
                'data-target' => '#modalAddTraitement'
            ]
        ]
            : []
        )
    ),
    __d('default', 'default.titreRegistre') => [
        'disabled' => !($this->Autorisation->authorized(['4'], $this->Session->read('Droit.liste'))),
        'icon' => 'fas fa-book',
        __d('default', 'default.sousTitreRegistreActivite') => [
            'disabled' => !($this->Autorisation->authorized(['4'], $this->Session->read('Droit.liste'))),
            'icon' => 'fas fa-book',
            'url' => [
                'controller' => 'registres',
                'action' => 'registreActivite'
            ]
        ],
        __d('default', 'default.sousTitreRegistreSoustraitance') => [
            'disabled' => !($this->Autorisation->authorized(['4'], $this->Session->read('Droit.liste'))),
            'icon' => 'fas fa-book',
            'url' => [
                'controller' => 'registres',
                'action' => 'registreSoustraitance'
            ]
        ],
    ],
    __d('default', 'default.titreAdministration') => [
        'disabled' => !($this->Autorisation->authorized(['8', '9', '10', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '23', '24', '25', '26', '27', '28', '29', '31', '32'], $this->Session->read('Droit.liste'))),
        'icon' => 'fas fa-tools',
        __d('default', 'default.titreGenerale') => [
            'disabled' => !($this->Autorisation->authorized(['12'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreGeneraleEntite') => [
                'icon' => 'fa-building',
                'url' => [
                    'controller' => 'organisations',
                    'action' => 'edit',
                    $this->Session->read('Organisation.id')
                ]
            ],
            __d('default', 'default.sousTitrePassword') => [
                'icon' => 'fas fa-lock',
                'url' => [
                    'controller' => 'organisations',
                    'action' => 'politiquepassword'
                ]
            ]
        ],
        __d('default', 'default.sousTitreFormulaire') => [
            'disabled' => !($this->Autorisation->authorized(['27'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreTousFormulaires') => [
                'icon' => 'fa-copy',
                'url' => [
                    'controller' => 'formulaires',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreMesFormulaire') => [
                'icon' => 'fa-file-alt',
                'url' => [
                    'controller' => 'formulaires',
                    'action' => 'entite'
                ]
            ]
        ],
        __d('default', 'default.titreModeles') => [
            'disabled' => !($this->Autorisation->authorized(['26'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreModeleFormulaire') => [
                __d('default', 'default.sousTitreTousModelesFormulaires') => [
                    'icon' => 'fas fa-file-import',
                    'url' => [
                        'controller' => 'modeles',
                        'action' => 'index'
                    ]
                ],
                __d('default', 'default.sousTitreMesModelesFormulaires') => [
                    'icon' => 'fas fa-file-import',
                    'url' => [
                        'controller' => 'modeles',
                        'action' => 'entite'
                    ]
                ],
            ],
            __d('default', 'default.sousTitreModeleExtraitRegistre') => [
                __d('default', 'default.sousTitreTousModeleExtraitRegistre') => [
                    'icon' => 'fas fa-file-import',
                    'url' => [
                        'controller' => 'modeleExtraitRegistres',
                        'action' => 'index'
                    ]
                ],
                __d('default', 'default.sousTitreMonModeleExtraitRegistre') => [
                    'icon' => 'fas fa-file-import',
                    'url' => [
                        'controller' => 'modeleExtraitRegistres',
                        'action' => 'entite'
                    ]
                ],
            ],
            __d('default', 'default.sousTitreModelePresentation') => [
                'disabled' => ($this->Session->read('Organisation.usemodelepresentation') === false),
                'icon' => 'fas fa-file-import',
                'url' => [
                    'controller' => 'modelePresentations',
                    'action' => 'index'
                ]
            ]
        ],
        __d('default', 'default.titreAdministrationUser') => [
            'disabled' => !($this->Autorisation->authorized(['8', '9', '10', '13', '14', '15', '16', '17', '18'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreProfils') => [
                'disabled' => !($this->Autorisation->authorized(['13', '14', '15'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-users',
                'url' => [
                    'controller' => 'roles',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreServices') => [
                'disabled' => !($this->Autorisation->authorized(['16', '17', '18'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-sitemap',
                'url' => [
                    'controller' => 'services',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreUser') => [
                'disabled' => !($this->Autorisation->authorized(['8', '9', '10'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-user',
                'url' => [
                    'controller' => 'users',
                    'action' => 'index'
                ]
            ],
            'divider',
            __d('default', 'default.sousTitreAjouterUser') => [
                'disabled' => !($this->Autorisation->authorized(['8'], $this->Session->read('Droit.liste'))),
                'icon' => 'fas fa-plus-circle',
                'url' => [
                    'controller' => 'users',
                    'action' => 'add'
                ]
            ]
        ],
        __d('default', 'default.titreFAQ') => [
            'disabled' => !($this->Autorisation->authorized(['28', '29', '31'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreToutesLaFAQ') => [
                'icon' => 'fa-question-circle',
                'url' => [
                    'controller' => 'articles',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreMaFAQ') => [
                'icon' => 'fas fa-link',
                'url' => [
                    'controller' => 'articles',
                    'action' => 'entite'
                ]
            ]
        ],
        __d('default', 'default.titreTypages') => [
            'disabled' => !($this->Autorisation->authorized(['32'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreTypagesAnnexes') => [
                'icon' => 'fa-list-alt',
                'url' => [
                    'controller' => 'typages',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreMesTypesAnnexes') => [
                'icon' => 'fas fa-link',
                'url' => [
                    'controller' => 'typages',
                    'action' => 'entite'
                ]
            ]
        ],
        __d('default', 'default.sousTitreLesNormes') => [
            'disabled' => !($this->Autorisation->authorized(['19', '21', '22'], $this->Session->read('Droit.liste'))),
            'icon' => 'fas fa-certificate',
            'url' => [
                'controller' => 'normes',
                'action' => 'index'
            ]
        ],
//        __d('default', 'default.sousTitreReferentiels') => [
//            'disabled' => !($this->Autorisation->authorized(['40', '42', '43'], $this->Session->read('Droit.liste'))),
//            'icon' => 'fas fa-certificate',
//            'url' => [
//                'controller' => 'referentiels',
//                'action' => 'index'
//            ]
//        ],
        __d('default', 'default.titreSousTraitant') => [
            'disabled' => !($this->Autorisation->authorized(['23'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreLesSousTraitants') => [
                'icon' => 'fa-list-alt',
                'url' => [
                    'controller' => 'soustraitants',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreMesSousTraitants') => [
                'icon' => 'fas fa-link',
                'url' => [
                    'controller' => 'soustraitants',
                    'action' => 'entite'
                ]
            ]
        ],
        __d('default', 'default.titreResponsable') => [
            'disabled' => !($this->Autorisation->authorized(['24'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreListeResponsable') => [
                'icon' => 'fa-list-alt',
                'url' => [
                    'controller' => 'responsables',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreMesResponsables') => [
                'icon' => 'fas fa-link',
                'url' => [
                    'controller' => 'responsables',
                    'action' => 'entite'
                ]
            ]
        ],
        __d('default', 'default.titreMaintenance') => [
            'disabled' => !($this->Autorisation->authorized(['25'], $this->Session->read('Droit.liste'))),
            __d('default', 'default.sousTitreConnecteurs') => [
                'icon' => 'fas fa-plug',
                'url' => [
                    'controller' => 'connecteurs',
                    'action' => 'index'
                ]
            ],
            __d('default', 'default.sousTitreTachesAutomatiques') => [
                'icon' => 'fa-clock',
                'url' => [
                    'controller' => 'crons',
                    'action' => 'index'
                ]
            ]
        ],
    ],
    __d('default', 'default.sousTitreLesNormes') => [
        'disabled' => !($this->Autorisation->authorized(['20'], $this->Session->read('Droit.liste'))) || ($this->Session->read('Su')),
        'icon' => 'fas fa-certificate',
        'url' => [
            'controller' => 'normes',
            'action' => 'index'
        ]
    ],
    __d('default', 'default.sousTitreReferentiels') => [
        'disabled' => !($this->Autorisation->authorized(['40', '41', '42', '43'], $this->Session->read('Droit.liste'))) || ($this->Session->read('Su')),
        'icon' => 'fas fa-certificate',
        'url' => [
            'controller' => 'referentiels',
            'action' => 'index'
        ]
    ]
];

$menu = $this->WebdpoMenu->main($items);
?>

<nav class="navbar fixed-top second-nav navbar-expand-md navbar-custom navbar-dark bg-ls-dark">
    <div class="collapse navbar-collapse">
        <?php
        echo $menu;
        ?>
    </div>
</nav>
