<div id="ongletComplementaireCoresponsable" class="tab-pane">
    <br/>

    <div class="alert alert-danger" role="alert">
        <?php
        echo __d('formulaire', 'formulaire.infoActifTabCoresponsable');
        ?>
    </div>

    <br/>

    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoConcernantCoresponsable');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <!-- Champs du formulaire -->
    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.coresponsables', [
                'empty' => true,
                'required' => true,
                'readonly' => true,
                'placeholder' => __d('fiche', 'fiche.placeholderChampMultiCoresponsable')
            ]);
            ?>
        </div>
    </div>

    <br/>

    <div class="alert alert-warning" role="alert">
        <?php
        echo __d('formulaire', 'formulaire.infoUseFieldsAdditionalCoresponsable');
        ?>
    </div>

    <br>

    <?php
    echo $this->element(
        'Formulaires/boutonsCreationChamp',
        [
            'typeCreateForm' => 'coresponsable'
        ]
    );
    ?>

    <!-- Corps du formulaire -->
    <div id="form-container-coresponsable" class="form-container col-md-12">
        <?php
        if (!empty($this->request->data['Formulaire']['form-container-coresponsable'])) {
            echo $this->element('Formulaires/champDuFormulaire', [
                'champs' => $this->request->data['Formulaire']['form-container-coresponsable']
            ]);
        }
        ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        createForm('coresponsable');
    });

</script>