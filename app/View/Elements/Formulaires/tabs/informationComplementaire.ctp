<?php
$empty = false;
if ($this->request->params['action'] == 'add'){
    $empty = true;
}
?>

<!-- Onglet Information complémentaire -->
<div id="info_complementaire" class="tab-pane">
    <br>

    <!-- Information concernant la sous-finalité -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoTraitementComplementaire');
            ?>
        </span>
        <br>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usesousfinalite', [
                'id' => 'usesousfinalite',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'empty' => false,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div id="infoSupSousFinalite" class="col-md-12" style="background-color: #f5f3f3;">
            <div id="AddSousFinalite">
                <a id="AddSousFinaliteFileBox" class="btn btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Ajouter une sous-finalité
                </a>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usebaselegale', [
                'id' => 'usebaselegale',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'empty' => $empty,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div id="infoSupBaseLegale" class="col-md-12" style="background-color: #f5f3f3;">
            <?php
            echo $this->WebcilForm->input('Fiche.baselegale', [
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champBaselegale'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champBaselegaleDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'multiple' => 'checkbox',
                'class' => 'form-check checkbox fieldReadonly',
                'options' => Fiche::LISTE_BASE_LEGALE,
                'disabled' => true
            ]);
            ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usedecisionautomatisee', [
                'id' => 'usedecisionautomatisee',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'placeholder' => false,
                'empty' => $empty,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <br>

    <div id="infoSupDecisionAutomatisee" style="background-color: #f5f3f3;">
        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.decisionAutomatisee', [
                    'options' => [
                        'Oui' => 'Oui',
                        'Non' => 'Non'
                    ],
                    'class' => 'form-control fieldReadonly',
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' ',
                    'readonly' => true
                ]);
                ?>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.descriptionDecisionAutomatisee', [
                    'type' => 'textarea',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ]);
                ?>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usetransferthorsue', [
                'id' => 'useTransfertHorsUe',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'placeholder' => false,
                'empty' => $empty,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div id="infoSupTransfertHorsUe">
        <div class="alert alert-warning" role="alert">
            <?php
            echo __d('formulaire', 'formulaire.infoWarningTransfertHorsUe');
            ?>
        </div>

        <div class="row" style="background-color: #f5f3f3;">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoTransfereHorsUEComplementaire');
                    ?>
                </span>
                <br>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'Fiche.organismeDestinataireHorsUe' => [
                        'required' => true,
                        'placeholder' => false,
                        'readonly' => true,
                    ],
                    'Fiche.typeGarantieHorsUe' => [
                        'options' => Fiche::TYPE_GARANTIE_HORS_UE,
                        'class' => 'form-control fieldReadonly',
                        'placeholder' => false,
                        'empty' => true,
                        'required' => true,
                        'readonly' => true,
                        'data-placeholder' => ' '
                    ],
                ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.paysDestinataireHorsUe', [
                    'options' => Fiche::LISTE_PAYS_HORS_UE,
                    'class' => 'form-control fieldReadonly',
                    'placeholder' => false,
                    'empty' => true,
                    'required' => true,
                    'readonly' => true,
                    'data-placeholder' => ' '
                ]);
                ?>
            </div>

            <div id="AddOrganismeHorsUE">
                <a id="AddOrganismeHorsUEFileBox" class="btn btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Ajouter un autre organisme hors de l'UE
                </a>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usedonneessensible', [
                'id' => 'usedonneessensible',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'placeholder' => false,
                'empty' => $empty,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div id="infoSupDonneesSensibles">
        <div class="alert alert-warning" role="alert">
            <?php
            echo __d('formulaire', 'formulaire.infoWarningDonneesSensibles');
            ?>
        </div>

        <div class="row" style="background-color: #f5f3f3;">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoDonneesSensiblesComplementaire');
                    ?>
                </span>
                <br>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'Fiche.typeDonneeSensible' => [
                        'options' => Fiche::LISTE_DONNEES_SENSIBLES,
                        'class' => 'form-control fieldReadonly',
                        'placeholder' => false,
                        'empty' => true,
                        'required' => true,
                        'readonly' => true,
                        'data-placeholder' => ' '
                    ],
                    'Fiche.dureeConservation' => [
                        'type' => 'textarea',
                        'required' => true,
                        'placeholder' => false,
                        'readonly' => true,
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.descriptionDonneSensible', [
                    'type' => 'textarea',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true,
                ]);
                ?>
            </div>

            <div id="AddDonneesSensibles">
                <a id="AddDonneesSensiblesFileBox" class="btn btn-outline-primary">
                    <i class="fa fa-plus"></i>
                    Ajouter d'autres données sensibles
                </a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        showOrHide($('#usesousfinalite').val(), $('#infoSupSousFinalite'));
        $('#usesousfinalite').change(function () {
            showOrHide($(this).val(), $('#infoSupSousFinalite'));
        });

        showOrHide($('#usebaselegale').val(), $('#infoSupBaseLegale'));
        $('#usebaselegale').change(function () {
            showOrHide($(this).val(), $('#infoSupBaseLegale'));
        });

        showOrHide($('#usedecisionautomatisee').val(), $('#infoSupDecisionAutomatisee'));
        $('#usedecisionautomatisee').change(function () {
            showOrHide($(this).val(), $('#infoSupDecisionAutomatisee'));
        });

        showOrHide($('#useTransfertHorsUe').val(), $('#infoSupTransfertHorsUe'));
        $('#useTransfertHorsUe').change(function () {
            showOrHide($(this).val(), $('#infoSupTransfertHorsUe'));
        });

        showOrHide($('#usedonneessensible').val(), $('#infoSupDonneesSensibles'));
        $('#usedonneessensible').change(function () {
            showOrHide($(this).val(), $('#infoSupDonneesSensibles'));
        });

        $('#info_complementaire .fieldReadonly').change(function () {
            $(this).val(null);
        });

    });

    function showOrHide(val, field)
    {
        if (val == true) {
            $(field).show();
        } else {
            $(field).hide();
        }
    }

</script>
