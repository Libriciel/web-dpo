<div id="ongletComplementaireSousTraitant" class="tab-pane">
    <br/>

    <div class="alert alert-danger" role="alert">
        <?php
        echo __d('formulaire', 'formulaire.infoActifTabSoustraitance');
        ?>
    </div>

    <br/>

    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoSoustraitant');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <!-- Champs du formulaire -->
    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.soustraitances', [
                'empty' => true,
                'required' => true,
                'readonly' => true,
                'placeholder' => __d('fiche', 'fiche.placeholderChampSoustraitance')
            ]);

            ?>
        </div>
    </div>

    <br/>

    <div class="alert alert-warning" role="alert">
        <?php
        echo __d('formulaire', 'formulaire.infoUseFieldsAdditionalSoustraitant');
        ?>
    </div>

    <br>

    <?php
    echo $this->element(
        'Formulaires/boutonsCreationChamp',
        [
            'typeCreateForm' => 'soustraitant'
        ]
    );
    ?>

    <!-- Corps du formulaire -->
    <div id="form-container-soustraitant" class="form-container col-md-12">
        <?php
        if (!empty($this->request->data['Formulaire']['form-container-soustraitant'])) {
            echo $this->element('Formulaires/champDuFormulaire', [
                'champs' => $this->request->data['Formulaire']['form-container-soustraitant']
            ]);
        }
        ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        createForm('soustraitant');
    });

</script>