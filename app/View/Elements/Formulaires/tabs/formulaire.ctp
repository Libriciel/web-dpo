<!-- Onglet Formulaire -->
<div id="info_formulaire" class="tab-pane">
    <br/>

    <div class="alert alert-warning" role="alert">
        <?php
        echo __d('fiche', 'fiche.textInfoChampsCommuns');
        ?>
    </div>

    <?php
    echo $this->element('Formulaires/boutonsCreationChamp', [
        'typeCreateForm' => 'formulaire'
    ]);
    ?>

    <!-- Corps du formulaire -->
    <div id="form-container-formulaire" class="form-container col-md-12">
        <?php
        if (!empty($this->request->data['Formulaire']['form-container-formulaire'])) {
            echo $this->element('Formulaires/champDuFormulaire', [
                'champs' => $this->request->data['Formulaire']['form-container-formulaire']
            ]);
        }
        ?>
    </div>
</div>
<!-- Fin onglet Formulaire -->

<script type="text/javascript">

    $(document).ready(function () {
        createForm('formulaire');
    });

</script>
