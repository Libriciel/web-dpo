<?php
if (!isset($usefieldsredacteur)) {
    $usefieldsredacteur = false;
}
?>

<!-- Onglet Information concernant le traitement -->
<div id="information_traitement" class="tab-pane <?php echo $classActiveInfoGenerale; ?>">
    <br/>

    <?php
    if ($usefieldsredacteur === true) {
        ?>
        <!-- Information sur le rédacteur -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoContact');
                ?>
            </span>
            <div class="row row35"></div>
        </div>
        <?php
    }
    ?>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            if ($usefieldsredacteur === true) {
                echo $this->WebcilForm->inputs([
                    'Fiche.declarantpersonnenom' => [
                        'id' => 'declarantpersonnenom',
                        'readonly' => true,
                        'required' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.declarantpersonneportable' => [
                        'id' => 'declarantpersonneportable',
                        'readonly' => true,
                        'placeholder' => false,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ]
                ]);
            }

            echo $this->WebcilForm->inputs([
                'Fiche.declarantservice' => [
                    'id' => 'declarantservice',
                    'readonly' => true,
                    'required' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            if ($usefieldsredacteur === true) {
                echo $this->WebcilForm->inputs([
                    'Fiche.declarantpersonneemail' => [
                        'id' => 'declarantpersonneemail',
                        'readonly' => true,
                        'required' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.declarantpersonnefix' => [
                        'id' => 'declarantpersonnefix',
                        'readonly' => true,
                        'placeholder' => false,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ]
                ]);
            }
            ?>
        </div>
    </div>

    <?php
    if (!empty($options_referentiels)) {
        ?>
        <!-- Information sur le référentiel -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo 'Informations concernant le référentiel à associer au traitement :';
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $this->WebcilForm->input('Fiche.referentiel_id', [
                    'id' => 'referentiel_id',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champReferentiel')
                    ],
                    'options' => $options_referentiels,
                    'readonly' => true,
                    'empty' => true,
                    'placeholder' => false,
                ]);
                ?>
            </div>
        </div>
        <?php
    }

    if (!empty($options_normes)) {
        ?>
        <!-- Information sur la norme -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoNorme');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.norme_id', [
                    'id' => 'norme_id',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champNorme')
                    ],
                    'options' => $options_normes,
                    'readonly' => true,
                    'empty' => true,
                    'placeholder' => false,
                ]);
                ?>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.descriptionNorme', [
                    'id' => 'descriptionNorme',
                    'type' => 'textarea',
                    'readonly' => true,
                    'placeholder' => false,
                ]);
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <!-- Information concernant le traitement -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoTraitement');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.outilnom' => [
                    'id' => 'outilnom',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ],
                'Fiche.transfert_hors_ue' => [
                    'id' => 'transfert_hors_ue',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champTransfertHorsUe'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champTransfertHorsUeDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'class' => 'transformSelect form-control',
                    'required' => true,
                    'readonly' => true,
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' '
                ],
                'Fiche.donnees_sensibles' => [
                    'id' => 'donnees_sensibles',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champDonneesSensibles'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champDonneesSensiblesDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'class' => 'transformSelect form-control',
                    'required' => true,
                    'readonly' => true,
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' '
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.finaliteprincipale', [
                'id' => 'finaliteprincipale',
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champFinaliteprincipale'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champFinaliteprincipaleDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'type' => 'textarea',
                'required' => true,
                'placeholder' => false,
                'readonly' => true
            ]);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoPia');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <div id="alertFieldRequiredPia" class="col-md-6 alert alert-warning" role="alert">
            <?php
            echo "Ce champ sera affiché à l'utilisateur qui rédigera un traitement que si vous utilisez le questionnaire afin de définir si l'AIPD est obligatoire";

            echo $this->WebcilForm->input('Fiche.obligation_pia', [
                'id' => 'obligation_pia',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'class' => 'transformSelect form-control',
                'required' => true,
                'readonly' => true,
                'empty' => true,
                'placeholder' => false,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-6"></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.realisation_pia', [
                'id' => 'realisation_pia',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champRealisationPia'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champRealisationPiaDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'class' => 'transformSelect form-control',
                'required' => true,
                'readonly' => true,
                'empty' => true,
                'placeholder' => false,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.depot_pia', [
                'id' => 'depot_pia',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'class' => 'transformSelect form-control',
                'required' => true,
                'readonly' => true,
                'empty' => true,
                'placeholder' => false,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <!-- Co-responsabilité sur le traitement -->
        <div class="col-md-6">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoCoresponsable');
                ?>
            </span>

            <div class="row row35"></div>

            <?php
            echo $this->WebcilForm->input('Fiche.coresponsable', [
                'id' => 'coresponsable',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champCoresponsable'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champCoresponsableDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'default' => false,
                'class' => 'transformSelect form-control',
                'placeholder' => false,
                'required' => true,
                'readonly' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <!-- Sous-traitance sur le traitement -->
        <div class="col-md-6">
            <span class='labelFormulaire'>
                <?php
                if ($rt_externe === false) {
                    echo __d('fiche', 'fiche.textInfoSousTraitance');
                    $libelleFieldSoustraitance = __d('fiche', 'fiche.champSoustraitance');
                } else {
                    echo __d('fiche', 'fiche.textInfoSousTraitanceUlterieur');
                    $libelleFieldSoustraitance = __d('fiche', 'fiche.champSoustraitanceUlterieur');
                }
                ?>
            </span>

            <div class="row row35"></div>

            <?php
            echo $this->WebcilForm->input('Fiche.soustraitance', [
                'id' => 'soustraitance',
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        $libelleFieldSoustraitance,
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champSoustraitanceDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'default' => false,
                'class' => 'transformSelect form-control',
                'placeholder' => false,
                'required' => true,
                'readonly' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>
    </div>
</div>
<!-- Fin onglet Information concernant le traitement-->

<script type="text/javascript">

    $(document).ready(function () {

        displayFieldRequiredAIPD($('#usepia').val());
        $('#usepia').change(function () {
            var select = $(this).val();

            displayFieldRequiredAIPD(select);
        });

    });

    function displayFieldRequiredAIPD(val){
        if (val == true) {
            $('#alertFieldRequiredPia').show();
        } else {
            $('#alertFieldRequiredPia').hide();
        }
    }

</script>
