<?php
//echo $this->Html->script([
//    'jquery-mask-plugin/dist/jquery.mask.min.js'
//]);
?>

<!-- Onglet Information sur l'entité -->
<div id="info_rt" class="tab-pane">
    <br/>
    <!--Information sur l'entité -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation','organisation.textEntite');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_raisonsociale' => [
                    'id' => 'rt_externe_raisonsociale',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneRaisonsociale')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_telephone' => [
                    'id' => 'rt_externe_telephone',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneTelephone')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_fax' => [
                    'id' => 'rt_externe_fax',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneFax')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_adresse' => [
                    'id' => 'rt_externe_adresse',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneAdresse')
                    ],
                    'type' => 'textarea',
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_email' => [
                    'id' => 'rt_externe_email',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneEmail')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_siret' => [
                    'id' => 'rt_externe_siret',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneSiret')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_ape' => [
                    'id' => 'rt_externe_ape',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneApe')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information sur le responsable de l'entitée -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation', 'organisation.titreResponsableEntitee');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_civiliteresponsable' => [
                    'id' => 'rt_externe_civiliteresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneCiviliteresponsable')
                    ],
                    'options' => [
                        'M.' => 'Monsieur',
                        'Mme.' => 'Madame'
                    ],
                    'class' => 'transformSelect form-control',
                    'readonly' => true,
                    'placeholder' => false,
                    'required' => true,
                    'empty' => true,
                    'data-placeholder' => ' '
                ],
                'Trash.rt_externe_prenomresponsable' => [
                    'id' => 'rt_externe_prenomresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExternePrenomresponsable')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_nomresponsable' => [
                    'id' => 'rt_externe_nomresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneNomresponsable')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_fonctionresponsable' => [
                    'id' => 'rt_externe_fonctionresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneFonctionresponsable')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_emailresponsable' => [
                    'id' => 'rt_externe_emailresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneEmailresponsable')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_telephoneresponsable' => [
                    'id' => 'rt_externe_telephoneresponsable',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneTelephoneresponsable')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Information sur le DPO -->
        <div class="col-md-8">
            <!-- Affichage du logo du DPO -->
            <?php
            if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
                echo $this->Html->image('logo_dpo.svg', [
                    'class' => 'logo-well'
                ]);
            }
            ?>
        </div>

        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_civility_dpo' => [
                    'id' => 'rt_externe_civility_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneCivilityDpo')
                    ],
                    'options' => [
                        'M.' => 'Monsieur',
                        'Mme.' => 'Madame'
                    ],
                    'class' => 'transformSelect form-control',
                    'readonly' => true,
                    'placeholder' => false,
                    'empty' => true,
                    'data-placeholder' => ' '
                ],
                'Trash.rt_externe_prenom_dpo' => [
                    'id' => 'rt_externe_prenom_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExternePrenomDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_nom_dpo' => [
                    'id' => 'rt_externe_nom_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneNomDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_numerocnil_dpo' => [
                    'id' => 'rt_externe_numerocnil_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneNumerocnilDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_externe_email_dpo' => [
                    'id' => 'rt_externe_email_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneTelephonefixeDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_telephonefixe_dpo' => [
                    'id' => 'rt_externe_telephonefixe_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneEmailDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_externe_telephoneportable_dpo' => [
                    'id' => 'rt_externe_telephoneportable_dpo',
                    'label' => [
                        'text' => __d('rt_externe', 'rt_externe.champRtExterneTelephoneportableDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<!--Fin onglet Information sur l'entité-->

<script type="text/javascript">

    $(document).ready(function () {

        // Mask champs
        // L'entité
        $('#rt_externe_telephone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_externe_fax').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_externe_siret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

        // Responsable de l'entité
        $('#rt_externe_telephoneresponsable').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        // DPO
        $('#rt_externe_telephonefixe_dpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_externe_telephoneportable_dpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

    });

</script>
