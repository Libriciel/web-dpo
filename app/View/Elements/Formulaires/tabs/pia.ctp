<?php
$empty = false;
if ($this->request->params['action'] == 'add'){
    $empty = true;
}

$optionFields = [
    'Oui' => 'Oui',
    'Non' => 'Non'
];
?>

<div id="info_pia" class="tab-pane <?php echo $classActivePia; ?>">
    <br/>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('usepia', [
                'id' => 'usepia',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'class' => 'transformSelect form-control',
                'placeholder' => false,
                'empty' => $empty,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div id="infoSupFieldsAIPD">
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.titleTraitementNotRequiredAIPD');
                ?>
            </span>
            <br>
        </div>

        <div class="row">
            <div class="col-md-12" style="background-color: #f5f3f3;">
                <?php
                echo $this->WebcilForm->inputs([
                    'Fiche.ressources_humaines' => [
                        'id' => 'ressources_humaines',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champRessourcesHumaines'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                    ],
                    'Fiche.relation_fournisseurs' => [
                        'id' => 'relation_fournisseurs',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champRelationFournisseurs'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>',
                    ],
                    'Fiche.gestion_electoral' => [
                        'id' => 'gestion_electoral',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champGestionElectoral'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.comites_entreprise' => [
                        'id' => 'comites_entreprise',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champComitesEntreprise'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.association' => [
                        'id' => 'association',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champAssociation'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.sante_prise_patient' => [
                        'id' => 'sante_prise_patient',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champSantePrisePatient'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.avocats' => [
                        'id' => 'avocats',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champAvocats'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.greffiers' => [
                        'id' => 'greffiers',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champGreffiers'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.notaires' => [
                        'id' => 'notaires',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champNotaires'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.collectivites_affaires_scolaires' => [
                        'id' => 'collectivites_affaires_scolaires',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champCollectivitesAffairesScolaires'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.controles_acces' => [
                        'id' => 'controles_acces',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champControlesAcces'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ],
                    'Fiche.ethylotests' => [
                        'id' => 'ethylotests',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champEthylotests'),
                        ],
                        'options' => $optionFields,
                        'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => '<hr>'
                    ]
                ]);
                ?>
            </div>
        </div>

        <br>

        <div id="liste_obligatoire">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.titleTraitementRequiredAIPD');
                    ?>
                </span>
                <br>
            </div>

            <div class="row">
                <div class="col-md-12" style="background-color: #f5f3f3;">
                    <?php
                    echo $this->WebcilForm->inputs([
                        'Fiche.sante_medicosociaux' => [
                            'id' => 'sante_medicosociaux',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champSanteMedicosociaux'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                        ],
                        'Fiche.donnees_genetiques' => [
                            'id' => 'donnees_genetiques',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champDonneesGenetiques'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.profils_personnes_gestion_rh' => [
                            'id' => 'profils_personnes_gestion_rh',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champProfilsPersonnesGestionRh'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.surveiller_constante_employes' => [
                            'id' => 'surveiller_constante_employes',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champSurveillerConstanteEmployes'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.gestion_alertes_sociale_sanitaire' => [
                            'id' => 'gestion_alertes_sociale_sanitaire',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champGestionAlertesSocialeSanitaire'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.gestion_alertes_professionnelle' => [
                            'id' => 'gestion_alertes_professionnelle',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champGestionAlertesProfessionnelle'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.donnees_sante_registre' => [
                            'id' => 'donnees_sante_registre',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champDonneesSanteRegistre'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.profilage_rupture_contrat' => [
                            'id' => 'profilage_rupture_contrat',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champProfilageRuptureContrat'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.mutualises_manquements_rupture_contrat' => [
                            'id' => 'mutualises_manquements_rupture_contrat',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champMutualisesManquementsRuptureContrat'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.profilage_donnees_externes' => [
                            'id' => 'profilage_donnees_externes',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champProfilageDonneesExternes'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.biometriques' => [
                            'id' => 'biometriques',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champBiometriques'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.gestion_logements_sociaux' => [
                            'id' => 'gestion_logements_sociaux',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champGestionLogementsSociaux'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.accompagnement_social' => [
                            'id' => 'accompagnement_social',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champAccompagnementSocial'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ],
                        'Fiche.localisation_large_echelle' => [
                            'id' => 'localisation_large_echelle',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champLocalisationLargeEchelle'),
                            ],
                            'options' => $optionFields,
                            'class' => 'col-md-2 transformSelect form-control displayInput fieldReadonly',
                            'required' => true,
                            'empty' => true,
                            'readonly' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' ',
                            'before' => '<hr>'
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <br>

        <div id="liste_criteres">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.titleCriteresAIPD');
                    ?>
                </span>
                <br>
            </div>

            <div class="row" style="background-color: #f5f3f3;">
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('Fiche.criteres', [
                        'id' => 'criteres',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champCriteres')
                        ],
                        'multiple' => 'checkbox',
                        'class' => 'form-check checkbox',
                        'options' => Fiche::LISTE_CRITERES,
                        'disabled' => true
                    ]);
                    ?>
                </div>

                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('Fiche.traitement_considere_risque', [
                        'id' => 'traitement_considere_risque',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champTraitementConsidereRisque')
                        ],
                        'options' => $optionFields,
                        'class' => 'transformSelect form-control displayInput fieldReadonly',
                        'required' => true,
                        'empty' => true,
                        'readonly' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' '
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        showOrHideAIPD($('#usepia').val(), $('#infoSupFieldsAIPD'));
        $('#usepia').change(function () {
            showOrHideAIPD($(this).val(), $('#infoSupFieldsAIPD'));
        });

        $('#info_pia .fieldReadonly').change(function () {
            $(this).val(null);
        });

    });

    function showOrHideAIPD(val, field)
    {
        if (val == true) {
            $(field).show();
        } else {
            $(field).hide();
        }
    }

</script>
