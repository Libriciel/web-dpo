<!-- Onglet Annexe(s) -->
<div id="annexe" class="tab-pane">
    <br/>

    <h4>
        <?php echo __d('formulaire', 'formulaire.textInfoFonctionnementAnnexe'); ?>
    </h4>

    <hr>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-8">
            <?php
            echo $this->WebcilForm->input('useallextensionfiles', [
                'id' => 'useallextensionfiles',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'class' => 'transformSelect form-control',
                'placeholder' => false,
                'default' => false,
                'empty' => false,
                'required' => true,
                'data-placeholder' => ' '
            ]);
            ?>
        </div>

        <div class="col-md-4"></div>
    </div>

    <div id="infoUseAllExtension">
        <hr>

        <div class="alert alert-danger" role="alert">
            <?php
            echo __d('formulaire', 'formulaire.alertInfoUseAllExtention');
            ?>
        </div>

        <div class="alert alert-info" role="alert">
            <?php
            echo __d('formulaire', 'formulaire.infoExtentionUtilisable');
            ?>
        </div>
    </div>
</div>
<!-- Fin onglet Annexe(s) -->

<script type="text/javascript">

    $(document).ready(function () {

        displayAlertDanger($('#useallextensionfiles').val());
        $('#useallextensionfiles').change(function () {
            var select = $(this).val();

            displayAlertDanger(select);
        });

    });

    function displayAlertDanger(val){
        if (val == true) {
            $('#infoUseAllExtension').show();
        } else {
            $('#infoUseAllExtension').hide();
        }
    }

</script>