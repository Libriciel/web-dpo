<?php
$body = '<div class="com-md-12">'
        . 'La modification du champ va entrainer la suppression des conditions suivantes :'
        .'<ul>'
            .'<div id="allConditionsDelete" class="row"></div>'
        .'</ul>'
    .'</div>'
;

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        .'<button id="deleteCondition" type="button" class="btn btn-outline-danger" >'
            .'<i class="fa fa-trash fa-lg"></i>'
            . 'Supprimer les conditions en relation avec ce champ'
        .'</button>'
    .'</div>'
;

$content = [
    'title' => 'Suppression des conditions !',
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalWarningDeleteConditions',
    'controller' => 'modalWarningDeleteConditions',
    'action' => '#',
    'content' => $content
]);
