<?php
$body = '<div class="row">'
        .'<div class="col-md-12">'
            . $this->WebcilForm->input('id', ["id" => "FormulaireId", "value" => 0])
            . $this->WebcilForm->input('libelle', [
                'id' => 'libelle',
                'class' => 'form-control',
                'placeholder' => __d('formulaire', 'formulaire.popupPlaceholderNomFormulaire'),
                'label' => [
                    'text' => __d('formulaire', 'formulaire.popupNomFormulaire'),
                ],
                'required' => true
            ])
            . $this->WebcilForm->input('rt_externe', [
                'label' => [
                    'text' => __d('formulaire', 'formulaire.popupChampRt'),
                ],
                'required' => true,
                'class' => 'form-control usersDeroulant',
                'options' => [
                    false => 'Non',
                    true => 'Oui'
                ],
                'default' => false
            ])
            . $this->WebcilForm->input('description', [
                'type' => 'textarea',
                'class' => 'form-control',
                'placeholder' => __d('formulaire', 'formulaire.popupPlaceholderDescription'),
                'label' => [
                    'text' => __d('formulaire', 'formulaire.popupDescription'),
                ],
                'required' => false
            ])
        .'</div>'
    .'</div>'
;

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
        'id' => 'btnSaveModal',
        'type' => 'submit',
        'class' => 'btn btn-primary',
        'escape' => false,
    ])
    .'</div>'
;

$content = [
    'title' => __d('formulaire', 'formulaire.popupInfoGeneraleFormulaireDuplication'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalDupliquer',
    'controller' => 'formulaires',
    'action' => 'dupliquer',
    'content' => $content
]);
?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#btnSaveModal').attr('disabled', true);

        $("#libelle").keyup(function () {
            if ($(this).val().length !== 0) {
                $('#btnSaveModal').removeAttr('disabled');
            } else {
                $('#btnSaveModal').attr('disabled', true);
            }
        });

    });

</script>
