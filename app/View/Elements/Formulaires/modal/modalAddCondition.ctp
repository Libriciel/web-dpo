<?php
$ifTheField_options = [
    'transfert_hors_ue' => __d('fiche', 'fiche.champTransfertHorsUe'),
    'donnees_sensibles' => __d('fiche', 'fiche.champDonneesSensibles'),
    'realisation_pia' => __d('fiche', 'fiche.champRealisationPia'),
    'depot_pia' => __d('fiche', 'fiche.champDepotPia'),
    'coresponsable' => __d('fiche', 'fiche.champCoresponsable'),
    'soustraitance' => __d('fiche', 'fiche.champSoustraitance')
];

$body = '<div class="row">'
    .'<div class="col-md-12">'
        . $this->WebcilForm->inputs([
            'Fiche.ifTheFields' => [
                'id' => 'ifTheField',
                'options' => $ifTheField_options,
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champIfTheField')
                ],
                'empty' => true,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' '
            ],
            'Fiche.hasValue' => [
                'id' => 'hasValue',
                'options' => [],
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champHasValue')
                ],
                'empty' => true,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' '
            ],
            'Fiche.hasValueMulti' => [
                'id' => 'hasValueMulti',
                'options' => [],
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champHasValue')
                ],
                'empty' => true,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' ',
                'multiple' => true
            ],
            'Fiche.thenTheField' => [
                'id' => 'thenTheField',
                'options' => [],
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champThenTheField')
                ],
                'empty' => true,
                'placeholder' => false,
                'required' => true,
                'data-placeholder' => ' '
            ],
            'Fiche.mustBe' => [
                'id' => 'mustBe',
                'data-placeholder' => ' ',
                'empty' => true,
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champMustBe')
                ],
                'options' => [
                    'shown' => "affiché",
                    'hidden' => "caché"
                ],
                'placeholder' => false,
                'required' => true,
            ],
            'Fiche.ifNot' => [
                'id' => 'ifNot',
                'options' => [
                    'shown' => "affiché",
                    'hidden' => "caché"
                ],
                'label' => [
                    'text' => __d('formulaire', 'formulaire.champIfNot')
                ],
                'empty' => true,
                'placeholder' => false,
                'required' => true,
                'readonly' => true,
                'disabled' => true,
                'data-placeholder' => ' '
            ]
        ])
    .'</div>'
.'</div>'
;

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        .'<button id="saveCondition" type="button" class="btn btn-primary" >'
            .'<i class="far fa-save fa-lg"></i>'
            .__d('default', 'default.btnSave')
        .'</button>'
    .'</div>'
;


$content = [
    'title' => "Ajouter une condition sur un champ",
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddCondtion',
    'controller' => 'modalConditions',
    'action' => '#',
    'content' => $content
]);
?>

<script type="text/javascript">

    $(document).ready(function () {
        $('#hasValueMulti').select2({
            language: "fr",
            width: '100%',
        });

        let idContainer = null,
            idFieldConditionCreated = null;

        // $('#btn-condition-formulaire, #btn-condition-coresponsable, #btn-condition-soustraitant').click(function () {
        $('#btn-condition-formulaire').click(function () {
            hideAllFields();
            $('#ifTheField option').slice(7).remove();

            idContainer = '#' + $(this).parent().parent().nextAll('.form-container').attr('id');
            if (!idContainer) {
                return false;
            }

            $(idContainer).find('.ui-selected').removeClass('ui-selected');
            $('.field-options >div').remove();

            if ($(idContainer).find('.draggable').length <= 0 ) {
                alert('Vous devez au minimum créé un champ avant de pouvoir ajouter une condition');
                return false;
            }

            $(idContainer).find('.draggable').filter('.checkboxes, .radios, .deroulant, .multi-select').each(function (key, field) {
                let name = $(field).find('input, option').attr('name'),
                    label = $(field).find('.labeler').html();

                if (name && label) {
                    $('#ifTheField').append('<option value="' + name + '">' + label + '</option>');
                }
            });
        });

        $('#ifTheField').change(function () {
            hideAllFields();

            let idFieldConditionCreated = $(this).val(),
                showHasValueMulti = false;

            if (!idFieldConditionCreated) {
                hideAllFields();
            } else {
                if (jQuery.inArray(idFieldConditionCreated, [
                    'transfert_hors_ue',
                    'donnees_sensibles',
                    'realisation_pia',
                    'depot_pia',
                    'coresponsable',
                    'soustraitance'
                ]) !== -1) {
                    // On recupere les "options" du champ "selected" sur lequel nous allons faire la condition
                    $('#'+idFieldConditionCreated).find('option').each(function () {
                        let option = $(this).attr('value'),
                            label = $(this).text();

                        if (option && label) {
                            $('#hasValue').append('<option value="' + option + '">' + label + '</option>');
                        }
                    });
                } else {
                    // On recupere les "options" du champ "selected" sur lequel nous allons faire la condition
                    if ($('#'+idFieldConditionCreated).is("select")) {
                        // Champs "Menu multi-select" ou "Menu déroulant"
                        $(idContainer).find('input[name ="'+idFieldConditionCreated+'"],option[name ="'+idFieldConditionCreated+'"]').each(function () {
                            let value = $(this).attr('value'),
                                labelValue = $(this).text();

                            if (value && labelValue) {
                                if ($(this).attr('type')) {
                                    $('#hasValue').append('<option value="' + value + '">' + labelValue + '</option>');
                                } else {
                                    let newOption = new Option(labelValue, value, false, false);
                                    $('#hasValueMulti').append(newOption).trigger('change');
                                }
                            }
                        });
                    } else {
                        // Champs "Case à cocher" ou "Choix unique"
                        $(idContainer).find('input[name="'+idFieldConditionCreated+'"], option[name="'+idFieldConditionCreated+'"]').each(function () {
                            let option = null,
                                id = null;

                            if ($(this).attr('type') == 'checkbox') {
                                showHasValueMulti = true;
                            }

                            option = $(this).next('label').text();

                            if ($(this).attr('type') == 'radio') {
                                id = $(this).attr('value');
                            } else {
                                id = $(this).attr('id');
                            }

                            if (option && id) {
                                if (showHasValueMulti === true) {
                                    let newOption = new Option(option, id, false, false);
                                    $('#hasValueMulti').append(newOption).trigger('change');
                                } else {
                                    $('#hasValue').append('<option value="' + id + '">' + option + '</option>');
                                }
                            }
                        });
                    }
                }

                if ($('#'+idFieldConditionCreated).hasClass('multiSelect') === true ||
                    showHasValueMulti === true
                ) {
                    $('#hasValueMulti').parent().show();
                } else {
                    $('#hasValue').parent().show();
                }
            }
        });

        $('#hasValue, #hasValueMulti').change(function () {
            let valueSelected = $(this).val();
            $('#thenTheField').parent().hide();
            $('#thenTheField option').not(':first').remove();

            $('#mustBe').parent().hide();
            $('#mustBe').val('');

            $('#ifNot').parent().hide();
            $('#ifNot').val('');

            if (valueSelected) {
                let fieldsContainer = $(idContainer).find('.draggable');
                if (!fieldsContainer) {
                    return false;
                }

                $.each(fieldsContainer, function (key, field) {
                    let name = $(field).find('input, textarea, option').attr('name'),
                        label = $(field).find('.labeler').html();

                    if (name && label) {
                        if (name !== $('#ifTheField').val()) {
                            $('#thenTheField').append('<option value="' + name + '">' + label + '</option>');
                        }
                    }
                });

                $('#thenTheField').parent().show();
            }
        });

        $('#thenTheField').change(function () {
            let val = $(this).val();

            if (!val) {
                $('#mustBe').parent().hide();
                $('#mustBe').val('');

                $('#ifNot').parent().hide();
                $('#ifNot').val('');
            } else {
                $('#mustBe').parent().show();
            }
        });

        $('#mustBe').change(function () {
            let mustBeValue = $(this).val();

            if (!mustBeValue) {
                $('#ifNot').parent().hide();
                $('#ifNot').val('');
            } else {
                if (mustBeValue == 'shown') {
                    $('#ifNot').val('hidden');
                }

                if (mustBeValue == 'hidden') {
                    $('#ifNot').val('shown');
                }

                $('#ifNot').parent().show();
            }
        });

        $('#saveCondition').click(function () {
            $('.is-invalid').removeClass('is-invalid');
            $('.invalid-feedback').remove();

            let ifTheField = $('#ifTheField').val(),
                hasValue = $('#hasValue').val(),
                thenTheField = $('#thenTheField').val(),
                mustBe = $('#mustBe').val(),
                ifNot = $('#ifNot').val(),
                success = true;

            if (!ifTheField && $('#ifTheField').is(":visible")) {
                $('#ifTheField').addClass('is-invalid');
                $('#ifTheField').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                success = false;
            }

            if (!hasValue) {
                if ($('#hasValue').is(":visible") === true) {
                    $('#hasValue').addClass('is-invalid');
                    $('#hasValue').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                    success = false;
                } else {
                    hasValue = $('#hasValueMulti').val();
                    if (!hasValue) {
                        $('#hasValueMulti').addClass('is-invalid');
                        $('#hasValueMulti').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                        success = false;
                    }
                }
            }

            if (!thenTheField && $('#thenTheField').is(":visible")) {
                $('#thenTheField').addClass('is-invalid');
                $('#thenTheField').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                success = false;
            }

            if (!mustBe && $('#mustBe').is(":visible")) {
                $('#mustBe').addClass('is-invalid');
                $('#mustBe').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                success = false;
            }

            if (!ifNot && $('#ifNot').is(":visible")) {
                $('#ifNot').addClass('is-invalid');
                $('#ifNot').parent().append('<div class="invalid-feedback">Champ obligatoire</div>');
                success = false;
            }

            if (success === false) {
                return success;
            }

            objCondition = {
                'ifTheField' : ifTheField,
                'hasValue' : hasValue,
                'thenTheField' : thenTheField,
                'mustBe' : mustBe,
                'ifNot' : ifNot
            };

            let conditions = {};
            conditions[uuidv4()] = objCondition;

            let idContainer = '#' + $('.form-container').attr('id');

            // On selectionne le champs qui va recevoir la condition
            let fieldChoose = $(idContainer).find('input[name ="' + thenTheField + '"], textarea[name ="' + thenTheField + '"], option[name ="' + thenTheField + '"]').closest('.draggable');

            let otherConditions = $(fieldChoose).attr('data-virtual-conditions');
            if (otherConditions) {
                $.each(JSON.parse(otherConditions), function (key, value) {
                    conditions[key] = value
                });
            }

            $(fieldChoose).attr('data-virtual-conditions', JSON.stringify(conditions));

            $('#modalAddCondtion').modal('toggle');
        });

        $('#modalAddCondtion').on('hidden.bs.modal', function () {
            hideAllFields();
            $('#ifTheField option').slice(7).remove();
            $('#ifTheField').val('');
        })
    });

    function hideAllFields()
    {
        $('#hasValue').parent().hide();
        $('#hasValueMulti').parent().hide();
        $('#thenTheField').parent().hide();
        $('#mustBe').parent().hide();
        $('#ifNot').parent().hide();

        removeValuesAllFields();
    }

    function removeValuesAllFields()
    {
        $('#hasValue option').not(':first').remove();
        $('#hasValueMulti option').not(':first').remove();
        $('#thenTheField option').not(':first').remove();
        $('#mustBe').val('');
        $('#ifNot').val('');
    }

    function uuidv4() {
        // return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        return ([1e3]+-1e3).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

</script>
