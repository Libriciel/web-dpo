<?php
echo $this->Html->script('scroll');

$typeCreateForm = isset($typeCreateForm) === true ? $typeCreateForm : '';
?>

<div id="virtualFieldsOptions" class="forScroll">
    <div class="btn-group-<?php echo $typeCreateForm;?>">
        <!-- Bouton Petit champ texte -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-small-text-<?php echo $typeCreateForm;?>">
            <i class="fa fa-font fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnPetitChamp');
            ?>
        </button>

        <!-- Bouton Grand champ texte -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-long-text-<?php echo $typeCreateForm;?>">
            <i class="fa fa-text-height fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnGrandChamp');
            ?>
        </button>

        <!-- Bouton Champ date -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-date-<?php echo $typeCreateForm;?>">
            <i class="fa fa-calendar fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnChampDate');
            ?>
        </button>

        <!-- Bouton Cases à cocher -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-checkbox-<?php echo $typeCreateForm;?>">
            <i class="fa fa-check-square fa-lg "></i>
            <?php
            echo __d ('formulaire','formulaire.btnCheckbox');
            ?>
        </button>

        <!-- Bouton Choix unique -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-radio-<?php echo $typeCreateForm;?>">
            <i class="fa fa-dot-circle fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnRadio');
            ?>
        </button>

        <!-- Bouton Menu deroulant -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-deroulant-<?php echo $typeCreateForm;?>">
            <i class="fa fa-list-alt fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnDeroulant');
            ?>
        </button>

        <!-- Bouton Menu multi-select-->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-multi-select-<?php echo $typeCreateForm;?>">
            <i class="fa fa-list-alt fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnMultiSelect');
            ?>
        </button>

        <!-- Bouton Titre de catégorie -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-title-<?php echo $typeCreateForm;?>">
            <i class="fa fa-tag fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnTitreCategorie');
            ?>
        </button>

        <!-- Bouton Champ d'information -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-help-<?php echo $typeCreateForm;?>">
            <i class="fa fa-info-circle fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnChampInfo');
            ?>
        </button>

        <!-- Bouton Label -->
        <button type="button" class="btn btn-outline-dark btn-sm btn-input-<?php echo $typeCreateForm;?>" id="btn-texte-<?php echo $typeCreateForm;?>">
            <i class="fa fa-pencil-alt fa-lg"></i>
            <?php
            echo __d ('formulaire','formulaire.btnLabel');
            ?>
        </button>

        <!-- Bouton ajout d'une condition -->
        <?php
        if ($typeCreateForm === 'formulaire') {
            echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>' . ' ' . __d('formulaire', 'formulaire.btnCondition'), ['#' => '#'], [
                'id' => 'btn-condition-' . $typeCreateForm,
                'escape' => false,
                'data-toggle' => 'modal',
                'data-target' => '#modalAddCondtion',
                'class' => 'btn btn-outline-primary btn-sm'
            ]);
        }
        ?>
    </div>

    <br>

    <div class="card">
        <div class="card-header">
            <?php
            echo __d('formulaire','formulaire.textOptionChamp');
            ?>
        </div>
        <div class="card-body field-options" id="field-options-<?php echo $typeCreateForm;?>"></div>
    </div>
</div>
