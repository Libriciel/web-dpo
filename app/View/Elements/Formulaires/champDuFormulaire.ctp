<?php
//echo $this->Html->script([
//    '/js/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
//    '/js/smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js'
//]);
//echo $this->Html->css('/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min');

echo $this->Html->script([
    'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
    'smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js',
]);

echo $this->Html->css([
    '/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min'
]);

$calendrier = [];
foreach ($champs as $key => $value) {
    $details = json_decode($value['Champ']['details'], true);

    if (isset($details['default']) === false) {
        $details['default'] = null;
    }

    // Possitionnement du champ sur la ligne
    $line = 45 * ($value['Champ']['ligne'] - 1);

    // Possitionnement du champ dans la colonne de droite ou gauche
    if ($value['Champ']['colonne'] == 1) {
        $colonne = 'left: 0px;';
    } else {
        $colonne = 'right: 0px;';
    }

    // Champ obligatoire ou non
    if ($details['obligatoire'] == true) {
        $champObligatoire = "fieldRequired";
    } else {
        $champObligatoire = "fieldNotRequired";
    }

    if (!isset($details['conditions'])) {
        $details['conditions'] = '';
    }

    switch ($value['Champ']['type']) {
        // Petit champ texte
        case 'input':
            echo '<div class="draggable form-group col-md-6 small-text" style="top: '.$line.'px; '.$colonne.'" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                . '<div class="col-md-12">'
                    . '<label class="control-label" for="'.$details['name'].'">'
                    . '<span class="labeler">' . $details['label'] . '</span>'
                    . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    . '</label>'
                    . '<input type="text" value="'.$details['default'].'" name="'.$details['name'].'" checked="' . $details['obligatoire'] . '" placeholder="' . $details['placeholder'] . '" class="form-control champNomVariableReadonly"/>'
                . '</div>'
            . '</div>';
            break;

        // Grand champ texte
        case 'textarea':
            echo '<div class="draggable form-group col-md-6 long-text" style="top:' . $line . 'px; ' . $colonne . '" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                . '<div class="col-md-12">'
                    . '<label class="control-label" for="'.$details['name'].'">'
                        . '<span class="labeler">' . $details['label'] . '</span>'
                        . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    . '</label>'
                    . '<textarea type="textarea" name="' . $details['name'] . '" checked="' . $details['obligatoire'] . '" placeholder="' . $details['placeholder'] . '"class="form-control champNomVariableReadonly" cols="30" rows="6">'.$details['default'].'</textarea>'
                . '</div>'
            . '</div>';
            break;

        // Champ date
        case 'date':
            echo '<div class="draggable form-group col-md-6 date" style="top:' . $line . 'px; ' . $colonne . '" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                . '<div class="col-md-12">'
                    . '<label class="control-label" for="'.$details['name'].'">'
                        . '<span class="labeler">' . $details['label'] . '</span>'
                        . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    . '</label>'
                    . '<input class="form-control champNomVariableReadonly" id="'.$details['name'].'" value="'.$details['default'].'" name="'.$details['name'].'" required="'.$details['obligatoire'].'" placeholder="'.$details['placeholder'].'"/>'
                . '</div>'
            . '</div>';

            $calendrier[] = $details['name'];
            break;

        // Titre de catégorie
        case 'title':
            echo '<div class="draggable form-group col-md-6 title text-center" style="top:' . $line . 'px; ' . $colonne . '">'
                . '<h1>' . $details['content'] . '</h1>'
                . '</div>';
            break;

        // Champ d'information
        case 'help':
            echo '<div class="draggable form-group col-md-6 help" style="top:' . $line . 'px; ' . $colonne . '">'
                . '<div class="col-md-12 alert alert-info">'
                    . '<div class="col-md-12 text-center">'
                        . '<i class="fa fa-info-circle fa-2x"></i>'
                    . '</div>'
                    . '<div class="col-md-12 messager">'
                        . $details['content']
                    . '</div>'
                . '</div>'
            . '</div>';
            break;

        // Cases à cocher
        case 'checkboxes':
            echo '<div class="draggable form-group col-md-6 checkboxes" style="top: '.$line.'px; '.$colonne.'" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                .'<div class="input select">'
                    .'<label class="control-label" for="'.$details['name'].'" class="col-md-12">'
                        .'<span class="labeler">'.$details['label'].'</span>'
                        . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    .'</label>'
                    .'<div class="col-md-12 contentCheckbox">'
                        .'<input id="'.$details['name'].'" type="hidden" name="'.$details['name'].'" value="">';
                        foreach ($details['options'] as $key => $val) {
                            $checkbox_id = $details['name'].$key;

                            $checked = "";
                            if (isset($details['default'])) {
                                if (in_array($key, $details['default'])) {
                                    $checked = "checked";
                                }
                            }

                            echo '<div class="form-check">'
                                .'<input id="'.$checkbox_id.'" class="form-check-input champNomVariableReadonly" type="checkbox" '.$checked.' name="'.$details['name'].'" value="'.$key.'">'
                                .'<label class="form-check-label" for="'.$checkbox_id.'">'
                                . $val
                                .'</label>'
                            .'</div>';
                        }
                    echo '</div>'
                .'</div>'
            .'</div>';

            break;

        // Choix unique
        case 'radios':
            echo '<div class="draggable form-group col-md-6 radios" style="top:' . $line . 'px; ' . $colonne . '" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                .'<label class="control-label" for="'.$details['name'].'" class="col-md-12">'
                    .'<span class="labeler">' . $details['label'] . '</span>'
                    .($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                .'</label>'
                .'<div class="col-md-12 contentRadio">'
                    .'<input id="'.$details['name'].'" type="hidden" name="'.$details['name'].'" value="">';
                    foreach ($details['options'] as $key => $val) {
                        $radio_id = $details['name'].$key;

                        $checked = "";
                        if (isset($details['default'])) {
                            if ($details['default'] === $val) {
                                $checked = "checked";
                            }
                        }

                        echo '<div class="form-check">'
                            .'<input name="'.$details['name'].'" class="form-check-input" type="radio" '.$checked.' value="'.$val.'" id="'.$radio_id.'">'
                            .'<label class="form-check-label" for="'.$radio_id.'">'
                            . $val
                            .'</label>'
                        .'</div>';
                    }
                echo '</div>'
            .'</div>';

            break;

        // Menu déroulant
        case 'deroulant':
            echo '<div class="draggable form-group col-md-6 deroulant" style="top:' . $line . 'px; ' . $colonne . '" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                . '<div class="col-md-12">'
                    . '<label class="control-label" for="'.$details['name'].'">'
                        . '<span class="labeler">' . $details['label'] . '</span>'
                        . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    . '</label>'
                    . '<select id="'.$details['name'].'" class="transformSelect form-control contentDeroulant" name="data[Fiche]['.$details['name'].']" data-placeholder=" ">'
                        . '<option name="'.$details['name'].'" value=""></option>';
                        foreach ($details['options'] as $key => $val) {
                            $selected = "";
                            if (isset($details['default'])) {
                                if ($details['default'] === strval($key)) {
                                    $selected = "selected";
                                }
                            }

                            echo '<option id="'.$details['name'].$key.'" class="champNomVariableReadonly" type="deroulant" '.$selected.' name ="'.$details['name'].'" value="'.$key.'">'.$val.'</option>';
                        }
                    echo '</select>'
                . '</div>'
            . '</div>';
            break;

        case 'multi-select' :
            echo '<div class="draggable col-md-6 form-group multi-select" style="top:'.$line.'px; '.$colonne.'" '
                .'data-virtual-required="'.$champObligatoire.'" '
                .'data-virtual-conditions='.$details['conditions'].'>'
                .'<div class="col-md-12">'
                    .'<label class="control-label" for="'.$details['name'].'">'
                        .'<span class="labeler">' . $details['label'] . '</span>'
                        . ($details['obligatoire'] ? '<span class="obligatoire"> *</span>' : '')
                    .'</label>'
                    .'<input id="'.$details['name'].'_" type="hidden" name="'.$details['name'].'" value=""/>'
                    .'<select id="'.$details['name'].'" name="'.$details['name'].'" class="form-control multiSelect contentMultiSelect" multiple="multiple">';
                        foreach ($details['options'] as $key => $val) {
                            $selected = "";
                            if (isset($details['default'])) {
                                if (in_array($key, $details['default'])) {
                                    $selected = 'selected';
                                }
                            }

                            echo '<option id="'.$details['name'].$key.'" class="champNomVariableReadonly" '.$selected.' name="'.$details['name'].'" value="'.$key.'">'.$val.'</option>';
                        }
                    echo '</select>'
                .'</div>'
            .'</div>';
            break;

        // Label
        case 'texte':
            echo '<div class="draggable form-group col-md-6 texte" style="top:' . $line . 'px;' . $colonne . '">'
                . '<h5>' . $details['content'] . '</h5>'
                . '</div>';
            break;

        default :
            break;
    }
}
?>

<script type="text/javascript">
    $(document).ready(function () {

        // @todo
        // Les "seuls" endroits du code où l'on utilise select2 semblent être les "formulaires" et les "fiches".
        // Pour les autres champ de formulaire (autres que multiSelect ?) on utilise chosen.
        $('.multiSelect').select2({
            language: "fr",
            placeholder: 'Sélectionnez une ou plusieurs options',
            allowClear: true,
            width: '100%'
        });

        let champsDate = <?php echo json_encode($calendrier)?>;
        jQuery.each(champsDate, function(key, val){
            $('#' + val).datetimepicker({
                viewMode: 'year',
                startView: 'decade',
                format: 'dd/mm/yyyy',
                minView: 2,
                language: 'fr',
                autoclose: true
            });
        });

    });
</script>
