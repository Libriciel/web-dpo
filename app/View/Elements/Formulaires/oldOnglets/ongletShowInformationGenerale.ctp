<!-- Onglet Information concernant le traitement -->
<div id="information_traitement" class="tab-pane active">
    <?php
//    echo $this->WebcilForm->create('informationGenerale', [
//        'autocomplete' => 'off',
//        'inputDefaults' => ['div' => false],
//        'class' => 'form-horizontal',
//        'novalidate' => 'novalidate'
//    ]);
    ?>
    <br/>

    <!-- Information sur le rédacteur -->
    <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoContact');
                    ?>
                </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.declarantpersonnenom' => [
                    'id' => 'declarantpersonnenom',
                    'readonly' => true,
                    'required' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.declarantservice' => [
                    'id' => 'declarantservice',
                    'readonly' => true,
                    'required' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.declarantpersonneportable' => [
                    'id' => 'declarantpersonneportable',
                    'readonly' => true,
                    'placeholder' => false,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.declarantpersonneemail' => [
                    'id' => 'declarantpersonneemail',
                    'readonly' => true,
                    'required' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.declarantpersonnefix' => [
                    'id' => 'declarantpersonneportable',
                    'readonly' => true,
                    'placeholder' => false,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information concernant le traitement -->
    <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoTraitement');
                    ?>
                </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.outilnom' => [
                    'id' => 'outilnom',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ],
                'Fiche.transfert_hors_ue' => [
                    'id' => 'transfert_hors_ue',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ],
                'Fiche.donnees_sensibles' => [
                    'id' => 'donnees_sensibles',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.finaliteprincipale', [
                'id' => 'finaliteprincipale',
                'type' => 'textarea',
                'required' => true,
                'placeholder' => false,
                'readonly' => true
            ]);
            ?>
        </div>

        <!-- Co-responsabilité sur le traitement -->
        <div class="col-md-12">
                    <span class='labelFormulaire'>
                        <?php
                        echo __d('fiche', 'fiche.textInfoCoresponsable');
                        ?>
                    </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.coresponsable', [
                    'type' => 'text',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ]);
                ?>
            </div>
        </div>

        <!-- Sous-traitance sur le traitement -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoSousTraitance');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.soustraitance', [
                    'type' => 'text',
                    'required' => true,
                    'placeholder' => false,
                    'readonly' => true
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
//echo $this->WebcilForm->end();
?>
<!-- Fin onglet Information concernant le traitement-->