<!-- Onglet Co-responsable -->
<div id="ongletCoresponsable" class="tab-pane">
    <?php
    echo $this->WebcilForm->create('informationCoresponsable', [
        'autocomplete' => 'off',
        'inputDefaults' => ['div' => false],
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate'
    ]);
    ?>
    <br/>

    <!-- Information sur le rédacteur -->
    <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoConcernantCoresponsable');
                    ?>
                </span>
        <div class="row row35"></div>
    </div>

    <!-- Champs du formulaire -->
    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Fiche.coresponsableid', [
                'empty' => true,
                'required' => true,
                'readonly' => true,
                'placeholder' => __d('fiche', 'fiche.placeholderChampCoresponsable')
            ]);

            ?>
        </div>
    </div>

    <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('responsable', 'responsable.titreResponsable');
                    ?>
                </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.nomcoresponsable' => [
                    'id' => 'nomcoresponsable',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.prenomcoresponsable' => [
                    'id' => 'prenomcoresponsable',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.fonctioncoresponsable' => [
                    'id' => 'fonctioncoresponsable',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.emailcoresponsable' => [
                    'id' => 'emailcoresponsable',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.telephonecoresponsable' => [
                    'id' => 'telephonecoresponsable',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('responsable', 'responsable.titreStructureResponsable');
                    ?>
                </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.raisonsocialestructure' => [
                    'id' => 'raisonsocialestructure',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.siretstructure' => [
                    'id' => 'siretstructure',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.apestructure' => [
                    'id' => 'apestructure',
                    'required' => true,
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Fiche.telephonestructure' => [
                    'id' => 'telephonestructure',
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.faxstructure' => [
                    'id' => 'faxstructure',
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.adressestructure' => [
                    'id' => 'adressestructure',
                    'type' => 'textarea',
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ],
                'Fiche.emailstructure' => [
                    'id' => 'email',
                    'readonly' => true,
                    'value' => __d('default', 'default.valueChampRemplissageAuto')
                ]
            ]);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();
// Fin onglet Co-responsable