<!-- Onglet soustraitance -->
<?php
if ($soustraitantOLD === true) {
    ?>
    <div id="soustraitant" class="tab-pane">
        <?php
        echo $this->WebcilForm->create('informationSoustraitant', [
            'autocomplete' => 'off',
            'inputDefaults' => ['div' => false],
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate'
        ]);
        ?>
        <br/>

        <!-- Information sur le rédacteur -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoSoustraitant');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <!-- Champs du formulaire -->
        <div class="row">
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Fiche.soustraitantid', [
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => 'Choisir un sous-traitant'
                ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'Fiche.soustraitantraisonsociale' => [
                        'id' => 'soustraitantraisonsociale',
                        'type' => 'hidden'
                    ],
                    'Fiche.soustraitantsiret' => [
                        'id' => 'soustraitantsiret',
                        'required' => true,
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.soustraitantape' => [
                        'id' => 'soustraitantape',
                        'required' => true,
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.soustraitanttelephone' => [
                        'id' => 'soustraitanttelephone',
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.soustraitantfax' => [
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.soustraitantadresse' => [
                        'type' => 'textarea',
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ],
                    'Fiche.soustraitantemail' => [
                        'readonly' => true,
                        'value' => __d('default', 'default.valueChampRemplissageAuto')
                    ]
                ]);

                ?>
            </div>
        </div>
    </div>
    <?php
    echo $this->WebcilForm->end();
} else {
    echo $this->WebcilForm->input('soustraitant', [
        'id' => 'soustraitant',
        'type' => 'hidden',
        'value' => 0
    ]);
}
//Fin onglet soustraitance