<?php
$items = [
    "Déconnexion de l'entité : " . $this->Session->read('Organisation.raisonsociale') => [
        'icon' => 'fas fa-sign-out-alt',
        'url' => [
            'controller' => 'checks',
            'action' => 'index'
        ]
    ],
    __d('default', 'default.titreGenerale') => [
        __d('default', 'default.sousTitreGeneraleEntite') => [
            'icon' => 'fa-building',
            'url' => [
                'controller' => 'organisations',
                'action' => 'edit',
                $this->Session->read('Organisation.id')
            ]
        ],
        __d('default', 'default.sousTitrePassword') => [
            'icon' => 'fas fa-lock',
            'url' => [
                'controller' => 'organisations',
                'action' => 'politiquepassword'
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.titreAdministrationUser') => [
        __d('default', 'default.sousTitreProfils') => [
            'icon' => 'fas fa-users',
            'url' => [
                'controller' => 'roles',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreServices') => [
            'icon' => 'fas fa-sitemap',
            'url' => [
                'controller' => 'services',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreUser') => [
            'icon' => 'fas fa-user',
            'url' => [
                'controller' => 'users',
                'action' => 'index',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.titreTypages') => [
        __d('default', 'default.sousTitreTypagesAnnexes') => [
            'icon' => 'fas fa-list-alt',
            'url' => [
                'controller' => 'typages',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreMesTypesAnnexes') => [
            'icon' => 'fas fa-link',
            'url' => [
                'controller' => 'typages',
                'action' => 'entite',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.titreFAQ') => [
        __d('default', 'default.sousTitreToutesLaFAQ') => [
            'icon' => 'fas fa-list-alt',
            'url' => [
                'controller' => 'articles',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreMaFAQ') => [
            'icon' => 'fas fa-link',
            'url' => [
                'controller' => 'articles',
                'action' => 'entite',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.sousTitreLesNormes') => [
        'icon' => 'fas fa-certificate',
        'url' => [
            'controller' => 'normes',
            'action' => 'index',
        ]
    ],
    __d('default', 'default.titreSousTraitant') => [
        __d('default', 'default.sousTitreLesSousTraitants') => [
            'icon' => 'fas fa-list-alt',
            'url' => [
                'controller' => 'soustraitants',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreMesSousTraitants') => [
            'icon' => 'fas fa-link',
            'url' => [
                'controller' => 'soustraitants',
                'action' => 'entite',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.titreResponsable') => [
        __d('default', 'default.sousTitreListeResponsable') => [
            'icon' => 'fas fa-list-alt',
            'url' => [
                'controller' => 'responsables',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreMesResponsables') => [
            'icon' => 'fas fa-link',
            'url' => [
                'controller' => 'responsables',
                'action' => 'entite',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.titreMaintenance') => [
        __d('default', 'default.sousTitreConnecteurs') => [
            'icon' => 'fas fa-plug',
            'url' => [
                'controller' => 'connecteurs',
                'action' => 'index',
            ]
        ],
        __d('default', 'default.sousTitreTachesAutomatiques') => [
            'icon' => 'fas fa-clock',
            'url' => [
                'controller' => 'crons',
                'action' => 'index',
            ]
        ],
//        'icon' => 'fas fa-users'
    ],
];

$menu = $this->WebdpoMenu->main($items);
?>

<nav class="navbar fixed-top second-nav navbar-expand-md navbar-custom navbar-dark bg-ls-dark">
    <div class="collapse navbar-collapse">
        <?php
        echo $menu;
        ?>
    </div>
</nav>
