<?php 
use Libriciel\Utility\Password\PasswordStrengthMeterAnssi;

$body = '<div class="row col-md-12">'
        .'<p>'
            . __d('organisation', 'organisation.texteExplicationForcePassword')
        .'</p>'
        .'<p>'
           . __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles')
        .'</p>'
        .'<table id="theorie" class="table table-striped table-hove">'
            .'<thead>'
                .'<tr class="d-flex">'
                    .'<th class="col-md-4">Alphabet</th>'
                    .'<th class="col-md-3">Symboles de l\'alphabet</th>'
                    .'<th class="col-md-1">Très faible</th>'
                    .'<th class="col-md-1">Faible</th>'
                    .'<th class="col-md-1">Moyen</th>'
                    .'<th class="col-md-1">Fort</th>'
                    .'<th class="col-md-1">Très fort</th>'
                .'</tr>'
            .'</thead>'
            .'<tbody>'
                .'<tr class="d-flex symbols_2">'
                    .'<th class="tdleft col-md-4">2 symboles</th>'
                    .'<th class="tdleft col-md-3">0 ou 1</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 64</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">64</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">80</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">100</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">128</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_10">'
                    .'<th class="tdleft col-md-4">10 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 20</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">20</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">25</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">31</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">39</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_16">'
                    .'<th class="tdleft col-md-4">16 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9 et A à F</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 16</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">16</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">20</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">25</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">32</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_26">'
                    .'<th class="tdleft col-md-4">26 symboles</th>'
                    .'<th class="tdleft col-md-3">A à Z</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 14</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">14</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">18</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">22</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">28</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_36">'
                    .'<th class="tdleft col-md-4">36 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9 et A à Z</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 13</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">13</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">16</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">20</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">25</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_52">'
                    .'<th class="tdleft col-md-4">52 symboles</th>'
                    .'<th class="tdleft col-md-3">A à Z et a à z</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 12</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">12</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">15</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">18</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">23</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_62">'
                    .'<th class="tdleft col-md-4">62 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9, A à Z et a à z</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 11</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">11</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">14</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">17</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">22</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_70">'
                    .'<th class="tdleft col-md-4">70 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9, A à Z, a à z et  ! #$*% ?</th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 11</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">11</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">14</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">17</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">21</td>'
                .'</tr>'
                .'<tr class="d-flex symbols_90">'
                    .'<th class="tdleft col-md-4">90 symboles</th>'
                    .'<th class="tdleft col-md-3">0 à 9, A à Z, a à z et  ! #$*% ? &[|]@^µ§ :/ ;.,<>°²³ </th>'
                    .'<td class="tdleft col-md-1 number strength-very-weak">< 10</td>'
                    .'<td class="tdleft col-md-1 number strength-weak">10</td>'
                    .'<td class="tdleft col-md-1 number strength-medium">13</td>'
                    .'<td class="tdleft col-md-1 number strength-strong">16</td>'
                    .'<td class="tdleft col-md-1 number strength-very-strong">20</td>'
                .'</tr>'
            .'</tbody>'
        .'</table>'
        .'<div class="row col-md-12 form-group">'
            .'<div id="informationPassword" class="alert alert-info col-md-12">'
                .'<strong>Exemples : </strong>'
                .'<ul id="exemples">'
                    .'<li class="strength-very-weak">Très faible (force 1)'
                        .'<ul>'
                            .'<li class="symbols_2"><kbd>00111</kbd> : <strong>5</strong> caractères dans un alphabet de <strong>2</strong> symboles. </li>'
                        .'</ul>'
                    .'</li>'
                    .'<li class="strength-weak">Faible (force 2)'
                        .'<ul>'
                            .'<li class="symbols_10"><kbd>96442571499252715213</kbd> : <strong>20</strong> caractères dans un alphabet de <strong>10</strong> symboles. </li>'
                        .'</ul>'
                    .'</li>'
                    .'<li class="strength-medium">Moyen (force 3)'
                        .'<ul>'
                            .'<li class="symbols_16"><kbd>65C88BAED3D6ECC0B705</kbd> : <strong>20</strong> caractères dans un alphabet de <strong>16</strong> symboles. </li>'
                        .'</ul>'
                    .'</li>'
                    .'<li class="strength-strong">Fort (force 4)'
                        .'<ul>'
                            .'<li class="symbols_26"><kbd>KSFUNDDVTJWHHPKGXUNZJB</kbd> : <strong>22</strong> caractères dans un alphabet de <strong>26</strong> symboles. </li>'
                        .'</ul>'
                    .'</li>'
                    .'<li class="strength-very-strong">Très fort (force 5)'
                        .'<ul>'
                            .'<li class="symbols_62"><kbd>Voitures2ColoSoupersos</kbd> : <strong>25</strong> caractères dans un alphabet de <strong>62</strong> symboles. </li>'
                        .'</ul>'
                    .'</li>'
                .'</ul>'
            .'</div>'
        .'</div>'
    .'</div>';

$footer = '';

$content = [
    'title' => __d('organisation', 'organisation.titreNbCaracterMinimalRequisAlphabet'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddForm',
    'content' => $content,
    'sizeModal' => 'modal-xl'
]);
?>

<script type="text/javascript">
    function affichageCouleurForceTableau(forcePassword) {           
        $("#theorie td").removeClass('highlight');
        switch (forcePassword) {
            case 1:
                $("#theorie .strength-very-weak").addClass('highlight');
            break;

            case 2:
                $("#theorie .strength-weak").addClass('highlight');
            break;

            case 3:
                $("#theorie .strength-medium").addClass('highlight');
            break;

            case 4:
                $("#theorie .strength-strong").addClass('highlight');
            break;

            case 5:
                $("#theorie .strength-very-strong").addClass('highlight');
            break;
        }
    }

    $(document).ready(function () {
        affichageCouleurForceTableau(<?php echo $forcePassword; ?>);

        
        $("#exemples li li").mouseover(function() {
            let symbols = $(this).attr('class'),
                strength = $(this).closest('ul').closest('li').attr('class');
                
            $("#theorie td").removeClass('highlight');
            $("#theorie tr." + symbols + " td." + strength).addClass('highlight');
        });

        $("#exemples li li").mouseout(function() {
            $("#theorie td").removeClass('highlight');
            affichageCouleurForceTableau(<?php echo $forcePassword; ?>); // @fixme
        });
        
    });
    
</script>
