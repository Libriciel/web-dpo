<?php
//echo $this->Html->script([
//    'jquery-mask-plugin/dist/jquery.mask.min.js'
//]);
?>

<!-- Onglet Information sur l'entité -->
<div id="onglet_rt_organisation" class="tab-pane">
    <br/>
    <!--Information sur l'entité -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation','organisation.textEntite');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_raisonsociale' => [
                    'id' => 'rt_organisation_raisonsociale',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationRaisonsociale')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_telephone' => [
                    'id' => 'rt_organisation_telephone',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationTelephone')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_fax' => [
                    'id' => 'rt_organisation_fax',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationFax')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_adresse' => [
                    'id' => 'rt_organisation_adresse',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationAdresse')
                    ],
                    'type' => 'textarea',
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_email' => [
                    'id' => 'rt_organisation_email',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationEmail')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_sigle' => [
                    'id' => 'rt_organisation_sigle',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationSigle')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_siret' => [
                    'id' => 'rt_organisation_siret',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationSiret')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_ape' => [
                    'id' => 'rt_organisation_ape',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationApe')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information sur le responsable de l'entitée -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation', 'organisation.titreResponsableEntitee');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_civiliteresponsable' => [
                    'id' => 'rt_organisation_civiliteresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationCiviliteresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_prenomresponsable' => [
                    'id' => 'rt_organisation_prenomresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationPrenomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_nomresponsable' => [
                    'id' => 'rt_organisation_nomresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationNomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_fonctionresponsable' => [
                    'id' => 'rt_organisation_fonctionresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationFonctionresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_emailresponsable' => [
                    'id' => 'rt_organisation_emailresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationEmailresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_telephoneresponsable' => [
                    'id' => 'rt_organisation_telephoneresponsable',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationTelephoneresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Information sur le DPO -->
        <div class="col-md-8">
            <!-- Affichage du logo du DPO -->
            <?php
            if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
                echo $this->Html->image('logo_dpo.svg', [
                    'class' => 'logo-well'
                ]);
            }
            ?>
        </div>

        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_nom_complet_dpo' => [
                    'id' => 'rt_organisation_nom_complet_dpo',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationNomCompletDpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_numerodpo' => [
                    'id' => 'rt_organisation_numerodpo',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationNumerodpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_telephonefixe_dpo' => [
                    'id' => 'rt_organisation_telephonefixe_dpo',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationTelephonefixeDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.rt_organisation_emaildpo' => [
                    'id' => 'rt_organisation_emaildpo',
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationEmailDpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.rt_organisation_telephoneportable_dpo' => [
                    'id' => 'rt_organisation_telephoneportable_dpo',
                    'readonly' => true,
                    'placeholder' => false,
                    'label' => [
                        'text' => __d('rt_organisation', 'rt_organisation.champRtOrganisationTelephoneportableDpo')
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<!--Fin onglet Information sur l'entité-->

<script type="text/javascript">

    $(document).ready(function () {

        // Mask champs
        // L'entité
        $('#rt_organisation_telephone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_organisation_fax').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_organisation_siret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

        // Responsable de l'entité
        $('#rt_organisation_telephoneresponsable').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        // DPO
        $('#rt_organisation_telephonefixe_dpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#rt_organisation_telephoneportable_dpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

    });

</script>
