<!-- Onglet Soustraitance -->
<div id="ongletComplementaireSoustraitance" class="tab-pane">
    <br>

    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            if ($rt_externe === true) {
                echo __d('fiche', 'fiche.textInfoSoustraitantUlterieur');
                $labelFieldSoustraitances = __d('fiche', 'fiche.champSoustraitancesUlterieure');
            } else {
                echo __d('fiche', 'fiche.textInfoSoustraitant');
                $labelFieldSoustraitances = __d('fiche', 'fiche.champSoustraitances');
            }
            ?>
        </span>
        <div class="row row45"></div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Soustraitance.soustraitances', [
                'id' => 'soustraitances',
                'label' => [
                    'text' => $labelFieldSoustraitances
                ],
                'class' => 'form-control',
                'options' => Hash::combine($soustraitants, '{n}.Soustraitant.id', '{n}.Soustraitant.raisonsocialestructure'),
                'empty' => true,
                'multiple' => true,
                'required' => true,
                'data-placeholder' => __d('fiche', 'fiche.placeholderChampSoustraitance')
            ]);
            ?>
        </div>

        <?php
        if ($this->Autorisation->authorized('35', $this->Session->read('Droit.liste')) &&
            $this->request['action'] !== 'show'
        ) {
            echo $this->element('Fiches/modal/modalAddSoustraitantInTraitement');
            ?>
            <div class="pull-left">
                <a class="btn btn-outline-primary borderless" type="button" data-toggle="modal" data-target="#modalAddSoustraitantInTraitement">
                    <span class="fa fa-plus fa-lg" aria-hidden="true"></span>
                </a>
            </div>
            <?php
        }
        ?>
    </div>

    <div id="soustraitanceCards"></div>

    <?php
    if (!empty($soustraitances)) {
        foreach ($soustraitances as $key => $soustraitance) {
            echo $this->Form->hidden("Soustraitance.currentSoustraitance.{$soustraitance['Soustraitance']['soustraitant_id']}.soustraitant_id", [
                'value' => $soustraitance['Soustraitance']['soustraitant_id'],
                'id' => false
            ]);
            echo $this->Form->hidden("Soustraitance.currentSoustraitance.{$soustraitance['Soustraitance']['soustraitant_id']}.soustraitance_id", [
                'value' => $soustraitance['Soustraitance']['id'],
                'id' => false
            ]);
        }
    }
    ?>

</div>
<!-- Fin onglet Soustraitance -->

<?php
$soustraitancesRequestData = null;
if (isset($this->request->data['Soustraitance']['soustraitances'])) {
    $soustraitancesRequestData = $this->request->data['Soustraitance']['soustraitances'];
}

$soustraitancefieldsRequestData = null;
if (isset($this->request->data['WebdpoSoustraitance']) && !empty($this->request->data['WebdpoSoustraitance'])) {
    $soustraitancefieldsRequestData = $this->request->data['WebdpoSoustraitance'];
}

$validationErrors = null;
if (isset($this->validationErrors['WebdpoSoustraitance']) ) {
    $validationErrors = $this->validationErrors['WebdpoSoustraitance'];
}
?>

<script type="text/javascript">

    $(document).ready(function () {

        let eventSelect = $("#soustraitances");
        $(eventSelect).select2({
            language: "fr",
            width: '100%',
        });

        let infoSoustraitances = <?php echo json_encode(Hash::combine($soustraitants, '{n}.Soustraitant.id', '{n}.Soustraitant'))?>;

        <?php
        if (isset($fields['soustraitant']) && !empty($fields['soustraitant'])) {
            $templateFieldsSoustraitance = $this->element('Fiches/tabs/formulaire', [
                'champs' => $fields['soustraitant']
            ]);
            $arrayFieldsId = [];
            foreach ($fields['soustraitant'] as $field) {
                $arrayFieldsId[] = json_decode($field['Champ']['details'], true)['name'];
            }
        } else {
            $arrayFieldsId = [];
            $templateFieldsSoustraitance = '';
        }
        ?>

        let templateFieldsSoustraitance = <?php echo json_encode($templateFieldsSoustraitance);?>;
        let soustraitancesRequestData = <?php echo json_encode($soustraitancesRequestData);?>;
        let soustraitancefieldsRequestData = <?php echo json_encode($soustraitancefieldsRequestData);?>;
        let validationErrors = <?php echo json_encode($validationErrors);?>;

        if (soustraitancesRequestData) {
            $(soustraitancesRequestData).each(function (key, val) {
                createCardSoustraitant(templateFieldsSoustraitance, infoSoustraitances, val, soustraitancefieldsRequestData);
            });

            // Mise en évidence des champs en erreur
            if (validationErrors !== null) {
                $.each(validationErrors, function (soustraitantId, fieldError) {
                    $.each(fieldError, function (field, errorMessage) {
                        $('#st_' + soustraitantId + '_' + field).closest('div').after('<div class="error-message">'+errorMessage.toString()+'</div>');
                    });
                });
            }
        }

        $(eventSelect).on("select2:select", function (e) {
            let idSelect = e.params.data.id;

            $.ajax({
                url: '<?php echo Router::url(['controller' => 'fiches', 'action' => 'ajax_update_listing_soustraitant']);?>',
                method: 'GET',
                success: function(dataResponse) {
                    try {
                        let contents = JSON.parse(dataResponse);
                        let newInfoSoustraitants = {};

                        $.each(contents, function (k, soustraitant) {
                            $.each(soustraitant, function (k, value) {
                                newInfoSoustraitants[value['id']] = value;
                            });
                        });

                        createCardSoustraitant(templateFieldsSoustraitance, newInfoSoustraitants, idSelect);

                        $('.calendar').datetimepicker({
                            viewMode: 'year',
                            startView: 'decade',
                            format: 'dd/mm/yyyy',
                            minView: 2,
                            language: 'fr',
                            autoclose: true
                        });

                    } catch(e) {
                        console.error(e);
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        $(eventSelect).on("select2:unselect", function (e) {
            let id = e.params.data.id
            let idRemove = '#card_soustraitance_' + id;

            $('#soustraitanceCards').find(idRemove).remove();

            $('input[type="hidden"][name="data[Soustraitance][currentSoustraitance]['+id+'][soustraitant_id]"]').remove();
            $('input[type="hidden"][name="data[Soustraitance][currentSoustraitance]['+id+'][soustraitance_id]"]').remove();
        });

    });

    function createCardSoustraitant(templateFieldsSoustraitance, infoSoustraitances, idSelect, soustraitancefieldsRequestData = null) {
        let soustraitant = infoSoustraitances[idSelect];
        let colStructure = '';
        let colResponsable = '';
        let colDpo = '';
        let formSoustraitanceFields = '';

        if (templateFieldsSoustraitance) {
            let fields = templateFieldsSoustraitance.replace(/data\[WebdpoFiche\]/g, 'data[WebdpoSoustraitance]['+idSelect+']');
            fields = fields.replace(/(for|id)="(<?php echo implode('|', $arrayFieldsId);?>)"/g, '$1="st_'+idSelect+'_$2"');

            if (fields) {
                formSoustraitanceFields = '<div id="form-container-soustraitance" class="row">';
                formSoustraitanceFields = formSoustraitanceFields + fields;
                formSoustraitanceFields = formSoustraitanceFields + '</div>';
            }
        }

        if (soustraitant['siretstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>N° SIRET :</u>' +
                ' ' + soustraitant['siretstructure'] +
                '</p>';
        }

        if (soustraitant['apestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Code APE :</u>' +
                ' ' + soustraitant['apestructure'] +
                '</p>';
        }

        if (soustraitant['adressestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Adresse :</u>' +
                ' ' + soustraitant['adressestructure'] +
                '</p>';
        }

        if (soustraitant['telephonestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Téléphone :</u>' +
                ' ' + soustraitant['telephonestructure'] +
                '</p>';
        }

        if (soustraitant['faxstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Fax :</u>' +
                ' ' + soustraitant['faxstructure'] +
                '</p>';
        }

        if (soustraitant['emailstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + soustraitant['emailstructure'] +
                '</p>';
        }

        if (soustraitant['nom_complet'] != null) {
            colResponsable = colResponsable + '<h5 class="card-title">' +
                soustraitant['nom_complet'] +
                '</h5>';
        }

        if (soustraitant['fonctionresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>Fonction :</u>' +
                ' ' + soustraitant['fonctionresponsable'] +
                '</p>';
        }

        if (soustraitant['telephoneresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>Téléphone :</u>' +
                ' ' + soustraitant['telephoneresponsable'] +
                '</p>';
        }

        if (soustraitant['emailresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + soustraitant['emailresponsable'] +
                '</p>';
        }

        if (soustraitant['nom_complet_dpo'] != null) {
            colDpo = colDpo + '<h5 class="card-title">' +
                ' ' + soustraitant['nom_complet_dpo'] +
                '</h5>';
        }

        if (soustraitant['numerocnil_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Numéro d\'enregistrement CNIL :</u>' +
                ' ' + soustraitant['numerocnil_dpo'] +
                '</p>';
        }

        if (soustraitant['telephonefixe_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Téléphone fixe :</u>' +
                ' ' + soustraitant['telephonefixe_dpo'] +
                '</p>';
        }

        if (soustraitant['telephoneportable_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Téléphone portable :</u>' +
                ' ' + soustraitant['telephoneportable_dpo'] +
                '</p>';
        }

        if (soustraitant['email_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + soustraitant['email_dpo'] +
                '</p>';
        }

        $('#soustraitanceCards').append('<div id="card_soustraitance_'+idSelect+'" class="card border-primary row">' +
            '<div class="card-header">' +
                '<h4>' +
                    soustraitant['raisonsocialestructure'] +
                '</h4>' +
            '</div>' +
            '<div class="card-body">' +
                '<div class="row col-md-12">' +
                    '<div class="col-md-4">' +
                        colStructure +
                    '</div>' +
                    '<div class="col-md-4">' +
                        colResponsable +
                    '</div>' +
                    '<div class="col-md-4">' +
                        colDpo +
                    '</div>' +
                '</div>' +
                '<div class="col-md-12">' +
                    formSoustraitanceFields +
                '</div>' +
            '</div>' +
        '</div>');

        // On attribut la valeur au champ si il y en a une.
        if (soustraitancefieldsRequestData !== null) {
            $.each(soustraitancefieldsRequestData[idSelect], function (key, val) {
                let fieldId = '#st_' + idSelect + '_' + key;

                if (jQuery.isArray(val)) {
                    let ifMultiSelect = $(fieldId + '[name="data[WebdpoSoustraitance]['+idSelect+']['+key+'][]"]');

                    $.each(val, function (k, v) {
                        if (ifMultiSelect.length) {
                            $(fieldId + ' option[value="' + v + '"]').prop("selected", true);
                        } else {
                            $('#' + key + v + '[name="data[WebdpoSoustraitance]['+idSelect+']['+key+'][]"]').prop("checked", true);
                        }
                    });
                } else {
                    let ifRadio = $('input[type="radio"][name="data[WebdpoSoustraitance]['+idSelect+']['+key+']"][value="'+val+'"]');

                    if (ifRadio.length) {
                        $(ifRadio).prop("checked",true);
                    } else {
                        $(fieldId).val(val);
                    }
                }
            });
        }
    }
</script>
