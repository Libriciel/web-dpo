<!-- Onglet Annexe(s) -->
<div id="annexe" class="tab-pane">
    <br/>
    <?php
    if ($this->request->params['action'] != 'show') {
        ?>
        <h4>
            <?php echo __d('fiche', 'fiche.textAjouterPieceJointe'); ?>
        </h4>

        <br/>

        <?php
        $extentionsAccepter = __d('fiche', 'fiche.textTypeFichierAccepter');

        if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
            if ($useAllExtensionFiles['Formulaire']['useallextensionfiles'] === true) {
                $extentionsAccepter = __d('formulaire', 'formulaire.infoExtentionUtilisable');
            }
        }
        ?>

        <!-- Texte format fichier accepté -->
        <div class="alert alert-warning" role="alert">
            <?php echo $extentionsAccepter; ?>
        </div>

        <?php
        $i = $this->Html->tag(
            'i',
            '',
            ['class' => 'fas fa-cloud-upload-alt fa-4x text-primary']
        );

        $p = $this->Html->tag(
            'p',
            ('Glissez-déposez vos fichiers ici')
        );

        echo $this->Html->tag(
            'div',
            $i . $p,
            ['class' => 'dropbox']
        );
        ?>

        <br/>

        <table class="table table-striped table-hover" id="render">
            <tbody>
            <?php
            if (!empty($files)) {
                foreach ($files as $key => $file) {
                    ?>
                    <tr class="d-flex" id="rowFiche<?php echo $key; ?>">
                        <td class="tdleft col-md-2">
                            <i class="far fa-file-alt fa-lg"></i>
                        </td>

                        <td class="tdleft col-md-4">
                            <?php
                            echo $file
                            ?>
                        </td>

                        <td class="tdleft col-md-4">
                            <?php
                            if (!empty($typages)) {
                                echo $this->WebcilForm->input('Fichier_tmp.typage_tmp_' . $key, [
                                    'id' => 'typage_tmp_' . $key,
                                    'label' => [
                                        'text' => __d('fiche', 'fiche.emptySelectTypeFile') . ' ' . $file,
                                        'class' => 'sr-only',
                                    ],
                                    'class' => 'form-control typeAnnexeSelected',
                                    'options' => $typages,
                                    'empty' => __d('fiche', 'fiche.emptySelectTypeFile'),
                                    'required' => false,
                                    'placeholder' => false
                                ]);
                            }
                            ?>
                        </td>

                        <td class="tdleft col-md-2">
                            <button type="button" class="btn btn-warning"
                                    onclick="deleteFile('<?php echo $file ?>','<?php echo $key ?>')">
                                <i class="fa fa-times-circle"></i>
                                Annuler
                            </button>
                        </td>

                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
        <?php

        echo $this->element('Fiches/modal/modalErrorExtentionAnnexe', [
            'extentionsAccepter' => $extentionsAccepter
        ]);
    }
    ?>

    <?php
    if (empty($filesSave)) {
        ?>
        <div class="col-md-12 top30">
            <h4>
                <?php echo __d('fiche', 'fiche.textInfoAucunePieceJointe'); ?>
            </h4>
        </div>
        <?php
    } else {
        ?>
        <div class="col-md-12 top30">
            <h4>
                <?php
                echo __d('fiche', 'fiche.textInfoPieceJointe');
                ?>
            </h4>

            <table class="table table-striped table-hover">
                <tbody>
                <?php
                foreach ($filesSave as $key => $val) {
                    ?>
                    <tr class="d-flex" id="rowFichier<?php echo $val['Fichier']['id']; ?>">
                        <td class="tdleft col-md-2">
                            <i class="far fa-file-alt fa-lg"></i>
                        </td>

                        <td class="tdleft col-md-4">
                            <?php
                            echo $val['Fichier']['nom'];
                            ?>
                        </td>

                        <td class="tdleft col-md-4">
                            <?php
                            if (!empty($typages)) {
                                echo $this->WebcilForm->input('Fichier.typage_' . $val['Fichier']['id'], [
                                    'id' => 'typage_' . $val['Fichier']['id'],
                                    'label' => [
                                        'text' => __d('fiche', 'fiche.emptySelectTypeFile') . ' ' . $val['Fichier']['nom'],
                                        'class' => 'sr-only',
                                    ],
                                    'class' => 'form-control typeAnnexeSelected',
                                    'options' => $typages,
                                    'empty' => __d('fiche', 'fiche.emptySelectTypeFile'),
                                    'required' => false,
                                    'placeholder' => false
                                ]);
                            }
                            ?>
                        </td>

                        <td class="tdleft col-md-2 boutonsFile boutonsFile"<?php echo $val['Fichier']['id']; ?>>
                            <?php
                            echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                                'controller' => 'fiches',
                                'action' => 'download',
                                $val['Fichier']['url'],
                                $val['Fichier']['nom']
                                    ], [
                                'class' => 'btn btn-outline-dark borderless boutonShow',
                                'title' => 'Télécharger le fichier',
                                'escapeTitle' => false
                            ]);

                            if ($this->request->params['action'] != 'show') {
                                ?>
                                <button type="button"
                                        class="btn btn-outline-danger borderless btn-del-file"
                                        onclick="deleteFileSave('<?php echo $val['Fichier']['id'] ?>','<?php echo $val['Fichier']['url'] ?>')">
                                    <i class="fa fa-trash fa-lg"></i>
                                </button>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <hr/>
        <?php
    }
    ?>
</div>
<!-- Fin onglet Annexe(s) -->

<script type="text/javascript">

    $(document).ready(function () {
        $.fn.extend({
            logEvents: function () {
                return this.each(function () {
                    $(this).onAny(function (event) {
                        console.log(event.target, event.type);
                    });
                });
            }
        });

        $.fn.onAny = function (cb) {
            for (let k in this[0])
                if (k.search('on') === 0)
                    this.on(k.slice(2), function (e) {
                        // Probably there's a better way to call a callback function with right context, $.proxy() ?
                        cb.apply(this, [e]);
                    });
            return this;
        };

        $('.dropbox').onAny(function (event) {
            event.preventDefault();
            $(this).removeClass('dragover');
        });

        $('.dropbox').on('dragover', function () {
            $(this).addClass('dragover');
        });

        $('.dropbox').on('dragout, dragleave, dragexit', function () {
            $(this).removeClass('dragover');
        });

        $('.dropbox').on('drop', function (event) {
            let formData = new FormData(),
                files = [];

            for (let key in event.originalEvent.dataTransfer.files) {
                formData.append('fichiers[]', event.originalEvent.dataTransfer.files[key]);
            }

            let idFormulaire = <?php echo json_encode($formulaire_id);?>;

            $.ajax({
                url: '/fiches/saveFileTmp/'+idFormulaire,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    try {
                        let content = JSON.parse(data);
                        let countTr = $('#render').find('tbody').children('tr').length;

                        for (let key in content) {
                            if (content[key].status === "success") {
                                let tr = $('<tr class="d-flex" id="rowFiche' + countTr + '"><\/tr>')
                                    .append('<td class="tdleft col-md-2"><i class="far fa-file-alt fa-lg"><\/i><\/td>')
                                    .append('<td class="tdleft col-md-4">' + content[key].filename + '<\/td>')
                                    .append('<td class="tdleft col-md-4">' + content[key].optionType + '<\/td>')
                                    .append('<td class="tdleft col-md-2"><button type="button" class="btn btn-warning" onclick=\"deleteFile(\'' + content[key].filename + '\',\'' + countTr + '\')\"><i class="fa fa-times-circle"><!----><\/i> Annuler<\/button><\/td>')
                                $('#render').find('tbody').append(tr);
                                countTr++;
                            } else {
                                $('#errorExtentionAnnexe').modal('show');
                            }
                        }
                    } catch (e) {
                        alert("Erreur d\'enregistrement");
                        return;
                    }
                },
                error: function () {
                    alert('Erreur d\'enregistrement');
                }
            });
        });

        changeValueFieldsAIPD();

    });

    function changeValueFieldsAIPD() {
        $(document).on('focusin', '.typeAnnexeSelected', function() {
            let fieldTypage_id = $(this).attr('id');
            let typageValue = $('#'+fieldTypage_id).find('option:selected').text();

            $(this).data('previousVal', typageValue);
        }).on('change', '.typeAnnexeSelected', function(){
            let prev = $(this).data('previousVal');
            let current = $(this).find('option:selected').text();

            if (prev === 'AIPD') {
                let validationDeleteValueAIPD = confirm("Vous venez de modifier le type 'AIPD' du document.\n\nVoulez-vous réinitialiser le champ concernant le dépot de l'analyse d'impact ?\n\nSi 'OK', le champ 'Avez-vous déposé l'analyse d'impact (AIPD) dans l'onglet annexe ?' sera réinitialisé");

                if (validationDeleteValueAIPD === true) {
                    $('#depot_pia').val('');
                }
            }

            if (current === 'AIPD') {
                let realisationAIPD = $('#realisation_pia').val();
                let depotAIPD = $('#depot_pia').val();

                if (realisationAIPD != true && depotAIPD != true ||
                    realisationAIPD == true && depotAIPD != true
                ) {
                    let validationAIPD = confirm("Le document que vous venez de typer en 'AIPD', correspond-il réellement à l'analyse d'impact ?\n\nSi 'OK' les champs correspondant à la réalisation et au dépôt de l'AIPD seront modifiés à 'Oui'");

                    if (validationAIPD === true) {
                        $('#realisation_pia').val(1);
                        $('#depot_pia').parent().parent().show();
                        $('#depot_pia').val(1);
                    }
                }
            }

            $(this).blur();
        });
    }

    function deleteFile(file, key) {
        let validationDeleteFile = confirm("Voulez vous supprimer l'annexe \" "+file+" \" ?");

        if (validationDeleteFile === true) {
            let typageAnnexe = $('#typage_tmp_'+key).find('option:selected').text()

            $.ajax({
                url: '/fiches/deleteFile',
                method: 'POST',
                data: {filename: file},
                success: function () {
                    let row = document.getElementById("rowFiche" + key);
                    row.parentNode.removeChild(row);

                    resetFieldDepotAIPD(typageAnnexe);
                },
                error: function () {
                    alert('Erreur lors de la suppression du fichier');
                }
            });
        }
    }

    function deleteFileSave(idFile, urlFile) {
        let validationDeleteFile = confirm("Voulez vous supprimer l'annexe ?");

        if (validationDeleteFile === true) {
            let typageAnnexe = $('#typage_'+idFile).find('option:selected').text()

            $.ajax({
                url: '/fiches/deleteRecordingFile',
                method: 'POST',
                data: {
                    idFile: idFile,
                    urlFile: urlFile
                },
                success: function () {
                    let row = document.getElementById("rowFichier" + idFile);
                    row.parentNode.removeChild(row);

                    resetFieldDepotAIPD(typageAnnexe);
                },
                error: function () {
                    alert('Erreur lors de la suppression du fichier');
                }
            });
        }
    }

    function resetFieldDepotAIPD(typageAnnexe)
    {
        if (typageAnnexe === 'AIPD') {
            let validationDeleteValueAIPD = confirm("Vous venez de supprimer un document typé 'AIPD'.\n\nSi 'OK', le champ 'Avez-vous déposé l'analyse d'impact (AIPD) dans l'onglet annexe ?' sera réinitialisé");

            if (validationDeleteValueAIPD === true) {
                $('#depot_pia').val('');
            }
        }

        return;
    }

</script>
