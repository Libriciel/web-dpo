<!-- Onglet Information sur l'entité -->
<div id="infos" class="tab-pane">
    <br/>
    <!--Information sur l'entité -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation','organisation.textEntite');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_raisonsociale' => [
                    'id' => 'entite_raisonsociale',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champRaisonsociale')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_telephone' => [
                    'id' => 'telephone',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champTelephone')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_fax' => [
                    'id' => 'fax',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champFax')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.entite_adresse' => [
                    'id' => 'adresse',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champAdresse')
                    ],
                    'type' => 'textarea',
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_email' => [
                    'id' => 'email',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champEmail')
                    ],
                    'readonly' => true,
                    'required' => true
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_sigle' => [
                    'id' => 'sigle',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSigle')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.entite_siret' => [
                    'id' => 'siret',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSiret')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_ape' => [
                    'id' => 'ape',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champApe')
                    ],
                    'readonly' => true,
                    'required' => true
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information sur le responsable de l'entitée -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation', 'organisation.titreResponsableEntitee');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_civiliteresponsable' => [
                    'id' => 'civiliteresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champCiviliteresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_nomresponsable' => [
                    'id' => 'nomresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champNomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_prenomresponsable' => [
                    'id' => 'prenomresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champPrenomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_fonctionresponsable' => [
                    'id' => 'fonctionresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champFonctionresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_emailresponsable' => [
                    'id' => 'emailresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champEmailresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_telephoneresponsable' => [
                    'id' => 'telephoneresponsable',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champTelephoneresponsable')
                    ],
                    'readonly' => true,
                    'required' => true
                ]
            ]);
            ?>
        </div>

        <!-- Information sur le DPO -->
        <div class="col-md-8">
            <!-- Affichage du logo du DPO -->
            <?php
            if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
                echo $this->Html->image('logo_dpo.svg', [
                    'class' => 'logo-well'
                ]);
            }
            ?>
        </div>

        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_dpo' => [
                    'id' => 'dpo',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champDpo')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_numerodpo' => [
                    'id' => 'numerodpo',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champNumerodpo')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_fixDpo' => [
                    'id' => 'fixDpo',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champFixDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.entite_emailDpo' => [
                    'id' => 'emailDpo',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champEmailDpo')
                    ],
                    'readonly' => true,
                    'required' => true
                ],
                'Trash.entite_portableDpo' => [
                    'id' => 'portableDpo',
                    'readonly' => true,
                    'placeholder' => false,
                    'label' => [
                        'text' => __d('fiche', 'fiche.champPortableDpo')
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<!--Fin onglet Information sur l'entité-->

<script type="text/javascript">

    $(document).ready(function () {

        // Mask champs
        // L'entité
        $('#telephone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#fax').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#siret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

        // Responsable de l'entité
        $('#telephoneresponsable').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        // DPO
        $('#fixDpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#portableDpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

    });

</script>