<?php
echo $this->Html->script('Fiches/pia');

if (!isset($fieldsIsRequired)){
    $fieldsIsRequired = true;
}
?>

<div id="info_pia" class="tab-pane <?php echo $classActivePia; ?>">
    <br/>

    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.titleTraitementNotRequiredAIPD');
            ?>
        </span>
        <br>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            foreach (Fiche::LISTING_FIELDS_TRAITEMENT_NO_REQUIRED_PIA as $key => $id) {
                $text = ucwords($id, "_");
                $text = str_replace('_', '', $text);

                $classDisplayInput = 'displayInput';
                $before = '<hr>';
                if ($key == 0) {
                    $classDisplayInput = '';
                    $before = '';
                }

                echo $this->WebcilForm->input('WebdpoFiche.'.$id, [
                    'id' => $id,
                    'label' => [
                        'text' => __d('fiche', 'fiche.champ'.$text),
                    ],
                    'options' => [
                        'Oui' => 'Oui',
                        'Non' => 'Non'
                    ],
                    'class' => 'col-md-2 transformSelect form-control '.$classDisplayInput,
                    'required' => $fieldsIsRequired,
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' ',
                    'before' => $before
                ]);
            }
            ?>
        </div>
    </div>

    <br>

    <div id="liste_obligatoire">
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.titleTraitementRequiredAIPD');
                ?>
            </span>
            <br>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                foreach (Fiche::LISTING_FIELDS_TRAITEMENT_REQUIRED_PIA as $key => $id) {
                    $text = ucwords($id, "_");
                    $text = str_replace('_', '', $text);

                    $classDisplayInput = 'displayInput';
                    $before = '<hr>';
                    if ($key == 0) {
                        $classDisplayInput = '';
                        $before = '';
                    }

                    echo $this->WebcilForm->input('WebdpoFiche.'.$id, [
                        'id' => $id,
                        'label' => [
                            'text' => __d('fiche', 'fiche.champ'.$text),
                        ],
                        'options' => [
                            'Oui' => 'Oui',
                            'Non' => 'Non'
                        ],
                        'class' => 'col-md-2 fieldRequiredPia transformSelect form-control '.$classDisplayInput,
                        'required' => $fieldsIsRequired,
                        'empty' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' ',
                        'before' => $before
                    ]);
                }
                ?>
            </div>
        </div>
    </div>

    <br>

    <div id="liste_criteres">
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.titleCriteresAIPD');
                ?>
            </span>
            <br>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('WebdpoFiche.criteres', [
                    'id' => 'criteres',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champCriteres')
                    ],
                    'multiple' => 'checkbox',
                    'class' => 'form-check checkbox',
                    'options' => Fiche::LISTE_CRITERES,
                ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('WebdpoFiche.traitement_considere_risque', [
                    'id' => 'traitement_considere_risque',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champTraitementConsidereRisque')
                    ],
                    'options' => [
                        'Oui' => 'Oui',
                        'Non' => 'Non'
                    ],
                    'class' => 'transformSelect form-control displayInput',
                    'required' => $fieldsIsRequired,
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' '
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<!--https://www.cnil.fr/fr/liste-traitements-aipd-requise-->
