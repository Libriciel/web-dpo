<!-- Onglet Formulaire -->
<?php
echo $this->Html->script([
    'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
    'smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js'
]);
echo $this->Html->css('/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min');

$calendrier = [];
$conditions = [];

$col = 1;
$line = 1;
?>

<!-- Champs du formulaire -->
<div class="col-md-6">
    <?php
    foreach ($champs as $value) {
        if ($value['Champ']['colonne'] > $col) {
            ?>
            </div>

            <div class="col-md-6">
            <?php
            $line = 1;
            $col++;
        }

        if ($value['Champ']['ligne'] > $line) {
            for ($i = $line; $i < $value['Champ']['ligne']; $i++) {
                ?>
                <div class="row row45"></div>
                <?php
            }
            $line = $value['Champ']['ligne'];
        }

        $options = json_decode($value['Champ']['details'], true);

        if (isset($options['conditions']) && !empty($options['conditions'])) {
            $fieldConditions = json_decode($options['conditions'], true);
            $conditions = array_merge($conditions, $fieldConditions);
        }

        $required = $options['obligatoire'];
        if (isset($fieldsIsRequired) && $fieldsIsRequired === false) {
            $required = false;
        }
        ?>

        <div class="row row45">
            <div class="col-md-12">
                <?php
                switch ($value['Champ']['type']) {
                    // Petit champ texte
                    case 'input':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'required' => $required,
                            'placeholder' => $options['placeholder'],
                        ]);
                        break;

                    // Grand champ texte
                    case 'textarea':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'type' => 'textarea',
                            'required' => $required,
                            'placeholder' => $options['placeholder']
                        ]);
                        break;

                    // Champ date
                    case 'date':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'class' => 'form-control calendar',
                            'required' => $required,
                            'placeholder' => $options['placeholder']
                        ]);
                        $calendrier[] = $options['name'];
                        break;

                    // Cases à cocher
                    case 'checkboxes':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'required' => $required,
                            'multiple' => 'checkbox',
                            'class' => 'form-check',
                            'options' => $options['options'],
                        ]);
                        break;

                    // Menu déroulant
                    case 'deroulant':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'class' => 'form-control custom-select',
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'options' => $options['options'],
                            'required' => $required,
                            'empty' => true,
                            'placeholder' => false,
                            'data-placeholder' => ' '
                        ]);
                        break;

                    // Menu multi-select
                    case 'multi-select':
                        echo $this->WebcilForm->input('WebdpoFiche.'.$options['name'], [
                            'id' => $options['name'],
                            'options' => $options['options'],
                            'class' => 'form-group multi-select2',
                            'label' => [
                                'text' => $options['label'],
                            ],
                            'required' => $required,
                            'multiple' => 'multiple'
                        ]);
                        break;

                    // Choix unique
                    case 'radios':
                        $fieldName = 'WebdpoFiche.'.$options['name'];
                        if (false === strpos($fieldName, '.')) {
                            $modelName = Inflector::classify($this->request->params['controller']);
                        } else {
                            list($modelName, $fieldName) = explode('.', $fieldName);
                        }

                        $requiredRadio = '';
                        if ($options['obligatoire'] === true) {
                            $requiredRadio = 'required="required"';
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label" for="<?php echo $options['name']; ?>">
                                <?php
                                    echo $options['label'];
                                    if ($options['obligatoire'] === true) {
                                        echo '<span class="requis"> *</span>';
                                    }
                                ?>
                            </label>
                            <div <?php if ($options['obligatoire']): echo 'required="required"'; endif;?>>
                                <input type="hidden" id="<?php echo $options['name']; ?>" name="data[<?php echo $modelName;?>][<?php echo $fieldName;?>]" value="" />
                                <?php
                                foreach ($options['options'] as $key => $val) {
                                    $radio_id = $options['name'].$key;
                                    $checked = $val == $this->request->data("{$modelName}.{$fieldName}") ? ' checked="checked"' : '';

                                    echo '<div class="form-check">'
                                        .'<input class="form-check-input" type="radio" id="'.$radio_id.'" name="data['.$modelName.']['.$fieldName.']" value="'.$val.'" '.$checked.'>'
                                        .'<label class="form-check-label" for="'.$radio_id.'">'
                                            . $val
                                        .'</label>'
                                    .'</div>';
                                }
                                ?>
                            </div>
                            <?php
                            echo $this->WebcilForm->error("{$modelName}.{$fieldName}");
                            ?>
                        </div>
                        <?php
                        break;

                    // Titre de catégorie
                    case 'title':
                        ?>
                        <div class="text-center">
                            <h1>
                                <?php
                                echo $options['content'];
                                ?>
                            </h1>
                        </div>
                        <?php
                        break;

                    // Champ d'information
                    case 'help':
                        ?>
                        <div class="alert alert-info">
                            <div class="text-center">
                                <i class="fa fa-info-circle fa-2x"></i>
                            </div>
                            <div class="col-md-12">
                                <?php
                                echo $options['content'];
                                ?>
                            </div>
                        </div>
                        <?php
                        break;

                    // Label
                    case 'texte':
                        echo '<div class="form-group">'
                            . '<h5>'
                                . $options['content']
                            .'</h5>'
                        . '</div>';
                        break;

                    default :
                        break;
                }
                $line++;
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<!-- Fin onglet Formulaire -->

<script type="text/javascript">

    $(document).ready(function () {
        /**
         * Si le champ n'est pas affiché au départ (dans le second onglet), la largeur correcte n'est pas calculée, il
         * faut donc la recalculer au changement d'onglet.
         *
         * @see Search.prototype.resizeSearch
         */
        let fixSelect2SearchFieldWidth = function() {
            $('.select2.select2-container').each(function(idx, container) {
                let field = $(container).find('.select2-search__field');
                if (field && $(field).attr('placeholder') !== '') {
                    $(field).width($(container).width());
                }
            });
        };

        /**
         * Lorsque la classe d'un tab-pane change, on exécute la fonction fixSelect2SearchFieldWidth
         * @see https://stackoverflow.com/a/19401707
         */
            // let $div = $("#info_formulaire");
        let observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.attributeName === "class") {
                    let attributeValue = $(mutation.target).prop(mutation.attributeName);
                    fixSelect2SearchFieldWidth();
                }
            });
        });

        // observer.observe($div[0], { attributes: true });
        $('.tab-pane').each(function() {
            observer.observe(this, { attributes: true });
        });
        //--------------------------------------------------------------------------------------------------------------

        let champsDate = null;
        champsDate = <?php echo json_encode($calendrier); ?>;
        jQuery.each(champsDate, function(key, val){
            $('#' + val).datetimepicker({
                viewMode: 'year',
                startView: 'decade',
                format: 'dd/mm/yyyy',
                minView: 2,
                language: 'fr',
                autoclose: true
            });
        });

        // Mise en place des conditions
        let conditions = <?php echo json_encode($conditions); ?>;
        $.each(conditions, function (key, value) {
            let valueIfTheField = $('#'+value['ifTheField']);

            // Si le champ a comme balise "select" (deroulant et multi-select)
            if ($(valueIfTheField).is('select')) {
                // Champ multi-select
                if ($(valueIfTheField).hasClass('multi-select2') === true) {
                    // Si le champ multi-select est visible
                    $(valueIfTheField).parent().parent().on('show', function() {
                        let showCondition = false;

                        if ($(valueIfTheField).val()) {
                            $.each($(valueIfTheField).val(), function (k, v) {
                                if (jQuery.inArray(v, value['hasValue']) !== -1) {
                                    showCondition = true;
                                }
                            });
                        }

                        if (showCondition === true) {
                            shownHideField(value['mustBe'], value['thenTheField'])
                        } else {
                            shownHideField(value['ifNot'], value['thenTheField']);
                        }
                    });

                    // Si le champ multi-select n'est pas visible
                    $(valueIfTheField).parent().parent().on('hide', function() {
                        shownHideField(value['ifNot'], value['thenTheField']);
                    });

                    if ($(valueIfTheField).parent().parent().css('display') !== 'none') {
                        let ifTheField_val = $(valueIfTheField).val();

                        if (ifTheField_val) {
                            $.each(ifTheField_val, function (k, v) {
                                if (jQuery.inArray(v, value['hasValue']) !== -1 &&
                                    $(valueIfTheField).parent().parent().css('display') !== 'none'
                                ) {
                                    shownHideField(value['mustBe'], value['thenTheField'])
                                } else {
                                    shownHideField(value['ifNot'], value['thenTheField']);
                                }
                            });
                        } else {
                            shownHideField(value['ifNot'], value['thenTheField']);
                        }
                    } else {
                        shownHideField(value['ifNot'], value['thenTheField']);
                    }
                } else {
                    // Si le champ déroulant est visible
                    $(valueIfTheField).parent().parent().on('show', function() {
                        if ($(valueIfTheField).val() === value['hasValue']) {
                            shownHideField(value['mustBe'], value['thenTheField'])
                        } else {
                            shownHideField(value['ifNot'], value['thenTheField']);
                        }
                    });

                    // Si le champ déroulant n'est pas visible
                    $(valueIfTheField).parent().parent().on('hide', function() {
                        shownHideField(value['ifNot'], value['thenTheField']);
                    });

                    if ($(valueIfTheField).val() === value['hasValue'] &&
                        $(valueIfTheField).parent().parent().css('display') !== 'none'
                    ) {
                        shownHideField(value['mustBe'], value['thenTheField'])
                    } else {
                        shownHideField(value['ifNot'], value['thenTheField']);
                    }
               }

               // Quand le champ deroulant ou multi-select change de valeur
               $(valueIfTheField).change(function () {
                   if ($(valueIfTheField).hasClass('multi-select2') === true) {
                       if (Array.isArray(value['hasValue']) === true) {
                            let showCondition = false;

                            if ($(this).val()) {
                                $.each($(this).val(), function (k, v) {
                                    if (jQuery.inArray(v, value['hasValue']) !== -1) {
                                        showCondition = true;
                                    }
                                });
                            }

                            if (showCondition === true) {
                                shownHideField(value['mustBe'], value['thenTheField'])
                            } else {
                                shownHideField(value['ifNot'], value['thenTheField']);
                            }
                       }
                   } else {
                       if ($(this).val() === value['hasValue']) {
                           shownHideField(value['mustBe'], value['thenTheField'])
                       } else {
                           shownHideField(value['ifNot'], value['thenTheField']);
                       }
                   }
               });
           }

           // Si le champ a comme balise "input"
           if ($(valueIfTheField).is('input')) {

               let valuehasValue = $('#'+value['hasValue']);

               // Si le champ est de type "checkbox"
               if ($(valuehasValue).attr('type') === "checkbox") {
                   $(valuehasValue).parent().parent().parent().on('show', function() {
                       if ($(valuehasValue).prop("checked") === true) {
                           shownHideField(value['mustBe'], value['thenTheField'])
                       } else {
                           shownHideField(value['ifNot'], value['thenTheField']);
                       }
                   });

                   $(valuehasValue).parent().parent().parent().on('hide', function() {
                       shownHideField(value['ifNot'], value['thenTheField']);
                   });

                   if ($(valuehasValue).prop("checked") === true &&
                       $(valueIfTheField).parent().parent().css('display') !== 'none'
                   ) {
                       shownHideField(value['mustBe'], value['thenTheField'])
                   } else {
                       shownHideField(value['ifNot'], value['thenTheField']);
                   }

                   $('input[name ="data[WebdpoFiche]['+value['ifTheField']+'][]"]').change(function() {
                       let valueSelected = [];
                       $.each($('input[name ="data[WebdpoFiche]['+value['ifTheField']+'][]"]:checked'), function(){
                           valueSelected.push($(this).attr('id'));
                       });

                       if (Array.isArray(valueSelected) === true) {
                           let showCondition = false;

                           if (valueSelected) {
                               $.each(valueSelected, function (k, v) {
                                   if (jQuery.inArray(v, value['hasValue']) !== -1) {
                                       showCondition = true;
                                   }
                               });
                           }

                           if (showCondition === true) {
                               shownHideField(value['mustBe'], value['thenTheField'])
                           } else {
                               shownHideField(value['ifNot'], value['thenTheField']);
                           }
                       }
                   });
               }

               // Si le champ est de type "radio"
               if ($('input[type="radio"][value="'+value['hasValue']+'"][name="data[WebdpoFiche]['+value['ifTheField']+']"]').length === 1) {

                   // Quand le changer est affiché
                   $(valueIfTheField).parent().parent().on('show', function() {
                       $(valueIfTheField).parent().find('input[type=radio]:checked').each(function () {
                           if ($(this).val() === value['hasValue']) {
                               shownHideField(value['mustBe'], value['thenTheField'])
                           } else {
                               shownHideField(value['ifNot'], value['thenTheField']);
                           }
                       });
                   });

                   // Quand le champ est caché
                   $(valueIfTheField).parent().parent().on('hide', function() {
                       shownHideField(value['ifNot'], value['thenTheField']);
                   });

                   if ($(valueIfTheField).parent().parent().css('display') !== 'none') {
                       if ($(valueIfTheField).parent().find('input[type=radio]:checked').val() === value['hasValue']) {
                           shownHideField(value['mustBe'], value['thenTheField'])
                       } else {
                           shownHideField(value['ifNot'], value['thenTheField']);
                       }

                   } else {
                       shownHideField(value['ifNot'], value['thenTheField']);
                   }

                   // Quand le champ radio change de valeur
                   $(valueIfTheField).parent().find('input[type=radio]').on('change', function () {
                       if ($(this).val() === value['hasValue']) {
                           shownHideField(value['mustBe'], value['thenTheField'])
                       } else {
                           shownHideField(value['ifNot'], value['thenTheField']);
                       }
                   });
               }
           }
        });

        function shownHideField(condition, thenTheField)
        {
            if (condition === 'shown') {
                $('#' + thenTheField).parent().parent().show();
            } else {
                $('#' + thenTheField).parent().parent().hide();
            }

            resizeFormContainer();
        }
    });

    let resizeFormContainer = function () {
        let idContainer = '#form-container-formulaire',
            fixmeOffset = 45;

        // On cherche l'élément le plus bas du tableau
        let last = null,
            lastPosition = 0,
            // lastHeight = 500,
            lastHeight = parseInt($(idContainer).css('min-height')),
            currentPosition = null,
            currentHeight = null,
            //children = $(idContainer).children(),
            children = $(idContainer).find('.form-group'),
            newHeight = null;

        $(children).each(function () {
            currentPosition = $(this).offset().top;
            currentHeight = $(this).height();

            if ((currentPosition + currentHeight) >= (lastPosition + lastHeight)) {
                last = $(this);
                lastPosition = currentPosition;
                lastHeight = currentHeight;
            }
        });

        newHeight = (lastPosition + lastHeight - $(idContainer).offset().top) + fixmeOffset;
        newHeight = Math.round(newHeight);

        $(idContainer).css('height', newHeight + 'px');
    };

    $(window).resize(function() {
       resizeFormContainer();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        resizeFormContainer();
    });

    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
            let el = $.fn[ev];
            $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);
</script>
