<?php
$initialisationRecu = isset($initialisationRecu) ? $initialisationRecu : false;
$empty = true;
$keySubmit = 'WebdpoFiche.';
$keySubmitService = 'WebdpoFiche.';

if (in_array($this->request->params['action'], ['edit', 'show']) === true) {
    $empty = false;
    $keySubmit = 'Trash.';

    if ($initialisationRecu === true) {
        $keySubmitService = 'WebdpoFiche.';
    } else {
        $keySubmitService = 'Trash.';
    }
}

if (!isset($fieldsIsRequired)){
    $fieldsIsRequired = true;
}

if (!isset($initialisationMode)){
    $initialisationMode = false;
}

if (!isset($showPartage)){
    if ($this->request->params['action'] === 'add') {
        $showPartage = true;
    } else {
        $showPartage = false;
    }
}

$hiddenRedacteur = 'hidden';
if (!isset($usefieldsredacteur)) {
    $usefieldsredacteur = false;
}
if ($usefieldsredacteur === true) {
    $hiddenRedacteur = '';
}
?>

<!-- Onglet Information concernant le traitement -->
<div id="information_traitement" class="tab-pane <?php echo $classActiveInfoGenerale; ?>">
    <br/>
    <?php
    if ($initialisationMode !== true) {
        if ($usefieldsredacteur === true) {
            ?>
            <!-- Information sur le rédacteur -->
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoContact');
                    ?>
                </span>
                <div class="row row35"></div>
            </div>
            <?php
        }
        ?>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    $keySubmit.'declarantpersonnenom' => [
                        'id' => 'declarantpersonnenom',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDeclarantpersonnenom')
                        ],
                        'readonly' => true,
                        'required' => true,
                        'type' => $hiddenRedacteur
                    ],
                    $keySubmit.'declarantpersonneportable' => [
                        'id' => 'declarantpersonneportable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDeclarantpersonneportable')
                        ],
                        'readonly' => true,
                        'placeholder' => false,
                        'type' => $hiddenRedacteur
                    ]
                ]);


                if ($this->request->data['Fiche']['user_id'] === $this->Session->read('Auth.User.id') &&
                    $this->request->params['action'] !== 'show'
                ) {
                    if (isset($servicesUser) && !empty($servicesUser)) {
                        echo $this->WebcilForm->input('service_id', [
                            'id' => 'service_id',
                            'label' => [
                                'text' => __d('fiche', 'fiche.champDeclarantservice')
                            ],
                            'options' => $servicesUser,
                            'class' => 'usersDeroulant form-control',
                            'empty' => true,
                            'placeholder' => false,
                            'data-placeholder' => __d('fiche', 'fiche.placeholderChoisirService'),
                        ]);
                    }
                } else {
                    echo $this->WebcilForm->input('Trash.service', [
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDeclarantservice')
                        ],
                        'value' => $this->request->data['Trash']['service_libelle'],
                        'readonly' => true,
                        'disabled' => true
                    ]);
                }
                ?>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    $keySubmit.'declarantpersonneemail' => [
                        'id' => 'declarantpersonneemail',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDeclarantpersonneemail')
                        ],
                        'readonly' => true,
                        'required' => true,
                        'type' => $hiddenRedacteur
                    ],
                    $keySubmit.'declarantpersonnefix' => [
                        'id' => 'declarantpersonnefix',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDeclarantpersonnefix')
                        ],
                        'readonly' => true,
                        'placeholder' => false,
                        'type' => $hiddenRedacteur
                    ]
                ]);

                if ($showPartage === true) {
                    echo $this->WebcilForm->input('partage', [
                        'id' => 'partage',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champPartage')
                        ],
                        'options' => [
                            false => 'Non',
                            true => 'Oui'
                        ],
                        'default' => false,
                        'class' => 'form-control custom-select',
                        'empty' => false,
                        'required' => true,
                        'placeholder' => false,
                    ]);
                }
                ?>
            </div>
        </div>
        <?php
    }

    if (!empty($options_referentiel)) {
        $valueReferentiel = null;
        ?>
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo 'Informations concernant le référentiel à associer au traitement :';
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <?php
        if (isset($this->request->data['Fiche']['referentiel_id'])) {
            $valueReferentiel = $this->request->data['Fiche']['referentiel_id'];
        }
        ?>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $this->WebcilForm->input('referentiel_id', [
                    'id' => 'referentiel_id',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champReferentiel')
                    ],
                    'options' => $options_referentiel,
                    'class' => 'usersDeroulant form-control',
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => __d('fiche', 'fiche.placeholderChampReferentiel'),
                    'value' => $valueReferentiel
                ]);
                ?>
            </div>
        </div>
        <?php
    }

    if (isset($referentielTraitement) && !empty($referentielTraitement)) {
        ?>
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo 'Informations concernant le référentiel associé au traitement :';
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                echo $this->WebcilForm->input('showreferentiel', [
                    'id' => 'showreferentiel',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champReferentiel')
                    ],
                    'readonly' => true,
                    'placeholder' => false,
                    'value' => $referentielTraitement['Referentiel']['name']
                ]);
                ?>
            </div>
        </div>

        <table class="table" id="renderReferentiel">
            <thead></thead>
            <tbody>
            <tr class="d-flex">
                <td class="col-md-1">
                    <i class="far fa-file-alt fa-lg"></i>
                </td>

                <td class="col-md-9">
                    <?php
                    echo $referentielTraitement['Referentiel']['name_fichier'];
                    ?>
                </td>

                <td class="col-md-2">
                    <?php
                    echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                        'controller' => 'referentiels',
                        'action' => 'download',
                        $referentielTraitement['Referentiel']['fichier'],
                        $referentielTraitement['Referentiel']['name_fichier']
                    ], [
                        'class' => 'btn btn-outline-dark borderless boutonShow btn-sm my-tooltip',
                        'title' => 'Télécharger le référentiel',
                        'escapeTitle' => false
                    ]);
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }

    $valueNorme = null;
    if (isset($normeTraitement) && !empty($normeTraitement)) {
        $norme = $normeTraitement['Norme']['norme'] . '-' . sprintf('%03d', $normeTraitement['Norme']['numero']) . ' : ' . $normeTraitement['Norme']['libelle'];
        $normeDescription = preg_replace('@<[^>]*?>.*?>@si', "\n\r", $normeTraitement['Norme']['description']);
        ?>

        <!-- Information sur la norme -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo ("Informations concernant la norme associée au traitement : ");
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('norme', [
                    'id' => 'shownorme',
                    'readonly' => true,
                    'placeholder' => false,
                    'value' => $norme
                ]);
                ?>

                <table class="table" id="render">
                    <tbody>
                    <tr>
                        <td class="col-md-1">
                            <i class="far fa-file-alt fa-lg"></i>
                        </td>

                        <td class="col-md-9 tdleft">
                            <?php
                            echo $normeTraitement['Norme']['name_fichier'];
                            ?>
                        </td>

                        <td class="col-md-2">
                            <?php
                            echo $this->Html->link('<i class="fa fa-download fa-lg"></i>', [
                                'controller' => 'normes',
                                'action' => 'download',
                                $normeTraitement['Norme']['fichier'],
                                $normeTraitement['Norme']['name_fichier']
                            ], [
                                'class' => 'btn btn-outline-dark borderless boutonShow btn-sm my-tooltip',
                                'title' => 'Télécharger la norme',
                                'escapeTitle' => false
                            ]);
                            ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('descriptionNorme', [
                    'id' => 'showdescriptionNorme',
                    'type' => 'textarea',
                    'readonly' => true,
                    'placeholder' => false,
                    'value' => $normeDescription
                ]);
                ?>
            </div>
        </div>
        <?php
    } else {
        if (!empty($options_normes)) {
            ?>
            <!-- Information sur la norme -->
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoNorme');
                    ?>
                </span>
                <div class="row row35"></div>
            </div>

            <?php
            if (isset($this->request->data['Fiche']['norme_id'])) {
                $valueNorme = $this->request->data['Fiche']['norme_id'];
            }
            ?>

            <div class="row">
                <!-- Colonne de gauche -->
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('norme_id', [
                        'id' => 'norme_id',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champNorme')
                        ],
                        'options' => $options_normes,
                        'class' => 'usersDeroulant form-control',
                        'empty' => true,
                        'placeholder' => false,
                        'data-placeholder' => __d('norme', 'norme.placeholderFiltreNorme'),
                        'value' => $valueNorme
                    ]);
                    ?>
                </div>

                <!-- Colonne de droite -->
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('descriptionNorme', [
                        'id' => 'descriptionNorme',
                        'type' => 'textarea',
                        'readonly' => true,
                        'disabled' => true,
                        'placeholder' => false
                    ]);
                    ?>
                </div>
            </div>
            <?php
        }
    }
    ?>

    <!-- Information concernant le traitement -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoTraitement');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'WebdpoFiche.outilnom' => [
                    'id' => 'outilnom',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champOutilnom')
                    ],
                    'required' => true,
                    'placeholder' => false
                ],
                'transfert_hors_ue' => [
                    'id' => 'transfert_hors_ue',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champTransfertHorsUe'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champTransfertHorsUeDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'class' => 'form-control custom-select',
                    'required' => true,
                    'empty' => false,
                    'data-placeholder' => ' ',
                    'default' => false
                ],
                'donnees_sensibles' => [
                    'id' => 'donnees_sensibles',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champDonneesSensibles'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champDonneesSensiblesDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'class' => 'form-control custom-select',
                    'required' => true,
                    'empty' => false,
                    'data-placeholder' => ' ',
                    'default' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('WebdpoFiche.finaliteprincipale', [
                'id' => 'finaliteprincipale',
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champFinaliteprincipale'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champFinaliteprincipaleDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'type' => 'textarea',
                'required' => true,
                'placeholder' => false
            ]);
            ?>
        </div>
    </div>

    <?php
    if ($formulaireOLD['Formulaire']['oldformulaire'] === false) {
        ?>
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoPia');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <?php
        if (isset($usePIA) && $usePIA['Formulaire']['usepia'] === true) {
            ?>
            <div class="row">
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('obligation_pia', [
                        'id' => 'obligation_pia',
                        'options' => [
                            true => 'Oui',
                            false => 'Non'
                        ],
                        'class' => 'form-control custom-select',
                        'required' => true,
                        'readonly' => true,
                        'disabled' => true,
                        'empty' => true,
                        'placeholder' => false,
                        'data-placeholder' => ' '
                    ]);
                    ?>
                </div>

                <div class="col-md-6"></div>
            </div>
            <?php
        } else {
            echo $this->WebcilForm->hidden('obligation_pia', [
                'id' => 'obligation_pia',
                'value' => null
            ]);
        }
        ?>

        <div class="row">
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('realisation_pia', [
                    'id' => 'realisation_pia',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champRealisationPia'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champRealisationPiaDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'class' => 'form-control custom-select',
                    'required' => true,
                    'empty' => $empty,
                    'placeholder' => false,
                    'data-placeholder' => ' '
                ]);
                ?>
            </div>

            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('depot_pia', [
                    'id' => 'depot_pia',
                    'options' => [
                        true => 'Oui',
                        false => 'Non'
                    ],
                    'class' => 'form-control custom-select',
                    'required' => true,
                    'empty' => true,
                    'placeholder' => false,
                    'data-placeholder' => ' '
                ]);
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <!-- Co-responsabilité sur le traitement -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoCoresponsable');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('coresponsable', [
                'id' => 'coresponsable',
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        __d('fiche', 'fiche.champCoresponsable'),
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champCoresponsableDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'class' => 'form-control custom-select',
                'required' => true,
                'empty' => false,
                'data-placeholder' => ' ',
                'default' => false
            ]);
            ?>
        </div>
    </div>

    <!-- Sous-traitance sur le traitement -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            if ($rt_externe === false) {
                echo __d('fiche', 'fiche.textInfoSousTraitance');
                $libelleFieldSoustraitance = __d('fiche', 'fiche.champSoustraitance');
            } else {
                echo __d('fiche', 'fiche.textInfoSousTraitanceUlterieur');
                $libelleFieldSoustraitance = __d('fiche', 'fiche.champSoustraitanceUlterieur');
            }
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('soustraitance', [
                'id' => 'soustraitance',
                'label' => [
                    'text' => $this->Html->tag(
                        'abbr',
                        $libelleFieldSoustraitance,
                        [
                            'data-content' => nl2br(__d('fiche', 'fiche.champSoustraitanceDefinition')),
                            'class' => 'popoverText'
                        ]
                    )
                ],
                'options' => [
                    true => 'Oui',
                    false => 'Non'
                ],
                'class' => 'form-control custom-select',
                'required' => true,
                'empty' => false,
                'data-placeholder' => ' ',
                'default' => false
            ]);
            ?>
        </div>
    </div>
</div>
<!-- Fin onglet Information concernant le traitement-->

<script type="text/javascript">

    $(document).ready(function () {

        let descriptions = <?php echo json_encode($descriptions_normes); ?>;
        //let normeId = <?php ////echo json_encode($valueNorme);?>////;
        let normeId = $('#norme_id').val();

        if (typeof(normeId) != "undefined" && normeId !== null && normeId) {
            let str = descriptions[normeId].replace(/<br\s*[\/]?>/gi, "\n");
            $("#descriptionNorme").val(str);
        }

        $('#norme_id').change(function () {
            let idNorme = $(this).val();
            let str = '';

            if (idNorme === '') {
                str = null;
            } else {
                str = descriptions[idNorme].replace(/<br\s*[\/]?>/gi, "\n");
            }

            $("#descriptionNorme").val(str);
        });

        displayFieldDepotPIA($('#realisation_pia').val());
        $('#realisation_pia').change(function () {
            let val = $(this).val();
            displayFieldDepotPIA(val);
        });

        displayFieldPartage($('#service_id').val())
        $('#service_id').change(function () {
            displayFieldPartage($(this).val());
        });
    });

    function displayFieldDepotPIA(val)
    {
        if (val == true) {
            $('#depot_pia').parent().parent().show();
        } else {
            $('#depot_pia').val('');
            $('#depot_pia').parent().parent().hide();
        }
    }

    function displayFieldPartage(val)
    {
        if (val) {
            $('#partage').parent().show();
        } else {
            $('#partage').val(false);
            $('#partage').parent().hide();
        }
    }

</script>
