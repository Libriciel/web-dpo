<!-- Onglet Co-responsable -->
<div id="ongletComplementaireCoresponsable" class="tab-pane">
    <br>

    <!-- Information sur le rédacteur -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoConcernantCoresponsable');
            ?>
        </span>
        <div class="row row45"></div>
    </div>

    <!-- Champs du formulaire -->
    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('Coresponsable.coresponsables', [
                'id' => 'coresponsables',
                'label' => [
                    'text' => __d('fiche', 'fiche.champCoresponsables')
                ],
                'class' => 'form-control',
                'options' => Hash::combine($responsables, '{n}.Responsable.id', '{n}.Responsable.raisonsocialestructure'),
                'empty' => true,
                'multiple' => true,
                'required' => true,
                'data-placeholder' => __d('fiche', 'fiche.placeholderChampMultiCoresponsable'),
            ]);
            ?>
        </div>

        <?php
        if ($this->Autorisation->authorized('34', $this->Session->read('Droit.liste')) &&
            $this->request['action'] !== 'show'
        ) {
            echo $this->element('Fiches/modal/modalAddResponsableInTraitement');
            ?>
            <div class="pull-left">
                <a class="btn btn-outline-primary borderless" type="button" data-toggle="modal" data-target="#modalAddResponsableInFiche">
                    <i class="fa fa-plus fa-lg" aria-hidden="true"></i>
                </a>
            </div>
            <?php
        }
        ?>
    </div>

    <div id="coresponsableCards"></div>

    <?php
    if (!empty($coresponsables)) {
        foreach ($coresponsables as $key => $coresponsable) {
            echo $this->Form->hidden("Coresponsable.currentCoresponsable.{$coresponsable['Coresponsable']['responsable_id']}.responsable_id", [
                'value' => $coresponsable['Coresponsable']['responsable_id'],
                'id' => false
            ]);

            echo $this->Form->hidden("Coresponsable.currentCoresponsable.{$coresponsable['Coresponsable']['responsable_id']}.coresponsable_id", [
                'value' => $coresponsable['Coresponsable']['id'],
                'id' => false
            ]);
        }
    }
    ?>
</div>
<!-- Fin onglet Co-responsable -->

<?php
$coresponsablesRequestData = null;
if (isset($this->request->data['Coresponsable']['coresponsables'])) {
    $coresponsablesRequestData = $this->request->data['Coresponsable']['coresponsables'];
}

$coresponsabilitefieldsRequestData = null;
if (isset($this->request->data['WebdpoCoresponsable']) && !empty($this->request->data['WebdpoCoresponsable'])) {
    $coresponsabilitefieldsRequestData = $this->request->data['WebdpoCoresponsable'];
}

$validationErrors = null;
if (isset($this->validationErrors['WebdpoCoresponsable']) && !empty($this->validationErrors['WebdpoCoresponsable'])) {
    $validationErrors = $this->validationErrors['WebdpoCoresponsable'];
}
?>

<script type="text/javascript">

    $(document).ready(function () {
        let eventSelect = $("#coresponsables");
        $(eventSelect).select2({
            language: "fr",
            width: '100%',
        });

        let infoCoresponsables = <?php echo json_encode(Hash::combine($responsables, '{n}.Responsable.id', '{n}.Responsable'))?>;

        <?php
        if (isset($fields['coresponsable']) && !empty($fields['coresponsable'])) {
            $templateFieldsCoresponsable = $this->element('Fiches/tabs/formulaire', [
                'champs' => $fields['coresponsable']
            ]);
            $arrayFieldsId = [];
            foreach ($fields['coresponsable'] as $field) {
                $arrayFieldsId[] = json_decode($field['Champ']['details'], true)['name'];
            }
        } else {
            $arrayFieldsId = [];
            $templateFieldsCoresponsable = '';
        }
        ?>

        let templateFieldsCoresponsable = <?php echo json_encode($templateFieldsCoresponsable);?>;
        let coresponsablesRequestData = <?php echo json_encode($coresponsablesRequestData);?>;
        let coresponsabilitefieldsRequestData = <?php echo json_encode($coresponsabilitefieldsRequestData);?>;
        let validationErrors = <?php echo json_encode($validationErrors);?>;

        if (coresponsablesRequestData) {
            $(coresponsablesRequestData).each(function (key, val) {
                createCard(templateFieldsCoresponsable, infoCoresponsables, val, coresponsabilitefieldsRequestData);
            });

            // Mise en évidence des champs en erreur
            if (validationErrors !== null) {
                $.each(validationErrors, function (responsableId, fieldError) {
                    $.each(fieldError, function (field, errorMessage) {
                        $('#cr_' + responsableId + '_' + field).closest('div').after('<div class="error-message">'+errorMessage.toString()+'</div>');
                    });
                });
            }
        }

        // Quand on sélectionne dans le champ Co-responsable(s)
        $(eventSelect).on("select2:select", function (e) {
            let idSelect = e.params.data.id;

            $.ajax({
                url: '<?php echo Router::url(['controller' => 'fiches', 'action' => 'ajax_update_listing_responsable']);?>',
                method: 'GET',
                success: function(dataResponse) {
                    try {
                        let contents = JSON.parse(dataResponse);
                        let newInfoCoresponsables = {};

                        $.each(contents, function (k, responsable) {
                            $.each(responsable, function (k, value) {
                                newInfoCoresponsables[value['id']] = value;
                            });
                        });

                        createCard(templateFieldsCoresponsable, newInfoCoresponsables, idSelect);

                        $('.calendar').datetimepicker({
                            viewMode: 'year',
                            startView: 'decade',
                            format: 'dd/mm/yyyy',
                            minView: 2,
                            language: 'fr',
                            autoclose: true
                        });

                    } catch(e) {
                        console.error(e);
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        // Quand on déselectionne dans le champ Co-responsable(s)
        $(eventSelect).on("select2:unselect", function (e) {
            let id = e.params.data.id
            let idRemove = '#card_coresponsable_' + id;

            $('#coresponsableCards').find(idRemove).remove();

            $('input[type="hidden"][name="data[Coresponsable][currentCoresponsable]['+id+'][responsable_id]"]').remove();
            $('input[type="hidden"][name="data[Coresponsable][currentCoresponsable]['+id+'][coresponsable_id]"]').remove();
        });

    });

    function createCard(templateFieldsCoresponsable, infoCoresponsables, idSelect, coresponsabilitefieldsRequestData = null)
    {
        let coresponsable = infoCoresponsables[String(idSelect)];
        let colStructure = '';
        let colResponsable = '';
        let colDpo = '';
        let formCoreponsableFields = '';

        if (templateFieldsCoresponsable) {
            let fields = templateFieldsCoresponsable.replace(/data\[WebdpoFiche\]/g, 'data[WebdpoCoresponsable]['+idSelect+']');
            fields = fields.replace(/(for|id)="(<?php echo implode('|', $arrayFieldsId);?>)"/g, '$1="cr_'+idSelect+'_$2"');

            if (fields) {
                formCoreponsableFields = '<div id="form-container-coresponsable" class="row">';
                formCoreponsableFields = formCoreponsableFields + fields;
                formCoreponsableFields = formCoreponsableFields + '</div>';
            }
        }

        if (coresponsable['siretstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>N° SIRET :</u>' +
                ' ' + coresponsable['siretstructure'] +
                '</p>';
        }

        if (coresponsable['apestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Code APE :</u>' +
                ' ' + coresponsable['apestructure'] +
                '</p>';
        }

        if (coresponsable['adressestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Adresse :</u>' +
                ' ' + coresponsable['adressestructure'] +
                '</p>';
        }

        if (coresponsable['telephonestructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Téléphone :</u>' +
                ' ' + coresponsable['telephonestructure'] +
                '</p>';
        }

        if (coresponsable['faxstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>Fax :</u>' +
                ' ' + coresponsable['faxstructure'] +
                '</p>';
        }

        if (coresponsable['emailstructure'] != null) {
            colStructure = colStructure + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + coresponsable['emailstructure'] +
                '</p>';
        }

        if (coresponsable['nom_complet'] != null) {
            colResponsable = colResponsable + '<h5 class="card-title">' +
                coresponsable['nom_complet'] +
                '</h5>';
        }

        if (coresponsable['fonctionresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>Fonction :</u>' +
                ' ' + coresponsable['fonctionresponsable'] +
                '</p>';
        }

        if (coresponsable['telephoneresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>Téléphone :</u>' +
                ' ' + coresponsable['telephoneresponsable'] +
                '</p>';
        }

        if (coresponsable['emailresponsable'] != null) {
            colResponsable = colResponsable + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + coresponsable['emailresponsable'] +
                '</p>';
        }

        if (coresponsable['nom_complet_dpo'] != null) {
            colDpo = colDpo + '<h5 class="card-title">' +
                ' ' + coresponsable['nom_complet_dpo'] +
                '</h5>';
        }

        if (coresponsable['numerocnil_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Numéro d\'enregistrement CNIL :</u>' +
                ' ' + coresponsable['numerocnil_dpo'] +
                '</p>';
        }

        if (coresponsable['telephonefixe_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Téléphone fixe :</u>' +
                ' ' + coresponsable['telephonefixe_dpo'] +
                '</p>';
        }

        if (coresponsable['telephoneportable_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>Téléphone portable :</u>' +
                ' ' + coresponsable['telephoneportable_dpo'] +
                '</p>';
        }

        if (coresponsable['email_dpo'] != null) {
            colDpo = colDpo + '<p>' +
                '<u>E-mail :</u>' +
                ' ' + coresponsable['email_dpo'] +
                '</p>';
        }

        $('#coresponsableCards').append(
            '<div id="card_coresponsable_'+idSelect+'" class="card border-primary row">' +
                '<div class="card-header">' +
                    '<h4>' +
                    coresponsable['raisonsocialestructure'] +
                    '</h4>' +
                '</div>' +
                '<div class="card-body">' +
                    '<div class="row">' +
                        '<div class="col-md-4">' +
                            colStructure +
                        '</div>' +
                        '<div class="col-md-4">' +
                            colResponsable +
                        '</div>' +
                        '<div class="col-md-4">' +
                            colDpo +
                        '</div>' +
                    '</div>' +
                    formCoreponsableFields +
                '</div>' +
            '</div>'
        );

        // On attribut la valeur au champ si il y en a une.
        if (coresponsabilitefieldsRequestData !== null) {
            $.each(coresponsabilitefieldsRequestData[idSelect], function (key, val) {
                let fieldId = '#cr_' + idSelect + '_' + key;

                if (jQuery.isArray(val)) {
                    let ifMultiSelect = $(fieldId + '[name="data[WebdpoCoresponsable]['+idSelect+']['+key+'][]"]');

                    $.each(val, function (k, v) {
                        if (ifMultiSelect.length) {
                            $(fieldId + ' option[value="' + v + '"]').prop("selected", true);
                        } else {
                            $('#' + key + v + '[name="data[WebdpoCoresponsable]['+idSelect+']['+key+'][]"]').prop("checked", true);
                        }
                    });
                } else {
                    let ifRadio = $('input[type="radio"][name="data[WebdpoCoresponsable]['+idSelect+']['+key+']"][value="'+val+'"]');

                    if (ifRadio.length) {
                        $(ifRadio).prop("checked",true);
                    } else {
                        $(fieldId).val(val);
                    }
                }
            });
        }
    }
</script>
