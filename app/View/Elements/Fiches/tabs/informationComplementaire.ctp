<?php
echo $this->Html->script('Fiches/infoComplementaire');

$idSousFinalite = null;
$afterSousFinalite = '<a href="#" class="removeclass"><i class="fa fa-trash fa-lg fa-danger"></i></a>';
$viewBtn = true;

if ($this->request->params['action'] === 'show' ) {
    $afterSousFinalite = '';
    $viewBtn = false;
}

if (!isset($fieldsIsRequired)){
    $fieldsIsRequired = true;
}
?>

<!-- Onglet Information complementaire -->
<div id="information_complementaire" class="tab-pane">
    <br/>

    <?php
    if ($useFieldsFormulaire['Formulaire']['usesousfinalite'] === true ||
        $useFieldsFormulaire['Formulaire']['usebaselegale'] === true ||
        $useFieldsFormulaire['Formulaire']['usedecisionautomatisee'] === true
    ) {
        ?>
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoTraitementComplementaire');
                ?>
            </span>
            <br>
        </div>

        <?php
        if ($useFieldsFormulaire['Formulaire']['usesousfinalite'] === true) {
            ?>
            <div class="row">
                <div class="col-md-2"></div>

                <!-- Colonne de gauche -->
                <div class="col-md-8">
                    <div id="InputsWrapper">
                        <?php
                        if (isset($this->request->data['WebdpoFiche']['sousFinalite'])) {
                            foreach ($this->request->data['WebdpoFiche']['sousFinalite'] as $key => $sousFinalite) {
                                $idSousFinalite = $key + 1;
                                $label = 'Sous finalité ' . $idSousFinalite;

                                echo $this->WebcilForm->input('WebdpoFiche.sousFinalite_' . $idSousFinalite, [
                                    'id' => 'sousFinalite_' . $idSousFinalite,
                                    'value' => $sousFinalite,
                                    'name' => 'data[WebdpoFiche][sousFinalite][]',
                                    'label' => [
                                        'text' => $label
                                    ],
                                    'placeholder' => false,
                                    'after' => $afterSousFinalite
                                ]);
                            }
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-2"></div>

                <?php
                if ($viewBtn === true) {
                    ?>
                    <div id="AddMoreFileId">
                        <a id="AddMoreFileBox" class="btn btn-outline-primary">
                            <i class="fa fa-plus"></i>
                            Ajouter une sous-finalité
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <hr>
            <?php
        }

        if ($useFieldsFormulaire['Formulaire']['usebaselegale'] === true) {
            ?>
            <div class="row">
                <!-- Colonne de gauche -->
                <?php
                echo $this->WebcilForm->input('WebdpoFiche.baselegale', [
                    'id' => 'baselegale',
                    'label' => [
                        'text' => $this->Html->tag(
                            'abbr',
                            __d('fiche', 'fiche.champBaselegale'),
                            [
                                'data-content' => nl2br(__d('fiche', 'fiche.champBaselegaleDefinition')),
                                'class' => 'popoverText'
                            ]
                        )
                    ],
                    'multiple' => 'checkbox',
                    'class' => 'form-check',
                    'options' => Fiche::LISTE_BASE_LEGALE,
                ]);
                ?>
            </div>
            <hr>
            <?php
        }

        if ($useFieldsFormulaire['Formulaire']['usedecisionautomatisee'] === true) {
            ?>
            <div class="row">
                <!-- Colonne de gauche -->
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('WebdpoFiche.decisionAutomatisee', [
                        'id' => 'decisionAutomatisee',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDecisionAutomatisee')
                        ],
                        'options' => [
                            'Oui' => 'Oui',
                            'Non' => 'Non'
                        ],
                        'class' => 'form-control custom-select',
                        'required' => true,
                        'default' => 'Non',
                        'placeholder' => false,
                        'data-placeholder' => ' '
                    ]);
                    ?>
                </div>

                <!-- Colonne de droite -->
                <div class="col-md-6">
                    <?php
                    echo $this->WebcilForm->input('WebdpoFiche.descriptionDecisionAutomatisee', [
                        'id' => 'descriptionDecisionAutomatisee',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champDescriptionDecisionAutomatisee')
                        ],
                        'type' => 'textarea',
                        'required' => true,
                        'placeholder' => false
                    ]);
                    ?>
                </div>
            </div>
            <hr>
            <?php
        }
    }

    $i = null;
    if ($useFieldsFormulaire['Formulaire']['usetransferthorsue'] === true) {
        ?>
        <!-- Information concernant le transfert hors UE -->
        <div id="infoSupHorsUE">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoTransfereHorsUEComplementaire');
                    ?>
                </span>
                <br>
            </div>

            <?php
            for ($i = 1; $i <= max(count((array)Hash::get($this->data, 'WebdpoFiche.horsue')), 1); $i++) {
                if ($i !== 1) {
                    echo '<hr>';
                }
                echo '<div class="row" id="HorsUe_' . $i . '">';
                echo '<div class="col-md-6">';
                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$i}.organismeDestinataireHorsUe",
                        'type' => 'text',
                        'label' => __d('fiche', 'fiche.champOrganismeDestinataireHorsUeNombre') . $i,
                        'required' => $fieldsIsRequired,
                    ]
                );

                $trash = false;
                $classTrash = '';
                if ($i > 1 && $viewBtn === true) {
                    $trash = true;
                    $classTrash = 'removeHorsUE';
                }

                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$i}.typeGarantieHorsUe",
                        'type' => 'select',
                        'options' => Fiche::TYPE_GARANTIE_HORS_UE,
                        'label' => __d('fiche', 'fiche.champTypeGarantieHorsUeNombre') . $i,
                        'trash' => $trash,
                        'classTrash' => $classTrash,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo '</div>';

                echo '<div class="col-md-6">';
                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$i}.paysDestinataireHorsUe",
                        'type' => 'select',
                        'options' => Fiche::LISTE_PAYS_HORS_UE,
                        'label' => __d('fiche', 'fiche.champPaysDestinataireHorsUeNombre') . $i,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo '</div>';
                echo '</div>';
            }
            ?>

            <div id="InputsHorsUE"></div>

            <?php
            if ($viewBtn === true) {
                ?>
                <div id="AddOrganismeHorsUE">
                    <a id="AddOrganismeHorsUEFileBox" class="btn btn-outline-primary"> <!--href="#"-->
                        <i class="fa fa-plus"></i>
                        Ajouter un organisme hors de l'UE
                    </a>
                    <br><br>
                </div>
                <?php
            }
            ?>
            <hr>
        </div>
        <?php
    }

    $nb = null;
    if ($useFieldsFormulaire['Formulaire']['usedonneessensible'] === true) {
        ?>
        <!-- Information concernant les données sensibles -->
        <div id="infoSupDonneesSensibles">
            <div class="col-md-12">
                <span class='labelFormulaire'>
                    <?php
                    echo __d('fiche', 'fiche.textInfoDonneesSensiblesComplementaire');
                    ?>
                </span>
                <br>
            </div>

            <?php
            for ($nb = 1; $nb <= max(count((array)Hash::get($this->data, 'WebdpoFiche.donneessensibles')), 1); $nb++) {
                if ($nb !== 1) {
                    echo '<hr>';
                }
                echo '<div class="row" id="DonneeSensible_' . $nb . '">';
                echo '<div class="col-md-6">';
                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.donneessensibles.{$nb}.typeDonneeSensible",
                        'type' => 'select',
                        'label' => __d('fiche', 'fiche.champTypeDonneeSensibleNombre') . $nb,
                        'options' => Fiche::LISTE_DONNEES_SENSIBLES,
                        'required' => $fieldsIsRequired,
                    ]
                );

                $trash = false;
                $classTrash = '';
                if ($nb > 1 && $viewBtn === true) {
                    $trash = true;
                    $classTrash = 'removeDonneeSensible';
                }

                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.donneessensibles.{$nb}.descriptionDonneeSensible",
                        'type' => 'textarea',
                        'label' => __d('fiche', 'fiche.champDescriptionDonneeSensibleNombre') . $nb,
                        'trash' => $trash,
                        'classTrash' => $classTrash,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo '</div>';

                echo '<div class="col-md-6">';
                echo $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.donneessensibles.{$nb}.dureeConservationDonneeSensible",
                        'type' => 'textarea',
                        'label' => __d('fiche', 'fiche.champDureeConservationDonneeSensibleNombre') . $nb,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo '</div>';
                echo '</div>';
            }
            ?>

            <div id="InputsDonneesSensibles"></div>

            <?php
            if ($viewBtn === true) {
                ?>
                <div id="AddDonneesSensibles">
                    <a id="AddDonneesSensiblesFileBox" class="btn btn-outline-primary"> <!--href="#"-->
                        <i class="fa fa-plus"></i>
                        Ajouter données sensibles
                    </a>
                    <br><br>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        // Partie concernant la sous-finalité
        let useSousFinalite = <?php echo json_encode($useFieldsFormulaire['Formulaire']['usesousfinalite']);?>;
        if (useSousFinalite == true) {
            let actionForm = <?php echo json_encode($this->request->params['action']);?>;
            let idField = 1; // to keep track of text box added

            if (actionForm === 'edit') {
                idField = <?php echo json_encode($idSousFinalite); ?>;
                idField++;
            }

            addSousFinalite(idField);
        }

        // Partie concernant les champs sur la decision automatisee
        let useDecisionAutomatisee = <?php echo json_encode($useFieldsFormulaire['Formulaire']['usedecisionautomatisee']);?>;
        if (useDecisionAutomatisee == true) {
           displayDescriptionDecisionAutomatisee($('#decisionAutomatisee').val());
           addDecisionAutomatisee();
        }

        // Partie concernant le transfere hors ue
        let useHorsUE = <?php echo json_encode($useFieldsFormulaire['Formulaire']['usetransferthorsue']);?>;
        if (useHorsUE == true) {
            displayInfoSupHorsUE($('#transfert_hors_ue').val());

            let idHorsUE = <?php echo json_encode($i);?>;
            addHorsUE(idHorsUE);
        }

        // Partie concernant les données sensibles
        let useDonneesSensibles = <?php echo json_encode($useFieldsFormulaire['Formulaire']['usedonneessensible']);?>;
        if (useDonneesSensibles == true) {
            displayInfoSupDonneesSensibles($('#donnees_sensibles').val());

            let idDonneesSensibles = <?php echo json_encode($nb);?>;
            addDonneeSensible(idDonneesSensibles);
        }

    });

    function createInputsInfoSupHorsUe(id)
    {
        $('#InputsHorsUE').append('<hr><div class="row" id="HorsUe_'+id+'">' +
            '<div class="col-md-6">' +
                <?php
                $idElement = 9999;
                $element = $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$idElement}.organismeDestinataireHorsUe",
                        'type' => 'text',
                        'label' => __d('fiche', 'fiche.champOrganismeDestinataireHorsUeNombre') . $idElement,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo json_encode($element);
                ?>.replace(/<?php echo $idElement;?>/g, id) +

                <?php
                $idElement = 9999;
                $element = $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$idElement}.typeGarantieHorsUe",
                        'type' => 'select',
                        'options' => Fiche::TYPE_GARANTIE_HORS_UE,
                        'label' => __d('fiche', 'fiche.champTypeGarantieHorsUeNombre') . $idElement,
                        'trash' => true,
                        'classTrash' => 'removeHorsUE',
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo json_encode($element);
                ?>.replace(/<?php echo $idElement;?>/g, id) +
            '</div>' +
            '<div class="col-md-6">' +
                <?php
                $idElement = 9999;
                $element = $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.horsue.{$idElement}.paysDestinataireHorsUe",
                        'type' => 'select',
                        'options' => Fiche::LISTE_PAYS_HORS_UE,
                        'label' => __d('fiche', 'fiche.champPaysDestinataireHorsUeNombre') . $idElement,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo json_encode($element);
                ?>.replace(/<?php echo $idElement;?>/g, id) +
            '</div>' +
        '</div>');
    }

    function createInputsInfoSupDonneeSensible(idDonneesSensibles)
    {
        $('#InputsDonneesSensibles').append('<hr><div class="row" id="DonneeSensible_'+idDonneesSensibles+'">' +
            '<div class="col-md-6">' +
                <?php
                $idElement = 9999;
                $element = $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.donneessensibles.{$idElement}.typeDonneeSensible",
                        'type' => 'select',
                        'label' => __d('fiche', 'fiche.champTypeDonneeSensibleNombre') . $idElement,
                        'options' => Fiche::LISTE_DONNEES_SENSIBLES,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo json_encode($element);
                ?>.replace(/<?php echo $idElement;?>/g, idDonneesSensibles) +
                    <?php
                    $idElement = 9999;
                    $element = $this->element(
                        'Fiches/dynamicField',
                        [
                            'path' => "WebdpoFiche.donneessensibles.{$idElement}.descriptionDonneeSensible",
                            'type' => 'textarea',
                            'label' => __d('fiche', 'fiche.champDescriptionDonneeSensibleNombre') . $idElement,
                            'trash' => true,
                            'classTrash' => 'removeDonneeSensible',
                            'required' => $fieldsIsRequired,
                        ]
                    );
                    echo json_encode($element);
                    ?>.replace(/<?php echo $idElement;?>/g, idDonneesSensibles) +
            '</div>' +
            '<div class="col-md-6">' +
                <?php
                $idElement = 9999;
                $element = $this->element(
                    'Fiches/dynamicField',
                    [
                        'path' => "WebdpoFiche.donneessensibles.{$idElement}.dureeConservationDonneeSensible",
                        'type' => 'textarea',
                        'label' => __d('fiche', 'fiche.champDureeConservationDonneeSensibleNombre') . $idElement,
                        'required' => $fieldsIsRequired,
                    ]
                );
                echo json_encode($element);
                ?>.replace(/<?php echo $idElement;?>/g, idDonneesSensibles) +
            '</div>' +
        '</div>');
    }

</script>
