<?php
//echo $this->Html->script([
//    'jquery-mask-plugin/dist/jquery.mask.min.js'
//]);
?>

<!-- Onglet Information sur l'entité -->
<div id="onglet_st_organisation" class="tab-pane">
    <br/>
    <!--Information sur l'entité -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation','organisation.textEntite');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_raisonsociale' => [
                    'id' => 'st_organisation_raisonsociale',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationRaisonsociale')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_telephone' => [
                    'id' => 'st_organisation_telephone',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationTelephone')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_fax' => [
                    'id' => 'st_organisation_fax',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationFax')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_adresse' => [
                    'id' => 'st_organisation_adresse',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationAdresse')
                    ],
                    'type' => 'textarea',
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_email' => [
                    'id' => 'st_organisation_email',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationEmail')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_sigle' => [
                    'id' => 'st_organisation_sigle',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationSigle')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_siret' => [
                    'id' => 'st_organisation_siret',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationSiret')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_ape' => [
                    'id' => 'st_organisation_ape',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationApe')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information sur le responsable de l'entitée -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('organisation', 'organisation.titreResponsableEntitee');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_civiliteresponsable' => [
                    'id' => 'st_organisation_civiliteresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationCiviliteresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_prenomresponsable' => [
                    'id' => 'st_organisation_prenomresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationPrenomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_nomresponsable' => [
                    'id' => 'st_organisation_nomresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationNomresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_fonctionresponsable' => [
                    'id' => 'st_organisation_fonctionresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationFonctionresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_emailresponsable' => [
                    'id' => 'st_organisation_emailresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationEmailresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_telephoneresponsable' => [
                    'id' => 'st_organisation_telephoneresponsable',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationTelephoneresponsable')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Information sur le DPO -->
        <div class="col-md-8">
            <!-- Affichage du logo du DPO -->
            <?php
            if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
                echo $this->Html->image('logo_dpo.svg', [
                    'class' => 'logo-well'
                ]);
            }
            ?>
        </div>

        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_nom_complet_dpo' => [
                    'id' => 'st_organisation_nom_complet_dpo',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationNomCompletDpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_numerodpo' => [
                    'id' => 'st_organisation_numerodpo',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationNumerodpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_telephonefixe_dpo' => [
                    'id' => 'st_organisation_telephonefixe_dpo',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationTelephonefixeDpo')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'Trash.st_organisation_emaildpo' => [
                    'id' => 'st_organisation_emaildpo',
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationEmailDpo')
                    ],
                    'readonly' => true,
                    'required' => true,
                    'placeholder' => false
                ],
                'Trash.st_organisation_telephoneportable_dpo' => [
                    'id' => 'st_organisation_telephoneportable_dpo',
                    'readonly' => true,
                    'placeholder' => false,
                    'label' => [
                        'text' => __d('st_organisation', 'st_organisation.champStOrganisationTelephoneportableDpo')
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<!--Fin onglet Information sur l'entité-->

<script type="text/javascript">

    $(document).ready(function () {

        // Mask champs
        // L'entité
        $('#st_organisation_telephone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#st_organisation_fax').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#st_organisation_siret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

        // Responsable de l'entité
        $('#st_organisation_telephoneresponsable').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        // DPO
        $('#st_organisation_fixdpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#st_organisation_portabledpo').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

    });

</script>
