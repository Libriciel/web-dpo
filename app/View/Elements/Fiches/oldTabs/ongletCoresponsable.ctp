<?php
if (!empty($responsables)) {
    ?>
    <!-- Onglet Co-responsable -->
    <div id="ongletCoresponsable" class="tab-pane">
        <br/>

        <!-- Information sur le rédacteur -->
        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('fiche', 'fiche.textInfoConcernantCoresponsable');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <!-- Champs du formulaire -->
        <div class="row">
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('coresponsableid', [
                    'id' => 'coresponsableid',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champCoresponsableid')
                    ],
                    'class' => 'form-control usersDeroulant',
                    'options' => Hash::combine($responsables, '{n}.Responsable.id', '{n}.Responsable.raisonsocialestructure'),
                    'empty' => true,
                    'multiple' => false,
                    'required' => true,
//                    'value' => $coresponsableid,
                    'data-placeholder' => __d('fiche', 'fiche.placeholderChampMultiCoresponsable')
                ]);
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('responsable', 'responsable.titreCoresponsable');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'WebdpoFiche.nomcoresponsable' => [
                        'id' => 'nomcoresponsable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champNomcoresponsable')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.prenomcoresponsable' => [
                        'id' => 'prenomcoresponsable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champPrenomcoresponsable')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.fonctioncoresponsable' => [
                        'id' => 'fonctioncoresponsable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champFonctioncoresponsable')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ]
                ]);
                ?>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'WebdpoFiche.emailcoresponsable' => [
                        'id' => 'emailcoresponsable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champEmailcoresponsable')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.telephonecoresponsable' => [
                        'id' => 'telephonecoresponsable',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champTelephonecoresponsable')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <span class='labelFormulaire'>
                <?php
                echo __d('responsable', 'responsable.titreStructureCoresponsable');
                ?>
            </span>
            <div class="row row35"></div>
        </div>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'WebdpoFiche.raisonsocialestructure' => [
                        'id' => 'raisonsocialestructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champRaisonsocialestructure')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.siretstructure' => [
                        'id' => 'siretstructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champSiretstructure')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.apestructure' => [
                        'id' => 'apestructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champApestructure')
                        ],
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => false
                    ]
                ]);
                ?>
            </div>

            <!-- Colonne de droite -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->inputs([
                    'WebdpoFiche.telephonestructure' => [
                        'id' => 'telephonestructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champTelephonestructure')
                        ],
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.faxstructure' => [
                        'id' => 'faxstructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champFaxstructure')
                        ],
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.adressestructure' => [
                        'id' => 'adressestructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champAdressestructure')
                        ],
                        'type' => 'textarea',
                        'readonly' => true,
                        'placeholder' => false
                    ],
                    'WebdpoFiche.emailstructure' => [
                        'id' => 'emailstructure',
                        'label' => [
                            'text' => __d('fiche', 'fiche.champEmailstructure')
                        ],
                        'readonly' => true,
                        'placeholder' => false
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <!-- Fin onglet Co-responsable -->
    <?php
}
?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#telephonecoresponsable').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        $('#telephonestructure').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#faxstructure').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        $('#siretstructure').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

        var coresponsable = $('#coresponsable').val();
        displayOngletCoresponsable(coresponsable);

        $('#coresponsable').change(function () {
            var idCoresponsable = $(this).val();
            displayOngletCoresponsable(idCoresponsable);
        });

        var infoCoresponsable = <?php echo json_encode(Hash::combine($responsables, '{n}.Responsable.id', '{n}.Responsable'))?>;
        $('#coresponsableid').change(function () {
            var idcoresponsable = $(this).val();

            if (idcoresponsable === '') {
                str = null;

                $("#nomcoresponsable").val(str);
                $("#prenomcoresponsable").val(str);
                $("#emailcoresponsable").val(str);
                $("#telephonecoresponsable").val(str);
                $("#fonctioncoresponsable").val(str);
                $("#raisonsocialestructure").val(str);
                $("#siretstructure").val(str);
                $("#apestructure").val(str);
                $("#telephonestructure").val(str);
                $("#faxstructure").val(str);
                $("#adressestructure").val(str);
                $("#emailstructure").val(str);
            } else {
                $("#nomcoresponsable").val(infoCoresponsable[idcoresponsable]['nomresponsable']);
                $("#prenomcoresponsable").val(infoCoresponsable[idcoresponsable]['prenomresponsable']);
                $("#emailcoresponsable").val(infoCoresponsable[idcoresponsable]['emailresponsable']);
                $("#telephonecoresponsable").val(infoCoresponsable[idcoresponsable]['telephoneresponsable']);
                $("#fonctioncoresponsable").val(infoCoresponsable[idcoresponsable]['fonctionresponsable']);
                $("#raisonsocialestructure").val(infoCoresponsable[idcoresponsable]['raisonsocialestructure']);
                $("#siretstructure").val(infoCoresponsable[idcoresponsable]['siretstructure']);
                $("#apestructure").val(infoCoresponsable[idcoresponsable]['apestructure']);
                $("#telephonestructure").val(infoCoresponsable[idcoresponsable]['telephonestructure']);
                $("#faxstructure").val(infoCoresponsable[idcoresponsable]['faxstructure']);
                $("#adressestructure").val(infoCoresponsable[idcoresponsable]['adressestructure']);
                $("#emailstructure").val(infoCoresponsable[idcoresponsable]['emailstructure']);
            }
        });

    });

    function displayOngletCoresponsable(val){
        if (val == true) {
            $('#liOngletCoresponsable').removeClass('disabled');

            $('#aOngletCoresponsable').attr('role', 'tab');
            $('#aOngletCoresponsable').attr('data-toggle', 'tab');

        } else {
            $('#liOngletCoresponsable').addClass('disabled');

            $('#aOngletCoresponsable').attr('role', '');
            $('#aOngletCoresponsable').attr('data-toggle', '');
        }
    }

</script>