<!-- Onglet sous-traitant -->
<div id="ongletSoustraitance" class="tab-pane">
    <br/>
    <!-- Information sur le rédacteur -->
    <div class="col-md-12">
        <span class='labelFormulaire'>
            <?php
            echo __d('fiche', 'fiche.textInfoSoustraitant');
            ?>
        </span>
        <div class="row row35"></div>
    </div>

    <!-- Champs du formulaire -->
    <div class="row">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('soustraitantid', [
                'id' => 'soustraitantid',
                'label' => [
                    'text' => __d('fiche', 'fiche.champSoustraitantid')
                ],
                'class' => 'form-control usersDeroulant',
                'options' => Hash::combine($soustraitants, '{n}.Soustraitant.id', '{n}.Soustraitant.raisonsociale'),
                'empty' => true,
                'multiple' => false,
                'required' => true,
                'placeholder' => false,
                'data-placeholder' => 'Choisir un sous-traitant'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'WebdpoFiche.soustraitantraisonsociale' => [
                    'id' => 'soustraitantraisonsociale',
                    'type' => 'hidden'
                ],
                'WebdpoFiche.soustraitantsiret' => [
                    'id' => 'soustraitantsiret',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitantsiret')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitantape' => [
                    'id' => 'soustraitantape',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitantape')
                    ],
                    'required' => true,
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitanttelephone' => [
                    'id' => 'soustraitanttelephone',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitanttelephone')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitantfax' => [
                    'id' => 'soustraitantfax',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitantfax')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitantadresse' => [
                    'id' => 'soustraitantadresse',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitantadresse')
                    ],
                    'type' => 'textarea',
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitantemail' => [
                    'id' => 'soustraitantemail',
                    'label' => [
                        'text' => __d('fiche', 'fiche.champSoustraitantemail')
                    ],
                    'readonly' => true,
                    'placeholder' => false
                ],
                'WebdpoFiche.soustraitant' => [
                    'id' => 'soustraitant',
                    'type' => 'hidden',
                    'value' => $soustraitance['Formulaire']['soustraitant']
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<!-- Fin onglet sous-traitant -->

<script type="text/javascript">

    $(document).ready(function () {

        // $('#soustraitantsiret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});
        // $('#soustraitanttelephone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});
        // $('#soustraitantfax').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

        var infoSoustraitant = <?php echo json_encode(Hash::combine($soustraitants, '{n}.Soustraitant.id', '{n}.Soustraitant')); ?>;
        $('#soustraitantid').change(function () {
            var idsoutraitant = $(this).val();

            if (idsoutraitant === '') {
                var str = null;

                $('#soustraitantsiret').val(str);
                $('#soustraitantape').val(str);
                $('#soustraitanttelephone').val(str);
                $('#soustraitantfax').val(str);
                $('#soustraitantadresse').val(str);
                $('#soustraitantemail').val(str);
            } else {
                $('#soustraitantraisonsociale').val(infoSoustraitant[idsoutraitant]['raisonsociale']);
                $('#soustraitantsiret').val(infoSoustraitant[idsoutraitant]['siret']);
                $('#soustraitantape').val(infoSoustraitant[idsoutraitant]['ape']);
                $('#soustraitanttelephone').val(infoSoustraitant[idsoutraitant]['telephone']);
                $('#soustraitantfax').val(infoSoustraitant[idsoutraitant]['fax']);
                $('#soustraitantadresse').val(infoSoustraitant[idsoutraitant]['adresse']);
                $('#soustraitantemail').val(infoSoustraitant[idsoutraitant]['email']);
            }
        });

    });

</script>