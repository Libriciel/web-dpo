<?php
if (isset($this->validationErrors['WebdpoFiche']) && !empty($this->validationErrors['WebdpoFiche']) ||
    isset($this->validationErrors['Fiche']) && !empty($this->validationErrors['Fiche']) ||
    isset($this->validationErrors['Fichier']) && !empty($this->validationErrors['Fichier']) ||
    isset($this->validationErrors['WebdpoCoresponsable']) && !empty($this->validationErrors['WebdpoCoresponsable']) ||
    isset($this->validationErrors['WebdpoSoustraitance']) && !empty($this->validationErrors['WebdpoSoustraitance'])
) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="fa fa-exclamation-circle fa-fw" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites sur les champs suivants :
        <ul>
            <?php
            if (isset($this->validationErrors['WebdpoFiche']) && !empty($this->validationErrors['WebdpoFiche'])) {
                $champ = null;
                foreach ($this->validationErrors['WebdpoFiche'] as $key => $errorChamps) {
                    foreach ($fields['formulaire'] as $tab => $value) {
                        if (is_array($value)) {
                            foreach ($value as $val) {
                                $options = json_decode($val['details'], true);
                            }
                        } else {
                            $options = json_decode($value['details'], true);
                        }

                        if (isset($options['name']) === true && $key == $options['name']) {
                            $champ = $options['label'];
                        }
                    }

                    foreach ($errorChamps as $error) {
                        if ($champ === null) {
                            $champ = __d('fiche', 'fiche.champ' . ucfirst($key));
                        }

                        echo '<li>' . $champ . /*' : ' . $error .*/
                            '</li>';
                    }

                    $champ = null;
                }
            }

            if (isset($this->validationErrors['Fiche']) && !empty($this->validationErrors['Fiche'])) {
                $champ = null;
                foreach ($this->validationErrors['Fiche'] as $key => $errorChamps) {
                    foreach ($errorChamps as $error) {
                        if ($champ === null) {
                            $champ = __d('fiche', 'fiche.champ' . ucfirst($key));
                        }

                        echo '<li>' . $champ . /*' : ' . $error .*/
                            '</li>';
                    }

                    $champ = null;
                }
            }

            if (isset($this->validationErrors['Fichier']) && !empty($this->validationErrors['Fichier'])) {
                $validationErrorsFichier = $this->validationErrors['Fichier'];
                $champ = null;
                foreach ($this->validationErrors['Fichier'] as $key => $errorChamps) {
                    foreach ($errorChamps as $error) {
                        echo '<li>' . $error . /*' : ' . $error .*/
                            '</li>';
                    }

                    $champ = null;
                }
            }

            if (isset($this->validationErrors['WebdpoCoresponsable']) && !empty($this->validationErrors['WebdpoCoresponsable'])) {
                $champ = null;
                foreach ($this->validationErrors['WebdpoCoresponsable'] as $coresponsable_id => $errorChamps) {
                    foreach ($errorChamps as $fieldError => $messageErrorChamp) {
                        foreach ($fields['coresponsable'] as $value) {
                            if (is_array($value)) {
                                foreach ($value as $val) {
                                    $options = json_decode($val['details'], true);
                                }
                            } else {
                                $options = json_decode($value['details'], true);
                            }

                            if (isset($options['name']) === true && $fieldError == $options['name']) {
                                $champ = $options['label'];
                            }
                        }

                        foreach ($messageErrorChamp as $error) {
                            if ($champ === null) {
                                $champ = __d('fiche', 'fiche.champ'.ucfirst($key));
                            }

                            echo '<li>' . $champ . /*' : ' . $error .*/ '</li>';
                        }

                        $champ = null;
                    }
                }
            }

            if (isset($this->validationErrors['WebdpoSoustraitance']) && !empty($this->validationErrors['WebdpoSoustraitance'])) {
                $champ = null;
                foreach ($this->validationErrors['WebdpoSoustraitance'] as $coresponsable_id => $errorChamps) {
                    foreach ($errorChamps as $fieldError => $messageErrorChamp) {
                        foreach ($fields['soustraitant'] as $value) {
                            if (is_array($value)) {
                                foreach ($value as $val) {
                                    $options = json_decode($val['details'], true);
                                }
                            } else {
                                $options = json_decode($value['details'], true);
                            }

                            if (isset($options['name']) === true && $fieldError == $options['name']) {
                                $champ = $options['label'];
                            }
                        }

                        foreach ($messageErrorChamp as $error) {
                            if ($champ === null) {
                                $champ = __d('fiche', 'fiche.champ' . ucfirst($key));
                            }

                            echo '<li>' . $champ . /*' : ' . $error .*/
                                '</li>';
                        }

                        $champ = null;
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}
