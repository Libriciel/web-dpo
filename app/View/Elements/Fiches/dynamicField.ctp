<?php
$required = isset($required) === true ? $required : false; // @fixme: ne change rien pour l'instant
$label = isset($label) === true ? $label : null;
$trash = isset($trash) === true ? $trash : false;
$classTrash = isset($classTrash) === true ? $classTrash : '';
$options = isset($options) === true ? $options : [];
$id = $this->Html->domId($path);

if (!empty($label) && $required === true) {
    $label = $label . ' <span class="requis">*</span>';
}

echo '<div class="form-group">';

    echo $this->WebcilForm->label($label, null, ['class' => 'control-label', 'for' => $id]);

    $attributes = [
        'id' => $id,
        'required' => $required,
        'placeholder' => false
    ];

    if ($type === 'select') {
        $attributes += [
            'class' => 'multi-select2 transformSelect form-control',
            'empty' => true,
            'placeholder' => false,
            'data-placeholder' => ' '
        ];

        $field = $this->WebcilForm->{$type}($path, $options, $attributes);
    } else {
        $attributes += [
            'class' => 'form-control',
        ];

        $field = $this->WebcilForm->{$type}($path, $attributes);
    }

    echo $this->Html->tag('', $field, []);

    $errors = Hash::get($this->validationErrors, $path);
    if (empty($errors) === false) {
        if (count($errors) === 1) {
            $errors = $errors[0];
        } else {
            $errors = '<ul><li>'.implode('</li><li>', $errors).'</li></ul>';
        }
        echo $this->Html->tag('div', $errors, ['class' => 'invalid-feedback']);
    }

echo '</div>';

if ($trash === true) {
    echo '<a href="#" class="'.$classTrash.'">';
    echo $this->Html->tag('span', '', ['class' => 'fa fa-trash fa-lg fa-danger']);
    echo '</a>';
}
