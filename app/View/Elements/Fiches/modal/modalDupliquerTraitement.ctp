<?php
$body = $this->WebcilForm->inputs([
    'Fiche.id' => [
        'id' => "FicheId",
        'value' => null
    ],
    'Fiche.organisationcible' => [
        'id' => 'organisationcible',
        'options' => $mesOrganisations,
        'required' => true,
        'empty' => true,
        'placeholder' => false,
        'data-placeholder' => ' '
    ],
    'Fiche.usercible' => [
        'id' => 'usercible',
        'options' => [],
        'required' => true,
        'empty' => true,
        'placeholder' => false,
        'data-placeholder' => ' '
    ],
    'Fiche.userservicecible' => [
        'id' => 'userservicecible',
        'options' => [],
        'required' => true,
        'empty' => true,
        'placeholder' => false,
        'data-placeholder' => ' '
    ]
]);

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
        'id' => 'btnSave',
        'type' => 'submit',
        'class' => 'btn btn-primary',
        'escape' => false,
    ])
.'</div>';

$content = [
    'title' => __d('fiche', 'fiche.popupDupliquerFicheInEntite'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalDupliquerTraitement',
    'controller' => 'Fiche',
    'action' => 'dupliquerTraitement',
    'content' => $content,
]);
?>

<script type='text/javascript'>
    $(document).ready(function () {

        hideAndRemoveField();
        hideBtnSave();

        $('.btn_duplicate').click(function () {
            $('#FicheId').val($(this).attr('value'));
        });

        // Lors d'action sur une menu déroulant : organisationcible
        $("#organisationcible").change(function () {
            let organisationcible = $(this).val();

            hideAndRemoveField();

            if (organisationcible) {
                updateListingUser(organisationcible);
            } else {
                hideBtnSave();
            }
        });

        $("#usercible").change(function () {
            let usercible = $(this).val();
            let organisationcible = $('#organisationcible').val();

            $('#userservicecible option').remove();

            if (usercible && organisationcible) {
                updateListingServices(
                    usercible,
                    organisationcible
                );
            } else {
                hideBtnSave();
            }
        });

        $("#userservicecible").change(function () {
            if ($(this).val()) {
                $('#btnSave').removeAttr('disabled');
            } else {
                $('#btnSave').attr('disabled', true);
            }
        });

    });

    function hideAndRemoveField()
    {
        $('#usercible option').remove();
        $('#usercible').parent().hide();

        $('#userservicecible option').remove();
        $('#userservicecible').parent().hide();
    }

    function hideBtnSave()
    {
        $('#btnSave').attr('disabled', true);
    }

    function updateListingUser(organisationcible)
    {
        $.ajax({
            url: '<?php echo Router::url(['controller' => 'pannel', 'action' => 'ajax_listing_users_organisation']);?>',
            method: 'POST',
            data: {'organisation_id': organisationcible},
            success: function(dataResponse) {
                try {
                    let organisations = JSON.parse(dataResponse);

                    let newOption = new Option('', '', false, false);
                    $('#usercible').append(newOption).trigger('change');

                    $.each(organisations, function (key, value) {
                        newOption = new Option(value, key, false, false);
                        $('#usercible').append(newOption).trigger('change');
                    });

                    $('#usercible').parent().show();

                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }

    function updateListingServices(usercible, organisationcible)
    {
        $.ajax({
            url: '<?php echo Router::url(['controller' => 'pannel', 'action' => 'ajax_listing_services_user']);?>',
            method: 'POST',
            data: {
                'user_id': usercible,
                'organisation_id': organisationcible,
            },
            success: function(dataResponse) {
                try {
                    let services = JSON.parse(dataResponse);

                    let newOption = new Option('Aucun service', '', false, false);
                    $('#userservicecible').append(newOption).trigger('change');

                    if (services.length !== 0) {
                        $.each(services, function (key, value) {
                            newOption = new Option(value, key, false, false);
                            $('#userservicecible').append(newOption).trigger('change');
                        });
                    } else {
                        $('#btnSave').removeAttr('disabled');
                    }

                    $('#userservicecible').parent().show();
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }

</script>
