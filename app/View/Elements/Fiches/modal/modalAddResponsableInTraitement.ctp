<?php
$body = $this->WebcilForm->create('newResponsable', [
        'id' => 'newResponsable'
    ])
    . $this->element('Responsables_Soustraitants/responsable_ajax_add')
;

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        .'<button id="saveNewResponsable" type="button" class="btn btn-primary">'
            .'<i class="far fa-save fa-lg"></i>'
            . __d('default', 'default.btnSave')
        .'</button>'
    .'</div>'
    . $this->WebcilForm->end()
;

$content = [
    'title' => __d('fiche', 'fiche.popupAddResponsableInFiche'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddResponsableInFiche',
    'content' => $content,
    'sizeModal' => 'modal-xl modal-dialog-scrollable'
]);
?>

<script type="text/javascript">
    $(document).ready(function () {

        $('#saveNewResponsable').click(function () {
            let dataResponsable = {
                raisonsocialestructure: $('#modalAddResponsableInFiche #raisonsocialestructure').val(),
                telephonestructure: $('#modalAddResponsableInFiche #telephonestructure').val(),
                faxstructure: $('#modalAddResponsableInFiche #faxstructure').val(),
                adressestructure: $('#modalAddResponsableInFiche #adressestructure').val(),
                emailstructure: $('#modalAddResponsableInFiche #emailstructure').val(),
                siretstructure: $('#modalAddResponsableInFiche #siretstructure').val(),
                apestructure: $('#modalAddResponsableInFiche #apestructure').val(),
                civiliteresponsable: $('#modalAddResponsableInFiche #civiliteresponsable').val(),
                prenomresponsable: $('#modalAddResponsableInFiche #prenomresponsable').val(),
                nomresponsable: $('#modalAddResponsableInFiche #nomresponsable').val(),
                fonctionresponsable: $('#modalAddResponsableInFiche #fonctionresponsable').val(),
                emailresponsable: $('#modalAddResponsableInFiche #emailresponsable').val(),
                telephoneresponsable: $('#modalAddResponsableInFiche #telephoneresponsable').val(),
                civility_dpo: $('#modalAddResponsableInFiche #civility_dpo').val(),
                prenom_dpo: $('#modalAddResponsableInFiche #prenom_dpo').val(),
                nom_dpo: $('#modalAddResponsableInFiche #nom_dpo').val(),
                numerocnil_dpo: $('#modalAddResponsableInFiche #numerocnil_dpo').val(),
                email_dpo: $('#modalAddResponsableInFiche #email_dpo').val(),
                telephonefixe_dpo: $('#modalAddResponsableInFiche #telephonefixe_dpo').val(),
                telephoneportable_dpo: $('#modalAddResponsableInFiche #telephoneportable_dpo').val(),
            };

            sendNewResponsable(dataResponsable);
        });

        $('#modalAddResponsableInFiche').on('hidden.bs.modal', function () {
            $('#modalAddResponsableInFiche #raisonsocialestructure').val("");
            $('#modalAddResponsableInFiche #telephonestructure').val("");
            $('#modalAddResponsableInFiche #faxstructure').val("");
            $('#modalAddResponsableInFiche #adressestructure').val("");
            $('#modalAddResponsableInFiche #emailstructure').val("");
            $('#modalAddResponsableInFiche #siretstructure').val("");
            $('#modalAddResponsableInFiche #apestructure').val("");
            $('#modalAddResponsableInFiche #civiliteresponsable').val("");
            $('#modalAddResponsableInFiche #prenomresponsable').val("");
            $('#modalAddResponsableInFiche #nomresponsable').val("");
            $('#modalAddResponsableInFiche #fonctionresponsable').val("");
            $('#modalAddResponsableInFiche #emailresponsable').val("");
            $('#modalAddResponsableInFiche #telephoneresponsable').val("");
            $('#modalAddResponsableInFiche #civility_dpo').val("");
            $('#modalAddResponsableInFiche #prenom_dpo').val("");
            $('#modalAddResponsableInFiche #nom_dpo').val("");
            $('#modalAddResponsableInFiche #numerocnil_dpo').val("");
            $('#modalAddResponsableInFiche #email_dpo').val("");
            $('#modalAddResponsableInFiche #telephonefixe_dpo').val("");
            $('#modalAddResponsableInFiche #telephoneportable_dpo').val("");
        });

    });

    function sendNewResponsable(data)
    {
        $('#coresponsables').data('selected', $('#coresponsables').val());

        $.ajax({
            url: '<?php echo Router::url(['controller' => 'responsables', 'action' => 'ajax_add']);?>',
            method: 'POST',
            data: data,
            // data: $('#newResponsable').serialize(),
            success: function(dataResponse, textStatus, xhr) {
                try {
                    if (xhr.status === 201) {
                        // $('#modalAddResponsableInFiche')
                        //     .find("input, textarea")
                        //     .val('')
                        //     .end();
                        // $('#modalAddResponsableInFiche').find('form')[0].reset();
                        $('#modalAddResponsableInFiche').modal('toggle');


                        updateListingResponsable(dataResponse['id']);

                    } else {
                        $('#modalAddResponsableInFiche .modal-body').html(dataResponse);
                    }
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }

    function updateListingResponsable(newResponsable_id)
    {
        $.ajax({
            url: '<?php echo Router::url(['controller' => 'fiches', 'action' => 'ajax_update_listing_responsable']);?>',
            method: 'GET',
            success: function(dataResponse) {
                try {
                    let contents = JSON.parse(dataResponse);

                    // Suppression des options
                    $('#coresponsables option').remove();

                    // Suppression des cartes déjà selectionner
                    $('#coresponsableCards').empty();

                    $.each(contents, function (k, responsable) {
                        $.each(responsable, function (k, value) {
                            let dataOptions  = {
                                id: value['id'],
                                text: value['raisonsocialestructure']
                            };

                            let newOption = new Option(dataOptions.text, dataOptions.id, false, false);
                            $('#coresponsables').append(newOption).trigger('change');
                        });
                    });

                    let valueSelected = [];
                    if ($('#coresponsables').data('selected')) {
                        valueSelected = $('#coresponsables').data('selected');
                        $('#coresponsables').data('selected', null);
                    }

                    valueSelected.push(newResponsable_id);

                    $('#coresponsables').val(valueSelected).trigger('select2:selecting');

                    $.each(valueSelected, function (k, value) {
                        $('#coresponsables').trigger({
                            type: 'select2:select',
                            params: {
                                data: {
                                    id: value
                                }
                            }
                        });
                    });
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }
</script>
