<?php
$body = $this->WebcilForm->create('newSoustraitant', [
        'id' => 'newSoustraitant'
    ])
    . $this->element('Responsables_Soustraitants/soustraitant_ajax_add')
;

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        .'<button id="saveNewSoustraitant" type="button" class="btn btn-primary">'
            .'<i class="far fa-save fa-lg"></i>'
            . __d('default', 'default.btnSave')
        .'</button>'
    .'</div>'
    . $this->WebcilForm->end()
;

$content = [
    'title' => __d('fiche', 'fiche.popupAddSoustraitantInFiche'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddSoustraitantInTraitement',
    'content' => $content,
    'sizeModal' => 'modal-xl modal-dialog-scrollable'
]);
?>

<script type="text/javascript">
    $(document).ready(function () {

        $('#saveNewSoustraitant').click(function () {
            let dataSoustraitant = {
                raisonsocialestructure: $('#modalAddSoustraitantInTraitement #raisonsocialestructure').val(),
                telephonestructure: $('#modalAddSoustraitantInTraitement #telephonestructure').val(),
                faxstructure: $('#modalAddSoustraitantInTraitement #faxstructure').val(),
                adressestructure: $('#modalAddSoustraitantInTraitement #adressestructure').val(),
                emailstructure: $('#modalAddSoustraitantInTraitement #emailstructure').val(),
                siretstructure: $('#modalAddSoustraitantInTraitement #siretstructure').val(),
                apestructure: $('#modalAddSoustraitantInTraitement #apestructure').val(),
                civiliteresponsable: $('#modalAddSoustraitantInTraitement #civiliteresponsable').val(),
                prenomresponsable: $('#modalAddSoustraitantInTraitement #prenomresponsable').val(),
                nomresponsable: $('#modalAddSoustraitantInTraitement #nomresponsable').val(),
                fonctionresponsable: $('#modalAddSoustraitantInTraitement #fonctionresponsable').val(),
                emailresponsable: $('#modalAddSoustraitantInTraitement #emailresponsable').val(),
                telephoneresponsable: $('#modalAddSoustraitantInTraitement #telephoneresponsable').val(),
                civility_dpo: $('#modalAddSoustraitantInTraitement #civility_dpo').val(),
                prenom_dpo: $('#modalAddSoustraitantInTraitement #prenom_dpo').val(),
                nom_dpo: $('#modalAddSoustraitantInTraitement #nom_dpo').val(),
                numerocnil_dpo: $('#modalAddSoustraitantInTraitement #numerocnil_dpo').val(),
                email_dpo: $('#modalAddSoustraitantInTraitement #email_dpo').val(),
                telephonefixe_dpo: $('#modalAddSoustraitantInTraitement #telephonefixe_dpo').val(),
                telephoneportable_dpo: $('#modalAddSoustraitantInTraitement #telephoneportable_dpo').val(),
            };

            sendNewSoustraitant(dataSoustraitant);
        });

        $('#modalAddResponsableInFiche').on('hidden.bs.modal', function () {
            $('#modalAddSoustraitantInTraitement #raisonsocialestructure').val("");
            $('#modalAddSoustraitantInTraitement #telephonestructure').val("");
            $('#modalAddSoustraitantInTraitement #faxstructure').val("");
            $('#modalAddSoustraitantInTraitement #adressestructure').val("");
            $('#modalAddSoustraitantInTraitement #emailstructure').val("");
            $('#modalAddSoustraitantInTraitement #siretstructure').val("");
            $('#modalAddSoustraitantInTraitement #apestructure').val("");
            $('#modalAddSoustraitantInTraitement #civiliteresponsable').val("");
            $('#modalAddSoustraitantInTraitement #prenomresponsable').val("");
            $('#modalAddSoustraitantInTraitement #nomresponsable').val("");
            $('#modalAddSoustraitantInTraitement #fonctionresponsable').val("");
            $('#modalAddSoustraitantInTraitement #emailresponsable').val("");
            $('#modalAddSoustraitantInTraitement #telephoneresponsable').val("");
            $('#modalAddSoustraitantInTraitement #civility_dpo').val("");
            $('#modalAddSoustraitantInTraitement #prenom_dpo').val("");
            $('#modalAddSoustraitantInTraitement #nom_dpo').val("");
            $('#modalAddSoustraitantInTraitement #numerocnil_dpo').val("");
            $('#modalAddSoustraitantInTraitement #email_dpo').val("");
            $('#modalAddSoustraitantInTraitement #telephonefixe_dpo').val("");
            $('#modalAddSoustraitantInTraitement #telephoneportable_dpo').val("");
        });

    });

    function sendNewSoustraitant(data)
    {
        $('#soustraitances').data('selected', $('#soustraitances').val());

        $.ajax({
            url: '<?php echo Router::url(['controller' => 'soustraitants', 'action' => 'ajax_add']);?>',
            method: 'POST',
            data: data,
            // data: $('#newResponsable').serialize(),
            success: function(dataResponse, textStatus, xhr) {
                try {
                    if (xhr.status === 201) {
                        $('#modalAddSoustraitantInTraitement').modal('toggle');

                        updateListingSoustraitant(dataResponse['id']);

                    } else {
                        $('#modalAddSoustraitantInTraitement .modal-body').html(dataResponse);
                    }
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }

    function updateListingSoustraitant(newSoustraitant_id)
    {
        $.ajax({
            url: '<?php echo Router::url(['controller' => 'fiches', 'action' => 'ajax_update_listing_soustraitant']);?>',
            method: 'GET',
            success: function(dataResponse) {
                try {
                    let contents = JSON.parse(dataResponse);

                    // Suppression des options
                    $('#soustraitances option').remove();

                    // Suppression des cartes déjà selectionner
                    $('#soustraitanceCards').empty();

                    $.each(contents, function (k, responsable) {
                        $.each(responsable, function (k, value) {
                            let dataOptions  = {
                                id: value['id'],
                                text: value['raisonsocialestructure']
                            };

                            let newOption = new Option(dataOptions.text, dataOptions.id, false, false);
                            $('#soustraitances').append(newOption).trigger('change');
                        });
                    });

                    let valueSelected = [];
                    if ($('#soustraitances').data('selected')) {
                        valueSelected = $('#soustraitances').data('selected');
                        $('#soustraitances').data('selected', null);
                    }

                    valueSelected.push(newSoustraitant_id);

                    $('#soustraitances').val(valueSelected).trigger('select2:selecting');

                    $.each(valueSelected, function (k, value) {
                        $('#soustraitances').trigger({
                            type: 'select2:select',
                            params: {
                                data: {
                                    id: value
                                }
                            }
                        });
                    });
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }
</script>
