<?php
$body = '<div class="row">'
        .'<div class="col-md-12 text-warning">'
            .'<div class="col-md-12 text-center">'
                .'<i class="fa fa-exclamation-triangle fa-lg"></i>'
            .'</div>'
            .'<div class="col-md-12">'
                . __d('registre', 'registre.popupText')
            .'</div>'
        .'</div>'
    .'</div>'
    .'<div class="row top17">'
        .'<div class="col-md-12">'
            . $this->WebcilForm->create('Registre', [
                'url' => ['action' => 'edit'],
                'class' => 'form-horizontal'
            ])
            . $this->WebcilForm->input('motif', [
                'label' => [
                    'text' => __d('registre', 'registre.popupChampMotif'),
                ],
                'placeholder' => false,
                'class' => 'form-control',
                'div' => 'form-group',
                'required' => true
            ])
            .$this->WebcilForm->hidden('idEditRegistre', [
                'value' => '',
                'id' => "toModif"
            ])
        .'</div>'
    .'</div>'
;

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'escape' => false
        ])
    .'</div>'
;

$content = [
    'title' => __d('registre', 'registre.popupTitreEditionTraitementRegistre'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalEditRegistre',
    'controller' => 'Registre',
    'action' => 'edit',
    'content' => $content
]);
