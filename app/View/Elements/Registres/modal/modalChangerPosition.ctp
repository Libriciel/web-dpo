<?php
$body = '<table class="table table-striped table-hover" id="render">'
        .'<thead>'
            .'<tr>'
                .'<th class="col-md-1">'
                    . __d('registre', 'registre.textTableauPositionTraitement')
                .'</th>'
                .'<th class="col-md-1">'
                    . __d('registre', 'registre.textTableauNumeroEnregistrement')
                .'</th>'
                .'<th class="col-md-10">'
                    . __d('registre', 'registre.titreTableauNomTraitement')
                .'</th>'
            .'</tr>'
        .'</thead>'
        .'<tbody></tbody>'
    .'</table>'
;

$footer = '<div class="buttons">'
        .'<button type="button" class="btn btn-outline-primary" data-dismiss="modal">'
            .'<i class="fa fa-times-circle fa-lg"></i>'
            . __d('default', 'default.btnCancel')
        .'</button>'
        . ' '
        . $this->Form->button('<span class="fa fa-cogs fa-lg">&nbsp;</span>' . __d('registre', 'registre.btnGenerer'), [
            'onclick' => "sendDataExtrait()",
            'class' => 'btn btn-primary pull-left btn_popupGenerer'
        ])
        . $this->Form->button('<span class="fa fa-download fa-lg">&nbsp;</span>' . __d('registre', 'registre.btnTélécharger'), [
            'onclick' => "sendData()",
            'class' => 'btn btn-primary pull-left btn_popupTelecharger'
        ])
        .$this->Form->button('<span class="fa fa-cogs fa-lg">&nbsp;</span>' . __d('registre', 'registre.btnGenerer'), [
            'onclick' => "sendDataDeclaration()",
            'class' => 'btn btn-primary pull-left btn_popupGenererNonVerrouiller'
        ])
        . $this->Form->button('<span class="fa fa-download fa-lg">&nbsp;</span>' . __d('registre', 'registre.btnExport'), [
            'onclick' => "sendDataExport()",
            'class' => 'btn btn-primary pull-left btn_popupExportCsv'
        ])
        . $this->Form->button('<span class="fa fa-download fa-lg">&nbsp;</span>' . __d('registre', 'registre.btnGenerer'), [
            'onclick' => "sendDataExtraitHtml()",
            'class' => 'btn btn-primary pull-left btn_popupGenererExtraitRegistreHtml'
        ])
    .'</div>'
;

$content = [
    'title' => __d('registre', 'registre.popupPositionnement'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalChangerPosition',
    'content' => $content,
    'sizeModal' => 'modal-lg modal-dialog-scrollable'
]);
