<?php
// Attributs
$modalId = true === isset($modalId) ? $modalId : 'myModal';
$titleTag = true === isset($titleTag) ? $titleTag : 'h4';
$titleId =  true === isset($titleId) ? $titleId : "aria-labelledby-{$modalId}";
$controller =  true === isset($controller) ? $controller : null;
$action =  true === isset($action) ? $action : '';
$optionsForm = true === isset($optionsForm) ? $optionsForm : [];
$optionsUrl = true === isset($optionsUrl) ? $optionsUrl : [];
$sizeModal = true === isset($sizeModal) ? $sizeModal : '';

$content = true === isset($content)
    ? $content
    : [];

$content += [
    'title' => null,
    'body' => null,
    'footer' => null
];

$url = [
    'action' => $action,
] + $optionsUrl;
?>

<div class="modal fade" id="<?php echo $modalId;?>" tabindex="-1" aria-labelledby="<?php echo $titleId;?>" aria-hidden="true">
    <div class="modal-dialog <?php echo $sizeModal;?>">

        <?php
        if (!empty($controller)) {
            echo $this->WebcilForm->create($controller, [
                'url' => $url,
                'autocomplete' => 'off',
                'inputDefaults' => ['div' => false],
                'class' => 'form-horizontal',
                'novalidate' => 'novalidate'
            ] + $optionsForm);
        }
        ?>

        <div class="modal-content">
            <div class="modal-header">
                <?php
                echo $this->Html->tag($titleTag, $content['title'], [
                    'id' => $titleId,
                    'class' => 'modal-title'
                ]);
                ?>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
                <?php
                echo $content['body'];
                ?>
            </div>
            
            </hr>
            
            <div class="modal-footer">
                <?php
                echo $content['footer'];
                ?>
            </div>
        </div>

        <?php
        if (!empty($controller)) {
            echo $this->WebcilForm->end();
        }
        ?>

    </div>
</div>
