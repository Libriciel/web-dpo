<?php
$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);
$fieldname = isset($fieldname) ? $fieldname : 'organisation';
$label = isset($label) ? $label : __d($domain, "{$domain}.champFiltreEntiteAssociee");

echo $this->element('Filtres/Parametrages/organisation', compact('domain', 'fieldname', 'label', 'options'));
