<?php
$id = true === isset($id) ? $id : '';
$fieldname = true === isset($fieldname) ? $fieldname : '';
$label = true === isset($label) ? $label : '';
$placeholder = true === isset($placeholder) ? $placeholder : '';

echo $this->Form->input(
    $fieldname,
    [
        'id' => $id,
        'class' => 'usersDeroulant form-control',
        'label' => $label,
        'empty' => true,
        'data-placeholder' => $placeholder,
        'options' => [
            'true' => 'Oui',
            'false' => 'Non'
        ],
        'before' => '<div class="form-group">',
        'after' => '</div>'
    ]
);
