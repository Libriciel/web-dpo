<?php
$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);

$fieldname = isset($fieldname) ? $fieldname : 'archive';
$id = isset($id) ? $id : $this->Html->domId($fieldname);
$label = isset($label) ? $label : __d($domain, "{$domain}.champFiltreArchive");
$placeholder = isset($placeholder) ? $placeholder : __d($domain, "{$domain}.placeholderChampFiltreArchive");

echo $this->element('Filtres/Parametrages/true_false', [
    'fieldname' => $fieldname,
    'label' => $label,
    'placeholder' => $placeholder
]);
