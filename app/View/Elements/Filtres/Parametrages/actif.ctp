<?php
$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);
$fieldname = isset($fieldname) ? $fieldname : 'actif';
$id = isset($id) ? $id : $this->Html->domId($fieldname);
$label = isset($label) ? $label : __d($domain, "{$domain}.champFiltreActif");
$placeholder = isset($placeholder) ? $placeholder : __d($domain, "{$domain}.placeholderChampFiltreActif");

echo $this->Form->input(
    $fieldname,
    [
        'id' => $id,
        'class' => 'usersDeroulant form-control',
        'label' => $label,
        'empty' => true,
        'data-placeholder' => $placeholder,
        'options' => [
            'true' => 'Activé',
            'false' => 'Désactivé'
        ],
        'before' => '<div class="form-group">',
        'after' => '</div>'
    ]
);
