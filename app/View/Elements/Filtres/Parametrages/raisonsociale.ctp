<?php
$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);
$fieldname = isset($fieldname) ? $fieldname : 'raisonsociale';
$id = isset($id) ? $id : $this->Html->domId($fieldname);
$label = isset($label) ? $label : __d($domain, "{$domain}.champFiltreRaisonSociale");
//$options = [];
$placeholder = isset($placeholder) ? $placeholder : __d($domain, "{$domain}.placeholderChampFiltreRaisonSociale");
echo $this->Form->input(
    $fieldname,
    [
        'id' => $id,
        'class' => 'usersDeroulant form-control',
        'label' => $label,
        'empty' => true,
        'data-placeholder' => $placeholder,
        'options' => $options,
        'before' => '<div class="form-group">',
        'after' => '</div>'
    ]
);
