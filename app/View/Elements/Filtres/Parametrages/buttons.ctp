<div class="row">
    <div class="col-md-12 top30 text-center">
        <div class="buttons">
            <?php
            // Bouton Réinitialiser le filtre
            echo $this->Html->link('<i class="fa fa-undo fa-lg"></i> ' . __d('registre', 'registre.btnReinitialiserFiltre'), [
                'controller' => $this->request->params['controller'],
                'action' => $this->request->params['action']
                ],[
                'class' => 'btn btn-outline-danger',
                'escape' => false,
            ]);
            ?>

            <?php
            // Bouton Appliquer les filtres
            echo $this->Form->button('<i class="fa fa-filter fa-lg"></i> ' . __d('registre', 'registre.btnFiltre'), [
                'type' => 'submit',
                'class' => 'btn btn-outline-primary'
            ]);
            ?>
        </div>
    </div>
</div>
