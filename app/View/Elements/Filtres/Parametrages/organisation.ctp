<?php
//$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);
//$fieldname = null;
//$label = null;
//$options = [];
$placeholder = isset($placeholder) ? $placeholder : __d($domain, "{$domain}.placeholderChoisirEntite");
echo $this->Form->input(
    $fieldname,
    [
        'class' => 'usersDeroulant form-control',
        'id' => $this->Html->domId($fieldname),
        'label' => $label,
        'empty' => true,
        'data-placeholder' => $placeholder,
        'options' => $options,
        'before' => '<div class="form-group">',
        'after' => '</div>'
    ]
);
