<?php
$domain = isset($domain) ? $domain : Inflector::singularize($this->request->params['controller']);
$fieldname = isset($fieldname) ? $fieldname : 'createdbyorganisation';
$label = isset($label) ? $label : __d($domain, "{$domain}.champFiltreEntiteCreatrice");

echo $this->element('Filtres/Parametrages/organisation', compact('domain', 'fieldname', 'label', 'options'));
