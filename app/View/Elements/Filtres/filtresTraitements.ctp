<?php
$showForRegistre = isset($showForRegistre) ? $showForRegistre : false;
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create($nameForm, [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);

    if ($showForRegistre === false) {
        ?>
        <div class="row col-md-12">
            <div class="col-md-6">
                <?php
                // Filtrer par Etat du traitement
                echo $this->Form->input('filtreEtatTraitement', [
                    'options' => $options['etatTraitement'],
                    'class' => 'usersDeroulant form-control',
                    'empty' => true,
                    'label' => __d('pannel', 'pannel.champFiltreEtatTraitement'),
                    'data-placeholder' => __d('pannel', 'pannel.placeholderSelectionnerEtatTraitement'),
                    'multiple' => true,
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par rédacteur
            echo $this->Form->input('user', [
                'options' => $options['users'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreUser'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerUser'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtrer par service
            echo $this->Form->input('service', [
                'options' => $options['services'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreService'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerService'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtrer par nom du traitement
            echo $this->Form->input('outil', [
                'class' => 'form-control',
                'label' => __d('registre', 'registre.filtreNomTraitement'),
                'placeholder' => __d('registre', 'registre.placeholderNomTraitement'),
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <br/>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtre sur les normes
            echo $this->Form->input('norme', [
                'options' => $options_normes,
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreNorme'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerNorme'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtre sur les référentiels
            echo $this->Form->input('referentiel', [
                'options' => $options_referentiels,
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreReferentiel'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerReferentiel'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <br/>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            if ($showForRegistre === true) {
                // Filtre sur le nombre de traitement à l'affichage
                echo $this->Form->input('nbAffichage', [
                    'options' => [
                        PHP_INT_MAX => 'Tous les traitements',
                        10 => '10',
                        20 => '20',
                        50 => '50',
                        100 => '100'
                    ],
                    'class' => 'usersDeroulant form-control',
                    'label' => __d('registre', 'registre.filtreNbAffichageTraitement'),
                    'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerNbTraitementAfficher'),
                    'empty' => false,
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
            }
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtre par ordre d'affichage
            if ($showForRegistre === true) {
                echo $this->Form->input('order', [
                    'options' => [
                        'Fiche.created DESC' => 'Classement par date de création la plus récente',
                        'Fiche.created ASC' => 'Classement par date de création la plus vieille',
                        'Fiche.numero ASC' => 'Classement par numéro d\'enregistrement croissant',
                        'Fiche.numero DESC' => 'Classement par numéro d\'enregistrement décroissant',
                    ],
                    'class' => 'usersDeroulant form-control',
                    'label' => __d('registre', 'registre.filtreOrder'),
                    'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerOrder'),
                    'empty' => false,
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
            }
            ?>
        </div>
    </div>

    <br/>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtre sur les formulaires
            echo $this->Form->input('formulaire', [
                'options' => $options['formulaires'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreFormulaire'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerFormulaire'),
                'multiple' => 'multiple',
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur l'obligation d'un AIPD
            echo $this->Form->input('obligation_pia', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreObligationPIA'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerObligationPIA'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <br/>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtre sur la réalisation de AIPD
            echo $this->Form->input('realisation_pia', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreRealisationPIA'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerRealisationPIA'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur les normes
            echo $this->Form->input('transfert_hors_ue', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreTransfereHorsUE'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerTransfereHorsUE'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur les sous-traitants
            echo $this->Form->input('soustraitance', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreSoustraitance'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerSoustraitance'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur les coresponsables
            echo $this->Form->input('coresponsable', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreCoresponsable'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerCoresponsable'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur le type de registre
            echo $this->Form->input('decisionAutomatisee', [
                'options' => [
                    'Non' => 'Non',
                    'Oui' => 'Oui'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreDecisionAutomatisee'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerDecisionAutomatisee'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtre sur le depot de AIPD
            echo $this->Form->input('depot_pia', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreDepotPIA'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerDepotPIA'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur les formulaires
            echo $this->Form->input('donnees_sensibles', [
                'options' => [
                    'true' => 'Oui',
                    'false' => 'Non'
                ],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreDonneesSensibles'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerDonneesSensibles'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            echo $this->Form->input('soustraitant', [
                'options' => $options['all_soustraitants'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreSoustraitant'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerSoustraitant'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            echo $this->Form->input('responsable', [
                'options' => $options['all_responsables'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreResponsable'),
                'data-placeholder' =>  __d('registre', 'registre.placeholderSelectionnerResponsable'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtre sur type d'annexe
            echo $this->Form->input('type_annexe', [
                'options' => $options['types_annexe'],
                'class' => 'usersDeroulant form-control',
                'label' => __d('registre', 'registre.filtreTypeAnnexe'),
                'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerTypeAnnexe'),
                'empty' => true,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <?php
    if ($showForRegistre === true) {
        ?>
        <br/>

        <div class="row col-md-12">
            <div class="col-md-6">
                <?php
                // Filtre sur le type de registre
                echo $this->Form->input('typeRegistre', [
                    'options' => [
                        'null' => "Registre d'activité et de sous-traitance",
                        'false' => "Registre d'activité",
                        'true' => "Registre de sous-traitance"
                    ],
                    'class' => 'usersDeroulant form-control',
                    'label' => __d('registre', 'registre.filtreTypeRegistre'),
                    'data-placeholder' => __d('registre', 'registre.placeholderSelectionnerTypeRegistre'),
                    'empty' => true,
                    'before' => '<div class="form-group">',
                    'after' => '</div>'
                ]);
                ?>
            </div>
        </div>
        <?php
    }

    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>
