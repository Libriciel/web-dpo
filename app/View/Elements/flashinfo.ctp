<?php
echo $this->element('flash', [
    'classFlash' => 'flashinfo',
    'icon' => 'fa-info-circle',
    'message' => $message,
]);
