<?php
if (Configure::read('debug') > 0) {
    $append = true === isset($append) ? $append : 'footer div:not(.ls-xs-hide)';
    echo $this->Html->tag('span', $this->element('sql_dump'), ['id' => 'sqldump', 'style' => 'display: none']);
    ?>

    <script type="text/javascript">
    //<![CDATA[
        $(document).ready(function () {
            $(<?php echo "'{$append}'"; ?>).append(' - <a href="#" id="SqlDumpToggler" onclick="$(\'#sqldump\').toggle();return false;">requêtes SQL<\/a>');
            let text = $('#sqldump caption').html().replace(/^.* ([0-9]+) queries took ([0-9]+) ms$/, '$1 requêtes SQL en $2 ms');
            $('#SqlDumpToggler').html(text);
        });
    //]]>
    </script>
    <?php
}
