<?php
echo $this->element('flash', [
    'classFlash' => 'flasherror',
    'icon' => 'fa-times',
    'message' => $message,
]);
