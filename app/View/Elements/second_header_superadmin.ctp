<?php
$items = [
    'Entités' => [
        __d('default', 'default.sousTitreEntites') => [
            'icon' => 'fa-building',
            'url' => [
                'controller' => 'organisations',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.titreAdministrationEntite') => [
            'icon' => 'fas fa-wrench',
            'url' => [
                '#' => '#'
            ],
            'data-toggle' => 'modal',
            'data-target' => '#modalAdministrerEntite'
        ],
        'icon' => 'fas fa-wrench',
    ],
    'Gestion' => [
        __d('default', 'default.sousTitreSuperAdministrateur') => [
            'icon' => 'fas fa-user',
            'url' => [
                'controller' => 'admins',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.sousTitreGestionTousUtilisateurs') => [
            'icon' => 'fas fa-users',
            'url' => [
                'controller' => 'users',
                'action' => 'admin_index'
            ]
        ],
        __d('default', 'default.sousTitreGestionTousSousTraitants') => [
            'icon' => 'fas fa-list-alt',
            'url' => [
                'controller' => 'soustraitants',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.sousTitreGestionTousResponsable') => [
            'icon' => 'fa-list-alt',
            'url' => [
                'controller' => 'responsables',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.sousTitreGestionTypagesAnnexes') => [
            'icon' => 'fa-list-alt',
            'url' => [
                'controller' => 'typages',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.sousTitreReferentiels') => [
            'icon' => 'fas fa-certificate',
            'url' => [
                'controller' => 'referentiels',
                'action' => 'index'
            ]
        ],
        __d('default', 'default.sousTitreGestionPolitiqueConfidentialite') => [
            'icon' => 'fas fa-user-secret',
            'url' => [
                'controller' => 'organisations',
                'action' => 'gestionrgpd'
            ]
        ],
        'icon' => 'fas fa-users'
    ],
    __d('default', 'default.sousTitrePageConnexion') => [
        'icon' => 'fas fa-sliders-h',
        'url' => [
            'controller' => 'admins',
            'action' => 'changeviewlogin'
        ],
        'class' => 'nav-link'
    ]
];

$menu = $this->WebdpoMenu->main($items);
?>

<nav class="navbar fixed-top second-nav navbar-expand-md navbar-custom navbar-dark bg-ls-dark">
    <div class="collapse navbar-collapse">
        <?php
        echo $menu;
        ?>
    </div>
</nav>
