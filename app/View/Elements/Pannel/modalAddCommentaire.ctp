<?php
$body = $this->WebcilForm->inputs([
    'EtatFiche.commentaire' => [
        'label' => [
            'text' =>  'Commentaire',
        ],
        'class' => 'form-control reponseStartTinyMCE',
        'required' => true,
        'placeholder' => false,
        'type' => 'textarea'
    ],
    'EtatFiche.idUserCommentaire' => [
        'type' => 'hidden',
        'value' => json_encode($idUserCommentaire)
    ],
    'EtatFiche.etat_fiche_id' => [
        'type' => 'hidden',
        'value' => $id
    ],
    'EtatFiche.fiche_id' => [
        'type' => 'hidden',
        'value' => $fiche_id
    ],
]);

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='fa fa-paper-plane fa-lg'></i>" . __d('default', 'default.btnSend'), [
        'type' => 'submit',
        'class' => 'btn btn-outline-success',
        'escape' => false
    ])
    .'</div>'
;

$content = [
    'title' => __d('formulaire', 'formulaire.modalTitleRepondreCommentaire'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'AddCommentaire',
    'controller' => 'EtatFiches',
    'action' => 'repondreCommentaire',
    'content' => $content,
    'sizeModal' => 'modal-xl'
]);
