<?php
if (!empty($redacteurs)) {
    echo $this->element(
        'modal',
        [
            'modalId' => 'modalSendRedaction',
            'content' => [
                'title' => __d('pannel', 'pannel.popupEnvoyerTraitementRedaction'),
                'body' => $this->Html->tag(
                    'div',
                    $this->Form->create('EtatFiche', ['url' => [
                        'controller' => 'EtatFiches',
                        'action' => 'sendRedaction'
                    ]])
                    . $this->Form->input('destinataire', [
                        'class' => 'form-control usersDeroulant transformSelect form-control bottom5',
                        'label' => [
                            'text' => __d('pannel', 'pannel.textSelectUserRedacteur') . '<span class="requis">*</span>',
                            'class' => 'control-label'
                        ],
                        'options' => $redacteurs,
                        'empty' => __d('pannel', 'pannel.textSelectUserRedacteur'),
                        'required' => true,
                        'autocomplete' => 'off',
                        'id' => 'destinataireRedacteur'
                    ])
                    . '<br>'
                    . $this->Form->hidden('ficheNum', ['id' => 'ficheNumVal'])
                    . $this->Form->hidden('etatFiche', ['id' => 'etatFicheVal']),
                    ['class' => 'form-group']
                ),
                'footer' => $this->Html->tag(
                    'div',
                    $this->Form->button(
                        '<i class="fa fa-times-circle fa-lg"></i>'
                        . __d('default', 'default.btnCancel'),
                        ['class' => 'btn btn-outline-primary', 'data-dismiss' => 'modal']
                    )
                    . ' '
                    . $this->Form->button("<i class='fas fa-paper-plane fa-lg'></i>" . __d('default', 'default.btnSend'), [
                        'type' => 'submit',
                        'class' => 'btn btn-outline-success',
                        'escape' => false
                    ]),
                    ['class' => 'buttons']
                )
                . $this->Form->end()
            ]
        ]
    );
}
