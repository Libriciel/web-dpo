<?php
if (!empty($consultants)) {
    echo $this->element(
        'modal',
        [
            'modalId' => 'modalEnvoieConsultation',
            'content' => [
                'title' => __d('pannel', 'pannel.popupEnvoyerTraitementConsultation'),
                'body' => $this->Html->tag(
                    'div',
                    $this->Form->create('EtatFiche', ['url' => [
                        'controller' => 'EtatFiches',
                        'action' => 'askAvis'
                    ]])
                    . $this->Form->input('destinataire', [
                        'class' => 'form-control usersDeroulant transformSelect form-control bottom5',
                        'label' => [
                            'text' => __d('pannel', 'pannel.textSelectUserConsultant') . '<span class="requis">*</span>',
                            'class' => 'control-label'
                        ],
                        'options' => $consultants,
                        'empty' => __d('pannel', 'pannel.textSelectUserConsultant'),
                        'required' => true,
                        'autocomplete' => 'off',
                        'id' => 'destinataireCons'
                    ])
                    . '<br>'
                    . $this->Form->hidden('ficheNum', ['id' => 'ficheNumCons'])
                    . $this->Form->hidden('etatFiche', ['id' => 'etatFicheCons']),
                    ['class' => 'form-group']
                ),
                'footer' => $this->Html->tag(
                    'div',
                    $this->Form->button(
                        '<i class="fa fa-times-circle fa-lg"></i>'
                        . __d('default', 'default.btnCancel'),
                        ['class' => 'btn btn-outline-primary', 'data-dismiss' => 'modal']
                    )
                    . ' '
                    . $this->Form->button("<i class='fas fa-paper-plane fa-lg'></i>" . __d('default', 'default.btnSend'), [
                        'type' => 'submit',
                        'class' => 'btn btn-outline-success',
                        'escape' => false
                    ]),
                    ['class' => 'buttons']
                )
                . $this->Form->end()
            ]
        ]
    );
}
