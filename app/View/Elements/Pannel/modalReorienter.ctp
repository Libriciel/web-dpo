<?php
if (!empty($validants)) {
    echo $this->element(
        'modal',
        [
            'modalId' => 'modalReorienter',
            'content' => [
                'title' => __d('pannel', 'pannel.popupReorienterTraitement'),
                'body' => $this->Html->tag(
                    'div',
                    $this->Form->create('EtatFiche', ['url' => [
                        'controller' => 'EtatFiches',
                        'action' => 'reorientation'
                    ]])
                    . $this->Form->input('destinataire', [
                        'class' => 'form-control usersDeroulant transformSelect form-control bottom5',
                        'label' => [
                            'text' => __d('pannel', 'pannel.textSelectReorienterValideur') . '<span class="requis">*</span>',
                            'class' => 'control-label'
                        ],
                        'options' => $validants,
                        'empty' => __d('pannel', 'pannel.textSelectReorienterValideur'),
                        'required' => true,
                        'autocomplete' => 'off',
                        'id' => 'destinataireReo'
                    ])
                    . $this->Form->hidden('ficheNum', ['id' => 'ficheNumReo'])
                    . $this->Form->hidden('etatFiche', ['id' => 'etatFicheReo']),
                    ['class' => 'form-group']
                ),
                'footer' => $this->Html->tag(
                    'div',
                    $this->Form->button(
                        '<i class="fa fa-times-circle fa-lg"></i>'
                        . __d('default', 'default.btnCancel'),
                        ['class' => 'btn btn-outline-primary', 'data-dismiss' => 'modal']
                    )
                    . ' '
                    . $this->Form->button("<i class='fas fa-paper-plane fa-lg'></i>" . __d('default', 'default.btnSend'), [
                        'type' => 'submit',
                        'class' => 'btn btn-outline-success',
                        'escape' => false
                    ]),
                    ['class' => 'buttons']
                )
                . $this->Form->end()
            ]
        ]
    );
}
