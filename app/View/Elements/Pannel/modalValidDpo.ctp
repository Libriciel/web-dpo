<?php
//if ($this->Autorisation->isDpo() && !empty($options_normes)) {
if ($this->Autorisation->isDpo()) {
    echo $this->element(
        'modal',
        [
            'modalId' => 'modalValidDpo',
            'content' => [
                'title' => 'Insertion au registre',
                'body' => $this->Html->tag(
                        'div',
                        $this->Html->tag(
                            'div',
                            $this->Html->tag(
                                'div',
                                '<i class="fa fa-fw fa-exclamation-triangle"></i>',
                                ['class' => 'col-md-12 text-center']
                            )
                            . $this->Html->tag(
                                'div',
                                __d('pannel','pannel.confirmationInsererRegistre'),
                                ['class' => 'col-md-12']
                            ),
                            ['class' => 'col-md-12 text-warning']
                        ),
                        ['class' => 'row']
                    )
                    . $this->Html->tag(
                        'div',
                        $this->Html->tag(
                            'div',
                            $this->WebcilForm->create('Registre', [
                                'url' => [
                                    'controller' => 'registres',
                                    'action' => 'add'
                                ]
                            ])
                            . $this->WebcilForm->input('Registre.numero', [
                                'id' => 'numero',
                                'label' => [
                                    'text' => 'Numéro d\'enregistrement',
                                    'class' => 'control-label'
                                ],
                                'placeholder' => false
                            ])
                            . $this->WebcilForm->hidden('idfiche', ['id' => 'idFiche']),
                            ['class' => 'col-md-12 form-horizontal']
                        ),
                        ['class' => 'row top17']
                    ),
                'footer' => $this->Html->tag(
                    'div',
                    $this->WebcilForm->button(
                        '<i class="fa fa-times-circle fa-lg"></i>'
                        . __d('default', 'default.btnCancel'),
                        ['class' => 'btn btn-outline-primary', 'data-dismiss' => 'modal']
                    )
                    . ' '
                    . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
                        'type' => 'submit',
                        'class' => 'btn btn-primary',
                        'escape' => false
                    ]),
                    ['class' => 'buttons']
                )
                . $this->WebcilForm->end()
            ]
        ]
    );
}
