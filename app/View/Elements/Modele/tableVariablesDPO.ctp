<?php
// Logo DPO
if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
    echo $this->Html->image('logo_dpo.svg', [
        'class' => 'logo-well',
    ]);
} else {
    ?>
    <h4>
        <?php echo __d('modele', 'modele.sousTitreDPO'); ?>
    </h4>
    <?php
}

if (!empty($variablesOrganisation['Dpo'])) {
    ?>
    <!-- Tableau du DPO -->
    <table class="table">
        <thead>
            <tr>
                <th class="col-md-4">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomChamp');
                    ?>
                </th>

                <th class="col-md-4">
                    <?php
                    echo __d('modele', 'modele.titreTableauNomVariable');
                    ?>
                </th>

                <th class="col-md-4">
                    <?php
                    echo __d('modele', 'modele.titreTableauValeur');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
        <?php
        foreach ($variablesOrganisation['Dpo'] as $keyDpo => $dpo) {
            foreach ($dpo as $variable => $value) {
                ?>
                <tr>
                    <td class="tdleft">
                        <?php
                        echo __d('user', 'user.champ' . ucfirst($keyDpo));
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $variable;
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $value;
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <?php
}
?>

<br/>
