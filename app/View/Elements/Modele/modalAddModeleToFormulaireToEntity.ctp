<?php
$body = '<div class="row top17">'
        .'<div class="col-md-12">'
            . $this->WebcilForm->inputs([
                'addFormulaire_id' => [
                    'id' => 'addFormulaire_id',
                    'options' => $mesFormulaires,
                    'class' => 'usersDeroulant form-control',
                    'required' => true,
                    'empty' => true,
                    'data-placeholder' => 'Sélectionnez un formulaire',
                ],
                'addOrganisation_id' => [
                    'id' => 'addOrganisation_id',
                    'class' => 'usersDeroulant form-control',
                    'options' => [],
                    'empty' => true,
                    'multiple' => true,
                    'data-placeholder' => __d('responsable_soustraitant','responsable_soustraitant'.'.placeholderSelectOrganisation'),
                    'label' => [
                        'text' => __d('responsable_soustraitant','responsable_soustraitant'.'.champSelectOrganisation'),
                    ],
                    'required' => true
                ],
                'modele' => [
                    'id' => 'modele',
                    'div' => 'input-group inputsForm',
                    'type' => 'file',
                    'class' => 'filestyle',
                    'accept' => 'application/vnd.oasis.opendocument.text',
                    'required' => true,
                ]
            ])
        .'</div>'
    .'</div>'
;

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
        'id' => 'btnSave',
        'type' => 'submit',
        'class' => 'btn btn-primary',
        'escape' => false,
    ])
    .'</div>'
;

$content = [
    'title' => __d('modele', 'modele.popupEnvoiModele'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddModele',
    'controller' => $controller,
    'action' => 'add',
    'optionsForm' => [
        'type' => 'file'
    ],
    'content' => $content
]);
?>

<script type="text/javascript">

    $(document).ready(function () {

        hideAndRemoveAllFields();

        $('#addFormulaire_id').change(function () {
            let form_id = $(this).val()

            hideAndRemoveAllFields();

            if (form_id) {
                updateListingOrganisation(form_id);
            }
        });

        $("#addOrganisation_id").change(function () {
            if ($(this).val() != null) {
                $('#modele').parent().show();
            } else {
                $('#btnSave').attr('disabled', true);
                hideModeleField();
            }
        });

        $("#modele").change(function () {
            if ($(this).val() != null) {
                $('#btnSave').removeAttr('disabled');
            } else {
                $('#btnSave').attr('disabled', true);
            }
        });

    });

    function hideAndRemoveAllFields()
    {
        $('#btnSave').attr('disabled', true);

        $('#addOrganisation_id option').remove();
        $('#addOrganisation_id').parent().hide();

        hideModeleField();
    }

    function hideModeleField()
    {
        $("#modele").val('');
        $("#modele").filestyle('clear');
        $('#modele').parent().hide();
    }

    function updateListingOrganisation(form_id)
    {
        $.ajax({
            url: '<?php echo Router::url(['controller' => 'modeles', 'action' => 'ajax_update_listing_organisation']);?>',
            method: 'POST',
            data: {'formulaire_id': form_id},
            success: function(dataResponse) {
                try {
                    let organisations = JSON.parse(dataResponse);

                    $.each(organisations, function (key, value) {
                        let newOption = new Option(value, key, false, false);
                        $('#addOrganisation_id').append(newOption).trigger('change');
                    });

                    $('#addOrganisation_id').parent().show();
                } catch(e) {
                    console.error(e);
                }
            },
            error: function(e) {
                console.error(e);
            }
        });
    }

</script>
