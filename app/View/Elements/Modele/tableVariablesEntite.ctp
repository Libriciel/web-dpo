<?php
if (!empty($variablesOrganisation['Entite'])) {
    ?>
    <!-- Tableau de l'entité -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreEntité');
            ?>
        </h4>

        <thead>
        <tr>
            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauValeur');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($variablesOrganisation['Entite'] as $keyEntite => $entite) {
            foreach ($entite as $variable => $value) {
                ?>
                <tr>
                    <td class="tdleft">
                        <?php
                        echo __d('organisation', 'organisation.champ' . ucfirst($keyEntite));
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $variable;
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $value;
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>

    <br>
    <?php
} else {
    echo __d('modele', 'modele.textAucuneEntite');
}
?>

<br/>
