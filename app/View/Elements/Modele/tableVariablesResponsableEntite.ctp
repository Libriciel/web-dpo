<?php
if (!empty($variablesOrganisation['Responsable'])) {
    ?>
    <!-- Tableau du responsable l'organisation -->
    <table class="table">
        <h4>
            <?php
            echo __d('modele', 'modele.sousTitreResponsableEntité');
            ?>
        </h4>

        <thead>
        <tr>
            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauNomChamp');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauNomVariable');
                ?>
            </th>

            <th class="thleft col-md-5">
                <?php
                echo __d('modele', 'modele.titreTableauValeur');
                ?>
            </th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($variablesOrganisation['Responsable'] as $keyResponsable => $responsable) {
            foreach ($responsable as $variable => $value) {
                ?>
                <tr>
                    <td class="tdleft">
                        <?php
                        echo __d('organisation', 'organisation.champ' . ucfirst($keyResponsable));
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $variable;
                        ?>
                    </td>

                    <td class="tdleft">
                        <?php
                        echo $value;
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>

    <br>
    <?php
} else {
    echo __d('modele', 'modele.textAucuneEntite');
}
?>

<br/>
