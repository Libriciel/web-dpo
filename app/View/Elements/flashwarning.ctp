<?php
echo $this->element('flash', [
    'classFlash' => 'flashwarning',
    'icon' => 'fa-exclamation-triangle',
    'message' => $message,
]);
