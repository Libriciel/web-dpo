<br>

<div class="row col-md-12">
	<p class="counter text-muted col-md-12">
		<?php
        $format = 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}';
        echo $this->Paginator->counter([
            'format' => __($format)
        ]);
		?>
	</p>

	<?php
    if (true === $this->Paginator->hasPage(null, 2)) {
        ?>
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev('<<',
                [
                    'tag' => 'li',
                    'class' => 'page-item',
                    'disabledTag' => 'a',
                ],
                null,
                [
                    'class' => 'page-item disabled',
                ]
            );

            echo $this->Paginator->numbers([
                'separator' => '',
                'currentTag' => 'a',
                'currentClass' => 'active',
                'class' => 'page-item',
                'tag' => 'li',
                'first' => 1
            ]);

            echo $this->Paginator->next(__('>>'),
                [
                    'tag' => 'li',
                    'class' => 'page-item',
                    'disabledTag' => 'a',
                ],
                null,
                [
                    'class' => 'page-item disabled',
                ]
            );
            ?>
        </ul>
        <?php
    }
    ?>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('.pagination').find('.page-item').children().addClass('page-link');
    });

</script>
