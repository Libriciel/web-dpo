<?php
echo $this->element('flash', [
    'classFlash' => 'flashsuccess',
    'icon' => 'fa-check',
    'message' => $message,
]);
