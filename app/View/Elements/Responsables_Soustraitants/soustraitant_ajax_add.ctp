<?php
if (isset($this->validationErrors['Soustraitant']) && !empty($this->validationErrors['Soustraitant'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->element('Responsables_Soustraitants/fields');