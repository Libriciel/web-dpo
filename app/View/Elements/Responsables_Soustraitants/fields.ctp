<?php
//echo $this->Html->script('jquery-mask-plugin/dist/jquery.mask.min.js');
echo $this->Html->script('responsables_soustraitants.js');

$readonly = isset($readonly) === true ? $readonly : false;
$cannotModified = isset($cannotModified) === true ? $cannotModified : false;
$association = isset($association) === true ? $association : false;
$titleAssociation = isset($titleAssociation) === true ? $titleAssociation : '';
?>

<div class="users form">
    <h2>
        <?php
        echo __d('responsable_soustraitant', 'responsable_soustraitant.titreStructureResponsable');
        ?>
    </h2>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'raisonsocialestructure' => [
                    'id' => 'raisonsocialestructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champRaisonsocialestructure')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampRaisonsocialestructure'),
                    'required' => true,
                    'readonly' => $readonly || $cannotModified
                ],
                'telephonestructure' => [
                    'id' => 'telephonestructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champTelephonestructure')
                    ],
                    'class' => 'form-control maskTelephone',
                    'placeholder' => false,
                    'readonly' => $readonly
                ],
                'faxstructure' => [
                    'id' => 'faxstructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champFaxstructure')
                    ],
                    'class' => 'form-control maskTelephone',
                    'placeholder' => false,
                    'readonly' => $readonly
                ],
                'adressestructure' => [
                    'id' => 'adressestructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champAdressestructure')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampAdressestructure'),
                    'type' => 'textarea',
                    'readonly' => $readonly
                ],
                'emailstructure' => [
                    'id' => 'emailstructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champEmailstructure')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampEmailstructure'),
                    'readonly' => $readonly
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'siretstructure' => [
                    'id' => 'siretstructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champSiretstructure')
                    ],
                    'class' => 'form-control maskSiret',
                    'placeholder' => false,
                ],
                'apestructure' => [
                    'id' => 'apestructure',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champApestructure')
                    ],
                    'class' => 'form-control maskApe',
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampApestructure'),
                    'readonly' => $readonly
                ]
            ]);
            ?>
        </div>
    </div>

    <h2>
        <?php
        echo __d('responsable_soustraitant', 'responsable_soustraitant.titreResponsable');
        ?>
    </h2>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'civiliteresponsable' => [
                    'id' => 'civiliteresponsable',
                    'options' => [
                        'M.' => 'Monsieur',
                        'Mme.' => 'Madame'
                    ],
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champCiviliteresponsable')
                    ],
                    'placeholder' => false,
                    'empty' => true,
                    'readonly' => $readonly
                ],
                'prenomresponsable' => [
                    'id' => 'prenomresponsable',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champPrenomresponsable')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampPrenomresponsable'),
                    'readonly' => $readonly
                ],
                'nomresponsable' => [
                    'id' => 'nomresponsable',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champNomresponsable')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampNomresponsable'),
                    'readonly' => $readonly
                ],
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'fonctionresponsable' => [
                    'id' => 'fonctionresponsable',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champFonctionresponsable')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampFonctionresponsable'),
                    'readonly' => $readonly
                ],
                'emailresponsable' => [
                    'id' => 'emailresponsable',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champEmailresponsable')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampEmailresponsable'),
                    'readonly' => $readonly
                ],
                'telephoneresponsable' => [
                    'id' => 'telephoneresponsable',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champTelephoneresponsable')
                    ],
                    'class' => 'form-control maskTelephone',
                    'placeholder' => false,
                    'readonly' => $readonly
                ]
            ]);
            ?>
        </div>
    </div>

    <!-- Information sur le DPO -->
    <div class="col-md-8">
        <!-- Affichage du logo du DPO -->
        <?php
        if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
            echo $this->Html->image('logo_dpo.svg', [
                'class' => 'logo-well'
            ]);
        }
        ?>
    </div>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'civility_dpo' => [
                    'id' => 'civility_dpo',
                    'options' => [
                        'M.' => 'Monsieur',
                        'Mme.' => 'Madame'
                    ],
                    'class' => 'transformSelect form-control',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champCivilityDpo')
                    ],
                    'placeholder' => false,
                    'empty' => true,
                    'data-placeholder' => ' ',
                    'readonly' => $readonly
                ],
                'prenom_dpo' => [
                    'id' => 'prenom_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champPrenomDpo')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampPrenomDpo'),
                    'readonly' => $readonly
                ],
                'nom_dpo' => [
                    'id' => 'nom_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champNomDpo')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampNomDpo'),
                    'readonly' => $readonly
                ],
                'numerocnil_dpo' => [
                    'id' => 'numerocnil_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champNumerocnilDpo')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderNumerocnilDpo'),
                    'readonly' => $readonly
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'email_dpo' => [
                    'id' => 'email_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champEmailDpo')
                    ],
                    'placeholder' => __d('responsable_soustraitant', 'responsable_soustraitant.placeholderChampEmailDpo'),
                    'readonly' => $readonly
                ],
                'telephonefixe_dpo' => [
                    'id' => 'telephonefixe_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champTelephonefixeDpo')
                    ],
                    'class' => 'form-control maskTelephone',
                    'placeholder' => false,
                    'readonly' => $readonly
                ],
                'telephoneportable_dpo' => [
                    'id' => 'telephoneportable_dpo',
                    'label' => [
                        'text' => __d('responsable_soustraitant', 'responsable_soustraitant.champTelephoneportableDpo')
                    ],
                    'class' => 'form-control maskTelephone',
                    'placeholder' => false,
                    'readonly' => $readonly
                ]
            ]);
            ?>
        </div>
    </div>

    <?php
    if ($association === true) {
        ?>
        <h2>
            <?php
            echo $titleAssociation;
            ?>
        </h2>

        <div class="row">
            <!-- Colonne de gauche -->
            <div class="col-md-6">
                <?php
                echo $this->WebcilForm->input('Organisation.Organisation', [
                    'class' => 'form-control usersDeroulant',
                    'options' => $mesOrganisations,
                    'label' => [
                        'text' => __d('responsable_soustraitant','responsable_soustraitant.champSelectOrganisation'),
                    ],
                    'data-placeholder' => __d('responsable_soustraitant','responsable_soustraitant.placeholderSelectOrganisation'),
                    'readonly' => $readonly,
                    'empty' => true,
                    'multiple' => true,
                ]);
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
