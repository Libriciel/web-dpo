<?php
$body = '<div class="">'
    . $this->WebcilForm->input('Organisation.organisationcible', [
        'id' => 'organisationcible2',
        'options' => $listeOrganisations,
        'class' => 'usersDeroulant transformSelect form-control',
        'required' => true,
        'empty' => true,
        'placeholder' => false,
        'data-placeholder' => ' '
    ])
    .'</div>'
;

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='fa fa-wrench fa-lg'></i>" . __d('organisation', 'organisation.btnAdministrerEntite'), [
        'type' => 'submit',
        'class' => 'btn btn-outline-success',
        'escape' => false
    ])
    .'</div>'
;

$content = [
    'title' => __d('pannel', 'pannel.popupAdministrerEntite'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAdministrerEntite',
    'controller' => 'Organisation',
    'action' => 'change',
    'optionsUrl' => [
        'null',
        1,
        'organisations',
        'administrer'
    ],
    'content' => $content
]);

