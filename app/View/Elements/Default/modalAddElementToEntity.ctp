<?php
if (empty($justOneCheckbox)) {
    $justOneCheckbox = false;
}

if (empty($getOptionsEntity)) {
    $getOptionsEntity = false;
}

if (empty($mesOrganisations)) {
    $mesOrganisations = [];
}

$body = $this->WebcilForm->input($formulaireName.'.organisation_id', [
    'id' => 'organisation_id',
    'class' => 'form-control usersDeroulant',
    'options' => $mesOrganisations,
    'empty' => true,
    'multiple' => true,
    'data-placeholder' => __d('responsable_soustraitant','responsable_soustraitant'.'.placeholderSelectOrganisation'),
    'label' => [
        'text' => __d('responsable_soustraitant','responsable_soustraitant'.'.champSelectOrganisation'),
    ],
    'required' => true,
    'value' => null
]);

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
            'id' => 'btnSave',
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'escape' => false,
        ])
    .'</div>'
;

$content = [
    'title' => $modalTitle,
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddElementToEntity',
    'controller' => $controller,
    'content' => $content
]);
?>

<script type='text/javascript'>

    $(document).ready(function () {
        let formulaireName = <?php echo json_encode($formulaireName); ?>;
        let fieldName = <?php echo json_encode($fieldName); ?>;
        let btnAffecter = <?php echo json_encode($btnAffecter); ?>;
        let checkboxID = <?php echo json_encode($checkboxID); ?>;
        let justOneCheckbox = <?php echo json_encode($justOneCheckbox); ?>;
        let getOptionsEntity = <?php echo json_encode($getOptionsEntity); ?>;

        $('.'+checkboxID).attr('checked', false);
        $('#'+btnAffecter).attr('disabled', true);

        // Lors que l'on veux selectionner tous les checkbox
        $("#"+checkboxID).change(function () {
            $("."+checkboxID).not(':disabled').prop('checked', $(this).prop('checked'));

            if ($(this).is(':checked')) {
                $('#'+btnAffecter).removeAttr('disabled');
                $('.'+checkboxID).closest('tr').addClass('trChecked');
            } else {
                $('#'+btnAffecter).attr('disabled', true);
                $('.'+checkboxID).closest('tr').removeClass('trChecked');
            }
        });

        let inputTypeCheckbox = $('input[type="checkbox"]');
        $(inputTypeCheckbox).not("#"+checkboxID).change(function () {
            if (justOneCheckbox == false) {
                $('#' + checkboxID).prop('checked', $(inputTypeCheckbox).not('#' + checkboxID).not(':disabled').not(':checked').length === 0);
            } else {
                if ($('input.'+checkboxID+'[type="checkbox"]:checked').length === 0) {
                    $('input.'+checkboxID+'[type="checkbox"]').attr('disabled', false);
                } else {
                    $('input.'+checkboxID+'[type="checkbox"]').not('#'+$(this).attr('id')).attr('disabled', true);
                }
            }

            if (this.checked) {
                $(this).closest('tr').addClass('trChecked');
            } else {
                $(this).closest('tr').removeClass('trChecked');
            }
        });

        $(inputTypeCheckbox).not("#"+checkboxID).click(function() {
            $('#'+btnAffecter).attr("disabled", !$(inputTypeCheckbox).is(":checked"));

            $('input[type="hidden"]').remove()
        });

        $('#modalAddElementToEntity').on('shown.bs.modal', function () {
            let checkboxes = $('table tbody input[type=checkbox]'),
                hidden,
                value;

            $(this).find('input[type=hidden]').remove();

            $(checkboxes).each(function(idx) {
                if ($(checkboxes[idx]).prop('checked') === true) {
                    value = $(checkboxes[idx]).prop('value');

                    if (justOneCheckbox == false) {
                        hidden = '<input type="hidden" name="data[' + formulaireName + '][' + fieldName + '][]" value="' + value + '" />';
                    } else {
                        hidden = '<input type="hidden" name="data[' + formulaireName + '][' + fieldName + ']" value="' + value + '" />';
                    }

                    $("#modalAddElementToEntity .modal-body").append(hidden);
                }
            });

            if (getOptionsEntity === true) {
                let modele_id = $(this).find('input[type=hidden]').val();

                $("#organisation_id").empty();

                $.ajax({
                    url: '<?php echo Router::url(['controller' => 'modeles', 'action' => 'ajax_update_listing_organisation']);?>',
                    method: 'POST',
                    data: {'modele_id': modele_id},
                    success: function(dataResponse) {
                        try {
                            let organisations = JSON.parse(dataResponse);

                            $.each(organisations, function (key, value) {
                                let newOption = new Option(value, key, false, false);
                                $('#organisation_id').append(newOption).trigger('change');
                            });

                        } catch(e) {
                            console.error(e);
                        }
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            }
        });

        $('#btnSave').attr('disabled', true);
        // Lors d'action sur une menu déroulant : organisation_id
        $("#organisation_id").change(function () {
            if ($("#organisation_id").val() != null) {
                $('#btnSave').removeAttr('disabled');
            } else {
                $('#btnSave').attr('disabled', true);
            }
        });
    });

</script>
