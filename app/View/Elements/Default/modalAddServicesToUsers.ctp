<?php
if (empty($justOneCheckbox)) {
    $justOneCheckbox = false;
}

$body = $this->WebcilForm->input($formulaireName.'.user_id', [
    'id' => 'user_id',
    'class' => 'form-control usersDeroulant',
    'options' => $users,
    'empty' => true,
    'multiple' => true,
    'required' => true,
    'data-placeholder' => "Sélectionnez un ou plusieurs utilisateur(s)",
    'label' => [
        'text' => "Associer à une ou plusieurs utilisateur(s)",
    ]
]);

$footer = '<div class="buttons">'
        . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
            'type' => 'submit',
            'data-dismiss' => 'modal',
            'class' => 'btn btn-outline-primary',
            'escape' => false,
        ])
        . ' '
        . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
            'id' => 'btnSave',
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'escape' => false,
        ])
    .'</div>'
;

$content = [
    'title' => __d('service', 'service.popupTitreAffecterServices'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddServicesToUsers',
    'controller' => 'Service',
    'content' => $content
]);
?>

<script type='text/javascript'>

    $(document).ready(function () {
        let formulaireName = <?php echo json_encode($formulaireName); ?>;
        let fieldName = <?php echo json_encode($fieldName); ?>;
        let btnAffecter = <?php echo json_encode($btnAffecter); ?>;
        let checkboxID = <?php echo json_encode($checkboxID); ?>;
        let justOneCheckbox = <?php echo json_encode($justOneCheckbox); ?>;

        $('.'+checkboxID).attr('checked', false);
        $('#'+btnAffecter).attr('disabled', true);

        // Lors que l'on veux selectionner tous les checkbox
        $("#"+checkboxID).change(function () {
            $("."+checkboxID).not(':disabled').prop('checked', $(this).prop('checked'));

            if ($(this).is(':checked')) {
                $('#'+btnAffecter).removeAttr('disabled');
                $('.'+checkboxID).closest('tr').addClass('trChecked');
            } else {
                $('#'+btnAffecter).attr('disabled', true);
                $('.'+checkboxID).closest('tr').removeClass('trChecked');
            }
        });

        let inputTypeCheckbox = $('input[type="checkbox"]');
        $(inputTypeCheckbox).not("#"+checkboxID).change(function () {
            if (justOneCheckbox == false) {
                $('#' + checkboxID).prop('checked', $(inputTypeCheckbox).not('#' + checkboxID).not(':disabled').not(':checked').length === 0);
            } else {
                if ($('input.'+checkboxID+'[type="checkbox"]:checked').length === 0) {
                    $('input.'+checkboxID+'[type="checkbox"]').attr('disabled', false);
                } else {
                    $('input.'+checkboxID+'[type="checkbox"]').not('#'+$(this).attr('id')).attr('disabled', true);
                }
            }

            if (this.checked) {
                $(this).closest('tr').addClass('trChecked');
            } else {
                $(this).closest('tr').removeClass('trChecked');
            }
        });

        $(inputTypeCheckbox).not("#"+checkboxID).click(function() {
            $('#'+btnAffecter).attr("disabled", !$(inputTypeCheckbox).is(":checked"));

            $('input[type="hidden"]').remove()
        });

        $('#modalAddServicesToUsers').on('shown.bs.modal', function () {
            let checkboxes = $('table tbody input[type=checkbox]'),
                hidden, value;

            $(checkboxes).each(function(idx) {
                if ($(checkboxes[idx]).prop('checked') === true) {
                    if (justOneCheckbox == false) {
                        value = $(checkboxes[idx]).prop('value');
                        hidden = '<input type="hidden" name="data[' + formulaireName + '][' + fieldName + '][]" value="' + value + '" />';
                    } else {
                        value = $(checkboxes).prop('value');
                        hidden = '<input type="hidden" name="data[' + formulaireName + '][' + fieldName + ']" value="' + value + '" />';
                    }

                    $("#modalAddServicesToUsers .modal-body").append(hidden);
                }
            });
        });

        $('#btnSave').attr('disabled', true);
        // Lors d'action sur une menu déroulant : organisation_id
        $("#user_id").change(function () {
            if ($("#user_id").val() != null) {
                $('#btnSave').removeAttr('disabled');
            } else {
                $('#btnSave').attr('disabled', true);
            }
        });
    });

</script>
