<?php
$body = '<div class="container-fluid" role="main">'
    .'<ul class="nav nav-tabs" role="tablist">'
        .'<li class="nav-item">'
            .'<a class="nav-link active" data-toggle="tab" href="#formulaire_registre_activite">'
                .__d('default', 'default.ongletFormulaireActivite')
            .'</a>'
        .'</li>'
        .'<li class="nav-item">'
            .'<a class="nav-link" data-toggle="tab" href="#formulaire_registre_soustraitance">'
                .__d('default', 'default.ongletFormulaireSoustraitance')
            .'</a>'
        .'</li>'
    .'</ul>'
    .'<div class="container-fluid">'
        .'<div class="tab-content">'
            .'<div id="formulaire_registre_activite" class="tab-pane fade show active" role="tabpanel">'
                .'<table class="table table-bordered">'
                    .'<thead>'
                        .'<tr>'
                            .'<th class="col-md-3">'
                                .__d('default', 'default.popupTitreTableauNom')
                            .'</th>'
                            .'<th class="col-md-7">'
                                .__d('default', 'default.popupTitreTableauDescription')
                            .'</th>'
                            .'<th class="col-md-2">'
                                .__d('default', 'default.popupTitreTableauAction')
                            .'</th>'
                        .'</tr>'
                    .'</thead>'
                    .'<tbody>';
                        foreach ($formulaires_actifs as $key => $value) {
                            $body .= '<tr>'
                                .'<td>' . $value['Formulaire']['libelle'] . '</td>'
                                .'<td>' . $value['Formulaire']['description'] . '</td>'
                                .'<td>' . $this->Html->link(__d('default', 'default.popupBtnChoisir'), [
                                            'controller' => 'fiches',
                                            'action' => 'add',
                                            $value['Formulaire']['id']
                                        ], ['class' => 'btn btn-outline-dark'])
                                . '</td>'
                            .'</tr>';
                        }
                    $body .= '</tbody>'
                .'</table>'
            .'</div>'
            .'<div id="formulaire_registre_soustraitance" class="tab-pane fade" role="tabpanel">'
                .'<table class="table table-bordered">'
                    .'<thead>'
                        .'<tr>'
                            .'<th class="col-md-3">'
                                . __d('default', 'default.popupTitreTableauNom')
                            .'</th>'
                            .'<th class="col-md-7">'
                                . __d('default', 'default.popupTitreTableauDescription')
                            .'</th>'
                            .'<th class="col-md-2">'
                                . __d('default', 'default.popupTitreTableauAction')
                            .'</th>'
                        .'</tr>'
                    .'</thead>'
                    .'<tbody>';
                    foreach ($formulaires_soustraitances as $key => $value) {
                        $body .= '<tr>'
                            .'<td>' . $value['Formulaire']['libelle'] . '</td>'
                            .'<td>' . $value['Formulaire']['description'] . '</td>'
                            .'<td>' . $this->Html->link(__d('default', 'default.popupBtnChoisir'), [
                                'controller' => 'fiches',
                                'action' => 'add',
                                $value['Formulaire']['id']
                                ], ['class' => 'btn btn-outline-dark'])
                            .'</td>'
                        .'</tr>';
                    }
                    $body .= '</tbody>'
                .'</table>'
            .'</div>'
        .'</div>'
    .'</div>'
.'</div>';

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    .'</div>'
;

$content = [
    'title' => __d('default', 'default.popupChoisirFormulaire'),
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => 'modalAddTraitement',
    'content' => $content,
    'sizeModal' => 'modal-xl'
]);
