<?php
if ($this->Session->read('Su') === false) {
    $userEntiteEnCours = sprintf("%s - %s", $userCo['User']['nom_complet'], $this->Session->read('Organisation.raisonsociale'));
} else {
    if ($this->here === '/organisations/administrer') {
        $userEntiteEnCours = sprintf("%s - %s", $this->Session->read('Auth.User.nom_complet'), $this->Session->read('Organisation.raisonsociale'));
    } else {
        $userEntiteEnCours = $this->Session->read('Auth.User.nom_complet');
    }
}
?>

<nav class="navbar fixed-top navbar-expand-md navbar-ls navbar-dark bg-ls-dark">
    <?php
    $modalSelectOrganisations = false;
    if ($this->Session->read('Su') === false) {
        $logoAction = '/';
    } else {
        if (array_key_exists('Organisation', $this->Session->read()) === true) {
            $logoAction = '/organisations/administrer';
        } else {
            $logoAction = '/checks';
        }
    }
    ?>

    <li class="nav-item">
        <a class="navbar-brand" href="<?php echo $logoAction?>">
            <img src="/img/logo-white-blue.svg" title="Revenir à l'accueil de l'application" alt="<?php echo $cakeDescription?>">
        </a>
    </li>

    <div class="collapse navbar-collapse justify-content-end">
        <?php
        if ($this->Autorisation->authorized('30', $this->Session->read('Droit.liste')) &&
            $this->Session->read('Su') === false
        ) {
            ?>
            <a class="btn" title="<?php echo  __d('default', 'default.titleFAQ')?>"
               href="/articles/faq/<?php echo $this->Session->read('Organisation.id')?>">
                <i class="fas fa-question-circle fa-white"></i>
            </a>
            <?php
        }

        if (isset($userCo['User']['nom_complet'])) {
            $nbNotification = count($vuNotifications);

            if ($nbNotification >= 1) {
                ?>
                <a class="btn" href="#" data-toggle="modal" data-target="#modalNotification">
                    <i class="fa fa-bell fa-white"><?php echo $nbNotification;?></i>
                </a>
                <?php
            }
        }
        ?>

        <ul class="navbar-nav">
            <li  class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                >
                    <?php
                    echo $userEntiteEnCours;
                    ?>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <?php
                    if ($this->Session->read('Su') === false) {
                        ?>
                        <div class="dropdown-header">
                            <?php
                            echo __d('default', 'default.sousTitreEntite');
                            ?>
                        </div>

                        <?php
                        $listeMesOrganisation = [];
                        if (count($organisations) <= 5) {
                            foreach ($organisations as $datas) {
                                ?>
                                <a class="dropdown-item" href="/organisations/change/<?php echo $datas['Organisation']['id']?>">
                                    <span>
                                        <?php
                                        echo $datas['Organisation']['raisonsociale'];
                                        ?>
                                    </span>
                                </a>
                                <?php
                            }
                        } else {
                            $modalSelectOrganisations = true;
                            ?>

                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalChangeEntite">
                                <i class="fa fa-exchange"></i>
                                <span>
                                    <?php
                                    echo __d('pannel', 'pannel.champOrganisationcible');
                                    ?>
                                </span>
                            </a>

                            <?php
                            foreach ($organisations as $datas) {
                                $listeMesOrganisation[$datas['Organisation']['id']] = $datas['Organisation']['raisonsociale'];
                            }
                        }
                        ?>

                        <div class="dropdown-divider"></div>
                    <?php
                    }
                    ?>

                    <div class="dropdown-header">
                        <?php
                        echo __d('default', 'default.sousTitreCompte');
                        ?>
                    </div>

                    <a class="dropdown-item" href="/users/preferences">
                        <i class="fas fa-cogs"></i>
                        <span>
                            <?php
                            echo __d('default', 'default.sousTitreModifCompte');
                            ?>
                        </span>
                    </a>

                    <?php
                    if ($this->Session->read('Auth.User.ldap') === false) {
                        ?>
                        <a class="dropdown-item" href="/users/changepassword">
                            <i class="fa fa-lock"></i>
                            <span>
                                <?php
                                echo __d('default', 'default.sousTitreChangePassword');
                                ?>
                            </span>
                        </a>
                        <?php
                    }

                    if ($this->Session->read('Su') === false) {
                        ?>
                        <a class="dropdown-item"  href="/organisations/politiquedeconfidentialite/<?php echo $this->Session->read('Organisation.id')?>">
                            <i class="fa fa-user-secret"></i>
                            <span>
                                <?php
                                echo __d('default', 'default.sousTitrePolitiqueDeConfidentialite');
                                ?>
                            </span>
                        </a>
                        <?php
                    }
                    ?>

                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="/users/logout">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>
                            <?php
                            echo __d('default', 'default.sousTitreDeconnexion');
                            ?>
                        </span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<?php
if ($modalSelectOrganisations === true) {
    // Pop-up pour administrer une entité
    echo $this->element('Default/modalChangeEntite', [
        'listeMesOrganisation' => $listeMesOrganisation
    ]);
}
