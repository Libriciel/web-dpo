<?php
$acceptTypeDoc = isset($acceptTypeDoc) ? $acceptTypeDoc : 'application/vnd.oasis.opendocument.text';
$action = isset($action) ? $action : 'add';
$titleModal = isset($titleModal) ? $titleModal : __d('modele_extrait', 'modele_extrait.popupEnvoiModeleExtrait');
$labelField = isset($labelField) ? $labelField : __d('modele_extrait', 'modele_extrait.popupChampModeleExtrait');

$body = '<div class="row top17">'
        .'<div class="col-md-12">'
            . $this->WebcilForm->input($controller.'.entity_id', [
                'id' => 'entity_id',
                'class' => 'usersDeroulant form-control',
                'options' => $mesOrganisations,
                'empty' => true,
                'multiple' => true,
                'data-placeholder' => __d('responsable_soustraitant','responsable_soustraitant'.'.placeholderSelectOrganisation'),
                'label' => [
                    'text' => __d('responsable_soustraitant','responsable_soustraitant'.'.champSelectOrganisation'),
                ],
                'required' => true
            ])
            . $this->WebcilForm->input($controller, [
                'id' => 'modele',
                'div' => 'input-group inputsForm',
                'type' => 'file',
                'class' => 'filestyle',
                'accept' => $acceptTypeDoc,
                'label' => [
                    'text' => $labelField,
                    'class' => 'control-label'
                ],
                'required' => true
            ])
        .'</div>'
    .'</div>'
;

$footer = '<div class="buttons">'
    . $this->WebcilForm->button("<i class='fa fa-times-circle fa-lg'></i>" . __d('default', 'default.btnCancel'), [
        'type' => 'submit',
        'data-dismiss' => 'modal',
        'class' => 'btn btn-outline-primary',
        'escape' => false,
    ])
    . ' '
    . $this->WebcilForm->button("<i class='far fa-save fa-lg'></i>" . __d('default', 'default.btnSave'), [
        'id' => 'btnSaveModal',
        'type' => 'submit',
        'class' => 'btn btn-primary',
        'escape' => false,
    ])
    .'</div>'
;

$content = [
    'title' => $titleModal,
    'body' => $body,
    'footer' => $footer,
];

echo $this->element('modal', [
    'modalId' => $modal_id,
    'controller' => $controller,
    'action' => $action,
    'optionsForm' => [
        'type' => 'file'
    ],
    'content' => $content
]);
?>

<script type="text/javascript">

    $(document).ready(function () {

        hideAndRemoveAllFields();

        $("#entity_id").change(function () {
            if ($(this).val() != null) {
                $('#modele').parent().show();
            }
        });

        $("#modele").change(function () {
           if ($(this).val() != null) {
               $('#btnSaveModal').removeAttr('disabled');
           }
        });

    });

    function hideAndRemoveAllFields()
    {
        $('#btnSaveModal').attr('disabled', true);

        $("#modele").val('');
        $("#modele").filestyle('clear');
        $('#modele').parent().hide();
    }

</script>
