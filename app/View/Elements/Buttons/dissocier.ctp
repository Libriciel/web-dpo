<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'dissocier';
$id = isset($id) ? $id : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;
$confirmation = isset($confirmation) ? $confirmation : null;

$icon = "fa-unlink";

echo $this->element('Buttons/btn', [
    'icon' => $icon,
    'controller' => $controller,
    'action' => $action,
    'id' => $id,
    'titleBtn' => $titleBtn,
    'confirmation' => $confirmation,
    'classBtn' => 'btn-outline-danger borderless'
]);
