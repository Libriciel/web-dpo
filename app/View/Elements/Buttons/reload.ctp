<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'reload';
$id = isset($id) ? $id : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;

$icon = "fa-sync-alt";

echo $this->element('Buttons/btn', [
    'icon' => $icon,
    'controller' => $controller,
    'action' => $action,
    'id' => $id,
    'titleBtn' => $titleBtn,
    'classBtn' => 'btn-outline-dark borderless'
]);
