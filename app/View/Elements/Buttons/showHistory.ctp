<?php
$id = isset($id) ? $id : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;
$value = isset($value) ? $value : null;

$icon = "fa-history";

echo $this->Form->button('<i class="fas '.$icon.' fa-lg"></i>', [
    'id' => $id,
    'class' => 'btn boutonList btn-outline-primary borderless',
    'title' => $titleBtn,
    'escape' => false,
    'type' => 'button',
    'value' => $value
]);
