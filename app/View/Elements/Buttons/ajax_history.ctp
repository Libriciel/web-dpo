<?php
$id = isset($id) ? $id : null;

$icon = "fa-history";

echo $this->Form->button('<i class="fas '.$icon.' fa-lg"></i>', [
    'id' => 'btn_ajax_historique'.$id,
    'class' => 'btn boutonList btn-outline-primary borderless',
    'title' => __d('pannel', 'pannel.commentaireVoirParcours'),
    'escape' => false,
    'type' => 'button',
    'onclick' => "getHistory('$id')"
]);
?>

<script type="text/javascript">
    function getHistory(id) {
        let newElement = $('#ajax_list_history_'+id);

        if (newElement.is(":hidden") === true && newElement.is(':empty') === true) {
            $.ajax({
                url: '<?php echo Router::url(['controller' => 'Pannel', 'action' => 'ajax_get_history']);?>',
                method: 'POST',
                data: {fiche_id: id},
                success: function (data) {
                    newElement.append(data);
                    newElement.show();

                    $("#btn_full_history_"+id).click(function () {
                        $("#full_history_"+id).toggle();
                    });
                },
                error: function () {
                    alert('Erreur lors de la récupération de l\'historique du traitement');
                }
            });
        } else {
            if (newElement.is(":hidden") === true) {
                newElement.show();
            } else {
                newElement.hide();
            }
        }
    }
</script>