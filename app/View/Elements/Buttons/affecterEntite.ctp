<?php
$titleBtn = isset($titleBtn) ? $titleBtn : null;
?>

<fieldset>
    <div class="pull-left btnAffecterEntite" style="margin-left:25px">
        <button type="button" id="btnAffecterEntite" class="btn btn-outline-primary btn_affecterEntite"
                data-toggle="modal" data-target="#modalAddElementToEntity">
            <i class="fa fa-link fa-lg"></i>
            <?php
            echo $titleBtn;
            ?>
        </button>
    </div>
</fieldset>
