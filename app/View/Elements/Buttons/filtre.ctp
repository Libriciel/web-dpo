<?php
$id = isset($id) ? $id : 'filtrage';
$titleBtn = isset($titleBtn) ? $titleBtn : null;

echo $this->Form->button('<i class="fa fa-filter fa-lg"></i>' . $titleBtn, [
    'type' => 'button',
    'class' => 'btn btn-outline-dark float-right',
    'id' => $id
]);
