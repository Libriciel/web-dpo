<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'abroger';
$id = isset($id) ? $id : null;
$active = isset($active) ? $active : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;

if ($active === true) {
    $icon = "fa-toggle-off";
    $class = "btn-outline-success borderless";
} else {
    $icon = "fa-toggle-on";
    $class = "btn-outline-danger borderless";
}

echo $this->Html->link('<i class="fas '.$icon.' fa-lg"></i>', [
    'controller' => $controller,
    'action' => $action,
    $id,
    $active
        ], [
    'class' => 'btn ' . $class,
    'title' => $titleBtn,
    'escape' => false
]);
