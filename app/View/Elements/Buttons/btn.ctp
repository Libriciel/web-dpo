<?php
$classBtn = isset($classBtn) ? $classBtn : null;
$confirmation = isset($confirmation) ? $confirmation : null;
$labelBtn = isset($labelBtn) ? $labelBtn : null;
$id = isset($id) ? $id : null;
$before = isset($before) ? $before : null;
$after = isset($after) ? $after : null;

if (!empty($before)) {
    echo ($before);
}

echo $this->Html->link('<i class="fas '.$icon.' fa-lg"></i>' . $labelBtn, [
    'controller' => $controller,
    'action' => $action,
    $id
        ], [
    'class' => 'btn ' . $classBtn,
    'title' => $titleBtn,
    'escape' => false,
    'escapeTitle' => false
    ], $confirmation
);

if (!empty($after)) {
    echo ($after);
}
