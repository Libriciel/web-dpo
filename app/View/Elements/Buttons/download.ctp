<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'download';
$fichier = isset($fichier) ? $fichier : null;
$name_modele = isset($name_modele) ? $name_modele : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;
$labelBtn = isset($labelBtn) ? $labelBtn : null;

$icon = 'fa-download';

echo $this->Html->link('<i class="fas '.$icon.' fa-lg"></i>' . $labelBtn, [
    'controller' => $controller,
    'action' => $action,
    $fichier,
    $name_modele
        ], [
    'class' => 'btn btn-outline-primary borderless',
    'title' => $titleBtn,
    'escape' => false
]);
