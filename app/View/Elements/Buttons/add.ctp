<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'add';
$titleBtn = isset($titleBtn) ? $titleBtn : null;
$labelBtn = isset($labelBtn) ? $labelBtn : null;
$classBtn = isset($classBtn) ? $classBtn : 'btn-primary';
$before = isset($before) ? $before : '<div class="text-center">';
$after = isset($after) ? $after : '</div>';

$icon = "fa-plus-circle";

echo $this->element('Buttons/btn', [
    'icon' => $icon,
    'controller' => $controller,
    'action' => $action,
    'titleBtn' => $titleBtn,
    'classBtn' => $classBtn,
    'labelBtn' => $labelBtn,
    'before' => $before,
    'after' => $after,
]);
