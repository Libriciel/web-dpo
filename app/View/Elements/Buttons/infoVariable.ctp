<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : 'infoVariable';
$id = isset($id) ? $id : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;
$labelBtn = isset($labelBtn) ? $labelBtn : null;
$classBtn = isset($classBtn) ? $classBtn : 'btn-outline-dark borderless';

$icon = "fa-info-circle";

echo $this->element('Buttons/btn', [
    'icon' => $icon,
    'controller' => $controller,
    'action' => $action,
    'id' => $id,
    'labelBtn' => $labelBtn,
    'classBtn' => $classBtn,
    'titleBtn' => $titleBtn
]);
