<?php
$controller = isset($controller) ? $controller : null;
$action = isset($action) ? $action : null;
$id = isset($id) ? $id : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;

$icon = "fa-cog";

echo $this->element('Buttons/btn', [
    'icon' => $icon,
    'controller' => $controller,
    'action' => $action,
    'id' => $id,
    'titleBtn' => $titleBtn,
    'classBtn' => 'btn-outline-primary borderless'
]);
