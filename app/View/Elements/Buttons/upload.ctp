<?php
//$dataId = isset($dataId) ? $dataId : null;
$dataTarget = isset($dataTarget) ? $dataTarget : null;
$titleBtn = isset($titleBtn) ? $titleBtn : null;

$icon = 'fa-upload';

echo $this->Html->link('<i class="fas '.$icon.' fa-lg"></i>', ['#' => '#'], [
    'escape' => false,
    'data-toggle' => 'modal',
    'data-target' => $dataTarget,
    'class' => 'btn btn-outline-primary borderless',
    'title' => $titleBtn
]);
