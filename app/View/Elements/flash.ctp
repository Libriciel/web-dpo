<div class="toast fade show float-right <?php echo $classFlash; ?>" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header float-right borderless" style="background-color: transparent;">
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body">
    <div class="container-fluid" style="margin: 0; padding: 0;">
        <div class="row">
            <div class="col" style="padding-left: 0; padding-right: 0; max-width: 1rem;">
                <i class="fas <?php echo $icon; ?> fa-lg"></i>
            </div>
            <div class="col">
              <?php echo h($message);?>
            </div>

        </div>
    </div>
  </div>
</div>
