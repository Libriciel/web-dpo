<?php
echo $this->Form->button("<i class='fas fa-eye'>&nbsp;</i>" . __d('pannel','pannel.btnVoirHistoriqueComplet'), [
    'id' => 'btn_full_history_'.$id,
    'type' => 'button',
    'class' => 'btn btn-outline-dark',
]);

echo '<ul class="list-group list-historique " id="full_history_'.$id.'" >';

foreach ($historique as $value) {
    echo '<li class="list-group-item"><strong>' . $this->Time->format($value['Historique']['created'], FORMAT_DATE_HEURE) . ':</strong> ' . $value['Historique']['content'] . '</li>';
}

echo '</ul>';