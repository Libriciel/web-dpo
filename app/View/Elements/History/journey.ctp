<ul class="list-group list-parcours">
    <?php
    if (function_exists('_echoUserActionTimestamp') === false) {
        function _echoUserActionTimestamp($View, $nomComplet, $timestamp, $actionBy, $action) {
            if (empty($nomComplet) === false) {
                printf(' %s ', __d('element', $actionBy));
                printf(' <b>%s</b> ', $nomComplet);
            } else {
                printf(' %s ', __d('element', $action));
            }
            printf(' %s ', __d('element', __d('element', 'element.Le')));
            printf(' <b>%s</b> ', $View->Time->format($timestamp, FORMAT_DATE_HEURE));
        }
    }

    if (function_exists('_echoUserActionSimple') === false) {
        function _echoUserActionSimple($View, $nomComplet, $actionBy) {
            if (empty($nomComplet) === false) {
                printf(' %s ', __d('element', $actionBy));
                printf(' <b>%s</b> ', $nomComplet);
            }
        }
    }

    $cle = [];
    foreach ($parcours as $key => $test) {
        if ($test['EtatFiche']['etat_id'] == EtatFiche::DEMANDE_AVIS && $test['EtatFiche']['actif'] == false) {
            $cle[] = $key;
        }
    }
    foreach ($cle as $value) {
        unset($parcours[$value]);
    }

    $cle = [];
    foreach ($parcours as $key => $yaya) {
        if ($yaya['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_VALIDATION && $yaya['EtatFiche']['actif'] == false) {
            $cle[] = $key;
        }
    }
    foreach ($cle as $value) {
        unset($parcours[$value]);
    }

    foreach ($parcours as $value) {
        switch ($value['EtatFiche']['etat_id']) {
            //Rectangle bleu Rédaction
            case EtatFiche::ENCOURS_REDACTION:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.Redaction');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['Fiche']['created'],
                            'element.CreePar',
                            'element.Cree'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div  style="margin-left: 60px; ">
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $val['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            //Rectangle orange En attente de validation
            case EtatFiche::ENCOURS_VALIDATION:
                ?>
                <div class='bg-warning tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.AttenteValidation');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.RecuePar',
                            'element.Recue'
                        );
                        ?>
                    </div>
                    <?php
                    $idUserCommentaire = [];
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                if ($value['User']['id'] != $val['user_id'] && !in_array($val['user_id'], $idUserCommentaire)) {
                                    $idUserCommentaire[$val['user_id']] = $val['user_id'];
                                }
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $val['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>

                            <!--Bouton de réponse à un commentaire-->
                            <div class="row bottom10">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#AddCommentaire">
                                        <i class="fa fa-reply"></i>
                                        <?php
                                        echo __d('formulaire', 'formulaire.btnRepondreCommentaire');
                                        ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                echo $this->element('Pannel/modalAddCommentaire', [
                    'idUserCommentaire' => $idUserCommentaire,
                    'id' => $value['EtatFiche']['id'],
                    'fiche_id' => $value['EtatFiche']['fiche_id']
                ]);
                break;

            //Rectangle vert Validée
            case EtatFiche::VALIDER:
                ?>
                <div class='bg-success tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.Validee');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.ValideePar',
                            'element.Validee'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionSimple(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            'element.CommenterPar'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            // Rectangle rouge Refusée
            case EtatFiche::REFUSER:
                if (!empty($value['Commentaire'])) {
                    foreach ($value['Commentaire'] as $val) {
                        ?>
                        <div class='bg-danger tuilesStatuts col-md-10 col-md-offset-1'>
                            <div class='text-center'>
                                <h3>
                                    <b>
                                        <?php
                                        echo __d('element', 'element.Refusee');
                                        ?>
                                    </b>
                                </h3>
                            </div>
                            <div class='tuilesStatutsNom'>
                                <?php
                                _echoUserActionTimestamp(
                                    $this,
                                    Hash::get($val, 'User.nom_complet'),
                                    $value['Fiche']['created'],
                                    'element.RefuseePar',
                                    'element.Refusee'
                                );
                                ?>
                            </div>

                            <div>
                                <br/>
                                <hr class='hrComms'/>
                                <div class='text-center'>
                                    <h4>
                                        <?php
                                        echo __d('element', 'element.Commentaire');
                                        ?>
                                    </h4>
                                </div>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionSimple(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            'element.CommenterPar'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                            </div>
                        </div>
                        <?php
                    }
                }
                break;

            //Rectangle Validéé Inseréé dans le registre
            case EtatFiche::VALIDER_DPO:
                ?>
                <div class='bg-success tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.ValideeInsereeRegistre');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.ValideePar',
                            'element.Validee'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionSimple(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            'element.CommenterPar'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            //Rectangle orange En attente de consultation
            case EtatFiche::DEMANDE_AVIS:
                ?>
                <div class='bg-warning tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.AttenteConsultation');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.RecuePar',
                            'element.Recue'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionSimple(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            'element.CommenterPar'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            //Rectangle vert Archivée
            case EtatFiche::ARCHIVER:
                ?>
                <div class='bg-success tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.Verrouille');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.VerrouilleePar',
                            'element.Verrouillee'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionSimple(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            'element.CommenterPar'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            //Rectangle bleu Replacer en rédaction
            case EtatFiche::REPLACER_REDACTION:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.ReplacerRedaction');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.ReplacerRedactionPar',
                            'element.ReplacerRedaction'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div>
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $value['EtatFiche']['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            //Rectangle orange Modification du traitement inséré au registre
            case EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE:
                ?>
                <div class='bg-warning tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.ModificationTraitementRegistre');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.ModificationTraitementRegistrePar',
                            'element.ModificationTraitementRegistre'
                        );
                        ?>
                    </div>
                    <?php

                    if (!empty($value['Modification']['id'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.MotifModification');
                                    ?>
                                </h4>
                            </div>

                            <div>
                                <p>
                                    <?php
                                    echo $value['Modification']['modif'];
                                    ?>
                                </p>
                            </div>

                            <br/>
                            <hr class='hrComms'/>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            // Rectangle bleu Initialisation d'un traitement par le DPO
            // EtatFiche 11
            case EtatFiche::INITIALISATION_TRAITEMENT:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.Initialisation');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.CreePar',
                            'element.Le'
                        );
                        ?>
                    </div>
                </div>
                <?php
                break;

            // Rectangle bleu Traitement initialisé par le DPO, en attente de rédaction
            // EtatFiche 12
            case EtatFiche::TRAITEMENT_INITIALISE_RECU:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.TraitementInitialiseAttenteRedaction');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.EnvoyeePar',
                            'element.Le'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div  style="margin-left: 60px; ">
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $value['EtatFiche']['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            // Rectangle bleu Rédaction du traitement initialisé par le DPO
            // EtatFiche 13
            case EtatFiche::TRAITEMENT_INITIALISE_REDIGER:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.RedactionTraitementInitialisation');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.RedigerPar',
                            'element.Le'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div  style="margin-left: 60px; ">
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $value['EtatFiche']['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            case EtatFiche::TRAITEMENT_SEND_USER_SERVICE_DECLARANT:
                ?>
                <div class='bg-info tuilesStatuts col-md-10 col-md-offset-1'>
                    <div class='text-center'>
                        <h3>
                            <b>
                                <?php
                                echo __d('element', 'element.TraitementSendByUserServiceDeclarant');
                                ?>
                            </b>
                        </h3>
                    </div>
                    <div class='tuilesStatutsNom'>
                        <?php
                        _echoUserActionTimestamp(
                            $this,
                            Hash::get($value, 'User.nom_complet'),
                            $value['EtatFiche']['created'],
                            'element.SendBy',
                            'element.Le'
                        );
                        ?>
                    </div>
                    <?php
                    if (!empty($value['Commentaire'])) {
                        ?>
                        <div>
                            <br/>
                            <hr class='hrComms'/>
                            <div class='text-center'>
                                <h4>
                                    <?php
                                    echo __d('element', 'element.Commentaire');
                                    ?>
                                </h4>
                            </div>
                            <?php
                            foreach ($value['Commentaire'] as $val) {
                                ?>
                                <div  style="margin-left: 60px; ">
                                    <p>
                                        <?php
                                        echo $val['content'];
                                        ?>
                                    </p>
                                    <footer>
                                        <?php
                                        _echoUserActionTimestamp(
                                            $this,
                                            Hash::get($val, 'User.nom_complet'),
                                            $value['EtatFiche']['created'],
                                            'element.CommenterPar',
                                            'element.Commenter'
                                        );
                                        ?>
                                    </footer>
                                </div>
                                <br/>
                                <hr class='hrComms'/>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

            default:
                break;
        }
    }
    ?>
</ul>
