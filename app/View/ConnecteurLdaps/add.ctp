<?php
$breadcrumbs = [
    __d('connecteur', 'connecteur.titreGestionConnecteurs') => [
        'controller' => 'connecteurs',
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['ConnecteurLdap']) && !empty($this->validationErrors['User'])) {
    ?>
	<div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
            <span class="sr-only">Error:</span>
            Ces erreurs se sont produites:
            <ul>
                <?php
                foreach ($this->validationErrors as $donnees) {
                    foreach ($donnees as $champ) {
                        foreach ($champ as $error) {
                            echo '<li>' . $error . '</li>';
                        }
                    }
                }
                ?>
            </ul>
	</div>
    <?php
}

echo $this->WebcilForm->create('ConnecteurLdap',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
    'type' => 'file'
]);
?>

<h3>
    <?php
    echo __d('connecteur_ldap', 'connecteur_ldap.titreActivationLDAP');
    ?>
</h3>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->input('use', [
            'id' => 'use',
            'class' => 'form-control custom-select',
            'options' => [
                false => 'Non',
                true => 'Oui'
            ],
            'empty' => false,
            'required' => true,
            'placeholder' => false
        ]);
        ?>
    </div>
</div>

<fieldset id="ldap-block" class="ldap-block">
    <div class="row col-md-12">
        <div class="col-md-6">
            <h3>
                <?php
                echo __d('connecteur_ldap', 'connecteur_ldap.titreInformationConnexion');
                ?>
            </h3>

            <?php
            echo $this->WebcilForm->inputs([
                'id' => [
                    'type' => 'hidden'
                ],
                'type' => [
                    'id' => 'type',
                    'class' => 'form-control custom-select',
                    'options' => $options['ConnecteurLdap']['type'],
                    'empty' => false,
                    'required' => true,
                    'placeholder' => false
                ],
                'host' => [
                    'id' => 'host',
                    'required' => true
                ],
                'host_fall_over' => [
                    'id' => 'host_fall_over',
                ],
                'port' => [
                    'id' => 'port',
                    'type' => 'number',
                    'default' => '389',
                    'required' => true
                ],
                'login' => [
                    'id' => 'login',
                    'required' => true
                ],
                'password' => [
                    'id' => 'password',
                    'autocomplete' => 'off',
                    'required' => true
                ],
                'basedn' => [
                    'id' => 'basedn',
                    'required' => true
                ],
                'tls' => [
                    'id' => 'tls',
                    'class' => 'form-control custom-select',
                    'options' => [
                        false => 'Non',
                        true => 'Oui'
                    ],
                    'empty' => false,
                    'required' => true,
                    'placeholder' => false
                ],
                'version' => [
                    'id' => 'version',
                    'class' => 'form-control custom-select',
                    'options' => $options['ConnecteurLdap']['version'],
                    'default' => 3,
                    'empty' => false,
                    'required' => true,
                    'placeholder' => false
                ],
                'account_suffix' => [
                    'id' => 'account_suffix'
                ]
            ]);

            if (empty($this->request->data['ConnecteurLdap']['certificat_name'])) {
                echo $this->WebcilForm->input('certificat', [
                    'id' => 'certificat',
                    'type' => 'file',
                    'class' => 'filestyle',
                    'data-text' => __d('connecteur_ldap', 'connecteur_ldap.titleBtFileCertificat'),
                    'data-buttonBefore' => 'true',
                    'label' => [
                        'text' => false
                    ],
                    'accept' => [
                        'application/x-x509-ca-cert',
                        'application/pkix-cert'
                    ],
                ]);
            } else {
                ?>
                <table class="table" id="render">
                    <tbody>
                    <tr>
                        <td class="col-md-1">
                            <i class="far fa-file-alt fa-lg"></i>
                        </td>

                        <td class="col-md-9 tdleft">
                            <?php
                            echo $this->request->data['ConnecteurLdap']['certificat_name'];
                            ?>
                        </td>

                        <td class="col-md-2">
                            <?php
                            echo $this->Html->link('<span class="fa fa-trash fa-lg"><!----></span>', [
                                'controller' => 'connecteurLdaps',
                                'action' => 'deleteFile',
                                $this->request->data['ConnecteurLdap']['id'],
                                $this->request->data['ConnecteurLdap']['certificat_url'],
                            ], [
                                'class' => 'btn btn-outline-danger btn-sm my-tooltip',
                                'title' => __d('norme','norme.btnSupprimerFichierNorme'),
                                'escapeTitle' => false
                            ]);
                            ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </div>

        <div class="col-md-6">
            <h3>
                <?php
                echo __d('connecteur_ldap', 'connecteur_ldap.titreTableauConcordances');
                ?>
            </h3>

            <?php
            echo $this->WebcilForm->inputs([
                'username' => [
                    'id' => 'username',
                    'default' => 'samaccountname'
                ],
                'civilite' => [
                    'id' => 'civilite',
                    'default' => 'civilite'
                ],
                'note' => [
                    'id' => 'note',
                    'default' => 'info'
                ],
                'nom' => [
                    'id' => 'nom',
                    'default' => 'sn'
                ],
                'prenom' => [
                    'id' => 'prenom',
                    'default' => 'givenname'
                ],
                'email' => [
                    'id' => 'email',
                    'default' => 'mail'
                ],
                'telfixe' => [
                    'id' => 'telfixe',
                    'default' => 'telephonenumber'
                ],
                'telmobile' => [
                    'id' => 'telmobile',
                    'default' => 'mobile'
                ],
                'active' => [
                    'id' => 'active',
                    'placeholder' => false
                ]
            ]);
            ?>
        </div>
    </div>
</fieldset>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    $(document).ready(function () {

        $("#ldap-block").hide();
        
        <?php 
        if ($this->request->data('ConnecteurLdap.use') == true) {
            ?>
            $("#ldap-block").show();
            <?php
        }
        ?>
        
        $('#use').change(function() {
            let select = $(this),
                selected = $(select).val();

            if (selected === '1') {
                $("#ldap-block").show();
            } else {
                $("#ldap-block").hide();
            }
        });
        
        $('#type').change(function() {
            let select = $(this),
                selected = $(select).val();
                
            if (selected === 'OpenLDAP') {
                $("#username").val('uid');
            } else {
                $("#username").val('samaccountname');
            }
        });
        
    });
</script>
