<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if ($this->Autorisation->authorized(16, $droits)) {
    echo $this->element('Buttons/upload', [
        'dataTarget' => '#modalImportServices',
        'titleBtn' => __d('service', 'service.commentaireImportServices')
    ]);

    echo $this->element('Buttons/download', [
        'controller' => 'services',
        'titleBtn' => __d('service', 'service.commentaireTelechargerExempleImportServices')
    ]);

    // Bouton du filtre de la liste
    echo $this->element('Buttons/filtre', [
        'titleBtn' => __d('service', 'service.btnFiltrerService')
    ]);
}

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par service
            echo $this->Form->input('service', [
                'empty' => true,
                'class' => 'usersDeroulant form-control',
                'label' => __d('service', 'service.labelFiltreChercherService'),
                'data-placeholder' => __d('service', 'service.placeholderFiltreChercherService'),
                'options' => $options['services'],
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
if (!empty($serv)) {
    echo $this->element('Buttons/add', [
        'controller' => 'services',
        'labelBtn' => __d('service','service.btnAjouterService'),
        'before' => '<div class="text-left">',
    ]);

    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <fieldset>
        <div class="pull-left btnAffecterUsers" style="margin-left:25px">
            <button type="button" id="btnAffecterUsers" class="btn btn-outline-primary"
                    data-toggle="modal" data-target="#modalAddServicesToUsers"
            >
                <i class="fa fa-link fa-lg"></i>
                <?php
                echo __d('service', 'service.btnAffecterUsersServices');
                ?>
            </button>
        </div>
    </fieldset>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                    <input id="serviceUserCheckbox" type="checkbox" class="serviceUserCheckbox_checkbox"/>
                </th>

                <!-- Nom du service -->
                <th class="col-md-2">
                    <?php
                    echo __d('service', 'service.titreTableauNomService');
                    ?>
                </th>

                <!-- Entité -->
                <th class="col-md-4">
                    <?php
                    echo __d('service', 'service.textTableauOrganisation');
                    ?>
                </th>

                <!-- Nombre d'utilisateurs -->
                <th class="col-md-3">
                    <?php
                    echo __d('service', 'service.titreTableauNbUtilisateur');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('service', 'service.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($serv as $value) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer un responsable à une entité -->
                    <td class="tdleft col-md-1">
                        <input id='checkbox<?php echo $value['Service']['id']; ?>' type="checkbox"
                               class="serviceUserCheckbox" value="<?php echo $value['Service']['id']; ?>" >
                    </td>

                    <!-- Nom du service -->
                    <td class="tdleft col-md-2">
                        <?php
                        echo $value['Service']['libelle'];
                        ?>
                    </td>

                    <!-- Entité -->
                    <td class="tdleft col-md-4">
                        <?php
                        echo $this->Session->read('Organisation.raisonsociale');
                        ?>
                    </td>

                    <!-- Nombre d'utilisateurs -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $value['Service']['users_count'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if ($this->Autorisation->authorized(17, $droits) &&
                                $value['Service']['fiches_count'] == 0
                            ) {
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'services',
                                    'id' => $value['Service']['id'],
                                    'titleBtn' => __d('service', 'service.commentaireModifierService')
                                ]);
                            }

                            if ($this->Autorisation->authorized(9, $droits) &&
                                $value['Service']['users_count'] != 0
                            ) {
                                echo $this->element('Buttons/dissocier', [
                                    'controller' => 'services',
                                    'id' => $value['Service']['id'],
                                    'titleBtn' => __d('service', 'service.btnDissocier'),
                                    'confirmation' => sprintf( __d('service', 'service.confirmationDissocierService'), $value['Service']['libelle'])
                                ]);
                            }

                            if ($this->Autorisation->authorized(18, $droits) &&
                                $value['Service']['users_count'] == 0 &&
                                $value['Service']['fiches_count'] == 0
                            ) {
                                echo $this->element('Buttons/delete', [
                                    'controller' => 'services',
                                    'id' => $value['Service']['id'],
                                    'titleBtn' => __d('service', 'service.commentaireSupprimerService'),
                                    'confirmation' => sprintf( __d('service', 'service.confirmationSupprimerService'), $value['Service']['libelle'])
                                ]);
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('service', 'service.textAucunService');
            ?>
        </h3>
    </div>
    <?php
}

if ($this->Autorisation->authorized(16, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'services',
        'labelBtn' => __d('service', 'service.btnAjouterService'),
    ]);

    echo $this->element('Default/modalFileImport', [
        'titleModal' => __d('service', 'service.popupTitleImportListeService'),
        'labelField' => __d('service', 'service.popupFieldImportService'),
        'modal_id' => 'modalImportServices',
        'controller' => 'Service',
        'action' => 'import_services',
        'acceptTypeDoc' => 'text/csv'
    ]);
}

if ($this->Autorisation->authorized(9, $droits)) {
    echo $this->element('Default/modalAddServicesToUsers', [
        'formulaireName' => 'ServiceUser',
        'fieldName' => 'service_id',
        'btnAffecter' => 'btnAffecterUsers',
        'checkboxID' => 'serviceUserCheckbox'
    ]);
}
