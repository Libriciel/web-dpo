<?php
$breadcrumbs = [
    __d('service', 'service.titreService') . $this->Session->read('Organisation.raisonsociale') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

echo $this->WebcilForm->create('Service', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="row">
    <div class="col-md-10">
        <?php
        echo $this->WebcilForm->input('libelle', [
            'id' => 'libelle',
            'autocomplete' => 'off',
            'required' => true
        ]);
            
        echo $this->WebcilForm->hidden('organisation_id', [
            'value' => $this->Session->read('Organisation.id')
        ]);
        ?>
    </div>
</div>

<?php
// Groupe de boutons
echo $this->WebcilForm->buttons(['Cancel', 'Save']);
echo $this->WebcilForm->end();
