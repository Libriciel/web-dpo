<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('formulaire', 'formulaire.btnFiltrerFormulaires')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par entité associée
            echo $this->element('Filtres/Parametrages/organisation_associee', ['options' => $mesOrganisations]);

            // Filtrer par entité créatrice
            echo $this->element('Filtres/Parametrages/organisation_creatrice', ['options' => $mesOrganisations]);

            echo $this->element('Filtres/Parametrages/archive');
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par formulaire actif
            echo $this->element('Filtres/Parametrages/actif');

            // Filtrer par formulaire pour le compte d'un RT
            echo $this->element('Filtres/Parametrages/formulaire_rt');
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<!--Bouton de création d'un formulaire -->
<div class="pull-left">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddForm">
        <i class="fa fa-plus-circle fa-lg"></i>
        <?php
        echo __d('formulaire', 'formulaire.btnCreerFormulaire');
        ?>
    </button>
</div>

<?php
$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

echo $this->element('Buttons/affecterEntite', [
    'titleBtn' => __d('formulaire', 'formulaire.popupTitreAffecterFormulaire')
]);
?>

<!-- Tableau des formulaires -->
<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <!-- checkbox -->
            <th class="col-md-1">
                <input id="formulaireOrganisationCheckbox" type="checkbox" class="formulaireOrganisationCheckbox_checkbox"/>
            </th>

            <!-- Etat -->
            <th class="col-md-1">
                <?php
                echo __d('formulaire', 'formulaire.titreTableauEtat');
                ?>
            </th>

            <!-- Nom -->
            <th class="col-md-2">
                <?php
                echo __d('user', 'user.titreTableauNomDuFormulaire');
                ?>
            </th>

            <!-- Description -->
            <th class="col-md-2">
                <?php
                echo __d('user', 'user.titreTableauDescription');
                ?>
            </th>

            <!-- Sous-traitant pour le compte d'un RT -->
            <th class="col-md-1">
                <?php
                echo __d('formulaire', 'formulaire.titreTableauSoustraitantPourRT');
                ?>
            </th>

            <!-- Date de création -->
            <th class="col-md-1">
                <?php
                echo __d('user', 'user.titreTableauDateCreation');
                ?>
            </th>

            <!-- Entité -->
            <th class="col-md-2">
                <?php
                echo __d('typage', 'typage.titreTableauEntite');
                ?>
            </th>

            <!-- Actions -->
            <th class="col-md-2">
                <?php
                echo __d('user', 'user.titreTableauAction');
                ?>
            </th>
        </tr>
    </thead>
    
    <!-- Info tableau -->
    <tbody>
        <?php
        foreach ($formulaires as $data) {
            ?>
            <tr class="d-flex">
                <!-- Casse à coché pour associer un responsable à une entité -->
                <td class="tdleft col-md-1">
                    <?php
                    if ($data['Formulaire']['archive'] == false) {
                        ?>
                        <input id='checkbox<?php echo $data['Formulaire']['id']; ?>' type="checkbox"
                               class="formulaireOrganisationCheckbox" value="<?php echo $data['Formulaire']['id']; ?>" >
                        <?php
                    }
                    ?>
                </td>

                <!-- Status du formulaire -->
                <td class="tdleft col-md-1">
                    <?php
                    if ($data['Formulaire']['archive'] == false) {
                        if ($data['Formulaire']['active'] == true) {
                            $titleBtn = __d('formulaire', 'formulaire.commentaireDesactiverFormulaire');
                        } else {
                            $titleBtn = __d('formulaire', 'formulaire.commentaireActiverFormulaire');
                        }

                        echo $this->element('Buttons/toggle', [
                            'controller' => 'formulaires',
                            'id' => $data['Formulaire']['id'],
                            'active' => $data['Formulaire']['active'],
                            'titleBtn' => $titleBtn
                        ]);
                    }
                    ?>
                </td>
                
                <!-- Nom du formulaire -->
                <td class="tdleft col-md-2">
                    <?php
                    echo $data['Formulaire']['libelle'];
                    ?>
                </td>
                
                <!-- Description du formulaire -->
                <td class="tdleft col-md-2">
                    <?php
                    echo $data['Formulaire']['description'];
                    ?>
                </td>
                
                <!-- Sous-traitant pour le compte d'un RT -->
                <td class="tdleft col-md-1">
                    <?php
                    // Si le formulaire est pour les sous-traitants
                    $image = null;
                    if ($data['Formulaire']['rt_externe'] === true ){
                        $image = '<i class="fa fa-check fa-lg fa-success">oui</i>';
                    }

                    echo $image;
                    ?>
                </td>

                <!-- Date de création -->
                <td class="tdleft col-md-1">
                    <?php
                    echo $this->Time->format($data['Formulaire']['created'], FORMAT_DATE);
                    ?>
                </td>

                <!-- Entité -->
                <td class="tdleft col-md-2">
                    <?php
                    $organisations = Hash::extract($data, 'FormulaireOrganisation.{n}.Organisation.id');
                    if (empty($organisations) === false) {
                        echo '<ul><li>' . implode('</li><li>', Hash::extract($data, 'FormulaireOrganisation.{n}.Organisation.raisonsociale')) . '</li></ul>';
                    }
                    ?>
                </td>

                <!-- Action possible d'effectuer en fonction de l'état du formulaire -->
                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        // Bouton voir le formulaire
                        echo $this->element('Buttons/show', [
                            'controller' => 'formulaires',
                            'id' => $data['Formulaire']['id'],
                            'titleBtn' => __d('formulaire', 'formulaire.commentaireVoirFormulaire')
                        ]);

                        if ($data['Formulaire']['fiches_count'] === 0 &&
                            $data['Formulaire']['active'] == false &&
                            $data['Formulaire']['archive'] == false &&
                            $data['Formulaire']['createdbyorganisation'] == $this->Session->read('Organisation.id')
                        ){
                            // Bouton édité le formulaire
                            echo $this->element('Buttons/edit', [
                                'controller' => 'formulaires',
                                'id' => $data['Formulaire']['id'],
                                'titleBtn' => __d('formulaire', 'formulaire.commentaireModifierFormulaire')
                            ]);
                        }
                        ?>

                        <!--Bouton dupliqué le formulaire-->
                        <button type="button" class="btn btn_duplicate btn-outline-primary borderless"
                                data-toggle="modal" data-target="#modalDupliquer" value="<?php echo $data['Formulaire']['id']; ?>"
                                title="<?php echo __d('formulaire', 'formulaire.commentaireDupliquerFormulaire'); ?>">
                            <i class="fas fa-copy fa-lg"></i>
                        </button>

                        <?php
                        if ($data['Formulaire']['fiches_count'] === 0 &&
                            $data['Formulaire']['active'] == false &&
                            $data['Formulaire']['createdbyorganisation'] == $this->Session->read('Organisation.id') &&
                            empty($organisations)
                        ) {
                            // Bouton supprimé le formulaire
                            echo $this->element('Buttons/delete', [
                                'controller' => 'formulaires',
                                'id' => $data['Formulaire']['id'],
                                'titleBtn' => __d('formulaire', 'formulaire.commentaireSupprimerFormulaire'),
                                'confirmation' => __d('formulaire', 'formulaire.confirmationSupprimerFormulaire'),
                            ]);
                        }

                        if (empty($organisations) &&
                            $data['Formulaire']['active'] === false
                        ) {
                            if ($data['Formulaire']['archive'] == false) {
                                $iconArchive = 'fa-archive btn-outline-primary';
                                $titleArchive = __d('formulaire', 'formulaire.commentaireArchiverFormulaire');
                            } else {
                                $iconArchive = 'fa-redo-alt btn-outline-primary';
                                $titleArchive = __d('formulaire', 'formulaire.commentaireDearchiverFormulaire');
                            }

                            echo $this->element('Buttons/btn', [
                                'controller' => 'formulaires',
                                'action' => 'archive',
                                'id' => $data['Formulaire']['id'],
                                'icon' => $iconArchive,
                                'titleBtn' => $titleArchive
                            ]);
                        }

                        echo $this->element('Buttons/infoVariable', [
                            'controller' => 'modeles',
                            'id' => $data['Formulaire']['id'],
                            'titleBtn' => __d('modele', 'modele.commentaireVariableModel'),
                        ]);
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
echo $pagination;
?>

<!--Bouton de création d'un formulaire -->
<div class="row text-center">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddForm">
            <i class="fa fa-plus-circle fa-lg"></i>
            <?php
            echo __d('formulaire', 'formulaire.btnCreerFormulaire');
            ?>
        </button>
    </div>
</div>

<?php
echo $this->element('Formulaires/modal/modalAddForm');

echo $this->element('Formulaires/modal/modalDupliquer');

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Formulaire',
    'modalTitle' => __d('formulaire', 'formulaire.popupTitreAffecterFormulaire'),
    'formulaireName' => 'FormulaireOrganisation',
    'fieldName' => 'formulaire_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'formulaireOrganisationCheckbox'
]);
?>

<script type='text/javascript'>
    $(document).ready(function () {

        $(".btn_duplicate").click(function () {
            let valueId = $(this).val();
            $('#FormulaireId').val(valueId);
        });
        
    });
</script>
