<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;
?>

<!-- Tableau des formulaires -->
<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <!-- Etat -->
            <th class="col-md-1">
                <?php
                echo __d('formulaire', 'formulaire.titreTableauEtat');
                ?>
            </th>

            <!-- Nom -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauNomDuFormulaire');
                ?>
            </th>

            <!-- Description -->
            <th class="col-md-3">
                <?php
                echo __d('user', 'user.titreTableauDescription');
                ?>
            </th>

            <!-- Sous-traitant pour le compte d'un RT -->
            <th class="col-md-2">
                <?php
                echo __d('formulaire', 'formulaire.titreTableauSoustraitantPourRT');
                ?>
            </th>

            <!-- Date de création -->
            <th class="col-md-1">
                <?php
                echo __d('user', 'user.titreTableauDateCreation');
                ?>
            </th>

            <!-- Actions -->
            <th class="col-md-2">
                <?php
                echo __d('user', 'user.titreTableauAction');
                ?>
            </th>
        </tr>
    </thead>
    
    <!-- Info tableau -->
    <tbody>
        <?php
        foreach ($formulaires as $data) {
            $formulaireOrganisationActive = Hash::get($data['FormulaireOrganisation'][0], 'active');
            ?>
            <tr class="d-flex">
                <!-- Status du formulaire -->
                <td class="tdleft col-md-1">
                    <?php
                    if ($formulaireOrganisationActive == true) {
                        $titleBtn = __d('formulaire', 'formulaire.commentaireDesactiverFormulaire');
                    } else {
                        $titleBtn = __d('formulaire', 'formulaire.commentaireActiverFormulaire');
                    }

                    echo $this->element('Buttons/toggle', [
                        'controller' => 'formulaires',
                        'id' => $data['Formulaire']['id'],
                        'active' => $formulaireOrganisationActive,
                        'titleBtn' => $titleBtn
                    ]);
                    ?>
                </td>
                
                <!-- Nom du formulaire -->
                <td class="tdleft col-md-3">
                    <?php
                    echo $data['Formulaire']['libelle'];
                    ?>
                </td>
                
                <!-- Description du formulaire -->
                <td class="tdleft col-md-3">
                    <?php
                    echo $data['Formulaire']['description'];
                    ?>
                </td>
                
                <!-- Sous-traitant pour le compte d'un RT -->
                <td class="tdleft col-md-2">
                    <?php
                    // Si le formulaire est pour les sous-traitants
                    $image = null;
                    if ($data['Formulaire']['rt_externe'] === true ){
                        $image = '<i class="fa fa-check fa-lg fa-success">oui</i>';
                    }

                    echo $image;
                    ?>
                </td>

                <!-- Date de création -->
                <td class="tdleft col-md-1">
                    <?php
                    echo $this->Time->format($data['Formulaire']['created'], FORMAT_DATE);
                    ?>
                </td>

                <!-- Action possible d'effectuer en fonction de l'état du formulaire -->
                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        // Bouton voir le formulaire
                        echo $this->element('Buttons/show', [
                            'controller' => 'formulaires',
                            'id' => $data['Formulaire']['id'],
                            'titleBtn' => __d('formulaire', 'formulaire.commentaireVoirFormulaire')
                        ]);

                        echo $this->element('Buttons/dissocier', [
                            'controller' => 'formulaires',
                            'id' => $data['Formulaire']['id'],
                            'titleBtn' => __d('formulaire', 'formulaire.btnDissocier'),
                            'confirmation' => __d('formulaire', 'formulaire.confirmationDissocierFormulaire') . ' " ' . $data['Formulaire']['libelle'] . ' " de votre entité ?',
                        ]);
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
