<?php
echo $this->Html->script([
    'FormulaireGenerator/createFormulaire',
]);

echo $this->Html->css([
    'createFormulaire',
]);

$this->Breadcrumbs->breadcrumbs([
    __d('formulaire', 'formulaire.titreListeFormulaire') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

echo $this->WebcilForm->create('Formulaire', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);

$classActiveInfoGenerale = 'active';
$classActivePia = '';
if ($formulaireOLD === false) {
    $classActiveInfoGenerale = '';
    $classActivePia = 'active';
}
?>

<div id="tabs" class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <?php
        if ($formulaireOLD === false) {
            ?>
            <li class="nav-item">
                <a class="nav-link <?php echo $classActivePia ?>" data-toggle="tab" href="#info_pia">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletPia');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#info_rt">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletInformationResponsableTraitement');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link <?php echo $classActiveInfoGenerale ?>" data-toggle="tab" href="#information_traitement">
                <i class="fa fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletInformationTraitement');
                ?>
            </a>
        </li>

        <?php
        if ($formulaireOLD === false) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#info_complementaire">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletInformationComplementaire');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#info_formulaire">
                <i class="fa fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletFormulaire');
                ?>
            </a>
        </li>

        <!-- Co-responsable -->
        <?php
        if ($formulaireOLD === false) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ongletComplementaireCoresponsable">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletCoresponsable');
                    ?>
                </a>
            </li>
            <?php
        } else {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ongletCoresponsable">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletCoresponsable');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <!-- Sous-traitance -->
        <?php
        if ($formulaireOLD === false) {
            if ($rt_externe === true) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#onglet_st_organisation">
                        <i class="fa fa-eye fa-fw"></i>
                        <?php
                        echo __d('fiche', 'fiche.ongletST');
                        ?>
                    </a>
                </li>
                <?php
            }
            ?>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#ongletComplementaireSousTraitant">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    if ($rt_externe === false) {
                        echo __d('fiche', 'fiche.ongletSousTraitant');
                    } else {
                        echo __d('fiche', 'fiche.ongletSousTraitanceUlterieur');
                    }
                    ?>
                </a>
            </li>
            <?php
        } else {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#soustraitant">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletSousTraitant');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fa fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletAnnexe');
                ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <?php
        if ($formulaireOLD === false) {
            echo $this->element('Formulaires/tabs/pia', [
                'classActivePia' => $classActivePia
            ]);

            if ($rt_externe === true) {
                echo $this->element('Formulaires/tabs/rt');
            }

            // Onglet Information générale concernant le traitement
            echo $this->element('Formulaires/tabs/informationGenerale', [
                'classActiveInfoGenerale' => $classActiveInfoGenerale
            ]);

            echo $this->element('Formulaires/tabs/informationComplementaire');
        } else {
            echo $this->element('Formulaires/oldOnglets/ongletShowInformationGenerale');
        }
        ?>

        <div id="info_formulaire" class="tab-pane">
            <br/>

            <div id="form-container-formulaire" class="form-container col-md-12">
                <?php
                echo $this->element('Formulaires/champDuFormulaire', [
                    'champs' => $this->request->data['Formulaire']['form-container-formulaire']
                ]);
                ?>
            </div>
        </div>

        <?php
        // Co-responsable
        if ($formulaireOLD === false) {
            ?>
            <div id="ongletComplementaireCoresponsable" class="tab-pane">
                <br/>
                <div id="form-container-coresponsable" class="form-container col-md-12">
                    <?php
                    echo $this->element('Formulaires/champDuFormulaire', [
                        'champs' => $this->request->data['Formulaire']['form-container-coresponsable']
                    ]);
                    ?>
                </div>
            </div>
            <?php
        } else {
            // Onglet OLD Co-responsable
            echo $this->element('Formulaires/oldOnglets/ongletShowCoresponsable');
            // Fin onglet OLD Co-responsable
        }

        // Sous-traitance
        if ($formulaireOLD === false) {
            if ($rt_externe === true) {
                echo $this->element('Fiches/tabs/st_organisation');
            }
            ?>
            <div id="ongletComplementaireSousTraitant" class="tab-pane">
                <br/>
                <div id="form-container-soustraitant" class="form-container col-md-12">
                    <?php
                    echo $this->element('Formulaires/champDuFormulaire', [
                        'champs' => $this->request->data['Formulaire']['form-container-soustraitant']
                    ]);
                    ?>
                </div>
            </div>
            <?php
        } else {
            // Onglet OLD soustraitance
            echo $this->element('Formulaires/oldOnglets/ongletShowSoustraitance');
            // Fin onglet OLD soustraitance
        }

        if ($formulaireOLD === false) {
            // Onglet Annexe
            echo $this->element('Formulaires/tabs/annexe');
            // Fin onglet annexe
        } else {
            // Onglet OLD Annexe
            echo $this->element('Formulaires/oldOnglets/ongletShowAnnexe');
            // Fin onglet OLD annexe
        }
        ?>
    </div>

    <!--Bontou Retour-->
    <div class="row">
        <div class="col-md-12 top17 text-center">
            <div class="buttons">
                <?php
                echo $this->Html->link('<i class="fa fa-arrow-left fa-lg"></i>' . __d('fiche', 'fiche.btnRevenir'), $referer, [
                    'class' => 'btn btn-outline-primary',
                    'escape' => false
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    $(document).ready(function () {

        $(":input").prop("disabled", true);

        createForm('formulaire');
        createForm('coresponsable');
        createForm('soustraitant');

        $('.popoverText').popover({
            delay: {
                show: 500,
                hide: 100
            },
            placement: 'right',
            trigger: 'hover',
            container: 'body',
            html: true,
        });

    });

</script>
