<?php
echo $this->Html->script([
    'FormulaireGenerator/createFormulaire',
    'FormulaireGenerator/saveFieldsFormulaire',
    'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min',
    'smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js',
]);

echo $this->Html->css([
    'createFormulaire',
    '/js/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min'
]);

$this->Breadcrumbs->breadcrumbs([
    __d('formulaire', 'formulaire.titreListeFormulaire') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

if (isset($this->validationErrors['Formulaire']) && !empty($this->validationErrors['Formulaire'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="fa fa-exclamation-circle fa-fw" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites sur les champs suivants :
        <ul>
            <?php
            $champ = null;
            foreach ($this->validationErrors['Formulaire'] as $key => $errorChamps) {
                foreach ($errorChamps as $error) {
                    if ($champ === null) {
                        $champ = __d('formulaire', 'formulaire.champ'.ucfirst($key));
                    }

                    echo '<li>' . $champ . /*' : ' . $error .*/ '</li>';
                }

                $champ = null;
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Formulaire', [
    'id' => 'addAndEditForm',
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#info_pia">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletPia');
                ?>
            </a>
        </li>

        <?php
        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#info_rt">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                    echo __d('fiche', 'fiche.ongletInformationResponsableTraitement');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#information_traitement">
                <i class="fa fa-eye fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletInformationTraitement');
                ?>
            </a>
        </li>

        <li class="nav-item" id="onglet_info_complementaire">
            <a class="nav-link" data-toggle="tab" href="#info_complementaire">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletInformationComplementaire');
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#info_formulaire">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletFormulaire');
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ongletComplementaireCoresponsable">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletCoresponsable');
                ?>
            </a>
        </li>

        <?php
        if ($rt_externe === true) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_st_organisation">
                    <i class="fa fa-eye fa-fw"></i>
                    <?php
                        echo __d('fiche', 'fiche.ongletST');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ongletComplementaireSousTraitant">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                if ($rt_externe === false) {
                    echo __d('fiche', 'fiche.ongletSousTraitant');
                } else {
                    echo __d('fiche', 'fiche.ongletSousTraitanceUlterieur');
                }
                ?>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#annexe">
                <i class="fa fa-pencil-alt fa-fw"></i>
                <?php
                echo __d('fiche', 'fiche.ongletAnnexe');
                ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <?php
        echo $this->element('Formulaires/tabs/pia', [
            'classActivePia' => 'active'
        ]);

        if ($rt_externe === true) {
            echo $this->element('Formulaires/tabs/rt');
        }

        // Onglet Information générale concernant le traitement
        echo $this->element('Formulaires/tabs/informationGenerale', [
            'classActiveInfoGenerale' => ''
        ]);
        // Fin de l'onglet Information générale concernant le traitement

        echo $this->element('Formulaires/tabs/informationComplementaire');

        echo $this->element('Formulaires/tabs/formulaire');

        echo $this->element('Formulaires/tabs/coResponsable');

        if ($rt_externe === true) {
            echo $this->element('Fiches/tabs/st_organisation');
        }

        echo $this->element('Formulaires/tabs/sousTraitant');

        // Onglet Annexe
        echo $this->element('Formulaires/tabs/annexe');
        // Fin onglet annexe
        ?>
    </div>

    <?php
    echo $this->WebcilForm->buttons(['Cancel', 'Save']);
    ?>
</div>

<?php
echo $this->element('Formulaires/modal/modalAddCondition');
echo $this->element('Formulaires/modal/modalWarningDeleteConditions');

echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    $(window).scrollTop(0);

    $(document).ready(function () {

        $('.nav-tabs li').click(function () {
            $('.ui-selected').removeClass('ui-selected');
            $('.field-options>div').remove();
        });

        // Mise en évidence des onglets ayant des erreurs.
        $('div.form-group .invalid-feedback').closest('div.tab-pane').each(function(idx, pane) {
            let a = $( "a[href='#"+$(pane).attr('id')+"']" );
            $(a).addClass("is-invalid");
            $(a).append("<i class='fa fa-exclamation-circle fa-danger' aria-hidden='true'></i>");
        });

        $('.popoverText').popover({
            delay: {
                show: 500,
                hide: 100
            },
            placement: 'right',
            trigger: 'hover',
            container: 'body',
            html: true,
        });

        let timeoutId = window.setInterval(function(){
            saveAuto();
        }, 5000);

        function saveAuto() {
            let url = window.location.pathname,
                form_id = url.substring(url.lastIndexOf('/') + 1);

            if (form_id.length !== 0 ) {
                let formSaveAuto = $("#addAndEditForm"),
                    dynamicFieldsSaveAuto = getVirtualFields(false);

                $.each(dynamicFieldsSaveAuto, function(idx, hidden) {
                    formSaveAuto.append(hidden);
                });

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/formulaires/ajax_save_auto_formulaire/'+form_id,
                    data: formSaveAuto.serialize(),
                    // success : function(data)
                    // {
                        // alert(data);
                        // alert($.parseJSON(data.responseText).val);
                    // }
                });
            }
        }

    });

</script>
