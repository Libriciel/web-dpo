<?php
echo $this->Html->script([
    'pannel.js',
    'filtre.js'
]);
echo $this->Html->css('filtre.css');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

// balise du scrollTo
$idFicheNotification = $this->Session->read('idFicheNotification');
unset($_SESSION['idFicheNotification']);

// Filtrer la liste
// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('registre', 'registre.btnFiltrerListe')
]);

$filters = $this->request->data;
unset($filters['sort'], $filters['direction'], $filters['page'], $filters['Registre']['nbAffichage']);
if (empty($filters['Registre'])) {
    unset($filters['Registre']);
}

echo $this->element('Filtres/filtresTraitements', [
    'nameForm' => 'Registre',
    'showForRegistre' => true,
]);
?>

</br>

<?php
if (!empty($fichesValid)) {
    if ($this->Autorisation->authorized('7', $this->Session->read('Droit.liste'))) {

        $this->Paginator->options([
            'url' => Hash::flatten( (array)$this->request->data, '.' )
        ]);
        $pagination = $this->element('pagination');

        echo $pagination;
        ?>

        <fieldset>
            <div class="row col-md-12">
                <div class="pull-left" style="min-width:30em">
                    <?php
                    echo $this->WebcilForm->input('action', [
                        'id' => 'action',
                        'options' => $optionsAction,
                        'class' => 'form-control usersDeroulant pull-left',
                        'label' => [
                            'text' => __d( 'registre', 'registre.placeholderSelectionnerAction' ),
                            'class' => 'sr-only'
                        ],
                        'data-placeholder' => __d( 'registre', 'registre.placeholderSelectionnerAction' ),
                        'empty' => true,
                        'style' => 'min-width:16em',
                    ]);
                    ?>
                </div>

                <i class="fa fa-arrow-right pull-left" style="margin-left:30px; margin-top: 7px"></i>

                <div class="pull-left btnExecuter" style="margin-left:25px">
                    <?php
                    echo $this->Form->button('<i class="fa fa-cogs fa-lg"></i>' . 'Exécuter', [
                        'class' => 'btn btn-primary pull-left disabled'
                    ]);
                    ?>
                </div>

                <?php
                if ($this->Autorisation->authorized('7', $this->Session->read('Droit.liste'))) {
                    ?>
                    <!--Bouton qui permet de choisir l'ordre de generation pour généré l'extrait de registre  -->
                    <div class="pull-left btnExtraitRegistre" style="margin-left:25px">
                        <button type="button" class="btn btn-primary btn_generer" data-toggle="modal" data-target="#modalChangerPosition">
                            <i class="fa fa-cogs fa-lg"></i>
                            <?php
                            echo __d('registre', 'registre.btnGenerer');
                            ?>
                        </button>
                    </div>
                    <?php
                }
                ?>

                <?php
                if ($organisationActuelle['Organisation']['dpo'] == $this->Session->read('Auth.User.id')) {
                    ?>
                    <!--Bouton qui permet de choisir l'ordre de generation pour généré l'extrait de registre  -->
                    <div class="pull-left btnTraitementRegistreNonVerrouiller" style="margin-left:25px">
                        <button type="button" class="btn btn-primary btn_telechargerNonVerrouiller" data-toggle="modal" data-target="#modalChangerPosition">
                            <i class="fa fa-cogs fa-lg"></i>
                            <?php
                            echo __d('registre', 'registre.btnGenerer');
                            ?>
                        </button>
                    </div>

                    <!--Bouton pour l'export csv  -->
                    <div class="pull-left btnExportCsv" style="margin-left:25px">
                        <button type="button" class="btn btn-primary btn_exportCsv" data-toggle="modal" data-target="#modalChangerPosition">
                            <i class="fa fa-download fa-lg"></i>
                            <?php
                            echo __d('registre', 'registre.btnExport');
                            ?>
                        </button>
                    </div>

                    <!--Bouton pour l'export csv  -->
                    <div class="pull-left btnExtraitRegistreHtml" style="margin-left:25px">
                        <button type="button" class="btn btn-primary btn_genererExtraitRegistreHtml" data-toggle="modal" data-target="#modalChangerPosition">
                            <i class="fa fa-download fa-lg"></i>
                            <?php
                            echo __d('registre', 'registre.btnGenerer');
                            ?>
                        </button>
                    </div>
                    <?php
                }
                ?>
            </div>
        </fieldset>

        <br/>
        <?php
    }
    ?>

    <table class="table table-hover">
        <thead>
            <tr>
                <th class="col-md-1">
                    <?php
                    if ($this->Autorisation->authorized('7', $this->Session->read('Droit.liste'))) {
                        ?>
                        <!-- checkbox extrait registre -->
                        <input id="extraitRegistreCheckbox" type="checkbox" class = "extraitRegistreCheckbox_checkbox" />
                        <?php
                    }
                    ?>
                </th>

                <!-- Nom du traitement -->
                <th class="col-md-2">
                    <?php
                    echo __d('registre', 'registre.titreTableauNomTraitement');
                    ?>
                </th>

                <!-- Finalité principale -->
                <th class="col-md-3">
                    <?php
                    echo __d('registre', 'registre.textTableauFinalitePrincipale');
                    ?>
                </th>

                <th class="col-md-2">
                    <?php
                    echo __d('registre', 'registre.titreTableauSyntheseDonnees');
                    ?>
                </th>

                <!-- Synthèse -->
                <th class="col-md-2">
                    <?php
                    echo __d('registre', 'registre.titreTableauSynthese');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('registre', 'registre.titreTableauOutil');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            $idExtrait = [];
            foreach ($fichesValid as $key => $value) {
//                if ($value['EtatFiche']['etat_id'] != EtatFiche::ARCHIVER || $organisationActuelle['Organisation']['verrouiller'] == false) {
//                    $iconBtn = $this->Html->tag('i','<!---->',['class' => 'fa fa-cog fa-lg']);
//                    $titleBtn =  __d('registre', 'registre.commentaireGenererRegistrePDF');
//                    $DlOrGenerate = 'genereTraitement';
//                    $idExtrait = json_encode([$value['Fiche']['id']]);
//                }

                if ($value['Fiche']['Valeur'] != null) {
                    ?>
                    <tr>
                        <td class="tdleft">
                        <?php
                        if ($this->Autorisation->authorized('7', $this->Session->read('Droit.liste'))) {
                            ?>
                            <!-- Casse à coché pour télécharger les extraits de registre -->
                            <input type="checkbox" class="extraitRegistreCheckbox" id="<?php echo $value['Fiche']['id']; ?>" >
                            <?php
                        }
                        ?>
                        </td>

                        <!-- Nom du traitement -->
                        <td class="tdleft">
                            <?php
                            echo $value['Fiche']['Valeur']['outilnom'];
                            ?>
                        </td>

                        <!-- Finalité principale -->
                        <td class="tdleft">
                            <?php
                            echo nl2br($value['Fiche']['Valeur']['finaliteprincipale']);
                            ?>
                        </td>

                        <td class="tdleft">
                            <?php
                            if ($value['Fiche']['transfert_hors_ue'] === true) {
                                $etatTransfertHorsUe = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                            } else {
                                $etatTransfertHorsUe = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauTransfertHorsUe');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $etatTransfertHorsUe;
                                    ?>
                                </div>
                            </div>

                            <?php
                            if ($value['Fiche']['donnees_sensibles'] === true) {
                                $etatDonneesSensibles = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                            } else {
                                $etatDonneesSensibles = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauDonneesSensibles');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $etatDonneesSensibles;
                                    ?>
                                </div>
                            </div>

                            <?php
                            if ($value['Fiche']['coresponsable'] === true) {
                                $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                            } else {
                                $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauCoresponsable');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $etatCoresponsable;
                                    ?>
                                </div>
                            </div>

                            <?php
                            if ($value['Fiche']['soustraitance'] === true) {
                                $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                            } else {
                                $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauSousTraitance');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $etatCoresponsable;
                                    ?>
                                </div>
                            </div>

                            <?php
                            if (!is_null($value['Fiche']['obligation_pia'])) {
                                if ($value['Fiche']['obligation_pia'] === true) {
                                    $etatObligationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                                } else {
                                    $etatObligationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>
                                            <?php
                                            echo __d('registre', 'registre.textTableauObligationPIA');
                                            ?>
                                        </strong>
                                        <?php
                                        echo $etatObligationPIA;
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }

                            if (!is_null($value['Fiche']['realisation_pia'])) {
                                if ($value['Fiche']['realisation_pia'] === true) {
                                    $etatRealisationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                                } else {
                                    $etatRealisationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>
                                            <?php
                                            echo __d('registre', 'registre.textTableauRealisationPIA');
                                            ?>
                                        </strong>
                                        <?php
                                        echo $etatRealisationPIA;
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }

                            if (!is_null($value['Fiche']['realisation_pia'])) {
                                if ($value['Fiche']['depot_pia'] === true) {
                                    $etatDepotPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                                } else {
                                    $etatDepotPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>
                                            <?php
                                            echo __d('registre', 'registre.textTableauDepotPIA');
                                            ?>
                                        </strong>
                                        <?php
                                        echo $etatDepotPIA;
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </td>

                        <td class="tdleft">
                            <!-- Numéro -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauNumeroEnregistrement');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $value['Fiche']['numero'];
                                    ?>
                                </div>
                            </div>

                            <!-- Date de création -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauDateCreation');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $this->Time->format($value['Fiche']['created'], FORMAT_DATE_HEURE)
                                    ?>
                                </div>
                            </div>

                            <!-- Date de dernière modification -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('pannel', 'pannel.motDerniereModification');
                                        ?>
                                    </strong>
                                    <?php
                                    echo $this->Time->format($value['Fiche']['modified'], FORMAT_DATE_HEURE)
                                    ?>
                                </div>
                            </div>

                            <!-- Service déclarant -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauServiceDeclarant');
                                        ?>
                                    </strong>
                                    <?php
                                    $declarantServiceTraitement = 'Aucun service';
                                    if (!empty($value['Fiche']['Service']['libelle'])) {
                                        $declarantServiceTraitement = $value['Fiche']['Service']['libelle'];
                                    }

                                    echo $declarantServiceTraitement;
                                    ?>
                                </div>
                            </div>

                            <!-- Norme -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php echo __d('registre', 'registre.textTableauNorme'); ?>
                                    </strong>
                                        <?php
                                            if (isset($value['Fiche']['Norme']['norme'])) {
                                                $title = Hash::get($enumsNormes, "Norme.norme.{$value['Fiche']['Norme']['norme']}");
                                                $abbr = $this->Html->tag('abbr', $value['Fiche']['Norme']['norme'], ['title' => $title]);

                                                $titleDescription = strip_tags($value['Fiche']['Norme']['description'], '<br />');

                                                $numeroNorme = sprintf('%03d', $value['Fiche']['Norme']['numero']);
                                                $abbrNumero = $this->Html->tag('abbr', $numeroNorme, ['title' => $titleDescription]);

                                                $norme = ' ' . $abbr . '-' . $abbrNumero . ' : ' . $value['Fiche']['Norme']['libelle'];

                                                echo $norme;

                                            } else {
                                                echo __d('registre', 'registre.textAucuneNormeDefini');
                                            }
                                        ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php echo __d('registre', 'registre.textTableauReferentiel'); ?>
                                    </strong>
                                    <?php
                                    if (isset($value['Fiche']['Referentiel']['name'])) {
                                        echo $value['Fiche']['Referentiel']['name'];
                                    } else {
                                        echo __d('registre', 'registre.textAucunReferentielDefini');
                                    }
                                    ?>
                                </div>
                            </div>

                            <!-- Annexe(s) -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>
                                        <?php
                                        echo __d('registre', 'registre.textTableauAnnexe');
                                        ?>
                                    </strong>
                                        <?php
                                        if (!empty($value['Fiche']['Fichier'])) {
                                            ?>
                                                <div class="dropdown">
                                                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="false">
                                                        Liste des annexes :
                                                        <i class="badge badge-light">
                                                            <?php
                                                            echo count($value['Fiche']['Fichier']);
                                                            ?>
                                                        </i>
                                                        <i class="caret"></i>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                        <?php
                                                            foreach ($value['Fiche']['Fichier'] as $fichier) {
                                                                ?>
                                                                <li role="presentation">
                                                                    <?php
                                                                    echo $this->element('Buttons/download', [
                                                                        'controller' => 'fiches',
                                                                        'fichier' => $fichier['url'],
                                                                        'name_modele' => $fichier['nom'],
                                                                        'titleBtn' => __d('registre', 'registre.commentaireTelechargerAnnexe'),
                                                                        'labelBtn' => $fichier['nom']
                                                                    ]);
                                                                    ?>
                                                                </li>
                                                                <?php
                                                            }
                                                        ?>
                                                    </ul>
                                                </div>
                                            <?php
                                        } else {
                                            echo "Aucune annexe";
                                        }
                                        ?>
                                </div>
                            </div>
                        </td>

                        <!-- Actions -->
                        <td class="tdleft">
                            <div id="fiche<?php echo $value['Fiche']['id']; ?>'" class="buttons">
                                <?php
                                if ($this->Autorisation->authorized('7', $this->Session->read('Droit.liste'))) {
                                    // Bouton de téléchargement du traitement en PDF
//                                        echo $this->Html->link($iconBtn, [
//                                            'controller' => 'fiches',
//                                            'action' => $DlOrGenerate,
//                                            $idExtrait
//                                            ], [
//                                                'escape' => false,
//                                                'class' => 'btn btn-outline-dark btn-sm my-tooltip',
//                                                'title' => $titleBtn
//                                            ]
//                                        );

                                    echo $this->element('Buttons/generer', [
                                        'controller' => 'fiches',
                                        'action' => 'genereTraitement',
                                        'id' => $value['Fiche']['id'],
                                        'titleBtn' => __d('registre', 'registre.commentaireGenererRegistrePDF')
                                    ]);
                                }

                                // Bouton pour visualiser le traitement
                                echo $this->element('Buttons/show', [
                                    'controller' => 'fiches',
                                    'id' => $value['Fiche']['id'],
                                    'titleBtn' => __d('registre', 'registre.commentaireVoirTraitement')
                                ]);

                                // Bouton de visualisation de l'historique du traitement
                                echo $this->element('Buttons/ajax_history', [
                                    'id' => $value['Fiche']['id']
                                ]);

                                if ($this->Autorisation->isDpo()) {
                                    if ($value['EtatFiche']['etat_id'] != EtatFiche::ARCHIVER) {
                                        // Bouton de modification du traitement
                                        echo $this->Form->button('<i class="fas fa-pencil-alt fa-lg"></i>', [
                                            'id' => $value['Fiche']['id'],
                                            'class' => 'btn btn-outline-primary borderless btn-edit-registre modif_traitement',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#modalEditRegistre',
                                            'title' => __d('registre', 'registre.commentaireModifierTraitement')
                                        ]);
                                    }
                                }

                                if ($this->Autorisation->authorized('36', $this->Session->read('Droit.liste'))) {
                                    echo $this->Form->button('<i class="fas fa-copy fa-lg"></i>', [
                                        'class' => 'btn btn_duplicate btn-outline-primary borderless',
                                        'escape' => false,
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modalDupliquerTraitement',
                                        'title' => __d('pannel', 'pannel.commentaireDupliquerTraitement'),
                                        'data-id' => $value['Fiche']['id'],
                                        'value' => $value['Fiche']['id']
                                    ]);
                                }
                                ?>
                            </div>
                        </td>
                    </tr>

                    <tr class='listeValidation' id='ajax_list_history_<?php echo $value['Fiche']['id']; ?>'></tr>

                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('registre', 'registre.textAucunTraitementRegistre')
                : __d('registre', 'registre.textAucunTraitementFiltre');
            ?>
        </h3>
    </div>
    <?php
}

echo $this->element('Registres/modal/modalEditRegistre');

echo $pagination;

echo $this->element('Registres/modal/modalChangerPosition');

if ($this->Autorisation->authorized('36', $this->Session->read('Droit.liste'))) {
    echo $this->element('Fiches/modal/modalDupliquerTraitement');
}
?>

<script type="text/javascript">
    //<![CDATA[
    let currentTraitementId = 0;

    $(document).ready(function () {

        $(".extraitRegistreCheckbox").change(function() {
            if (this.checked) {
                $(this).closest('tr').addClass('trChecked');
            } else {
                $(this).closest('tr').removeClass('trChecked');
            }
        });

        openTarget("<?php echo $idFicheNotification ?>");

        // Par défaut, on cache toutes les cases à cocher
        // @fixme Désactiver ?
        $('.extraitRegistreCheckbox')
            .attr('disabled', true)
            .attr('checked', false);

        $('#extraitRegistreCheckbox')
            .attr('checked', false);

        $('.btnExtraitRegistre').hide();
        $('.btnTraitementRegistre').hide();
        $('.btnTraitementRegistreNonVerrouiller').hide();
        $('.btnExportCsv').hide();
        $('.btnExtraitRegistreHtml').hide();

        // Lors d'action sur une checkbox : extraitRegistreCheckbox
        $("#extraitRegistreCheckbox").change(function () {
            $(".extraitRegistreCheckbox").not(':disabled').prop('checked', $(this).prop('checked'));

            if ($(this).prop('checked') && $(".extraitRegistreCheckbox").is(":not(:disabled)") === true) {
                $(".extraitRegistreCheckbox").closest('tr').addClass('trChecked');
            } else {
                $(".extraitRegistreCheckbox").closest('tr').removeClass('trChecked');
            }
        });

        // Checkbox -> extraitRegistreCheckbox
        // @fixme . (class) pas # (id)
        $('input[type="checkbox"]').not("#extraitRegistreCheckbox").change(function () {
            $('#extraitRegistreCheckbox').prop('checked', $('input[type="checkbox"]').not('#extraitRegistreCheckbox').not(':disabled').not(':checked').length === 0);
        });

        $('.modif_traitement').click(function () {
            currentTraitementId = $(this).attr('id');
        });

        $('#RegistreEditForm').submit(function (el) {
            $("#toModif").val(currentTraitementId);
            return true;
        });

        $("#action").change(function (evt ,element) {

            if (!element) {
                $('.extraitRegistreCheckbox')
                    .attr('disabled', true)
                    .attr('checked', false);

                $('#extraitRegistreCheckbox')
                    .attr('checked', false);

                $('.btnExecuter').show();
                $('.btnExtraitRegistre').hide();
                $('.btnTraitementRegistre').hide();
                $('.btnTraitementRegistreNonVerrouiller').hide();
                $('.btnExportCsv').hide();
                $('.btnExtraitRegistreHtml').hide();
                return;
            }

            let action = element.selected;

            // $('.extraitRegistreCheckbox')
            //     .attr('disabled', true)
            //     .attr('checked', false);
            //
            // $('#extraitRegistreCheckbox')
            //     .attr('checked', false);
            //
            // $('.btnExecuter').show();
            // $('.btnExtraitRegistre').hide();
            // $('.btnTraitementRegistre').hide();
            // $('.btnTraitementRegistreNonVerrouiller').hide();
            // $('.btnExportCsv').hide();

            $('.extraitRegistreCheckbox')
                .attr('disabled', false)
                .attr('checked', false);

            $('#extraitRegistreCheckbox')
                .attr('checked', false);

            $('.btnExecuter').hide();
            $('.btnExtraitRegistre').hide();
            $('.btnTraitementRegistre').hide();
            $('.btnTraitementRegistreNonVerrouiller').hide();
            $('.btnExportCsv').hide();
            $('.btnExtraitRegistreHtml').hide();

            if (action == 0) {
                $('.btnExtraitRegistre').show();
            } else if (action == 1) {
                $('a.boutonArchive')
                    .closest('tr')
                    .find('.extraitRegistreCheckbox')
                    .attr('disabled', true);

                $('.btnTraitementRegistre').show();
            } else if (action == 2) {
                $('.btnTraitementRegistreNonVerrouiller').show();
            } else if (action == 3) {
               // $("#formulaire").attr('disabled', false).trigger("chosen:updated");
                $('.btnExportCsv').show();
            } else if (action == 4) {
               // $("#formulaire").attr('disabled', false).trigger("chosen:updated");
                $('.btnExtraitRegistreHtml').show();
            }
        });
    });

    /**
     * Cree en fonction du nombre de traitement selectionner un tableau d'option
     * pour le menu déroulant avec pour chaque traitement une position pres
     * selectionne
     *
     * @param {int} nbTraitement
     * @param {int} numSelected
     * @returns {Array|createOption.optionPosition}
     */
    function createOption(nbTraitement, numSelected)
    {
        let optionPosition = [];

        for (let i = 1; i< nbTraitement + 1; i++) {
            if (i === numSelected) {
                optionPosition.push('<option value="'+i+'" selected="selected">'+i+'<\/option>');
            } else {
                optionPosition.push('<option value="'+i+'">'+i+'<\/option>');
            }
        }

        return (optionPosition);
    }

    $(".btn_generer").click(function () {
        $(".btn_popupGenerer").show();
        $(".btn_popupTelecharger").hide();
        $(".btn_popupGenererNonVerrouiller").hide();
        $(".btn_popupExportCsv").hide();
        $(".btn_popupGenererExtraitRegistreHtml").hide();
    });

    $(".btn_telecharger").click(function () {
        $(".btn_popupGenerer").hide();
        $(".btn_popupTelecharger").show();
        $(".btn_popupGenererNonVerrouiller").hide();
        $(".btn_popupExportCsv").hide();
        $(".btn_popupGenererExtraitRegistreHtml").hide();
    });

    $(".btn_telechargerNonVerrouiller").click(function () {
        $(".btn_popupGenerer").hide();
        $(".btn_popupTelecharger").hide();
        $(".btn_popupGenererNonVerrouiller").show();
        $(".btn_popupExportCsv").hide();
        $(".btn_popupGenererExtraitRegistreHtml").hide();
    });

    $(".btn_exportCsv").click(function () {
        $(".btn_popupGenerer").hide();
        $(".btn_popupTelecharger").hide();
        $(".btn_popupGenererNonVerrouiller").hide();
        $(".btn_popupExportCsv").show();
        $(".btn_popupGenererExtraitRegistreHtml").hide();
    });

    $(".btn_genererExtraitRegistreHtml").click(function () {
        $(".btn_popupGenerer").hide();
        $(".btn_popupTelecharger").hide();
        $(".btn_popupGenererNonVerrouiller").hide();
        $(".btn_popupExportCsv").hide();
        $(".btn_popupGenererExtraitRegistreHtml").show();
    });

    $('#modalChangerPosition').on('shown.bs.modal', function () {
        let selectedList = [],
            traitement = [],
            nbTraitement = 0,
            oldPosition = 0,
            fichesValid = <?php echo json_encode($fichesValid); ?>;

        $("#render > tbody > tr").remove();

        $(".extraitRegistreCheckbox").each(function () {
            if (this.checked) {
                selectedList.push(this.id);
            }
        });

        nbTraitement = selectedList.length;

        for (let key = 0; key < fichesValid.length; key++) {
            for (let selectkey = 0; selectkey < nbTraitement; selectkey++) {
                if (parseInt(fichesValid[key]['Fiche']['id'], 10) === parseInt(selectedList[selectkey], 10)) {
                    traitement.push(fichesValid[key]);
                }
            }
        }

        for (let key = 0; key < nbTraitement; key++) {
            let tr = $('<tr id="'+selectedList[key]+'"><\/tr>')
                .append('<td class="tdleft col-md-1"><select class="form-control optionSelected" required="required">' + createOption(nbTraitement, key+1) + '<\/select><\/td>')
                .append('<td class="tdleft col-md-1">'+traitement[key]['Fiche']['numero']+'<\/td>')
                .append('<td class="tdleft col-md-10">'+traitement[key]['Fiche']['Valeur']['outilnom']+'<\/td>')

            $('#render').find('tbody').append(tr);
        }

        $(".optionSelected").click(function () {
            oldPosition =  parseInt($(this).val(), 10);
        });

        $(".optionSelected").change(function () {
            let newPosition = parseInt($(this).val(), 10),
                row = $(this).closest("tr"),
                table = $('#render > tbody'),
                nRow = table.children('tr').eq(newPosition-1);

            if (oldPosition !== newPosition){
                if (oldPosition > newPosition) {
                    nRow.before(row);
                } else {
                    nRow.after(row);
                }

                reordonnerPosition();
            }
        });

    });

    function reordonnerPosition()
    {
        let table = $('#render > tbody'),
            nbRow = table.children('tr').length;

        for (let i = 0; i < nbRow; i++) {
            let newValue = i + 1,
                nRow = table.children('tr').eq(i),
                row = nRow.children('td').children('select');

            row.val(newValue).attr('selected', true);
        }
    }

    function sendDataExtrait()
    {
        window.location.href = send("<?php echo Router::url(['controller' => 'fiches', 'action' => 'genereExtraitRegistre']); ?>");
    }

    function sendDataExtraitHtml()
    {
        window.location.href = send("<?php echo Router::url(['controller' => 'fiches', 'action' => 'genereExtraitRegistreHtml']); ?>");
    }

    function sendData()
    {
        window.location.href = send("<?php echo Router::url(['controller' => 'registres', 'action' => 'imprimer']); ?>");
    }

    function sendDataDeclaration()
    {
        window.location.href = send("<?php echo Router::url(['controller' => 'fiches', 'action' => 'genereTraitementNonVerrouiller']); ?>");
    }

    function sendDataExport()
    {
        window.location.href = send("<?php echo Router::url(['controller' => 'fiches', 'action' => 'export']); ?>");
    }

    function send (url)
    {
        let selectedList = [],
            table = $('#render > tbody'),
            nbRow = table.children('tr').length;

        for (let i = 0; i < nbRow; i++) {
            let nRow = table.children('tr').eq(i),
                idRow = nRow.attr('id');

            selectedList.push(idRow);
        }

        return (url + '/' + JSON.stringify(selectedList));
    }

    //]]>
</script>
