<?php
//debug($data['traitement']);
//die;
$summary = '';
$main = '';
$traitements = '';

foreach ($data['traitement'] as $key => $value) {
    $a = $this->Html->tag('a', $value['valeur_outilnom']['value'], ['href' => "#traitement-{$key}"]);
    $summary .= $this->Html->tag('div', $a, ['class' => "line"]);

    if (!empty($value['valeur_norme']['value']) && !empty($value['valeur_normelibelle']['value'])) {
        $valueNorme = $value['valeur_norme']['value'] . ' : ' . $value['valeur_normelibelle']['value'];
    } else {
        $valueNorme = '';
    }

    $tr = $this->Html->tableCells([
        [
            ["Finalité principale", ['class' => 'tdleft col-md-4']],
            [$value['valeur_finaliteprincipale']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Numéro d’enregistrement", ['class' => 'tdleft col-md-4']],
            [$value['valeur_numeroenregistrement']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Service déclarant", ['class' => 'tdleft col-md-4']],
            [$value['valeur_declarantservice']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Norme", ['class' => 'tdleft col-md-4']],
            [$valueNorme, ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Description de la norme", ['class' => 'tdleft col-md-4']],
            [$value['valeur_normedescription']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Transfert hors de l'Union européenne", ['class' => 'tdleft col-md-4']],
            [$value['valeur_transfert_hors_ue']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Données sensibles", ['class' => 'tdleft col-md-4']],
            [$value['valeur_donnees_sensibles']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Obligation PIA", ['class' => 'tdleft col-md-4']],
            [$value['valeur_obligation_pia']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Réalisation PIA", ['class' => 'tdleft col-md-4']],
            [$value['valeur_realisation_pia']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Dépot PIA", ['class' => 'tdleft col-md-4']],
            [$value['valeur_depot_pia']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Co-responsable sur le traitement", ['class' => 'tdleft col-md-4']],
            [$value['valeur_coresponsable']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Sous-traitance sur le traitement", ['class' => 'tdleft col-md-4']],
            [$value['valeur_soustraitance']['value'], ['class' => 'tdleft col-md-8']],
        ],
        [
            ["Date de dernière modification", ['class' => 'tdleft col-md-4']],
            [$this->Time->format($value['valeur_fichemodified']['value'], FORMAT_DATE_HEURE), ['class' => 'tdleft col-md-8']],
        ],
    ], ['class' => 'd-flex'], ['class' => 'd-flex'], false);
    $table = $this->Html->tag('table', $tr, ['class' => 'table table-striped table-hover table-bordered']);

    $titleController = $this->Html->tag('div', 'Registre des traitements mis en oeuvre par : ' . $data['organisation_raisonsociale']['value'], ['class' => "controller"]);
    $title = $this->Html->tag('div',
        $this->Html->tag('div',
            $this->Html->tag('a',
                '⮹',
                [
                    'href' => "#top",
                    'class' => 'btn',
                    'style' => 'font-size: smaller',
                    'role' => 'button',
                    'title'=>"Retourner au sommaire"
                ]
            ),
            ['class' => "float-right"]
        ) . $value['valeur_outilnom']['value'],
        ['class' => "title"]
    );

    $register = $this->Html->tag('div', $titleController.$title, ['class' => "register"]);
    $traitements = $traitements . $this->Html->tag('div', $register . $table, ['class' => "main", 'id '=> "traitement-{$key}"]);
}
?>

<div class="main">
    <b>
        <?php
        echo 'Coordonnées de l’organisme :';
        ?>
    </b>
    <p></p>
    <ul>
        <?php
        echo $data['organisation_raisonsociale']['value'];
        ?>

        <p></p>
        <?php
        echo $data['organisation_adresse']['value'];
        ?>

        <p></p>
        <?php
        echo sprintf(__d('rgpd', 'rgpd.textNumeroSIRET'),
            $data['organisation_siret']['value']
        );
        ?>

        <p></p>
        <?php
        echo sprintf(__d('rgpd', 'rgpd.textCodeAPE'),
            $data['organisation_ape']['value']
        );
        ?>

        <p></p>
        <?php
        echo sprintf(__d('rgpd', 'rgpd.textNumeroTel'),
            $data['organisation_telephone']['value']
        );
        ?>

        <p></p>
        <?php
        echo sprintf(__d('rgpd', 'rgpd.textEmail'),
            $data['organisation_email']['value']
        );
        ?>
    </ul>

    <b>
        <?php
        echo __d('rgpd', 'rgpd.sousTitreResponsableTraitements');
        ?>
    </b>
    <p></p>
    <ul>
        <?php
        echo $data['organisation_responsable_nom_complet']['value'];

        echo '<br>';

        echo $data['organisation_fonctionresponsable']['value'];
        ?>
    </ul>

    <b>
        <?php
        echo __d('rgpd', 'rgpd.sousTitreDPOEntité');
        ?>
    </b>
    <p></p>
    <ul>
        <?php
        echo $data['dpo_nom_complet']['value'];
        ?>
        <p></p>
        <?php
        echo $data['dpo_emaildpo']['value'];
        ?>
    </ul>
</div>

<div class="main" id="top">
    <div class="title" id="summary">
        Liste des traitements au registre
    </div>

    <b>
        Avertissement:
    </b>

    la tenue du présent registre s'inscrit dans une démarche de revue dynamique des traitements relevant de la responsabilité de l'oganisation : <b><?php echo $data['organisation_raisonsociale']['value'];?></b>.
    Son contenu (description des activités de traitement et fiches associées à chacune de ces activités intégrant les informations prévues aux articles 30, 13, 14 et 15 du RGPD) est susceptible d'être mises à jour régulièrement.

    <br>

    <div class="register-summary">
        <?php
        echo $summary;
        ?>
    </div>
</div>

<?php
echo $traitements;
?>
