<?php
$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
?>

<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <th class="col-md-10">
                <?php 
                echo __d('connecteur', 'connecteur.titreTableauConnecteur');
                ?>
            </th>
            
            <th class="col-md-2">
                <?php 
                echo __d('connecteur', 'connecteur.titreTableauActions');
                ?>
            </th>
        </tr>
    </thead>
    
    <!-- Info tableau -->
    <tbody>
        <?php
        foreach ($connecteurs as $connecteur){
            ?>
            <tr class="d-flex">
                <td class="tdleft col-md-10">
                    <?php
                    echo $connecteur['connecteur'];
                    ?>
                </td>

                <!-- Actions -->
                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        echo $this->element('Buttons/edit', [
                            'controller' => $connecteur['controller'],
                            'action' => $connecteur['action'],
                            'titleBtn' => __d('connecteur', 'connecteur.commentaireModifierConnecteur') . $connecteur['connecteur'],
                        ]);
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
