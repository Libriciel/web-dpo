<?php 
    echo $this->WebcilForm->input($model.'.ModelGroup', [
        'class' => 'form-control usersDeroulant',
        'options' => $groups,
        'empty' => true, 
        'multiple' => true,
        'required' => true,
        'data-placeholder' => 'Choisir un groupe LDAP'
    ]);