<?php
echo $this->Html->script('pannel.js');

$params = ['limit' => 5];

// balise du scrollTo
$idFicheNotification = $this->Session->read('idFicheNotification');
unset($_SESSION['idFicheNotification']);

// Banette mes déclarations en cours de rédaction
if (true === isset($banettes['encours_redaction'])) {
    echo $this->Banettes->encoursRedaction($banettes['encours_redaction'], $params);
}

if (true === isset($banettes['service'])) {
    echo $this->Banettes->partageServices($banettes['service'], $params);
}

// Banette mes déclarations en attente
if (true === isset($banettes['attente'])) {
    echo $this->Banettes->attente($banettes['attente'], $params);
}

// Banette mes déclarations refusées
if (true === isset($banettes['refuser'])) {
    echo $this->Banettes->refuser($banettes['refuser'], $params);
}

// Banette traitements reçus pour validation
if (true === isset($banettes['recuValidation'])) {
    echo $this->Banettes->recuValidation($banettes['recuValidation'], $params);
}

// Banette traitements reçus pour consultation
if (true === isset($banettes['recuConsultation'])) {
    echo $this->Banettes->recuConsultation($banettes['recuConsultation'], $params);
}

// Pop-up envoie consultation
echo $this->element('Pannel/modalEnvoieConsultation');

// Pop-up envoie validation
echo $this->element('Pannel/modalEnvoieValidation');

// Pop-up reorientation du traitement
echo $this->element('Pannel/modalReorienter');

// Pop-up inséré traitement registre
echo $this->element('Pannel/modalValidDpo');
