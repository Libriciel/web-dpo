<div class="d-flex align-items-start">
    <ul class="nav flex-column nav-pills me-3" role="tablist">
        <?php
        if ($this->Autorisation->authorized(12, $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#onglet_generale">
                    <?php
                    echo __d('default', 'default.titreGenerale');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized([8, 9, 10, 13, 14, 15, 16, 17, 18], $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_administrationUser">
                    <?php
                    echo __d('default', 'default.titreAdministrationUser');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized(32, $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_typages">
                    <?php
                    echo __d('default', 'default.titreTypages');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized([28, 29, 30, 31], $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_faq">
                    <?php
                    echo __d('default', 'default.titreFAQ');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized([19, 21, 22], $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_normes">
                    <?php
                    echo __d('default', 'default.titreNormes');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized(23, $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_sousTraitant">
                    <?php
                    echo __d('default', 'default.titreSousTraitant');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized(24, $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_responsable">
                    <?php
                    echo __d('default', 'default.titreResponsable');
                    ?>
                </a>
            </li>
            <?php
        }

        if ($this->Autorisation->authorized(25, $droits)) {
            ?>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#onglet_maintenance">
                    <?php
                    echo __d('default', 'default.titreMaintenance');
                    ?>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>

    <div class="container-fluid">
        <div class="tab-content">
            <?php
            if ($this->Autorisation->authorized(12, $droits)) {
                ?>
                <div id="onglet_generale" class="tab-pane fade show active" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/organisations/edit/<?php echo $this->Session->read('Organisation.id');?>">
                            <div class="fa fa-building fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreGeneraleEntite');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/organisations/politiquepassword/">
                            <div class="fa fa-lock fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitrePassword');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized([8, 9, 10, 13, 14, 15, 16, 17, 18], $droits)) {
                ?>
                <div id="onglet_administrationUser" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <?php
                        if ($this->Autorisation->authorized([13, 14, 15], $droits)) {
                            ?>
                            <a type="button" class="btn btn-default-menu" href="/roles/index/">
                                <div class="fa fa-users fa-primary fa-2x"></div>

                                <div>
                                    <?php
                                    echo __d('default', 'default.sousTitreProfils');
                                    ?>
                                </div>
                            </a>
                            <?php
                        }

                        if ($this->Autorisation->authorized([16, 17, 18], $droits)) {
                            ?>
                            <a type="button" class="btn btn-default-menu" href="/services/index/">
                                <div class="fa fa-sitemap fa-primary fa-2x"></div>

                                <div>
                                    <?php
                                    echo __d('default', 'default.sousTitreServices');
                                    ?>
                                </div>
                            </a>
                            <?php
                        }

                        if ($this->Autorisation->authorized([8, 9, 10], $droits)) {
                            ?>
                            <a type="button" class="btn btn-default-menu" href="/users/index/">
                                <div class="fa fa-user fa-primary fa-2x"></div>

                                <div>
                                    <?php
                                    echo __d('default', 'default.sousTitreUser');
                                    ?>
                                </div>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized(32, $droits)) {
                ?>
                <div id="onglet_typages" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/typages/index/">
                            <div class="fa fa-list-alt fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreTypagesAnnexes');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/typages/entite/">
                            <div class="fa fa-link fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreMesTypesAnnexes');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized([28, 29, 30, 31], $droits)) {
                ?>
                <div id="onglet_faq" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/articles/index/">
                            <div class="fa fa-list-alt fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreToutesLaFAQ');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/articles/entite/">
                            <div class="fa fa-link fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreMaFAQ');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized([19, 21, 22], $droits)) {
                ?>
                <div id="onglet_normes" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/normes/index/">
                            <div class="fa fa-certificate fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreLesNormes');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized(23, $droits)) {
                ?>
                <div id="onglet_sousTraitant" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/soustraitants/index/">
                            <div class="fa fa-list-alt fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreLesSousTraitants');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/soustraitants/entite/">
                            <div class="fa fa-link fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreMesSousTraitants');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized(24, $droits)) {
                ?>
                <div id="onglet_responsable" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/responsables/index/">
                            <div class="fa fa-list-alt fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreListeResponsable');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/responsables/entite/">
                            <div class="fa fa-link fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreMesResponsables');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }

            if ($this->Autorisation->authorized(25, $droits)) {
                ?>
                <div id="onglet_maintenance" class="tab-pane fade" role="tabpanel">
                    <div class="col-md-12">
                        <a type="button" class="btn btn-default-menu" href="/connecteurs/index/">
                            <div class="fa fa-plug fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreConnecteurs');
                                ?>
                            </div>
                        </a>

                        <a type="button" class="btn btn-default-menu" href="/crons/index/">
                            <div class="fa fa-clock fa-primary fa-2x"></div>

                            <div>
                                <?php
                                echo __d('default', 'default.sousTitreTachesAutomatiques');
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
