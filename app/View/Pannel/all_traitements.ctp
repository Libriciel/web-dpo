<?php
echo $this->Html->script([
    'pannel.js',
    'filtre.js'
]);
echo $this->Html->css('filtre.css');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('pannel', 'pannel.btnFiltreAllTraitements')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<br>
<br>
<br>

<?php
echo $this->element('Filtres/filtresTraitements', [
    'nameForm' => 'Filtre',
]);

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

echo $this->element('Fiches/modal/modalDupliquerTraitement');

$params = [
    'mesOrganisations' => $mesOrganisations
];

// Banette mes déclarations en cours d'initialisation
echo $this->Banettes->allTraitement(
    $banettes['allTraitements'],
    $params
);

echo $pagination;
