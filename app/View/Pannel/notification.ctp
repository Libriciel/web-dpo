<?php
if (!empty($vuNotifications)) {
    $body = '';

    // Modal de notification
    $organisationId = $this->Session->read('Organisation.id');
    $this->Organisation = new Organisation();
    foreach ($vuNotifications as $key => $value) {
        if ($this->Session->read('Organisation.id') == $value['Fiche']['organisation_id']) {
            switch ($value['Notification']['content']) {
                case Notification::DEMANDE_AVIS:
                    // Demande d'avis
                    $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/recuConsultation/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationAvisDemandeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    break;
                case Notification::VALIDATION_DEMANDEE:
                    // Validation demandée
                    $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/recuValidation/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationValidationDemandeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    break;
                case Notification::TRAITEMENT_VALIDE:
                    // Traitement validé
                    $body .= '<a href="/organisations/changenotification/' . $organisationId . '/registres/index/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-success">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementValidee').'</a>';
                    break;
                case Notification::TRAITEMENT_REFUSE:
                    // Traitement refusé
                    $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/refuser/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-danger">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementRefusee').'</a>';
                    break;
                case Notification::COMMENTAIRE_AJOUTE:
                    // Commentaire ajouté sur le traitement
//                    if ($this->Autorisation->authorized(ListeDroit::VISER_TRAITEMENT, $this->Session->read('Droit.liste'))) {
//                        $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/consulte/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationCommentaireAjouterTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
//                    } else {
                        $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/encours_redaction/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationCommentaireAjouterTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
//                    }
                    break;
                case Notification::TRAITEMENT_INITIALISE:
                    // Traitement initialisé par le DPO
                    $body .= '<a href="/organisations/changenotification/' . $organisationId . '/pannel/encours_redaction/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementInitialise').'</a>';
                    break;
            }
        }
    }

    $footer = '<div class="buttons">'
            .'<button type="button" class="btn btn-outline-primary" data-dismiss="modal">'
                .'<i class="fa fa-times-circle fa-lg"></i>'
                . __d('default', 'default.btnCancel')
            .'</button>'
        .'</div>'
    ;

    $content = [
        'title' => 'Notifications',
        'body' => $body,
        'footer' => $footer,
    ];

    echo $this->element('modal', [
        'modalId' => 'modalNotification',
        'content' => $content
    ]);
}

if (!empty($notifications)){
    // Modal de notification
    $arrayNotificationNotVuNotAfficher = [];

    foreach ($notifications as $key => $value) {
        if ($value['Notification']['vu'] == false && $value['Notification']['afficher'] == false) {
            array_push($arrayNotificationNotVuNotAfficher, $value['Notification']['fiche_id']);
        }
    }

    if (!empty($notifications) && !empty($arrayNotificationNotVuNotAfficher)) {
        $this->Organisation = new Organisation();

        echo '<div class="modal fade" id="modalNotif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Nouvelles notifications</h4>
                </div>
            <div class="modal-body">';

        $oldmairie = '';

        foreach ($notifications as $key => $value) {
            $mairie = $nameOrganisation[$key]['Organisation']['raisonsociale'];

            if ($oldmairie != $mairie) {
                echo '<div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel">' . $mairie . '</h5>
                        </div>';
            }

            switch ($value['Notification']['content']) {
                case Notification::DEMANDE_AVIS:
                    // Demande d'avis
                    echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/recuConsultation/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationAvisDemandeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    break;
                case Notification::VALIDATION_DEMANDEE:
                    // Validation demandée
                    echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/recuValidation/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationValidationDemandeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    break;
                case Notification::TRAITEMENT_VALIDE:
                    // Traitement validé
                    echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/registres/index/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-success">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementValidee').'</a>';
                    break;
                case Notification::TRAITEMENT_REFUSE:
                    // Traitement refusé
                    echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/refuser/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-danger">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementRefusee').'</a>';
                    break;
                case Notification::COMMENTAIRE_AJOUTE:
                    // Commentaire ajouté sur le traitement
                    if ($this->Autorisation->authorized('3', $this->Session->read('Droit.liste'))) {
                        echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/consulte/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationCommentaireAjouterTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    } else if ($this->Autorisation->authorized('2', $this->Session->read('Droit.liste'))) {
                        echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/recuValidation/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationCommentaireAjouterTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    } else {
                        echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/encours_redaction/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationCommentaireAjouterTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong></a>';
                    }
                    break;
                case Notification::TRAITEMENT_INITIALISE:
                    // Traitement initialisé par le DPO
                    echo '<a href="/organisations/changenotification/' . $value['Fiche']['organisation_id'] . '/pannel/encours_redaction/' . $value['Fiche']['id'] . '" class="list-group-item list-group-item-info">'.__d('default','default.notificationLeTraitement').' <strong>"' . $value['Fiche']['Valeur'][0]['valeur'] . '"</strong>'.__d('default','default.notificationTraitementInitialise').'</a>';
                    break;
            }

            $oldmairie = $mairie;

            $this->requestAction([
                'controller' => 'pannel',
                'action' => 'notifAfficher',
                $arrayNotificationNotVuNotAfficher[$key]
            ]);
        }

        echo '</div>
                    <div class="modal-footer">';

        echo $this->Html->link("<i class='fa fa-times fa-lg'><!----></i>" . ' Fermer', [
            'controller' => 'pannel',
            'action' => 'validNotif'
                ], [
            'class' => 'btn btn-outline-primary',
            'escapeTitle' => false
        ]);

        echo '</div>
                    </div>
                    </div>
                   </div>
                </div>';
    }
}
