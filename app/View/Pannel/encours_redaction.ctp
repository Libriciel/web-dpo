<?php
echo $this->Html->script('pannel.js');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

// balise du scrollTo
$idFicheNotification = $this->Session->read('idFicheNotification');
unset($_SESSION['idFicheNotification']);

// Banette mes déclarations en cours de rédaction
echo $this->Banettes->encoursRedaction($banettes['encours_redaction']);

echo $pagination;

// Pop-up envoie consultation
echo $this->element('Pannel/modalEnvoieConsultation');

// Pop-up envoie validation
echo $this->element('Pannel/modalEnvoieValidation');

// Pop-up inséré traitement registre
echo $this->element('Pannel/modalValidDpo');
?>

<script type="text/javascript">

    $(document).ready(function () {

        openTarget("<?php echo $idFicheNotification ?>");

    });
    
</script>
