<td></td>

<td colspan='4' class='tdleft'>
    <?php
    echo $this->element('History/journey', [
        'parcours' => $parcours
    ]);
    ?>
</td>

<td class="tdleft">
    <?php
    echo $this->element('History/full_history', [
        'historique' => $historique,
        'id' => $id
    ]);
    ?>
</td>

<td></td>