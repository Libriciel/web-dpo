<?php
echo $this->Html->script('pannel.js');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

// Banette mes déclarations en cours d'initialisation
echo $this->Banettes->initialisationTraitement($banettes['initialisation']);

echo $pagination;

// Pop-up envoie redacteur
echo $this->element('Pannel/modalSendRedaction');

echo $this->element('Default/modalInitialisationTraitement');
