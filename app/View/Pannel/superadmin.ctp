<div class="row row-btn-default-menu">
    <a type="button" class="btn btn-default-menu" href="/organisations/index/">
        <div class="fa fa-building fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.sousTitreEntites');
            ?>
        </div>
    </a>

    <div type="button" class="btn btn-default-menu" data-toggle="modal" data-target="#modalAdministrerEntite">
        <div class="fa fa-wrench fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.titreAdministrationEntite');
            ?>
        </div>
    </div>

    <a type="button" class="btn btn-default-menu" href="/admins/index/">
        <div class="fa fa-user fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.sousTitreSuperAdministrateur');
            ?>
        </div>
    </a>

    <!--Bouton Gestion de tous les utilisateurs-->
    <a type="button" class="btn btn-default-menu" href="/users/admin_index/">
        <div class="fa fa-users fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.sousTitreGestionTousUtilisateurs');
            ?>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu text-center" href="/formulaires/index/">
        <div class="fa fa-list-alt fa-primary fa-2x"></div>

        <div>
            <span style="word-wrap: break-word; text-align: center">
                <?php
                echo __d('default', 'default.sousTitreGestionTousFormulaires');
                ?>
            </span>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu text-center" href="/soustraitants/index/">
        <div class="fa fa-list-alt fa-primary fa-2x"></div>

        <div>
            <span style="word-wrap: break-word; text-align: center">
                <?php
                echo __d('default', 'default.sousTitreGestionTousSousTraitants');
                ?>
            </span>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu text-center" href="/responsables/index/">
        <div class="fa fa-list-alt fa-primary fa-2x"></div>

        <div>
            <span style="word-wrap: break-word; text-align: center">
                <?php
                echo __d('default', 'default.sousTitreGestionTousResponsable');
                ?>
            </span>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu text-center" href="/typages/index/">
        <div class="fa fa-list-alt fa-primary fa-2x"></div>

        <div>
            <span style="word-wrap: break-word; text-align: center">
                <?php
                echo __d('default', 'default.sousTitreGestionTypagesAnnexes');
                ?>
            </span>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu text-center" href="/organisations/gestionrgpd/">
        <div class="fa fa-user-secret fa-primary fa-2x"></div>

        <div>
            <span style="word-wrap: break-word; text-align: center">
                <?php
                echo __d('default', 'default.sousTitreGestionPolitiqueConfidentialite');
                ?>
            </span>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu" href="/checks/index/">
        <div class="fa fa-cogs fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.sousTitreChecksIndex');
            ?>
        </div>
    </a>

    <a type="button" class="btn btn-default-menu" href="/admins/changeviewlogin/">
        <div class="fa fa-sliders-h fa-primary fa-2x"></div>

        <div>
            <?php
            echo __d('default', 'default.sousTitrePageConnexion');
            ?>
        </div>
    </a>
</div>
