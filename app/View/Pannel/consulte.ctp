<?php
echo $this->Html->script('pannel.js');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

// balise du scrollTo
$idFicheNotification = $this->Session->read('idFicheNotification');
unset($_SESSION['idFicheNotification']);

// Banette etat des traitements passés en ma possession
echo $this->Banettes->consulte($banettes['consulte']);

echo $pagination;

// Pop-up reorientation du traitement
echo $this->element('Pannel/modalReorienter');
?>

<script type="text/javascript">

    $(document).ready(function () {

        openTarget("<?php echo $idFicheNotification ?>");

    });
    
</script>
