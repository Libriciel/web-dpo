<?php
echo $this->Html->script('pannel.js');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

// balise du scrollTo
$idFicheNotification = $this->Session->read('idFicheNotification');
unset($_SESSION['idFicheNotification']);

// Banette mes traitements validés et insérés au registre
echo $this->Banettes->archives($banettes['archives']);

echo $pagination;
