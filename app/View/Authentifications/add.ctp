<?php
$breadcrumbs = [
    __d('connecteur', 'connecteur.titreGestionConnecteurs') => [
        'controller' => 'connecteurs',
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Authentification']) && !empty($this->validationErrors['User'])) {
    ?>
    <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
            <span class="sr-only">Error:</span>
            Ces erreurs se sont produites:
            <ul>
                <?php
                foreach ($this->validationErrors as $donnees) {
                    foreach ($donnees as $champ) {
                        foreach ($champ as $error) {
                            echo '<li>' . $error . '</li>';
                        }
                    }
                }
                ?>
            </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Authentification',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
//    'type' => 'file'
]);
?>

<h2>
    <?php
    echo __d('authentification', 'authentification.titreAuthentificationExterne');
    ?>
</h2>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->input('use', [
            'id' => 'use',
            'class' => 'form-control custom-select',
            'options' => [
                false => 'Non',
                true => 'Oui'
            ],
            'empty' => false,
            'required' => true,
            'placeholder' => false
        ]);
        ?>
    </div>
</div>

<fieldset id="ldap-block">
    <h2>
        <?php
        echo __d('authentification', 'authentification.titreTypeAuthentification');
        ?>
    </h2>
    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('type', [
                'id' => 'type',
                'options' => $options['Authentification']['type'],
                'empty' => false,
                'required' => true,
                'placeholder' => false
            ]);
            ?>
        </div>
    </div>
</fieldset>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#ldap-block").hide();
        $("#cas-block").hide();

        <?php 
        if ($this->request->data('Authentification.use') == true) {
            ?>
            $("#ldap-block").show();
            <?php
        }
        ?>
        
        $('#use').change(function() {
            let select = $(this),
                selected = $(select).val();

            if (selected === '1') {
                $("#ldap-block").show();
            } else {
                $("#ldap-block").hide();
            }
        });
        
    });
</script>
