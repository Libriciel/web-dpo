<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('responsable', 'responsable.btnFiltrerResponsable')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer sur le siret
            echo $this->element('Filtres/Parametrages/siret', [
                'fieldname' => 'siretstructure',
                'options' => $options['siretstructure']
            ]);

            // Filtrer sur le code APE du co-responsable
            echo $this->element('Filtres/Parametrages/ape', [
                'fieldname' => 'apestructure',
                'options' => $options['apestructure']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par raison sociale du co-responsable
            echo $this->element('Filtres/Parametrages/raisonsociale', [
                'fieldname' => 'raisonsocialestructure',
                'options' => $options['raisonsocialestructure']
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
$pagination = null;

if (!empty($responsables)) {
    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- Structure -->
                <th class="col-md-3">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauStructure');
                    ?>
                </th>

                <!--  Responsable -->
                <th class="col-md-3">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauResponsable');
                    ?>
                </th>

                <!-- Information sur le Dpo -->
                <th class="col-md-3">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauInfoDPO');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-3">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($responsables as $responsable) {
                ?>
                <tr class="d-flex">
                    <!-- Structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $responsable['Responsable']['raisonsocialestructure'];

                        echo '<ul><li><strong>Siret : </strong>'.$responsable['Responsable']['siretstructure'].'</li></ul>';

                        $items = Hash::extract($responsable, 'Responsable.telephonestructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.faxstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fax : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.emailstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Responsable de la structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        if (empty($responsable['Responsable' ]['nom_complet']) === false) {
                            echo $responsable['Responsable' ]['nom_complet'];
                        }

                        $items = Hash::extract($responsable, 'Responsable.fonctionresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fonction : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephoneresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.emailresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- DPO de la structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        if (empty($responsable['Responsable' ]['nom_complet_dpo']) === false) {
                            echo $responsable['Responsable' ]['nom_complet_dpo'];
                        }

                        $items = Hash::extract($responsable, 'Responsable.numerocnil_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Numéro CNIL : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.email_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephonefixe_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone fixe : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephoneportable_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone portable : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-3">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/show', [
                                'controller' => 'responsables',
                                'id' => $responsable['Responsable']['id'],
                                'titleBtn' => __d('responsable', 'responsable.commentaireBtnVisualiserResponsable')
                            ]);

                            echo $this->element('Buttons/dissocier', [
                                'controller' => 'responsables',
                                'action' => 'dissocierResponsable',
                                'id' => $responsable['Responsable']['id'],
                                'titleBtn' => __d('responsable', 'responsable.commentaireBtnSupprimerResponsableEntite'),
                                'confirmation' => __d('responsable', 'responsable.confirmationSupprimerResponsableEntite') . ' " ' . $responsable['Responsable']['raisonsocialestructure'] . ' " de votre entité ?',
                            ]);
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('responsable', 'responsable.textAucunResponsableEntite')
                : __d('responsable', 'responsable.textFiltreAucunResponsableEntite');
            ?>
        </h3>
    </div>
    <?php
}
