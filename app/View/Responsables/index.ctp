<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
if (array_key_exists('Organisation', $this->Session->read()) === true) {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
} else {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('responsable', 'responsable.btnFiltrerResponsable')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par entité associée
            echo $this->element('Filtres/Parametrages/organisation_associee', [
                'options' => $options['organisations']
            ]);

            // Filtrer sur le siret
            echo $this->element('Filtres/Parametrages/siret', [
                'fieldname' => 'siretstructure', 'options' => $options['siretstructure']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par raison sociale du co-responsable
            echo $this->element('Filtres/Parametrages/raisonsociale', [
                'fieldname' => 'raisonsocialestructure',
                'options' => $options['raisonsocialestructure']
            ]);

            // Filtrer sur le code APE du co-responsable
            echo $this->element('Filtres/Parametrages/ape', [
                'fieldname' => 'apestructure',
                'options' => $options['apestructure']
            ]);
            ?>
        </div>

        <br>

        <div class="col-md-6">
            <?php
            // Filtrer par entité créatrice
            echo $this->element('Filtres/Parametrages/organisation_creatrice', ['options' => $options['organisations']]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<h3>
    <div class='text-warning'>
        <i class="fa fa-fw fa-exclamation-triangle"></i>
        <?php
        echo __d('responsable', 'responsable.warningTexteExplication')
        ?>
    </div>
</h3>

<?php
if (!empty($responsables)) {
    echo $this->element('Buttons/add', [
        'controller' => 'responsables',
        'labelBtn' => __d('responsable','responsable.btnAjouterResponsable'),
        'before' => '<div class="text-left">',
    ]);

    $pagination = null;

    $this->Paginator->options(
        array( 'url' => Hash::flatten( (array)$this->request->data, '.' ) )
    );
    $pagination = $this->element('pagination');

    echo $pagination;

    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('responsable', 'responsable.btnAffecterEntite')
    ]);
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                    <input id="responsableEntiteCheckbox" type="checkbox" class = "responsableEntiteCheckbox_checkbox" />
                </th>

                <!-- Structure -->
                <th class="col-md-3">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauStructure');
                    ?>
                </th>

                <!--  Responsable -->
                <th class="col-md-2">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauResponsable');
                    ?>
                </th>

                <!-- Information sur le Dpo -->
                <th class="col-md-2">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauInfoDPO');
                    ?>
                </th>

                <!-- Entité associée -->
                <th class="col-md-2">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauEntiteResponsable');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($responsables as $responsable) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer un responsable à une entité -->
                    <td class="tdleft col-md-1">
                        <input id='checkbox<?php echo $responsable['Responsable']['id']; ?>' type="checkbox" class="responsableEntiteCheckbox" value="<?php echo $responsable['Responsable']['id']; ?>" >
                    </td>

                    <!-- Structure -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $responsable['Responsable']['raisonsocialestructure'];

                        $items = Hash::extract($responsable, 'Responsable.siretstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Siret : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephonestructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.faxstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fax : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.emailstructure');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Responsable de la structure -->
                    <td class="tdleft col-md-2">
                        <?php
                        if (empty($responsable['Responsable' ]['nom_complet']) === false) {
                            echo $responsable['Responsable' ]['nom_complet'];
                        }

                        $items = Hash::extract($responsable, 'Responsable.fonctionresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Fonction : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephoneresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.emailresponsable');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- DPO de la structure -->
                    <td class="tdleft col-md-2">
                        <?php
                        if (empty($responsable['Responsable' ]['nom_complet_dpo']) === false) {
                            echo $responsable['Responsable' ]['nom_complet_dpo'];
                        }

                        $items = Hash::extract($responsable, 'Responsable.numerocnil_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Numéro CNIL : </strong>' . implode('</li><li>', $items) . '</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.email_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>E-mail : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephonefixe_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone fixe : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }

                        $items = Hash::extract($responsable, 'Responsable.telephoneportable_dpo');
                        if (empty($items) === false) {
                            echo '<ul><li><strong>Téléphone portable : </strong>'.implode('</li><li>', $items).'</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Entité associée -->
                    <td class="tdleft col-md-2">
                        <?php
                        $items = Hash::extract($responsable, 'Organisation.{n}.raisonsociale');
                        if (empty($items) === false) {
                            echo '<ul><li>' . implode('</li><li>', $items) . '</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/show', [
                                'controller' => 'responsables',
                                'id' => $responsable['Responsable']['id'],
                                'titleBtn' => __d('responsable', 'responsable.commentaireBtnVisualiserResponsable')
                            ]);

                            if ($this->Session->read('Su') ||
                                $responsable['Responsable']['createdbyorganisation'] == $this->Session->read('Organisation.id')
                            ) {
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'responsables',
                                    'id' => $responsable['Responsable']['id'],
                                    'titleBtn' => __d('responsable', 'responsable.commentaireBtnModifierResponsable')
                                ]);

                                if (empty($responsable['Organisation']) &&
                                    empty($responsable['Fiche'])
                                ) {
                                    echo $this->element('Buttons/delete', [
                                        'controller' => 'responsables',
                                        'id' => $responsable['Responsable']['id'],
                                        'titleBtn' => __d('responsable', 'responsable.commentaireBtnSupprimerResponsable'),
                                        'confirmation' => __d('responsable', 'responsable.confirmationSupprimerResponsable') . $responsable['Responsable']['raisonsocialestructure'] . ' ?'
                                    ]);
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('responsable', 'responsable.textAucunResponsable')
                : __d('responsable', 'responsable.textFiltreAucunResponsable');
            ?>
        </h3>
    </div>
    <?php
}

echo $this->element('Buttons/add', [
    'controller' => 'responsables',
    'labelBtn' => __d('responsable','responsable.btnAjouterResponsable'),
]);

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Responsable',
    'modalTitle' => __d('responsable', 'responsable.popupTitreAffecterResponsable'),
    'formulaireName' => 'ResponsableOrganisation',
    'fieldName' => 'responsable_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'responsableEntiteCheckbox'
]);
