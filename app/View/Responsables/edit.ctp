<?php
$breadcrumbs = [
    __d('responsable', 'responsable.titreGestionResponsableApplication') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
if (array_key_exists('Organisation', $this->Session->read()) === true) {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
} else {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

if (isset($this->validationErrors['Responsable']) && !empty($this->validationErrors['Responsable'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

$readonly = false;
if ($this->request->params['action'] === 'show') {
    $readonly = true;
}

if (!isset($cannotModified) && $this->request->params['action'] === 'add') {
    $cannotModified = false;
} elseif(!isset($cannotModified) && $this->request->params['action'] === 'edit') {
    $cannotModified = true;
}

echo $this->WebcilForm->create('Responsable', [
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);

echo $this->element('Responsables_Soustraitants/fields', [
    'readonly' => $readonly,
    'cannotModified' => $cannotModified,
    'association' => true,
    'titleAssociation' => __d('responsable', 'responsable.titreAssociationEntiteResponsable'),
]);

if ($readonly === true) {
    echo $this->WebcilForm->buttons(['Back']);
} else {
    echo $this->WebcilForm->buttons(['Cancel', 'Save']);
}

echo $this->WebcilForm->end();

if ($readonly === true) {
    ?>
    <script type="text/javascript">
        $("#OrganisationOrganisation").prop("disabled", true);
    </script>
    <?php
}
