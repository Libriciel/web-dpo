<?php
echo $this->Html->script('organisations.js');

$this->Breadcrumbs->breadcrumbs([
    __d('organisation', 'organisation.titreIndexEntite') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

if (isset($this->validationErrors['Organisation']) && !empty($this->validationErrors['Organisation'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
                foreach ( $this->validationErrors as $donnees ) {
                    foreach ( $donnees as $champ ) {
                        foreach ( $champ as $error ) {
                            echo '<li>' . $error . '</li>';
                        }
                    }
                }
            ?>
        </ul>
    </div>
    <?php
}
?>

<div class="users form">
    <?php
        echo $this->Form->create('Organisation', [
            'type' => 'file',
            'autocomplete' => 'off',
            'class' => 'form-horizontal'
        ]); 
    ?>

    <!--Information sur l'entité -->
    <h2>
        <?php
            echo __d('organisation','organisation.textEntite');
        ?>
    </h2>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'raisonsociale' => [
                        'id' => 'raisonsociale',
                        'readonly' => true,
                        'required' => true
                    ],
                    'telephone' => [
                        'id' => 'telephone',
                        'readonly' => true,
                        'required' => true
                    ],
                    'fax' => [
                        'id' => 'fax',
                        'readonly' => true
                    ],
                    'adresse' => [
                        'id' => 'adresse',
                        'type' => 'textarea',
                        'readonly' => true,
                        'required' => true
                    ],
                    'email' => [
                        'id' => 'email',
                        'readonly' => true,
                        'required' => true
                    ]
                ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'sigle' => [
                        'id' => 'sigle',
                        'readonly' => true
                    ],
                    'siret' => [
                        'id' => 'siret',
                        'readonly' => true,
                        'required' => true
                    ],
                    'ape' => [
                        'id' => 'ape',
                        'readonly' => true,
                        'required' => true
                    ]
                ]);
            ?>

            <!-- Affichage logo si l'entitée en a déjà un d'enregistré -->
            <div class="col-md-8 col-md-offset-4">
                <?php 
                if (file_exists(IMAGES . 'logos' . DS . $this->request->data[ 'Organisation' ][ 'id' ] . '.' . $this->request->data[ 'Organisation' ][ 'logo' ])) {
                    ?>
                    <div class="thumbnail">
                        <?php 
                            echo $this->Html->image('logos/' . $this->request->data[ 'Organisation' ][ 'id' ] . '.' . $this->request->data[ 'Organisation' ][ 'logo' ], [
                                'alt' => 'Logo',
                                'width' => '300px'
                            ]);
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    
    <!-- Information sur le responsable de l'entitée -->
    <h2>
        <?php
            echo __d('organisation', 'organisation.titreResponsableEntitee');
        ?>
    </h2>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'responsable' => [
                        'id' => 'responsable',
                        'value' => $this->request->data('Organisation.responsable_nom_complet'),
                        'readonly' => true,
                        'required' => true
                    ],
                    'fonctionresponsable' => [
                        'id' => 'fonctionresponsable',
                        'readonly' => true,
                        'required' => true
                    ]
                ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'emailresponsable' => [
                        'id' => 'emailresponsable',
                        'readonly' => true,
                        'required' => true
                    ],
                    'telephoneresponsable' => [
                        'id' => 'telephoneresponsable',
                        'readonly' => true,
                        'required' => true
                    ]
                ]);
            ?>
        </div>

        <!-- Information sur le DPO -->
        <div class="col-md-8">
            <!-- Affichage du logo du DPO -->
            <?php 
                if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
                    echo $this->Html->image('logo_dpo.svg', [
                        'class' => 'logo-well'
                    ]);
                }
            ?>
        </div>

        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
            $nameDpo = trim($this->request->data('Dpo.civilite').' '.$this->request->data('Dpo.prenom').' '.$this->request->data('Dpo.nom'));

            echo $this->WebcilForm->inputs([
                'dpo_structure' => [
                    'id' => 'dpo_structure',
                    'label' => [
                        'text' => __d('organisation', 'organisation.textDPO'),
                    ],
                    'placeholder' => __d('organisation', 'organisation.placeholderChampDpo'),
                    'value' => $nameDpo,
                    'readonly' => true,
                    'required' => true
                ],
                'numerodpo' => [
                    'id' => 'numerodpo',
                    'readonly' => true,
                    'required' => true
                ]
            ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->input('emailDpo', [
                    'id' => 'emailDpo',
                    'value' => $this->request->data('Dpo.email'),
                    'readonly' => true,
                    'required' => true
                ]);
            ?>
        </div>
    </div>
</div>

<div style="clear: both">
    <?php
        echo $this->WebcilForm->buttons(['Back']);
    ?>
</div>

<?php
echo $this->Form->end();
