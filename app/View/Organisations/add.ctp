<?php
echo $this->Html->script('organisations.js');

$this->Breadcrumbs->breadcrumbs([
    __d('organisation', 'organisation.titreIndexEntite') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
]);

if (isset($this->validationErrors['Organisation']) && !empty($this->validationErrors['Organisation'])) {
    ?>

    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}
?>

<div class="users form">
    <?php
        echo $this->WebcilForm->create('Organisation',[
            'url' => 'add',
            'type' => 'file',
            'autocomplete' => 'off',
            'inputDefaults' => ['div' => false],
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate'
        ]);
    ?>

    <!--Information sur l'entité -->
    <h2>
        <?php
            echo __d('organisation','organisation.textEntite');
        ?>
    </h2>
    
    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'raisonsociale' => ['id' => 'raisonsociale', 'required' => true],
                    'telephone' => ['id' => 'telephone', 'required' => true],
                    'fax' => ['id' => 'fax'],
                    'adresse' => ['id' => 'adresse', 'type' => 'textarea', 'required' => true],
                    'email' => ['id' => 'email', 'required' => true]
                ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'sigle' => ['id' => 'sigle'],
                    'siret' => ['id' => 'siret', 'required' => true],
                    'ape' => ['id' => 'ape', 'required' => true]
                ]);

                // Ajouter un logo
                echo $this->WebcilForm->input('logo_file', [
                    'id' => 'logo_file',
                    'div' => 'input-group inputsForm',
                    'type' => 'file',
                    'class' => 'filestyle',
                    'data-buttonText' => ' Ajouter un logo',
                    'data-buttonBefore' => "true",
                    'accept' => Configure::read('logoAcceptedTypes'),
                    'label' => [
                        'text' => false
                    ]
                ]);
            ?>
        </div>
    </div>

    <h2>
        <?php
        // Texte : "Responsable de l'entité"
        echo __d('organisation', 'organisation.titreResponsableEntitee');
        ?>
    </h2>

    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'civiliteresponsable' => [
                        'id' => 'civiliteresponsable',
                        'options' => $options['Organisation']['civiliteresponsable'],
                        'empty' => true,
                        'required' => true,
                        'placeholder' => false
                    ],
                    'nomresponsable' => [
                        'id' => 'nomresponsable',
                        'required' => true
                    ],
                    'prenomresponsable' => [
                        'id' => 'prenomresponsable',
                        'required' => true
                    ]
                ]);
            ?>
        </div>

        <!-- Colonne de droite -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->inputs([
                    'fonctionresponsable' => ['id' => 'fonctionresponsable', 'required' => true],
                    'emailresponsable' => ['id' => 'emailresponsable', 'required' => true],
                    'telephoneresponsable' => ['id' => 'telephoneresponsable', 'required' => true]
                ]);
            ?>
        </div>
    </div>
</div>

<div style="clear: both">
    <?php
        // Groupe de boutons
        echo $this->WebcilForm->buttons(['Cancel', 'Save']);
    ?>
</div>

<?php
    echo $this->Form->end();
?>

<script>

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');
        
    });

</script>
