<?php
// JS
echo $this->Html->script([
    '/js/node_modules/jquery/dist/jquery.min.js',
    'chosen/chosen.jquery.min.js',
    'select.js'
]);

// CSS
echo $this->Html->css([
    '/js/node_modules/bootstrap/dist/css/bootstrap.min.css',
    '/js/chosen/chosen.min.css',
    'select.css',
    'main.css',
]);
?>

<ls-lib-login-model
        logo="/img/logo-black-blue.svg"
        visual-configuration='<?php echo $configLoginExistante; ?>' >

    <?php
    echo $this->WebcilForm->create('Organisation', [
        'autocomplete' => 'off',
        'inputDefaults' => ['div' => false],
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate'
    ]);
    ?>

    <br/>

    <?php
    //Champ Séléctionner une entité *
    echo $this->WebcilForm->input('Organisation.organisationcible', [
        'id' => 'organisationcible',
        'options' => $orgaListe,
        'class' => 'usersDeroulant transformSelect form-control',
        'empty' => false,
        'required' => true,
        'placeholder' => false,
    ]);

    echo $this->WebcilForm->button('<i class="fas fa-sign-in-alt fa-fw"></i>' . ' ' . __d('user', 'user.btnConnexion'), [
        'type' => 'submit',
        'class' => 'float-right btn btn-outline-primary'
    ]);

    echo $this->WebcilForm->end();
    ?>
</ls-lib-login-model>
