<?php
echo $this->Html->script('organisations.js');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Organisation']) && !empty($this->validationErrors['Organisation'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <i class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></i>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Organisation',[
    'type' => 'file',
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<!--Information sur l'entité -->
<h3 class="col-md-12">
    <?php
    echo __d('organisation','organisation.textEntite');
    ?>
</h3>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'raisonsociale' => [
                'id' => 'raisonsociale',
                'required' => true
            ],
            'telephone' => [
                'id' => 'telephone',
                'required' => true
            ],
            'fax' => [
                'id' => 'fax'
            ],
            'adresse' => [
                'id' => 'adresse',
                'type' => 'textarea',
                'required' => true
            ],
            'email' => [
                'id' => 'email',
                'required' => true
            ]
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'sigle' => [
                'id' => 'sigle'
            ],
            'siret' => [
                'id' => 'siret',
                'required' => true
            ],
            'ape' => [
                'id' => 'ape',
                'required' => true
            ]
        ]);

        if (isset($this->request->data['Organisation']['logo'])) {
            ?>
            <!-- Affichage logo si l'entitée en a déjà un d'enregistré -->
            <div class="col-md-offset-4">
                <?php
                if (file_exists(CHEMIN_LOGOS . $this->request->data['Organisation']['logo'])) {
                    ?>
                    <div class="thumbnail">
                        <?php
                        echo $this->Html->image(CHEMIN_LIEN_SYMBOLIQUE_LOGO_ORGANISATION . $this->request->data['Organisation']['logo'], [
                            'alt' => 'Logo',
                            'width' => '300px'
                        ]);
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
        }

        // Champ charger un logo
        echo $this->WebcilForm->input('logo_file', [
            'id' => 'logo_file',
            'type' => 'file',
            'class' => 'filestyle',
            'data-text' => __d('organisation', 'organisation.btnLogo'),
            'data-buttonBefore' => 'true',
            'accept' => Configure::read('logoAcceptedTypes'),
            'label' => [
                'text' => false
            ]
        ]);
        ?>
    </div>
</div>

<h3 class="col-md-12">
    <?php
    // Texte : "Responsable de l'entité"
    echo __d('organisation', 'organisation.titreResponsableEntitee');
    ?>
</h3>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'civiliteresponsable' => [
                'id' => 'civiliteresponsable',
                'class' => 'form-control custom-select',
                'options' => $options['Organisation']['civiliteresponsable'],
                'empty' => false,
                'required' => true,
                'placeholder' => false
            ],
            'nomresponsable' => [
                'id' => 'nomresponsable',
                'required' => true
            ],
            'prenomresponsable' => [
                'id' => 'prenomresponsable',
                'required' => true
            ]
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'fonctionresponsable' => [
                'id' => 'fonctionresponsable',
                'required' => true
            ],
            'emailresponsable' => [
                'id' => 'emailresponsable',
                'required' => true
            ],
            'telephoneresponsable' => [
                'id' => 'telephoneresponsable',
                'required' => true
            ]
        ]);
        ?>
    </div>
</div>

<?php
if ($array_users != null) {
    ?>
    <div class="col-md-12">
        <?php
        if (file_exists(IMAGES . DS . 'logo_dpo.svg')) {
            echo $this->Html->image('logo_dpo.svg', [
                'class' => 'logo-well'
            ]);
        }
        ?>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->inputs([
                'dpo' => [
                    'id' => 'dpo',
                    'options' => $array_users,
                    'class' => 'form-control usersDeroulant',
                    'label' => [
                        'text' => __d('organisation', 'organisation.textDPO'),
                    ],
                    'empty' => __d('organisation', 'organisation.placeholderChampDpo'),
                    'placeholder' => false,
                    'required' => true
                ],
                'numerodpo' => [
                    'id' => 'numerodpo',
                    'required' => true
                ]
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            echo $this->WebcilForm->input('emaildpo', [
                'id' => 'emaildpo',
                'required' => true
            ]);

            // Menu déroulant droit
            echo $this->WebcilForm->input('newRoleOldDPO', [
                'id' => 'newRoleOldDPO',
                'options' => $roles,
                'empty' => false,
                'required' => true,
                'placeholder' => false
            ]);
            ?>
        </div>
    </div>
    <?php
}
?>

<h3 class="col-md-12">
    <?php
    // Texte : "Registre"
    echo __d('organisation', 'organisation.titreRegistre');
    ?>
</h3>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'prefixenumero' => [
                'id' => 'prefixenumero',
                'required' => true
            ],
            'numeroregistre' => [
                'id' => 'numeroregistre',
                'type' => 'hidden',
                'required' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->input('usefieldsredacteur', [
            'id' => 'usefieldsredacteur',
            'options' => [
                true => 'Oui',
                false => 'Non'
            ],
            'class' => 'transformSelect form-control',
            'required' => true,
            'empty' => false,
            'placeholder' => false,
            'data-placeholder' => ' '
        ]);
        ?>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">
    $(document).ready(function () {
        let infoUsers = <?php echo json_encode($informationsUsers); ?>;
        let DPO_id = <?php echo json_encode(Hash::get($this->request->data, 'Organisation.dpo')); ?>;

        $('#newRoleOldDPO').parent().hide();

        $('#dpo').on("change", function () {
            if (this.value == "" || this.value == DPO_id) {
                $('#newRoleOldDPO').attr("required", false);
                $('#newRoleOldDPO').parent().hide();
            } else {
                $('#newRoleOldDPO').attr("required", true);
                $('#newRoleOldDPO').parent().show();
            }
        });

        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');
    });
</script>
