<?php
echo $this->Html->script([
    'seiyria-bootstrap-slider/dist/bootstrap-slider.min.js'
]);

echo $this->Html->css([
    'password-force.css',
    '/js/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
]);

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Organisation']) && !empty($this->validationErrors['Organisation'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Organisation',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
    'type' => 'file'
]);
?>

<div class="users form">
    <div class='row'>
        <div class="col-md-6">
            <label>
                <?php
                echo __d('organisation', 'organisation.champDefinirForcePasswordEntitee')
                ?>
            </label>
            <input id="forcePassword" type="text"
                   data-provide="slider"
                   data-slider-ticks="[1, 2, 3, 4, 5]"
                   data-slider-ticks-labels='["Très faible", "Faible", "Moyen", "Fort", "Très fort"]'
                   data-slider-min="1"
                   data-slider-max="5"
                   data-slider-step="1"
                   data-slider-value="<?php echo Hash::get($this->request->data, 'Organisation.force');?>"
                   data-slider-tooltip="hide"
            />
        </div>
    </div>

    <br>
    <br>

    <div class='row'>
        <div id="forcePasswordTresFaible" class="alert alert-force1">
            <strong>
                <?php
                echo __d('organisation', 'organisation.texteForcePasswordTresFaible');
                ?>
            </strong>

            <p>
                <?php
                echo __d('organisation', 'organisation.texteExplicationForcePassword');
                ?>
            </p>
            <p>
                <?php
                echo __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles');
                ?>
            </p>
        </div>

        <div id="forcePasswordFaible" class="alert alert-force2">
            <strong>
                <?php
                echo __d('organisation', 'organisation.texteForcePasswordFaible')
                ?>
            </strong>

            <p>
                <?php
                echo __d('organisation', 'organisation.texteExplicationForcePassword');
                ?>
            </p>
            <p>
                <?php
                echo __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles');
                ?>
            </p>
        </div>

        <div id="forcePasswordMoyen" class="alert alert-force3">
            <strong>
                <?php
                echo __d('organisation', 'organisation.texteForcePasswordMoyen')
                ?>
            </strong>

            <p>
                <?php
                echo __d('organisation', 'organisation.texteExplicationForcePassword');
                ?>
            </p>
            <p>
                <?php
                echo __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles');
                ?>
            </p>
        </div>

        <div id="forcePasswordFort" class="alert alert-force4">
            <strong>
                <?php
                echo __d('organisation', 'organisation.texteForcePasswordFort')
                ?>
            </strong>

            <p>
                <?php
                echo __d('organisation', 'organisation.texteExplicationForcePassword');
                ?>
            </p>
            <p>
                <?php
                echo __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles');
                ?>
            </p>
        </div>

        <div id="forcePasswordTresFort" class="alert alert-force5">
            <strong>
                <?php
                echo __d('organisation', 'organisation.texteForcePasswordTresFort')
                ?>
            </strong>

            <p>
                <?php
                echo __d('organisation', 'organisation.texteExplicationForcePassword');
                ?>
            </p>
            <p>
                <?php
                echo __d('organisation', 'organisation.textExplicationNbCaractereNbSymboles');
                ?>
            </p>
        </div>
    </div>

    <?php
    echo $this->WebcilForm->buttons(['Cancel', 'Save']);
    ?>
</div>

<hr>

<h3>
    <?php
    echo __d('organisation', 'organisation.titreNbCaracterMinimalRequisAlphabet');
    ?>
</h3>

<div class="users form">
    <div class="row">
        <table id="theorie" class="col-md-7 table table-striped table-hover">
            <thead>
                <tr class="d-flex">
                    <th class="col-md-4">
                        Alphabet
                    </th>
                    <th class="col-md-3">
                        Symboles de l'alphabet
                    </th>
                    <th class="col-md-1">
                        Très faible
                    </th>
                    <th class="col-md-1">
                        Faible
                    </th>
                    <th class="col-md-1">
                        Moyen
                    </th>
                    <th class="col-md-1">
                        Fort
                    </th>
                    <th class="col-md-1">
                        Très fort
                    </th>
                </tr>
            </thead>

            <tbody>
                <tr class="d-flex symbols_2">
                    <td class="tdleft col-md-4">2 symboles</td>
                    <td class="tdleft col-md-3">0 ou 1</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 64</td>
                    <td class="tdleft col-md-1 number strength-weak">64</td>
                    <td class="tdleft col-md-1 number strength-medium">80</td>
                    <td class="tdleft col-md-1 number strength-strong">100</td>
                    <td class="tdleft col-md-1 number strength-very-strong">128</td>
                </tr>

                <tr class="d-flex symbols_10">
                    <td class="tdleft col-md-4">10 symboles</td>
                    <td class="tdleft col-md-3">0 à 9</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 20</td>
                    <td class="tdleft col-md-1 number strength-weak">20</td>
                    <td class="tdleft col-md-1 number strength-medium">25</td>
                    <td class="tdleft col-md-1 number strength-strong">31</td>
                    <td class="tdleft col-md-1 number strength-very-strong">39</td>
                </tr>

                <tr class="d-flex symbols_16">
                    <td class="tdleft col-md-4">16 symboles</td>
                    <td class="tdleft col-md-3">0 à 9 et A à F</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 16</td>
                    <td class="tdleft col-md-1 number strength-weak">16</td>
                    <td class="tdleft col-md-1 number strength-medium">20</td>
                    <td class="tdleft col-md-1 number strength-strong">25</td>
                    <td class="tdleft col-md-1 number strength-very-strong">32</td>
                </tr>

                <tr class="d-flex symbols_26">
                    <td class="tdleft col-md-4">26 symboles</td>
                    <td class="tdleft col-md-3">A à Z</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 14</td>
                    <td class="tdleft col-md-1 number strength-weak">14</td>
                    <td class="tdleft col-md-1 number strength-medium">18</td>
                    <td class="tdleft col-md-1 number strength-strong">22</td>
                    <td class="tdleft col-md-1 number strength-very-strong">28</td>
                </tr>

                <tr class="d-flex symbols_36">
                    <td class="tdleft col-md-4">36 symboles</td>
                    <td class="tdleft col-md-3">0 à 9 et A à Z</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 13</td>
                    <td class="tdleft col-md-1 number strength-weak">13</td>
                    <td class="tdleft col-md-1 number strength-medium">16</td>
                    <td class="tdleft col-md-1 number strength-strong">20</td>
                    <td class="tdleft col-md-1 number strength-very-strong">25</td>
                </tr>

                <tr class="d-flex symbols_52">
                    <td class="tdleft col-md-4">52 symboles</td>
                    <td class="tdleft col-md-3">A à Z et a à z</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 12</td>
                    <td class="tdleft col-md-1 number strength-weak">12</td>
                    <td class="tdleft col-md-1 number strength-medium">15</td>
                    <td class="tdleft col-md-1 number strength-strong">18</td>
                    <td class="tdleft col-md-1 number strength-very-strong">23</td>
                </tr>

                <tr class="d-flex symbols_62">
                    <td class="tdleft col-md-4">62 symboles</td>
                    <td class="tdleft col-md-3">0 à 9, A à Z et a à z</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 11</td>
                    <td class="tdleft col-md-1 number strength-weak">11</td>
                    <td class="tdleft col-md-1 number strength-medium">14</td>
                    <td class="tdleft col-md-1 number strength-strong">17</td>
                    <td class="tdleft col-md-1 number strength-very-strong">22</td>
                </tr>

                <tr class="d-flex symbols_70">
                    <td class="tdleft col-md-4">70 symboles</td>
                    <td class="tdleft col-md-3">0 à 9, A à Z, a à z et  ! #$*% ?</td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 11</td>
                    <td class="tdleft col-md-1 number strength-weak">11</td>
                    <td class="tdleft col-md-1 number strength-medium">14</td>
                    <td class="tdleft col-md-1 number strength-strong">17</td>
                    <td class="tdleft col-md-1 number strength-very-strong">21</td>
                </tr>

                <tr class="d-flex symbols_90">
                    <td class="tdleft col-md-4">90 symboles</td>
                    <td class="tdleft col-md-3">0 à 9, A à Z, a à z et  ! #$*% ? &[|]@^µ§ :/ ;.,<>°²³ </td>
                    <td class="tdleft col-md-1 number strength-very-weak">< 10</td>
                    <td class="tdleft col-md-1 number strength-weak">10</td>
                    <td class="tdleft col-md-1 number strength-medium">13</td>
                    <td class="tdleft col-md-1 number strength-strong">16</td>
                    <td class="tdleft col-md-1 number strength-very-strong">20</td>
                </tr>
            </tbody>
        </table>

        <div class="col-md-5">
            <div id="informationPassword" class="alert alert-info">
                <strong>Exemples : </strong>
                <ul id="exemples">
                    <li class="strength-very-weak">Très faible (force 1)
                        <ul>
                            <li class="symbols_2"><kbd>00111</kbd> : <strong>5</strong> caractères dans un alphabet de <strong>2</strong> symboles. </li>
                        </ul>
                    </li>

                    <li class="strength-weak">Faible (force 2)
                        <ul>
                            <li class="symbols_10"><kbd>96442571499252715213</kbd> : <strong>20</strong> caractères dans un alphabet de <strong>10</strong> symboles. </li>
                        </ul>
                    </li>

                    <li class="strength-medium">Moyen (force 3)
                        <ul>
                            <li class="symbols_16"><kbd>65C88BAED3D6ECC0B705</kbd> : <strong>20</strong> caractères dans un alphabet de <strong>16</strong> symboles. </li>
                        </ul>
                    </li>

                    <li class="strength-strong">Fort (force 4)
                        <ul>
                            <li class="symbols_26"><kbd>KSFUNDDVTJWHHPKGXUNZJB</kbd> : <strong>22</strong> caractères dans un alphabet de <strong>26</strong> symboles. </li>
                        </ul>
                    </li>

                    <li class="strength-very-strong">Très fort (force 5)
                        <ul>
                            <li class="symbols_62"><kbd>Voitures2ColoSoupersos</kbd> : <strong>25</strong> caractères dans un alphabet de <strong>62</strong> symboles. </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
<!--    </div>-->
<!--</div>-->

<?php
echo $this->WebcilForm->hidden('force', [
    'id' => 'force',
    'value' => ''
]);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">

    $(document).ready(function() {

        $("#exemples li li").mouseover(function() {
            let symbols = $(this).attr('class'),
                strength = $(this).closest('ul').closest('li').attr('class');
            $("#theorie td").removeClass('highlight');
            $("#theorie tr." + symbols + " td." + strength).addClass('highlight');
        });

        $("#exemples li li").mouseout(function() {
            $("#theorie td").removeClass('highlight');
            passwordHelpBox(force);
        });

        $('#forcePasswordTresFaible').hide();
        $('#forcePasswordFaible').hide();
        $('#forcePasswordMoyen').hide();
        $('#forcePasswordFort').hide();
        $('#forcePasswordTresFort').hide();

        let force = $('#forcePassword').val();
        infoForce('#forcePassword', force);

        $("#forcePassword").change(function () {
            force = $('#forcePassword').val();
            infoForce('#forcePassword', force);
        });

        function passwordHelpBox(force) {
            $('#force').val(force);
            $("#theorie td").removeClass('highlight');

            switch(force){
                case '1':
                    $('#forcePasswordTresFaible').show();
                    $('#forcePasswordFaible').hide();
                    $('#forcePasswordMoyen').hide();
                    $('#forcePasswordFort').hide();
                    $('#forcePasswordTresFort').hide();

                    $("#theorie .strength-very-weak").addClass('highlight');
                    break;

                case '2':
                    $('#forcePasswordTresFaible').hide();
                    $('#forcePasswordFaible').show();
                    $('#forcePasswordMoyen').hide();
                    $('#forcePasswordFort').hide();
                    $('#forcePasswordTresFort').hide();

                    $("#theorie .strength-weak").addClass('highlight');
                    break;

                case '3':
                    $('#forcePasswordTresFaible').hide();
                    $('#forcePasswordFaible').hide();
                    $('#forcePasswordMoyen').show();
                    $('#forcePasswordFort').hide();
                    $('#forcePasswordTresFort').hide();

                    $("#theorie .strength-medium").addClass('highlight');
                    break;

                case '4':
                    $('#forcePasswordTresFaible').hide();
                    $('#forcePasswordFaible').hide();
                    $('#forcePasswordMoyen').hide();
                    $('#forcePasswordFort').show();
                    $('#forcePasswordTresFort').hide();

                    $("#theorie .strength-strong").addClass('highlight');
                    break;

                case '5':
                    $('#forcePasswordTresFaible').hide();
                    $('#forcePasswordFaible').hide();
                    $('#forcePasswordMoyen').hide();
                    $('#forcePasswordFort').hide();
                    $('#forcePasswordTresFort').show();

                    $("#theorie .strength-very-strong").addClass('highlight');
                    break;
            }
        };

        function infoForce(id, force){
            let slider_track = $(id).closest('div').find('div.slider-track'),
                slider_trick_container = $(id).closest('div').find('div.slider-tick-container').children(),
                slider_handle= $(id).closest('div').find('div.slider-handle '),
                i;

            for (i = 1 ; i <= 5 ; i++) {
                $(slider_track).removeClass('password-force-' + i);
                $(slider_trick_container).removeClass('password-force-' + i);
                $(slider_handle).removeClass('password-force-' + i);
            }

            $(slider_track).addClass('password-force-' + force);
            $(slider_trick_container).addClass('password-force-' + force);
            $(slider_handle).addClass('password-force-' + force);
            passwordHelpBox(force);
        }
    });

</script>
