<?php
echo $this->Html->script('organisations.js');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

if (isset($this->validationErrors['Organisation']) && !empty($this->validationErrors['Organisation'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}
?>

<div class="users form">
    <?php
    echo $this->WebcilForm->create('Organisation',[
        'autocomplete' => 'off',
        'inputDefaults' => ['div' => false],
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate'
    ]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?php
            // Menu déroulant rgpd
            echo $this->WebcilForm->input('rgpd', [
                'id' => 'rgpd',
                'options' => $listeOrganisations,
                'value' => $organisationRGPD['Organisation']['id'],
                'class' => 'form-control usersDeroulant',
                'label' => [
                    'text' => __d('organisation', 'organisation.champEntiteResponsableRGPD'),
                    'class' => 'col-md-4 control-label'
                ],
                'empty' => false,
                'between' => '<div class="col-md-8">',
                'after' => '</div>',
                'placeholder' => false,
            ]);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->Form->end();
