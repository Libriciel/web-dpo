<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('organisation', 'organisation.btnFiltrerEntite')
]);

$filters = $this->request->data;
unset($filters['sort'], $filters['direction'], $filters['page'], $filters['Soustraitant']['nbAffichage']);
if (empty($filters['Soustraitant'])) {
    unset($filters['Soustraitant']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
            <?php
            // Filtrer par organisation
            echo $this->Form->input('organisation', [
                'class' => 'usersDeroulant form-control',
                'label' => __d('soustraitant', 'soustraitant.champFiltreEntite'),
                'empty' => true,
                'data-placeholder' => __d( 'soustraitant', 'soustraitant.placeholderChoisirEntite' ),
                'options' => $mesOrganisations,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
echo $this->element('Buttons/add', [
    'controller' => 'organisations',
    'labelBtn' => ' Ajouter une entité',
    'before' => '<div class="text-left">',
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;
?>

<table class="table table-striped table-hover">
    <thead>
        <tr class="d-flex">
            <!-- Entité -->
            <th class="col-md-6">
                <?php
                echo __d('organisation', 'organisation.titreTableauEntite');
                ?>
            </th>

            <!-- Organisation -->
            <th class="col-md-4">
                <?php
                echo __d('organisation', 'organisation.titreTableauNbUtilisateur');
                ?>
            </th>

            <!-- Actions -->
            <th class="col-md-2">
                <?php
                echo __d('organisation', 'organisation.titreTableauActions');
                ?>
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($organisations as $donnees) {
            ?>
            <tr class="d-flex">
                <td class="tdleft col-md-6">
                    <?php
                    echo $donnees['Organisation']['raisonsociale'];
                    ?>
                </td>

                <td class="tdleft col-md-4">
                    <?php
                    echo $donnees['Count'];
                    ?>
                </td>

                <td class="tdleft col-md-2">
                    <div class="buttons">
                        <?php
                        echo $this->element('Buttons/show', [
                            'controller' => 'organisations',
                            'id' => $donnees['Organisation']['id'],
                            'titleBtn' => __d('organisation','organisation.commentaireVIsualiserEntite')
                        ]);

                        if ($donnees['Count'] == 0 &&
                            $this->Autorisation->isSu()
                        ) {
                            echo $this->element('Buttons/delete', [
                                'controller' => 'organisations',
                                'id' => $donnees['Organisation']['id'],
                                'titleBtn' => 'Supprimer cette organisation',
                                'confirmation' => 'Voulez vous vraiment supprimer l\'entité ' . $donnees['Organisation']['raisonsociale'],
                            ]);
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
echo $pagination;

echo $this->element('Buttons/add', [
    'controller' => 'organisations',
    'labelBtn' => ' Ajouter une entité'
]);
