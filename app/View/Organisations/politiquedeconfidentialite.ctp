<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);
?>

<div id="tabs-wrapper" class="tab-content">
    <?php
    echo __d('rgpd', 'rgpd.textPreambule');
    ?>

    <div class="panel-group" id="accordion">
        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreResponsableTraitements');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse" class="card-body collapse">
                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitreEntité');
                    ?>
                </b>

                <p></p>
                <ul>
                    <?php
                    echo $organisationResponsableTraitement['Organisation']['raisonsociale'];
                    ?>

                    <p></p>
                    <?php
                    echo $organisationResponsableTraitement['Organisation']['adresse'];
                    ?>

                    <p></p>
                    <?php
                    echo sprintf(__d('rgpd', 'rgpd.textNumeroSIRET'),
                        $organisationResponsableTraitement['Organisation']['siret']
                    );
                    ?>

                    <p></p>
                    <?php
                    echo sprintf(__d('rgpd', 'rgpd.textCodeAPE'),
                        $organisationResponsableTraitement['Organisation']['ape']
                    );
                    ?>

                    <p></p>
                    <?php
                    echo sprintf(__d('rgpd', 'rgpd.textNumeroTel'),
                        $organisationResponsableTraitement['Organisation']['telephone']
                    );
                    ?>

                    <p></p>
                    <?php
                    echo sprintf(__d('rgpd', 'rgpd.textEmail'),
                        $organisationResponsableTraitement['Organisation']['email']
                    );
                    ?>
                </ul>

                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitreResponsableTraitements');
                    ?>
                </b>
                <p></p>
                <ul>
                    <?php
                    echo $organisationResponsableTraitement['Organisation']['responsable_nom_complet'];

                    echo '<br>';

                    echo $organisationResponsableTraitement['Organisation']['fonctionresponsable'];
                    ?>
                </ul>

                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitreDPOEntité');
                    ?>
                </b>
                <p></p>
                <ul>
                    <?php
                    echo $organisationResponsableTraitement['Dpo']['nom_complet'];
                    ?>
                    <p></p>
                    <?php
                    echo $organisationResponsableTraitement['Organisation']['emaildpo'];
                    ?>
                </ul>
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle1');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse1" class="card-body collapse">
                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitre1-1Article1');
                    ?>
                </b>

                <p></p>
                <ul>
                    <?php
                    echo __d('rgpd', 'rgpd.textApplication') . VERSION;
                    ?>
                </ul>
                <p></p>

                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitre1-2Article1');
                    ?>
                </b>

                <p></p>
                <ul>
                    <?php
                    echo __d('rgpd', 'rgpd.textInfoLS');
                    ?>
                </ul>
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle2');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse2" class="card-body collapse">
                <b>
                    <?php
                    if (Configure::read('MODE_SAAS') === true) {
                        echo __d('rgpd', 'rgpd.sousTitre2-1Article2SAAS');
                    } else {
                        echo __d('rgpd', 'rgpd.sousTitre2-1Article2NoSAAS');
                    }
                    ?>
                </b>

                <p></p>

                <?php
                if (Configure::read('MODE_SAAS') === true) {
                    ?>
                    <ul>
                        <?php
                        echo __d('rgpd', 'rgpd.textInfoLS');
                        ?>
                    </ul>

                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.sousTitre2-2Article2SAAS');
                        ?>
                    </b>

                    <p></p>
                    <?php
                    echo __d('rgpd', 'rgpd.textHebergeurSAAS');
                } elseif (Configure::read('MODE_SAAS') === false &&
                    !empty($organisationHebergeur)
                ) {
                    ?>
                    <ul>
                        Le serveur applicatif est maintenu par :
                        <p></p>
                        <ul>

                            <?php
                            echo $organisationHebergeur['Organisation']['raisonsociale'];
                            ?>

                            <p></p>
                            <?php
                            echo $organisationHebergeur['Organisation']['adresse'];
                            ?>

                            <p></p>
                            <?php
                            echo sprintf(__d('rgpd', 'rgpd.textRepresenteePar'),
                                $organisationHebergeur['Organisation']['responsable_nom_complet'],
                                $organisationHebergeur['Organisation']['fonctionresponsable']
                            );
                            ?>

                            <p></p>
                            <?php
                            echo sprintf(__d('rgpd', 'rgpd.textNumeroSIRET'),
                                $organisationHebergeur['Organisation']['siret']
                            );
                            ?>

                            <p></p>
                            <?php
                            echo sprintf(__d('rgpd', 'rgpd.textCodeAPE'),
                                $organisationHebergeur['Organisation']['ape']
                            );
                            ?>

                            <p></p>
                            <?php
                            echo sprintf(__d('rgpd', 'rgpd.textNumeroTel'),
                                $organisationHebergeur['Organisation']['telephone']
                            );
                            ?>

                            <p></p>
                            <?php
                            echo sprintf(__d('rgpd', 'rgpd.textEmail'),
                                $organisationHebergeur['Organisation']['email']
                            );
                            ?>
                        </ul>
                    </ul>
                    <?php
                }
                ?>
             </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle3');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse3" class="card-body collapse">
                Lors de la création de votre compte utilisateur, les informations suivantes sont collectées obligatoirement :
                <ul>
                    <li>
                        Civilité de l'utilisateur
                    </li>
                    <li>
                        Nom d'utilisateur
                    </li>
                    <li>
                        Prénom de l'utilisateur
                    </li>
                    <li>
                        Adresse de messagerie professionnelle
                    </li>
                    <li>
                        Identifiant de connexion
                    </li>
                </ul>
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle4');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse4" class="card-body collapse">
                Vos informations personnelles sont utilisées pour le bon fonctionnement de web-DPO dans les cas listés ci-après :

                <p></p>

                Affichage de vos données personnelles :
                <ul>
                    <li>
                        liste des utilisateurs : votre identifiant de connexion, votre civilité, votre prénom, votre nom et votre adresse de messagerie professionnelle apparaissent dans la liste
                    </li>

                    <li>
                        journal d'événement d'un traitement : votre prénom et votre nom apparaît pour chaque action (rédaction, modification, validation, consultation) de votre part sur un traitement.
                    </li>

                    <li>
                        dans un traitement : votre civilité, votre prénom, votre nom, votre e-mail, votre numéro de téléphone fixe (si renseigné) et votre numéro de téléphone portable (si renseigné) sont utilisés pour connaître la personne à l'origine de la déclaration d'un traitement.
                    </li>

                    <li>
                        lors de la génération documentaire : votre civilité, votre prénom, votre nom, votre e-mail, votre numéro de téléphone fixe et votre numéro de téléphone portable peuvent apparaître dans le document PDF généré en fonction des informations qui ont été paramétré dans le modèle de document.
                    </li>
                </ul>
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle5');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse5" class="card-body collapse">
                <b>
                    <?php
                    echo __d('rgpd', 'rgpd.sousTitre5-1Article2');
                    ?>
                </b>

                <p></p>

                <ul>
                    <?php
                    echo "Ces cookies sont nécessaires au fonctionnement de l'application et ne peuvent pas être désactivés.";

                    echo '<br>';
                    echo '<br>';

                    echo "Nous utilisons un cookie de session qui est utilisé pour stocker les informations de votre session en cours. Les informations, telles que les données de connexion ou les formulaires en ligne déjà remplis, sont également conservées pendant la session. Toutefois, si vous mettez fin à votre session de navigation ou que votre session expire (30 minutes par défaut), l’identifiant de session et toutes les autres données stockées seront effacés."
                    ?>
                </ul>
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle6');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse6" class="card-body collapse">
                Sur votre demande ou à la demande de l’administrateur de web-DPO, votre compte utilisateur peut être supprimé définitivement.
                <p></p>
                La suppression de votre compte laissera les traces de votre activité sur les documents circulant dans web-DPO, le temps de leur présence.
                <p></p>
                Ainsi que dans l'historique des traitements réalisés.
                <p></p>
                Ces traces disparaîtront avec l'extraction de ces documents hors de web-DPO.
            </div>
        </div>

        <div class="card border-primary">
            <h4 class="card-header">
                <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                    <b>
                        <?php
                        echo __d('rgpd', 'rgpd.titreArticle7');
                        ?>
                    </b>
                </a>
            </h4>
            <div id="collapse7" class="card-body collapse">
                Vous pouvez accéder et obtenir une copie des données vous concernant, vous opposer au traitement de ces données, les faire rectifier ou les faire effacer. Vous disposez également d'un droit à la limitation du traitement de vos données.
                <p></p>

                <a href = "https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles" target="_blank">> Comprendre vos droits informatique et libertés</a>

                <p></p>

                <h3>
                    Exercer ses droits
                </h3>
                Pour exercer vos droits sur les données personnelles vous concernant, vous pouvez contacter le Délégué à la Protection des Données (DPO), à l’adresse suivante :
                <ul>
                    <li>
                        <?php
                        echo $organisationResponsableTraitement['Organisation']['emaildpo'];
                        ?>
                    </li>
                </ul>

                <p></p>

                <h3>
                    Réclamation (plainte) auprès de la CNIL
                </h3>
                Si vous estimez, après nous avoir contacté, que vos droits sur vos données ne sont pas respectés, vous pouvez <a href = "https://www.cnil.fr/fr/webform/adresser-une-plainte" target="_blank">adresser une réclamation (plainte) à la CNIL.</a>
            </div>
        </div>
    </div>
</div>
