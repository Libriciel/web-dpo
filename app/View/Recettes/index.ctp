<style type="text/css" media="all">
    td ul, td li {
        margin: 0;
        padding: 0;
    }
    td ul {
        padding-left: 0.666em;
    }
</style>
<?php
    $this->set('title', sprintf('Recette v. %s', VERSION));
    $yes = '<span class="fas fa-check-square fa-fw"> </span><span class="sr-only">Oui</span>';
    $no = '<span class="far fa-square fa-fw"> </span><span class="sr-only">Non</span>';
?>

<h2>Entités</h2>

<div class="table-responsive">
    <table border="1" cellpadding="1" cellspacing="1" class="table">
        <thead>
            <tr>
                <th rowspan="2">Raison sociale</th>
                <th rowspan="2">Profils</th>
                <th rowspan="2">Services</th>
                <th rowspan="2">Utilisateurs</th>
                <th colspan="3">Associations</th>
            </tr>
            <tr>
                <th>Sous-traitants</th>
                <th>Responsables</th>
                <th>Types d'annexes</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($organisations as $organisation):?>
            <tr>
                <td><?php echo $organisation['Organisation']['raisonsociale']?></td>
                <?php
                $colums = [
                    'Role.{n}.libelle',
                    'Service.{n}.libelle',
                    'User.{n}.username',
                    'Soustraitant.{n}.raisonsocialestructure',
                    'Responsable.{n}.nom_complet',
                    'Typage.{n}.libelle',
                ];
                ?>
                <?php foreach($colums as $column):?>
                    <td>
                        <?php
                        $items = Hash::extract($organisation, $column);
                        if (empty($items) === false) {
                            echo sprintf('<ul><li>%s</li></ul>', implode('</li><li>', $items));
                        }
                        ?>
                    </td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php /*debug($organisations);*/?>

<h2>Utilisateurs</h2>

<div class="table-responsive">
    <table border="1" cellpadding="1" cellspacing="1" class="table">
        <thead>
        <tr>
            <th>Identifiant</th>
            <th>Nom</th>
            <th>Mot de passe</th>
            <th>Courriel</th>
            <th>Notifications</th>
            <th>Spécial</th>
            <th>Entité</th>
            <th>Profil</th>
            <th>Services</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user):?>
            <tr>
            <?php
            $links = 0;
            if (isset($user['Organisation']) === true) {
                $links = count($user['Organisation']);
            }
            $rowspan = $links < 1 ? 1 : $links;
            ?>
            <td rowspan="<?php echo $rowspan;?>"><?php echo $user['User']['username'];?></td>
            <td rowspan="<?php echo $rowspan;?>"><?php echo $user['User']['nom_complet'];?></td>
            <td rowspan="<?php echo $rowspan;?>">
                <?php
                if ($user['User']['password'] === true) {
                    echo $user['User']['pwd'];
                } else {
                    printf('<span class="text-danger"><strong>Devrait être:</strong> %s</span>', $user['User']['pwd']);
                }
                ?>
            </td>
            <td rowspan="<?php echo $rowspan;?>"><?php echo $user['User']['email'];?></td>
            <td rowspan="<?php echo $rowspan;?>"><?php echo $user['User']['notification'] ? $yes : $no;?></td>
            <?php if ($links === 0):?>
                <td>Superadmin</td>
                <td colspan="3"></td>
            <?php else:?>
                <?php foreach ($user['Organisation'] as $uo):?>
                    <td><?php
                        if ($uo['User']['is_dpo'] === true) {
                            echo 'DPO';
                        }
                        ?></td>
                    <td><?php echo $uo['raisonsociale']?></td>
                    <td><?php echo $uo['OrganisationUser']['OrganisationUserRole'][0]['Role']['libelle'];?></td>
                    <td>
                        <?php
                        $services = Hash::extract($uo, 'OrganisationUser.OrganisationUserService.Service.libelle');
                        if (empty($services) === false) {
                            echo sprintf('<ul><li>%s</li></ul>', implode('</li><li>', $services));
                        }
                        ?>
                    </td>
                    </tr><tr>
                <?php endforeach;?>
            <?php endif;?>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>

<h2>Formulaires</h2>

<div class="table-responsive">
    <table border="1" cellpadding="1" cellspacing="1" class="table">
        <thead>
        <tr>
            <th>Entité</th>
            <th>Formulaire</th>
            <th>Registre</th>
            <th>Actif</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($formulaires as $formulaire):?>
            <tr>
                <td><?php echo $formulaire['Organisation']['raisonsociale'];?></td>
                <td><?php echo $formulaire['Formulaire']['rt_externe'] ? 'Sous-traitance' : 'Activité';?></td>
                <td><?php echo $formulaire['Formulaire']['libelle'];?></td>
                <td><?php echo $formulaire['Formulaire']['active'] ? $yes : $no;?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php /*debug($formulaires);*/?>
