<div class="container-fluid" role="main">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#software">
                Environnement logiciel
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#webdpo">
                Application
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#services">
                Services
            </a>
        </li>
    </ul>

    <div id="accordion" class="tab-content panel-group">
        <div id="software" class="tab-pane active">
            <br>
            <?php
            echo $this->Checks->accordions(
                [
                    'Binaires' => $this->Checks->table($results['Environment']['binaries']),
                    'Installation' =>
                        $this->Html->tag('h4', 'Répertoires')
                        . $this->Checks->table($results['Environment']['directories'])
                        . $this->Html->tag('h4', 'Fichiers')
                        . $this->Checks->table($results['Environment']['files'])
                        . $this->Html->tag('h4', 'Cache')
                        . $this->Checks->table($results['Environment']['cache'])
                        . $this->Html->tag('h4', 'Accès au cache')
                        . $this->Checks->table($results['Environment']['cache_check'])
                        . $this->Html->tag('h4', 'Espace libre')
                        . $this->Checks->table($results['Environment']['freespace']),
                    'PHP' =>
                        $this->Checks->table($results['Php']['informations'])
                        . $this->Html->tag('h4', 'Configuration')
                        . $this->Checks->table($results['Php']['inis'])
                        . $this->Html->tag('h4', 'Extensions')
                        . $this->Checks->table($results['Php']['extensions'])
                        . $this->Html->tag('h4', 'Extensions PEAR')
                        . $this->Checks->table($results['Php']['pear_extensions'])

                ],
                [
                    'level' => 3,
                    'id' => false
                ]
            );
            ?>
        </div>

        <div id="webdpo" class="tab-pane">
            <br>
            <?php
            echo $this->Checks->accordions(
                [
                    'Configuration' => $this->Checks->table($results['WebDPO']['configure']),
                    'Définitions' => $this->Checks->table($results['WebDPO']['define']),
                ],
                [
                    'level' => 3,
                    'id' => false
                ]
            );
            ?>
        </div>

        <div id="services" class="tab-pane">
            <br>
            <?php
            $data = [];
            foreach ($results['Services'] as $name => $service) {
                $data[$name] = $this->Checks->table( $service['tests'] );
            }

            echo $this->Checks->accordions($data, ['level' => 3, 'id' => false]);
            ?>
        </div>
    </div>
</div>

<?php
echo $this->Checks->javascript();
?>
