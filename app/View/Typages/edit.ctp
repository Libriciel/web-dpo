<?php
$breadcrumbs = [
    __d('typage', 'typage.titreListeTypages') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Typage']) && !empty($this->validationErrors['Typage'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Typage',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate'
]);
?>

<div class="users form">
    <div class="row">
        <!-- Colonne de gauche -->
        <div class="col-md-6">
            <?php
                echo $this->WebcilForm->input('libelle', [
                    'id' => 'libelle',
                    'required' => true
                ]);
            ?>
        </div>
    </div>
</div>

<div style="clear: both">
    <?php
        echo $this->WebcilForm->buttons(['Cancel', 'Save']);
    ?>
</div>

<?php
    echo $this->WebcilForm->end();
