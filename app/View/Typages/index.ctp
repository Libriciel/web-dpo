<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$refererActionAdminIndex = '/organisations/administrer' === $referer;

$breadcrumbs = [
    $title => []
];
if ($refererActionAdminIndex === true) {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs, true);
} else {
    $this->Breadcrumbs->breadcrumbs($breadcrumbs);
}

// Bouton du filtre de la liste
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('typage', 'typage.btnFiltrerTypage')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('Filtre', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row col-md-12">
        <div class="col-md-6">
        <?php
        // Filtrer par entité associée
        echo $this->element('Filtres/Parametrages/organisation_associee', [
            'options' => $options['organisations']
        ]);
        ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtrer par entité créatrice
            echo $this->element('Filtres/Parametrages/organisation_creatrice', [
                'options' => $options['organisations']
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<?php
if (!empty($typages)) {
    echo $this->element('Buttons/add', [
        'controller' => 'typages',
        'labelBtn' => __d('typage','typage.btnAjouterType'),
        'before' => '<div class="text-left">',
    ]);

    $pagination = null;

    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;

    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('typage', 'typage.btnAffecterEntite')
    ]);
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                    <input id="typageOrganisationCheckbox" type="checkbox" class="typageOrganisationCheckbox_checkbox"/>
                </th>

                <!-- Libelle -->
                <th class="col-md-4">
                    <?php
                    echo __d('typage', 'typage.titreTableauLibelle');
                    ?>
                </th>

                <!-- Entité -->
                <th class="col-md-4">
                    <?php
                    echo __d('typage', 'typage.titreTableauEntite');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-3">
                    <?php
                    echo __d('typage', 'typage.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($typages as $typage) {
                ?>
                <tr class="d-flex">
                    <!-- Casse à coché pour associer un responsable à une entité -->
                    <td class="tdleft col-md-1">
                        <input id='checkbox<?php echo $typage['Typage']['id']; ?>' type="checkbox"
                               class="typageOrganisationCheckbox" value="<?php echo $typage['Typage']['id']; ?>" >
                    </td>

                    <!--  Prenom Nom co-responsable -->
                    <td class="tdleft col-md-4">
                        <?php
                        echo $typage['Typage']['libelle'];
                        ?>
                    </td>

                    <!-- Entité -->
                    <td class="tdleft col-md-4">
                        <?php
                        $organisations = Hash::extract($typage, 'Organisation.{n}.id');
                        if (empty($organisations) === false) {
                            echo '<ul><li>' . implode('</li><li>', Hash::extract($typage, 'Organisation.{n}.raisonsociale')) . '</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-3">
                        <div class="buttons">
                            <?php
                            if ($this->Session->read('Su') || $typage['Typage']['createdbyorganisation'] == $this->Session->read('Organisation.id')) {
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'typages',
                                    'id' => $typage['Typage']['id'],
                                    'titleBtn' => __d('typage', 'typage.commentaireBtnModifierType')
                                ]);

                                if (empty($organisations) === true) {
                                    echo $this->element('Buttons/delete', [
                                        'controller' => 'typages',
                                        'id' => $typage['Typage']['id'],
                                        'titleBtn' => __d('typage', 'typage.commentaireBtnDeleteType'),
                                        'confirmation' => __d('typage', 'typage.confirmationDeleteType') . $typage['Typage']['libelle'] . ' ?',
                                    ]);
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo [] === Hash::filter($filters)
                ? __d('typage', 'typage.textAucunTypage')
                : __d('typage', 'typage.filtreTextAucunTypage');
            ?>
        </h3>
    </div>
    <?php
}

echo $this->element('Buttons/add', [
    'controller' => 'typages',
    'labelBtn' => __d('typage','typage.btnAjouterType'),
]);

echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'Typage',
    'modalTitle' => __d('typage', 'typage.popupTitreAffecterType'),
    'formulaireName' => 'TypageOrganisation',
    'fieldName' => 'typage_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'typageOrganisationCheckbox'
]);
