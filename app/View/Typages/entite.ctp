<?php
$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

$pagination = null;

if (!empty($typages)) {
    $this->Paginator->options([
            'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!--  Libelle-->
                <th class="col-md-10">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauResponsable');
                    ?>
                </th>

                <!-- Actions -->
                <th class="col-md-2">
                    <?php
                    echo __d('responsable', 'responsable.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($typages as $typage) {
                ?>
                <tr class="d-flex">
                    <!-- Libelle -->
                    <td class="tdleft col-md-10">
                        <?php
                        echo $typage['Typage']['libelle'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/dissocier', [
                                'controller' => 'typages',
                                'id' => $typage['Typage']['id'],
                                'titleBtn' => __d('typage', 'typage.btnDissocier'),
                                'confirmation' => __d('typage', 'typage.confirmationDissocierType') . ' " ' . $typage['Typage']['libelle'] . ' " de votre entité ?',
                            ]);
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('typage', 'typage.textAucunTypageEntite');
            ?>
        </h3>
    </div>
    <?php
}
