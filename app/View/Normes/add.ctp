<?php
$breadcrumbs = [
    __d('norme', 'norme.titreListeNorme') => [
        'action' => 'index',
        'prepend' => true
    ],
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

if (isset($this->validationErrors['Norme']) && !empty($this->validationErrors['Norme'])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"><!----></span>
        <span class="sr-only">Error:</span>
        Ces erreurs se sont produites:
        <ul>
            <?php
            foreach ($this->validationErrors as $donnees) {
                foreach ($donnees as $champ) {
                    foreach ($champ as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}

echo $this->WebcilForm->create('Norme',[
    'autocomplete' => 'off',
    'inputDefaults' => ['div' => false],
    'class' => 'form-horizontal',
    'novalidate' => 'novalidate',
    'type' => 'file'
]);
?>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->input('norme', [
            'id' => 'norme',
            'options' => $options['Norme']['norme'],
            'class' => 'form-control custom-select',
            'empty' => true,
            'required' => true,
            'placeholder' => false
        ]);
        ?>
    </div>

    <div class="col-md-1 text-center">
        <i style="margin-left:10px; margin-top: 30px" class="fa fa-arrow-right pull-left"></i>
    </div>

    <div class="col-md-5">
        <?php
        echo $this->WebcilForm->input('prefixNumero', [
            'id' => 'prefixNumero',
            'label' => [
                'text' => false,
                'class' => false
            ],
            'readonly' => true,
            'placeholder' => false
        ]);
        ?>
    </div>
</div>

<div class="row col-md-12">
    <div class="col-md-6">
        <?php
        echo $this->WebcilForm->inputs([
            'numero' => [
                'id' => 'numero',
                'type' => 'text',
                'required' => true,
                'maxlength' => 3
            ],
            'libelle' => [
                'id' => 'libelle',
                'required' => true
            ],
            'description' => [
                'id' => 'description',
                'type' => 'textarea'
            ]
        ]);

        // Ajouter le PDF de la norme
        echo $this->WebcilForm->input('Norme.fichier', [
            'id' => 'logo_file',
            'type' => 'file',
            'class' => 'filestyle',
            'data-text' => ' Ajouter le PDF de la norme',
            'data-buttonBefore' => 'true',
            'accept' => 'application/pdf',
            'label' => [
                'text' => false
            ]
        ]);
        ?>
    </div>
</div>

<?php
echo $this->WebcilForm->buttons(['Cancel', 'Save']);

echo $this->WebcilForm->end();
?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#numero').mask("000", {
            placeholder: "___"
        });

        let prefixNorme = "",
            numeroNorme = "",
            libelleNorme = "";

        // Lors d'action sur le menu déroulant
        $('#norme').change(function () {
            prefixNorme = $(this).val();
            $('#prefixNumero').val(prefixNorme + numeroNorme + libelleNorme);
        });

        $('#numero').change(function () {

            if ($(this).val() === '') {
                numeroNorme = "";
            } else {
                numeroNorme = '-' + $(this).val();
            }

            $('#prefixNumero').val(prefixNorme + numeroNorme + libelleNorme);
        });

        $('#libelle').change(function () {
            if ($(this).val() === '') {
                libelleNorme = "";
            } else {
                libelleNorme = ' : ' + $(this).val();
            }

            $('#prefixNumero').val(prefixNorme + numeroNorme + libelleNorme);
        });

    });
</script>
