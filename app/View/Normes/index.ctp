<?php
echo $this->Html->script('filtre.js');
echo $this->Html->css('filtre.css');

$breadcrumbs = [
    $title => []
];
$this->Breadcrumbs->breadcrumbs($breadcrumbs, true);

// Bouton du filtre des normes
echo $this->element('Buttons/filtre', [
    'titleBtn' => __d('norme', 'norme.btnFiltrerNorme')
]);

$filters = $this->request->data;

unset($filters['sort'], $filters['direction'], $filters['page']);
if (empty($filters['Filtre'])) {
    unset($filters['Filtre']);
}
?>

<!-- Filtre -->
<div id="divFiltrage" <?php if(true === empty($filters)) {echo 'style="display: none;"';}?>>
    <?php
    echo $this->Form->create('normes', [
        'url' => [
            'controller' => $this->request->params['controller'],
            'action' => $this->request->params['action']
        ],
        'class' => 'search-form'
    ]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?php
            // Filtrer par norme
            echo $this->Form->input('norme', [
                'empty' => true,
                'data-placeholder' => __d('norme', 'norme.placeholderFiltreNorme'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('norme', 'norme.filtreNorme'),
                'options' => $options,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtrer par Abroger
            echo $this->Form->input('abroger', [
                'empty' => true,
                'data-placeholder' => __d('norme', 'norme.placeholderFiltreAbroger'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('norme', 'norme.filtreAbroger'),
                'options' => $abroger,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?php
            // Filtrer par organisation
            echo $this->Form->input('numero', [
                'empty' => true,
                'data-placeholder' => __d('norme', 'norme.placeholderFiltreNumero'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('norme', 'norme.filtreNumero'),
                'options' => $numeros,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);

            // Filtrer par nom de norme
            echo $this->Form->input('nomnorme', [
                'empty' => true,
                'data-placeholder' => __d('norme', 'norme.placeholderFiltreNomNorme'),
                'class' => 'usersDeroulant form-control',
                'label' => __d('norme', 'norme.filtreNomNorme'),
                'options' => $options_normes,
                'before' => '<div class="form-group">',
                'after' => '</div>'
            ]);
            ?>
        </div>
    </div>

    <?php
    echo $this->element('Filtres/Parametrages/buttons');
    echo $this->Form->end();
    ?>
</div>

<div class="text-left">
    <?php
    // Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
    if ($this->Autorisation->authorized(19, $droits)) {
        echo $this->element('Buttons/add', [
            'controller' => 'normes',
            'labelBtn' => __d('norme', 'norme.btnAjouterNorme'),
            'before' => '<div class="text-left">',
        ]);
    }
    ?>
</div>

<?php
if (!empty($normes)) {
    $this->Paginator->options([
        'url' => Hash::flatten( (array)$this->request->data, '.' )
    ]);
    $pagination = $this->element('pagination');

    echo $pagination;
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- Abroger -->
                <th class="col-md-1">
                    <?php
                    echo __d('norme', 'norme.titreTableauEtat');
                    ?>
                </th>

                <!-- Norme -->
                <th class="col-md-1">
                    <?php
                    echo __d('norme', 'norme.titreTableauNorme');
                    ?>
                </th>

                <!-- Libelle -->
                <th class="col-md-3">
                    <?php
                    echo __d('norme', 'norme.titreTableauLibelle');
                    ?>
                </th>

                <!-- Description -->
                <th class="col-md-5">
                    <?php
                    echo __d('norme', 'norme.titreTableauDescription');
                    ?>
                </th>

                <!-- Actions -->
                <th class='col-md-2'>
                    <?php
                    echo __d('norme', 'norme.titreTableauAction');
                    ?>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($normes as $donnees) {
                ?>
                <tr class="d-flex">
                    <!-- Status du formulaire -->
                    <td class="tdleft col-md-1">
                        <?php
                        if ($donnees['Norme']['abroger'] === true) {
                            ?>
                            <div class="text-center">
                                <span class="badge badge-danger">
                                    <?php
                                    echo __d('norme', 'norme.titreTableauAbroger');
                                    ?>
                                </span>
                            </div>
                            <?php
                        }
                        ?>
                    </td>

                    <!-- Norme -->
                    <td class="tdleft col-md-1">
                        <?php
                        $title = Hash::get($options, "Norme.norme.{$donnees['Norme']['norme']}");
                        $abbr = $this->Html->tag('abbr', $donnees['Norme']['norme'], ['title' => $title]);

                        echo $abbr . "-" . sprintf('%03d', $donnees['Norme']['numero']);
                        ?>
                    </td>

                    <!-- Libelle -->
                    <td class="tdleft col-md-3">
                        <?php
                        echo $donnees['Norme']['libelle'];
                        ?>
                    </td>

                    <!-- Description -->
                    <td class="tdleft col-md-5">
                        <?php
                        echo $donnees['Norme']['description'];
                        ?>
                    </td>

                    <!-- Bouton -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            if (!empty($donnees['Norme']['fichier'])) {
                                echo $this->element('Buttons/download', [
                                    'controller' => 'normes',
                                    'fichier' => $donnees['Norme']['fichier'],
                                    'name_modele' => $donnees['Norme']['name_fichier'],
                                    'titleBtn' => 'Télécharger la norme'
                                ]);
                            }

                            if ($this->Autorisation->authorized(21, $droits) &&
                                $donnees['Norme']['abroger'] == false
                            ) {
                                // Bouton de modification
                                echo $this->element('Buttons/edit', [
                                    'controller' => 'normes',
                                    'id' => $donnees['Norme']['id'],
                                    'titleBtn' => __d('norme', 'norme.commentaitreModifierNorme')
                                ]);
                            }

                            if ($this->Autorisation->authorized(22, $droits)) {
                                if ($donnees['Norme']['abroger'] == false) {
                                    // Bouton abroger la norme
                                    echo $this->Html->link('<i class="fa fa-toggle-on fa-lg"></i>', [
                                        'controller' => 'normes',
                                        'action' => 'abroger',
                                        $donnees['Norme']['id']
                                    ], [
                                        'class' => 'btn btn-outline-danger borderless',
                                        'escape' => false,
                                        'title' => __d('norme', 'norme.commentaireAbrogerNorme')
                                    ], __d('norme', 'norme.confirmationAbrogerNorme') . ' " ' . $donnees['Norme']['norme'] . "-" . sprintf('%03d', $donnees['Norme']['numero']) . ' " ?');
                                } else {
                                    // Bouton revoquer abrogation de la norme
                                    echo $this->Html->link('<i class="fa fa-toggle-off fa-lg"></i>', [
                                        'controller' => 'normes',
                                        'action' => 'revoquerAbrogation',
                                        $donnees['Norme']['id']
                                    ], [
                                        'class' => 'btn btn-outline-success borderless',
                                        'escape' => false,
                                        'title' => __d('norme', 'norme.commentaireRevoquerAbrogerNorme')
                                    ], __d('norme', 'norme.confirmationRevoquerAbrogerNorme') . ' " ' . $donnees['Norme']['norme'] . "-" . sprintf('%03d', $donnees['Norme']['numero']) . ' " ?');
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
    echo $pagination;
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            if ([] === Hash::filter($filters)) {
                echo 'admin_index' === $this->request->params['action']
                        ? __d('user', 'user.textAucunUser')
                        : __d('user', 'user.textAucunUserCollectiviter');
            } else {
                echo [] === Hash::filter($filters)
                    ? __d('norme', 'norme.AucuneNorme')
                    : __d('norme', 'norme.filtreAucuneNorme');
            }
            ?>
        </h3>
    </div>
    <?php
}

//Si les droits de l'utilisateur le permet, affichage du bouton "+ Ajouter un profil"
if ($this->Autorisation->authorized(19, $droits)) {
    echo $this->element('Buttons/add', [
        'controller' => 'normes',
        'labelBtn' => __d('norme', 'norme.btnAjouterNorme'),
    ]);
}
