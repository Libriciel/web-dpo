<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

echo $this->element('Buttons/infoVariable', [
    'controller' => 'modeles',
    'titleBtn' => __d('modele_extrait', 'modele_extrait.btnInfoVariableModeleExtrait'),
    'classBtn' => 'btn-outline-dark float-right',
    'labelBtn' => __d('modele', 'modele.commentaireVariableModel'),
]);

$pagination = null;

$this->Paginator->options([
    'url' => Hash::flatten( (array)$this->request->data, '.' )
]);
$pagination = $this->element('pagination');

echo $pagination;

if (!empty($modelesExtrait)) {
    echo $this->element('Buttons/affecterEntite', [
        'titleBtn' => __d('modele_extrait', 'modele_extrait.popupTitreAffecterModeleExtrait')
    ]);
    ?>

    <!-- Tableau modèles pour l'extrait de registre -->
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <!-- checkbox -->
                <th class="col-md-1">
                </th>

                <th class="col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauModele');
                    ?>
                </th>

                <th class="col-md-3">
                    <?php
                    echo __d('modele', 'modele.titreTableauFichierModele');
                    ?>
                </th>

                <!-- Entité -->
                <th class="col-md-3">
                    <?php
                    echo __d('typage', 'typage.titreTableauEntite');
                    ?>
                </th>

                <th class="col-md-2">
                    <?php
                    echo __d('modele', 'modele.titreTableauActions');
                    ?>
                </th>
            </tr>
        </thead>

        <!-- Info tableau -->
        <tbody>
            <?php
            foreach ($modelesExtrait as $key => $modeleExtrait) {
                ?>
                <tr class="d-flex">
                    <td class="tdleft col-md-1">
                        <?php
                        if (!empty($mesOrganisations)) {
                            ?>
                            <input id='checkbox<?php echo $modeleExtrait['ModeleExtraitRegistre']['id']; ?>' type="checkbox"
                                   class="modeleExtraitRegistreCheckbox" value="<?php echo $modeleExtrait['ModeleExtraitRegistre']['id']; ?>" >
                            <?php
                        }
                        ?>
                    </td>

                    <td class="tdleft col-md-3">
                        <?php
                        echo __d('modele', 'modele.textTableauExtraitRegistreModele');
                        ?>
                    </td>

                    <!-- Nom du fichier -->
                    <td class="tdleft col-md-3">
                        <i class="far fa-file-alt fa-lg fa-fw"></i>
                        <?php
                        echo $modeleExtrait['ModeleExtraitRegistre']['name_modele'];
                        ?>
                    </td>

                    <td class="tdleft col-md-3">
                        <?php
                        $organisations = Hash::extract($modeleExtrait, 'ModeleExtraitRegistreOrganisation.{n}.Organisation.id');
                        if (empty($organisations) === false) {
                            echo '<ul><li>' . implode('</li><li>', Hash::extract($modeleExtrait, 'ModeleExtraitRegistreOrganisation.{n}.Organisation.raisonsociale')) . '</li></ul>';
                        }
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/download', [
                                'controller' => 'modeleExtraitRegistres',
                                'fichier' => $modeleExtrait['ModeleExtraitRegistre']['fichier'],
                                'name_modele' => $modeleExtrait['ModeleExtraitRegistre']['name_modele'],
                                'titleBtn' => __d('modele', 'modele.commentaireTelechargerModel')
                            ]);

                            if (empty($organisations) === true) {
                                echo $this->element('Buttons/delete', [
                                    'controller' => 'modeleExtraitRegistres',
                                    'id' => $modeleExtrait['ModeleExtraitRegistre']['id'],
                                    'titleBtn' => __d('modele', 'modele.commentaireSupprimerModel'),
                                    'confirmation' => __d('modele', 'modele.confirmationSupprimerModel')
                                ]);
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('modele_extrait', 'modele_extrait.textAucunModeleExtrait');
            ?>
        </h3>
    </div>
    <?php
}
?>

<!--Bouton de création d'un modele -->
<div class="row text-center">
    <div class="col-md-12 text-center">
        <button id="btnAddModeleExtraitToForm" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAddModeleExtrait">
            <span class="fa fa-plus fa-lg"></span>
            <?php
            echo __d('modele_extrait', 'modele_extrait.btnImporterModeleExtrai');
            ?>
        </button>
    </div>
</div>

<?php
echo $this->element('Default/modalAddElementToEntity', [
    'controller' => 'ModeleExtraitRegistre',
    'modalTitle' => __d('modele_extrait', 'modele_extrait.popupTitreAffecterModeleExtrait'),
    'formulaireName' => 'ModeleExtraitRegistre',
    'fieldName' => 'modeleextraitregistre_id',
    'btnAffecter' => 'btnAffecterEntite',
    'checkboxID' => 'modeleExtraitRegistreCheckbox',
    'justOneCheckbox' => true
]);

echo $this->element('Modele_Extrait/modalAddModeleExtraitToEntity', [
    'modal_id' => 'modalAddModeleExtrait',
    'controller' => 'modeleExtraitRegistre',
]);
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');

        let mesOrganisations = <?php echo json_encode($mesOrganisations)?>;
        if (mesOrganisations.length === 0) {
            $('#btnAddModeleExtraitToForm').attr('disabled', true);
        } else {
            $('#btnAddModeleExtraitToForm').removeAttr('disabled');
        }
    });

</script>
