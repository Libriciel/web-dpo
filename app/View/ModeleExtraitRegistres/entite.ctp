<?php
$this->Breadcrumbs->breadcrumbs([
    $title => []
]);

if (!empty($modelesExtrait)) {
    ?>
    <!-- Tableau modèles pour l'extrait de registre -->
    <table class="table table-striped table-hover">
        <thead>
            <tr class="d-flex">
                <th class="col-md-5">
                    <?php 
                    echo __d('modele', 'modele.titreTableauFormulaire');
                    ?>
                </th>
                
                <th class="col-md-5">
                    <?php 
                    echo __d('modele', 'modele.titreTableauFichierModele');
                    ?>
                </th>

                <th class="col-md-2">
                    <?php 
                    echo __d('modele', 'modele.titreTableauActions');
                    ?>
                </th>
            </tr>
        </thead>

        <!-- Info tableau -->
        <tbody>
            <?php
            foreach ($modelesExtrait as $key => $modeleExtrait) {
                ?>
                <tr class="d-flex">
                    <td class="tdleft col-md-5">
                        <?php
                        echo __d('modele', 'modele.textTableauExtraitRegistreModele');
                        ?>
                    </td>

                    <!-- Nom du fichier -->
                    <td class="tdleft col-md-5">
                        <i class="far fa-file-alt fa-lg fa-fw"></i>
                        <?php
                        echo $modeleExtrait['ModeleExtraitRegistre']['name_modele'];
                        ?>
                    </td>

                    <!-- Actions -->
                    <td class="tdleft col-md-2">
                        <div class="buttons">
                            <?php
                            echo $this->element('Buttons/download', [
                                'controller' => 'modeleExtraitRegistres',
                                'fichier' => $modeleExtrait['ModeleExtraitRegistre']['fichier'],
                                'name_modele' => $modeleExtrait['ModeleExtraitRegistre']['name_modele'],
                                'titleBtn' => __d('modele', 'modele.commentaireTelechargerModel')
                            ]);

                            echo $this->element('Buttons/dissocier', [
                                'controller' => 'modeleExtraitRegistres',
                                'titleBtn' => __d('modele_extrait', 'modele_extrait.btnDissocier'),
                                'confirmation' => __d('modele_extrait', 'modele_extrait.confirmationDissocierModeleExtrait'),
                            ]);
                            ?>
                        </div>

                        <?php
                        echo $this->element('Buttons/infoVariable', [
                            'controller' => 'modeles',
                            'titleBtn' => __d('modele', 'modele.commentaireVariableModel'),
                        ]);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
} else {
    ?>
    <div class='text-center'>
        <h3>
            <?php
            echo __d('modele_extrait', 'modele_extrait.textAucunModeleExtraitOrganisation');
            ?>
        </h3>
    </div>
    <?php
}
?>

<script type="text/javascript">

    $(document).ready(function () {
        
        $("span.icon-span-filestyle").removeClass('icon-span-filestyle');
        
    });

</script>
