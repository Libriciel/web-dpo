<?php

App::uses('AppHelper', 'View/Helper');
App::uses('ListeDroit', 'Model');

class AutorisationHelper extends AppHelper
{
    public $helpers = [
        'Session'
    ];

    public function authorized($level, $table = null)
    {
        if ($table === null) {
            $table = (array)$this->Session->read('Droit.liste');
        }

        if ( $this->isSu() ) {
            return true;
        }
        if ( is_array($level) ) {
            foreach ( $level as $value ) {
                foreach ( $table as $valeur ) {
                    if ( $valeur == $value ) {
                        return true;
                    }
                }
            }
        }
        else {
            if ( in_array($level, $table) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Vérification si l'user est DPO de son organisation
     *
     * @return boolean
     *
     * @access public
     *
     * @modified 08/10/2019
     * @version V1.1.0
     */
    public function isDpo()
    {
        if ($this->Session->read('Organisation.id') != null) {
            $Organisation = ClassRegistry::init('Organisation');

            $dpoDeclarer_id = $Organisation->find('first', [
                'conditions' => [
                    'Organisation.id' => $this->Session->read('Organisation.id')
                ],
                'fields' => [
                    'Organisation.dpo'
                ]
            ]);

            if ($dpoDeclarer_id['Organisation']['dpo'] == $this->Session->read('Auth.User.id')) {
                if ($this->authorized(ListeDroit::LISTE_DROITS_DPO_MINIMUM) == true) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     *
     * @deprecated 08/10/2019
     */
    public function existCil()
    {
        if ( $this->Session->read('Organisation.dpo') != NULL ) {
            return true;
        }
        return false;
    }

    public function isSu()
    {
        if ( $this->Session->read('Su') ) {
            return true;
        }
        return false;
    }


}
