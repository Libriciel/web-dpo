<?php

App::uses('FormHelper', 'View/Helper');

class TinyMCEHelper extends AppHelper
{
    var $helpers = Array('Html');

    /**
     * load TinyMCE
     * @param int $id
     * @version 5.0
     * @since 4.0
     */
    function load($class, $disable = false)
    {
        $code = '$(document).ready(function () {
                tinymce.init({'. ($disable ? 'readonly : true,' : '').'
                    selector: \'textarea\',
                    mode : \'specific_textareas\',
                    browser_spellcheck: true,
                    language_url : \'/js/node_modules/tinymce-i18n/langs/fr_FR.js\',
                    language : \'fr_FR\',
                    editor_selector: \'' . $class . '\',
                    plugins: [
                        \'lists\', //liste à puce
                        \'link\', // création de lien
                        \'autolink\', // détection de lien
                        \'table\', // tableau
                        \'image\', // image
                        \'imagetools\', // image
                        \'wordcount\', // nombre de mot
                        \'preview\', // visualisation de la mise en page
                        \'hr\', // ligne horizontale
                        \'searchreplace\', // recherché remplacer
                    ],
                    toolbar: \'styleselect fontsizeselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link unlink | codesample image media | preview\',
                })
        });';

        return $this->Html->scriptBlock($code);
    }

}
