<?php

class BreadcrumbsHelper extends AppHelper
{
    public $helpers = [
        'Autorisation',
        'Html',
        'Session',
    ];

    public function breadcrumbs($breadcrumbs, $showAdministration = false)
    {
        if ($showAdministration === true &&
            $this->Autorisation->isSu()
        ) {
            $breadcrumbs += [
                __d('default', 'default.titreAdministration') . ': ' . $this->Session->read('Organisation.raisonsociale') => [
                    'controller' => 'organisations',
                    'action' => 'administrer',
                    'prepend' => true
                ],
            ];
        }

        foreach ($breadcrumbs as $title => $breadcrumb) {
            $prepend = [];
            if (!isset($breadcrumb['controller'])) {
                $controller = $this->params['controller'];
            } else {
                $controller = $breadcrumb['controller'];
            }

            if (!isset($breadcrumb['action'])) {
                $action = $this->params['action'];
            } else {
                $action = $breadcrumb['action'];
            }

            if (isset($breadcrumb['prepend']) &&
                $breadcrumb['prepend'] === true
            ) {
                $prepend = [
                    'prepend' => true
                ];
            }

            if (isset($this->params['pass'][0])) {
                $params = $this->params['pass'][0];
            } else {
                $params = null;
            }

            $this->Html->addCrumb($title,
                [
                    'controller' => $controller,
                    'action' => $action,
                    $params
                ],
                $prepend
            );

        }
    }
}
