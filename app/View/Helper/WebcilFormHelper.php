<?php

/**
 * Code source de la classe WebcilFormHelper.
 */
App::uses('FormHelper', 'View/Helper');

/**
 * La classe WebcilFormHelper simplifie l'écriture des éléments de formulaire pour web-DPO
 */
class WebcilFormHelper extends FormHelper {

    public $helpers = ['Html'];

    protected function _options(array $defaults, array $options)
    {
        $options += $defaults;

        foreach (array_keys($defaults) as $key) {
            if (true === is_array($defaults[$key])) {
                $options[$key] += $defaults[$key];
            }
        }

        return $options;
    }

    public function input($fieldName, $options = [])
    {
        if (false === strpos($fieldName, '.')) {
            $modelName = Inflector::classify($this->request->params['controller']);
        } else {
            list($modelName, $fieldName) = explode('.', $fieldName);
        }

        $defaults = [
            'id' => null,
            'name' => null,
            'div' => false,
            'class' => 'form-control',
            'placeholder' => null,
            'label' => [
                'text' => null,
                'class' => 'control-label'
            ],
            'help-text' => false,
            'between' => null,
            'after' => null,
            'required' => false,
            'value' => null
        ];
        $options = $this->_options($defaults, $options);

        if ($options['id'] === null) {
            $options['id'] = $modelName . ucfirst($fieldName);
        }

        if ($options['name'] === null) {
            $options['name'] = 'data['.$modelName.']['.$fieldName.']';
        }

        if (null === $options['placeholder']) {
            $options['placeholder'] = __m(Inflector::underscore($modelName) . '.placeholderChamp' . Inflector::camelize($fieldName));
        }

        if (empty($options['help-text']) === false) {
            $options['aria-describedby'] = "{$options['id']}HelpText";
        }

        if (null === $options['label']['text']) {
            $options['label']['text'] = __m(Inflector::underscore($modelName) . '.champ' . Inflector::camelize($fieldName));
        }

        if (true === $options['required']) {
            $options['label']['text'] .= ' <span class="requis">*</span>';
        }

        if ($options['value'] === null) {
            $options['value'] = $this->request->data($modelName.'.'.$fieldName);
        }

//        return $this->Html->tag('div', parent::input($fieldName, $options), ['class' => 'form-group']);
        
        if (empty($options['help-text']) === false) {
            $options['after'] = $this->Html->tag('span', $options['help-text'], [
                'id' => $options['aria-describedby'],
                'class' => 'help-block'
            ]).$options['after'];
        }

        unset($options['help-text']);
        $result = parent::input("{$modelName}.{$fieldName}", $options);

        // @todo code help-text
        return $this->Html->tag('div', $result, ['class' => 'form-group']);

    }

    public function inputs($fields = null, $blacklist = null, $options = [])
    {
        $fields += [
            'fieldset' => false,
            'legend' => false
        ];

        return parent::inputs($fields, $blacklist, $options);
    }

    public function buttons( array $buttons, array $options = [])
    {
        $defaults = [
            'Back' => [
                'i' => 'fa-arrow-left',
                'button' => 'btn-outline-primary'
            ],
            'Cancel' => [
                'i' => 'fa-times-circle',
                'button' => 'btn-outline-primary'
            ],
            'Save' => [
                'i' => 'fa-save',
                'button' => 'btn-primary'
            ],
            'Send' => [
                'i' => 'fa-paper-plane',
                'button' => 'btn-outline-success'
            ],
            'Refuse' => [
                'i' => 'fa fa-times fa-lg fa-danger',
                'button' => 'btn-outline-danger'
            ]
        ];

        $options = $this->_options($defaults, $options);

        $result = '';
        foreach (Hash::normalize($buttons) as $button => $params) {
            $classes = true === isset($options[$button])
                ? $options[$button]
                : [ 'i' => '', 'button' => 'btn-outline-dark'];

            $result .= ' ' . $this->button("<i class=\"fa {$classes['i']} fa-lg\"></i> ".__m("default.btn{$button}"),
                [
                    'type' => 'submit',
                    'name' => 'submit',
                    'value' => $button,
                    'escape' => false,
                    'class' => "btn {$classes['button']}"
                ]
            );
        }

        return $this->Html->tag(
            'div',
            $this->Html->tag( 'div', $result, ['class' => 'buttons send']),
            ['class' => 'text-center']
        );
    }

    /**
     * @CakePHP 2.10.23, changed 3 lines - see @custom
     */
    protected function _selectOptions($elements = array(), $parents = array(), $showParents = null, $attributes = array()) {
        $select = array();
        $attributes = array_merge(
            array('escape' => true, 'style' => null, 'value' => null, 'class' => null),
            $attributes
        );
        $selectedIsEmpty = ($attributes['value'] === '' || $attributes['value'] === null);
        $selectedIsArray = is_array($attributes['value']);

        // Cast boolean false into an integer so string comparisons can work.
        if ($attributes['value'] === false) {
            $attributes['value'] = 0;
        }

        $this->_domIdSuffixes = array();
        foreach ($elements as $name => $title) {
            $htmlOptions = array();
            if (is_array($title) && (!isset($title['name']) || !isset($title['value']))) {
                if (!empty($name)) {
                    if ($attributes['style'] === 'checkbox') {
                        $select[] = $this->Html->useTag('fieldsetend');
                    } else {
                        $select[] = $this->Html->useTag('optiongroupend');
                    }
                    $parents[] = (string)$name;
                }
                $select = array_merge($select, $this->_selectOptions(
                    $title, $parents, $showParents, $attributes
                ));

                if (!empty($name)) {
                    $name = $attributes['escape'] ? h($name) : $name;
                    if ($attributes['style'] === 'checkbox') {
                        $select[] = $this->Html->useTag('fieldsetstart', $name);
                    } else {
                        $select[] = $this->Html->useTag('optiongroup', $name, '');
                    }
                }
                $name = null;
            } elseif (is_array($title)) {
                $htmlOptions = $title;
                $name = $title['value'];
                $title = $title['name'];
                unset($htmlOptions['name'], $htmlOptions['value']);
            }

            if ($name !== null) {
                $isNumeric = is_numeric($name);
                if ((!$selectedIsArray && !$selectedIsEmpty && (string)$attributes['value'] == (string)$name) ||
                    ($selectedIsArray && in_array((string)$name, $attributes['value'], !$isNumeric))
                ) {
                    if ($attributes['style'] === 'checkbox') {
                        $htmlOptions['checked'] = true;
                    } else {
                        $htmlOptions['selected'] = 'selected';
                    }
                }

                if ($showParents || (!in_array($title, $parents))) {
                    $title = ($attributes['escape']) ? h($title) : $title;

                    $hasDisabled = !empty($attributes['disabled']);
                    if ($hasDisabled) {
                        $disabledIsArray = is_array($attributes['disabled']);
                        if ($disabledIsArray) {
                            $disabledIsNumeric = is_numeric($name);
                        }
                    }
                    if ($hasDisabled &&
                        $disabledIsArray &&
                        in_array((string)$name, $attributes['disabled'], !$disabledIsNumeric)
                    ) {
                        $htmlOptions['disabled'] = 'disabled';
                    }
                    if ($hasDisabled && !$disabledIsArray && $attributes['style'] === 'checkbox') {
                        $htmlOptions['disabled'] = $attributes['disabled'] === true ? 'disabled' : $attributes['disabled'];
                    }

                    if ($attributes['style'] === 'checkbox') {
                        $htmlOptions['value'] = $name;

                        $tagName = $attributes['id'] . $this->domIdSuffix($name);
                        $htmlOptions['id'] = $tagName;
                        $label = array('for' => $tagName);

                        if (isset($htmlOptions['checked']) && $htmlOptions['checked'] === true) {
                            $label['class'] = 'selected';
                        }

                        $name = $attributes['name'];

                        if (empty($attributes['class'])) {
                            $attributes['class'] = 'checkbox';
                        } elseif ($attributes['class'] === 'is-invalid') {//@custom
                            $attributes['class'] = 'checkbox ' . $attributes['class'];
                        }
                        $label = $this->addClass($label, 'form-check-label');//@custom
                        $label = $this->label(null, $title, $label);
                        $htmlOptions = $this->addClass($htmlOptions, 'form-check-input');//@custom
                        $item = $this->Html->useTag('checkboxmultiple', $name, $htmlOptions);
                        $select[] = $this->Html->div($attributes['class'], $item . $label);
                    } else {
                        if ($attributes['escape']) {
                            $name = h($name);
                        }
                        $select[] = $this->Html->useTag('selectoption', $name, $htmlOptions, $title);
                    }
                }
            }
        }

        return array_reverse($select, true);
    }

    /**
     * @CakePHP 2.10.23, changed 1 line - see @custom
     */
    protected function _initInputField($field, $options = array()) {
        if (isset($options['secure'])) {
            $secure = $options['secure'];
            unset($options['secure']);
        } else {
            $secure = (isset($this->request['_Token']) && !empty($this->request['_Token']));
        }

        $disabledIndex = array_search('disabled', $options, true);
        if (is_int($disabledIndex)) {
            unset($options[$disabledIndex]);
            $options['disabled'] = true;
        }

        $result = parent::_initInputField($field, $options);
        if ($this->tagIsInvalid() !== false) {
            $result = $this->addClass($result, 'is-invalid');//@custom
        }

        $isDisabled = false;
        if (isset($result['disabled'])) {
            $isDisabled = (
                $result['disabled'] === true ||
                $result['disabled'] === 'disabled' ||
                (is_array($result['disabled']) &&
                    !empty($result['options']) &&
                    array_diff($result['options'], $result['disabled']) === array()
                )
            );
        }
        if ($isDisabled) {
            return $result;
        }

        if (!isset($result['required']) &&
            $this->_introspectModel($this->model(), 'validates', $this->field())
        ) {
            $result['required'] = true;
        }

        if ($secure === static::SECURE_SKIP) {
            return $result;
        }

        $this->_secure($secure, $this->_secureFieldName($options));
        return $result;
    }

    public function error($field, $text = null, $options = array())
    {
        $defaults = array('class' => 'invalid-feedback');
        $options += $defaults;
        return parent::error($field, $text, $options);
    }
}
