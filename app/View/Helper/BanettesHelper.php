<?php

/**
 * BanettesHelper
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.View.Helper
 */

App::uses('FormHelper', 'View/Helper');
App::uses('EtatFiche', 'Model');
App::uses('DefaultUrl', 'Default.Utility');
App::uses('DefaultUtility', 'Default.Utility');

class BanettesHelper extends AppHelper {

    public $helpers = [
        'Html',
        'Time',
        'Session',
        'WebcilForm',
        'Form',
        'Autorisation'
    ];
    
    /**
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function thead() {
        $tr = $this->Html->tableHeaders(
            [
                [__d('pannel', 'pannel.motEtat') => ['class' => 'col-md-1']],
                [__d('pannel', 'pannel.motNomTraitement') => ['class' => 'col-md-2']],
                [__d('pannel', 'pannel.motFinalitePrincipale') => ['class' => 'col-md-3']],
                [__d('pannel', 'pannel.motSyntheseDonnees') => ['class' => 'col-md-2']],
                [__d('pannel', 'pannel.motSynthese') => ['class' => 'col-md-2']],
                [__d('pannel', 'pannel.motActions') => ['class' => 'col-md-2']],
            ],
        );
        return $this->Html->tag('thead', $tr);
    }

    /**
     * Retourne le contenu de la première cellule, en fonction de l'état du
     * traitement, de l'utilisateur connecté et de l'utilisateur lié à l'état du
     * traitement.
     * 
     * @param array $result
     * @return string
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function etatIcone(array $result) {
        $me = $this->Session->read('Auth.User.id');
        $etat = Hash::get($result, 'EtatFiche.etat_id');
        $valide = Hash::get($result, 'Fiche.valide');
        $user_id = Hash::get($result, 'EtatFiche.user_id');

        switch($etat) {
            case EtatFiche::ENCOURS_REDACTION:
            case EtatFiche::TRAITEMENT_INITIALISE_REDIGER:
                $class = 'fa-edit';
                $text = __d('pannel', 'pannel.textEtatEnCoursRedaction');

                if ($valide === false) {
                    $text .= ' / BROUILLON';
                }

                break;

            case EtatFiche::ENCOURS_VALIDATION:
                if($me == $user_id) {
                    $class = 'fa-check-square';
                    $text = __d('pannel', 'pannel.textEtatEnCoursValidation');                   
                } else {
                    $class = 'fa-clock';
                    $text = __d('pannel', 'pannel.textEtatEnAttente');  
                }
                break;

            case EtatFiche::REFUSER:
                $class = 'fa-times fa-danger';
                $text = '<span class="fa-danger">'.__d('pannel', 'pannel.textEtatRefuser').'</span>';
                break;

            case EtatFiche::VALIDER_DPO:
                $class = 'fa-check';
                $text = __d('pannel', 'pannel.textEtatValiderDPO');
                break;

            case EtatFiche::DEMANDE_AVIS:
                $class = 'fa-eye';

                if ($me == $user_id) {
                    $text = __d('pannel', 'pannel.textEtatEnAttenteDemandeAvis');
                } else {
                    $text = __d('pannel', 'pannel.textEtatDemandeAvis');
                }
                break;

            case EtatFiche::ARCHIVER:
                $class = 'fa-lock';
                $text = __d('pannel', 'pannel.textEtatArchiver');
                break;

            case EtatFiche::REPLACER_REDACTION:
                $class = 'fa-edit';
                $text = __d('pannel', 'pannel.textEtatReplacerRedaction');
                break;

            case EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE:
                $class = 'fa-edit';
                $text = __d('pannel', 'pannel.textEtatModificationTraitementAuRegistre');
                break;

            case EtatFiche::REPONSE_AVIS:
                $class = 'fa-eye';

                if($me == $user_id) {
                    $text = __d('pannel', 'pannel.textEtatRetourDemandeAvis');
                } else {
                    $text = __d('pannel', 'pannel.textEtatConsulte');
                }
                break;

            case EtatFiche::INITIALISATION_TRAITEMENT :
                $class = 'fa-edit';
                $text = __d('pannel', 'pannel.textEtatInitialisationTraitement');
                break;

            case EtatFiche::TRAITEMENT_INITIALISE_RECU :
                $class = 'fa-edit';
                $text = __d('pannel', 'pannel.textEtatTraitementInitialiseEnAttenteRedaction');
                break;

            default:
                $class = null;
                $text = '';
        }

        return $this->Html->tag(
            'div',
            $this->Html->tag('i', '', ['class' => "fa {$class} fa-3x"])
                .'<br/>'
                .$text,
            ['class' => 'etatIcone']
        );
    }
    
    // -------------------------------------------------------------------------
    
    protected function _url($url) {
        if (false === is_array($url)) {
            $params = Router::parse($url);
        } else {
            $params = $url;
        }

        $params['url'] = true === isset($params['url']) ? $params['url'] : array();
        $params['controller'] = Inflector::underscore($params['controller']);

        return Router::normalize(Router::reverse($params));
    }
    
    protected function _urlToArray($url) {
        if (false === is_array($url)) {
            $params = Router::parse($url);
        } else {
            $params = $url;
        }

        $params['url'] = true === isset($params['url']) ? $params['url'] : array();
        $params['controller'] = Inflector::underscore($params['controller']);

        return $params;
    }
    
    /**
     * Retourne la chaîne de caractères $string dont les occurences de
     * #Model.champ# ont été remplacées par leur valeur extraite depuis $data.
     *
     * @param array $data
     * @param string $string
     * @return string
     */
    public static function evaluateString(array $data, $string) {
        if (strpos($string, '#') !== false) {
            $pattern = '/("#[^#]+#"|\'#[^#]#\'|#[^#]+#)/';
            if (preg_match_all($pattern, $string, $out)) {
                $tokens = $out[0];
                foreach (array_unique($tokens) as $token) {
                    // Pour échapper efficacement les guillemets simples et doubles
                    if ($token[0] === '"') {
                        $escape = '"';
                        $token = trim($token, '"');
                    } else if ($token[0] === "'") {
                        $escape = "'";
                        $token = trim($token, "'");
                    } else {
                        $escape = false;
                    }

                    $token = trim($token, '#');
                    $value = Hash::get($data, $token);

                    if (false !== $escape) {
                        $value = str_replace($escape, "\\{$escape}", $value);
                    }

                    $string = str_replace("#{$token}#", $value, $string);
                }
            }
            $string = preg_replace('/^\/\//', '/', $string);
        }

        return $string;
    }

    /**
     * Retourne le paramètre $mixed dont les occurences de #Model.champ# ont
     * été remplacées par leur valeur extraite depuis $data.
     *
     * @see Hash::get()
     *
     * @param array $data
     * @param string|array $mixed
     * @return string|array
     */
    public static function evaluate(array $data, $mixed) {
        if (is_array($mixed)) {
            $array = array();
            if (!empty($mixed)) {
                foreach ($mixed as $key => $value) {
                    $array[self::evaluateString($data, $key)] = self::evaluate($data, $value);
                }
            }
            return $array;
        }

        return self::evaluateString($data, $mixed);
    }

    //--------------------------------------------------------------------------
    
    protected $_linkClasses = [
        '/fiches/show/#Fiche.id#' => 'fa-eye',
        '/fiches/edit/#Fiche.id#' => 'fa-pencil-alt',
        '/EtatFiches/relaunch/#Fiche.id#' => 'fa-reply',
        '/fiches/genereTraitementEnCours/#Fiche.id#' => 'fa-cog',
        '/fiches/downloadFileTraitement/#Fiche.id#' => 'fa-download',
//        '/fiches/historique/#Fiche.id#' => 'fa-history',//action virtuelle
        '/fiches/reponse/#Fiche.id#' => 'fa-reply',//action virtuelle
        '#refuser' => 'fa-times',//action virtuelle
        '/fiches/reorienter/#Fiche.id#' => 'fa-exchange',//action virtuelle
        '/fiches/envoyer/#Fiche.id#' => 'fa-paper-plane',//action virtuelle
        '/fiches/delete/#Fiche.id#' => 'fa-trash'
    ];
    
    protected function _button($url, array $result, array $params = []) {
        $params += [
            'confirm' => null,
            'onclick' => null
        ];
       
        $icone = true === isset($this->_linkClasses[$url])
            ? $this->_linkClasses[$url]
            : null;
        
        $parts = $this->_urlToArray($url);
       
        $toggle = '';
        if (true === in_array($icone, ['fa-times', 'fa-trash'])) {
            $span = '<i class="fas '.$icone.' fa-lg"></i>';
            $class = 'btn btn-outline-danger borderless';
        } elseif ((true === in_array($icone, ['fa-paper-plane']))) {
            $span = '<i class="fa '.$icone.' fa-lg"></i>';
            $span .= '<span class="caret"></span>';
            $class = 'btn dropdown-toggle btn-outline-success borderless';
            $toggle = 'dropdown';
        } else {
            $span = '<i class="fas '.$icone.' fa-lg"></i>';
            $class = 'btn btn-outline-primary borderless';
        }

        $link = $this->Html->link(
            $span,
            static::evaluate($result, $url),
            [
                'class' => $class,
                'data-toggle' => $toggle,
                'escapeTitle' => false,
                'title' => __d('pannel', 'pannel.commentaire'.Inflector::camelize($parts['action']).'Traitement'),
                'id' => static::evaluate($result, 'btn'.Inflector::camelize($parts['action']).'#Fiche.id#'),
                'onclick' => self::evaluateString($result, $params['onclick'])
            ],
            $params['confirm']
        );

        return $link;
    }
    
    protected function _buttonReorienter(array $result, array $params = [])
    {
        if (!empty($params['nbUserValideur']) && $params['nbUserValideur'] >= 2) {
            $fiche_id = Hash::get($result, 'Fiche.id');
            $etatFiche_id = Hash::get($result, 'EtatFiche.id');

            return $this->Html->link('<i class="fas fa-exchange-alt fa-lg"></i>', ['#' => '#'], [
                'data-id' => $fiche_id,
                'data-fiche' => $etatFiche_id,
                'escape' => false,
                'data-toggle' => 'modal',
                'data-target' => '#modalReorienter',
                'class' => 'btn btn-outline-dark borderless btn_ReorienterTraitement',
                'title' => __d('pannel', 'pannel.commentaireReorienterTraitement')
            ]);
        }

        return '';
    }
    
    protected function _menuEnvoyerTraitement($banette, array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $etatFiche_id = Hash::get($result, 'EtatFiche.id');
        $menu= '';

        $fiche_typedeclaration = 'false';
        
        foreach ($result['Valeur'] as $valeur) {
            if ($valeur['champ_name'] === 'typedeclaration' && !empty($valeur['champ_name'])) {
                $fiche_typedeclaration = 'true';
            }
        }

        if ($this->Autorisation->authorized(ListeDroit::INITIALISATION_TRAITEMENT, $this->Session->read('Droit.liste')) &&
            $banette == 'initialisation'
        ) {
            // Envoyer au rédacteur validation
            $menu .= $this->Html->tag(
                'li',
                $this->Html->link(__d('pannel', 'pannel.textEnvoyerRedacteur'), ['#' => '#'], [
                    'role' => 'menuitem',
                    'tabindex' => -1,
                    'escape' => false,
                    'data-toggle' => 'modal',
                    'data-target' => '#modalSendRedaction',
                    'data-id' => $fiche_id,
                    'data-fiche' => $etatFiche_id,
                    'class' => 'btn_envoyerValideur dropdown-item'
                ]),
                ['role' => 'presentation', 'class' => 'dropdown-item']
            );
        } else {
            if (!empty($params['nbUserConsultant'])) {
                // Envoyer pour consultation
                $menu .= $this->Html->tag(
                    'li',
                    $this->Html->link(__d('pannel', 'pannel.textEnvoyerConsultation'), ['#' => '#'], [
                        'role' => 'menuitem',
                        'tabindex' => -1,
                        'escape' => false,
                        'data-toggle' => 'modal',
                        'data-target' => '#modalEnvoieConsultation',
                        'data-id' => $fiche_id,
                        'data-fiche' => $etatFiche_id,
                        'class' => 'btn_envoyerConsultation dropdown-item'
                    ]),
                    ['role' => 'presentation', 'class' => 'dropdown-item']
                );
            }

            if (!empty($params['nbUserValideur'])) {
                if ($banette == 'encours_redaction') {
                    $titreSendValidation = __d('pannel', 'pannel.textEnvoyerValidation');
                } else {
                    $titreSendValidation = __d('pannel', 'pannel.textValiderEnvoyerValidation');
                }

                // Envoyer pour validation
                $menu .= $this->Html->tag(
                    'li',
                    $this->Html->link($titreSendValidation, ['#' => '#'], [
                        'role' => 'menuitem',
                        'tabindex' => -1,
                        'escape' => false,
                        'data-toggle' => 'modal',
                        'data-target' => '#modalEnvoieValidation',
                        'data-id' => $fiche_id,
                        'data-fiche' => $etatFiche_id,
                        'class' => 'btn_envoyerValideur dropdown-item'
                    ]),
                    ['role' => 'presentation', 'class' => 'dropdown-item']
                );
            }

            // Envoyer au DPO ou insèrer au registre
            if ($this->Autorisation->isDpo() === true &&
                (
                $result['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_REDACTION ||
                $result['EtatFiche']['etat_id'] == EtatFiche::ENCOURS_VALIDATION ||
                $result['EtatFiche']['etat_id'] == EtatFiche::TRAITEMENT_INITIALISE_REDIGER
                )
            ) {
                $menu .= $this->Html->tag(
                    'li',
                    $this->Html->link(__d('pannel', 'pannel.textValiderInsererRegistre'), ['#' => '#'], [
                        'role' => 'menuitem',
                        'tabindex' => '-1',
                        'data-toggle' => 'modal',
                        'data-target' => '#modalValidDpo',
                        'data-type' => $fiche_typedeclaration,
                        'data-id' => $fiche_id,
                        'class' => 'btn-insert-registre dropdown-item',
                        'data-norme-id' => Hash::get($result, 'Fiche.norme_id'),
                    ]),
                    ['role' => 'presentation', 'class' => 'dropdown-item']
                );
            } else {
                if ($banette == 'encours_redaction') {
                    $title = __d('pannel', 'pannel.textEnvoyerDPO');
                } else {
                    $title = __d('pannel', 'pannel.textValiderEnvoyerDPO');
                }

                $menu .= $this->Html->tag(
                    'li',
                    $this->Html->link($title, [
                        'controller' => 'etatFiches',
                        'action' => 'dpoValid',
                        $fiche_id
                    ], [
                        'role' => 'menuitem',
                        'tabindex' => '-1',
                        'class' => 'dropdown-item'
                    ]),
                    ['role' => 'presentation', 'class' => 'dropdown-item']
                );
            }
        }

        $span = $this->Html->tag(
            'ul',
            $menu,
            ['class' => 'dropdown-menu', 'role' => 'menu']
        );
        
        return $span;
    }
    
    /**
     * Groupe de boutons correspondant à la banette 
     * "Mes déclarations en cours de rédaction"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 11/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsEncoursRedaction(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $etat = Hash::get($result, 'EtatFiche.etat_id');
        $traitement_name = $this->_getOutilnom(Hash::extract($result, 'Valeur.{n}'));
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

//        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
//            . $this->_button('/fiches/edit/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, [
//                'onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"
//            ]);

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/fiches/edit/#Fiche.id#', $result)
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
        ;

        if ($result['Fiche']['valide'] === true) {
            $btnGroups .= $this->_button('/fiches/envoyer/#Fiche.id#', $result)
                . $this->_menuEnvoyerTraitement('encours_redaction', $result, $params);
        }

        if ($etat !== EtatFiche::TRAITEMENT_INITIALISE_RECU) {
            $btnGroups .= $this->_button('/fiches/delete/#Fiche.id#', $result, ['confirm' => __d('pannel', 'pannel.confirmationSupprimerTraitement') . $traitement_name . ' " ?']);
        }

        return $btnGroups;
    }

    protected function _buttonsPartageServices(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');

        $btnGroups = $this->_button('/fiches/genereTraitement/#Fiche.id#', $result)
            . $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/fiches/edit/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, [
//                'onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"
//            ])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
        ;

        if ($result['Fiche']['valide'] === true) {
            $btnGroups .= $this->_button('/fiches/envoyer/#Fiche.id#', $result)
                . $this->_menuEnvoyerTraitement('encours_redaction', $result, $params);
        }

        return $btnGroups;
    }

    /**
     * Groupe de boutons correspondant à la banette "Mes déclarations en attente"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsAttente(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $this->_buttonReorienter($result, $params)
        ;

        return $btnGroups;
    }
    
    /**
     * Groupe de boutons correspondant à la banette "Mes déclarations refusées"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsRefuser(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $traitement_name = $this->_getOutilnom(Hash::extract($result, 'Valeur.{n}'));
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/EtatFiches/relaunch/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $this->_button('/fiches/delete/#Fiche.id#', $result, [
                'confirm' => __d('pannel', 'pannel.confirmationSupprimerTraitement') . $traitement_name . ' " ?'
            ])
        ;

        return $btnGroups;
    }
    
    /**
     * Groupe de boutons correspondant à la banette "Mes traitements validés et insérés au registre"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsArchives(array $result, array $params = []) {
        $fiche_id = Hash::get($result, 'Fiche.id');

        if ($result['EtatFiche']['etat_id'] == EtatFiche::VALIDER_DPO || $result['EtatFiche']['etat_id'] == EtatFiche::MODIFICATION_TRAITEMENT_REGISTRE){
            $action = 'genereTraitement';
        } else if ($result['EtatFiche']['etat_id'] == EtatFiche::ARCHIVER) {
            $action = 'downloadFileTraitement';
        }
        
        return $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/fiches/'.$action.'/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
        ;
    }
    
    /**
     * Groupe de boutons correspondant à la banette "Etat des traitements passés en ma possession"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsConsulte(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $etatFiche_etatId = Hash::get($result, 'EtatFiche.etat_id');
        $etatFiche_userId = Hash::get($result, 'EtatFiche.user_id');
        $traitement_name = Hash::get($result, 'Valeur.0.valeur');
        $reorienter = null;
        $remiseRedaction = null;
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::VALIDER_TRAITEMENT, $this->Session->read('Droit.liste')) &&
            $etatFiche_etatId == EtatFiche::ENCOURS_VALIDATION &&
            $this->Session->read('Auth.User.id') != $etatFiche_userId)
        {
            $reorienter = $this->_buttonReorienter($result, $params);
        } elseif ($this->Autorisation->authorized(ListeDroit::INSERER_TRAITEMENT_REGISTRE, $this->Session->read('Droit.liste'))) {
            if ($etatFiche_etatId == EtatFiche::REFUSER) {
                $remiseRedaction = $this->_button('/EtatFiches/relaunch/#Fiche.id#', $result, ['confirm' => __d('pannel', 'pannel.confirmationReapproprierTraitement') . $traitement_name . ' " ?']);
            }
        }

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $reorienter
            . $remiseRedaction;

        return $btnGroups;
    }
    
    /**
     * Groupe de boutons correspondant à la banette "Traitements reçus pour validation"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsRecuValidation(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/fiches/edit/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $this->_button('/fiches/envoyer/#Fiche.id#', $result)
            . $this->_menuEnvoyerTraitement('recuValidation', $result, $params)
            . $this->_button('#refuser', $result, [
                'onclick' => "$('#commentaireRefus{$fiche_id}').toggle(); return false;"
            ])
        ;

        return $btnGroups;
    }
    
    /**
     * Groupe de boutons correspondant à la banette "Traitements reçus pour consultation"
     * 
     * @param array $result
     * @param array $params
     * @return string
     * 
     * @access protected
     *
     * @created 19/07/2017
     * @version V1.0.0
     *
     * @modified 30/09/2020
     * @version V2.1.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsRecuConsultation(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        $btnGroups = null;

        if ($this->Autorisation->authorized(ListeDroit::GENERER_TRAITEMENT_EN_COURS, $this->Session->read('Droit.liste'))) {
            $btnGroups = $this->_button('/fiches/genereTraitementEnCours/#Fiche.id#', $result);
        }

        $btnGroups .= $this->_button('/fiches/show/#Fiche.id#', $result);

        if ($this->Autorisation->authorized(ListeDroit::MODIFIER_TRAITEMENT_CONSULTATION, $this->Session->read('Droit.liste'))) {
            $btnGroups .= $this->_button('/fiches/edit/#Fiche.id#', $result);
        }

//        $btnGroups .= $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
//            . $this->_button('/fiches/reponse/#Fiche.id#', $result, ['onclick' => "$('#commentaireRepondre{$fiche_id}').toggle(); return false;"]);

        $btnGroups .= $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $this->_button('/fiches/reponse/#Fiche.id#', $result, [
                'onclick' => "$('#commentaireRepondre{$fiche_id}').toggle(); return false;"
            ])
        ;

        return $btnGroups;
    }
    
    /**
     * 
     * @param type $banette
     * @param array $result
     * @param array $params
     * @return type
     * 
     * @access protected
     * @created 19/07/2017
     * @version V1.0.0
     */
    protected function _buttons($banette, array $result, array $params = [])
    {
        $method = '_buttons'.Inflector::camelize($banette);
        $content = null;

        if (true === method_exists($this, $method)) {
            $content = $this->{$method}($result, $params);
        }

        return $this->Html->tag('div', $content, ['class' => 'row buttons']);
    }

    /**
     * Nouvelle ligne dans le tableau de la banette avec l'historique du
     * traitement.
     * 
     * @return string
     * 
     * @access protected
     * @created 19/07/2017
     * @version V1.0.0
     */
    protected function _rowHistorique(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');
        return "<tr class='listeValidation' id='ajax_list_history_{$fiche_id}'></tr>";
    }
    
    /**
     * Nouvelle ligne dans le tableau de la banette pour répondre dans le cas 
     * d'un refus ou d'une consultation
     * 
     * @param array $result
     * @param array $params
     * @return type
     * 
     * @access protected
     * @created 19/07/2017
     * @version V1.0.0
     */
    protected function _rowReponse(array $result, array $params = [])
    {
        $etatFiche_id = Hash::get($result, 'EtatFiche.id');
        $etatFiche_previousUserId = Hash::get($result, 'EtatFiche.previous_user_id');
        $fiche_id = Hash::get($result, 'Fiche.id');

        $options = ['class' => 'commentaireRepondre', 'id' => "commentaireRepondre{$fiche_id}"];

        $content = $this->WebcilForm->create('EtatFiche', [
            'url' => ['action' => 'answerAvis'],
            'autocomplete' => 'off',
            'inputDefaults' => ['div' => false],
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate'
        ]);

        $content .= $this->WebcilForm->inputs([
            'EtatFiche.commentaireRepondre' => [
                'id' => 'commentaireRepondre',
                'label' => [
                    'text' =>  __d('pannel', 'pannel.champCommentaireRepondre'),
                ],
                'class' => 'form-control reponseStartTinyMCE',
                'required' => true,
                'placeholder' => false,
                'type' => 'textarea'
            ]
        ]);
        
        // Champs cachés
        $content .= $this->WebcilForm->hidden('etatFiche', ['value' => $etatFiche_id]);
        $content .= $this->WebcilForm->hidden('previousUserId', ['value' => $etatFiche_previousUserId]);
        $content .= $this->WebcilForm->hidden('ficheNum', ['value' => $fiche_id]);
        
        // Groupe de bouton
        $content .= $this->WebcilForm->buttons(['Cancel', 'Send']);
        
        $content .= $this->WebcilForm->end();

        return $this->Html->tableCells(
            [
                [
                    ['', []],
                    [$content, ['class' => 'tdleft col-md-12', 'colspan' => 4]],
                    ['', []]
                ]   
            ],
            $options,
            $options,
            false
        );
    }
    
    protected function _rowRefuser(array $result, array $params = [])
    {
        $etatFiche_id = Hash::get($result, 'EtatFiche.id');
        $fiche_id = Hash::get($result, 'Fiche.id');

        $options = ['class' => 'commentaireRefus', 'id' => "commentaireRefus{$fiche_id}"];

        $content = $this->WebcilForm->create('EtatFiche', [
            'url' => ['action' => 'refuse'],
            'autocomplete' => 'off',
            'inputDefaults' => ['div' => false],
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate'
        ]);
        
        $content .= $this->WebcilForm->inputs([
            'EtatFiche.commentaireRepondre' => [
                'id' => 'commentaireRepondre',
                'label' => [
                    'text' =>  __d('pannel', 'pannel.textExpliquezRaisonRefus'),
                ],
                'class' => 'form-control reponseStartTinyMCE',
                'required' => true,
                'placeholder' => false,
                'type' => 'textarea'
            ]
        ]);
        
        // Champs cachés
        $content .= $this->WebcilForm->hidden('ficheNum', ['value' => $fiche_id]);
        $content .= $this->WebcilForm->hidden('etatFiche', ['value' => $etatFiche_id]);
        
        // Groupe de bouton
        $content .= $this->WebcilForm->buttons(['Cancel', 'Refuse']);
        
        $content .= $this->WebcilForm->end();

        return $this->Html->tableCells(
            [
                [
                    ['', []],
                    [$content, ['class' => 'tdleft', 'colspan' => 4]],
                    ['', []]
                ]   
            ],
            $options,
            $options,
            false
        );
    }

    /**
     * 
     * 
     * @param type $banette
     * @param array $result
     * @param array $params
     * @return type
     * 
     * @access protected
     * @created 19/07/2017
     * @version V1.0.0
     */
    protected function _rowSupplementaire($banette, array $result, array $params = [])
    {
        if ('recuConsultation' === $banette) {
            return $this->_rowHistorique($result, $params) . $this->_rowReponse($result, $params);
        } elseif ('recuValidation' === $banette) {
            return $this->_rowHistorique($result, $params) . $this->_rowRefuser($result, $params);
        } else {
            return $this->_rowHistorique($result, $params);
        }
    }

    /**
     * 
     * @param array $results
     * @param array $params
     * @return type
     * 
     * @access protected
     * @created 18/07/2017
     * @version V1.0.0
     */
    protected function _banette(array $results, array $params = [])
    {
        $params += [
            'limit' => false,
            'link' => false
        ];
        
        $traitement = Inflector::camelize($params['action']);

        if (true === empty($results)) {
            $h3 = $this->Html->tag('h3', __d('pannel', "pannel.aucunTraitement{$traitement}"));
            $span = null;
            $content = $this->Html->tag('div', $h3, ['class' => 'text-center']);
        } else {
            if (false !== $params['limit'] && $params['count'] >= $params['limit']) {
                $span = $this->Html->tag(
                    'span',
                    $this->Html->link('<i class="fas fa-eye fa-fw"></i>' . __d('pannel', "pannel.btnVoirTraitement{$traitement}"), [
                            'controller' => 'pannel',
                            'action' => $params['action'],
                                ], [
                            'class' => 'btn btn-primary-card',
                            'escapeTitle' => false,
                    ]),
                    ['class' => 'float-right']
                );
            } else {
                $span = null;
            }

            $thead = $this->thead();
            
            $rows = '';
            foreach ($results as $result) {
                if (!empty($result['Norme']['id'])) {
                    $title = Hash::get($params['enumsNormes'], "Norme.norme.{$result['Norme']['norme']}");
                    $abbr = $this->Html->tag('abbr', $result['Norme']['norme'], ['title' => $title]);
                    
                    $titleDescription = strip_tags($result['Norme']['description'], '<br />');
                    $numeroNorme = sprintf('%03d', $result['Norme']['numero']);
                    $abbrNumero = $this->Html->tag('abbr', $numeroNorme, ['title' => $titleDescription]);
                    
                    $norme = ' ' . $abbr . '-' . $abbrNumero . ' : ' . $result['Norme']['libelle'];
                } else {
                    $norme = __d('registre', 'registre.textAucuneNormeDefini');
                }

                if (!empty($result['Referentiel']['id'])) {
                    $referentiel = $result['Referentiel']['name'];
                } else {
                    $referentiel = __d('registre', 'registre.textAucunReferentielDefini');
                }

                if ($result['Fiche']['transfert_hors_ue'] === true) {
                    $etatTransfertHorsUe = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatTransfertHorsUe = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoTransfertHorsUe = '<strong>'
                    . __d('registre', "registre.textTableauTransfertHorsUe")
                    . '</strong>'
                    . $etatTransfertHorsUe
                    . '<br/>';

                if ($result['Fiche']['coresponsable'] === true) {
                    $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatCoresponsable = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoCoresponsable = '<strong>'
                    . __d('registre', "registre.textTableauCoresponsable")
                    . '</strong>'
                    . $etatCoresponsable
                    . '<br/>';

                if ($result['Fiche']['soustraitance'] === true) {
                    $etatSousTraitance = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatSousTraitance = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoSousTraitance = '<strong>'
                    . __d('registre', "registre.textTableauSousTraitance")
                    . '</strong>'
                    . $etatSousTraitance
                    . '<br/>';

                if (!is_null($result['Fiche']['obligation_pia'])) {
                    if ($result['Fiche']['obligation_pia'] === true) {
                        $etatObligationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                    } else {
                        $etatObligationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                    }
                    $infoObligationPIA = '<strong>'
                        . __d('registre', "registre.textTableauObligationPIA")
                        . '</strong>'
                        . $etatObligationPIA
                        . '<br/>';

                } else {
                    $infoObligationPIA = '';
                }

                if ($result['Fiche']['realisation_pia'] === true) {
                    $etatRealisationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatRealisationPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoRealisationPIA = '<strong>'
                    . __d('registre', "registre.textTableauRealisationPIA")
                    . '</strong>'
                    . $etatRealisationPIA
                    . '<br/>';

                if ($result['Fiche']['depot_pia'] === true) {
                    $etatDepotPIA = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatDepotPIA = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoDepotPIA = '<strong>'
                    . __d('registre', "registre.textTableauDepotPIA")
                    . '</strong>'
                    . $etatDepotPIA
                    . '<br/>';

                if ($result['Fiche']['rt_externe'] === true) {
                    $etatRtExterne = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                } else {
                    $etatRtExterne = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                }
                $infoRtExterne = '<strong>'
                    . __d('registre', "registre.textTableauRtExterne")
                    . '</strong>'
                    . $etatRtExterne
                    . '<br/>';


                $infoPartage = '';
                if (!empty($result['Fiche']['service_id'])) {
                    if ($result['Fiche']['partage'] === true) {
                        $etatPartage = $this->Html->tag('i', '', ['class' => 'fas fa-check fa-lg fa-success']);
                    } else {
                        $etatPartage = $this->Html->tag('i', '', ['class' => 'fas fa-times fa-lg fa-danger']);
                    }

                    $infoPartage = '<strong>'
                        . __d('registre', "registre.textTableauPartage")
                        . '</strong>'
                        . $etatPartage
                        . '<br/>';
                }

                $valeursTraitement = Hash::extract($result,'Valeur.{n}');

                $nomTraitement = 'À DÉFINIR';
                $finalitePrincipaleTraitement = 'À DÉFINIR';
                foreach ($valeursTraitement as $valeurTraitement) {
                    if ($valeurTraitement['champ_name'] === 'outilnom') {
                        $nomTraitement = $valeurTraitement['valeur'];
                    }

                    if ($valeurTraitement['champ_name'] === 'finaliteprincipale') {
                        $finalitePrincipaleTraitement = $valeurTraitement['valeur'];
                    }
                }

                $declarantServiceTraitement = 'Aucun service';
                if (!empty($result['Service']['libelle'])) {
                    $declarantServiceTraitement = $result['Service']['libelle'];
                }

                $rows .= $this->Html->tableCells(
                    [
                        [
                            // Etat du traitement
                            [
                                $this->etatIcone($result), ['class' => 'tdleft']
                            ],
                            // Nom du traitement
                            [
                                $nomTraitement, ['class' => 'tdleft']
                            ],
                            // Finalité principale
                            [
                                nl2br($finalitePrincipaleTraitement), ['class' => 'tdleft']
                            ],
                            // Synthèse données
                            [
                                $infoTransfertHorsUe
                                . $infoCoresponsable
                                . $infoSousTraitance
                                . $infoObligationPIA
                                . $infoRealisationPIA
                                . $infoDepotPIA
                                . $infoRtExterne
                                . $infoPartage,
                                ['class' => 'tdleft']
                            ],
                            // Synthèse
                            [
                                '<strong>' . __d('pannel', "pannel.motCreee") . '</strong>'. $result['User']['prenom'] . ' ' . $result['User']['nom']
                                . '<br/>'
                                . '<strong>' . __d('registre', "registre.textTableauDateCreation") . '</strong>'. $this->Time->format($result['Fiche']['created'], FORMAT_DATE_HEURE)
                                . '<br/>'
                                . '<strong>' . __d('pannel', "pannel.motDerniereModification") . '</strong>'. $this->Time->format($result['Fiche']['modified'], FORMAT_DATE_HEURE)
                                . '<br/>'
                                . '<strong>' . __d('registre', "registre.textTableauServiceDeclarant") . '</strong>'. $declarantServiceTraitement
                                . '<br/>'
                                . '<strong>' . __d('pannel', "pannel.textNorme") . '</strong>' . $norme
                                . '<br/>'
                                . '<strong>' . __d('pannel', "pannel.textReferentiel") . '</strong>' . $referentiel
                                . '<br/>',
                                ['class' => 'tdleft']
                            ],
                            // Actions
                            [
                                $this->_buttons($params['action'], $result, $params), ['class' => 'tdleft']
                            ],
                        ]
                    ],
                )
                . $this->_rowSupplementaire($params['action'], $result);
            }

            $tbody = $this->Html->tag('tbody', $rows);

            $content = $this->Html->tag('table', $thead.$tbody, ['class' => 'table table-hover']);
            
            // @todo: ajouter des actions si besoin ("Créer un traitement" dans "Mes déclarations en cours de rédaction")
        }

        $head = $this->Html->tag(
            'h4',
            sprintf(__d('pannel', "pannel.traitement{$traitement}"), Hash::get($params, 'count')).$span,
            ['class' => 'card-header']
        );

//        $head = $this->Html->tag(
//            'div',
//            $this->Html->tag(
//                'div',
//                $this->Html->tag('div', $title, ['class' => 'col-md-12']),
//                ['class' => 'row']
//            ),
//            ['class' => 'card-heading']
//        );
//
        $body = $this->Html->tag('div', $content, ['class' => 'card-body']);

        $link = false === empty($params['link']) ? $params['link'] : '';
        
        return $this->Html->tag('div', $head.$body.$link, ['class' => 'card border-primary']);
    }
    
    /**
     * 
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function encoursRedaction(array $banette, array $params = [])
    {
        $params += [
            'action' => 'encours_redaction',
            'count' => $banette['count'],
            'link' => $this->Html->tag(
                'div',
                $this->Html->tag(
                    'div',
                    $this->Html->link('<i class="fa fa-plus fa-lg"></i>' . ' ' . __d('pannel', 'pannel.btnCreerTraitement'), ['#' => '#'], [
                        'escape' => false,
                        'data-toggle' => 'modal',
                        'data-target' => '#modalAddTraitement',
                        'class' => 'btn btn-primary'
                    ]),
                    ['class' => 'col-md-12 text-center']
                ),
                ['class' => 'row bottom10']
            ),
            'nbUserValideur' => $banette['nbUserValideur'],
            'nbUserConsultant' => $banette['nbUserConsultant'],
            'enumsNormes' => $banette['enumsNormes']
        ];
        
        return $this->_banette($banette['results'], $params);
    }

    public function partageServices(array $banette, array $params = [])
    {
        $params += [
            'action' => 'partageServices',
            'count' => $banette['count'],
            'nbUserValideur' => $banette['nbUserValideur'],
            'nbUserConsultant' => $banette['nbUserConsultant'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function attente(array $banette, array $params = [])
    {
        $params += [
            'action' => 'attente',
            'count' => $banette['count'],
            'nbUserValideur' => $banette['nbUserValideur'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function recuValidation(array $banette, array $params = [])
    {
        $params += [
            'action' => 'recuValidation',
            'count' => $banette['count'],
            'nbUserValideur' => $banette['nbUserValideur'],
            'nbUserConsultant' => $banette['nbUserConsultant'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        // @todo
        $cle = [];
        foreach ($banette['results'] as $key => $etatTraitement) {
            if ($etatTraitement['EtatFiche']['etat_id'] == 2 && $etatTraitement['EtatFiche']['actif'] == true) {
                $tampon = $key+1;
                if (array_key_exists($tampon,$banette['results']) == true) {
                    if ($banette['results'][$key+1]['EtatFiche']['etat_id'] == 6 &&$banette['results'][$key+1]['EtatFiche']['actif'] == true){
                        unset($banette['results'][$key]);
                    }
                }
            }
        }
                
        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function recuConsultation(array $banette, array $params = []) {
        $params += [
            'action' => 'recuConsultation',
            'count' => $banette['count'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function refuser(array $banette, array $params = []) {
        $params += [
            'action' => 'refuser',
            'count' => $banette['count'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function archives(array $banette, array $params = []) {
        $params += [
            'action' => 'archives',
            'count' => $banette['count'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }
    
    /**
     * 
     * @param array $banette
     * @param array $params
     * @return type
     * 
     * @access public
     * @created 18/07/2017
     * @version V1.0.0
     */
    public function consulte(array $banette, array $params = []) {
        $params += [
            'action' => 'consulte',
            'count' => $banette['count'],
            'nbUserValideur' => $banette['nbUserValideur'],
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }

    /**
     *
     *
     * @param array $banette
     * @param array $params
     * @return type
     *
     * @access public
     * @created 10/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function initialisationTraitement(array $banette, array $params = []) {
        $params += [
            'action' => 'initialisation',
            'count' => $banette['count'],
            'link' => $this->Html->tag(
                'div',
                $this->Html->tag(
                    'div',
                    $this->Html->link('<i class="fa fa-plus fa-lg"></i>' . ' ' . __d('pannel', 'pannel.btnInitialisationTraitement'), ['#' => '#'], [
                        'escape' => false,
                        'data-toggle' => 'modal',
                        'data-target' => '#modalInitialisationTraitement',
                        'class' => 'btn btn-primary'
                    ]),
                    ['class' => 'col-md-12 text-center']
                ),
                ['class' => 'row bottom10']
            ),
            'enumsNormes' => $banette['enumsNormes']
        ];

        return $this->_banette($banette['results'], $params);
    }

    /**
     * Groupe de boutons correspondant à la banette
     * "Mes déclarations en cours d'initialisation"
     *
     * @param array $result
     * @param array $params
     * @return string
     *
     * @access protected
     * @created 11/12/2019
     * @version V2.0.0
     */
    protected function _buttonsInitialisation(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');

        $traitement_name = $this->_getOutilnom(Hash::extract($result, 'Valeur.{n}'));

        return $this->_button('/fiches/show/#Fiche.id#', $result)
            . $this->_button('/fiches/edit/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, ['onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $this->_button('/fiches/envoyer/#Fiche.id#', $result)
            . $this->_menuEnvoyerTraitement('initialisation', $result, $params)
            . $this->_button('/fiches/delete/#Fiche.id#', $result, [
                'confirm' => __d('pannel', 'pannel.confirmationSupprimerTraitement') . $traitement_name . ' " ?'
            ])
        ;
    }

    /**
     * Retourne la valeur "outilnom" (nom du traitement)
     *
     * @param array $valeursTraitement
     *
     * @access protected
     * @created 11/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _getOutilnom($valeursTraitement)
    {
        $traitement_name = '';

        foreach ($valeursTraitement as $key => $valeur) {
            if ($valeur['champ_name'] === 'outilnom') {
                $traitement_name = $valeur['valeur'];
            }
        }

        return ($traitement_name);
    }

    /**
     *
     * @param array $banette
     * @param array $params
     * @return type
     *
     * @access public
     * @created 12/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    public function allTraitement(array $banette, array $params = []) {
        $params += [
            'action' => 'allTraitements',
            'count' => $banette['count'],
            'enumsNormes' => $banette['enumsNormes'],
        ];

        return $this->_banette($banette['results'], $params);
    }

    /**
     * Groupe de boutons correspondant à la banette "Toutes les déclarations de l'entité"
     *
     * @param array $result
     * @param array $params
     * @return string
     *
     * @access protected
     * @created 12/12/2019
     * @version V2.0.0
     * @author Théo GUILLON <theo.guillon@libriciel.coop>
     */
    protected function _buttonsAllTraitements(array $result, array $params = [])
    {
        $fiche_id = Hash::get($result, 'Fiche.id');

        $btnDupliquer = null;
        if ($this->Autorisation->authorized(ListeDroit::DUPLIQUER_TRAITEMENT, $this->Session->read('Droit.liste')) &&
            !empty($params['mesOrganisations'])
        ) {
            $btnDupliquer = $this->Html->link('<i class="fa fa-copy fa-lg"></i>', ['#' => '#'], [
                'escape' => false,
                'data-toggle' => 'modal',
                'data-target' => '#modalDupliquerTraitement',
                'class' => 'btn btn_duplicate btn-outline-primary borderless',
                'data-id' => $fiche_id,
                'value' => $fiche_id,
                'title' => __d('pannel', 'pannel.commentaireDupliquerTraitement')
            ]);
        }

        return $this->_button('/fiches/show/#Fiche.id#', $result)
//            . $this->_button('/fiches/historique/#Fiche.id#', $result, [
//                'onclick' => "$('#listeValidation{$fiche_id}').toggle(); return false;"
//            ])
            . $this->_View->element('Buttons/ajax_history', [
                'id' => $fiche_id
            ])
            . $btnDupliquer
        ;
    }
}
