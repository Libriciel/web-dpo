<?php
//App::uses('AppHelper', 'View/Helper');
App::uses('LibricielChecksHelper', 'LibricielChecks.View/Helper');

class WebdpoChecksHelper extends LibricielChecksHelper
{
    /**
     *
     * @param array $elements
     * @param string $class
     * @return string
     */
    public function table(array $elements, $class = 'values')
    {
        $rows = [];

        if (!empty($elements)) {
            foreach ($elements as $name => $result) {
                if (!is_array($result)) {
                    $th = $this->Html->tag('th', $name);
                    $rows[] = $this->Html->tag('tr', $th, ['class' => 'col-xs-12']);
                } else {
                    $success = isset($result['success']) && $result['success'] === true;

                    $value = ( !isset($result['value']) ? '' : $result['value'] );

                    $th = $this->Html->tag('th', $name, ['class' => 'col-xs-4']);
                    $tdValue = $this->Html->tag('td', is_array($value) ? var_export($value, true) : $value, [ 'class' => 'col-xs-3' ]);
                    $tdMessage = $this->Html->tag('td', ( !isset($result['message']) ? '' : $result['message'] ), [ 'class' => 'col-xs-4' ]);
                    $tdStatus = $this->Html->tag('td', isset($result['success']) && $result['success'] ? 'OK' : 'Erreur', [ 'class' => ( isset($result['success']) && $result['success'] ? 'col-xs-1 status success' : 'col-xs-1 status error' ) ]);

                    $rows[] = $this->Html->tag(
                        'tr',
                        $this->_statusCell($success)
                        . "{$th}{$tdValue}{$tdMessage}",
                        ['class' => $success === true ? 'success' : 'danger']
                    );
                }
            }
        }

        $tbody = $this->Html->tag('tbody', implode($rows));
        $table = $this->Html->tag(
            'table',
            $tbody,
            [
                'class' => "table table-striped table-bordered libriciel-checks checks {$class}"
            ]
        );

        return $this->Html->tag('div', $table, ['class' => 'table-responsive']);
    }

    public function accordion($title, $content, array $params = [])
    {
        $defaults = [
            'id' => uniqid('check-panel-'),
            'level' => 1,
            'parent' => uniqid('parent-panel-'),
            'expanded' => null
        ];
        $params += $defaults;

        $bodyId = $params['id'] . '-body';
        $headingId = $params['id'] . '-heading';

        // @todo: ="#accordion"
//        $heading = $this->Html->tag(
//            "h{$params['level']}",
//            $this->Html->link(
//                $title,
//                "#{$bodyId}",
//                [
//                    'role' => 'button',
//                    'data-toggle' => 'collapse',
//                    'aria-expanded' => empty($params['expanded']) === false,
//                    'aria-controls' => $bodyId,
//                    'data-parent' => "#{$params['parent']}",
//                    'style' => 'display: block;',
//                ]
//            ),
//            ['class' => 'panel-title']
//        );

        $heading = $this->Html->tag(
            'a',
            $this->Html->tag('b', $title),
            [
                'class' => 'card-link checks-link panel-title',
                'data-toggle' => 'collapse',
                'data-parent' => '#accordion',
                'href' => "#{$bodyId}"
            ]
        );

        $headingDiv = $this->Html->tag(
            'h4',
            $heading,
            [
                'class' => 'libriciel-checks card-header checks-header',
            ]
        );

        $body = $this->Html->tag('div', $content, ['class' => 'panel-body']);

        $bodyDiv = $this->Html->tag(
            'div',
            $body,
            [
                'id' => $bodyId,
                'aria-labelledby' => $headingId,
                'class' => 'card-body collapse',
            ]
        );

        return $this->Html->tag('div', $headingDiv . $bodyDiv, ['class' => 'card border-primary panel']);
    }

    public function javascript()
    {
        $success = LibricielChecksTranslate::singular('Success');
        $error = LibricielChecksTranslate::singular('Error');

        $code = "$(document).ready(function () {
                try {
                    let success = '<i class=\"fa fa-check text-success\"><span class=\"sr-only\">{$success}</span></i> ',
                    error = '<i class=\"fa fa-times text-danger\"><span class=\"sr-only\">{$error}</span></i> ';

                // Traitement des panel-title contenant des statuts en erreur
                $('.libriciel-checks td.status.error').each(function(idxTd, td) {
                    $(td).parents('.panel').find('.panel-title:first').each(function(idxTitle, title) {
                        if ($(title).hasClass('error') === false) {
                            $(title).find('b').prepend(error);
                            $(title).addClass('error');
                            $(title).closest('.panel').addClass('panel-danger');
                        }
                    });
                });
        
                // Traitement des panel-title qui ne sont pas en erreur
                $('.libriciel-checks .panel-title:not(.error)').each(function(idxTitle, title) {
                    $(title).find('b').prepend(success);
                    $(title).closest('.panel').addClass('panel-success');
                });
        
                // Traitement des onglets
                $('.libriciel-checks td.status.error').each(function(idx, td) {
                    var id = $(td).closest('div.tab-pane').attr('id'),
                        link = $('a[href=\"#'+id+'\"]'),
                        li = $(link).closest('li');
        
                    if ($(li).hasClass('tab-error') === false) {
                        $(link).prepend(error);
        
                        $(li).addClass('tab-error');
                    }
                });
                $('ul.nav-tabs li').each(function(idx, li) {
                    if ($(li).hasClass('tab-error') === false) {
                        $(li).find('> a').prepend(success);
                        $(li).addClass('tab-success');
                    }
                });
            } catch(e) {
                console.exception(e);
            }
        });";

        return $this->Html->scriptBlock($code);
    }
}
