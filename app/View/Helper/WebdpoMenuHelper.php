<?php

/**
 * La classe WebdpoMenuHelper simplifie l'écriture d'un menu pour web-dpo
 * en fournissant des méthodes facilitant la construction de menus sous forme de
 * liste non ordonnées (ul) imbriquées.
 */
class WebdpoMenuHelper extends AppHelper {

    public $helpers = ['Html'];

    /**
     * Permet de construire un menu à plusieurs niveaux.
     *
     * Cette méthode ajoute la possibilité de:spécifier la balise qui sera
     * utilisée pour construire les éléments inactifs "parents" d'éléments
     * actifs, d'ajouter un attribut title aux éléments et de désactiver un
     * élément (et ses sous-éléments) en plus des permissions.
     *
     * @param array $items Les éléments du menu
     * @fixme
     * @return string
     */
    public function main($items, $disabledTag = 'a', $level = 0, $extraTopUlClass = null)
    {
        $return = '';

        foreach ($items as $key => $item) {
            if (is_numeric($key) && $item === 'divider') {
                $return .= $this->Html->tag('div', '', ['class' => 'dropdown-divider']);

            } elseif (is_numeric($key) && is_string($item)) {
                $return .= $this->Html->tag('li', $item, ['class' => 'dropdown-header']);

            } elseif (!isset($item['disabled']) || !$item['disabled']) {
                $sub = $item;
                $title = (isset($sub['title']) ? $sub['title'] : false);
                $icon = (isset($sub['icon']) ? $sub['icon'] : false);
                $class = (isset($sub['class']) ? $sub['class'] : false);
                // @todo Faire plus propre, on n'a peut-être pas que les data-* à gérer
                $extra = [];

                foreach ($sub as $subKey => $subValue) {
                    if (strpos($subKey, 'data-') === 0) {
                        $extra[$subKey] = $subValue;
                        unset($sub[$subKey]);
                    }
                }
                unset($sub['url'], $sub['disabled'], $sub['title'], $sub['icon'], $sub['class']);

                $sub = $this->main($sub, $disabledTag, $level+1);

                $content = '';
                $htmlOptions = ['title' => $title];

                if (isset($item['url']['controller']) && $item['url']['controller'] !== '/') {
                    // Cas array('url' => array('controller' => 'moncontroller', 'action' => 'monaction'))
                    $controllerName = $item['url']['controller'];
                    $actionName = isset($item['url']['action']) ? $item['url']['action'] : 'index';
                } elseif (isset($item['url']) && is_string($item['url']) && preg_match('/^\/(?:([\w]+)\/([\w]+)|([\w]+))/', $item['url'], $matches)) {
                    // Cas array('url' => '/moncontroller/monaction')
                    $controllerName = $matches[1];
                    $actionName = isset($matches[2]) ? $matches[2] : 'index';
                } else {
                    $controllerName = '/';
                    $actionName = '/';
                }

                if (isset($item['url']) && ( $item['url'] != '#' )) {
                    $url = $item['url'];
                    if (is_array($url)) {
                        $url += ['plugin' => null];
                    }

                    if ($class !== false) {
                        $contentClass = $class;
                    } else {
                        $contentClass = 'dropdown-item';
                    }

                    $content .= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => $icon === false ? false : "far {$icon} fa-fw"])
                        . ' ' . $key,
                        $url,
                        $extra + ['class' => $contentClass, 'escape'=>false]
                    ). $sub;

                } elseif (!empty($sub)) {
                    $htmlOptions = [];

                    if ($disabledTag == 'a') {
                        $htmlOptions['href'] = '#';
                    }

                    if ($level === 0) {
                        $icon = null;
                        if (!empty($item['icon'])) {
                            $icon = "far {$item['icon']} fa-fw";
                        }

                        $caret = $this->Html->tag('i', '', ['class' => $icon]);

                        $idContent = 'navbar' . str_replace(' ', '_', iconv('UTF-8','ASCII//TRANSLIT', $key));

                        $content .= $this->Html->tag($disabledTag, $caret . $key, [
                            'class' => 'nav-link dropdown-toggle',
                            'href' => '#',
                            'id' => $idContent,
                            'role' => 'button',
                            'data-toggle' => 'dropdown',
                            'aria-haspopup' => 'true',
                            'aria-expanded' => 'false'
                        ] + $htmlOptions) . $sub;
                    } else {
                        $htmlOptions['class'] = 'dropdown-item';
                        $key = sprintf(
                            '%s %s',$this->Html->tag('i', '', ['class' => 'far fa-empty fa-fw']),
                            $key
                        );
                        $content .= $this->Html->tag($disabledTag, $key, $htmlOptions) . $sub;
                    }
                }

                //$liClass = ( empty($sub) ? '' : ($level === 0 || $level === 1 ? 'nav-item dropdown' : 'dropdown-submenu') );
                if ($level === 0) {
                    $liClass = 'nav-item';
                    if (empty($sub) === false) {
                        $liClass .= ' dropdown';
                    }
                } elseif($level === 1 || $level === 2) {
                    if (empty($sub) === false) {
                        $liClass = 'dropdown-submenu';
                    } else {
                        $liClass = 'dropdown-item';
                    }
                } else {
                    $liClass = '';
                }

                $return .= empty($content) ? '' : $this->Html->tag('li', $content, [
                    'class' => $liClass,
                    'title' => $title
                ]);
            }
        }

        $ulParams = $level === 0
            ? ['class' => "navbar-nav {$extraTopUlClass}"]
            : ['class' => 'dropdown-menu', 'role' => 'menu'];
        return empty($return) ? '' : $this->Html->tag('ul', $return, $ulParams);
    }

}
