<?php

App::uses('Conversion', 'Utility');

/**
 * Application Task for Cake
 * @version 5.0.1
 * @since   4.3.0
 * @package app.Console.Command.Task
 */
class AnnexeToolsTask extends Shell
{
    public $uses = [
        'Cron',
        'Fichier'
    ];

    
    /**
     * Fonction de conversion des annexes
     * 
     * @param int $tacheID
     * @return type
     * 
     * @version 1.0.1
     */
    public function conversion($tacheID) {
        $converted = 0;
        $errors = [];

        $annexes = $this->Fichier->find('all', [
            'fields' => [
                'Fichier.nom',
                'Fichier.url'
            ],
            'joins' => [
                $this->Fichier->join('Fiche', ['type' => 'INNER']),
                $this->Fichier->Fiche->join('Organisation', ['type' => 'INNER']),
                $this->Fichier->Fiche->Organisation->join('Cron', ['type' => 'INNER'])
            ],
            'conditions' => [
                'Cron.id' => $tacheID
            ]
        ]);

        // On verifie si le dossier file existe. Si c'est pas le cas on le cree
        create_arborescence_files();

        if (!empty($annexes)) {
            foreach ($annexes as $annexe) {
                $filename = CHEMIN_PIECE_JOINT.$annexe['Fichier']['url'];

                if (file_exists($filename) === true) {
                    $mimeFile = mime_content_type($filename);
                    if ($mimeFile === 'application/pdf') {
                        $target = CHEMIN_PIECE_JOINT_CONVERSION . preg_replace('/\.pdf$/i', '.odt', $annexe['Fichier']['url']);
                        if (file_exists($target) === false) {
                            try {
                                $odt = Conversion::pdf2odt($filename);
                                $written = file_put_contents($target, $odt);
                                if ($written === false) {
                                    $msgstr = 'Impossible d\'écrire dans le fichier %s';
                                    $message = sprintf($msgstr, $filename);
                                    $errors[] = $message;
                                    $this->log($message, LOG_ERROR);//@fixme: this->log dans un shell ?
                                } else {
                                    $converted++;
                                }
                            } catch(Exception $e) {
                                $msgstr = 'Erreur lors de la conversion du fichier %s: %s';
                                $message = sprintf($msgstr, $filename, $e->getMessage());
                                $errors[] = $message;
                                $this->log($message, LOG_ERROR);//@fixme: this->log dans un shell ?
                            }
                        }
                    }
                } else {
                    $msgstr = 'Le fichier d\'annexe %s n\'est pas présent sur le système de fichier.';
                    $message = sprintf($msgstr, $filename);
                    $errors[] = $message;
                    $this->log($message, LOG_ERROR);//@fixme: this->log dans un shell ?
                }
            }
        }

        if (count($errors) === 0) {
            if ($converted === 0) {
                $message = 'Aucune annexe à convertir';
            } else {
                $msgstr = __mn('Annexe convertie: %%d', 'Annexes converties: %%d', $converted);
                $message = sprintf($msgstr, $converted);
            }

            $this->out("<success>{$message}</success>");
            return sprintf('%s %s', Cron::MESSAGE_FIN_EXEC_SUCCES, $message);
        } else {
            $msgstr = __mn('%%d erreur rencontrée: %%s', '%%d erreurs rencontrées: %%s', count($errors));
            $message = sprintf($msgstr, count($errors), implode("\n", $errors));
            $this->err("<error>{$message}</error>");
            return sprintf('%s %s', Cron::MESSAGE_FIN_EXEC_ERROR, $message);
        }
    }

}
