<?php
App::uses('WebdpoChecks', 'Utility');

class ChecksShell extends AppShell
{
    /**
     * La constante à utiliser dans la méthode _stop() en cas de succès.
     */
    const SUCCESS = 0;
    
    /**
     * La constante à utiliser dans la méthode _stop() en cas d'erreur.
     */
    const ERROR = 1;

    /**
     * Nombre d'erreurs relevées dans les résultats.
     *
     * @var integer
     */
    protected $_errors = 0;
    
    /**
     * Vérifie que l'utilisateur qui lance le shell soit bien le même que
     * l'utilisateur du serveur web, afin d'éviter les problèmes de droits
     * sur les fichiers du cache ou les fichiers temporaires.
     */
    public function checkCliUser()
    {
        $whoami = exec('whoami');
        $accepted = ['www-data', 'apache', 'httpd'];

        if (in_array($whoami, $accepted) !== true) {
            $msgstr = 'Mauvais utilisateur (%s), veuillez exécuter ce shell en tant que: %s';

            $Parser = $this->getOptionParser();
            $command = $Parser->command();

            $title = sprintf($msgstr, $whoami, implode(', ', $accepted));
            $message = "<info>Exemple:</info> sudo -u {$accepted[0]} lib/Cake/Console/cake {$command} [...]";
            $this->error($title, $message);
        }
    }
    
    /**
     * Surcharge de la méthode pour vérifier que l'utilisateur qui lance la
     * commande soit le même que l'utilisateur du serveur web.
     *
     * @see AppShell::checkCliUser()
     */
    public function startup()
    {
        parent::startup();
        $this->checkCliUser();
    }
    

    protected function _title($level, $text)
    {
        $this->out(sprintf('%s %s', str_repeat('#', $level), $text), 2);
    }

    protected function _table(array $results)
    {
        $table = [];

        if (empty($results) === false) {
            $table[] = ['', 'Clé', 'Valeur', 'Message'];
        }

        foreach($results as $key => $result) {
            $result += ['value' => null];
            if ($result['success'] === true) {
                $icon = "\xE2\x9C\x94";
            } else {
                $this->_errors++;
                $icon = "\xE2\x9D\x8C";
            }

            $row = [
                $icon,
                $key,
                $result['value'],
                $result['message']
            ];
            $table[] = $row;
        }

        $this->helper('table')->output($table);
        $this->out();
    }

    public function main()
    {
        $results = WebdpoChecks::results();

        $this->_title(1, 'Environnement logiciel');

        $this->_title(2, 'Binaires');
        $this->_table($results['Environment']['binaries']);

        $this->_title(2, 'Installation');

        $this->_title(3, 'Répertoires');
        $this->_table($results['Environment']['directories']);

        $this->_title(3, 'Fichiers');
        $this->_table($results['Environment']['files']);

        $this->_title(3, 'Cache');
        $this->_table($results['Environment']['cache']);

        $this->_title(3, 'Accès au cache');
        $this->_table($results['Environment']['cache_check']);

        $this->_title(3, 'Espace libre');
        $this->_table($results['Environment']['freespace']);

        $this->_title(2, 'PHP');

        $this->_table($results['Php']['informations']);

        $this->_title(3, 'Configuration');
        $this->_table($results['Php']['inis']);

        $this->_title(3, 'Extensions');
        $this->_table($results['Php']['extensions']);

        $this->_title(3, 'Extensions PEAR');
        $this->_table($results['Php']['pear_extensions']);

        $this->_title(2, 'Web-DPO');

        $this->_title(3, 'Configuration');
        $this->_table($results['WebDPO']['configure']);

        $this->_title(3, 'Définitions');
        $this->_table($results['WebDPO']['define']);

        $this->_title(2, 'Services');

        foreach ($results['Services'] as $name => $service) {
            $this->_title(3, $name);
            //@todo: seule la clé 'tests' est remplie
            //$this->_title(4, 'Tests');
            $this->_table($service['tests']);
        }

        if ($this->_errors > 0) {
            $message = __n('%d erreur rencontrée', '%d erreurs rencontrées', $this->_errors);
            $this->err(sprintf($message, $this->_errors));
        } else {
            $this->out('Aucune erreur rencontrée');
        }

        $this->_stop($this->_errors > 0 ? static::ERROR : static::SUCCESS);
    }
}

