<?php

/**
 * CronShell
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Console.Command.Shell
 */

App::uses('CronsComponent', 'Controller/Component');
App::uses('CakeTime', 'Utility');

class CronShell extends Shell {
    public $uses = [
        'Authentification',
        'Cron',
        'ConnecteurLdap'
    ];
    
    public $tasks = [
        'AnnexeTools'
    ];

    /**
     * Function main
     *
     * @version v1.0.0
     */
    public function main() {
        $this->runPending();
    }
    
    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version v1.0.0
     */
    public function getOptionParser() {

        return parent::getOptionParser()
            ->addSubcommand('runPending', [
                'help' => __('Exécute toutes les tâches planifiée en attente.'),
            ])
            ->addSubcommand('runId', [
                'help' => __('Exécute une tâche définie par son identifiant.'),
                'parser' => [
                    'arguments' => [
                        'id' => [
                            'required' => true,
                            'help' => __('ID de la tâche'),
                        ]
                    ]
                ]
            ]);
    }
    
    
    /**
     * Exécute toutes les tâches planifiée en attente.
     * Vérifie si la date/time d'execution prévue est inférieur à la date/time du jour OU si le délais entre 2 exécutions est dépassé
     *
     * @version v1.0.0
     */
    public function runPending() {
        $rapport = date('Y-m-d H:i:s') . "\n";
        
        // lecture des crons à exécuter
        $crons = $this->Cron->find('all', [
            'recursive' => -1,
            'fields' => ['id', 'nom'],
            'conditions' => [
                'OR' => [
                    'next_execution_time <= ' => date('Y-m-d H:i:s'),
                    'next_execution_time' => null,
                ],
                'active' => true,
                'lock' => false
            ],
            'order' => ['next_execution_time ASC']]);
        
        if (!empty($crons)) {
            // exécutions
            foreach ($crons as $cron) {
                $rapport .= $cron['Cron']['id'] . '-' . $cron['Cron']['nom'] . " : \n" . $this->runId($cron['Cron']['id']) . "\n";
            }
        } else {
            $rapport .= "Aucune tâche planifiée à exécuter";
        }
        return $this->out($rapport);
    }
    
    /**
     * 
     * @param type $id
     */
    public function runId($id = null) {
        if ($id === null) {
            $id = $this->args[0];
        }
        
        try {
            // initialisation date
            $executionStartTime = date('Y-m-d H:i:s');

            // lecture du cron à exécuter
            $cron = $this->Cron->find('first', [
                'recursive' => -1,
                'conditions' => [
                    'id' => $id
                ]
            ]);
            
            // Sortie si tâche non trouvée
            if (empty($cron)) {
                throw new Exception('Tâche cron non disponible', '404');
            }

            if ($cron['Cron']['active'] == false) {
                throw new Exception('Tâche cron non active', '401');
            }

            if ($cron['Cron']['lock'] == true) {
                throw new Exception('Tâche déjà en execution', '423');
            }

            $this->Cron->id = $id;

            // Verrouille la tâche pour éviter les exécutions parallèles
            $this->Cron->saveField('lock', true);

            // Execution
            //$output = $this->dispatchShell(sprintf('cron %s %d', ucfirst($cron['Cron']['action']), $id));
            $output = $this->{$cron['Cron']['action']}($id);
        } catch (Exception $e) {
            if ($e->getCode() == '404' or $e->getCode() == '401' or $e->getCode() == '423') {
                $this->out('ERREUR (' . $e->getCode() . '): ' . $e->getMessage());
                return;
            }
            $output = Cron::MESSAGE_FIN_EXEC_ERROR . " Exception levée : \n" . $e->getMessage();
            $this->log($e->getTraceAsString(), 'error');
        }

        // initialisation du rapport d'exécution
        $rappExecution = str_replace([Cron::MESSAGE_FIN_EXEC_SUCCES, Cron::MESSAGE_FIN_EXEC_WARNING, Cron::MESSAGE_FIN_EXEC_ERROR], '', $output);

        $cron['Cron']['lock'] = 0;
        $cron['Cron']['last_execution_start_time'] = $executionStartTime;
        unset($cron['Cron']['created'], $cron['Cron']['modified']);

        if (strpos($output, Cron::MESSAGE_FIN_EXEC_SUCCES) !== false) {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_SUCCES;
        } elseif (strpos($output, Cron::MESSAGE_FIN_EXEC_WARNING) !== false) {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_WARNING;
        } else {
            $cron['Cron']['last_execution_status'] = Cron::EXECUTION_STATUS_FAILED;
        }

        $cron['Cron']['next_execution_time'] = $this->Cron->calcNextExecutionTime($cron);

        $cron['Cron']['last_execution_end_time'] = date('Y-m-d H:i:s');
        $cron['Cron']['last_execution_report'] = $rappExecution;

        if ($this->Cron->save($cron, ['atomic' => true])) {
            return $output;
        } else {
            return Cron::EXECUTION_STATUS_FAILED . "\n" . $output;
        }
    }


    /**
     *
     * @return string
     * @version 4.3
     */
    public function syncLdap($id) {
        $this->writeConfigLdap($id);
        
        try {
            if (Configure::read('LdapManager.Ldap.use')) {
                if ($this->dispatchShell('LdapManager.ldap_manager group_update')) {
                    $output = $this->dispatchShell('LdapManager.ldap_manager user_update');
                    return Cron::MESSAGE_FIN_EXEC_SUCCES . $output;
                } else {
                    throw new Exception('Syncroisation des groupes echoués');
                }
            } else {
                return Cron::MESSAGE_FIN_EXEC_SUCCES . __('Connecteur LDAP non configuré');
            }
        } catch (Exception $e) {
            $output = Cron::MESSAGE_FIN_EXEC_ERROR . " Exception levée : \n" . $e->getMessage();
            $this->log($e->getTraceAsString(), 'error');
        }

        return $output;
    }
    
    /**
     * 
     * @param type $id
     * @return string
     * @throws Exception
     */
    public function writeConfigLdap($id) {
        $organisation_id = $this->Cron->find('first', [
            'conditions' => [
                'id' => $id
            ],
            'fields' => [
                'organisation_id'
            ]
        ]);
        
        $connecteurLdap = $this->ConnecteurLdap->find('first', [
           'conditions' => [
               'organisation_id' => $organisation_id['Cron']['organisation_id']
           ]
        ]);
        
        $connecteurLdap['ConnecteurLdap']['fields']['User'] = [
            'username' => $connecteurLdap['ConnecteurLdap']['username'],
            'civilite' => $connecteurLdap['ConnecteurLdap']['civilite'],
            'note' => $connecteurLdap['ConnecteurLdap']['note'],
            'nom' => $connecteurLdap['ConnecteurLdap']['nom'],
            'prenom' => $connecteurLdap['ConnecteurLdap']['prenom'],
            'email' => $connecteurLdap['ConnecteurLdap']['email'],
            'telfixe' => $connecteurLdap['ConnecteurLdap']['telfixe'],
            'telmobile' => $connecteurLdap['ConnecteurLdap']['telmobile'],
            'active' => $connecteurLdap['ConnecteurLdap']['active']
        ];

        unset ($connecteurLdap['ConnecteurLdap']['username']);
        unset ($connecteurLdap['ConnecteurLdap']['civilite']);
        unset ($connecteurLdap['ConnecteurLdap']['note']);
        unset ($connecteurLdap['ConnecteurLdap']['nom']);
        unset ($connecteurLdap['ConnecteurLdap']['prenom']);
        unset ($connecteurLdap['ConnecteurLdap']['email']);
        unset ($connecteurLdap['ConnecteurLdap']['telfixe']);
        unset ($connecteurLdap['ConnecteurLdap']['telmobile']);
        unset ($connecteurLdap['ConnecteurLdap']['active']);
        
        $authentification = $this->Authentification->find('first', [
           'conditions' => [
               'organisation_id' => $organisation_id['Cron']['organisation_id']
            ]
        ]);

        Configure::write('LdapManager.Ldap', $connecteurLdap['ConnecteurLdap']);

        if (!empty($connecteurLdap['ConnecteurLdap']['certificat_url'])) {
            Configure::write('LdapManager.Ldap.certificat', CHEMIN_CERTIFICATS . $connecteurLdap['ConnecteurLdap']['certificat_url']);
        }

        Configure::write('AuthManager.Authentification', $authentification['Authentification']);
    }
    
    /**
     * 
     */
    public function conversionAnnexes($id) {      
        return $this->AnnexeTools->conversion($id);
    }

}
