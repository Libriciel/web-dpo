<?php

/**
 * MajShell
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Console.Command.Shell
 */

App::uses('LettercaseFormattableBehavior', 'Model/Behavior');

class MajShell extends Shell {
    public $uses = [
        'Champ',
        'Valeur'
    ];
    
    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version v1.0.0
     */
    public function getOptionParser() {

        return parent::getOptionParser()
            ->addSubcommand('majVariables', [
                'help' => __('Mise à jour des noms de variables dans les formulaires et dans les traitements. Suppression accent, majuscule, espace'),
            ]);
    }

    /**
     *
     */
    public function majVariables()
    {
        $success = true;
        $this->Champ->begin();

        $noCheckNomVariable = [
            'declarantpersonnenom',
            'declarantservice',
            'declarantpersonneemail',
            'outilnom',
            'transfertHorsUe',
            'donneesSensible',
            'finaliteprincipale',
            'coresponsable',
            'soustraitant',
            'raisonsociale',
            'telephone',
            'adresse',
            'email',
            'sigle',
            'siret',
            'ape',
            'nomresponsable',
            'prenomresponsable',
            'fonctionresponsable',
            'emailresponsable',
            'telephoneresponsable',
            'dpo',
            'numerodpo',
            'emailDpo'
        ];
        
        $detailsChampsFormulaire = $this->Champ->find('all', [
            'conditions' => [
                'type !=' => [
                    'title',
                    'help',
                    'texte'
                ]
            ],
            'fields' => [
                'id',
                'details'
            ]
        ]);

        foreach ($detailsChampsFormulaire as $detailChamp) {
            $json = (array)json_decode($detailChamp['Champ']['details']);

            if (!in_array($json['name'], $noCheckNomVariable)) {
                $oldChampNameFormulaire = $json['name'];

                $newChampNameFormulaire =  LettercaseFormattableBehavior::formatageVariable($oldChampNameFormulaire);

                if ($oldChampNameFormulaire === $newChampNameFormulaire) {
                    echo ("On ne change rien sur la varible : " . $oldChampNameFormulaire . "\n");
                } else {
                    $json['name'] = $newChampNameFormulaire;

                    $this->Champ->create([
                        'id' => $detailChamp['Champ']['id'],
                        'details' => json_encode($json)
                    ]);
                    $success = $success && false !== $this->Champ->save(null, ['atomic' => false]);

                    if ($success === true) {
                        echo "-----------------------------------------------------------------------------------------\n";
                        echo "Changement du nom de variable dans le FORMULAIRE " . $oldChampNameFormulaire . " par " . $newChampNameFormulaire . " \n";
                        echo "\n";

                        $champsNameTraitement = $this->Valeur->find('all', [
                            'conditions' => [
                                'champ_name' => $oldChampNameFormulaire
                            ],
                            'fields' => [
                                'id'
                            ]
                        ]);

                        foreach ($champsNameTraitement as $champName) {
                            if ($success === true) {
                                echo "Changement du nom de variable dans le TRAITEMENT " . $oldChampNameFormulaire . " par " . $newChampNameFormulaire . "\n";
                                $this->Valeur->create([
                                    'id' => $champName['Valeur']['id'],
                                    'champ_name' => $newChampNameFormulaire
                                ]);
                                $success = $success && false !== $this->Valeur->save(null, ['atomic' => false]);
                            }
                        }
                        echo "-----------------------------------------------------------------------------------------\n";
                    }
                }
            }
        }

        echo "\n";

        if ($success === true) {
            $this->Champ->commit();

            echo "Les noms de variables des formulaires et des traitements ont été mis à jour \n";
        } else {
            $this->Champ->rollback();

            echo "Une erreur est survenue lors de la mise à jour \n";
        }

    }

}
