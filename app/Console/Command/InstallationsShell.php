<?php

/**
 * Installations Shell
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Console.Command.Shell
 */

use Libriciel\Utility\Password\PasswordGeneratorAnssi;

class InstallationsShell extends Shell {

    public $uses = [
        'Admin',
        'User'
    ];
    
    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return Parser $parser
     * @version v1.0.0
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description([
            'Ajout du SALT et du cipherSeed dans le core.php',
            '',
            'Exemple: cake users updateSaltAndCipherSeed -s xxxxxxxxxxxxxxxxxxxxxxxxx -c 00000000000000000000000',
        ]);

        $options = [
            'salt' => [
                'short' => 's',
                'help' => "Chaîne aléatoire de 40 caractères minimum composée de lettre de l'alphabet de A à Z (minuscule et majuscule) et de chiffre entre 0 et 9.",
                'default' => null,
                'required' => false,
            ],
            'cipher' => [
                'short' => 'c',
                'help' => "Chaîne aléatoire de 30 chiffres minimum composée uniquement de chiffre entre 0 et 9.",
                'default' => null,
                'required' => false,
            ]
        ];
        $parser->addOptions($options);

        return $parser;
    }

    public function updateSaltAndCipherSeed()
    {
        $pathCore = CONFIG . 'core.php';

        if (is_readable($pathCore) !== true) {
            $this->error("Le fichier {$pathCore} n'est pas accessible en lecture.");
        }

        if (is_writable($pathCore) !== true) {
            $this->error("Le fichier {$pathCore} n'est pas accessible en écriture.");
        }

        $refNbSalt = 40;
        $salt = Hash::get($this->params, 'salt');
        if (empty($salt)) {
            $salt = PasswordGeneratorAnssi::generate($refNbSalt);
        } else {
            $nbSalt = strlen($salt);
            if ($nbSalt < $refNbSalt) {
                $this->error("Le SALT défini ne respect pas la contrainte de taille : 40 caractères minimum.");
            }

            if (ctype_alnum($salt) === false) {
                $this->error("Le SALT défini ne respect pas la contrainte de composition : lettre de l'alphabet de A à Z (minuscule et majuscule) et de chiffre entre 0 et 9.");
            }
        }

        $refNbCipherSeed = 30;
        $cipherSeed = Hash::get($this->params, 'cipher');
        if (empty($cipherSeed)) {
            $params = ['numbers' => true] + array_fill_keys(array_keys(PasswordGeneratorAnssi::defaults()), false);
            $cipherSeed = PasswordGeneratorAnssi::generate($refNbCipherSeed, $params);
        } else {
            $nbCipherSeed = strlen($cipherSeed);
            if ($nbCipherSeed < $refNbCipherSeed) {
                $this->error("Le cipherSeed défini ne respect pas la contrainte de taille : 30 chiffres minimum.");
            }

            if (is_numeric($cipherSeed) === false) {
                $this->error("Le cipherSeed défini ne respect pas la contrainte numérique : uniquement de chiffre entre 0 et 9.");
            }
        }

        $keySalt = 'webdpo_security_salt_core';
        $keycipherSeed = 'webdpo_security_cipherSeed_core';

        $str = file_get_contents($pathCore);

        if (strpos($str, $keySalt) === false) {
            $this->error("La clé {$keySalt} n'a pas été trouvé dans le fichier {$pathCore}");
        }

        if (strpos($str, $keycipherSeed) === false) {
            $this->error("La clé {$keycipherSeed} n'a pas été trouvé dans le fichier {$pathCore}");
        }

        $str = str_replace($keySalt, $salt, $str);
        $str = str_replace($keycipherSeed, $cipherSeed, $str);

        file_put_contents($pathCore, $str);

        echo ("Les clés {$keySalt} et {$keycipherSeed} ont été modifiées dans le fichier {$pathCore} \n");
        echo ("{$keySalt} = {$salt} \n");
        echo ("{$keycipherSeed} = {$cipherSeed} \n");
    }

}
