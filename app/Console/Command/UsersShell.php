<?php

/**
 * UsersShell
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Console.Command.Shell
 */

use Libriciel\Utility\Password\PasswordGeneratorAnssi;

class UsersShell extends Shell
{
    public $uses = [
        'Admin',
        'User'
    ];
    
    /**
     * Function getOptionParser
     *
     * Options d'exécution et validation des arguments
     *
     * @return ConsoleOptionParser $parser
     * @version v1.0.0
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->description([
            'Création du premier utilisateur superadmin dans l\'application',
            '',
            'Exemple: cake users addFirstUserSuperadmin -p Lem0tdepassedemonSuperadmin',
        ]);

        $options = [
            'password' => [
                'short' => 'p',
                'help' => 'Mot de passe de force 5 minimum',
                'default' => null,
                'required' => false,
            ]
        ];
        $parser->addOptions($options);

        return $parser;
    }

    public function addFirstUserSuperadmin()
    {
        $pathCore = CONFIG . 'core.php';

        $keySalt = 'webdpo_security_salt_core';
        $keycipherSeed = 'webdpo_security_cipherSeed_core';

        $str = file_get_contents($pathCore);

        if (is_int(strpos($str, $keySalt)) === true) {
            $this->error("La clé {$keySalt} dans le fichier {$pathCore}, n'a pas été modifier.");
        }

        if (is_int(strpos($str, $keycipherSeed)) === true) {
            $this->error("La clé {$keycipherSeed} dans le fichier {$pathCore}, n'a pas été modifier.");
        }

        $nbUser = $this->User->find('count');
        if ($nbUser !== 0) {
            $this->error("Il existe déjà au moins un utilisateur en base de données.");
        }

        $success = true;
        $this->User->begin();

        $password = Hash::get($this->params, 'password');
        if (empty($password)) {
            $password = PasswordGeneratorAnssi::generate();
        }

        $data = [
            'User' => [
                'civilite' => 'M.',
                'nom' => 'Admin',
                'prenom' => 'Super',
                'username' => 'superadmin',
                'password' => $password,
                'email' => 'admin@test.fr',
                'createdby' => null
            ]
        ];

        $this->User->create($data);
        $success = false !== $this->User->save(null, ['atomic' => false]);

        if ($success === true) {
            $dataAdmin = [
                'Admin' => [
                    'user_id' => $this->User->getLastInsertID()
                ]
            ];

            $this->Admin->create($dataAdmin);
            $success = false !== $this->Admin->save(null, ['atomic' => false]);
        }

        if ($success === true) {
            $this->User->commit();

            echo "-----------------------------------------------------------------------------------------\n";
            echo "username : superadmin";
            echo "\n";
            echo "password : " . $password;
            echo "\n";
            echo "-----------------------------------------------------------------------------------------\n";
            echo "\n";
        } else {
            $this->User->rollback();
            var_dump($this->User->validationErrors);
            $this->error("Une erreur est survenue lors de l'enregistrement de l'utilisateur.");
        }
    }
}
