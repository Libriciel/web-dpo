<?php

/**
 * FrValidation
 *
 * Validation en Français
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la 
 * réglementation relative à la protection des données personnelles (RGPD)
 * 
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v1.0.0
 * @package     App.Validation
 */

/**
 * Classe de validation des numéros de téléphone et des codes postaux
 * français.
 */
class FrValidation {

    /**
     * Vérifie que la valeur soit celle d'un numéro de téléphone français.
     *
     * @see http://fr.wikipedia.org/wiki/Plan_de_num%C3%A9rotation_t%C3%A9l%C3%A9phonique_en_France#Indicatif_1
     *
     * @param string $check
     * @return bool
     */
    public static function phone($check) {
        $pattern = '/^(0[1-9][0-9]{8}|1[0-9]{1,3}|11[0-9]{4}|3[0-9]{3})$/';
        return (bool) preg_match($pattern, $check);
    }

    /**
     * Vérifie que la valeur soit celle d'un code postal français.
     *
     * @param string $check
     * @return bool
     */
    public static function postal($check) {
        $pattern = '/^\d{5}$/';
        return (bool) preg_match($pattern, $check);
    }

}