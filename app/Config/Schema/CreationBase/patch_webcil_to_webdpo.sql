BEGIN;

--CREATE UNIQUE INDEX organisations_siret_idx ON organisations (siret);

--
-- Création de la table soustraitants
--
CREATE TABLE soustraitants (
    id serial NOT NULL PRIMARY KEY,
    raisonsociale VARCHAR(75) NOT NULL,
    siret VARCHAR(14) NOT NULL,
    ape VARCHAR(5) NOT NULL,
    telephone VARCHAR(15),
    fax VARCHAR(15),
    adresse TEXT,
    email VARCHAR(75),
    createdbyorganisation INT DEFAULT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX soustraitants_siret_idx ON soustraitants (siret);

ALTER TABLE soustraitants ADD CONSTRAINT soustraitants_ape_alpha_numeric_chk CHECK ( cakephp_validate_alpha_numeric( ape ) );
ALTER TABLE soustraitants ADD CONSTRAINT soustraitants_telephone_phone_chk CHECK ( cakephp_validate_phone( telephone, NULL, 'fr' ) );
ALTER TABLE soustraitants ADD CONSTRAINT soustraitants_fax_phone_chk CHECK ( cakephp_validate_phone( fax, NULL, 'fr' ) );
ALTER TABLE soustraitants ADD CONSTRAINT soustraitants_email_email_chk CHECK ( cakephp_validate_email( email ) );

--
-- Création de la table de jointure Soustraitants Organisations
--
CREATE TABLE soustraitants_organisations (
    id SERIAL PRIMARY KEY NOT NULL,
    soustraitant_id INTEGER NOT NULL REFERENCES soustraitants(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organisation_id INTEGER NOT NULL REFERENCES organisations(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE UNIQUE INDEX soustraitants_organisations_soustraitant_id_organisation_id_idx ON soustraitants_organisations (soustraitant_id, organisation_id);

--
-- Création de la table responsables
--
CREATE TABLE responsables (
    id serial NOT NULL PRIMARY KEY,
    nomresponsable VARCHAR(50) NOT NULL,
    prenomresponsable VARCHAR(50) NOT NULL,
    emailresponsable VARCHAR(75) NOT NULL,
    telephoneresponsable VARCHAR(15) NOT NULL,
    fonctionresponsable VARCHAR(75) NOT NULL,
    raisonsocialestructure VARCHAR(75) NOT NULL,
    siretstructure VARCHAR(14) NOT NULL,
    apestructure VARCHAR(5) NOT NULL,
    telephonestructure VARCHAR(15),
    faxstructure VARCHAR(15),
    adressestructure TEXT,
    emailstructure VARCHAR(75),
    createdbyorganisation INT DEFAULT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX responsables_siretstructure_idx ON responsables (siretstructure);

ALTER TABLE responsables ADD CONSTRAINT responsables_apestructure_alpha_numeric_chk CHECK ( cakephp_validate_alpha_numeric( apestructure ) );
ALTER TABLE responsables ADD CONSTRAINT responsables_telephonestructure_phone_chk CHECK ( cakephp_validate_phone( telephonestructure, NULL, 'fr' ) );
ALTER TABLE responsables ADD CONSTRAINT responsables_faxstructure_phone_chk CHECK ( cakephp_validate_phone( faxstructure, NULL, 'fr' ) );
ALTER TABLE responsables ADD CONSTRAINT responsables_emailstructure_email_chk CHECK ( cakephp_validate_email( emailstructure ) );
ALTER TABLE responsables ADD CONSTRAINT responsables_telephoneresponsable_phone_chk CHECK ( cakephp_validate_phone( telephoneresponsable, NULL, 'fr' ) );
ALTER TABLE responsables ADD CONSTRAINT responsables_emailresponsable_email_chk CHECK ( cakephp_validate_email( emailresponsable ) );

ALTER TABLE organisations ADD COLUMN responsable_id INTEGER REFERENCES responsables(id) ON DELETE SET NULL;

--
-- Création de la table de jointure Responsables Organisations
--
CREATE TABLE responsables_organisations (
    id SERIAL PRIMARY KEY NOT NULL,
    responsable_id INTEGER NOT NULL REFERENCES responsables(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organisation_id INTEGER NOT NULL REFERENCES organisations(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX responsables_organisations_soustraitant_id_organisation_id_idx ON responsables_organisations (responsable_id, organisation_id);

--
-- Création de la table connecteur_ldaps
--
CREATE TABLE connecteur_ldaps (
    id SERIAL NOT NULL PRIMARY KEY,
    organisation_id INTEGER NOT NULL REFERENCES organisations (id) ON DELETE CASCADE ON UPDATE CASCADE,
    use BOOLEAN NOT NULL DEFAULT FALSE,
    type VARCHAR(15) NOT NULL,
    host VARCHAR(100) NOT NULL,
    host_fall_over VARCHAR(100) NOT NULL,
    port INTEGER NOT NULL,
    login VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    basedn VARCHAR(100) NOT NULL,
    tls BOOLEAN NOT NULL DEFAULT FALSE,
    version INTEGER NOT NULL,
    account_suffix VARCHAR(100),
    username VARCHAR(100),
    note VARCHAR(100),
    nom VARCHAR(100),
    prenom VARCHAR(100),
    email VARCHAR(100),
    telfixe VARCHAR(100),
    telmobile VARCHAR(100),
    active VARCHAR(100)
);

CREATE UNIQUE INDEX connecteur_ldaps_organisation_id_idx ON connecteur_ldaps (organisation_id);
ALTER TABLE connecteur_ldaps ADD CONSTRAINT connecteur_ldaps_type_in_list_chk CHECK (cakephp_validate_in_list(type, ARRAY['ActiveDirectory', 'OpenLDAP']));
ALTER TABLE connecteur_ldaps ADD CONSTRAINT connecteur_ldaps_version_in_list_chk CHECK (cakephp_validate_in_list(version, ARRAY[1,2,3]));

--
-- Création de la table authentifications
--
CREATE TABLE authentifications (
    id SERIAL NOT NULL PRIMARY KEY,
    organisation_id INTEGER NOT NULL REFERENCES organisations (id) ON DELETE CASCADE ON UPDATE CASCADE,
    use BOOLEAN NOT NULL DEFAULT FALSE,
    type VARCHAR(4) NOT NULL,
    serveur VARCHAR(100),
    port INTEGER,
    uri VARCHAR(100),
    name_fichier VARCHAR
);

CREATE UNIQUE INDEX authentifications_organisation_id_idx ON authentifications (organisation_id);
ALTER TABLE authentifications ADD CONSTRAINT ldaps_type_in_list_chk CHECK (cakephp_validate_in_list(type, ARRAY['CAS', 'LDAP']));

--
-- Création de la table crons 
--
CREATE TABLE crons (
    id SERIAL NOT NULL PRIMARY KEY,
    organisation_id INTEGER NOT NULL REFERENCES organisations (id) ON DELETE CASCADE ON UPDATE CASCADE,
    active BOOLEAN NOT NULL DEFAULT FALSE,
    lock BOOLEAN NOT NULL DEFAULT FALSE,
    nom VARCHAR(100) NOT NULL,
    action VARCHAR(50) NOT NULL,
    next_execution_time timestamp without time zone,
    execution_duration VARCHAR(255),
    last_execution_start_time timestamp without time zone,
    last_execution_end_time timestamp without time zone,
    last_execution_report TEXT,
    last_execution_status VARCHAR(255),
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);

ALTER TABLE fg_formulaires ADD COLUMN soustraitant BOOL NOT NULL DEFAULT FALSE;

ALTER TABLE fiches ADD COLUMN soustraitantid INTEGER DEFAULT NULL;
ALTER TABLE fiches ADD COLUMN coresponsable BOOL NOT NULL DEFAULT FALSE;
ALTER TABLE fiches ADD COLUMN coresponsableid INTEGER DEFAULT NULL;

CREATE UNIQUE INDEX organisations_sigle_idx ON organisations (sigle);


ALTER TABLE organisations RENAME COLUMN cil TO dpo;
ALTER TABLE organisations RENAME COLUMN numerocil TO numerodpo;
UPDATE etats SET libelle = 'Validée par le DPO' WHERE id = 5;

ALTER TABLE users ADD COLUMN notification BOOLEAN NOT NULL DEFAULT FALSE;

COMMIT;