BEGIN;

UPDATE normes SET abroger = true WHERE norme='RU' AND numero='005';
UPDATE normes SET abroger = true WHERE norme='RU' AND numero='063';

UPDATE normes SET abroger = true WHERE norme='AU' AND numero='004';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='006';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='010';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='028';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='034';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='035';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='046';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='047';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='048';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='049';
UPDATE normes SET abroger = true WHERE norme='AU' AND numero='050';

UPDATE normes SET abroger = true WHERE norme='NS' AND numero='009';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='016';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='020';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='042';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='046';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='048';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='049';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='050';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='051';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='052';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='054';
UPDATE normes SET abroger = true WHERE norme='NS' AND numero='057';

CREATE TABLE referentiels (
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(500) NOT NULL,
    description TEXT NOT NULL,
    abroger BOOLEAN DEFAULT FALSE,
    name_fichier VARCHAR,
    fichier VARCHAR(500),
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX referentiels_name_idx ON referentiels (name);

ALTER TABLE fiches ADD COLUMN referentiel_id integer REFERENCES referentiels (id) ON UPDATE CASCADE;

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel relatif à la gestion des ressources humaines', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<h2>L&rsquo;encadrement des traitements courants en mati&egrave;re RH</h2>
<p>Adopt&eacute; &agrave; la suite d&rsquo;une consultation publique, <a title="R&eacute;f&eacute;rentiel relatif aux traitements de donn&eacute;es &agrave; caract&egrave;re personnel mis en oeuvre aux fins de gestion du personnel (PDF, 379 ko) - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/sites/default/files/atoms/files/referentiel_grh_novembre_2019_0.pdf" target="_blank" rel="noopener">ce r&eacute;f&eacute;rentiel</a> s&rsquo;adresse &agrave; l&rsquo;ensemble des organismes priv&eacute;s et publics qui mettent en place des traitements de donn&eacute;es &agrave; des fins de gestion des ressources humaines.</p>
<p>Outil d&rsquo;aide &agrave; la mise en conformit&eacute;, il applique les r&egrave;gles de protection des donn&eacute;es aux traitements courants de gestion du personnel, tels que le recrutement, la gestion administrative du personnel, la r&eacute;mun&eacute;ration, ou encore la mise &agrave; disposition des salari&eacute;s d&rsquo;outils de travail.</p>
<h2>Les traitements exclus du r&eacute;f&eacute;rentiel</h2>
<p>Certains traitements sont exclus du champ d&rsquo;application du r&eacute;f&eacute;rentiel en raison de leurs sp&eacute;cificit&eacute;s&nbsp;et font l&rsquo;objet d&rsquo;un encadrement particulier (<a title="Le contr&ocirc;le d&rsquo;acc&egrave;s biom&eacute;trique sur les lieux de travail - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/le-controle-dacces-biometrique-sur-les-lieux-de-travail" target="_blank" rel="noopener">contr&ocirc;le d&rsquo;acc&egrave;s aux locaux de travail &agrave; l&rsquo;aide des dispositifs biom&eacute;triques</a>, <a title="Dispositifs d&rsquo;alertes professionnelles : publication du r&eacute;f&eacute;rentiel pour les traitements de donn&eacute;es personnelles - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/dispositifs-dalertes-professionnelles-publication-du-referentiel-pour-les-traitements-de-donnees" target="_blank" rel="noopener">dispositif d&rsquo;alertes professionnelles</a>, <a title=" La vid&eacute;osurveillance &ndash; vid&eacute;oprotection au travail - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/la-videosurveillance-videoprotection-au-travail" target="_blank" rel="noopener">vid&eacute;osurveillance</a>, <a title="Ecoute et enregistrement des conversations t&eacute;l&eacute;phoniques sur le lieu de travail - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/declaration/ns-057-ecoute-et-enregistrement-des-conversations-telephoniques-sur-le-lieu-de-travail" target="_blank" rel="noopener">d&rsquo;&eacute;coute et enregistrement des conversations t&eacute;l&eacute;phoniques</a>, des analyses algorithmiques visant &agrave; pr&eacute;dire le comportement ou la productivit&eacute; des salari&eacute;s, etc.). Il en va de m&ecirc;me pour certains traitements invasifs ou ayant recours &agrave; des outils particuli&egrave;rement innovants.</p>
<p>Aussi, un responsable de traitement qui souhaiterait mettre en &oelig;uvre de tels dispositifs devra s&rsquo;assurer de la conformit&eacute; de sa d&eacute;marche &agrave; la r&eacute;glementation en vigueur, en proc&eacute;dant &agrave; sa propre analyse. Il pourra partiellement s&rsquo;aider du pr&eacute;sent r&eacute;f&eacute;rentiel, mais ce dernier ne garantira pas la conformit&eacute; de son traitement.</p>
<h2>Les principales &eacute;volutions du r&eacute;f&eacute;rentiel</h2>
<p>Afin de r&eacute;pondre au mieux aux besoins des organismes, le champ d&rsquo;application du r&eacute;f&eacute;rentiel a &eacute;t&eacute; &eacute;largi et couvre d&eacute;sormais non seulement la gestion des ressources humaines, mais &eacute;galement la gestion de la paye et les traitements les plus r&eacute;pandus en mati&egrave;re de recrutement.</p>
<p>Des d&eacute;veloppements nouveaux ont &eacute;t&eacute; rajout&eacute;s concernant l&rsquo;identification des <a title="Les bases l&eacute;gales - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/les-bases-legales" target="_blank" rel="noopener">bases l&eacute;gales</a> susceptibles de fonder les traitements courants en mati&egrave;re RH. Des pr&eacute;cisions ont &eacute;t&eacute; &eacute;galement apport&eacute;es sur les hypoth&egrave;ses dans lesquelles la r&eacute;alisation d&rsquo;une <a title="L''analyse d&rsquo;impact relative &agrave; la protection des donn&eacute;es (AIPD) - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/RGPD-analyse-impact-protection-des-donnees-aipd" target="_blank" rel="noopener">analyse d&rsquo;impact sur la protection des donn&eacute;es (AIPD)</a> est obligatoire, ou non, pour le responsable de traitement.</p>
<p><a title="Le r&eacute;f&eacute;rentiel relatif &agrave; la gestion des ressources humaines en questions - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/le-referentiel-relatif-la-gestion-des-ressources-humaines-en-questions" target="_blank" rel="noopener">Une FAQ accompagne la publication du r&eacute;f&eacute;rentiel</a> pour r&eacute;pondre aux questions les plus fr&eacute;quentes.</p>
</div>
</div>
</div>', false, 'Referentiel_relatif_aux_traitements_de_données_a_caractere_personnel_mis_en_oeuvre_aux_fins_de_gestion_du_personnel.pdf', '1635145836.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel relatif à la désignation des conducteurs ayant commis une infraction au code la route', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<h2>L&rsquo;encadrement des traitements dans le cadre de la d&eacute;signation des conducteurs</h2>
<p>Les organismes titulaires de certificats d&rsquo;immatriculation de v&eacute;hicules mis &agrave; disposition du public (agences de location, entreprises de livraison) sont destinataires des proc&egrave;s-verbaux en cas d&rsquo;infraction&nbsp;: ils doivent, dans ce cas, d&eacute;signer le conducteur aux autorit&eacute;s comp&eacute;tentes.</p>
<p>Ce r&eacute;f&eacute;rentiel, adopt&eacute; suite &agrave; une consultation publique, permet &agrave; ces organismes d&rsquo;encadrer les traitements de donn&eacute;es relatifs &agrave; ces d&eacute;signations en respectant les droits des personnes concern&eacute;es.</p>
<p>Il actualise l&rsquo;autorisation unique n&deg; 10 qui n&rsquo;a plus de valeur juridique en tant que formalit&eacute; pr&eacute;alable depuis l&rsquo;entr&eacute;e en vigueur du r&egrave;glement g&eacute;n&eacute;ral sur la protection des donn&eacute;es (RGPD).</p>
<h2>P&eacute;rim&egrave;tre du r&eacute;f&eacute;rentiel</h2>
<p>Le r&eacute;f&eacute;rentiel cible trois finalit&eacute;s (objectifs) diff&eacute;rentes&nbsp;:</p>
<ul>
<li>la d&eacute;signation, aupr&egrave;s de l''Agence nationale de traitement automatis&eacute; des infractions (ANTAI), de la personne qui conduisait ou &eacute;tait susceptible de conduire le v&eacute;hicule lorsque l''infraction a &eacute;t&eacute; constat&eacute;e&nbsp;;</li>
<li>le suivi de la proc&eacute;dure de recouvrement des contraventions au code de la route dont peuvent &ecirc;tre redevables p&eacute;cuniairement les organismes publics ou priv&eacute;s&nbsp;;</li>
<li>la r&eacute;alisation de statistiques anonymes (analyses statistiques des types d''infractions routi&egrave;res et des sinistres), notamment en vue d''adapter les formations de pr&eacute;vention routi&egrave;re.</li>
</ul>
<h2>Les principales &eacute;volutions du r&eacute;f&eacute;rentiel</h2>
<p>Les contributions re&ccedil;ues par la CNIL lors de sa consultation publique ont soulev&eacute; diff&eacute;rentes probl&eacute;matiques et ont permis d&rsquo;enrichir le r&eacute;f&eacute;rentiel afin de r&eacute;pondre au mieux aux besoins des organismes concern&eacute;s. Des pr&eacute;cisions ont ainsi &eacute;t&eacute; apport&eacute;es concernant&nbsp;:</p>
<ul>
<li>les organismes concern&eacute;s par le r&eacute;f&eacute;rentiel&nbsp;;</li>
<li>les donn&eacute;es susceptibles d&rsquo;&ecirc;tre collect&eacute;es&nbsp;;</li>
<li>la r&eacute;utilisation des donn&eacute;es&nbsp;;</li>
<li>les destinataires.</li>
</ul>
</div>
</div>
</div>', false, 'referentiel_relatif_aux_traitements_de_donnees_personnelles_mis_en_oeuvre_dans_le_cadre_de_la_designation_des_conducteurs_ayant_commis_une_infraction_au_code_de_la_route.pdf', '1635145900.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel relatif à la gestion locative', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<h2>L&rsquo;encadrement des traitements dans le cadre de la gestion locative</h2>
<p>Les organismes mettant un logement en location ainsi que les interm&eacute;diaires participant &agrave; la mise en location d&rsquo;un logement sont amen&eacute;s &agrave; traiter de nombreuses donn&eacute;es personnelles de candidats &agrave; la location, locataires et garants.</p>
<p>Ce r&eacute;f&eacute;rentiel, adopt&eacute; &agrave; la suite d&rsquo;une consultation publique, permet &agrave; ces organismes d&rsquo;encadrer les traitements de donn&eacute;es relatifs &agrave; la gestion locative en respectant les principes relatifs &agrave; la protection des donn&eacute;es ainsi que les droits des personnes.</p>
<p>Il actualise la norme simplifi&eacute;e n&deg;21 qui n&rsquo;a plus de valeur juridique depuis l&rsquo;entr&eacute;e en application du r&egrave;glement g&eacute;n&eacute;ral sur la protection des donn&eacute;es (RGPD).</p>
<h2>P&eacute;rim&egrave;tre du r&eacute;f&eacute;rentiel</h2>
<p>Le r&eacute;f&eacute;rentiel a vocation &agrave; encadrer l&rsquo;ensemble des traitements mis en &oelig;uvre pendant toute la dur&eacute;e d&rsquo;un contrat de bail.</p>
<p>Il cible ainsi de nombreuses finalit&eacute;s (objectifs) qui peuvent &ecirc;tre regroup&eacute;es en quatre th&eacute;matiques distinctes&nbsp;:</p>
<ul>
<li>la <strong>proposition de biens &agrave; louer</strong> (analyse des crit&egrave;res de potentiels futurs locataires, envoi de propositions de location)&nbsp;;</li>
<li>la <strong>pr&eacute;-contractualisation et la conclusion du contrat de bail</strong> (organisation des visites du logement, appr&eacute;ciation de la solvabilit&eacute; des candidats &agrave; la location, etc.)&nbsp;;</li>
<li>le <strong>d&eacute;roulement du contrat de bail</strong> (suivi du paiement des loyers, v&eacute;rification de la souscription d&rsquo;une assurance, etc.)&nbsp;;</li>
<li>la <strong>fin du contrat</strong> (r&eacute;siliation du contrat, fin de solidarit&eacute; des locataires pour le paiement des loyers).</li>
</ul>
<h2>Les principales &eacute;volutions du r&eacute;f&eacute;rentiel</h2>
<p>Les contributions re&ccedil;ues par la CNIL lors de sa consultation publique ont soulev&eacute; diff&eacute;rentes probl&eacute;matiques et ont permis d&rsquo;enrichir le r&eacute;f&eacute;rentiel afin de r&eacute;pondre au mieux aux besoins des organismes concern&eacute;s. Des pr&eacute;cisions ont ainsi &eacute;t&eacute; apport&eacute;es concernant&nbsp;:</p>
<ul>
<li>les organismes concern&eacute;s par le r&eacute;f&eacute;rentiel&nbsp;;</li>
<li>les donn&eacute;es susceptibles d&rsquo;&ecirc;tre collect&eacute;es&nbsp;;</li>
<li>les destinataires&nbsp;;</li>
<li>les dur&eacute;es de conservation.</li>
</ul>
</div>
</div>
</div>', false, 'referentiel_relatif_aux_traitements_de_donnees_personnelles_mis_en_oeuvre_dans_le_cadre_de_la_gestion_locative.pdf', '1635145939.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel pour la prise en charge médico-sociale des personnes âgées, en situation de handicap ou en difficulté', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<h2>L&rsquo;encadrement des traitements dans le secteur social et m&eacute;dico-social</h2>
<p>Adopt&eacute; &agrave; la suite d&rsquo;une consultation publique, ce r&eacute;f&eacute;rentiel s&rsquo;adresse &agrave; l&rsquo;ensemble des organismes priv&eacute;s ou publics concern&eacute;s par l&rsquo;accueil, l&rsquo;h&eacute;bergement ou l&rsquo;accompagnement des personnes &acirc;g&eacute;es, en situation de handicap ou en difficult&eacute;.</p>
<p>Outil d&rsquo;aide &agrave; la mise en conformit&eacute;, ce r&eacute;f&eacute;rentiel permet d&rsquo;<strong>appliquer les r&egrave;gles de protection des donn&eacute;es aux traitements relevant du secteur social et/ou m&eacute;dico-social</strong>, tels que l&rsquo;instruction, la gestion, l&rsquo;ouverture et/ou le versement des prestations sociales l&eacute;gales ou facultatives ou encore l&rsquo;accompagnement social et m&eacute;dico-social adapt&eacute; aux difficult&eacute;s rencontr&eacute;es.</p>
<p>Suite &agrave; la consultation publique et pour r&eacute;pondre au mieux aux besoins des organismes concern&eacute;s, de nouvelles pr&eacute;cisions ont notamment &eacute;t&eacute; ajout&eacute;es sur&nbsp;:</p>
<ul>
<li>les bases l&eacute;gales qui peuvent &ecirc;tre retenues dans le secteur social et m&eacute;dico-social&nbsp;;</li>
<li>les donn&eacute;es susceptibles d&rsquo;&ecirc;tre collect&eacute;es&nbsp;;</li>
<li>les dur&eacute;es de conservation&nbsp;;</li>
<li>les destinataires&nbsp;;</li>
<li>l&rsquo;information et les droits des personnes concern&eacute;es.</li>
</ul>
<p>Certaines finalit&eacute;s ont &eacute;galement &eacute;t&eacute; regroup&eacute;es.</p>
<p><a title="Questions-r&eacute;ponses sur le r&eacute;f&eacute;rentiel pour le suivi m&eacute;dico-social des personnes &acirc;g&eacute;es, en situation de handicap ou en difficult&eacute; - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/questions-reponses-referentiel-suivi-medico-social-des-personnes-agees-handicap-difficulte" target="_blank" rel="noopener">Une FAQ accompagne la publication du r&eacute;f&eacute;rentiel</a>.</p>
<h2>Les traitements exclus du r&eacute;f&eacute;rentiel</h2>
<p>Certains traitements sont exclus du champ d&rsquo;application du r&eacute;f&eacute;rentiel en raison de leurs sp&eacute;cificit&eacute;s. C&rsquo;est le cas&nbsp;:</p>
<ul>
<li>Des traitements portant sur <strong>la pr&eacute;vention et la protection de l&rsquo;enfance.</strong> Un projet de r&eacute;f&eacute;rentiel distinct est actuellement en cours d&rsquo;&eacute;laboration et fera l''objet d''une consultation publique avant son adoption d&eacute;finitive. Il aura notamment vocation &agrave; regrouper les anciennes autorisations uniques relatives &agrave;&nbsp;:
<ul>
<li>l&rsquo;accompagnement et suivi-social dans le cadre de la pr&eacute;vention et de la protection des mineurs et jeunes majeurs (<a title="Autorisation unique 49 - Accompagnement et suivi social dans le cadre de la pr&eacute;vention et de la protection des mineurs et jeunes majeurs (PDF, 288 ko) - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/sites/default/files/atoms/files/au49.pdf" target="_blank" rel="noopener">AU-49</a>)&nbsp;;</li>
<li>l&rsquo;enfance en danger et &laquo;&nbsp;Informations pr&eacute;occupantes&nbsp;&raquo; (<a title="Autorisation unique 28 - Enfance en danger et &quot;informations pr&eacute;occupantes&quot; (PDF, 44 ko) - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/sites/default/files/atoms/files/au28.pdf" target="_blank" rel="noopener">AU-28</a>).</li>
</ul>
</li>
<li>Des traitements mis en &oelig;uvre par <strong>les mandataires judiciaires &agrave; la protection des majeurs</strong> (MJPM). Dans l&rsquo;attente de la production d&rsquo;un r&eacute;f&eacute;rentiel propre &agrave; ce secteur, les organismes concern&eacute;s peuvent s&rsquo;inspirer de l&rsquo;ancienne <a title="Autorisation unique 50 - Mandataires judiciaires &agrave; la protection des majeurs (PDF, 293 ko) - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/sites/default/files/atoms/files/au50.pdf" target="_blank" rel="noopener">AU-050</a> pour les mandataires judiciaires agr&eacute;&eacute;s &agrave; la protection des majeurs aux fins d&rsquo;assurer la gestion et le suivi de la repr&eacute;sentation juridique, de l&rsquo;assistance et du contr&ocirc;le des personnes plac&eacute;es par l&rsquo;autorit&eacute; judiciaire (sauvegarde judiciaire, curatelle, tutelle, mesure d&rsquo;accompagnement judiciaire).</li>
</ul>
</div>
</div>
</div>', false, 'referentiel_relatif_aux_traitements_de_donnees_personnelles_pour_le_suivi_social_et_medico-social_des_personnes_agees_en_situation_de_handicap_ou_en_difficulte.pdf', '1635145996.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel relatif au dispositif d’alertes professionnelles', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<h2>L&rsquo;encadrement des dispositifs d&rsquo;alertes</h2>
<p>La CNIL a publi&eacute; <a title="R&eacute;f&eacute;rentiel relatif aux traitements de donn&eacute;es &agrave; caract&egrave;re personnel destins&eacute; &agrave; la mise en oeuvre d''un dispositif d''alertes professionnelles - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/sites/default/files/atoms/files/referentiel-alertes-professionnelles_decembre-2019.pdf" target="_blank" rel="noopener">un r&eacute;f&eacute;rentiel relatif aux dispositifs d''alertes professionnelles (DAP)</a>, adopt&eacute; &agrave; la suite d''une consultation publique.</p>
<p>Les traitements respectant les pr&eacute;conisations du r&eacute;f&eacute;rentiel s&rsquo;inscrivent dans le respect des dispositions du RGPD. Le r&eacute;f&eacute;rentiel peut &eacute;galement constituer un outil de r&eacute;f&eacute;rence en vue de la conception d&rsquo;une analyse d&rsquo;impact relative &agrave; la protection des donn&eacute;es (AIPD).</p>
<h2>Les principales &eacute;volutions du r&eacute;f&eacute;rentiel</h2>
<p>Ce r&eacute;f&eacute;rentiel actualise et consolide la doctrine de la CNIL sur les alertes professionnelles, en int&eacute;grant les &eacute;volutions li&eacute;es &agrave; l&rsquo;entr&eacute;e en application du RGPD et &agrave; la modification de la loi &laquo;&nbsp;Informatique et Libert&eacute;s&nbsp;&raquo;. Il s&rsquo;inscrit dans la continuit&eacute; de l&rsquo;autorisation unique AU-004.</p>
<p>Il anticipe par ailleurs certaines &eacute;volutions introduites par la directive europ&eacute;enne relative &agrave; la protection des lanceurs d&rsquo;alerte dont le texte a &eacute;t&eacute; adopt&eacute; d&eacute;but octobre par le Conseil de l&rsquo;Union europ&eacute;enne, pour une application effective pr&eacute;vue &agrave; partir de 2021.&nbsp;</p>
<p>Parmi les &eacute;volutions notables du r&eacute;f&eacute;rentiel figurent&nbsp;:</p>
<ul>
<li>l&rsquo;encadrement des dispositifs r&eacute;sultant &agrave; la fois d''une obligation l&eacute;gale (<a title=" LOI n&deg; 2017-399 du 27 mars 2017 relative au devoir de vigilance des soci&eacute;t&eacute;s m&egrave;res et des entreprises donneuses d''ordre - L&eacute;gifrance - Nouvelle fen&ecirc;tre" href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034290626&amp;dateTexte=20191211" target="_blank" rel="noopener">loi dite&nbsp;&laquo;&nbsp;devoir de vigilance &raquo;</a>, <a title=" LOI n&deg; 2016-1691 du 9 d&eacute;cembre 2016 relative &agrave; la transparence, &agrave; la lutte contre la corruption et &agrave; la modernisation de la vie &eacute;conomique - L&eacute;gifrance - Nouvelle fen&ecirc;tre" href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033558528&amp;categorieLien=id" target="_blank" rel="noopener">loi &laquo;&nbsp;Sapin II&nbsp;&raquo;</a>, etc.), et ceux mis en place &agrave; la seule initiative du responsable de traitement (notamment les alertes dites &laquo;&nbsp;&eacute;thiques&nbsp;&raquo;)&nbsp;;</li>
<li>l&rsquo;instauration d&rsquo;un cadre unique pour l''ensemble des dispositifs d&rsquo;alerte, qui am&eacute;liore leur lisibilit&eacute; pour les personnes concern&eacute;es&nbsp;;</li>
<li>l&rsquo;ajout de pr&eacute;cisions sur les dur&eacute;es de conservation des donn&eacute;es.</li>
</ul>
<p>La CNIL rappelle &eacute;galement que la mise en place d''un tel dispositif vient en compl&eacute;ment des autres possibilit&eacute;s de remont&eacute;es d&rsquo;alertes (comme la voie hi&eacute;rarchique) et ne doit avoir ni pour objet ni pour effet d''exon&eacute;rer l''employeur de ses obligations (telle que celle de pr&eacute;venir les risques psychosociaux), et du respect de la r&egrave;glementation qui lui est applicable (droits et libert&eacute;s fondamentales, Code du travail, etc.).</p>
<p>Une <a title="Le r&eacute;f&eacute;rentiel relatif au dispositif d&rsquo;alertes professionnelles en questions - Nouvelle fen&ecirc;tre" href="https://www.cnil.fr/fr/le-referentiel-relatif-au-dispositif-dalertes-professionnelles-en-questions" target="_blank" rel="noopener">FAQ</a> pour r&eacute;pondre &agrave; certaines questions pratiques r&eacute;guli&egrave;rement pos&eacute;es &agrave; la CNIL accompagne la publication du r&eacute;f&eacute;rentiel.</p>
</div>
</div>
</div>', false, 'Referentiel_relatif_au_dispositif_d_alertes_professionnelles.pdf', '1635146060.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel vigilance sanitaire', '<div class="field field-name-body field-type-text-with-summary field-label-hidden">
<div class="field-items">
<div class="field-item even">
<p>Avant l&rsquo;entr&eacute;e en application du RGPD, la CNIL avait adopt&eacute; une autorisation unique sp&eacute;cifiquement d&eacute;di&eacute;e &agrave; la pharmacovigilance.</p>
<p>Ce nouveau r&eacute;f&eacute;rentiel d&eacute;cline au domaine des vigilances sanitaires, de mani&egrave;re pragmatique et op&eacute;rationnelle, les principes du RGPD.</p>
<p>Ce r&eacute;f&eacute;rentiel fait suite &agrave; une concertation organis&eacute;e aupr&egrave;s de l&rsquo;Institut national des donn&eacute;es de sant&eacute; (INDS) et d&rsquo;organismes publics et priv&eacute;s repr&eacute;sentatifs des acteurs concern&eacute;s*. Cette concertation a permis aux professionnels de soumettre des propositions. Cela a notamment conduit &agrave; exclure express&eacute;ment du r&eacute;f&eacute;rentiel les traitements de donn&eacute;es &agrave; caract&egrave;re personnel mis en &oelig;uvre par les professionnels et &eacute;tablissements de sant&eacute; ainsi que par les agences sanitaires. De m&ecirc;me, &agrave; la suite de cette concertation, la dur&eacute;e de conservation des donn&eacute;es a &eacute;t&eacute; allong&eacute;e.</p>
<p>Le syst&egrave;me des vigilances sanitaires fonctionne sur la base de principes communs, en termes de finalit&eacute;, de cat&eacute;gories de donn&eacute;es trait&eacute;es, et de destinataires. La CNIL a donc &eacute;labor&eacute; un r&eacute;f&eacute;rentiel unique, applicable &agrave; l&rsquo;ensemble des vigilances sanitaires (pharmacovigilance, addictovigilance, biovigilance, cosm&eacute;tovigilance, h&eacute;movigilance, etc.).</p>
<p>Si les traitements de donn&eacute;es &agrave; caract&egrave;re personnel des organismes respectent toutes les exigences qui y sont fix&eacute;es, ces organismes n&rsquo;ont pas &agrave; demander &agrave; la CNIL une autorisation&nbsp;: ils peuvent se contenter de proc&eacute;der en ligne &agrave; une simple <a href="https://declarations.cnil.fr/declarations/declaration/brouillon.action?declarationType=DS" target="_blank" rel="noopener">d&eacute;claration de conformit&eacute;</a> avant de mettre en &oelig;uvre leurs traitements de donn&eacute;es. Si, en revanche, ils souhaitent s&rsquo;&eacute;carter du r&eacute;f&eacute;rentiel, ces organismes devront formuler une <a href="https://declarations.cnil.fr/declarations/declaration/brouillon.action?declarationType=DT" target="_blank" rel="noopener">demande d&rsquo;autorisation</a> aupr&egrave;s de la CNIL.</p>
<p>Une FAQ accompagne la publication du r&eacute;f&eacute;rentiel &laquo;&nbsp;vigilances sanitaires&nbsp;&raquo; afin d&rsquo;en faciliter la compr&eacute;hension.</p>
<p>&nbsp;</p>
<p><em>*Sant&eacute; publique France, Agence nationale de s&eacute;curit&eacute; du m&eacute;dicament et des produits de sant&eacute;, Autorit&eacute; de s&ucirc;ret&eacute; nucl&eacute;aire, Agence de la biom&eacute;decine, Agence nationale de s&eacute;curit&eacute; sanitaire de l&rsquo;alimentation, de l&rsquo;environnement et du travail, minist&egrave;re des solidarit&eacute;s et de la sant&eacute;, Les Entreprises du m&eacute;dicament, le Syndicat National de l&rsquo;Industrie des Technologies M&eacute;dicales, France Assos Sant&eacute;.</em></p>
</div>
</div>
</div>', false, 'referentiel_vigilances_sanitaires.pdf', '1635146149.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel pour la gestion des traitements courants des cabinets médicaux et paramédicaux', '<p style="font-size: 18px;">Pour aider les professionnels de sant&eacute; lib&eacute;raux dans leurs d&eacute;marches de conformit&eacute;, la CNIL a adopt&eacute; un nouveau r&eacute;f&eacute;rentiel qui recense et applique les principes du RGPD aux traitements de <a title="Donn&eacute;e sensible - nouvelle page" href="https://www.cnil.fr/fr/definition/donnee-sensible" target="_blank" rel="noopener">donn&eacute;es sensibles</a> couramment mis en &oelig;uvre dans le cadre de la gestion m&eacute;dicale et administrative d&rsquo;une patient&egrave;le.</p>
<h3>Faciliter la mise en conformit&eacute;</h3>
<p style="font-size: 18px;">Ce r&eacute;f&eacute;rentiel est <strong>un cadre de r&eacute;f&eacute;rence</strong> <strong>qui permet aux professionnels de sant&eacute; lib&eacute;raux de mettre en conformit&eacute; les traitements de donn&eacute;es personnelles utilis&eacute;s pour la gestion de leurs cabinets m&eacute;dicaux et param&eacute;dicaux</strong>. Il a &eacute;t&eacute; adopt&eacute; &agrave; la suite d&rsquo;une consultation des principaux repr&eacute;sentants du secteur.</p>
<p style="font-size: 18px;">Il a vocation &agrave; remplacer l&rsquo;ancienne norme simplifi&eacute;e NS-50 destin&eacute;e aux membres des professions m&eacute;dicales et param&eacute;dicales exer&ccedil;ant &agrave; titre lib&eacute;ral &agrave; des fins de gestion de leur cabinet.</p>
<p style="font-size: 18px;"><strong>Le r&eacute;f&eacute;rentiel n&rsquo;est pas contraignant</strong>. Les responsables de traitement peuvent s&rsquo;&eacute;carter de ses pr&eacute;conisations (par exemple, en identifiant d&rsquo;autres bases de traitement pour tel ou tel traitement sp&eacute;cifique, etc.), &agrave; condition toutefois de pouvoir justifier leur choix et sous leur responsabilit&eacute;.</p>
<h3>Qui est concern&eacute; par le r&eacute;f&eacute;rentiel&nbsp;?</h3>
<p style="font-size: 18px;">Il s&rsquo;agit des <strong>professionnels de sant&eacute;, exer&ccedil;ant &agrave; titre lib&eacute;ral, en cabinet individuel ou group&eacute;, ou encore au sein de maisons de sant&eacute;</strong>. Sont donc concern&eacute;s les m&eacute;decins g&eacute;n&eacute;ralistes ou sp&eacute;cialistes, les infirmiers, les radiologues, les masseurs kin&eacute;sith&eacute;rapeutes, les sages-femmes, les p&eacute;dicures-podologues, les orthophonistes et orthoptistes etc.</p>
<p style="font-size: 18px;">La r&eacute;f&eacute;rentiel n&rsquo;a en revanche pas vocation &agrave; s&rsquo;appliquer aux traitements mis en &oelig;uvre par les services de soins (&eacute;tablissements de sant&eacute;, centres de sant&eacute;, communaut&eacute;s professionnelles territoriales de sant&eacute;, etc.), ni &agrave; ceux mis en &oelig;uvre par les services de m&eacute;decine&nbsp;d&rsquo;entit&eacute;s publiques ou priv&eacute;es (m&eacute;decine du travail, m&eacute;decine scolaire, PMI, etc.), par les pharmaciens, par les laboratoires d&rsquo;analyses de biologie m&eacute;dicale ou par les opticiens.</p>
<h3>Les principales &eacute;volutions par rapport &agrave; la norme simplifi&eacute;e 50</h3>
<p style="font-size: 18px;">Certaines r&egrave;gles de fond ont &eacute;t&eacute; pr&eacute;cis&eacute;es&nbsp;: celles relatives &agrave; l&rsquo;identification des bases l&eacute;gales susceptibles de fonder des traitements en mati&egrave;re de gestion m&eacute;dicale et administrative de la patient&egrave;le, ou encore en mati&egrave;re de s&eacute;curit&eacute; des donn&eacute;es.</p>
<p style="font-size: 18px;">Ont &eacute;galement &eacute;t&eacute; int&eacute;gr&eacute;es les nouvelles obligations li&eacute;es au processus de conformit&eacute;, concernant notamment la tenue d&rsquo;un registre des traitements <strong>ainsi que l&rsquo;&eacute;laboration d&rsquo;une AIPD dans certaines hypoth&egrave;ses bien pr&eacute;cises</strong>.</p>
<ul>
<li><a title="R&eacute;f&eacute;rentiel Cabinet m&eacute;dicaux - nouvelle fen&ecirc;tre " href="https://www.cnil.fr/sites/default/files/atoms/files/referentiel_-_cabinet.pdf" target="_blank" rel="noopener"><strong>Pour en savoir plus</strong> :&nbsp;Un r&eacute;f&eacute;rentiel pour la gestion des traitements courants des cabinets m&eacute;dicaux et param&eacute;dicaux</a></li>
</ul>', false, 'Referentiel_pour_la_gestion_des_traitements_courants_des_cabinets_medicaux_et_paramedicaux.pdf', '1635146223.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel vise les traitements de données dans le domaine de la santé', '<p style="font-size: 18px;">La gestion du cycle de vie des donn&eacute;es et la d&eacute;termination de leurs dur&eacute;es de conservation constituent une &eacute;tape indispensable dans la mise en conformit&eacute; des traitements de donn&eacute;es personnelles des organismes publics et priv&eacute;s.</p>
<p style="font-size: 18px;">La CNIL, qui a publi&eacute; <a title="Guide dur&eacute;e de conservation - nouvelle fen&ecirc;tre " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">un guide des dur&eacute;es de conservation</a>, adopte &eacute;galement deux r&eacute;f&eacute;rentiels sp&eacute;cifiques aux secteurs de la sant&eacute; et de la recherche en sant&eacute;.</p>
<h3>Les r&eacute;f&eacute;rentiels sp&eacute;cifiques pour identifier les dur&eacute;es pertinentes dans le secteur de la sant&eacute; et de la recherche</h3>
<p style="font-size: 18px;">Les r&eacute;f&eacute;rentiels de dur&eacute;es de conservation ont pour objectif d&rsquo;accompagner, de mani&egrave;re op&eacute;rationnelle, les acteurs dans <strong>l&rsquo;identification et la d&eacute;termination de la dur&eacute;e&nbsp;pertinente pour les traitements</strong>.</p>
<p>Le r&eacute;f&eacute;rentiel vise les traitements de donn&eacute;es dans le domaine de la sant&eacute; &ndash; hors recherche (ex : tenue du dossier patient, ordonnancier, vigilances sanitaires, etc.)</p>
<p style="font-size: 18px;">Ils sont une aide &agrave; la <strong>prise de d&eacute;cision</strong> en orientant le responsable de traitement vers&nbsp;:</p>
<ul>
<li>les dur&eacute;es obligatoires du fait de la r&eacute;glementation en vigueur, et en particulier le Code de la sant&eacute; publique&nbsp;;</li>
<li>les dur&eacute;es recommand&eacute;es par la CNIL, qui sont des points de rep&egrave;re pour d&eacute;terminer la dur&eacute;e pertinente.</li>
</ul>
<p style="font-size: 18px;"><strong>Les r&eacute;f&eacute;rentiels ne sont pas exhaustifs</strong>&nbsp;: ils listent les dur&eacute;es pertinentes pour les traitements les plus fr&eacute;quents pour ces deux secteurs d&rsquo;activit&eacute;.</p>
<h3>Pour aller plus loin&nbsp;: le guide des dur&eacute;es de conservation</h3>
<p style="font-size: 18px;">La CNIL a publi&eacute; <a title="guide pratique conservation des donn&eacute;es - nouvelle fen&ecirc;tre  " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">un guide pratique</a>, plus g&eacute;n&eacute;ral,&nbsp;qui a vocation &agrave; apporter les r&eacute;ponses aux questions les plus fr&eacute;quentes des professionnels sur le principe de limitation de la conservation des donn&eacute;es. Il d&eacute;taille les &eacute;l&eacute;ments cl&eacute;s de cette obligation et apporte des conseils pratiques pour l&rsquo;impl&eacute;menter de mani&egrave;re concr&egrave;te au sein des organismes publics ou priv&eacute;es.</p>
<p style="font-size: 18px;">&Eacute;labor&eacute; en partenariat avec le <strong>Service interminist&eacute;riel des archives de France (SIAF)</strong>, le guide met en &nbsp;relation les obligations du RGPD et celles du Code du patrimoine.</p>
<ul>
<li><a title="Guide pratique dur&eacute;e de conservation des donn&eacute;es " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">Consulter le guide pratique</a></li>
</ul>', false, 'Referentiel_vise_les_traitements_de_donnees_dans_le_domaine_de_la_sante.pdf', '1635146344.pdf', NOW(), NOW());

INSERT INTO referentiels (name, description, abroger, name_fichier, fichier, created, modified) VALUES ('Référentiel vis les traitements de données mis en œuvre à des fins de recherche, d’étude, et d’évaluation dans le domaine de la santé', '<p style="font-size: 18px;">La gestion du cycle de vie des donn&eacute;es et la d&eacute;termination de leurs dur&eacute;es de conservation constituent une &eacute;tape indispensable dans la mise en conformit&eacute; des traitements de donn&eacute;es personnelles des organismes publics et priv&eacute;s.</p>
<p style="font-size: 18px;">La CNIL, qui a publi&eacute; <a title="Guide dur&eacute;e de conservation - nouvelle fen&ecirc;tre " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">un guide des dur&eacute;es de conservation</a>, adopte &eacute;galement deux r&eacute;f&eacute;rentiels sp&eacute;cifiques aux secteurs de la sant&eacute; et de la recherche en sant&eacute;.</p>
<h3>Les r&eacute;f&eacute;rentiels sp&eacute;cifiques pour identifier les dur&eacute;es pertinentes dans le secteur de la sant&eacute; et de la recherche</h3>
<p style="font-size: 18px;">Les r&eacute;f&eacute;rentiels de dur&eacute;es de conservation ont pour objectif d&rsquo;accompagner, de mani&egrave;re op&eacute;rationnelle, les acteurs dans <strong>l&rsquo;identification et la d&eacute;termination de la dur&eacute;e&nbsp;pertinente pour les traitements</strong>.</p>
<p>Le r&eacute;f&eacute;rentiel vis les traitements de donn&eacute;es mis en &oelig;uvre &agrave; des fins de recherche, d&rsquo;&eacute;tude, et d&rsquo;&eacute;valuation dans le domaine de la sant&eacute; (ex : les recherches interventionnelles, les recherches sur des donn&eacute;es d&eacute;j&agrave; collect&eacute;es, etc.).</p>
<p style="font-size: 18px;">Ils sont une aide &agrave; la <strong>prise de d&eacute;cision</strong> en orientant le responsable de traitement vers&nbsp;:</p>
<ul>
<li>les dur&eacute;es obligatoires du fait de la r&eacute;glementation en vigueur, et en particulier le Code de la sant&eacute; publique&nbsp;;</li>
<li>les dur&eacute;es recommand&eacute;es par la CNIL, qui sont des points de rep&egrave;re pour d&eacute;terminer la dur&eacute;e pertinente.</li>
</ul>
<p style="font-size: 18px;"><strong>Les r&eacute;f&eacute;rentiels ne sont pas exhaustifs</strong>&nbsp;: ils listent les dur&eacute;es pertinentes pour les traitements les plus fr&eacute;quents pour ces deux secteurs d&rsquo;activit&eacute;.</p>
<h3>Pour aller plus loin&nbsp;: le guide des dur&eacute;es de conservation</h3>
<p style="font-size: 18px;">La CNIL a publi&eacute; <a title="guide pratique conservation des donn&eacute;es - nouvelle fen&ecirc;tre  " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">un guide pratique</a>, plus g&eacute;n&eacute;ral,&nbsp;qui a vocation &agrave; apporter les r&eacute;ponses aux questions les plus fr&eacute;quentes des professionnels sur le principe de limitation de la conservation des donn&eacute;es. Il d&eacute;taille les &eacute;l&eacute;ments cl&eacute;s de cette obligation et apporte des conseils pratiques pour l&rsquo;impl&eacute;menter de mani&egrave;re concr&egrave;te au sein des organismes publics ou priv&eacute;es.</p>
<p style="font-size: 18px;">&Eacute;labor&eacute; en partenariat avec le <strong>Service interminist&eacute;riel des archives de France (SIAF)</strong>, le guide met en &nbsp;relation les obligations du RGPD et celles du Code du patrimoine.</p>
<ul>
<li><a title="Guide pratique dur&eacute;e de conservation des donn&eacute;es " href="https://www.cnil.fr/sites/default/files/atoms/files/guide_durees_de_conservation.pdf" target="_blank" rel="noopener">Consulter le guide pratique</a></li>
</ul>', false, 'Referentiel_vis_les_traitements_de_donnees_mis_en_oeuvre_a_des_fins_de_recherche_d_etude_et_d_evaluation_dans_le_domaine_de_la_sante.pdf', '1635146435.pdf', NOW(), NOW());

INSERT INTO liste_droits (libelle, value, created, modified) VALUES
('Créer un référentiel', 40, NOW(), NOW()),
('Visualiser un référentiel', 41, NOW(), NOW()),
('Modifier un référentiel', 42, NOW(), NOW()),
('Abroger un référentiel', 43, NOW(), NOW());

INSERT INTO role_droits (role_id, liste_droit_id)
SELECT roles.id, liste_droits.id
FROM roles
         INNER JOIN liste_droits ON (1 = 1)
WHERE
        roles.libelle = 'DPO'
  AND liste_droits.libelle IN (
    'Créer un référentiel',
    'Visualiser un référentiel',
    'Modifier un référentiel',
    'Abroger un référentiel'
  )
  AND liste_droits.id NOT IN (
    SELECT existing_roles_droits.liste_droit_id
    FROM role_droits AS existing_roles_droits
    WHERE existing_roles_droits.role_id = roles.id
)
ORDER BY roles.id, liste_droits.id;

INSERT INTO droits (organisation_user_id, liste_droit_id, created, modified)
SELECT organisations_users.id, liste_droits.id, NOW(), NOW()
FROM organisations_users
         INNER JOIN liste_droits ON (1 = 1)
WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
    SELECT organisations.id, organisations.dpo
    FROM organisations
    WHERE organisations.dpo IS NOT NULL
)
  AND liste_droits.libelle IN (
    'Créer un référentiel',
    'Visualiser un référentiel',
    'Modifier un référentiel',
    'Abroger un référentiel'
  )
  AND liste_droits.id NOT IN (
    SELECT existing_droits.liste_droit_id
    FROM droits AS existing_droits
    WHERE existing_droits.organisation_user_id = organisations_users.id
)
ORDER BY organisations_users.id, liste_droits.id;

COMMIT;
