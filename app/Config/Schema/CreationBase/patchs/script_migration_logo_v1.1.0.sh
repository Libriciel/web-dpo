#!/bin/sh

#chemin du dossier de web-DPO v1.0.1
cheminOldwebDPO='/var/www/web-dpo-1.0.1'

#chemin du dossier de web-DPO v1.1.0
cheminNewwebDPO='/var/www/web-dpo-1.1.0'

#chemin du dossier avec tous les documents de web-DPO
newCheminLogo='/data/workspace'

#utilisateur de la base de données
user=webdpo

#mot de passe de la base de données
export PGPASSWORD=webdpo

#la base de données
bdd=webdpo

##################################################################################################################
cheminOldLogoswebDPO="$cheminOldwebDPO/app/webroot/img/logos/"
cheminNewLogo="$newCheminLogo/files/logos"
fichierSaveSQL="$cheminNewwebDPO/app/Config/Schema/CreationBase/patchs/migration_logo.sql"
cheminNewLogo="$newCheminLogo/files/logos"

organisations_id=`psql -X -A -h "localhost" -d "$bdd" -U "$user"  -v "ON_ERROR_STOP=1" -t -c "SELECT id FROM organisations ORDER BY id;"`

if [ -d "$cheminNewLogo" ];then
  echo "Le dossier cible existe !";
else
  echo "Le dossier cible n'existe pas !"
  echo "Création du dosser : $cheminNewLogo";
  mkdir -p $cheminNewLogo
fi

echo "BEGIN;" >> $fichierSaveSQL

for fichier in $cheminOldLogoswebDPO*
do
  nameFichierLogo=$(basename $fichier .${fichier##*.})
  nameWithExtentionFichierLogo=$(basename $fichier .${fichier%.*})

  for organisation_id in $organisations_id
  do
    if [ $organisation_id = $nameFichierLogo ]
    then
      newDossier=$cheminNewLogo/$nameFichierLogo
      if [ ! -d "$newDossier" ];then
        echo "Création du dossier : $newDossier";
        mkdir $newDossier

        echo "Copie du fichier : $nameWithExtentionFichierLogo"
        cp $cheminOldLogoswebDPO/$nameWithExtentionFichierLogo $newDossier/$nameWithExtentionFichierLogo

        echo  "UPDATE organisations SET logo = '$nameFichierLogo/$nameWithExtentionFichierLogo' WHERE id = $nameFichierLogo;" >> $fichierSaveSQL
      fi
    fi
  done
done

echo "COMMIT;" >> $fichierSaveSQL

psql -h "localhost" -d "$bdd" -U "$user" -f $fichierSaveSQL