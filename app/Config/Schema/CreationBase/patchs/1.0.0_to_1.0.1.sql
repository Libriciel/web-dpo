BEGIN;

INSERT INTO crons(organisation_id, nom, action, active, lock, created, modified)
    SELECT
            id,
            'Conversion des annexes',
            'conversionAnnexes',
            'false',
            'false',
            NOW(),
            NOW()
        FROM organisations
        WHERE NOT EXISTS(
            SELECT existant.id
                FROM crons AS existant
                WHERE
                    existant.organisation_id = organisations.id
                    AND existant.action = 'conversionAnnexes'
        );

CREATE UNIQUE INDEX fichiers_url_idx ON fichiers (url);

ALTER TABLE organisations ADD COLUMN prefixenumero VARCHAR(50) NOT NULL DEFAULT 'DPO-';
ALTER TABLE organisations ADD COLUMN numeroregistre INTEGER NOT NULL DEFAULT (0);
CREATE UNIQUE INDEX fiches_organisation_id_numero_idx ON fiches (organisation_id, numero);

COMMIT;