BEGIN;

ALTER TABLE organisations ADD COLUMN civiliteresponsable VARCHAR(4) NOT NULL DEFAULT 'M.';
ALTER TABLE organisations ALTER COLUMN civiliteresponsable drop default;
ALTER TABLE organisations ADD CONSTRAINT organisations_civiliteresponsable_in_list_chk CHECK (cakephp_validate_in_list(civiliteresponsable, ARRAY['M.', 'Mme.']));

UPDATE users SET nom = UPPER(nom);
UPDATE organisations SET nomresponsable = UPPER(nomresponsable);

ALTER TABLE users ADD COLUMN ldap BOOLEAN DEFAULT FALSE;
ALTER TABLE users ALTER COLUMN notification drop not null;

ALTER TABLE organisations ADD COLUMN rgpd BOOLEAN DEFAULT FALSE;
ALTER TABLE organisations ALTER COLUMN verrouiller DROP NOT NULL;
ALTER TABLE organisations ALTER COLUMN verrouiller SET DEFAULT FALSE;

INSERT INTO liste_droits (libelle, value, created, modified) VALUES
('Créer un service', 16, NOW(), NOW()),
('Modifier un service', 17, NOW(), NOW()),
('Supprimer un service', 18, NOW(), NOW()),
('Créer une norme', 19, NOW(), NOW()),
('Visualiser une norme', 20, NOW(), NOW()),
('Modifier une norme', 21, NOW(), NOW()),
('Abroger une norme', 22, NOW(), NOW()),
('Gestion des sous-traitants', 23, NOW(), NOW()),
('Gestion des co-responsables', 24, NOW(), NOW()),
('Gestion de la maitenance', 25, NOW(), NOW()),
('Gestion des modèles', 26, NOW(), NOW()),
('Gestion des formulaires', 27, NOW(), NOW());

-- On crée un nouveau role "DPO" pour les organisations qui ne l'on pas
INSERT INTO roles(organisation_id, libelle, created, modified)
    SELECT
            id,
            'DPO',
            NOW(),
            NOW()
        FROM organisations
        WHERE NOT EXISTS(
            SELECT existant.id
                FROM roles AS existant
                WHERE
                    existant.organisation_id = organisations.id
                    AND existant.libelle = 'DPO'
        );

-- On supprime tout les droits attaché au profils DPO (au cas ou)
DELETE FROM role_droits WHERE role_id IN (SELECT id FROM roles WHERE libelle = 'DPO');

-- On insert tous les droits (sauf création d'une organisation) au profil "DPO"
INSERT INTO role_droits (role_id, liste_droit_id)
    SELECT roles.id, liste_droits.id
        FROM roles
            LEFT OUTER JOIN liste_droits ON (1 = 1)
        WHERE
            roles.libelle = 'DPO'
            AND liste_droits.libelle <> 'Créer une organisation'
        ORDER BY roles.id, liste_droits.id;

-- On supprimer les droits "Administrateur" des DPO
DELETE FROM droits WHERE organisation_user_id IN (
    SELECT organisations_users.id
        FROM organisations_users
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
            SELECT organisations.id, organisations.dpo
                FROM organisations
                WHERE organisations.dpo IS NOT NULL
        )
);

-- On insert les droits "DPO" aux DPOs
INSERT INTO droits (organisation_user_id, liste_droit_id, created, modified)
    SELECT organisations_users.id, liste_droits.id, NOW(), NOW()
        FROM organisations_users
            LEFT OUTER JOIN liste_droits ON (1 = 1)
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
                SELECT organisations.id, organisations.dpo
                    FROM organisations
                    WHERE organisations.dpo IS NOT NULL
            )
            AND liste_droits.libelle <> 'Créer une organisation'
        ORDER BY organisations_users.id, liste_droits.id;

-- On supprimer le profil "Administrateur" des DPO
DELETE FROM organisation_user_roles WHERE organisation_user_id IN (
    SELECT organisations_users.id
        FROM organisations_users
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
            SELECT organisations.id, organisations.dpo
                FROM organisations
                WHERE organisations.dpo IS NOT NULL
        )
);

-- On attribuer le profil "DPO" aux DPOs
INSERT INTO organisation_user_roles (organisation_user_id, role_id, created, modified)
    SELECT organisations_users.id, roles.id, NOW(), NOW()
        FROM organisations_users
            INNER JOIN roles ON (roles.organisation_id = organisations_users.organisation_id)
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
                SELECT organisations.id, organisations.dpo
                    FROM organisations
                    WHERE organisations.dpo IS NOT NULL
            )
            AND roles.libelle = 'DPO'
        ORDER BY organisations_users.id, roles.id;

INSERT INTO crons(organisation_id, nom, action, active, lock, next_execution_time, execution_duration, created, modified)
    SELECT
            id,
            'Conversion des annexes',
            'conversionAnnexes',
            'true',
            'false',
            NOW(),
            'PT20M',
            NOW(),
            NOW()
        FROM organisations
        WHERE NOT EXISTS(
            SELECT existant.id
                FROM crons AS existant
                WHERE
                    existant.organisation_id = organisations.id
                    AND existant.action = 'conversionAnnexes'
        );

UPDATE crons
    SET
        active = true,
        lock = false,
        next_execution_time = NOW(),
        execution_duration = 'PT20M'
    WHERE
        active = false
        AND
        action = 'conversionAnnexes';

INSERT INTO crons(organisation_id, nom, action, active, lock, created, modified)
    SELECT
            id,
            'LDAP : Synchronisation des utilisateurs et groupes',
            'syncLdap',
            'false',
            'false',
            NOW(),
            NOW()
        FROM organisations
        WHERE NOT EXISTS(
            SELECT existant.id
                FROM crons AS existant
                WHERE
                    existant.organisation_id = organisations.id
                    AND existant.action = 'syncLdap'
        );

--
-- Création de la table modeles presentation
--
CREATE TABLE modele_presentations (
    id SERIAL  NOT NULL PRIMARY KEY,
    organisations_id  INTEGER NOT NULL REFERENCES organisations (id) ON DELETE CASCADE ON UPDATE CASCADE,
    name_modele VARCHAR(100) NOT NULL,
    fichier VARCHAR(100) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);
ALTER TABLE organisations ADD COLUMN usemodelepresentation BOOLEAN DEFAULT FALSE;

COMMIT;
