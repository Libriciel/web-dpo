BEGIN;

INSERT INTO role_droits (role_id, liste_droit_id)
    SELECT roles.id, liste_droits.id
        FROM roles
            INNER JOIN liste_droits ON (1 = 1)
        WHERE
            roles.libelle = 'DPO'
            AND liste_droits.libelle IN (
                'Créer un article dans la FAQ',
                'Modifier un article dans la FAQ',
                'Consulter la FAQ',
                'Supprimer un article dans la FAQ',
                'Gestion du typage des annexes',
                'Consulter tous les traitements dans l''entité',
                'Gestion des co-responsables lors de la déclaration d''un traitement',
                'Gestion des sous-traitants lors de la déclaration d''un traitement'
            )
            AND liste_droits.id NOT IN (
                SELECT existing_roles_droits.liste_droit_id
                    FROM role_droits AS existing_roles_droits
                    WHERE existing_roles_droits.role_id = roles.id
            )
        ORDER BY roles.id, liste_droits.id;

INSERT INTO droits (organisation_user_id, liste_droit_id, created, modified)
    SELECT organisations_users.id, liste_droits.id, NOW(), NOW()
        FROM organisations_users
            INNER JOIN liste_droits ON (1 = 1)
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
                SELECT organisations.id, organisations.dpo
                    FROM organisations
                    WHERE organisations.dpo IS NOT NULL
            )
            AND liste_droits.libelle IN (
                'Créer un article dans la FAQ',
                'Modifier un article dans la FAQ',
                'Consulter la FAQ',
                'Supprimer un article dans la FAQ',
                'Gestion du typage des annexes',
                'Consulter tous les traitements dans l''entité',
                'Gestion des co-responsables lors de la déclaration d''un traitement',
                'Gestion des sous-traitants lors de la déclaration d''un traitement'
            )
            AND liste_droits.id NOT IN (
                SELECT existing_droits.liste_droit_id
                    FROM droits AS existing_droits
                    WHERE existing_droits.organisation_user_id = organisations_users.id
            )
        ORDER BY organisations_users.id, liste_droits.id;

COMMIT;
