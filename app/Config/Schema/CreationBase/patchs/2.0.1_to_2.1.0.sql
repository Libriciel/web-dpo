BEGIN;

ALTER TABLE responsables ALTER COLUMN siretstructure DROP NOT NULL;
DROP INDEX IF EXISTS responsables_raisonsocialestructure_siretstructure_idx;
CREATE UNIQUE INDEX responsables_emailstructure_idx ON responsables (emailstructure);
CREATE UNIQUE INDEX responsables_numerocnil_dpo_idx ON responsables (numerocnil_dpo);

ALTER TABLE soustraitants ALTER COLUMN siretstructure DROP NOT NULL;
DROP INDEX IF EXISTS soustraitants_raisonsocialestructure_siretstructure_idx;
DROP INDEX IF EXISTS soustraitants_siret_idx;
CREATE UNIQUE INDEX soustraitants_siretstructure_idx ON soustraitants (siretstructure);

CREATE TABLE formulaires_organisations (
    id SERIAL PRIMARY KEY NOT NULL,
    formulaire_id INTEGER NOT NULL REFERENCES formulaires(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organisation_id INTEGER NOT NULL REFERENCES organisations(id) ON DELETE CASCADE ON UPDATE CASCADE,
    active BOOL NOT NULL DEFAULT FALSE
);
CREATE UNIQUE INDEX formulaires_organisations_formulaire_id_organisation_id_idx ON formulaires_organisations (formulaire_id, organisation_id);

INSERT INTO formulaires_organisations (
formulaire_id,
organisation_id
)
    SELECT
        formulaires.id,
        formulaires.organisations_id
    FROM formulaires;

ALTER TABLE formulaires ADD COLUMN createdbyorganisation INT DEFAULT NULL;
UPDATE formulaires SET createdbyorganisation = organisations_id;
ALTER TABLE formulaires DROP COLUMN organisations_id ;

CREATE TABLE formulaires_modeles_organisations (
    id SERIAL PRIMARY KEY NOT NULL,
    formulaire_id INTEGER NOT NULL REFERENCES formulaires(id) ON DELETE CASCADE ON UPDATE CASCADE,
    modele_id INTEGER NOT NULL REFERENCES modeles(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organisation_id INTEGER NOT NULL REFERENCES organisations(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX formulaires_modeles_organisations_formulaire_id_modele_id_organisation_id_idx ON formulaires_modeles_organisations (formulaire_id, modele_id, organisation_id);
CREATE UNIQUE INDEX formulaires_modeles_organisations_formulaire_id_organisation_id_idx ON formulaires_modeles_organisations (formulaire_id, organisation_id);

ALTER TABLE modeles RENAME COLUMN formulaires_id TO formulaire_id ;
--CREATE UNIQUE INDEX modeles_name_modele_idx ON modeles (name_modele);
CREATE UNIQUE INDEX modeles_fichier_idx ON modeles (fichier);
CREATE UNIQUE INDEX modeles_name_modele_fichier_idx ON modeles (name_modele, fichier);

CREATE TABLE modele_extrait_registres_organisations (
    id SERIAL PRIMARY KEY NOT NULL,
    modele_extrait_registre_id INTEGER NOT NULL REFERENCES modele_extrait_registres(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organisation_id INTEGER NOT NULL REFERENCES organisations(id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX modele_extrait_registres_organisations_modele_extrait_registre_id_organisation_id_idx ON modele_extrait_registres_organisations (modele_extrait_registre_id, organisation_id);
CREATE UNIQUE INDEX modele_extrait_registres_organisations_organisation_id_idx ON modele_extrait_registres_organisations (organisation_id);

INSERT INTO modele_extrait_registres_organisations (
modele_extrait_registre_id,
organisation_id
)
    SELECT
        modele_extrait_registres.id,
        modele_extrait_registres.organisations_id
    FROM modele_extrait_registres;

ALTER TABLE modele_extrait_registres DROP COLUMN organisations_id ;
CREATE UNIQUE INDEX modele_extrait_registres_name_modele_fichier_idx ON modele_extrait_registres (name_modele, fichier);
--  CREATE UNIQUE INDEX modele_extrait_registres_name_modele_idx ON modele_extrait_registres (name_modele);
CREATE UNIQUE INDEX modele_extrait_registres_fichier_idx ON modele_extrait_registres (fichier);

ALTER TABLE organisations ALTER COLUMN usemodelepresentation SET DEFAULT TRUE;
UPDATE organisations SET usemodelepresentation = true;

ALTER TABLE formulaires ADD COLUMN archive BOOLEAN DEFAULT FALSE;

INSERT INTO liste_droits (libelle, value, created, modified) VALUES
('Dupliquer un traitement', 36, NOW(), NOW()),
('Initialisation d''un traitement', 37, NOW(), NOW()),
('Générer un traitement en cours de rédaction', 38, NOW(), NOW()),
('Modifier un traitement reçu en consultation', 39, NOW(), NOW());

INSERT INTO role_droits (role_id, liste_droit_id)
    SELECT roles.id, liste_droits.id
        FROM roles
            INNER JOIN liste_droits ON (1 = 1)
        WHERE
            roles.libelle = 'DPO'
            AND liste_droits.libelle IN (
                'Dupliquer un traitement',
                'Initialisation d''un traitement',
                'Générer un traitement en cours de rédaction',
                'Modifier un traitement reçu en consultation'
            )
            AND liste_droits.id NOT IN (
                SELECT existing_roles_droits.liste_droit_id
                FROM role_droits AS existing_roles_droits
                WHERE existing_roles_droits.role_id = roles.id
            )
        ORDER BY roles.id, liste_droits.id;

INSERT INTO droits (organisation_user_id, liste_droit_id, created, modified)
    SELECT organisations_users.id, liste_droits.id, NOW(), NOW()
        FROM organisations_users
            INNER JOIN liste_droits ON (1 = 1)
        WHERE (organisations_users.organisation_id, organisations_users.user_id) IN (
            SELECT organisations.id, organisations.dpo
            FROM organisations
            WHERE organisations.dpo IS NOT NULL
        )
        AND liste_droits.libelle IN (
            'Dupliquer un traitement',
            'Initialisation d''un traitement',
            'Générer un traitement en cours de rédaction',
            'Modifier un traitement reçu en consultation'
        )
        AND liste_droits.id NOT IN (
            SELECT existing_droits.liste_droit_id
            FROM droits AS existing_droits
            WHERE existing_droits.organisation_user_id = organisations_users.id
        )
    ORDER BY organisations_users.id, liste_droits.id;

ALTER TABLE fiches ADD COLUMN valide BOOLEAN DEFAULT TRUE;

UPDATE etats SET libelle = 'Initialisation du traitement' WHERE value = 11;

ALTER TABLE fiches ADD COLUMN service_id INT DEFAULT NULL REFERENCES services(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE fiches ADD COLUMN partage BOOLEAN;
UPDATE fiches SET partage = false;

INSERT INTO services (libelle, organisation_id, created, modified)
    SELECT valeurs.valeur, fiches.organisation_id, NOW(), NOW()
        FROM valeurs, fiches
    WHERE
        valeurs.fiche_id = fiches.id
        AND valeurs.champ_name = 'declarantservice'
        AND valeurs.valeur != 'Aucun service'
        AND valeurs.valeur NOT IN (SELECT srv.libelle FROM services AS srv WHERE srv.organisation_id = fiches.organisation_id)
    GROUP BY valeurs.valeur, fiches.organisation_id
;

UPDATE fiches
    SET service_id = services.id
    FROM valeurs, services
    WHERE
        valeurs.fiche_id = fiches.id
        AND valeurs.champ_name = 'declarantservice'
        AND valeurs.valeur != 'Aucun service'
        AND services.libelle = valeurs.valeur
        AND services.organisation_id = fiches.organisation_id
;

DELETE FROM valeurs WHERE champ_name = 'declarantservice';

CREATE TABLE jetons (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
    php_sid VARCHAR NOT NULL,
    controller VARCHAR NOT NULL,
    action VARCHAR NOT NULL,
    params VARCHAR NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);

ALTER TABLE organisations ADD COLUMN usefieldsredacteur BOOLEAN DEFAULT FALSE;

ALTER TABLE organisations ADD COLUMN emaildpo VARCHAR(100);

UPDATE organisations
    SET emaildpo = users.email
    FROM users
    WHERE
        organisations.dpo = users.id
;

ALTER TABLE organisations ADD CONSTRAINT organisations_emaildpo_email_chk CHECK (cakephp_validate_email(emaildpo));
UPDATE valeurs SET champ_name = 'rt_organisation_emaildpo' WHERE champ_name = 'rt_organisation_email_dpo';

ALTER TABLE connecteur_ldaps ADD COLUMN civilite VARCHAR(100);

UPDATE liste_droits SET libelle = 'Gestion de la maintenance' WHERE libelle = 'Gestion de la maitenance';

ALTER TABLE etats ALTER COLUMN libelle TYPE VARCHAR(100);
INSERT INTO etats (libelle, value, created, modified) VALUES
('Traitement envoyé par un membre du service déclarant', 14, NOW(), NOW());

COMMIT;
