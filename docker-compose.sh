#!/usr/bin/env bash
# @todo: reporter dans LS-PKi
CLEAR="0"

# ----------------------------------------------------------------------------------------------------------------------
# "Library"
# ----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

# Internal constants

declare -r _blue_="\e[34m"
declare -r _cyan_="\e[36m"
declare -r _default_="\e[0m"
declare -r _green_="\e[32m"
declare -r _red_="\e[31m"

# Bootstrap

if [ "`getopt --longoptions xtrace -- x "$@" 2> /dev/null | grep --color=none "\(^\|\s\)\(\-x\|\-\-xtrace\)\($\|\s\)"`" != "" ] ; then
    declare -r __XTRACE__=1
    set -o xtrace
else
    declare -r __XTRACE__=0
fi

declare -r __PID__="${$}"

declare -r __FILE__="$(realpath "${0}")"
declare -r __SCRIPT__="$(basename "${__FILE__}")"
declare -r __ROOT__="$(realpath "$(dirname "${__FILE__}")")"

printf "${_cyan_}Startup:${_default_} started process ${__PID__}\n\n"

# Error and exit handling: exit is trapped, as well as signals.
# If a __cleanup__ function exists, it will be called on signal or exit and the exit code will be passed as parameter.

__trap_signals__()
{
    local code="${?}"
    if [ ${code} -ne 0 ] ; then
        local signal=$((${code} - 128))
        local name="`kill -l ${signal}`"

        >&2 printf "\nProcess ${__PID__} received SIG${name} (${signal}), exiting..."
    fi
}

__trap_exit__()
{
    local code="${?}"

    if [ ${code} -eq 0 ] ; then
        printf "\n${_green_}Success:${_default_} process ${__PID__} exited normally\n"
    else
        >&2 printf "\n${_red_}Error:${_default_} process ${__PID__} exited with error code ${code}\n"
    fi

    if [ "`type -t __cleanup__`" = "function" ] ; then
        __cleanup__
    fi
}

trap "__trap_signals__" SIGHUP SIGINT SIGQUIT SIGTERM
trap "__trap_exit__" EXIT

# ----------------------------------------------------------------------------------------------------------------------
# Custom code
# ----------------------------------------------------------------------------------------------------------------------

COMPOSE_FILE="${__ROOT__}/docker-compose.yml"
IMAGE_NAMES="web-dpo-app web-dpo-cloudooo web-dpo-db web-dpo-golem web-dpo-mailcatcher"
PROJECT_NAME="web-dpo"

__usage__()
{
    printf "NAME\n"
    printf "  %s\n" "${__SCRIPT__}"
    printf "\nDESCRIPTION\n"
    printf "  Commandes courantes docker-compose pour le projet web-DPO\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION] [COMMAND]\n" "${__SCRIPT__}"
    printf "\nCOMMANDS\n"
    printf "  clear\t\tSupprime les images docker de web-DPO\n"
    printf "  web-dpo\tDémarrage de web-DPO\n"
    printf "\nOPTIONS\n"
    printf "  -c|--clear\tSupprime les images docker de web-DPO avant de les reconstruire\n"
    printf "  -h|--help\tAffiche cette aide\n"
    printf "  -x|--xtrace\tMode debug, affiche chaque commande avant de l'exécuter (set -o xtrace)\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "${__SCRIPT__}"
    printf "  %s clear\n" "${__SCRIPT__}"
    printf "  %s --clear web-dpo\n" "${__SCRIPT__}"
}

__clear_images__()
{
    if [ "$CLEAR" -eq "1" ]
    then
        docker image rmi -f ${IMAGE_NAMES} > /dev/null 2>&1 \
        && docker image prune -f
    fi

    return ${?}
}

__web_dpo__()
{
    ( \
        __clear_images__ \
        && docker-compose -f "${COMPOSE_FILE}" --project-name ${PROJECT_NAME} down -v --remove-orphans \
        && docker-compose -f "${COMPOSE_FILE}" --project-name ${PROJECT_NAME} up \
        && docker-compose -f "${COMPOSE_FILE}" --project-name ${PROJECT_NAME} down -v --remove-orphans \
    )

    EXIT_STATUS=${?}
    if [ ${EXIT_STATUS} -ne 0 ]
    then
        echo >&2  "Erreur(s) lors du lancement du docker web-DPO (exit ${EXIT_STATUS})"
    fi

    return ${EXIT_STATUS}
}

# ----------------------------------------------------------------------------------------------------------------------
# Main function
# ----------------------------------------------------------------------------------------------------------------------

__main__()
{
    (
        opts=`getopt --longoptions clear,help,xtrace -- chx "${@}"` || ( >&2 __usage__ ; exit 1 )
        eval set -- "$opts"
        while true ; do
            case "${1}" in
                -c|--clear)
                    CLEAR="1"
                    shift
                ;;
                -h|--help)
                    __usage__
                    exit 0
                ;;
                -x|--xtrace)
                    shift
                ;;
                --)
                    shift
                    break
                ;;
                *)
                    >&2 __usage__
                    exit 1
                ;;
            esac
        done

        case "${1:-}" in
            clear)
                CLEAR="1"
                __clear_images__
                exit ${?}
            ;;
            web-dpo)
                shift
                __web_dpo__
                exit ${?}
            ;;
            --)
                shift
            ;;
            *)
                >&2 __usage__
                exit 1
            ;;
        esac

        exit 0
    )
}

__main__ "${@}"
