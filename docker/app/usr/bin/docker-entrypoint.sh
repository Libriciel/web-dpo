#!/usr/bin/env bash
# @fixme: mv: cannot remove '/var/www/html/web-dpo/app/tmp/out': Device or resource busy
# @fixme: la documentation n'est pas à jour https://gitlab.libriciel.fr/web-DPO/DP-Documentations_installation/dp-doc-installations/ubuntu/DP-DOC-installation_Ubuntu_18.04#mise-en-place-du-sch%C3%A9ma-de-la-base-de-donn%C3%A9e
# @fixme
# web-dpo-app            | docker-entrypoint.sh: symlinks...
# web-dpo-app            | mv: cannot remove '/var/www/html/web-dpo/app/tmp/out': Device or resource busy
#-----------------------------------------------------------------------------------------------------------------------
# @info: tail -f /var/log/syslog | grep -i --color=none "CRON\["
#-----------------------------------------------------------------------------------------------------------------------
if [ "`getopt --longoptions xtrace -- x "$@" 2> /dev/null | grep --color=none "\(^\|\s\)\(\-x\|\-\-xtrace\)\($\|\s\)"`" != "" ] ; then
    declare -r __XTRACE__=1
    set -o xtrace
else
    declare -r __XTRACE__=0
fi
#-----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

__FILE__="$(realpath "$0")"
__SCRIPT__="$(basename "${__FILE__}")"
__ROOT__="$(dirname "${__FILE__}")/.."
__ROOT__="$(realpath "${__ROOT__}")"

declare -r __DEFAULT_APP_DIR__=/var/www/html/web-dpo

__APP_DIR__=$__DEFAULT_APP_DIR__
__DATA_DIR__=/data/web-dpo
__WWW_USER__=www-data

__COMPOSER_INSTALL__=0
__CONFIGURE__=0
__FIX_IMAGEMAGICK_POLICY__=0
__FIX_PHPUNIT_SRC__=0
__INIT_DATA_DB__=0
__INIT_TEST_DB__=0
__PERMISSIONS__=0
__START_SERVICES__=0
__SYMLINKS__=0

#-----------------------------------------------------------------------------------------------------------------------

__usage__()
{
    printf "NAME\n"
    printf "  %s\n" "${__SCRIPT__}"
    printf "\nDESCRIPTION\n"
    printf "  Script de développement sous Docker pour web-dpo\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION]...\n" "${__SCRIPT__}"
    printf "\nOPTIONS\n"
    printf "  --app-dir=<DIR>\tPermet de spécifier le dossier dans lequel se trouve l'appication (par défaut: ${__DEFAULT_APP_DIR__})\n"
    printf "  --composer-install\tExécute la commande composer install\n"
    printf "  --configure\t\tConfigure le fichier webdepo.inc\n"
    printf "  --dev\t\t\tEnvironnement de développement (composer-install, configure, fix-imagemagick-policy, fix-phpunit-src, init-data-db, init-test-db, permissions, start-services, symlinks)\n"
    printf "  -h|--help\t\tAffiche cette aide\n"
    printf "  --fix-imagemagick-policy\tModifie le fichier /etc/ImageMagick-6/policy.xml pour permettre la conversion de fichiers PDF\n"
    printf "  --fix-phpunit-src\tCorrige les sources de PHPUnit 3.7 et 4.0 afin d'être compatibles avec PHP >= 7.2\n"
    printf "  --init-data-db\tInitialise la base de données de production (${DATA_HOST}/${DATA_DB})\n"
    printf "  --init-test-db\tInitialise la base de données de tests (${TEST_HOST}/${TEST_DB})\n"
    printf "  --permissions\t\tChange les permissions pour l'utilisateur %s\n" "${__WWW_USER__}"
    printf "  --start-services\tDémarre les services nécessaires (rsyslog, cron et nginx)\n"
    printf "  --symlinks\t\tCrée les liens symboliques nécessaire (@fixme)\n"
    printf "  -x|--xtrace\t\tTrace les commandes avant de les exécuter (set -o xtrace)\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "${__SCRIPT__}"
    printf "  %s --dev\n" "${__SCRIPT__}"
    printf "  %s --fix-phpunit-src -x\n" "${__SCRIPT__}"
    printf "  %s --init-data-db --init-test-db --start-services\n" "${__SCRIPT__}"
}

#-----------------------------------------------------------------------------------------------------------------------

opts=$(getopt --longoptions app-dir:,composer-install,configure,dev,fix-imagemagick-policy,fix-phpunit-src,help,init-data-db,init-test-db,permissions,symlinks,start-services,xtrace -- x "$@") || ( >&2 __usage__ ; exit 1)
eval set -- "$opts"
while true; do
    case "${1}" in
        --app-dir)
            __APP_DIR__="${2}"
            shift 2
            ;;
        --composer-install)
            __COMPOSER_INSTALL__=1
            shift
            ;;
        --configure)
            __CONFIGURE__=1
            shift
            ;;
        --dev)
            __COMPOSER_INSTALL__=1
            __CONFIGURE__=1
            __FIX_IMAGEMAGICK_POLICY__=1
            __FIX_PHPUNIT_SRC__=1
            __INIT_DATA_DB__=1
            __INIT_TEST_DB__=1
            __PERMISSIONS__=1
            __START_SERVICES__=1
            __SYMLINKS__=1
            shift
            ;;
        -h|--help)
            __usage__
            exit 0
            ;;
        --fix-imagemagick-policy)
            __FIX_IMAGEMAGICK_POLICY__=1
            shift
            ;;
        --fix-phpunit-src)
            __FIX_PHPUNIT_SRC__=1
            shift
            ;;
        --init-data-db)
            __INIT_DATA_DB__=1
            shift
            ;;
        --init-test-db)
            __INIT_TEST_DB__=1
            shift
            ;;
        --permissions)
            __PERMISSIONS__=1
            shift
            ;;
        --start-services)
            __START_SERVICES__=1
            shift
            ;;
        --symlinks)
            __SYMLINKS__=1
            shift
            ;;
        -x|--xtrace)
            shift
            ;;
        --)
            shift
            break
            ;;
    esac
done

#-----------------------------------------------------------------------------------------------------------------------

wait_for_postgres()
{
    local hostname="${1}"
    local database="${2}"
    local username="${3}"
    local password="${4}"
    local status=1

    (
        set +o errexit

        while [ $status -ne 0 ]; do
            sleep 1
            psql postgres://${username}:${password}@${hostname}/${database} -v "ON_ERROR_STOP=1" -c "\q"
            status=$?
            echo "-> status ${status}"
        done
    )

    if [ $status -eq 0 ]; then
        echo "BDD ${database} correctement activee sur ${hostname} (code: ${status})"
    else
        echo "Impossible d'activer la BDD ${database} sur ${hostname} (code: ${status})" >&2
    fi
}

wait_for_postgres_data_host()
{
    wait_for_postgres "${DATA_HOST}" "${DATA_DB}" "${DATA_USER}" "${DATA_PASSWORD}"
}

wait_for_postgres_test_host()
{
    wait_for_postgres "${TEST_HOST}" "${DATA_DB}" "${TEST_USER}" "${TEST_PASSWORD}"
}

init_data_db()
{
    printf "%s: initialisation de la base de données de production...\n" "${__SCRIPT__}"

    wait_for_postgres_data_host

    files=(
        ${__APP_DIR__}/app/Plugin/Postgres/Config/sql/cakephp_validate_core.sql
        ${__APP_DIR__}/app/Plugin/Postgres/Config/sql/cakephp_validate_custom.sql
        ${__APP_DIR__}/app/Plugin/LdapManager/Config/Schema/create.sql
        ${__APP_DIR__}/app/Plugin/LdapManager/Config/Schema/patchs/1.0.0_to_1.0.1.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/01-createbase.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/02-insertMinimumData.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/1.0.0_to_1.0.1.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/1.0.1_to_1.1.0.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/1.1.0_to_2.0.0.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/2.0.0_to_2.0.1.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/2.0.1_to_2.1.0.sql
        ${__APP_DIR__}/app/Config/Schema/CreationBase/patchs/2.1.0_to_2.1.1.sql
    )

    if [ "${ADD_DATA_DEV}" = "1" ] ; then
        files+=(${__APP_DIR__}/app/Config/Schema/CreationBase/data-dev/2.1.3.sql)
    fi

    for file in ${files[*]}; do
        echo "Installation de ${file}"
        psql postgres://${DATA_USER}:${DATA_PASSWORD}@${DATA_HOST}/${DATA_DB} -v "ON_ERROR_STOP=1" -f "${file}"
    done
}

init_test_db()
{
    printf "%s: initialisation de la base de données de test...\n" "${__SCRIPT__}"

    wait_for_postgres_test_host

    psql postgres://${TEST_USER}:${TEST_PASSWORD}@${TEST_HOST} -v "ON_ERROR_STOP=1" -c "DROP DATABASE IF EXISTS \"${TEST_DB}\";"
    psql postgres://${TEST_USER}:${TEST_PASSWORD}@${TEST_HOST} -v "ON_ERROR_STOP=1" -c "CREATE DATABASE \"${TEST_DB}\" OWNER \"${TEST_USER}\" ENCODING 'UTF8';"
}

fix_phpunit_src_3_7()
{
    local file=""

    # phpunit/phpunit (3.7.18)
    file="vendors/phpunit/phpunit/PHPUnit/Framework/Comparator/DOMDocument.php"
    if [ -f "${file}" ] ; then
      sed -i 's/public function assertEquals(\$expected, \$actual, \$delta = 0, \$canonicalize = FALSE, \$ignoreCase = FALSE)/public function assertEquals(\$expected, \$actual, \$delta = 0, \$canonicalize = FALSE, \$ignoreCase = FALSE, array \&\$processed = array())/g' "${file}"
    fi

    file="vendors/phpunit/php-code-coverage/PHP/CodeCoverage/Report/HTML/Renderer/File.php"
    if [ -f "${file}" ] ; then
      sed -i 's/\$numTests = count(\$coverageData\[\$i\]);/\$numTests = count((array)\$coverageData[\$i]);/g' "${file}"
    fi
}

fix_phpunit_src_4_0()
{
    local file=""

    # phpunit/phpunit (4.0.0)
    file="vendors/phpunit/phpunit/src/Framework/Comparator/DOMNode.php"
    if [ -f "${file}" ] ; then
        sed -i 's/public function assertEquals(\$expected, \$actual, \$delta = 0\.0, \$canonicalize = false, \$ignoreCase = false)/public function assertEquals(\$expected, \$actual, \$delta = 0.0, \$canonicalize = false, \$ignoreCase = false, array \&\$processed = array())/g' "${file}"
    fi

    file="vendors/phpunit/phpunit/src/Framework/Comparator/DateTime.php"
    if [ -f "${file}" ] ; then
        sed -i 's/public function assertEquals(\$expected, \$actual, \$delta = 0\.0, \$canonicalize = false, \$ignoreCase = false)/public function assertEquals(\$expected, \$actual, \$delta = 0.0, \$canonicalize = false, \$ignoreCase = false, array \&\$processed = array())/g' "${file}"
    fi

    # @see https://github.com/sebastianbergmann/php-code-coverage/pull/554
    file="vendors/phpunit/php-code-coverage/src/CodeCoverage/Report/HTML/Renderer/File.php"
    if [ -f "${file}" ] ; then
        sed -i 's/\$numTests = count(\$coverageData\[\$i\]);/\$numTests = (\$coverageData[\$i] ? count(\$coverageData[\$i]) : 0);/g' "${file}"
    fi
}

fix_imagemagick_policy()
{
    sed -i.orig -e's#<policy domain="coder" rights="none" pattern="PDF" />#<policy domain="coder" rights="read" pattern="PDF" />#' /etc/ImageMagick-6/policy.xml
}

fix_phpunit_src()
{
    if [ ! -f "vendors/bin/phpunit" ] ; then
        >&2 printf "Impossible de trouver %s\n" "vendors/bin/phpunit"
        exit 1
    fi

    local version="$( vendors/bin/phpunit --version | grep --color=none -v "^$" | sed 's/^PHPUnit \(.*\) by.*$/\1/ig' )"
    printf "%s: fixing phpunit ${version} sources for PHP 7.*...\n" "${__SCRIPT__}"

    case "${version}" in
        3.7.*)
            fix_phpunit_src_3_7
        ;;
        4.0.*)
            fix_phpunit_src_4_0
        ;;
        *)
            >&2 printf "Impossible de corriger les sources de PHPUnit en version %s (versions supportées: 3.7.* et 4.0.*)\n" "${version}"
            exit 1
        ;;
    esac
}

__symlinks__()
{
    printf "%s: symlinks...\n" "${__SCRIPT__}"

    dirs=(
        "${__DATA_DIR__}/app/Config"
        "${__DATA_DIR__}/app/tmp"
    )

    for dir in ${dirs[*]}; do
        if [ ! -d "${dir}" ] ; then
            mkdir -p "${dir}"
        fi
    done

    if [ ! -h ${__APP_DIR__}/app/tmp ] ; then
        mv ${__APP_DIR__}/app/tmp ${__DATA_DIR__}/app \
        && ln -s ${__DATA_DIR__}/app/tmp ${__APP_DIR__}/app/tmp
    fi

    # Création du lien symbolique pour les logos de entités
    rm -rf ${__APP_DIR__}/app/webroot/img/logos/logo_organisation \
    && mkdir -p ${__APP_DIR__}/app/webroot/img/logos \
    && chown -R www-data: ${__APP_DIR__}/app/webroot/img/logos \
    && ln -s ${__DATA_DIR__}/workspace/files/logos ${__APP_DIR__}/app/webroot/img/logos/logo_organisation

    ${__APP_DIR__}/cake_utils.sh clear

    # @fixme
    files=(
        app/Config/webdpo.inc
    )

    for file in ${files[*]}; do
        rm -f ${__APP_DIR__}/${file} \
        && cp ${__APP_DIR__}/${file}.default ${__DATA_DIR__}/${file} \
        && ln -s ${__DATA_DIR__}/${file} ${__APP_DIR__}/${file}
    done

}

__configure__()
{
    printf "%s: configure...\n" "${__SCRIPT__}" \
    && sed -i "s/127\.0\.0\.1:8880/web-dpo-golem:8080/g" ${__DATA_DIR__}/app/Config/webdpo.inc \
    && sed -i "s/define(\"WORKSPACE\", DS . 'data' . DS . 'workspace');/define(\"WORKSPACE\", DS . 'data' . DS . 'web-dpo' . DS . 'workspace');/g" ${__DATA_DIR__}/app/Config/webdpo.inc \
    && sed -i "s/'FusionConv\.cloudooo_host', '127\.0\.0\.1'/'FusionConv.cloudooo_host', 'web-dpo-cloudooo'/g" ${__DATA_DIR__}/app/Config/webdpo.inc \
    && sed -i "s/'FusionConv.FusionConvConverterCloudooo.server', '127.0.0.1'/'FusionConv.FusionConvConverterCloudooo.server', 'web-dpo-cloudooo'/g" ${__DATA_DIR__}/app/Config/webdpo.inc
}

__permissions__()
{
    printf "%s: permissions...\n" "${__SCRIPT__}" \
    && chown -R ${__WWW_USER__}: "${__APP_DIR__}/app/tmp/" "${__DATA_DIR__}" \
    && chmod o+rwX,g+rwX -R ${__APP_DIR__}/app/tmp/
}

if [ ${__COMPOSER_INSTALL__} -eq 1 ]; then
    printf "%s: composer install...\n" "${__SCRIPT__}"
    composer install
fi

if [ ${__FIX_IMAGEMAGICK_POLICY__} -eq 1 ]; then
    fix_imagemagick_policy
fi

if [ ${__FIX_PHPUNIT_SRC__} -eq 1 ]; then
    fix_phpunit_src
fi

if [ ${__INIT_DATA_DB__} -eq 1 ]; then
    init_data_db
fi

if [ ${__INIT_TEST_DB__} -eq 1 ]; then
    init_test_db
fi

if [ ${__SYMLINKS__} -eq 1 ]; then
    __symlinks__
fi

if [ ${__CONFIGURE__} -eq 1 ]; then
    __configure__
fi

if [ ${__PERMISSIONS__} -eq 1 ]; then
    __permissions__
fi

if [ ${__START_SERVICES__} -eq 1 ]; then
    printf "%s: démarrage des services...\n" "${__SCRIPT__}" \
    && service rsyslog start \
    && service cron start \
    && ( cd ${__APP_DIR__} && sudo -u ${__WWW_USER__} app/Console/cake Postgres.postgres_maintenance all ) \
    && apachectl -D FOREGROUND
fi

if [ ${__COMPOSER_INSTALL__} -eq 0 ] && [ ${__CONFIGURE__} -eq 0 ] && [ ${__FIX_PHPUNIT_SRC__} -eq 0 ] && [ ${__INIT_DATA_DB__} -eq 0 ] && [ ${__INIT_TEST_DB__} -eq 0 ] && [ ${__START_SERVICES__} -eq 0 ] ; then
    >&2 printf "%s: rien à exécuter, sortie du script...\n\n" "${__SCRIPT__}"
    >&2 __usage__
    exit 1
fi
