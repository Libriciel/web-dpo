SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE USER "webdpo" WITH CREATEDB ENCRYPTED PASSWORD 'webdpo';
CREATE DATABASE "webdpo" OWNER "webdpo" ENCODING 'UTF8';
