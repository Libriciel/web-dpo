# Application web-DPO v2.1.3
[![Minimum PHP Version](https://img.shields.io/badge/php-7.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/licence-AGPL%20v3-blue.svg)](http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
[![Requires](https://img.shields.io/badge/requires-CakePHP%202.10-navy.svg)](http://book.cakephp.org/3.0/fr/index.html)

## Présentation
Vous êtes en présence des sources de l'application **web-DPO** version 2.1.3

Avant toute nouvelle installation, veuillez faire une sauvegarde de la base de données ainsi que la version actuelle des
sources.

Assurez-vous que l'activité sur la plate-forme est réduite au minimum en programmant une interruption de service et
veuillez arrêter les tâches planifiées.

## Nouvelle installation
Veuillez suivre les indications décrites dans la documentation d'installation :

### Pré-requis
- CakePHP 2.10
- PHP 7.4
- PostgrSQL 12.2

### Support
Les produits édités par **Libriciel** sont libres sous licence "AGPL v3".

Pour bénéficier du **support** de nos services pour l'installation et/ou la configuration du produit, ou pour bénéficier
de la **maintenance**, merci de contacter un commercial pour ouvrir un compte.
